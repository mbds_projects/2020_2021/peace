# PEACE

### Pré-requis
- Installer node 12.16.3 (la lts devrais fonctionner ... sinon utiliser nvm pour installer cette version précise)
- Installer yarn en global
- Installer noderize-scripts en global
- Installer altair graphql client
- Installer mongodb compass

### Installation

Les étapes pour installer votre programme....

- Dézippé marketplaceAPI-master.zip
- Faire un yarn install sur les 2 dossiers (client et serveur)
- Créez une base donnée mongodb https://www.mongodb.com/basics/create-database
- La remplir à partir du snapshot ClusterSR-2020-12-02T13-44-56.667Z.tgz https://docs.mongodb.com/manual/tutorial/backup-with-filesystem-snapshots/#restore-directly-from-a-snapshot OU en ajoutant chaque collection une à une depuis le dossier DB (le nom de la collection doit être exactement comme le nom du fichier, ex: Vendors)
- Configurer le fichier client/src/config/env/local.js
- Configurer le fichier server/src/config/env/local.js

## Démarrage

- Ouvrir un terminal, aller sur le dossier serveur, et tapez ``yarn watch``
- Ouvrir un second terminal, aller sur le dossier client, et tapez ``yarn start``

## Passez en admin

- S'inscrire sur le site
- Depuis la base de donnée, modifié l'utilisateur en role "admin"

