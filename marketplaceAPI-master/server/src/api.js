import Koa from 'koa'
import logger from 'koa-logger'
import helmet from 'koa-helmet'
import session from 'koa-session'
import bodyParser from 'koa-bodyparser'
import signale from 'signale'
import cors from '@koa/cors'
import {startAplPublic} from './graphQL/schemas/start'
import { graphqlUploadKoa } from 'graphql-upload'
import {ISPROD} from './config'
import serve from 'koa-static'
import mongoose from 'mongoose'

/*
* until static files koa-static dos not have koaV2 support
*/
import convert from 'koa-convert'


/*
* my configurations
*/
import { CONF } from './config'
import routing from './routes'


/*
* database
*/
mongoose.connect(CONF.database, {
    useNewUrlParser: true,
    retryWrites: true,
    authSource: 'admin'
})
mongoose.set('autoIndex', false);
mongoose.set('useCreateIndex', true)
mongoose.set('useFindAndModify', false)
mongoose.connection.on('error', console.error)

/*
* create app
*/
const app = new Koa()


/*
* graphQL types and resolvers for apollo
*/
startAplPublic(app)


/*
* keys
*/
app.keys = [CONF.secret];


const {
  default: sslify, // middleware factory with default options
  xForwardedProtoResolver: resolver // resolver, that sets out x-forwarded-proto header, needed for redirect on the Heroku platform
} = require('koa-sslify');
// initializing the middleware with resolver

/*
* using in app views and static files and others
*/
app
  .use(sslify({ resolver }))
  .use(cors())
  .use(logger())
  .use(bodyParser({
    "enableTypes": ['json', 'form', 'text'],
  }))
  .use(serve('../client/build'))
  .use(graphqlUploadKoa({ maxFileSize: 10000000, maxFiles: 10 }))
  .use(convert(session(app)))
  .use(helmet())


/*
* routes
*/
routing(app)


/*
* start listen
*/
app
  .listen(CONF.port, () => {
    signale.success(`Running at ${CONF.ip}:${CONF.port}/`)
  });

export default app
