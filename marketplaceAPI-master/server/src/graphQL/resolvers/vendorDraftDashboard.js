import { vendorDraftDashboard, updateInfoVendorDraftDashboard, updateCarouselPictureVendorDraftDashboard,
  updateFeaturedProductInfoVendorDraftDashboard, updateFeaturedProductPictureVendorDraftDashboard,
  publishDraftDashboard} from '../../models/vendorDraftDashboard'


export const resolvers = {
  Query: {
    vendorDraftDashboard:  async (root, args, context, info) => {
      return await vendorDraftDashboard(root, args, context, info)
    },
  },
  Mutation: {
    updateInfoVendorDraftDashboard: async (root, args, context, info) => {
      return await updateInfoVendorDraftDashboard(root, args, context, info)
    },
    updateCarouselPictureVendorDraftDashboard: async (root, args, context, info) => {
      return await updateCarouselPictureVendorDraftDashboard(root, args, context, info)
    },
    updateFeaturedProductInfoVendorDraftDashboard: async (root, args, context, info) => {
      return await updateFeaturedProductInfoVendorDraftDashboard(root, args, context, info)
    },
    updateFeaturedProductPictureVendorDraftDashboard: async (root, args, context, info) => {
      return await updateFeaturedProductPictureVendorDraftDashboard(root, args, context, info)
    },
    publishDraftDashboard: async (root, args, context, info) => {
      return await publishDraftDashboard(root, args, context, info)
    },
  }
}
