import { keywords, keywordsPopular, keywordsUsed, updateKeywordDesc, updatePictureKeyword } from '../../models/keyword'


export const resolvers = {
  Query: {
    keywords: async (root, args, context, info) => {
      return await keywords(root, args, context, info)
    },
    keywordsPopular: async (root, args, context, info) => {
      return await keywordsPopular(root, args, context, info)
    },
    keywordsUsed: async (root, args, context, info) => {
      return await keywordsUsed(root, args, context, info)
    }
  },
  Mutation: {
    updateKeywordDesc: async (root, args, context, info) => {
      return await updateKeywordDesc(root, args, context, info)
    },
    updatePictureKeyword: async (root, args, context, info) => {
      return await updatePictureKeyword(root, args, context, info)
    },
  }
}
