import { vendorTestimonies } from '../../models/vendorTestimony'


export const resolvers = {
  Query: {
    vendorTestimonies: async (root, args, context, info) => {
      return await vendorTestimonies(root, args, context, info)
    }
  },
  Mutation: {
  }
}
