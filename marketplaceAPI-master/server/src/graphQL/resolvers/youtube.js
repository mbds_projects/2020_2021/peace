import { youtubeRecent } from '../../models/youtube'


export const resolvers = {
  Query: {
    youtubeRecent: async (root, args, context, info) => {
      return await youtubeRecent(root, args, context, info)
    }
  }
}
