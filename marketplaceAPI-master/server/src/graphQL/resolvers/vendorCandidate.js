import { vendorsCandidate, vendorCandidate,vendorCandidateSaveInfo, createVendorCandidate, followNewsletterDashboard, transformVendorCandidateToVendor,vendorsendinfo } from '../../models/vendorCandidate'


export const resolvers = {
  Query: {
    vendorsCandidate: async (root, args, context, info) => {
      return await vendorsCandidate(root, args, context, info)
    },
    vendorCandidate: async (root, args, context, info) => {
      return await vendorCandidate(root, args, context, info)
    }
  },
  Mutation: {
    createVendorCandidate: async(root, args, context, info) => {
      return await createVendorCandidate(root, args, context, info);
    },
    followNewsletterDashboard: async (root, args, context, info) => {
      return await followNewsletterDashboard(root, args, context, info)
    },
    transformVendorCandidateToVendor: async (root, args, context, info) => {
      return await transformVendorCandidateToVendor(root, args, context, info)
    },
    vendorsendinfo: async (root, args, context, info) => {
      return await vendorsendinfo(root, args, context, info)
    },
    vendorCandidateSaveInfo: async (root, args, context, info) => {
      return await vendorCandidateSaveInfo(root, args, context, info)
    },
  }
}
