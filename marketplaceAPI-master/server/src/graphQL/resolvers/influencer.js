import { influencers } from '../../models/influencer'


export const resolvers = {
  Query: {
    influencers: async (root, args, context, info) => {
      return await influencers(root, args, context, info)
    }
  }
}
