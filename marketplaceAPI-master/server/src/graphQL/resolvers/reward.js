import { rewards, rewardsOfVendor, createReward, disableReward, enableReward, rewardsAdminVendor, createRewardAdmin } from '../../models/reward'


export const resolvers = {
  Query: {
    rewards: async (root, args, context, info) => {
      return await rewards(root, args, context, info)
    },
    rewardsOfVendor: async (root, args, context, info) => {
      return await rewardsOfVendor(root, args, context, info)
    },
    rewardsAdminVendor: async (root, args, context, info) => {
      return await rewardsAdminVendor(root, args, context, info)
    }
  },
  Mutation: {
    createReward: async (root, args, context, info) => {
      return await createReward(root, args, context, info)
    },
    createRewardAdmin: async (root, args, context, info) => {
      return await createRewardAdmin(root, args, context, info)
    },
    disableReward: async (root, args, context, info) => {
      return await disableReward(root, args, context, info)
    },
    enableReward: async (root, args, context, info) => {
      return await enableReward(root, args, context, info)
    }
  }
}
