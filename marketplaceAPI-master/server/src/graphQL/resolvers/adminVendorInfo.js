import {
  adminVendorInfoUpdate, adminVendorProfilePictureUpdate, adminVendorLogoUpdate
} from '../../models/adminVendorInfo'


export const resolvers = {
  Query: {
  },
  Mutation: {
    adminVendorInfoUpdate: async (root, args, context, info) => {
      return await adminVendorInfoUpdate(root, args, context, info)
    },
    adminVendorProfilePictureUpdate: async (root, args, context, info) => {
      return await adminVendorProfilePictureUpdate(root, args, context, info)
    },
    adminVendorLogoUpdate: async (root, args, context, info) => {
      return await adminVendorLogoUpdate(root, args, context, info)
    },
  }
}
