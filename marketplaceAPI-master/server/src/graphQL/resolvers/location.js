import {
  locationsByCity, locations, location, createLocation,
  updateLocation, searchAndCreateLocation, locationsOfVendorByCity,
  reverseLocation, locationsOfVendorByCounty, locationsByCounty,
  searchLocationDashboard, createLocationDashboard, updateLocationDashboard,
  allowedToCreateLocation, removeLocationFromVendorDashboard,
  createLocationAdminVendorDashboard, updateLocationAdminVendorDashboard, removeLocationAdminVendorDashboard
} from '../../models/location'


export const resolvers = {
  Query: {
    locationsByCity: async (root, args, context, info) => {
      return await locationsByCity(root, args, context, info)
    },
    locations: async (root, args, context, info) => {
      return await locations(root, args, context, info)
    },
    location: async (root, args, context, info) => {
      return await location(root, args, context, info)
    },
    locationsOfVendorByCity: async (root, args, context, info) => {
      return await locationsOfVendorByCity(root, args, context, info)
    },
    locationsByCounty: async (root, args, context, info) => {
      return await locationsByCounty(root, args, context, info)
    },
    locationsOfVendorByCounty: async (root, args, context, info) => {
      return await locationsOfVendorByCounty(root, args, context, info)
    },
    allowedToCreateLocation: async (root, args, context, info) => {
      return await allowedToCreateLocation(root, args, context, info)
    }, 
  },
  Mutation: {
    createLocation: async (root, args, context, info) => {
      return await createLocation(root, args, context, info)
    },
    updateLocation: async (root, args, context, info) => {
      return await updateLocation(root, args, context, info)
    },
    searchAndCreateLocation: async (root, args, context, info) => {
      return await searchAndCreateLocation(root, args, context, info)
    },
    reverseLocation: async (root, args, context, info) => {
      return await reverseLocation(root, args, context, info)
    },
    searchLocationDashboard: async (root, args, context, info) => {
      return await searchLocationDashboard(root, args, context, info)
    },
    createLocationDashboard: async (root, args, context, info) => {
      return await createLocationDashboard(root, args, context, info)
    },
    updateLocationDashboard: async (root, args, context, info) => {
      return await updateLocationDashboard(root, args, context, info)
    },
    removeLocationFromVendorDashboard: async (root, args, context, info) => {
      return await removeLocationFromVendorDashboard(root, args, context, info)
    },
    createLocationAdminVendorDashboard: async (root, args, context, info) => {
      return await createLocationAdminVendorDashboard(root, args, context, info)
    },
    updateLocationAdminVendorDashboard: async (root, args, context, info) => {
      return await updateLocationAdminVendorDashboard(root, args, context, info)
    },
    removeLocationAdminVendorDashboard: async (root, args, context, info) => {
      return await removeLocationAdminVendorDashboard(root, args, context, info)
    },
  }
}
