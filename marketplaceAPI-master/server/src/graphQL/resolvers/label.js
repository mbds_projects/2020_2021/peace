import { labels, updateLogoLabel, updateLabelName, createLabel } from '../../models/label'


export const resolvers = {
  Query: {
    labels: async (root, args, context, info) => {
      return await labels(root, args, context, info)
    }
  },
  Mutation: {
    createLabel: async (root, args, context, info) => {
      return await createLabel(root, args, context, info)
    },
    updateLogoLabel: async (root, args, context, info) => {
      return await updateLogoLabel(root, args, context, info)
    },
    updateLabelName: async (root, args, context, info) => {
      return await updateLabelName(root, args, context, info)
    }
  }
}
