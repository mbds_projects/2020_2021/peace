import { achievements } from '../../models/achievement'


export const resolvers = {
  Query: {
    achievements: async (root, args, context, info) => {
      return await achievements(root, args, context, info)
    }
  }
}
