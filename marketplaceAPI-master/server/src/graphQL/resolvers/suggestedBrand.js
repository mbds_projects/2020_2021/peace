import { suggestedBrand, suggestedBrands, createSuggestedBrand, updateSuggestedBrand, deleteSuggestedBrand, contactSuggestedBrand, createSuggestedBrandAdmin, transformSuggestedBrandToVendor } from '../../models/suggestedBrand'


export const resolvers = {
  Query: {
    suggestedBrands: async (root, args, context, info) => {
      return await suggestedBrands(root, args, context, info)
    },
    suggestedBrand: async (root, args, context, info) => {
      return await suggestedBrand(root, args, context, info)
    }
  },
  Mutation: {
    createSuggestedBrand: async (root, args, context, info) => {
      return await createSuggestedBrand(root, args, context, info)
    },
    createSuggestedBrandAdmin: async (root, args, context, info) => {
      return await createSuggestedBrandAdmin(root, args, context, info)
    },
    updateSuggestedBrand: async (root, args, context, info) => {
      return await updateSuggestedBrand(root, args, context, info)
    },
    deleteSuggestedBrand: async (root, args, context, info) => {
      return await deleteSuggestedBrand(root, args, context, info)
    },
    contactSuggestedBrand: async (root, args, context, info) => {
      return await contactSuggestedBrand(root, args, context, info)
    },
    transformSuggestedBrandToVendor: async (root, args, context, info) => {
      return await transformSuggestedBrandToVendor(root, args, context, info)
    },
  }
}
