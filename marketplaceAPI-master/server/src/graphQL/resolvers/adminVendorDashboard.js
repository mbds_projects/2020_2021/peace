import {
  adminProfileVendorDashboard, updateInfoAdminVendorDashboard, updateCarouselPictureAdminVendorDashboard,
  updateFeaturedProductInfoAdminVendorDashboard, updateFeaturedProductPictureAdminVendorDashboard, createTokenVendorPassword
} from '../../models/adminVendorDashboard'


export const resolvers = {
  Query: {
    adminProfileVendorDashboard: async (root, args, context, info) => {
      return await adminProfileVendorDashboard(root, args, context, info)
    },
  },
  Mutation: {
    createTokenVendorPassword: async (root, args, context, info) => {
      return await createTokenVendorPassword(root, args, context, info)
    },
    updateInfoAdminVendorDashboard: async (root, args, context, info) => {
      return await updateInfoAdminVendorDashboard(root, args, context, info)
    },
    updateCarouselPictureAdminVendorDashboard: async (root, args, context, info) => {
      return await updateCarouselPictureAdminVendorDashboard(root, args, context, info)
    },
    updateFeaturedProductInfoAdminVendorDashboard: async (root, args, context, info) => {
      return await updateFeaturedProductInfoAdminVendorDashboard(root, args, context, info)
    },
    updateFeaturedProductPictureAdminVendorDashboard: async (root, args, context, info) => {
      return await updateFeaturedProductPictureAdminVendorDashboard(root, args, context, info)
    },
  }
}
