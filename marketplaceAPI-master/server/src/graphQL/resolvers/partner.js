import { partners } from '../../models/partner'


export const resolvers = {
  Query: {
    partners: async (root, args, context, info) => {
      return await partners(root, args, context, info)
    }
  }
}
