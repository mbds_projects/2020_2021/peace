import { 
  user, createUser, authenticate, facebookAuthenticate, updateUser, 
  userFavorites, updateProfilePictureUser, updatePasswordUser, updateForgottenPasswordUser,
  forgotPassword, userComments, reportUser,  usersReported, blockUser,
  usersBlocked, unblockUser, checkNewsletter, setNewsletter,
  isFacebookAccountRegistered, createUserFacebook, isAccountRegistered
 } from '../../models/user'


export const resolvers = {
  Query: {
    user: async (root, args, context, info) => {
      return await user(root, args, context, info)
    },
    userFavorites: async (root, args, context, info) => {
      return await userFavorites(root, args, context, info)
    },
    userComments: async (root, args, context, info) => {
      return await userComments(root, args, context, info)
    },
    usersReported: async(root, args, context, info) => {
      return await usersReported(root, args, context, info);
    },
    usersBlocked: async(root, args, context, info) => {
      return await usersBlocked(root, args, context, info);
    },
    checkNewsletter: async(root, args, context, info) => {
      return await checkNewsletter(root, args, context, info);
    },
    isFacebookAccountRegistered: async(root, args, context, info) => {
      return await isFacebookAccountRegistered(root, args, context, info);
    },
    isAccountRegistered: async(root, args, context, info) => {
      return await isAccountRegistered(root, args, context, info);
    },
  },
  Mutation: {
    createUserFacebook: async (root, args, context, info) => {
      return await createUserFacebook(root, args, context, info);
    },
    createUser: async (root, args, context, info) => {
      return await createUser(root, args, context, info);
    },
    authenticate: async (root, args, context, info) => {
      return await authenticate(root, args, context, info)
    },
    facebookAuthenticate: async (root, args, context, info) => {
      return await facebookAuthenticate(root, args, context, info)
    },
    updateUser: async (root, args, context, info) => {
      return await updateUser(root, args, context, info)
    },
    updateProfilePictureUser: async(root, args, context, info) => {
      return await updateProfilePictureUser(root, args, context, info);
    },
    updatePasswordUser: async(root, args, context, info) => {
      return await updatePasswordUser(root, args, context, info);
    },
    updateForgottenPasswordUser: async(root, args, context, info) => {
      return await updateForgottenPasswordUser(root, args, context, info);
    },
    forgotPassword: async(root, args, context, info) => {
      return await forgotPassword(root, args, context, info);
    },
    reportUser: async(root, args, context, info) => {
      return await reportUser(root, args, context, info);
    },
    blockUser: async(root, args, context, info) => {
      return await blockUser(root, args, context, info);
    },
    unblockUser: async(root, args, context, info) => {
      return await unblockUser(root, args, context, info);
    },
    setNewsletter: async(root, args, context, info) => {
      return await setNewsletter(root, args, context, info);
    },
  }
}
