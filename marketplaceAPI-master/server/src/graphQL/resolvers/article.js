import { articles, articlesB2B } from '../../models/article'


export const resolvers = {
  Query: {
    articles: async (root, args, context, info) => {
      return await articles(root, args, context, info)
    },
    articlesB2B: async (root, args, context, info) => {
      return await articlesB2B(root, args, context, info)
    }
  }
}
