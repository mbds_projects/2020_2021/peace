import { retrieveAllInvoices, updateProfilePictureVendor, updateLogoVendor, updateDeliverInfoVendor, vendorAccount } from '../../models/vendorAccount'

export const resolvers = {
  Query: {
    retrieveAllInvoices: async (root, args, context, info) => {
      return await retrieveAllInvoices(root, args, context, info)
    },
    vendorAccount: async (root, args, context, info) => {
      return await vendorAccount(root, args, context, info)
    },
  },
  Mutation: {
    updateProfilePictureVendor: async (root, args, context, info) => {
      return await updateProfilePictureVendor(root, args, context, info)
    },
    updateLogoVendor: async (root, args, context, info) => {
      return await updateLogoVendor(root, args, context, info)
    },
    updateDeliverInfoVendor: async (root, args, context, info) => {
      return await updateDeliverInfoVendor(root, args, context, info)
    },
  }
}
