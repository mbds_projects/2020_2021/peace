import { commentsByVendor, reportComment, createComment, commentsReported, deleteComment, bestComments} from '../../models/comment'


export const resolvers = {
  Query: {
    commentsByVendor: async (root, args, context, info) => {
      return await commentsByVendor(root, args, context, info)
    },
    reportComment: async(root, args, context, info) => {
      return await reportComment(root, args, context, info)
    },
    commentsReported: async(root, args, context, info) => {
      return await commentsReported(root, args, context, info)
    },
    bestComments: async(root, args, context, info) => {
      return await bestComments(root, args, context, info)
    }
  },
  Mutation: {
    createComment: async (root, args, context, info) => {
      return await createComment(root, args, context, info)
    },
    deleteComment: async (root, args, context, info) => {
      return await deleteComment(root, args, context, info)
    }
  }
}
