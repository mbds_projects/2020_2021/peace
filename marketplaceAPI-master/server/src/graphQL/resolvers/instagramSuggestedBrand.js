import { instagramSuggestedBrand, instagramSuggestedBrands, detectAndSaveInstagramSuggestedBrands, updateInstagramSuggestedBrand, deleteInstagramSuggestedBrand } from '../../models/instagramSuggestedBrand'


export const resolvers = {
  Query: {
    instagramSuggestedBrands: async (root, args, context, info) => {
      return await instagramSuggestedBrands(root, args, context, info)
    },
    instagramSuggestedBrand: async (root, args, context, info) => {
      return await instagramSuggestedBrand(root, args, context, info)
    }
  },
  Mutation: {
    detectAndSaveInstagramSuggestedBrands: async(root, args, context, info) => {
      return await detectAndSaveInstagramSuggestedBrands(root, args, context, info);
    },
    updateInstagramSuggestedBrand: async(root, args, context, info) => {
      return await updateInstagramSuggestedBrand(root, args, context, info);
    },
    deleteInstagramSuggestedBrand: async(root, args, context, info) => {
      return await deleteInstagramSuggestedBrand(root, args, context, info);
    }
  }
}
