import { createCustomerOpinion, customerOpinions } from '../../models/customerOpinion'


export const resolvers = {
  Query: {
    customerOpinions: async (root, args, context, info) => {
      return await customerOpinions(root, args, context, info)
    }
  },
  Mutation: {
    createCustomerOpinion: async (root, args, context, info) => {
      return await createCustomerOpinion(root, args, context, info)
    }
  }
}
