import { contactUs } from '../../models/contactUs'


export const resolvers = {
  Mutation: {
    contactUs: async (root, args, context, info) => {
      return await contactUs(root, args, context, info)
    }
  }
}
