import { vendorsAssimilate, latestVendorsAdded,
  aroundPositionVendors,
  bestRateVendors, moreFavoritesVendors,
  onlyOnlineVendorsIds,
  vendorsMinimal, vendorLimited, vendorsLimited, aroundPositionVendorsIdsByKms,
  profileVendorLimited, vendorsAssimilateMinimal, vendorLimitedAdmin,
  vendorsMinimalAdmin, publishVendor, profileVendorLimitedAdmin, updateInfoVendor ,
  unpublishVendor, vendorsWithoutAccount, vendorsLimitedAdmin} from '../../models/vendor'
  


export const resolvers = {
  Query: {
    vendorsLimited: async (root, args, context, info) => {
      return await vendorsLimited(root, args, context, info)
    },
    vendorsLimitedAdmin: async (root, args, context, info) => {
      return await vendorsLimitedAdmin(root, args, context, info)
    },
    vendorsMinimalAdmin: async (root, args, context, info) => {
      return await vendorsMinimalAdmin(root, args, context, info)
    },
    vendorsMinimal: async (root, args, context, info) => {
      return await vendorsMinimal(root, args, context, info)
    },
    vendorsWithoutAccount: async (root, args, context, info) => {
      return await vendorsWithoutAccount(root, args, context, info)
    },
    vendorLimited: async (root, args, context, info) => {
      return await vendorLimited(root, args, context, info)
    },
    vendorLimitedAdmin: async (root, args, context, info) => {
      return await vendorLimitedAdmin(root, args, context, info)
    },
    profileVendorLimited: async (root, args, context, info) => {
      return await profileVendorLimited(root, args, context, info)
    },
    profileVendorLimitedAdmin: async (root, args, context, info) => {
      return await profileVendorLimitedAdmin(root, args, context, info)
    },
    vendorsAssimilate: async (root, args, context, info) => {
      return await vendorsAssimilate(root, args, context, info)
    },
    latestVendorsAdded: async (root, args, context, info) => {
      return await latestVendorsAdded(root, args, context, info)
    },
    aroundPositionVendors: async (root, args, context, info) => {
      return await aroundPositionVendors(root, args, context, info)
    },
    aroundPositionVendorsIdsByKms: async (root, args, context, info) => {
      return await aroundPositionVendorsIdsByKms(root, args, context, info)
    },
    onlyOnlineVendorsIds: async (root, args, context, info) => {
      return await onlyOnlineVendorsIds(root, args, context, info)
    },
    bestRateVendors: async (root, args, context, info) => {
      return await bestRateVendors(root, args, context, info)
    },
    moreFavoritesVendors:  async (root, args, context, info) => {
      return await moreFavoritesVendors(root, args, context, info)
    },
    vendorsAssimilateMinimal: async (root, args, context, info) => {
      return await vendorsAssimilateMinimal(root, args, context, info)
    },
  },
  Mutation: {
    publishVendor: async (root, args, context, info) => {
      return await publishVendor(root, args, context, info)
    },
    updateInfoVendor: async (root, args, context, info) => {
      return await updateInfoVendor(root, args, context, info)
    },
    unpublishVendor: async (root, args, context, info) => {
      return await unpublishVendor(root, args, context, info)
    }
  }
}
