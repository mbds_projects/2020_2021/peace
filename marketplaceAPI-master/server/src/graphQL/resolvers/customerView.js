import { createCustomerView, customerView } from '../../models/customerView'


export const resolvers = {
  Query: {
    customerView: async (root, args, context, info) => {
      return await customerView(root, args, context, info)
    }
  },
  Mutation: {
    createCustomerView: async (root, args, context, info) => {
      return await createCustomerView(root, args, context, info)
    }
  }
}
