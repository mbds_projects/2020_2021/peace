import { potentialBrand, potentialBrands, createPotentialBrand, updatePotentialBrand, deletePotentialBrand } from '../../models/potentialBrand'


export const resolvers = {
  Query: {
    potentialBrands: async (root, args, context, info) => {
      return await potentialBrands(root, args, context, info)
    },
    potentialBrand: async (root, args, context, info) => {
      return await potentialBrand(root, args, context, info)
    }
  },
  Mutation: {
    createPotentialBrand: async (root, args, context, info) => {
      return await createPotentialBrand(root, args, context, info)
    },
    updatePotentialBrand: async (root, args, context, info) => {
      return await updatePotentialBrand(root, args, context, info)
    },
    deletePotentialBrand: async (root, args, context, info) => {
      return await deletePotentialBrand(root, args, context, info)
    }
  }
}
