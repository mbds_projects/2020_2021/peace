import {
  authenticateVendor, createVendorPassword,
  vendorLogged, profileVendorDashboard, forgotPasswordVendor, allowedToPublish
} from '../../models/vendorDashboard'


export const resolvers = {
  Query: {
    vendorLogged: async (root, args, context, info) => {
      return await vendorLogged(root, args, context, info)
    },
    profileVendorDashboard: async (root, args, context, info) => {
      return await profileVendorDashboard(root, args, context, info)
    },
    allowedToPublish: async (root, args, context, info) => {
      return await allowedToPublish(root, args, context, info)
    },
  },
  Mutation: {
    authenticateVendor: async (root, args, context, info) => {
      return await authenticateVendor(root, args, context, info)
    },
    createVendorPassword: async (root, args, context, info) => {
      return await createVendorPassword(root, args, context, info)
    },
    forgotPasswordVendor: async (root, args, context, info) => {
      return await forgotPasswordVendor(root, args, context, info)
    }
  }
}
