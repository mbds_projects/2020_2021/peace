import { professionalViews } from '../../models/professionalView'


export const resolvers = {
  Query: {
    professionalViews: async (root, args, context, info) => {
      return await professionalViews(root, args, context, info)
    }
  },
  Mutation: {
  }
}
