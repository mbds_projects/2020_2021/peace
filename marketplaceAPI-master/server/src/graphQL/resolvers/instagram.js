import { instagramRecent, instagramPopular } from '../../models/instagram'


export const resolvers = {
  Query: {
    instagramRecent: async (root, args, context, info) => {
      return await instagramRecent(root, args, context, info)
    },
    instagramPopular: async (root, args, context, info) => {
      return await instagramPopular(root, args, context, info)
    }
  }
}
