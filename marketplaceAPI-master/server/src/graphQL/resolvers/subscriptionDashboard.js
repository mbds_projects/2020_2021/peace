import { createSubscriptionFromVendor, retryInvoice, 
  retrieveDashboardSubscription, cancelSubscription, createSubscriptionDetailOfVendor, 
  updateSubscription, calculateProration} from '../../models/subscriptionDashboard'


export const resolvers = {
  Query: {
    retrieveDashboardSubscription:  async (root, args, context, info) => {
      return await retrieveDashboardSubscription(root, args, context, info)
    },
  },
  Mutation: {
    createSubscriptionFromVendor: async (root, args, context, info) => {
      return await createSubscriptionFromVendor(root, args, context, info)
    },
    retryInvoice: async (root, args, context, info) => {
      return await retryInvoice(root, args, context, info)
    },
    cancelSubscription:  async (root, args, context, info) => {
      return await cancelSubscription(root, args, context, info)
    },
    createSubscriptionDetailOfVendor:  async (root, args, context, info) => {
      return await createSubscriptionDetailOfVendor(root, args, context, info)
    },
    updateSubscription:  async (root, args, context, info) => {
      return await updateSubscription(root, args, context, info)
    },
    calculateProration:  async (root, args, context, info) => {
      return await calculateProration(root, args, context, info)
    },
  }
}
