import { categories, categoriesUsed, updateCategoryDesc, updatePictureCategory } from '../../models/category'


export const resolvers = {
  Query: {
    categories: async (root, args, context, info) => {
      return await categories(root, args, context, info)
    },
    categoriesUsed: async (root, args, context, info) => {
      return await categoriesUsed(root, args, context, info)
    }
  },
  Mutation: {
    updateCategoryDesc: async (root, args, context, info) => {
      return await updateCategoryDesc(root, args, context, info)
    },
    updatePictureCategory: async (root, args, context, info) => {
      return await updatePictureCategory(root, args, context, info)
    },
  }
}
