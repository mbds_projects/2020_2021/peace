import { config } from '../../models/config'


export const resolvers = {
  Query: {
    config: async (root, args, context, info) => {
      return await config(root, args, context, info)
    }
  }
}
