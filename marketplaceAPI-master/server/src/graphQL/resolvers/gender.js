import { genders, gendersUsed, updateGenderDesc, updatePictureGender } from '../../models/gender'


export const resolvers = {
  Query: {
    genders: async (root, args, context, info) => {
      return await genders(root, args, context, info)
    },
    gendersUsed: async (root, args, context, info) => {
      return await gendersUsed(root, args, context, info)
    }
  },
  Mutation: {
    updateGenderDesc: async (root, args, context, info) => {
      return await updateGenderDesc(root, args, context, info)
    },
    updatePictureGender: async (root, args, context, info) => {
      return await updatePictureGender(root, args, context, info)
    },
  }
}
