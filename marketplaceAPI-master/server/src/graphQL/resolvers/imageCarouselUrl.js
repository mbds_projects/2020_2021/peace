import { imagesCarouselUrl } from '../../models/imageCarouselUrl'


export const resolvers = {
  Query: {
    imagesCarouselUrl: async (root, args, context, info) => {
      return await imagesCarouselUrl(root, args, context, info)
    }
  }
}
