import { interviews } from '../../models/interview'


export const resolvers = {
  Query: {
    interviews: async (root, args, context, info) => {
      return await interviews(root, args, context, info)
    }
  }
}
