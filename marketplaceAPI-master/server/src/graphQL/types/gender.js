export const typeDef = `
  extend type Query {
    genders: [Gender],
    gendersUsed: [Gender]
  }


  extend type Mutation {
    updatePictureGender(file: Upload, id: ID): Boolean
    updateGenderDesc(descFr: String, descEn: String, id:ID): Boolean
  }

  type Gender {
    id: ID,
    used: Boolean,
    name: String,
    lang: [Lang],
    pictureUrl: String
    createdAt: Date
    updatedAt: Date
  }


`;
