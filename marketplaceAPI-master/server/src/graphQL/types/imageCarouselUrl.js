export const typeDef = `
  extend type Query {
    imagesCarouselUrl: [ImageCarouselUrl],
  }

  type ImageCarouselUrl {
    id: ID,
    url: String,
    mode: String,
    pos: Int,
    lang: String,
    href: String,
    to: String
  }
`;
