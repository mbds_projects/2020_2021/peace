export const typeDef = `
  extend type Mutation {
    adminVendorInfoUpdate(input: _InfoVendorAdmin, vendorId: ID): Boolean,
    adminVendorProfilePictureUpdate(file: Upload, vendorId: ID): Boolean,
    adminVendorLogoUpdate(file: Upload, vendorId: ID): Boolean
  }
`;
