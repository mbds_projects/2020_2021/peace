export const typeDef = `
  extend type Query {
    partners: [Partner],
  }

  type Partner {
    link: String,
    imgUrl: String
  }


`;
