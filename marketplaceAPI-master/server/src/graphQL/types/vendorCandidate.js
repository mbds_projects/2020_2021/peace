export const typeDef = `
  extend type Query {
    vendorsCandidate: [VendorCandidate]
    vendorCandidate(id: ID): VendorCandidate
  }


  extend type Mutation {
    createVendorCandidate(input: _VendorCandidate): Boolean,
    followNewsletterDashboard(input: _VendorCandidateLimited): Boolean,
    transformVendorCandidateToVendor(id: ID): Boolean,
    vendorsendinfo(mail:String): Boolean,
    vendorCandidateSaveInfo(input: _InfoVendorCandidate, vendorId:ID): String
  }

  type VendorCandidate {
    id: ID
    name: String!
    contactName: String
    mail: String!
    phoneNumber: String
    site: String
    categories: [Category]
    keywords: [Keyword]
    subscriptionType: String
    createdAt: Date
    updatedAt: Date
    vendor: ID
    labels: [Label]
    genders: [Gender]
  }

 

  input _VendorCandidate {
    name: String!
    contactName: String
    mail: String!
    phoneNumber: String
    site: String
    subscriptionType: String
  }

  input _VendorCandidateLimited {
    name: String!
    mail: String!
  }
`;
