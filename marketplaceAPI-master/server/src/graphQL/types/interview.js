export const typeDef = `
  extend type Query {
    interviews: [Interview],
  }

  type Interview {
    author: String,
    title: String,
    subTitle: String,
    link: String,
    imgUrl: String,
    date: Date
    type: String
  }


`;
