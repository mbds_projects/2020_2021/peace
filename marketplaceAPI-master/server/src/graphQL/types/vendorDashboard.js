export const typeDef = `
  extend type Query {
    vendorLogged: VendorLogged
    profileVendorDashboard: ProfileVendorLimited
    allowedToPublish: Boolean
  }

  extend type Mutation {
    authenticateVendor(mail: String!, password: String!): JWT
    createVendorPassword(input: _VendorPassword!): JWT
    forgotPasswordVendor(mail: String!): Boolean
  }

  type VendorLogged {
    id: ID
    name: String!
    role: String
    published: Boolean
    logoUrl: String
    mail: String
    subscriptionDashboardId: String
    customerId: String,
    profilePictureUrl: String
    subscriptionType: String
    createdAt: Date
  }


`;
