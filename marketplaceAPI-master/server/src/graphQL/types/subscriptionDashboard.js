export const typeDef = `
  extend type Query {
    retrieveDashboardSubscription: Subscription
  }


  type Subscription {
    id: String
    status: String
    current_period_end: Int
    current_period_start: Int
    created: Int
    unit_amount: Int
    recurring: String
    currency: String
    customer: String
    price: String
    latest_invoice: LatestInvoice
    pending_setup_intent: PaymentIntent
    payment_method: PaymentMethod
    product: Product
  }

  extend type Mutation {
    createSubscriptionFromVendor: SubscriptionCreated,
    retryInvoice(paymentMethodId: String, invoiceId: String): LatestInvoice
    cancelSubscription: Boolean,
    createSubscriptionDetailOfVendor(paymentMethodId: String, from: Int, tvaIntra: String, subscriptionType: String): Boolean,
    updateSubscription(subscriptionType: String, proration: Boolean): Subscription
    calculateProration(subscriptionType: String, proration: Boolean): Proration
  }

  type Proration {
    finalPrice: Float
    price: Float,
    finalTaxes: Float
  }

  input _StripeEvent {
    type: String
  }

  type Product {
    name: String
  }

  type SubscriptionCreated {
    id: String
    status: String
    current_period_end: Int
    latest_invoice: LatestInvoice
  }

  type PaymentMethod {
    last4: String
    brand: String
  }
`;
