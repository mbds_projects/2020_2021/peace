export const typeDef = `
  extend type Query {
    influencers: [Influencer],
  }

  type Influencer {
    link: String,
    imgUrl: String,
    title: String
  }


`;
