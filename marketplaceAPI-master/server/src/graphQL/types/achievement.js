export const typeDef = `
  extend type Query {
    achievements: [Achievement],
  }

  type Achievement {
    id: ID,
    type: String,
    subtype: String,
    value: Int,
    icon: String,
    level: Int,
    activated: Boolean,
    link: String
  }
`;
