export const typeDef = `
  extend type Query {
    adminProfileVendorDashboard(id: ID!): ProfileVendorLimitedAdmin
  }

  extend type Mutation {
    updateInfoAdminVendorDashboard(input: _InfoVendorAdmin, vendorId: ID): Boolean
    updateCarouselPictureAdminVendorDashboard(file: Upload, indexOfImage: Int, vendorId: ID): Boolean
    updateFeaturedProductPictureAdminVendorDashboard(file: Upload, indexOfImage: Int, price: Float, link: String, vendorId: ID): Boolean
    updateFeaturedProductInfoAdminVendorDashboard(indexOfProduct: Int, price: Float, link: String, vendorId: ID): Boolean  
    createTokenVendorPassword(id: ID!, toAdmin: Boolean): Boolean
  }
`;
