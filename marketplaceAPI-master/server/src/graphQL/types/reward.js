export const typeDef = `
  extend type Query {
    rewards: [Reward]
    rewardsOfVendor: [Reward]
    rewardsAdminVendor(vendorId: ID): [Reward]
  }

  extend type Mutation {
    createReward(input: _Reward) : Boolean,
    createRewardAdmin(input: _Reward, vendorId: ID) : Boolean,
    disableReward(rewardId: ID): Boolean,
    enableReward(rewardId: ID): Boolean
  }

  input _Reward {
    type: String,
    message: String,
    value: String,
    code: String,
    validity: Date
  }

  type Reward {
    id: ID,
    vendor: VendorUltraLimited,
    type: String,
    message: String,
    value: String,
    code: String,
    validity: Date,
    activated: Boolean
  }

  type VendorUltraLimited {
    id: ID
    name: String!
    profilePictureUrl: String
    categories: [ID]
    genders: [ID]
    keywords: [ID]
    onlineShop: Boolean
    published: Boolean
    clickAndCollect: Boolean
  }
`;
