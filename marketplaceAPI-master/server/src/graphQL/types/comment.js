export const typeDef = `
  extend type Query {
    commentsByVendor(vendorId: ID!, page: Int!): [Comment_Extended],
    reportComment(commentId: ID!): Comment_Extended,
    commentsReported: [Comment_Extended],
    bestComments: [Comment_Extended]
  }

  extend type Mutation {
    createComment(input: _Comment!): Comment_Extended,
    deleteComment(commentId: ID!): Boolean
  }
  
  type Comment_Extended {
    id: ID,
    text: String,
    postedBy: PublicUser,
    date: Date,
    rate: Int,
    vendor: VendorLimited,
    reportedBy: [ID]
  }

  type Stat {
    avg: Float,
    count: Int
  }

  input _Comment {
    vendorId: ID,
    rate: Int,
    text: String
  }

`;
