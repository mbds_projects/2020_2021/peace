export const typeDef = `
  extend type Query {
    articles: [Article],
    articlesB2B: [Article]
  }

  type Article {
    link: String,
    title: String,
    media: String
  }


`;
