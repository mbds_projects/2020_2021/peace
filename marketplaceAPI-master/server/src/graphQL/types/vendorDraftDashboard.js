export const typeDef = `
  extend type Query {
    vendorDraftDashboard: VendorDraftDashboard
  }

  extend type Mutation {
    updateInfoVendorDraftDashboard(input: _InfoVendor): Boolean
    updateCarouselPictureVendorDraftDashboard(file: Upload, indexOfImage: Int): Boolean
    updateFeaturedProductPictureVendorDraftDashboard(file: Upload, indexOfImage: Int, price: Float, link: String): Boolean
    updateFeaturedProductInfoVendorDraftDashboard(indexOfProduct: Int, price: Float, link: String): Boolean  
    publishDraftDashboard: Boolean
  }

  type VendorDraftDashboard {
    id: ID
    name: String!
    logoUrl: String
    locations: [Location]
    desc: Desc
    site: String
    videoYoutubeId: String
    videoDailymotionId: String
    videoVimeoId: String
    categories: [ID]
    genders: [ID]
    keywords: [ID]
    labels: [Label]
    socialNetworks: [SocialNetwork]
    alsoAvailableOn: [ID]
    rewards: [Reward]
    onlineShop: Boolean
    clickAndCollect: Boolean
    carouselPicturesUrl: [String]
    featuredProductsUrl: [FeaturedProductUrl]
  }



`;
