export const typeDef = `
  extend type Query {
    categories: [Category],
    categoriesUsed: [Category],
  }

  extend type Mutation {
    updatePictureCategory(file: Upload, id: ID): Boolean
    updateCategoryDesc(descFr: String, descEn: String, id:ID): Boolean
  }


  type Category {
    id: ID,
    used: Boolean,
    name: String,
    lang: [Lang],
    pictureUrl: String,
    createdAt: Date
    updatedAt: Date
  }
`;
