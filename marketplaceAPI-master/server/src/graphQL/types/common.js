export const typeDef = `
  type Lang {
    locale: String
    value: String
  }

  input _Lang {
    locale: String
    value: String
  }

  type Image {
    id: ID,
    type: String,
    data: String
  }

  type Coordinates {
    coordinates: [Float]
  }

  input _Coordinates {
    coordinates: [Float]
  }


  input _VendorPassword {
    password: String
    token: String
  }

  input _InfoVendor {
    desc: _Desc
    site: String
    videoYoutubeId: String
    videoDailymotionId: String
    videoVimeoId: String
    locations: [ID]
    socialNetworks: [_SocialNetwork]
    onlineShop: Boolean
    clickAndCollect: Boolean
  }

  input _InfoVendorAdmin {
    desc: _Desc
    site: String
    videoYoutubeId: String
    videoDailymotionId: String
    videoVimeoId: String
    categories: [ID]
    keywords: [ID]
    genders: [ID]
    labels: [ID]
    locations: [ID]
    socialNetworks: [_SocialNetwork]
    onlineShop: Boolean
    alsoAvailableOn: [ID]
    clickAndCollect: Boolean
    published: Boolean
    activated: Boolean
  }

  input _InfoVendorCandidate {
    categories: [ID]
    keywords: [ID]
    genders: [ID]
    labels: [ID]
    labelscheck: [String]
  }

  input _Position {
    latitude: Float
    longitude: Float
  }


  input _SocialNetwork {
    type: String
    url: String
  }

  input _Desc {
    lang: [_Lang]
  }

  type SocialNetwork {
    type: String
    url: String
  }

  type Desc {
    lang: [Lang]
  }

  type ProfileVendorLimited {
    id: ID
    name: String!
    logoUrl: String
    locations: [Location]
    desc: Desc
    site: String
    videoYoutubeId: String
    videoDailymotionId: String
    videoVimeoId: String
    categories: [ID]
    genders: [ID]
    keywords: [ID]
    comments: [ID]
    rate: Float
    labels: [Label]
    socialNetworks: [SocialNetwork]
    alsoAvailableOn: [ID]
    rewards: [Reward]
    onlineShop: Boolean
    clickAndCollect: Boolean
    carouselPicturesUrl: [String]
    featuredProductsUrl: [FeaturedProductUrl]
    published: Boolean
    profilePictureUrl: String
  }

  type ProfileVendorLimitedAdmin {
    id: ID
    name: String!
    logoUrl: String
    locations: [Location]
    desc: Desc
    site: String
    videoYoutubeId: String
    videoDailymotionId: String
    videoVimeoId: String
    categories: [Category]
    genders: [Gender]
    keywords: [Keyword]
    comments: [ID]
    rate: Float
    labels: [Label]
    socialNetworks: [SocialNetwork]
    alsoAvailableOn: [ID]
    rewards: [Reward]
    onlineShop: Boolean
    clickAndCollect: Boolean
    carouselPicturesUrl: [String]
    featuredProductsUrl: [FeaturedProductUrl]
    published: Boolean
  }


  type VendorLimited {
    id: ID
    name: String!
    profilePictureUrl: String
    logoUrl: String
    comments: [ID]
    rate: Float
    categories: [ID]
    genders: [ID]
    keywords: [ID]
    labels: [ID]
    onlineShop: Boolean
    published: Boolean
    activated: Boolean
    clickAndCollect: Boolean
  }

  type FeaturedProductUrl {
    imgUrl: String
    price: Float
    link: String
  }

  type PaymentIntent {
    id: String
    status: String
    client_secret: String
    payment_method: String
  }

  type LatestInvoice {
    id: String
    status: String
    payment_intent: PaymentIntent
  }

  type InvoicePeriod {
    start: Date
    end: Date
  }

  type Invoice {
    id: String
    amount_paid: Float
    amount_due: Float
    currency: String
    period_start: Date
    period_end: Date
    invoice_pdf: String
  }


`;
