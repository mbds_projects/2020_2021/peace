export const typeDef = `
  extend type Query {
    youtubeRecent: [Youtube]
  }

  type Youtube {
    link: String,
    media: String,
    title: String
  }
`;
