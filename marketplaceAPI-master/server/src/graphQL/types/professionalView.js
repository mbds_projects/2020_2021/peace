export const typeDef = `
  extend type Query {
    professionalViews(minRate: Int!): [ProfessionalView]
  }


  type VendorLimitedComment {
    id: ID
    name: String!
    profilePictureUrl: String
    comments: [Comment_Extended]
  }

  type ProfessionalView {
    id: ID,
    text: String,
    date: Date,
    rate: Int,
    vendor: VendorLimitedComment
  }
`;
