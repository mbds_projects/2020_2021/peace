export const typeDef = `
  extend type Query {
    instagramRecent: [Insta],
    instagramPopular: [Insta],
  }

  type Insta {
    link: String,
    media: String
  }
`;
