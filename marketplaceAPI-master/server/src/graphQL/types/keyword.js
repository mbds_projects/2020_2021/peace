export const typeDef = `
  extend type Query {
    keywords: [Keyword],
    keywordsPopular: [Keyword],
    keywordsUsed: [Keyword]
  }

  extend type Mutation {
    updatePictureKeyword(file: Upload, id: ID): Boolean
    updateKeywordDesc(descFr: String, descEn: String, id:ID): Boolean
  }

  type Keyword {
    id: ID,
    used: Boolean,
    name: String,
    lang: [Lang],
    moral: Moral,
    pictureUrl: String,
    createdAt: Date
    updatedAt: Date
  }
`;
