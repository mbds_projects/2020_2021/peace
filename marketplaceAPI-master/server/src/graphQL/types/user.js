export const typeDef = `
  extend type Query {
    user: User
    userFavorites: [VendorLimited]
    userComments: [Comment_Extended]
    usersReported(minReport: Int): [User]
    usersBlocked: [User]
    checkNewsletter(mail: String!): Boolean
    isFacebookAccountRegistered(userID: String!): Boolean
    isAccountRegistered(mail: String!): UserType
  }

  extend type Mutation {
    createUser(input: _UserCreation!): JWT
    createUserFacebook(input: _UserCreationFacebook!): JWT
    updateUser(input: _User!): User
    authenticate(mail: String!, password: String!): JWT
    facebookAuthenticate(userID: String!, accessToken: String!): JWT
    updateProfilePictureUser(file: Upload): String
    updatePasswordUser(input: _UserPassword!): Boolean
    forgotPassword(mail: String!): Boolean
    updateForgottenPasswordUser(input: _UserForgotPassword!): JWT
    reportUser(userId: ID!): Boolean
    blockUser(userId: ID!): Boolean
    unblockUser(userId: ID!): Boolean
    setNewsletter(mail: String!): Boolean
  }

  type JWT {
    token: String
  }

  type UserType {
    isUser: Boolean,
    isFacebookUser: Boolean,
    isVendor: Boolean
  }

  type User {
    id: ID
    name: String!
    surname: String!
    mail: String!
    validated: Boolean
    favorites: [String]
    profilePictureUrl: String
    defaultColor: String
    role: String
    reported: Int
    blockedSince: Date
    birthday: Date
    newsletter: Boolean,
    facebookId: String
    keywords: [Keyword]
    rewards: [ID]
    achievements: [Achievement]
  }
  
  type PublicUser {
    id: ID
    name: String
    surname: String
    profilePictureUrl: String
    defaultColor: String,
    facebookId: String
  }

  input _User {
    name: String
    surname: String
    favorites: [String]
    birthday: Date
    newsletter: Boolean
    keywords: [ID]
    rewards: [ID]
  }

  input _UserPassword {
    password: String
    newPassword: String
  }

  input _UserForgotPassword {
    password: String
    token: String
  }

  input _UserCreation {
    mail: String!
    password: String!
    name: String!
    surname: String!
    birthday: Date
    newsletter: Boolean
  }

  input _UserCreationFacebook {
    mail: String!
    name: String!
    surname: String!
    birthday: Date
    newsletter: Boolean
    facebookID: String!,
    facebookAccessToken: String!
  }

  input _FacebookConnection {
    mail: String
    name: String
    surname: String
    favorites: [String]
    birthday: Date
    newsletter: Boolean
    facebookID: String!
    facebookAccessToken: String
  }
  
`;
