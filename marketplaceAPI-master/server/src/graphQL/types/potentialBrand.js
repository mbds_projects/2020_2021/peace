export const typeDef = `
  extend type Query {
    potentialBrands: [PotentialBrand]
    potentialBrand(id: ID): PotentialBrand
  }

  extend type Mutation {
    createPotentialBrand(input: _PotentialBrand): PotentialBrand,
    updatePotentialBrand(input: _PotentialBrandUpdate): Boolean,
    deletePotentialBrand(potentialBrandId: ID!): Boolean
  }

  type PotentialBrand {
    id: ID
    mail: String
    name: String!
    categories: [ID]
    keywords: [ID]
    note: String
    site: String
    cities: String
    createdAt: Date
    updatedAt: Date
    state: String
  }

  input _PotentialBrand {
    name: String!
    mail: String
    cities: String
    site: String
    categories: [ID]
    keywords: [ID]
    note: String
  }

  input _PotentialBrandUpdate {
    id: ID
    name: String
    mail: String
    site: String
    categories: [ID]
    keywords: [ID]
    note: String
    state: String
  }  
`;
