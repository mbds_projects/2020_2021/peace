export const typeDef = `

  extend type Query {
    customerOpinions: [CustomerOpinion]
  }

  extend type Mutation {
    createCustomerOpinion(input: _CustomerOpinion!): CustomerOpinion
  }

  type CustomerOpinion {
    id: ID,
    text: String,
    date: Date,
    rate: Int,
    mail: String,
    user: ID
  }

  input _CustomerOpinion {
    text: String,
    rate: Int,
    mail: String
  }

`;
