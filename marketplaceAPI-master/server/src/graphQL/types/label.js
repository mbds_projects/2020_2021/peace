export const typeDef = `
  extend type Query {
    labels: [Label]
  }

  extend type Mutation {
    updateLogoLabel(file: Upload, id: ID): Boolean
    updateLabelName(name: String, id:ID): Boolean
    createLabel(name: String, file: Upload): Boolean
  }

  type Label {
    id: ID,
    name: String,
    pictureUrl: String,
    createdAt: Date
    updatedAt: Date
  }
`;
