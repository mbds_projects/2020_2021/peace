export const typeDef = `

  extend type Query {
    customerView: Boolean
  }

  extend type Mutation {
    createCustomerView(input: _CustomerView!): CustomerView
  }

  type CustomerView {
    id: ID,
    text: String,
    postedBy: PublicUser,
    date: Date,
    rate: Int
  }

  input _CustomerView {
    rate: Int,
    text: String
  }

`;
