export const typeDef = `
  extend type Mutation {
    contactUs(input: _ContactUs): Boolean
  }

  input _ContactUs {
    name: String!
    surname: String
    mail: String!
    message: String!
    newsletter: Boolean!
    selectedQuestion: Int!
  }
`;
