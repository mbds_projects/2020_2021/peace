export const typeDef = `
  extend type Query {
    vendorsMinimal: [VendorMinimal]
    vendorsMinimalAdmin: [VendorMinimal]
    vendorsWithoutAccount: [VendorMinimal]
    vendorsLimited: [VendorLimited]
    vendorsLimitedAdmin: [VendorLimited]
    vendorLimited(id: ID): VendorLimited
    vendorLimitedAdmin(id: ID): VendorLimited
    profileVendorLimited(id: ID): ProfileVendorLimited
    profileVendorLimitedAdmin(id: ID): ProfileVendorLimitedAdmin
    vendorsAssimilate(categories: [ID], keywords: [ID], vendorId: ID, limit: Int): [VendorLimited]
    latestVendorsAdded: [VendorLimited]
    aroundPositionVendors(position: _Position, maxDistance: Int, limit: Int): [VendorLimited]
    aroundPositionVendorsIdsByKms(position: _Position): [[ID]],
    onlyOnlineVendorsIds(limit: Int): [ID],
    bestRateVendors: [VendorLimited]
    moreFavoritesVendors: [VendorLimited]
    vendorsAssimilateMinimal(categories: [ID], keywords: [ID], labels: [ID]): [VendorMinimal]
  }


  extend type Mutation {
    publishVendor(vendorId: ID): Boolean,
    unpublishVendor(vendorId: ID): Boolean,
    updateInfoVendor(vendorId: ID, input: _InfoVendor): Boolean,
  }

  type VendorMinimal {
    id: ID
    name: String!
    published: Boolean
  }

`;
