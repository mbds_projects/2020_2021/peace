export const typeDef = `
  extend type Query {
    vendorTestimonies: [VendorTestimony]
  }

  type testimonyLink {
    lang: [Lang]
  }

  type VendorTestimony {
    vendor: ID,
    testimonyLink: testimonyLink
  }
`;
