export const typeDef = `
  extend type Query {
    instagramSuggestedBrands: [InstagramSuggestedBrand]
    instagramSuggestedBrand(id: ID): InstagramSuggestedBrand
  }


  extend type Mutation {
    detectAndSaveInstagramSuggestedBrands(input: _InstagramSuggestedBrand): [InstagramSuggestedBrand],
    updateInstagramSuggestedBrand(input: _InstagramSuggestedBrandUpdate): Boolean,
    deleteInstagramSuggestedBrand(instagramSuggestedBrandId: ID!): Boolean,
  }

  input _InstagramSuggestedBrand {
    url: String
    onlyNew: Boolean
  }

  input _InstagramSuggestedBrandUpdate {
    id: ID
    name: String
    checked: Boolean
    occurrence: Int
  }

  type InstagramSuggestedBrand {
    id: ID
    name: String!
    occurrence: Int
    latestPostDate: Date
    checked: Boolean
    addedHimself: Boolean
    createdAt: Date
    updatedAt: Date
  }
  
`;
