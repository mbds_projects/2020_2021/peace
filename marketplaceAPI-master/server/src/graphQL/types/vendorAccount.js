export const typeDef = `
    extend type Query {
        vendorAccount: VendorLimited
        retrieveAllInvoices: [Invoice]
    }

    extend type Mutation {
        updateProfilePictureVendor(file: Upload): Boolean,
        updateLogoVendor(file: Upload): Boolean,
        updateDeliverInfoVendor(input: _DeliverInfoVendor): Boolean
    }

    input _DeliverInfoVendor {
        onlineShop: Boolean,
        clickAndCollect: Boolean
    }
`;
