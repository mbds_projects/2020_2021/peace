export const typeDef = `
  extend type Query {
    suggestedBrands: [SuggestedBrand]
    suggestedBrand(id: ID): SuggestedBrand
  }

  extend type Mutation {
    createSuggestedBrand(input: _SuggestedBrand): SuggestedBrand,
    createSuggestedBrandAdmin(input: _SuggestedBrandCreate): SuggestedBrand,
    updateSuggestedBrand(input: _SuggestedBrandUpdate): Boolean,
    deleteSuggestedBrand(suggestedBrandId: ID!): Boolean
    contactSuggestedBrand(mail: String, name: String, id: ID): SuggestedBrand
    transformSuggestedBrandToVendor(id: ID): Boolean
  }

  type SuggestedBrand {
    id: ID
    mail: String
    name: String!
    categories: [ID]
    keywords: [ID]
    suggestedBy: [suggestedBy]
    occurrence: Int
    site: String
    cities: String
    instagramId: String
    contact: Int
    createdAt: Date
    updatedAt: Date
    state: String
    vendor: ID
  }

  input _SuggestedBrand {
    name: String
    mail: String
    cities: String
    site: String
    categories: [ID]
    keywords: [ID]
    message: String
  }

  input _SuggestedBrandCreate {
    name: String
    mail: String
    site: String
    occurrence: Int
    instagramId: String
    categories: [ID]
    keywords: [ID]
  }

  input _SuggestedBrandUpdate {
    id: ID
    name: String
    mail: String
    site: String
    categories: [ID]
    keywords: [ID]
    occurrence: Int
    instagramId: String
    state: String
  }
  
  type suggestedBy {
    user: userInfo
    message: String
  }

  type userInfo {
    name: String
    surname: String
  }
  
`;
