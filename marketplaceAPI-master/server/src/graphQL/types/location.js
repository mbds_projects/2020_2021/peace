export const typeDef = `
  extend type Query {
    locationsByCity: [LocationFilerCity],
    locationsByCounty: [LocationFilerCounty],
    locations: [Location],
    location(id: ID): Location,
    locationsOfVendorByCity(vendorId: ID, position: _Position): [LocationByCity],
    locationsOfVendorByCounty(vendorId: ID): [LocationByCounty]
    allowedToCreateLocation: Boolean
  }

  extend type Mutation {
    updateLocation(input: _LocationAdmin, id: ID): Boolean,
    createLocation(input: _LocationCreationAdmin): Location,
    searchAndCreateLocation(input: _LocationSearchAdmin): LocationSearchAdmin,
    searchLocationDashboard(address: String): [LocationSearch],
    reverseLocation: Boolean
    updateLocationDashboard(input: _LocationDashboard, locId: ID): Boolean,
    createLocationDashboard(input: _LocationDashboardCreation): Location,
    removeLocationFromVendorDashboard(locId: ID): Boolean
    updateLocationAdminVendorDashboard(input: _LocationDashboard, locId: ID): Boolean,
    createLocationAdminVendorDashboard(input: _LocationDashboardCreation, vendorId: ID): Location,
    removeLocationAdminVendorDashboard(locId: ID, vendorId: ID): Boolean
  }
 
  type LocationSearch {
    address: String
    city: String
    postalCode: String
    shopName: String
    location: Coordinates
    openingHours: String
    phoneNumber: String
    webSite: String
    county: String
    state: String
  }

  type LocationSearchAdmin {
    success: Int,
    error: Int,
    na: Int
  }

  input _LocationDashboard {
    address: String
    city: String
    shopName: String
    postalCode: String
    location: _Coor
    openingHours: String
    phoneNumber: String
    webSite: String
    mail: String
    county: String
    state: String
  }

  input _LocationAdmin {
    address: String
    city: String
    shopName: String
    postalCode: String
    location: _Coor
    openingHours: String
    phoneNumber: String
    webSite: String
    mail: String
    county: String
    state: String
    vendors: [ID]
  }

  input _LocationSearchAdmin {
    addresses: [_Addresses],
    vendorId: ID
  }

  input _Addresses {
    name: String,
    description: String
  }

  input _Coor {
    coordinates: [Float]
    type: String
  }

  input _LocationDashboardCreation {
    address: String
    city: String
    shopName: String
    postalCode: String
    location: _Coor
    openingHours: String
    phoneNumber: String
    webSite: String
    mail: String
    county: String
    state: String
  }

  input _LocationCreationAdmin {
    address: String
    city: String
    shopName: String
    postalCode: String
    location: _Coor
    openingHours: String
    phoneNumber: String
    webSite: String
    mail: String
    county: String
    state: String
    vendors: [ID]
  }

  type Location {
    id: ID
    address: String
    city: String
    shopName: String
    postalCode: String
    location: Coordinates
    openingHours: String
    phoneNumber: String
    webSite: String
    mail: String
    county: String
    state: String
    vendors: [ID]
  }

  type LocationFilerCity {
    city: String
    vendors: [ID]
  }


  type LocationFilerCounty {
    county: String
    vendors: [ID]
  }


  type LocationByCity {
    city: String
    shops: [Location]
  }


  type LocationByCounty {
    county: String
    shops: [Location]
  }
`;
