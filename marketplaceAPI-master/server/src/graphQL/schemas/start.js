import { ApolloServer } from 'apollo-server-koa'
import { typeDefs as typeDefsPublic, resolvers as resolversPublic } from '../schemas/public'


/*
 * this const are called in ./api to start with appp
 */


//PUBLIC routes
export function startAplPublic(app) {
  const aplPublic = new ApolloServer({
    typeDefs: typeDefsPublic,
    resolvers: resolversPublic,
    playground: {
      settings: {
        'editor.cursorShape': 'line'
      }
    },
    context: ({ ctx }) => ctx
  });
  aplPublic.applyMiddleware({
    app,
    path: '/api/public/graphql'
  });
  //TODO: workaround for url's ended without /
  aplPublic.applyMiddleware({
    app,
    path: '/api/public/graphql/'
  });
}
