import { merge } from 'lodash';
import { gql } from 'apollo-server-koa'

import { typeDef as User } from '../types/user'
import { typeDef as Vendor } from '../types/vendor'
import { typeDef as Comment } from '../types/comment'
import { typeDef as Category } from '../types/category'
import { typeDef as Keyword } from '../types/keyword'
import { typeDef as Location } from '../types/location'
import { typeDef as SuggestedBrand } from '../types/suggestedBrand'
import { typeDef as PotentialBrand } from '../types/potentialBrand'
import { typeDef as InstagramSuggestedBrand } from '../types/instagramSuggestedBrand'
import { typeDef as ContactUs } from '../types/contactUs'
import { typeDef as VendorCandidate } from '../types/vendorCandidate'
import { typeDef as ProfessionalView } from '../types/professionalView'
import { typeDef as CustomerView } from '../types/customerView'
import { typeDef as Article } from '../types/article'
import { typeDef as Instagram } from '../types/instagram'
import { typeDef as Interview} from '../types/interview'
import { typeDef as Partner } from '../types/partner'
import { typeDef as Influencer } from '../types/influencer'
import { typeDef as Config } from '../types/config'
import { typeDef as Common } from '../types/common'
import { typeDef as Gender } from '../types/gender'
import { typeDef as Moral } from '../types/moral'
import { typeDef as Label } from '../types/label'
import { typeDef as Youtube } from '../types/youtube'
import { typeDef as ImageCarouselUrl } from '../types/imageCarouselUrl'
import { typeDef as Achievement } from '../types/achievement'
import { typeDef as Reward } from '../types/reward'
import { typeDef as VendorDashboard } from '../types/vendorDashboard'
import { typeDef as VendorAccount} from '../types/vendorAccount'
import { typeDef as SubscriptionDashboard} from '../types/subscriptionDashboard'
import { typeDef as VendorDraftDashboard} from '../types/vendorDraftDashboard'
import { typeDef as VendorTestimony} from '../types/vendorTestimony'
import { typeDef as CustomerOpinion} from '../types/customerOpinion'
import { typeDef as AdminVendorDashboard} from '../types/adminVendorDashboard'
import { typeDef as AdminVendorInfo} from '../types/adminVendorInfo'

import { resolvers as userRes } from '../resolvers/user'
import { resolvers as vendorRes } from '../resolvers/vendor'
import { resolvers as commentRes } from '../resolvers/comment'
import { resolvers as categoryRes } from '../resolvers/category'
import { resolvers as keywordRes } from '../resolvers/keyword'
import { resolvers as locationRes } from '../resolvers/location'
import { resolvers as suggestedBrandRes } from '../resolvers/suggestedBrand'
import { resolvers as potentialBrandRes } from '../resolvers/potentialBrand'
import { resolvers as instagramSuggestedBrandRes } from '../resolvers/instagramSuggestedBrand'
import { resolvers as contactUsRes } from '../resolvers/contactUs'
import { resolvers as vendorCandidateRes } from '../resolvers/vendorCandidate'
import { resolvers as professionalViewRes } from '../resolvers/professionalView'
import { resolvers as customerViewRes } from '../resolvers/customerView'
import { resolvers as articleRes } from '../resolvers/article'
import { resolvers as instagramRes } from '../resolvers/instagram'
import { resolvers as interviewRes } from '../resolvers/interview'
import { resolvers as partnerRes } from '../resolvers/partner'
import { resolvers as influencerRes } from '../resolvers/influencer'
import { resolvers as configRes } from '../resolvers/config'
import { resolvers as genderRes } from '../resolvers/gender'
import { resolvers as labelRes } from '../resolvers/label'
import { resolvers as youtubeRes } from '../resolvers/youtube'
import { resolvers as imageCarouselUrlRes } from '../resolvers/imageCarouselUrl'
import { resolvers as achievementRes } from '../resolvers/achievement'
import { resolvers as rewardRes } from '../resolvers/reward'
import { resolvers as vendorDashboardRes } from '../resolvers/vendorDashboard'
import { resolvers as vendorAccountRes } from '../resolvers/vendorAccount'
import { resolvers as subscriptionDashboardRes } from '../resolvers/subscriptionDashboard'
import { resolvers as vendorDraftDashboardRes } from '../resolvers/vendorDraftDashboard'
import { resolvers as vendorTestimonyRes } from '../resolvers/vendorTestimony'
import { resolvers as customerOpinionRes } from '../resolvers/customerOpinion'
import { resolvers as adminVendorDashboardRes } from '../resolvers/adminVendorDashboard'
import { resolvers as adminVendorInfoRes } from '../resolvers/adminVendorInfo'

import { GraphQLScalarType } from 'graphql';


export const typeDefs = gql `
  scalar Date
  scalar MongoID

  type Query {
    "Returns the current API version"
    version: String
  }

  type Mutation {
    "(deprecated) Set the current API version"
    version: String
  }

  ${User}
  ${Vendor}
  ${Comment}
  ${Category}
  ${Keyword}
  ${Location}
  ${PotentialBrand}
  ${SuggestedBrand}
  ${InstagramSuggestedBrand}
  ${ContactUs}
  ${VendorCandidate}
  ${ProfessionalView}
  ${CustomerView}
  ${Article}
  ${Instagram}
  ${Interview}
  ${Partner}
  ${Influencer}
  ${Config}
  ${Common}
  ${Gender}
  ${Moral}
  ${Label}
  ${Youtube}
  ${ImageCarouselUrl}
  ${Achievement}
  ${Reward}
  ${VendorDashboard}
  ${VendorAccount}
  ${SubscriptionDashboard}
  ${VendorDraftDashboard}
  ${VendorTestimony}
  ${CustomerOpinion}
  ${AdminVendorDashboard}
  ${AdminVendorInfo}
`;

export const resolvers = merge({}, 
  userRes, vendorRes, commentRes, categoryRes, keywordRes, 
  locationRes, suggestedBrandRes, potentialBrandRes, contactUsRes,
  vendorCandidateRes, professionalViewRes, customerViewRes,articleRes,
  instagramRes, instagramSuggestedBrandRes ,interviewRes, partnerRes, 
  influencerRes, configRes, genderRes, labelRes, youtubeRes, imageCarouselUrlRes, achievementRes,
  rewardRes, vendorDashboardRes, vendorAccountRes, subscriptionDashboardRes,
  vendorDraftDashboardRes, vendorTestimonyRes, customerOpinionRes,
  adminVendorDashboardRes, adminVendorInfoRes)
