import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'


const colSingle = 'InstagramSuggestedBrand'
const colPlural = 'InstagramSuggestedBrands'


const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const InstagramSuggestedBrandSchema = {
  name: {
    type: String,
    required: 'Name must be present'
  },
  occurrence: Number,
  checked: Boolean,
  latestPostDate: Date,
  addedHimself: Boolean
}

const modelSchema = new Schema(InstagramSuggestedBrandSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})


modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
