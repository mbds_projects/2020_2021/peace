import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'


const colSingle = 'SuggestedBrand'
const colPlural = 'SuggestedBrands'


const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const SuggestedBrandSchema = {
  name: {
    type: String,
    required: 'Name must be present'
  },
  categories: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category'
  }],
  keywords: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Keyword'
  }],
  mail: String,
  occurrence: Number,
  suggestedBy: [{
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    message: { type: String }
  }],
  site: String,
  cities: String,
  instagramId: String,
  contact: Number,
  state: {
    type: String,
    enum : ['notContacted', 'firstContact','certificationValidationOngoing', 'certificationValidated', 'dataValidationOngoing', 'dataValidated', 'dataCreatingOngoing', 'dataCreated'],
    default: 'notContacted'
  },
  vendor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Vendor'
  }
}

const modelSchema = new Schema(SuggestedBrandSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})


modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
