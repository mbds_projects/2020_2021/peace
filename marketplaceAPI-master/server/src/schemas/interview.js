import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'Interview'
const colPlural = 'Interviews'

const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const Interview = {
  author: String,
  title: String,
  subTitle: String,
  link: String,
  imgUrl: String,
  date: Date,
  type: {
    type: String,
    enum : ['article', 'video'],
    default: 'article'
  }
}

const modelSchema = new Schema(Interview, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})

modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
