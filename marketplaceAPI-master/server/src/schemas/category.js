import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'Category'
const colPlural = 'Categories'

const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const CategorySchema = {
    name: String,
    used: {type: Boolean, default: false},
    lang: [{
      locale: String,
      value: String
    }],
    pictureUrl: String,
    pos: Number
}

const modelSchema = new Schema(CategorySchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})

modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
