import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'Keyword'
const colPlural = 'Keywords'

const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const KeywordSchema = {
    name: String,
    used: {type: Boolean, default: false},
    lang: [{
      locale: String,
      value: String
    }],
    moral: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Moral'
    },
    pictureUrl: String
}

const modelSchema = new Schema(KeywordSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})

modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
