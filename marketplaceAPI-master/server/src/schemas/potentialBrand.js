import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'


const colSingle = 'PotentialBrand'
const colPlural = 'PotentialBrands'


const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const PotentialBrandSchema = {
  name: {
    type: String,
    required: 'Name must be present'
  },
  categories: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category'
  }],
  keywords: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Keyword'
  }],
  mail: String,
  note: String,
  site: String,
  cities: String,
  instagramId: String,
  state: {
    type: String,
    enum : ['notContacted', 'firstContact','certificationValidationOngoing', 'certificationValidated', 'dataValidationOngoing', 'dataValidated', 'dataCreatingOngoing', 'dataCreated'],
    default: 'notContacted'
  }
  
}

const modelSchema = new Schema(PotentialBrandSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})


modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
