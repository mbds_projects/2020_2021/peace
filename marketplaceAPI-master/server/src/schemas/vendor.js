import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'Vendor'
const colPlural = 'Vendors'


const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const VendorSchema = {
  name: {
    type: String,
    required: 'Name must be present'
  },
  site: {
    type: String
  },
  desc: {
    lang: [{
      locale: String,
      value: String
    }]
  },
  phoneNumber: {
    type: String
  },
  published: { type: Boolean, default: false },
  profilePictureUrl: String,
  logoUrl: String,
  carouselPicturesUrl: [String],
  videoYoutubeId: {
    type: String
  },
  videoDailymotionId: {
    type: String
  },
  videoVimeoId: {
    type: String
  },
  socialNetworks: [{
    type: {
      type: String
    },
    url: String
  }],
  featuredProductsUrl: [{
    link: String,
    price: Number,
    imgUrl: String
  }],
  categories: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category'
  }],
  labels: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Label'
  }],
  keywords: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Keyword'
  }],
  genders: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Gender'
  }],
  validated: {
    type: Boolean,
    default: false
  },
  locations: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Location'
  }],
  alsoAvailableOn: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Vendor'
  }],
  comments: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Comment'
  }],
  rewards: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Reward'
  }],
  rate: Number,
  onlineShop: Boolean,
  role: {
    type: String,
    enum: ['vendor'],
    default: 'vendor'
  },
  mail: {
    type: String,
    unique: true,
    match: new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$')
  },
  hash: {
    type: String
  },
  customerId: String,
  subscriptionDashboardId: String,
  activated: {
    type: Boolean,
    default: "true"
  },
  clickAndCollect: Boolean,
  paymentMethodId: String,
  priceId: String,
  from: Number, 
  tvaIntra: String,
  subscriptionType: String,
}

const modelSchema = new Schema(VendorSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})


modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
