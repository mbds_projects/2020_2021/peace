import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'VendorTestimony'
const colPlural = 'VendorTestimonies'

const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const VendorTestimony = {
  vendor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Vendor'
  },
  testimonyLink: {
    lang: [{
      locale: String,
      value: String
    }]
  },
}

const modelSchema = new Schema(VendorTestimony, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})

modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
