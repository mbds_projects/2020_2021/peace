import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'Location'
const colPlural = 'Locations'


const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const LocationSchema = {
  address: {
    type: String
  },
  city: {
    type: String
  },
  shopName: {
    type: String
  },
  postalCode: {
    type: String
  },
  openingHours: {
    type: String
  },
  phoneNumber: {
    type: String
  },
  webSite: {
    type: String
  },
  mail: {
    type: String
  },
  county: String,
  state: String,
  location: {
    //[LON, LAT]
    coordinates: {
      type: [Number]
    },
    type: {
      type: String
    }
  },
  vendors: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Vendor'
  }]
  
}

const modelSchema = new Schema(LocationSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})


modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
