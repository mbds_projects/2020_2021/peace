import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'VendorDashboardDraft'
const colPlural = 'VendorsDashboardDraft'


const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const VendorDashboardDraftSchema = {
  vendorId: {
    type: mongoose.Schema.Types.ObjectId
  },
  name: {
    type: String,
    required: 'Name must be present'
  },
  site: {
    type: String
  },
  desc: {
    lang: [{
      locale: String,
      value: String
    }]
  },
  phoneNumber: {
    type: String
  },
  profilePictureUrl: String,
  logoUrl: String,
  carouselPicturesUrl: [String],
  videoYoutubeId: {
    type: String
  },
  videoDailymotionId: {
    type: String
  },
  videoVimeoId: {
    type: String
  },
  socialNetworks: [{
    type: {
      type: String
    },
    url: String
  }],
  featuredProductsUrl: [{
    link: String,
    price: Number,
    imgUrl: String
  }],
  categories: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category'
  }],
  labels: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Label'
  }],
  keywords: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Keyword'
  }],
  genders: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Gender'
  }],
  locations: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Location'
  }],
  alsoAvailableOn: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Vendor'
  }],
  rewards: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Reward'
  }],
  onlineShop: Boolean
}

const modelSchema = new Schema(VendorDashboardDraftSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})


modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
