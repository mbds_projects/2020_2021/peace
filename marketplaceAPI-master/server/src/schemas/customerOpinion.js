import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'CustomerOpinion'
const colPlural = 'CustomerOpinions'

const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const CustomerOpinion = {
  text: String,
  date: Date,
  rate: Number,
  mail: {
    type: String,
    unique: true,
    match: new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$')
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
}

const modelSchema = new Schema(CustomerOpinion, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})

modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
