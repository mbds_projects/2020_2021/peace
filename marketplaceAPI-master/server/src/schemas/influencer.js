import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'Influencer'
const colPlural = 'Influencers'

const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const Influencer = {
  link: String,
  imgUrl: String,
  title: String
}

const modelSchema = new Schema(Influencer, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})

modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
