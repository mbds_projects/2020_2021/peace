import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'Achievement'
const colPlural = 'Achievements'


const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const AchievementSchema = {
  type: {
    type: String,
    required: 'Type must be present'
  },
  subtype: String,
  value: {
    type: Number,
    default: 0
  },
  icon: String,
  level: {
    type: Number,
    default: 0
  },
  activated: {
    type: Boolean,
    default: false
  },
  link: String
}

const modelSchema = new Schema(AchievementSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})


modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
