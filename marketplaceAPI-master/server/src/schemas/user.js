import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'User'
const colPlural = 'Users'


const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const UserSchema = {
  name: {
    type: String,
    required: 'Name must be present'
  },
  surname: {
    type: String,
    required: 'Surname must be present'
  },
  role: {
    type: String,
    enum : ['user','admin'],
    default: 'user'
  },
  reported: {
    type: Number,
    default: 0
  },
  birthday: {
    type: Date
  },
  newsletter: {
    type: Boolean
  },
  facebookId: {
    type: String,
    unique: true
  },
  mail: {
    type: String,
    required: 'Mail must be present',
    unique: true,
    match: new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$')
  },
  hash: {
    type: String
  },
  profilePictureUrl: String,
  validated: {
    type: Boolean,
    default: false
  },
  favorites: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Vendor'
  }],
  keywords: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Keyword'
  }],
  achievements: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Achievement'
  }],
  rewards: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Reward'
  }],
  defaultColor: String,
  blockedSince: Date
}

const modelSchema = new Schema(UserSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})


modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
