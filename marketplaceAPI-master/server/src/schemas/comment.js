import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'Comment'
const colPlural = 'Comments'

const Schema = mongoose.Schema
mongoose.Promise = global.Promise;

const ObjectId = mongoose.Schema.Types.ObjectId;

export const CommentSchema = {
  text: String,
  postedBy: {
      type: ObjectId,
      ref: 'User'
  },
  date: Date,
  rate: Number,
  vendor: {
    type: ObjectId,
    ref: 'Vendor'
  },
  reportedBy: [
    {
      type: ObjectId,
      ref: 'User'
    }
  ]
}

const modelSchema = new Schema(CommentSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})

modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
