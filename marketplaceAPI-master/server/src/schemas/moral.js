import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'Moral'
const colPlural = 'Morals'

const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const MoralSchema = {
    name: String,
    lang: [{
      locale: String,
      value: String
    }]
}

const modelSchema = new Schema(MoralSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})

modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
