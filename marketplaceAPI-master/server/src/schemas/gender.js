import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'

const colSingle = 'Gender'
const colPlural = 'Genders'

const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const GenderSchema = {
    name: String,
    used: {type: Boolean, default: false},
    lang: [{
      locale: String,
      value: String
    }],
    pictureUrl: String,
    pos: Number
}

const modelSchema = new Schema(GenderSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})

modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
