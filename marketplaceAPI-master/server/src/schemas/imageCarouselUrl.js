import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'ImageCarouselUrl'
const colPlural = 'ImagesCarouselUrl'

const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const ImageCarouselUrlSchema = {
    mode: String,
    url: String,
    pos: Number,
    lang: String,
    href: String,
    to: String
}

const modelSchema = new Schema(ImageCarouselUrlSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})

modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
