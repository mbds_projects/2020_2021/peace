import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'VendorCandidate'
const colPlural = 'VendorsCandidates'


const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const VendorCandidateSchema = {
  name: {
    type: String,
    required: 'Name must be present'
  },
  phoneNumber: {
    type: String
  },
  keywords: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Keyword'
  }],
  mail: {
    type: String,
    //required: 'Mail must be present',
    unique: true,
    match: new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$')
  },
  contactName: {
    type: String
  },
  site: {
    type: String
  },
  categories: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category'
  }],
  keywords: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Keyword'
  }],
  labels: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Label'
  }],
  genders: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Gender'
  }],
  certificate: [{
    type: String
    
  }],
  subscriptionType: String,
  pointOfSales: [{
    shopName: { type: String},
    address: {type: String},
    city: {type: String},
    postalCode: {type: String}
  }],
  paymentMethodId: String,
  priceId: String,
  from: Number, 
  tvaIntra: String,
  vendor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Vendor'
  }
  
}

const modelSchema = new Schema(VendorCandidateSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})


modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
