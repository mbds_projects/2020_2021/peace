import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'ProfessionalView'
const colPlural = 'ProfessionalViews'

const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const ProfessionalView = {
  text: String,
  date: Date,
  rate: Number,
  vendor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Vendor'
  }
}

const modelSchema = new Schema(ProfessionalView, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})

modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
