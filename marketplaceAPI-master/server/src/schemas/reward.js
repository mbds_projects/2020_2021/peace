import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'Reward'
const colPlural = 'Rewards'


const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const RewardSchema = {
  type: {
    type: String,
    required: 'Type must be present'
  },
  code: String,
  validity: Date,
  value: String,
  vendor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Vendor'
  },
  message: String,
  activated: {
    type: Boolean,
    default: "true"
  }
}

const modelSchema = new Schema(RewardSchema, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})


modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
