import mongoose from 'mongoose'
import bfyUnique from 'mongoose-unique-validator'
import mongodbErrorHandler from 'mongoose-mongodb-errors'
import _ from 'lodash'

const colSingle = 'CustomerView'
const colPlural = 'CustomerViews'

const Schema = mongoose.Schema
mongoose.Promise = global.Promise;


export const CustomerView = {
  text: String,
  date: Date,
  rate: Number,
  postedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
  }
}

const modelSchema = new Schema(CustomerView, {
  collection: colPlural,
  retainKeyOrder: true,
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})

modelSchema.pre('save', async function () {
  let model = this;
});

modelSchema.plugin(bfyUnique);
modelSchema.plugin(mongodbErrorHandler)

export default mongoose.model(colSingle, modelSchema)
