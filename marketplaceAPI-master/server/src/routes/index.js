import publics from './public'
const routes_in = [publics]


/*
* join all routes of the api
*/
export default (app) => {
  routes_in.forEach((route) => {
    app
      .use(route.routes())
      .use(route.allowedMethods({
        throw: true,
      }))
  })
}
