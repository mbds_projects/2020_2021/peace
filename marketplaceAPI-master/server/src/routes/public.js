import Router          from 'koa-router'
import { koa as voyagerMiddleware } from 'graphql-voyager/middleware';
import koaSend from 'koa-send';
import { CONF } from '../config'
const path = require('path');
const stripe = require('stripe')(CONF.stripe.sk);
/*
* routes
*/
const router = new Router();

router.all('/api/public/voyager', voyagerMiddleware({
  endpointUrl: '/api/public/graphql'
}));

router.post('/stripe-webhook', async (ctx, next) => {
  let event;

    try {
      event = stripe.webhooks.constructEvent(
        ctx.request.rawBody,
        ctx.request.header['stripe-signature'],
        CONF.stripe.webhook
      );
    } catch (err) {
      console.log(err);
      console.log(`⚠️  Webhook signature verification failed.`);
      console.log(
        `⚠️  Check the env file and enter the correct webhook secret.`
      );
      ctx.status = 400;
    }
    // Extract the object from the event.
    const dataObject = event.data.object;

    // Handle the event
    // Review important events for Billing webhooks
    // https://stripe.com/docs/billing/webhooks
    // Remove comment to see the various objects sent for this sample
    switch (event.type) {
      case 'invoice.paid':
        console.log('invoice.paid')
        // Used to provision services after the trial has ended.
        // The status of the invoice will show up as paid. Store the status in your
        // database to reference when a user accesses your service to avoid hitting rate limits.
        break;
      case 'invoice.payment_failed':
          console.log('invoice.payment_failed')
        // If the payment fails or the customer does not have a valid payment method,
        //  an invoice.payment_failed event is sent, the subscription becomes past_due.
        // Use this webhook to notify your user that their payment has
        // failed and to retrieve new card details.
        break;
      case 'invoice.finalized':
          console.log('invoice.finalized')
        // If you want to manually send out invoices to your customers
        // or store them locally to reference to avoid hitting Stripe rate limits.
        break;
      case 'customer.subscription.deleted':
          console.log('customer.subscription.deleted')
        if (event.request != null) {
          // handle a subscription cancelled by your request
          // from above.
        } else {
          // handle subscription cancelled automatically based
          // upon your subscription settings.
        }
        break;
      case 'customer.subscription.trial_will_end':
          console.log('customer.subscription.trial_will_end')
        // Send notification to your user that the trial will end
        break;
      default:
      // Unexpected event type
    }

    ctx.status = 200;
    
})

router.get('*', async ctx => {
  const absolutePath = path.join(__dirname, '../../client/build/index.html');
  return koaSend(ctx, absolutePath, {root: '/'})
});

export default router
