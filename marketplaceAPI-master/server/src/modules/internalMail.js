import nodemailer from 'nodemailer'
import { CONF } from '../config'


export function sendInternalMail(mailOptions) {
  var transporter = nodemailer.createTransport({
    host: "smtp.ionos.fr",
    port: 465,
    secure: true, // upgrade later with STARTTLS
    auth: {
      user: CONF.nodemailer.user,
      pass: CONF.nodemailer.pass
    }
  });

  try {
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        throw new Error("mail.cannotBeSent");
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
  } catch(e) {
    throw e;
  }
}

