import request from 'request';
import { CONF } from '../config'


export async function sendEmail(templateId, mail, params) {
  var options = {
    method: 'POST',
    url: 'https://api.sendinblue.com/v3/smtp/email',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      'api-key': CONF.sendinBlueAPIKeyInternal
    },
    body: `{"to":[{"email":"${mail}"}],
    "templateId":${templateId},
    "params":${JSON.stringify(params)}}`
  };
  
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
  });
}

export async function createContact(listIds, mail, name, surname, birthday, newsletter) {
  let attributes = "";
    if(!!birthday) {
      let date = new Date(birthday);
      let day = ("0" + date.getDate()).slice(-2)
      let month = ("0" + (date.getMonth() + 1)).slice(-2)
      let dateString= `${date.getFullYear()}-${month}-${day}`;
      attributes += `"DATE_DE_NAISSANCE":"${dateString}"`
    }
    
    if(attributes !== "") {
      attributes += ","
    }
    attributes += `"NEWSLETTER":${newsletter}`

    let body = 
    `{"email":"${mail}",
      "updateEnabled":true,
      "attributes":{
        "PRENOM":"${name}",
        "NOM":"${surname}",
        ${attributes}
      },
      "listIds":[${listIds.toString()}]
      }`;

  var options = {
    method: 'POST',
    url: 'https://api.sendinblue.com/v3/contacts',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      'api-key': CONF.sendinBlueAPIKey 
    },
    body: body
  };
  
  request(options, function (error, response, body) {
    if (error) {
      throw new Error(error);
    }
  });
}

export async function createContactBrand(listId, mail, brand) {
    let body = 
    `{"email":"${mail}",
      "updateEnabled":true,
      "attributes":{
        "MARQUE":"${brand}"
      },
      "listIds":[${listId}]
      }`;

  var options = {
    method: 'POST',
    url: 'https://api.sendinblue.com/v3/contacts',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      'api-key': CONF.sendinBlueAPIKey 
    },
    body: body
  };
  
  request(options, function (error, response, body) {
    if (error) {
      throw new Error(error);
    }
  });
}

  export async function updateContact(listId, email, birthday, newsletter, keywords) {
    let attributes = "";
    if(!!birthday) {
      let date = new Date(birthday);
      let day = ("0" + date.getDate()).slice(-2)
      let month = ("0" + (date.getMonth() + 1)).slice(-2)
      let dateString= `${date.getFullYear()}-${month}-${day}`;
      attributes += `"DATE_DE_NAISSANCE":"${dateString}",`
    }
    
    attributes += `"NEWSLETTER":${newsletter},`

    let strKeywords = keywords.join(' ; ');


    attributes += `"MOTS-CLES":"${strKeywords}"`
    
    let body = 
    `{
      "attributes":{
        ${attributes}
      },
      "listIds":[${listId}]
      }`;

    let encodedEmail = encodeURIComponent(email);
    
    let url = `https://api.sendinblue.com/v3/contacts/${encodedEmail}`

    var options = {
      method: 'PUT',
      url: url,
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
        'api-key': CONF.sendinBlueAPIKey 
      },
      body: body
    };
    
    request(options, function (error, response, body) {
      if (error) {
        throw new Error(error);
      }
    });
}
