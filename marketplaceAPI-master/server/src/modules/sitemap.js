const fs = require('fs');
const mongoose = require('mongoose');
if(process.env.DATABASE) {
    mongoose.connect(process.env.DATABASE, {useNewUrlParser: true});
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        const modelSchema = new mongoose.Schema({
            name: String
          }, {
            collection: 'Vendors',
            retainKeyOrder: true,
            timestamps: true,
            toJSON: {
              virtuals: true
            }
          })
        
        const Vendors = mongoose.model('Vendors', modelSchema);
          
        Vendors.find({}, 'name', function(err, res) {
            let vendors = [];
            res.forEach(el => {
                vendors.push(`${el.id}?name=${el.name}`)
            })
            fs.writeFileSync('../client/public/vendors.json', JSON.stringify(vendors));
            mongoose.disconnect()
        });
    });
}


