import jwt    from 'jsonwebtoken'
import {CONF} from '../config'
import {AuthError} from './errors'

export async function checkToken(token, secret=CONF.secret) {
  try {
    let decoded = await jwt.verify(token, secret);
    return (decoded ? decoded : false)
  } catch (err) {
    throw new AuthError('CheckTokenAuthError', '4')
  }
}

export function createAuthentJwt(user) {
  var timeAuth = '480m';
  const TKN_AUTH = {
    expiresIn: timeAuth
  }
    
  const token = jwt.sign({
    id: user.id,
    role: user.role
  }, CONF.secret, TKN_AUTH);
  return token;
}

export function createPasswordVendorJwt(id, name, timeAuth='15m') {
  const TKN_AUTH = {
  expiresIn: timeAuth
  }
  
  const token = jwt.sign({
  id,
  name
  }, CONF.secret, TKN_AUTH);
  return token;
  }