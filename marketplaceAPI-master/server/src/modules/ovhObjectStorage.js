var OVHStorage = require('node-ovh-objectstorage');
import { CONF } from '../config'

export async function sendImageToOvh(pathFrom, pathTo) {
  try {
    let storage = new OVHStorage(CONF.ovh);
    await storage.connection();
    let saved = await storage.objects().save_with_result(pathFrom, `/public-files/${pathTo}`)
    if(saved) {
      let pathOfFile = `https://storage.sbg.cloud.ovh.net/v1/AUTH_${CONF.ovh.tenantId}/public-files/${pathTo}`;
      return pathOfFile;
    } else {
      throw new Error("ovh.uploadFailed")
    }
  } catch(e) {
    throw e;
  }
}

export async function removeImageToOvh(path) {
  try {
    let storage = new OVHStorage(CONF.ovh);
    await storage.connection();
    let cleanedPath = path.replace(`https://storage.sbg.cloud.ovh.net/v1/AUTH_${CONF.ovh.tenantId}`, "")
    let deleted = await storage.objects().delete_with_result(cleanedPath)
    return deleted;
  } catch(e) {
    throw e;
  }
}