
import { CONF } from '../config'

import { compareAsc } from 'date-fns'

const stripe = require('stripe')(CONF.stripe.sk);

export async function isAllowedToPublish(vendor, numberOfPointOfSale) {
  try {
    let ableToPublish = false;
    let reason = "";
      let { subscriptionDashboardId } = vendor;
      if(subscriptionDashboardId) {
        const subscription = await stripe.subscriptions.retrieve(subscriptionDashboardId, {expand: ['items.data.price.product']});
        if(subscription) {
          if(subscription.status === "active" || subscription.status === "trialing" || subscription.status === "canceled") {
            let productName = subscription.items.data[0].price.product.name;
            if((compareAsc(new Date(), new Date(subscription.current_period_end * 1000)) < 0)) {
              switch(productName) {
                case "Basic":
                  if(vendor.published) reason = "basic"
                  else if(numberOfPointOfSale > 6) reason = "numberOfPointOfSale"
                  else ableToPublish = true;
                  break;
                case "Super":
                  if(numberOfPointOfSale <= 20) ableToPublish = true;
                  else reason = "numberOfPointOfSale"
                  break;
                case "Super +":
                  ableToPublish = true;
                  break;
              }
            } else {
              reason = "expired"
            }
          } else {
            reason = "incomplete"
          }
          
        } else {
          reason = "notSubscribe"
        }
      } else {
        reason = "notSubscribe"
      }
    return {ableToPublish, reason};
  } catch (err) {
    throw new Error(`Error during check allow of vendor publishing: ${err.message}`);
  }
}

export async function isValidSubscription(vendor) {
  try {
    let { subscriptionDashboardId } = vendor;
    if (subscriptionDashboardId) {
      const subscription = await stripe.subscriptions.retrieve(subscriptionDashboardId);
      return (subscription.status === "active" || subscription.status === "trialing" || subscription.status === "canceled") && compareAsc(new Date(), new Date(subscription.current_period_end * 1000)) < 0
    } else {
      return false
    }
  } catch (err) {
    throw new Error(`Error during check allow of vendor publishing: ${err.message}`);
  }
}
