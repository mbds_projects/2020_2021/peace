import { CONF } from '../config'

export function getPriceId(subscriptionType) {
  let subscriptionDetails = subscriptionType.split('.');
  let idOfFrequency = 0;
  switch(subscriptionDetails[1]) {
    case "monthly": idOfFrequency = 0; break;
    case "yearly": idOfFrequency = 1; break;
    case "weTakeCare": idOfFrequency = 2; break;
    default: throw new Error("subscription.unknown"); break;
  }
  let priceId = CONF.stripe.prices[subscriptionDetails[0]][idOfFrequency];
  return priceId;
}
