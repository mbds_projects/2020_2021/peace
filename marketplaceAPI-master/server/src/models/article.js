import _ from 'lodash'

var rp = require('request-promise');

export async function articles(root, args, context, info) {
  try {
    let url = "https://blog.super-responsable.org//wp-json/wp/v2/posts?_fields=link,featured_media,title,tags&tags_exclude=1106"
    return rp(url)
        .then(async function(res) {
          let articles = JSON.parse(res).slice(0, 3);
          return articles.map((a) => {

            let url = `https://blog.super-responsable.org/wp-json/wp/v2/media/${a.featured_media}?_fields=guid`
            return rp(url)
                .then(async function(res) {
                  let media = JSON.parse(res);
                  let title = a.title.rendered;
                  let guidRendered = media.guid.rendered;
                  guidRendered = guidRendered.replace("https://super-responsable.org", "https://blog.super-responsable.org")
                  let article = {
                    link: a.link,
                    title: title,
                    media:  guidRendered
                  }
                  return article;
                })
                .catch(function(err) {
                  throw new Error(`Error during retrieving media: ${err.message}`);
                })
          });
        })
        .catch(function(err) {
          throw new Error(`Error during retrieving articles: ${err.message}`);
        })
  } catch (err) {
    throw new Error(`Error during retrieving articles: ${err.message}`);
  }
}

export async function articlesB2B(root, args, context, info) {
  try {
    let url = "https://blog.super-responsable.org//wp-json/wp/v2/posts?_fields=link,featured_media,title,tags&categories=1105"
    return rp(url)
        .then(async function(res) {
          let articles = JSON.parse(res).slice(0, 3);
          return articles.map((a) => {

            let url = `https://blog.super-responsable.org/wp-json/wp/v2/media/${a.featured_media}?_fields=guid`
            return rp(url)
                .then(async function(res) {
                  let media = JSON.parse(res);
                  let title = a.title.rendered;
                  let guidRendered = media.guid.rendered;
                  guidRendered = guidRendered.replace("https://super-responsable.org", "https://blog.super-responsable.org")
                  let article = {
                    link: a.link,
                    title: title,
                    media:  guidRendered
                  }
                  return article;
                })
                .catch(function(err) {
                  throw new Error(`Error during retrieving media: ${err.message}`);
                })
          });
        })
        .catch(function(err) {
          throw new Error(`Error during retrieving articles: ${err.message}`);
        })
  } catch (err) {
    throw new Error(`Error during retrieving articles: ${err.message}`);
  }
}
