import _ from 'lodash'
import Location from '../schemas/location'
import Vendor from '../schemas/vendor'
import VendorDashboardDraft from '../schemas/vendorDashboardDraft'
import mongoose from 'mongoose';
import { checkToken } from '../modules/auth'
import { isAllowedToPublish } from '../modules/vendor'
const Nominatim = require('nominatim-geocoder')
const ObjectID = mongoose.Types.ObjectId;

export async function locationsByCity(root, args, context, info) {
  let results = {}
  try {

    results = await Location.aggregate([
      {
        $group: {
          _id: { $toLower: "$city" },
          city: { $first: { $toLower: "$city" } },
          vendors: { $addToSet: "$vendors" }
        }
      },
      {
        $project: {
          city: 1,
          vendors: {
            $reduce: {
              input: "$vendors",
              initialValue: [],
              in: { $setUnion: ["$$value", "$$this"] }
            }
          }
        }
      }
    ]);
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving locations by city: ${err.message}`);
  }
}

export async function locationsByCounty(root, args, context, info) {
  let results = {}
  try {

    results = await Location.aggregate([
      {
        $group: {
          _id: { $toLower: "$county" },
          county: { $first: { $toLower: "$county" } },
          vendors: { $addToSet: "$vendors" }
        }
      },
      {
        $project: {
          county: 1,
          vendors: {
            $reduce: {
              input: "$vendors",
              initialValue: [],
              in: { $setUnion: ["$$value", "$$this"] }
            }
          }
        }
      }
    ]);
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving locations by county: ${err.message}`);
  }
}

export async function locationsOfVendorByCity(root, args, context, info) {
  let results = {}
  try {
    let { vendorId, position } = args
    let vendor = await Vendor.findOne({ _id: ObjectID(vendorId) });
    let { locations } = vendor
    let aggregate = [];
    if (position) {

      let { latitude, longitude } = position;
      aggregate = [
        {
          $geoNear: {
            near: {
              type: "Point",
              coordinates: [latitude, longitude]
            },
            distanceField: "distance"
          }
        },
        { $match: { _id: { $in: locations } } },
        {
          $group: {
            _id: { $toLower: "$city" },
            city: { $first: { $toLower: "$city" } },
            distance: { $first: "$distance" },
            shops: {
              $addToSet: {
                id: "$_id",
                shopName: "$shopName",
                address: "$address",
                city: { $toLower: "$city" },
                postalCode: "$postalCode",
                location: "$location",
                openingHours: "$openingHours",
                phoneNumber: "$phoneNumber",
                webSite: "$webSite",
                mail: "$mail",
                county: "$county",
                state: "$state"
              }
            }
          }
        },
        {
          $sort: { distance: 1 }
        }
      ]
    } else {
      aggregate = [
        { $match: { _id: { $in: locations } } },
        {
          $group: {
            _id: { $toLower: "$city" },
            city: { $first: { $toLower: "$city" } },
            shops: {
              $addToSet: {
                id: "$_id",
                shopName: "$shopName",
                address: "$address",
                city: { $toLower: "$city" },
                postalCode: "$postalCode",
                location: "$location",
                openingHours: "$openingHours",
                phoneNumber: "$phoneNumber",
                webSite: "$webSite",
                mail: "$mail",
                county: "$county",
                state: "$state"
              }
            }
          }
        }
      ]
    }
    results = await Location.aggregate(aggregate);
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving locations by city: ${err.message}`);
  }
}

export async function locationsOfVendorByCounty(root, args, context, info) {
  let results = {}
  try {
    let { vendorId } = args
    let vendor = await Vendor.findOne({ _id: ObjectID(vendorId) });
    let { locations } = vendor
    results = await Location.aggregate([
      { $match: { _id: { $in: locations } } },
      {
        $group: {
          _id: { $toLower: "$county" },
          county: { $first: { $toLower: "$county" } },
          shops: {
            $addToSet: {
              id: "$_id",
              shopName: "$shopName",
              address: "$address",
              county: { $toLower: "$county" },
              postalCode: "$postalCode",
              location: "$location",
              openingHours: "$openingHours",
              phoneNumber: "$phoneNumber",
              webSite: "$webSite",
              mail: "$mail",
              city: "$city",
              state: "$state"
            }
          }
        }
      },
    ]);
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving locations by county: ${err.message}`);
  }
}


export async function locations(root, args, context, info) {
  let results = {}
  try {

    results = await Location.find({});
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving locations: ${err.message}`);
  }
}

export async function location(root, args, context, info) {
  let { id } = args;
  let result = {}
  try {
    result = await Location.findOne({ _id: ObjectID(id) });
    return result ? result : {}
  } catch (err) {
    throw new Error(`Error during retrieving a location: ${err.message}`);
  }
}

export async function createLocation(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if (role !== "admin") {
    throw new Error("token.adminRight");
  }
  let { input } = args
  try {
    let location = await Location.create(input);
    return location ? location : {}
  } catch (err) {
    throw new Error(`Error during creating a location: ${err.message}`);
  }
}

export async function updateLocation(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if (role !== "admin") {
    throw new Error("token.adminRight");
  }
  let { input, id } = args
  try {
    let locationUpdated = await Location.updateOne(
      { _id: ObjectID(id) },
      { $set: input });
    return !!locationUpdated
  } catch (err) {
    throw new Error(`Error during updating a location: ${err.message}`);
  }
}

export async function searchAndCreateLocation(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if (role !== "admin") {
    throw new Error("token.adminRight");
  }
  let { input } = args
  let count = 0;
  let countError = 0;
  let countOutFrance = 0;
  try {
    const geocoder = new Nominatim({}, {
      format: 'json',
      limit: 1,
    })
    let addresses = input.addresses;
    for (let i = 0; i < addresses.length; i++) {
      try {
        let address = addresses[i];
        let response = await geocoder.search({ q: address.description, addressdetails: 1, extratags: 1 });
        let firstLoc = response[0]
        let addFL = firstLoc.address
        if (addFL.country !== "France") {
          countOutFrance++;
        } if (firstLoc.lat < -90 || firstLoc.lat > 90) {
          countError++;
        } if (firstLoc.lon < -90 || firstLoc.lon > 90) {
          countError++;
        } else {
          let city = addFL.city || addFL.town || addFL.village || addFL.locality;
          if (city) {
            let locToCreate = {
              shopName: address.name,
              location: {
                type: "Point",
                coordinates: [firstLoc.lat, firstLoc.lon]
              },
              vendors: [input.vendorId],
              address: `${addFL.house_number || ''} ${addFL.road}`,
              city: addFL.city || addFL.town || addFL.village,
              postalCode: addFL.postcode || '',
              openingHours: firstLoc.extratags.opening_hours || null,
              webSite: firstLoc.extratags.website || null,
              phoneNumber: firstLoc.extratags.phone || null,
              county: addFL.county,
              state: addFL.state
            };
            let locCreated = await Location.create(locToCreate);

            await Vendor.findOneAndUpdate(
              { _id: ObjectID(input.vendorId) },
              { $push: { locations: locCreated.id } },
              { upsert: true, new: true }
            );
            count++;
          } else {
            countError++
          }
        }
      }
      catch (e) {
        countError++;
      }
    }

    return {
      success: count,
      error: countError,
      na: countOutFrance
    };

  } catch (err) {
    throw new Error(`Error during searching and creating a location: ${err.message}`);
  }
}

export async function reverseLocation(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if (role !== "admin") {
    throw new Error("token.adminRight");
  }
  try {
    const geocoder = new Nominatim({}, {
      format: 'json',
      limit: 1,
    })
    let allLocations = await Location.find({ county: null });
    if (allLocations) {
      allLocations.map(async (loc) => {
        let coordinates = loc.location.coordinates;
        let response = await geocoder.reverse({ lat: coordinates[0], lon: coordinates[1] });
        await Location.findOneAndUpdate(
          { _id: ObjectID(loc.id) },
          {
            $set: {
              county: response.address.county,
              state: response.address.state
            }
          },
          { upsert: true, new: true }
        );
      })
    }

  } catch (err) {
    throw new Error(`Error during reversing a location: ${err.message}`);
  }
}


export async function searchLocationDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if (role !== "vendor" && role !== "admin") {
    throw new Error("token.vendorRight");
  }
  let { address } = args
  try {
    const geocoder = new Nominatim({}, {
      format: 'json',
      limit: 5,
    })
    let responsesSearch = await geocoder.search({ q: address, addressdetails: 1, extratags: 1 });
    let responses = responsesSearch.map(el =>
      ({
        location: {
          type: "Point",
          coordinates: [el.lat, el.lon]
        },
        address: `${el.address.house_number || ''} ${el.address.road}`,
        city: el.address.city || el.address.town || el.address.village,
        postalCode: el.address.postcode || '',
        openingHours: el.extratags.opening_hours || null,
        webSite: el.extratags.website || null,
        phoneNumber: el.extratags.phone || null,
        county: el.address.county,
        state: el.address.state,
        shopName: el.address.shop || ''
      })
    );
    return responses;

  } catch (err) {
    throw new Error(`Error during searching a location: ${err.message}`);
  }
}

export async function createLocationDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let { input } = args
  try {
    let vendor = await Vendor.findOne({ _id: ObjectID(id) });
    let {ableToPublish, reason} = await isAllowedToPublish(vendor, (vendor.locations.length + 1));
    if (!ableToPublish)  throw new Error(`vendor.notAllowedToPublish.${reason}`);
    let searchLocation = await Location.findOne(
      {
        "location.coordinates.0": { $gt: (input.location.coordinates[0] - 0.0001), $lt: (input.location.coordinates[0] + 0.0001) },
        "location.coordinates.1": { $gt: (input.location.coordinates[1] - 0.0001), $lt: (input.location.coordinates[1] + 0.0001) }
      });
    if (searchLocation) {
      if (!searchLocation.vendors.includes(id)) {
        let locationUpdated = await Location.findOneAndUpdate(
          { _id: ObjectID(searchLocation.id) },
          {
            $set: input,
            $push: { "vendors": id }
          })
        await Vendor.updateOne({ _id: ObjectID(id) }, {
          $push: { "locations": locationUpdated.id }
        })
        await VendorDashboardDraft.updateOne({ vendorId: ObjectID(id) }, {
          $push: { "locations": locationUpdated.id }
        })
        return locationUpdated;
      } else {
        let locationUpdated = await Location.findOneAndUpdate(
          { _id: ObjectID(searchLocation.id) },
          { $set: input })
        await Vendor.updateOne({ _id: ObjectID(id) }, {
          $push: { "locations": locationUpdated.id }
        })
        await VendorDashboardDraft.updateOne({ vendorId: ObjectID(id) }, {
          $push: { "locations": locationUpdated.id }
        })
        return locationUpdated;
      }
    } else {
      let location = await Location.create({ ...input, vendors: [id] });
      await Vendor.updateOne({ _id: ObjectID(id) }, {
        $push: { "locations": location.id }
      })
      await VendorDashboardDraft.updateOne({ vendorId: ObjectID(id) }, {
        $push: { "locations": location.id }
      })
      return location ? location : {}
    }
  } catch (err) {
    throw new Error(`Error during creating a location from dashboard: ${err.message}`);
  }
}

export async function updateLocationDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let { input, locId } = args
  try {
    let vendor = await Vendor.findOne({ _id: ObjectID(id) });
    let {ableToPublish, reason} = await isAllowedToPublish(vendor, (vendor.locations.length));
    if (!ableToPublish)  throw new Error(`vendor.notAllowedToPublish.${reason}`);
    let locationUpdated = await Location.updateOne({ _id: ObjectID(locId) }, { $set: input });
    return locationUpdated.n > 0
  } catch (err) {
    throw new Error(`Error during updating a location from dashboard: ${err.message}`);
  }
}

export async function removeLocationFromVendorDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let { locId } = args
  try {
    let vendor = await Vendor.findOne({ _id: ObjectID(id) });
    let {ableToPublish, reason} = await isAllowedToPublish(vendor, (vendor.locations.length));
    if (!ableToPublish)  throw new Error(`vendor.notAllowedToPublish.${reason}`);
    let location = await Location.findOne({ _id: ObjectID(locId) });
    let locationUpdate
    if(location.vendors && location.vendors.length > 1) {
      locationUpdate = await Location.updateOne({ _id: ObjectID(locId) }, {
        $pull: { "vendors": id }
      })
    } else {
      locationUpdate = await Location.deleteOne({ _id: ObjectID(locId) })
    }
    let vendorUpdated = await Vendor.updateOne({ _id: ObjectID(id) }, {
      $pull: { "locations": locId }
    })
    await VendorDashboardDraft.updateOne({ vendorId: ObjectID(id) }, {
      $pull: { "locations": locId }
    })
    return vendorUpdated.n > 0 && locationUpdate.n > 0;
  } catch (err) {
    throw new Error(`Error during updating a location from dashboard: ${err.message}`);
  }
}

export async function allowedToCreateLocation(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  try {
    let vendor = await Vendor.findOne({ _id: ObjectID(id) });
    let vendorDraft = await VendorDashboardDraft.findOne({vendorId: ObjectID(id) }).lean()
    let {ableToPublish} = await isAllowedToPublish(vendor, vendorDraft.locations.length + 1);
    return ableToPublish;
  } catch (err) {
    throw new Error(`Error during check allow of vendor publishing: ${err.message}`);
  }
}


export async function createLocationAdminVendorDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if (role !== "admin") {
    throw new Error("token.adminRight");
  }
  let { input, vendorId } = args
  try {
    let vendor = await Vendor.findOne({ _id: ObjectID(vendorId) });
    let searchLocation = await Location.findOne(
      {
        "location.coordinates.0": { $gt: (input.location.coordinates[0] - 0.0001), $lt: (input.location.coordinates[0] + 0.0001) },
        "location.coordinates.1": { $gt: (input.location.coordinates[1] - 0.0001), $lt: (input.location.coordinates[1] + 0.0001) }
      });
    if (searchLocation) {
      if (!searchLocation.vendors.includes(id)) {
        let locationUpdated = await Location.findOneAndUpdate(
          { _id: ObjectID(searchLocation.id) },
          {
            $set: input,
            $push: { "vendors": vendorId }
          })
        await Vendor.updateOne({ _id: ObjectID(vendorId) }, {
          $push: { "locations": locationUpdated.id }
        })
        return locationUpdated;
      } else {
        let locationUpdated = await Location.findOneAndUpdate(
          { _id: ObjectID(searchLocation.id) },
          { $set: input })
        await Vendor.updateOne({ _id: ObjectID(vendorId) }, {
          $push: { "locations": locationUpdated.id }
        })
        return locationUpdated;
      }
    } else {
      let location = await Location.create({ ...input, vendors: [vendorId] });
      await Vendor.updateOne({ _id: ObjectID(vendorId) }, {
        $push: { "locations": location.id }
      })
      return location ? location : {}
    }
  } catch (err) {
    throw new Error(`Error during creating a location from admin vendor dashboard: ${err.message}`);
  }
}

export async function updateLocationAdminVendorDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if (role !== "admin") {
    throw new Error("token.adminRight");
  }
  let { input, locId } = args
  try {
    let locationUpdated = await Location.updateOne({ _id: ObjectID(locId) }, { $set: input });
    return locationUpdated.n > 0
  } catch (err) {
    throw new Error(`Error during updating a location from admin vendor dashboard: ${err.message}`);
  }
}

export async function removeLocationAdminVendorDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if (role !== "admin") {
    throw new Error("token.adminRight");
  }
  let { locId, vendorId } = args
  try {
    let vendor = await Vendor.findOne({ _id: ObjectID(vendorId) });
    let location = await Location.findOne({ _id: ObjectID(locId) });
    let locationUpdate
    if(location.vendors && location.vendors.length > 1) {
      locationUpdate = await Location.updateOne({ _id: ObjectID(locId) }, {
        $pull: { "vendors": vendorId }
      })
    } else {
      locationUpdate = await Location.deleteOne({ _id: ObjectID(locId) })
    }
    let vendorUpdated = await Vendor.updateOne({ _id: ObjectID(vendorId) }, {
      $pull: { "locations": locId }
    })
    return vendorUpdated.n > 0 && locationUpdate.n > 0;
  } catch (err) {
    throw new Error(`Error during updating a location from admin vendor dashboard: ${err.message}`);
  }
}