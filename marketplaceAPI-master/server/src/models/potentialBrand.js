import _ from 'lodash'
import PotentialBrand from '../schemas/potentialBrand'

import { checkToken } from '../modules/auth'

import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;

export async function potentialBrands(root, args, context, info) {
  let results = []
  try {
    results = await PotentialBrand.find({})
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving potential brands: ${err.message}`);
  }
}

export async function potentialBrand(root, args, context, info) {
  let result = {}
  let {id} = args;
  try {
    result = await PotentialBrand.findOne({_id: ObjectID(id)})
    return result ? result : {}
  } catch (err) {
    throw new Error(`Error during retrieving a potential brand: ${err.message}`);
  }
}

export async function createPotentialBrand(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  let {input} = args
  try {    
    if(role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let potentialBrand = await PotentialBrand.create({...input}
    );
    return potentialBrand;
  } catch (err) {
    throw new Error(`Error during potential brand creation: ${err.message}`);
    
  }
}

export async function updatePotentialBrand(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  let {input} = args
  try {    
    if(role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let potentialBrand = await PotentialBrand.updateOne(
      {_id: ObjectID(input.id)},
      { $set: input}
    );
    return !!potentialBrand;
  } catch (err) {
    throw new Error(`Error during potential brand update: ${err.message}`);
  }
}

export async function deletePotentialBrand(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  let {potentialBrandId} = args;
  try {
    if(role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let deleteQuery = await PotentialBrand.deleteOne({
      _id: ObjectID(potentialBrandId)
    });
    return deleteQuery.ok;
  } catch (err) {
    throw new Error(`Error during potential brand deletion: ${err.message}`);
  }
}


