import _ from 'lodash'
import Vendor from '../schemas/vendor'
import { checkToken } from '../modules/auth'
import { sendImageToOvh, removeImageToOvh } from '../modules/ovhObjectStorage'
import fs from 'fs';
import { format } from 'date-fns'

import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;

export async function adminVendorInfoUpdate(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  try {
    if(role !== "admin") {
      throw new Error("token.adminRight");
    }
    let {vendorId, input} = args
    let vendorUpdated = await Vendor.updateOne(
      { _id: ObjectID(vendorId) },
      {$set: input})
    return !!vendorUpdated.ok
  } catch (err) {
    throw new Error(`Error during updating vendor info admin: ${err.message}`);
  }
}

export async function adminVendorProfilePictureUpdate(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if(role !== "admin") {
    throw new Error("token.adminRight");
  }
  let { file, vendorId } = args
  try {

    let { profilePictureUrl } = await Vendor.findOne({ _id: ObjectID(vendorId) })
    let originalProfilePictureUrl = profilePictureUrl
    let { createReadStream, filename, mimetype } = await file

    if (!mimetype.includes('image/')) {
      throw new Error("vendor.invalidUpload");
    }

    let stream = createReadStream()

    let extension = filename.split('.').pop();

    let date = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")

    let path = `${vendorId}/profilePicture-${date.toString()}.${extension}`

    if (!fs.existsSync('upload')) {
      fs.mkdirSync('upload');
    }
    stream.pipe(fs.createWriteStream(`upload/${filename}`))
    let updatedFilePath = await sendImageToOvh(`upload/${filename}`, path)
    fs.unlinkSync(`upload/${filename}`);
    if (updatedFilePath) {
      let vendor = await Vendor.findOneAndUpdate({ _id: ObjectID(vendorId) }, { profilePictureUrl: updatedFilePath });
      if (!vendor) {
        throw new Error("vendor.uploadFail");
      } else {
        if (!!originalProfilePictureUrl) {
          await removeImageToOvh(originalProfilePictureUrl);
        }
      }
      return !!updatedFilePath;
    } else {
      throw new Error("vendor.uploadFail");
    }
  } catch (err) {
    throw new Error(`Error during admin vendor profile picture update: ${err.message}`);
  }
}

export async function adminVendorLogoUpdate(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if (role !== "admin") {
    throw new Error("token.adminRight");
  }
  let { file, vendorId } = args
  try {

    let { logoUrl } = await Vendor.findOne({ _id: ObjectID(vendorId) })
    let originalLogoUrl = logoUrl
    let { createReadStream, filename, mimetype } = await file

    if (!mimetype.includes('image/')) {
      throw new Error("vendor.invalidUpload");
    }

    let stream = createReadStream()

    let extension = filename.split('.').pop();

    let date = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")

    let path = `${vendorId}/logo-${date.toString()}.${extension}`

    if (!fs.existsSync('upload')) {
      fs.mkdirSync('upload');
    }
    stream.pipe(fs.createWriteStream(`upload/${filename}`))
    let updatedFilePath = await sendImageToOvh(`upload/${filename}`, path)
    fs.unlinkSync(`upload/${filename}`);
    if (updatedFilePath) {
      let vendor = await Vendor.findOneAndUpdate({ _id: ObjectID(vendorId) }, { logoUrl: updatedFilePath });
      if (!vendor) {
        throw new Error("vendor.uploadFail");
      } else {
        if (!!originalLogoUrl) {
          await removeImageToOvh(originalLogoUrl);
        }
      }
      return !!updatedFilePath;
    } else {
      throw new Error("vendor.uploadFail");
    }
  } catch (err) {
    throw new Error(`Error during vendor logo update: ${err.message}`);
  }
}