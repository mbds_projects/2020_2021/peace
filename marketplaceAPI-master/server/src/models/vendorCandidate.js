import _ from 'lodash'
import VendorCandidate from '../schemas/vendorCandidate'
import Vendor from '../schemas/vendor'

import { CONF } from '../config'
import Keyword from '../schemas/keyword'
import Category from '../schemas/category'
import { checkToken } from '../modules/auth'
import {sendInternalMail} from '../modules/internalMail'
import {getPriceId} from '../modules/util'

import { sendEmail, createContactBrand } from '../modules/sendinBlue'
import jwt from 'jsonwebtoken'

import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;

export async function vendorCandidate(root, args, context, info) {
  let result = {}
  let { id } = args;
  try {
    result = await VendorCandidate.findOne({ _id: ObjectID(id) })
      .populate('categories')
      .populate('keywords');

    return result ? result : {}
  } catch (err) {
    throw new Error(`Error during retrieving vendor candidate: ${err.message}`);
  }
}

export async function vendorsCandidate(root, args, context, info) {
  let result = {}
  try {
    result = await VendorCandidate.find({})
      .populate('categories')
      .populate('keywords');

    return result ? result : []
  } catch (err) {
    throw new Error(`Error during retrieving vendors candidates: ${err.message}`);
  }
}

export async function createVendorCandidate(root, args, context, info) {
  let { input } = args

  const { mail, subscriptionType, ...candidate } = input;
  var regexMail = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$')
  if (!regexMail.test(mail.toLowerCase() )) throw new Error("mail.malformed");
  try {
    let priceId = getPriceId(subscriptionType);

    let vendorCandidate = await VendorCandidate.create({
      ...candidate,
      mail: mail.toLowerCase(),
      subscriptionType,
      priceId
    });
    await createContactBrand(5, mail, vendorCandidate.name)

    /*await sendEmail(1, mail, { name: vendorCandidate.name });*/

    var mailOptions = {
      from: `${vendorCandidate.name} <${mail}>`,
      to: 'bonjour@super-responsable.org',
      subject: `CANDIDATURE ENSEIGNE - ${vendorCandidate.name}`,
      text: `L'enseigne ${vendorCandidate.name} souhaite être recensé. 
      Voici ses informations :
            nom du contact: ${vendorCandidate.contactName} ;
            abonnement: ${vendorCandidate.subscriptionType} ;
            mail : ${vendorCandidate.mail} ;
            téléphone : ${vendorCandidate.phoneNumber} ;
            site : ${vendorCandidate.site} ;
      --- répondre à ${mail}`,
      html: `<p>L'enseigne ${vendorCandidate.name} souhaite être recensé</p>
            <p>Voici ses informations :</p>
            nom du contact : ${vendorCandidate.contactName} <br/>
            abonnement: ${vendorCandidate.subscriptionType} <br/>
            mail : ${vendorCandidate.mail} <br/>
            téléphone : ${vendorCandidate.phoneNumber} <br/>
            site : ${vendorCandidate.site} <br/>
            <p>répondre à ${mail}</p>`
    };

    /*sendInternalMail(mailOptions)*/

    return !!vendorCandidate;
  } catch (err) {
    throw new Error(`Error during vendor candidate creation: ${err.message}`);

  }
}

export async function vendorsendinfo(root, args, context, info) {
  let { mail } = args
  let vendor = await VendorCandidate.findOne({ mail: mail.toLowerCase() });

      const token = jwt.sign({
        id: vendor.id,
        name: vendor.name
      }, CONF.secret);

      var urlWebApp = CONF.webAppUrl;

      var urlInfo = `${urlWebApp}vendorsendinfo/${token}`

      await sendEmail(1, mail, { urlInfo: urlInfo });
      return true;
    }

export async function followNewsletterDashboard(root, args, context, info) {
  let { input } = args
  const { mail, name } = input;
  var regexMail = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$')
  if (!regexMail.test(mail)) throw new Error("mail.malformed");
  try {
    await createContactBrand(12, mail, name)
    return true;
  } catch (err) {
    throw new Error(`Error during following newsletter of dashboard: ${err.message}`);
  }
}

export async function transformVendorCandidateToVendor(root, args, context, info) {
  const { id } = args;
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  try {
    if (role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let vendorCandidate = await VendorCandidate.findOne({ _id: ObjectID(id) });

    let { name, mail, categories, keywords, site, paymentMethodId, priceId, tvaIntra, from, subscriptionType } = vendorCandidate;
    let vendor = await Vendor.create({ name, mail: mail.toLowerCase(), categories, keywords, site, paymentMethodId, priceId, tvaIntra, from, subscriptionType  });
    let vendorCandidateUpdated = await VendorCandidate.updateOne(
      { _id: ObjectID(id) },
      { $set: { vendor: vendor.id } });
    return !!vendorCandidateUpdated;
  } catch (err) {
    throw new Error(`Error during transform a vendor candidate to a vendor : ${err.message}`);
  }
}

export async function vendorCandidateSaveInfo(root, args, context, info) {
    let {vendorId, input} = args
    let vendorUpdated = await VendorCandidate.findOneAndUpdate(
      { _id: ObjectID(vendorId) },
      {$set: input});

    await sendEmail(6, vendorUpdated.mail, { name: vendorUpdated.name })
    try{
      var mailOptions = {
        from: `${vendorUpdated.name} <support@super-responsable.org>`,
        to: 'janssenkoubikani@gmail.com',
        subject: 'Reponse au formulaire',
        text: `${vendorUpdated.name}  a répondu au formulaire `,
        html: `<h2>${vendorUpdated.name} a répondu au formulaire</h2>`
        };
        
        sendInternalMail(mailOptions)
    }
    catch (err) {
      throw new Error(`Error during transform a vendor candidate to a vendor : ${err.message}`);
    }
   
      
    return !!vendorUpdated
   
}
