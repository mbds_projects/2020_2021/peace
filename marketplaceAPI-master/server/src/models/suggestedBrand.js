import _ from 'lodash'
import SuggestedBrand from '../schemas/suggestedBrand'
import Vendor from '../schemas/vendor'
import User from '../schemas/user'
import Achievement from '../schemas/achievement'

import { checkToken } from '../modules/auth'

import { sendEmail, createContact } from '../modules/sendinBlue'
import { sendInternalMail } from '../modules/internalMail'

import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;

export async function suggestedBrands(root, args, context, info) {
  let results = []
  try {
    results = await SuggestedBrand.find({vendor: {$exists: false}}).populate('suggestedBy.user')
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving suggested brands: ${err.message}`);
  }
}

export async function suggestedBrand(root, args, context, info) {
  let result = {}
  let { id } = args;
  try {
    result = await SuggestedBrand.findOne({ _id: ObjectID(id) })
    return result ? result : {}
  } catch (err) {
    throw new Error(`Error during retrieving a suggested brand: ${err.message}`);
  }
}

export async function createSuggestedBrand(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { id } = logUser;
  let { input } = args
  try {
    let { name, message } = input;
    let suggestedBrand = await SuggestedBrand.findOneAndUpdate(
      { name: { $regex: name, $options: 'i' } },
      {
        $set: input,
        $push: { suggestedBy: { user: id, message: message } },
        $inc: { "occurrence": 1 }
      },
      { upsert: true, new: true }
    );

    let user = await User.findOne({_id: ObjectID(id)});
    var mailOptions = {
      from: `${user.name} ${user.surname} <${user.mail}>`,
      to: 'bonjour@super-responsable.org',
      subject: `SUGGESTION ENSEIGNE - ${name}`
    };
    if(suggestedBrand.occurrence === 1) {
      mailOptions.text = `${user.name} ${user.surname} a suggéré une nouvelle enseigne : "${name}. Son message: ${message}"`
      mailOptions.html = `<p>${user.name} ${user.surname}  a suggéré une nouvelle enseigne : "${name}"</p><p>Son message: ${message}</p>`
    }  else {
      mailOptions.text = `${user.name} ${user.surname} a suggéré l'enseigne : "${name}", elle a été suggéré ${suggestedBrand.occurrence} fois. Son message: ${message}`
      mailOptions.html = `<p>${user.name} ${user.surname} a suggéré l'enseigne : "${name}", elle a été suggéré ${suggestedBrand.occurrence} fois</p><p>Son message: ${message}</p>`

    }
    sendInternalMail(mailOptions);
    let countBrandSuggestedBy = await SuggestedBrand.count({ "suggestedBy.user": ObjectID(id) });
    if (countBrandSuggestedBy > 0) {
      let achievements = [];
      let achievementIdFirstSuggestion = await Achievement.findOne({ subtype: "first_suggestion" }, { id: 1, activated: 1 });
      if (achievementIdFirstSuggestion && !user.achievements.includes(achievementIdFirstSuggestion._id)) {
        achievements.push(achievementIdFirstSuggestion);
      }
      if (countBrandSuggestedBy >= 5) {
        let achievementIdFiveSuggestion = await Achievement.findOne({ subtype: "five_suggestion" }, { id: 1, activated: 1 });
        if (achievementIdFiveSuggestion && !user.achievements.includes(achievementIdFiveSuggestion._id)) {
          achievements.push(achievementIdFiveSuggestion);
        }
        if (countBrandSuggestedBy >= 10) {
          let achievementIdTenSuggestion = await Achievement.findOne({ subtype: "ten_suggestion" }, { id: 1, activated: 1 });
          if (achievementIdTenSuggestion && !user.achievements.includes(achievementIdTenSuggestion._id)) {
            achievements.push(achievementIdTenSuggestion);
          }
          if (countBrandSuggestedBy >= 20) {
            let achievementIdTwentySuggestion = await Achievement.findOne({ subtype: "twenty_suggestion" }, { id: 1, activated: 1 });
            if (achievementIdTwentySuggestion && !user.achievements.includes(achievementIdTwentySuggestion._id)) {
              achievements.push(achievementIdTwentySuggestion);
            }
            if (countBrandSuggestedBy >= 50) {
              let achievementIdFiftySuggestion = await Achievement.findOne({ subtype: "fifty_suggestion" }, { id: 1, activated: 1 });
              if (achievementIdFiftySuggestion && !user.achievements.includes(achievementIdFiftySuggestion._id)) {
                achievements.push(achievementIdFiftySuggestion);
              }
            }
          }
        }
      }
      achievements = _.uniq(achievements);

      user = await User.findOneAndUpdate(
        { _id: ObjectID(id) },
        { $push: { achievements: { $each: achievements.map(ac => ac._id) } } },
        { returnOriginal: false }
      );

      if (achievements.length > 0 && achievements.filter(ac => ac.activated === true).length > 0) {
        await sendEmail(4, user.mail, { firstname: user.name, lastname: user.surname });
      }
    }

    return suggestedBrand;
  } catch (err) {
    throw new Error(`Error during suggested brand creation: ${err.message}`);
  }
}

export async function createSuggestedBrandAdmin(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { input } = args
  try {
    if (role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let suggestedBrand = await SuggestedBrand.create({ ...input });
    return suggestedBrand;
  } catch (err) {
    throw new Error(`Error during suggested brand creation: ${err.message}`);

  }
}

export async function updateSuggestedBrand(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { input } = args
  try {
    if (role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let suggestedBrand = await SuggestedBrand.updateOne(
      { _id: ObjectID(input.id) },
      { $set: input }
    );
    return !!suggestedBrand;
  } catch (err) {
    throw new Error(`Error during suggested brand update: ${err.message}`);
  }
}

export async function deleteSuggestedBrand(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { suggestedBrandId } = args;
  try {
    if (role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let deleteQuery = await SuggestedBrand.deleteOne({
      _id: ObjectID(suggestedBrandId)
    });
    return deleteQuery.ok;
  } catch (err) {
    throw new Error(`Error during suggested brand deletion: ${err.message}`);
  }
}


export async function contactSuggestedBrand(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  const { mail, name, id } = args;
  var regexMail = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$')
  if (!regexMail.test(mail)) throw new Error("mail.malformed");
  try {
    if (role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    await createContact([10], mail, name, "", null, false)

    await sendEmail(6, mail, { name: name });

    let suggestedBrand = await SuggestedBrand.findOneAndUpdate(
      { _id: ObjectID(id) },
      { $inc: { "contact": 1 } }
    );

    return suggestedBrand;
  } catch (err) {
    throw new Error(`Error during contact suggested brand : ${err.message}`);
  }
}

export async function transformSuggestedBrandToVendor(root, args, context, info) {
  const { id } = args;
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  try {
    if (role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let brand = await SuggestedBrand.findOne({ _id: ObjectID(id) });

    let { name, mail, categories, keywords, site } = brand;
    let vendor = await Vendor.create({ name, mail: mail.toLowerCase() , categories, keywords, site });
    let brandUpdated = await SuggestedBrand.updateOne(
      { _id: ObjectID(id) },
      { $set: { vendor: vendor.id } });
    return !!brandUpdated;
  } catch (err) {
    throw new Error(`Error during transform a suggested brand to a vendor : ${err.message}`);
  }
}


