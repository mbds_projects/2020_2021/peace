import _ from 'lodash'
import Vendor from '../schemas/vendor'
import User from '../schemas/user'

import Comment from '../schemas/comment'
import Keyword from '../schemas/keyword'
import Category from '../schemas/category'
import Gender from '../schemas/gender'
import Label from '../schemas/label'
import Location from '../schemas/location'

import { checkToken, createAuthentJwt } from '../modules/auth'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import { CONF } from '../config'
import { sendEmail } from '../modules/sendinBlue'

import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;

const internalLimit = 3;


export async function vendorLogged(root, args, context, info) {
  let token = context.request.header.token
  let logVendor = await checkToken(token)
  let { id } = logVendor;
  try {
    let result = await Vendor.findOne({ _id: ObjectID(id) })
    return result
  } catch (err) {
    throw new Error(`Error during retrieving vendor logged: ${err.message}`);
  }
}

export async function vendorLimited(root, args, context, info) {
  let result = {}
  let { id } = args;
  try {
    result = await Vendor.findOne({ _id: ObjectID(id), published: true, activated: { $ne: false } })

    return result ? result : {}
  } catch (err) {
    throw new Error(`Error during retrieving vendor limited: ${err.message}`);
  }
}

export async function vendorLimitedAdmin(root, args, context, info) {
  let result = {}
  let { id } = args;
  try {
    result = await Vendor.findOne({ _id: ObjectID(id) })

    return result ? result : {}
  } catch (err) {
    throw new Error(`Error during retrieving vendor limited for admin: ${err.message}`);
  }
}

export async function profileVendorLimited(root, args, context, info) {
  let result = {}
  let { id } = args;
  try {
    result = await Vendor.findOne({ _id: ObjectID(id), published: true, activated: { $ne: false } })
      .populate('labels')
      .populate('locations')
      .populate('rewards')
    return result ? result : {}
  } catch (err) {
    throw new Error(`Error during retrieving profile vendor limited: ${err.message}`);
  }
}

export async function profileVendorLimitedAdmin(root, args, context, info) {
  let result = {}
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { id } = args;
  try {
    if (role !== "admin") {
      throw new Error("token.adminRight");
    }
    result = await Vendor.findOne({ _id: ObjectID(id) })
      .populate('categories')
      .populate('keywords')
      .populate('genders')
      .populate('labels')
      .populate('locations')
      .populate('rewards')

    return result ? result : {}
  } catch (err) {
    throw new Error(`Error during retrieving profile vendor limited admin: ${err.message}`);
  }
}

export async function profileVendorDashboard(root, args, context, info) {
  let result = {}
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  try {
    if (role !== "vendor") {
      throw new Error("token.vendorRight");
    }
    result = await Vendor.findOne({ _id: ObjectID(id) })
      .populate('labels')
      .populate('locations')
      .populate('rewards')

    return result ? result : {}
  } catch (err) {
    throw new Error(`Error during retrieving profile vendor limited admin: ${err.message}`);
  }
}


export async function updateInfoVendorDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  let { input } = args
  try {
    if (role !== "vendor") {
      throw new Error("token.vendorRight");
    }
    let vendorUpdated = await Vendor.updateOne(
      { _id: ObjectID(id) },
      { $set: input });
    return !!vendorUpdated
  } catch (err) {
    throw new Error(`Error during vendor info dashboard updating: ${err.message}`);
  }
}

export async function vendorsLimited(root, args, context, info) {
  let results = []
  try 
  {
    results = await Vendor.find({ published: true, activated: { $ne: false } })
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving vendors limited: ${err.message}`);
  }
}

export async function vendorsLimitedAdmin(root, args, context, info) {
  let results = []
  try {
    results = await Vendor.find();
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving vendors limited for admin: ${err.message}`);
  }
}

export async function vendorsMinimal(root, args, context, info) {
  let result = {}
  try {
    result = await Vendor.find({ published: true, activated: { $ne: false } })
    return result ? result : []
  } catch (err) {
    throw new Error(`Error during retrieving vendors minimal: ${err.message}`);
  }
}

export async function vendorsWithoutAccount(root, args, context, info) {
  let result = {}
  try {
    result = await Vendor.find({ hash: { $exists: false } })
    return result ? result : []
  } catch (err) {
    throw new Error(`Error during retrieving vendors without account: ${err.message}`);
  }
}

export async function vendorsMinimalAdmin(root, args, context, info) {
  let result = {}
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  try {
    if (role !== "admin") {
      throw new Error("token.adminRight");
    }
    result = await Vendor.find({})
    return result ? result : []
  } catch (err) {
    throw new Error(`Error during retrieving vendors minimal for admin: ${err.message}`);
  }
}

export async function latestVendorsAdded(root, args, context, info) {
  let result = {}
  try {
    result = await Vendor.find({ published: true, activated: { $ne: false } }, null, { limit: internalLimit, sort: { 'createdAt': -1, '_id': 1 } })
    return result ? result : []
  } catch (err) {
    throw new Error(`Error during retrieving latest vendors: ${err.message}`);
  }
}

export async function aroundPositionVendors(root, args, context, info) {
  let results = {}
  let { position, maxDistance, limit } = args;
  let { latitude, longitude } = position;

  let geometry = {
    type: "Point",
    coordinates: [latitude, longitude]
  }

  let nearSphere = maxDistance && maxDistance > 0 ? { $geometry: geometry, $maxDistance: maxDistance } : { $geometry: geometry }
  //let limitCalcul = withLimit ? {limit: limit * limit} : {}

  try {
    results = await Location.find({ location: { $nearSphere: nearSphere } })
      .populate({
        path: 'vendors'
      })


    const reducer = (accumulator, currentValue) => accumulator.concat(currentValue.vendors);

    let resultReduced = results.reduce(reducer, []);
    let locationVendorsUniq = _.uniq(resultReduced);
    locationVendorsUniq = locationVendorsUniq.filter(v => v.published === true)
    if (limit && limit > 0) locationVendorsUniq = locationVendorsUniq.slice(0, limit);

    
    return locationVendorsUniq ? locationVendorsUniq : []
  } catch (err) {
    throw new Error(`Error during retrieving vendors around position: ${err.message}`);
  }
}

export async function aroundPositionVendorsIdsByKms(root, args, context, info) {
  let results = []
  let { position } = args;
  let { latitude, longitude } = position;

  let geometry = {
    type: "Point",
    coordinates: [latitude, longitude]
  }

  let kms = [5000, 10000, 20000, 30000];

  try {
    for (let i = 0; i < kms.length; i++) {
      let nearSphere = { $geometry: geometry, $maxDistance: kms[i] }
      let locations = await Location.find({ location: { $nearSphere: nearSphere } }, { id: 1, vendors: 1 });
      const reducer = (accumulator, currentValue) => accumulator.concat(currentValue.vendors);
      let resultReduced = locations.reduce(reducer, []);
      let locationVendorsUniq = _.uniq(resultReduced.map(e => e.toString()));
      let vendorPublished = await Vendor.find({ published: true, activated: { $ne: false } }, { id: 1 });
      let vendorPublishedIds = vendorPublished.map(v => v._id.toString());
      let intersection = _.intersection(locationVendorsUniq, vendorPublishedIds);
      results.push(intersection);
    }
    return results;
  } catch (err) {
    throw new Error(`Error during retrieving vendors id around position group by kms: ${err.message}`);
  }
}

export async function onlyOnlineVendorsIds(root, args, context, info) {
  let results = {}
  let { limit } = args;

  try {
    results = await Vendor.find({ onlineShop: true, published: true, activated: { $ne: false } }, { id: 1 }, { limit: limit });
    return results ? results.map(e => e._id.toString()) : []
  } catch (err) {
    throw new Error(`Error during retrieving vendors online shop id: ${err.message}`);
  }
}

export async function bestRateVendors(root, args, context, info) {
  let results = []
  try {
    let query = [
      { $match: { published: true, activated: { $ne: false }, comments: {$exists: true}, rate: {$exists: true} }},
      { $addFields: {"id":"$_id", "__commentLength": { $size: "$comments" } } },
      { $sort: { "rate": -1, "__commentLength": -1 } },
      { $limit: internalLimit }
    ];
    results = await Vendor.aggregate(query);
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving average of rate of comments of vendor: ${err.message}`);
  }
}

export async function moreFavoritesVendors(root, args, context, info) {
  try {
    let resultUserFavorites = await User.aggregate([
      { $project: { favorites: 1 } },
      { $unwind: '$favorites' },
      {
        $group: {
          _id: { favorites: '$favorites' }
          , count: { $sum: 1 }
        }
      },
      { $sort: { 'count': -1 } }
    ]);

    let resultUserFavoritesObjectIds = resultUserFavorites.map((e) => ObjectID(e._id.favorites));

    let query = [
      { $match: { _id: { $in: resultUserFavoritesObjectIds }, published: true, activated: { $ne: false } } },
      { $addFields: { "id": "$_id", "__order": { $indexOfArray: [resultUserFavoritesObjectIds, "$_id"] } } },
      { $sort: { "__order": 1 } },
      { $limit: internalLimit }
    ];

    let vendors = await Vendor.aggregate(query);

    
    return vendors ? vendors : []
  } catch (err) {
    throw new Error(`Error during retrieving more favorites vendors: ${err.message}`);
  }
}

export async function vendorsAssimilate(root, args, context, info) {
  let results = []
  let { categories, keywords, vendorId, limit } = args;
  try {
    let query = {
      $or: [
        { categories: { $in: categories } },
        { keywords: { $in: keywords } }
      ],
      published: true,
      activated: {$ne:false}
    }


    if (vendorId) {
      query._id = { $ne: ObjectID(vendorId) }
    }
    let vendors = await Vendor.find(query)

    vendors = _.sortBy(vendors, function (vendor) {
      let compatibility = _.intersection(vendor.categories.map(c => c.toString()), categories.map(c => c.toString())).length + _.intersection(vendor.keywords.map(k => k.toString()), keywords.map(k => k.toString())).length;
      return compatibility;
    }).reverse();
    
    if(limit > 0) vendors = vendors.slice(0, limit)

    let objectIds = vendors.map(vendor => ObjectID(vendor.id));
    results = await Vendor.find({ _id: { $in: objectIds } })

    results.sort((vendor1, vendor2) => {
      let v1Rate = vendor1.rate ? vendor1.rate : 0;
      let v2Rate = vendor2.rate ? vendor2.rate : 0;
      return v2Rate - v1Rate;
    })
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving vendors assimilate: ${err.message}`);
  }
}

export async function publishVendor(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { vendorId } = args
  try {
    if (role !== "admin") {
      throw new Error("token.adminRight");
    }
    let vendorUpdated = await Vendor.updateOne(
      { _id: ObjectID(vendorId) },
      { $set: { published: true } })
    return !!vendorUpdated
  } catch (err) {
    throw new Error(`Error during vendor publishing: ${err.message}`);
  }
}

export async function unpublishVendor(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { vendorId } = args
  try {
    if (role !== "admin") {
      throw new Error("token.adminRight");
    }
    let vendorUpdated = await Vendor.updateOne(
      { _id: ObjectID(vendorId) },
      { $set: { published: false } })
    return !!vendorUpdated
  } catch (err) {
    throw new Error(`Error during vendor unpublishing: ${err.message}`);
  }
}

export async function updateInfoVendor(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { vendorId, input } = args
  try {
    if (role !== "admin") {
      throw new Error("token.adminRight");
    }

    let vendorUpdated = await Vendor.updateOne(
      { _id: ObjectID(vendorId) },
      { $set: input });
    return !!vendorUpdated
  } catch (err) {
    throw new Error(`Error during vendor info updating: ${err.message}`);
  }
}

export async function authenticateVendor(root, args, context, info) {
  let { mail, password } = args;
  try {
    let vendor = await Vendor.findOne({ mail: mail.toLowerCase() });
    let { hash } = vendor;


    if (bcrypt.compareSync(password, hash)) {
      const token = createAuthentJwt(vendor);
      return {
        token
      };
    } else {
      throw new Error("vendor.invalidCredential");
    }
  } catch (err) {
    throw new Error(`Error during vendor authentication: ${err.message}`);
  }
}

export async function createVendorPassword(root, args, context, info) {
  let { input } = args
  let { password, token } = input;
  let logVendor = await checkToken(token)
  let { id } = logVendor;
  try {
    var regexPwd = new RegExp("(?=.*[a-z])(?=.*[A-Z]).{8,}");
    if (!regexPwd.test(password)) throw new Error("password.malformed");
    let newHash = bcrypt.hashSync(password, 10);
    let vendor = await Vendor.findOneAndUpdate({ _id: ObjectID(id) }, { hash: newHash });
    if (vendor) {
      const token = createAuthentJwt(vendor);
      return {
        token
      };
    }
    throw new Error("error.unknown");
  } catch (err) {
    throw new Error(`Error during vendor password creation: ${err.message}`);
  }
}

export async function vendorsAssimilateMinimal(root, args, context, info) {
  let results = []
  let { keywords, labels, categories } = args;
  try {
    let query = {
      $or: [
        { categories: { $in: categories } },
        { labels: { $in: labels } },
        { keywords: { $in: keywords } }
      ],
      published: true,
      activated: {$ne:false}
    }
    let vendors = await Vendor.find(query)
    return results ? vendors : []
  } catch (err) {
    throw new Error(`Error during retrieving vendors assimilate minimal by keywords, categories, label: ${err.message}`);
  }
}