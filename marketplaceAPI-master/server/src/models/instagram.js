import _ from 'lodash'
import {registerSuggestedBrand} from '../models/instagramSuggestedBrand'

var rp = require('request-promise');

export async function instagramRecent(root, args, context, info) {
  try {
    let url = "http://super-responsable.org//wp-json/wp/v2/pages/994?_fields=content"
    return rp(url)
        .then(async function(res) {
          let insta = JSON.parse(res);
          let instaRes = insta.content.rendered;
          instaRes = instaRes.replace(/\n|\r/g, "");

          let arrayInsta = [];
          let isOlderThanLatestInstagramSuggestedBrand = false;
          while(instaRes.includes("<li class='Graph") && !isOlderThanLatestInstagramSuggestedBrand) {        
            let indexOfEnd = instaRes.indexOf('</li>');

            let background = instaRes.slice(instaRes.indexOf('url(') + 4, instaRes.indexOf(')'))
            
            let link = instaRes.slice(instaRes.indexOf("<a href='") + 9, instaRes.indexOf("' target='_blank'>"))
            
            arrayInsta.push({
                media: background,
                link: link
              })
            instaRes = instaRes.slice(indexOfEnd + 5);

          }

          await registerSuggestedBrand(arrayInsta.reverse(), true);
          return arrayInsta.reverse();
        })
        .catch(function(err) {
          throw new Error(`Error during retrieving instagram recent: ${err.message}`);
        })
  } catch (err) {
    throw new Error(`Error during retrieving instagram recent: ${err.message}`);
  }
}

export async function instagramPopular(root, args, context, info) {
  try {
    let url = "http://super-responsable.org//wp-json/wp/v2/pages/1147?_fields=content"
    return rp(url)
        .then(async function(res) {
          let insta = JSON.parse(res);
          let instaRes = insta.content.rendered;
          instaRes = instaRes.replace(/\n|\r/g, "");

          let arrayInsta = [];
          let isOlderThanLatestInstagramSuggestedBrand = false;
          while(instaRes.includes("<li class='Graph") && !isOlderThanLatestInstagramSuggestedBrand) {        
            let indexOfEnd = instaRes.indexOf('</li>');

            let background = instaRes.slice(instaRes.indexOf('url(') + 4, instaRes.indexOf(')'))
            
            let link = instaRes.slice(instaRes.indexOf("<a href='") + 9, instaRes.indexOf("' target='_blank'>"))
            
            arrayInsta.push({
                media: background,
                link: link
              })
            instaRes = instaRes.slice(indexOfEnd + 5);

          }

          await registerSuggestedBrand(arrayInsta.reverse(), true);
          return arrayInsta.reverse();
        })
        .catch(function(err) {
          throw new Error(`Error during retrieving instagram popular: ${err.message}`);
        })
  } catch (err) {
    throw new Error(`Error during retrieving instagram popular: ${err.message}`);
  }
}