import _ from 'lodash'
import CustomerView from '../schemas/customerView'

import { checkToken } from '../modules/auth'
import {sendInternalMail} from '../modules/internalMail'
import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;

export async function customerView(root, args, context, info) {
  let result = {}

  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {id} = logUser;

  try {
    result = await CustomerView.findOne({postedBy: ObjectID(id)})
    return !!result;
  } catch (err) {
    throw new Error(`Error during retrieving customer view: ${err.message}`);
  }
}


export async function createCustomerView(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {id} = logUser;
  let {input} = args;
  try {
    let customerView = await CustomerView.create({
      ...input,
      date: new Date(),
      postedBy: ObjectID(id)
    });

    customerView = await customerView.populate('postedBy').execPopulate()

    var mailOptions = {
      from: `${customerView.postedBy.name} ${customerView.postedBy.surname} <${customerView.postedBy.mail}>`,
      to: 'bonjour@super-responsable.org',
      subject: `AVIS PARTICULIER - ${customerView.postedBy.name} ${customerView.postedBy.surname}`,
      text: `Un avis d'un particulier à été envoyé : 
      Avis de : ${customerView.postedBy.name} ${customerView.postedBy.surname}; 
            Sa note: ${input.rate} / 5; 
            Son commentaire: ${input.text};
            --- Lui répondre: ${customerView.postedBy.mail}`,
      html: `<h1>Un avis d'un particulier à été envoyé:</h1>
            <p>Avis de : ${customerView.postedBy.name} ${customerView.postedBy.surname}<br/>
            Sa note: ${input.rate} / 5<br/>
            Son commentaire: ${input.text}<br/>
            Lui répondre: ${customerView.postedBy.mail}
            </p>`
    };

    sendInternalMail(mailOptions)

    return customerView ? customerView : {};
  } catch (err) {
    throw new Error(`Error during customer view creation: ${err.message}`);
  }
}


