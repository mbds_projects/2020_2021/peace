import _ from 'lodash'
import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;
import Label from '../schemas/label'
//import ImageLabel from '../schemas/imageLabel'
import { checkToken } from '../modules/auth'

import { sendImageToOvh, removeImageToOvh } from '../modules/ovhObjectStorage'
import fs from 'fs';
import { format} from 'date-fns'

export async function labels(root, args, context, info) {
  let results = {}
  try {
    results = await Label.find({})
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving labels: ${err.message}`);
  }
}

export async function updateLabelName(root, args, context, info)  {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  if(role !== "admin") {
    throw new Error("token.adminRight");
  }
  let {name, id} = args
  try {
    let labelUpdated = await Label.updateOne(
      {_id: ObjectID(id)},
      { $set: {name: name}});
    return !!labelUpdated
  } catch(e) {
    throw new Error(`Error during name update of label: ${err.message}`);
  }
}

export async function updateLogoLabel(root, args, context, info)  {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  if(role !== "admin") {
    throw new Error("token.adminRight");
  }
  let {file, id} = args
  try {
    let { createReadStream, filename, mimetype } = await file
    
    let { pictureUrl } = await Label.findOne({ _id: ObjectID(id) })
    let originalPictureUrl = {...pictureUrl}

    if(!mimetype.includes('image/')) {
      throw new Error("label.invalidUpload");
    }
    
    let stream = createReadStream()

    let extension = filename.split('.').pop();

    let date = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")

    let path = `/labels/${name}-${date.toString()}.${extension}`
    
    if (!fs.existsSync('upload')){
      fs.mkdirSync('upload');
    }
    stream.pipe(fs.createWriteStream(`upload/${filename}`))
    let updatedFilePath = await sendImageToOvh(`upload/${filename}`, path)
    fs.unlinkSync(`upload/${filename}`);
    if (updatedFilePath) {
      let updated = await Label.updateOne({_id: ObjectID(id)}, {$set:{ pictureUrl: updatedFilePath}});
      if(!updated.n > 0) {
        throw new Error("label.uploadFail");
      } else {
        if(originalPictureUrl) {
          await removeImageToOvh(originalPictureUrl);
        }
        return true;
      }
    } else {
      throw new Error("label.uploadFail");
    }
  } catch (err) {
    throw new Error(`Error during logo update of label: ${err.message}`);
  }
}

export async function createLabel(root, args, context, info)  {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  if(role !== "admin") {
    throw new Error("token.adminRight");
  }
  let {file, name} = args
  try {
    let { createReadStream, filename, mimetype } = await file
    
    if(!mimetype.includes('image/')) {
      throw new Error("label.invalidUpload");
    }
    
    let stream = createReadStream()

    let extension = filename.split('.').pop();

    let date = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")

    let path = `/labels/${name}-${date.toString()}.${extension}`
    
    if (!fs.existsSync('upload')){
      fs.mkdirSync('upload');
    }
    stream.pipe(fs.createWriteStream(`upload/${filename}`))
    let updatedFilePath = await sendImageToOvh(`upload/${filename}`, path)
    fs.unlinkSync(`upload/${filename}`);
    if (updatedFilePath) {
      let created = await Label.create({name: name, pictureUrl: updatedFilePath});
      return !!created;
    } else {
      throw new Error("label.uploadFail");
    }
  } catch (err) {
    throw new Error(`Error during label creation: ${err.message}`);
  }
}