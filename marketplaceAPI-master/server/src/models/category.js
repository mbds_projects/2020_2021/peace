import _ from 'lodash'
import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;
import Category from '../schemas/category'
import { checkToken } from '../modules/auth'

import { sendImageToOvh, removeImageToOvh } from '../modules/ovhObjectStorage'
import fs from 'fs';
import { format} from 'date-fns'

export async function categories(root, args, context, info) {
  let results = {}
  try {
    results = await Category.find({}, null, {sort: 'pos'})
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving categories: ${err.message}`);
  }
}

export async function categoriesUsed(root, args, context, info) {
  let results = {}
  try {
    results = await Category.find({used: true}, null, {sort: 'pos'})
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving categories used: ${err.message}`);
  }
}

export async function updateCategoryDesc(root, args, context, info)  {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  if(role !== "admin") {
    throw new Error("token.adminRight");
  }
  let {descFr, descEn, id} = args
  try {
    let lang = [
      {
        locale: "fr",
        value: descFr
      },
      {
        locale: "en-GB",
        value: descEn
      }
    ]
    let catUpdated = await Category.updateOne(
      {_id: ObjectID(id)},
      { $set: {lang: lang}});
    return !!catUpdated
  } catch(e) {
    throw new Error(`Error during desc update of cat: ${err.message}`);
  }
}

export async function updatePictureCategory(root, args, context, info)  {let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  if(role !== "admin") {
    throw new Error("token.adminRight");
  }
  let {file, id} = args
  try {
    let { createReadStream, filename, mimetype } = await file
    
    let { pictureUrl } = await Category.findOne({ _id: ObjectID(id) })
    let originalPictureUrl = pictureUrl.toString()

    if(!mimetype.includes('image/')) {
      throw new Error("category.invalidUpload");
    }
    
    let stream = createReadStream()

    let extension = filename.split('.').pop();

    let date = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")

    let path = `/categories/${id}-${date.toString()}.${extension}`
    
    if (!fs.existsSync('upload')){
      fs.mkdirSync('upload');
    }
    stream.pipe(fs.createWriteStream(`upload/${filename}`))
    let updatedFilePath = await sendImageToOvh(`upload/${filename}`, path)
    fs.unlinkSync(`upload/${filename}`);
    if (updatedFilePath) {
      let updated = await Category.updateOne({_id: ObjectID(id)}, {$set:{ pictureUrl: updatedFilePath}});
      if(!updated.n > 0) {
        throw new Error("category.uploadFail");
      } else {
        if(originalPictureUrl) {
          await removeImageToOvh(originalPictureUrl);
        }
        return true;
      }
    } else {
      throw new Error("category.uploadFail");
    }
  } catch (err) {
    throw new Error(`Error during picture update of category: ${err.message}`);
  }
}