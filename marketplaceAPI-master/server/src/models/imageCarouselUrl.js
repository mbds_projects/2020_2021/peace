import _ from 'lodash'
import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;
import ImageCarouselUrl from '../schemas/imageCarouselUrl'


export async function imagesCarouselUrl(root, args, context, info) {
  let results = {}
  try {
    results = await ImageCarouselUrl.find({}, null, {sort: 'pos'});
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving images url of carousel: ${err.message}`);
  }
}