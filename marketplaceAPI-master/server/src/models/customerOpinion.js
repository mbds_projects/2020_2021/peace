import _ from 'lodash'
import User from '../schemas/user'
import CustomerOpinion from '../schemas/customerOpinion'
import Achievement from '../schemas/achievement'
import { checkToken } from '../modules/auth'
import {sendInternalMail} from '../modules/internalMail'
import mongoose from 'mongoose';
import { sendEmail } from '../modules/sendinBlue'
const ObjectID = mongoose.Types.ObjectId;

export async function customerOpinions(root, args, context, info) {
  let results = []
  try {
    results = await CustomerOpinion.find()
    return results;
  } catch (err) {
    throw new Error(`Error during retrieving customer opinions: ${err.message}`);
  }
}

export async function createCustomerOpinion(root, args, context, info) {
  let isConnected = false;
  let user = null;
  let currentUser = null;
  try {
    let token = context.request.header.token
    let logUser = await checkToken(token)
    let {id} = logUser;
    isConnected = true;
    user = id;
  } catch(e) {

  }

  let {input} = args;
  try {
    let co = {
      ...input,
      date: new Date()
    }
    if(isConnected) {
      co.user = user;
    }
    let customerOpinion = await CustomerOpinion.create(co);

    let from = `${input.mail} <${input.mail}>`
    if(isConnected) {
      currentUser = await User.findOne({_id: ObjectID(user)})
      from =  `${currentUser.name} ${currentUser.surname} <${input.mail}>`
    }

    var mailOptions = {
      from,
      to: 'bonjour@super-responsable.org',
      subject: `AVIS PARTICULIER`,
      text: `Un avis d'un particulier à été envoyé : 
            Avis de : ${from}; 
            Sa note: ${input.rate} / 5; 
            Son commentaire: ${input.text};
            --- Lui répondre: ${input.mail}`,
      html: `<h1>Un avis d'un particulier à été envoyé:</h1>
            <p>Avis de : ${from}}<br/>
            Sa note: ${input.rate} / 5<br/>
            Son commentaire: ${input.text}<br/>
            Lui répondre: ${input.mail}
            </p>`
    };

    //todo add to user: customerOpinion Id

    sendInternalMail(mailOptions)
    if(!(isConnected)) {
      await sendEmail(7, input.mail,  {});
    } else {
      let achievementIdSiteOpinion = await Achievement.findOne({ subtype: "site_opinion" }, { id: 1, activated: 1 });

      if(!(currentUser.achievements.includes(achievementIdSiteOpinion._id)))
      {
        await User.updateOne(
          {_id: ObjectID(user)},
          {
            $push: { achievements: ObjectID(achievementIdSiteOpinion._id)}
          });
        await sendEmail(4, input.mail, {firstname: currentUser ? currentUser.name : '', lastname:currentUser ? currentUser.surname : ''});
      }
    }

    return customerOpinion ? customerOpinion : {};
  } catch (err) {
    throw new Error(`Error during customer opinion creation: ${err.message}`);
  }
}


