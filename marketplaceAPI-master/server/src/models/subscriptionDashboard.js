import _ from 'lodash'
import Vendor from '../schemas/vendor'
import { checkToken } from '../modules/auth'
import mongoose from 'mongoose';
import { sendInternalMail } from '../modules/internalMail'
import { CONF } from '../config'
import {getPriceId} from '../modules/util'
import { compareAsc } from 'date-fns'

const ObjectID = mongoose.Types.ObjectId;

const stripe = require('stripe')(CONF.stripe.sk);

export async function createSubscriptionDetailOfVendor(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let vendor = await Vendor.findOne({ _id: ObjectID(id) });
  let { paymentMethodId, from, tvaIntra, subscriptionType } = args;
  let priceId = getPriceId(subscriptionType);
  let vendorUpdated = await Vendor.updateOne(
    { _id: ObjectID(id) },
    { $set: { paymentMethodId, priceId, from, tvaIntra, subscriptionType }});
  return vendorUpdated.n > 0;

}

export async function createSubscriptionFromVendor(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }

  let vendor = await Vendor.findOne({ _id: ObjectID(id) });

  let { paymentMethodId, priceId, name, from, tvaIntra, subscriptionType, subscriptionDashboardId, createdAt } = vendor;
  let customerIdTmp;
  try {
    let { customerId } = vendor;
    customerIdTmp = customerId;
    if (!customerId) {
      let customer = await stripe.customers.create({
        email: vendor.mail,
        name: name,
        payment_method: paymentMethodId,
        description: `${vendor.name} dashboard subscription`
      });
      Vendor.updateOne({ _id: ObjectID(id) }, { $set: { customerId: customer.id } })
      await stripe.paymentMethods.attach(paymentMethodId, {
        customer: customer.id,
      });
      customerIdTmp = customer.id;
    } else {
      await stripe.paymentMethods.attach(paymentMethodId, {
        customer: customerId,
      });
    }

  } catch (error) {

    var mailOptions = {
      from: CONF.nodemailer.user,
      to: 'bonjour@super-responsable.org',
      subject: `ABONNEMENT ${subscriptionType}: Erreur`,
      text: `La marque ${vendor.name} n'a pas pu finaliser un abonnement ${subscriptionType}`,
      html: `<h2>La marque ${vendor.name} n'a pas pu finaliser un abonnement ${subscriptionType}</h2>`
    }
    sendInternalMail(mailOptions)
    throw error;
  }

  await stripe.customers.update(
    customerIdTmp,
    {
      invoice_settings: {
        default_payment_method: paymentMethodId,
      },
    }
  );

  // Create the subscription
  let options = {
    customer: customerIdTmp,
    items: [{ price: priceId }],
    expand: ['latest_invoice.payment_intent']
  }

  switch(from) {
    case 0:
      options.default_tax_rates = [CONF.stripe.taxes[0]]
      break;
    case 1:
      if(_.isEmpty(tvaIntra)) {
        options.default_tax_rates = [CONF.stripe.taxes[0]]
      } else {
        const taxRate = await stripe.taxRates.create({
          display_name: `TVA int. N°${tvaIntra} - art. 259-1 du CGI`,
          description: `TVA ${vendor.name}`,
          jurisdiction: 'UE',
          percentage: 0,
          inclusive: false,
        });
        options.default_tax_rates = [taxRate.id]
      }
      break;
    case 2:
      options.default_tax_rates = [CONF.stripe.taxes[1]]
      break;
  }

  if((compareAsc(new Date(createdAt), new Date("2020-11-26T00:00:00.0+00:00")) < 0) && !subscriptionDashboardId) {
    options.trial_period_days = 90
  }

  const subscription = await stripe.subscriptions.create(options);

  await Vendor.updateOne(
    { _id: ObjectID(id) },
    { $set: { subscriptionDashboardId: subscription.id, customerId: customerIdTmp } });

  if(subscriptionDashboardId) {
    try {
      await stripe.subscriptions.del(subscriptionDashboardId);
    } catch(e) {

    }
  }

  if (!!subscription) {
    var mailOptions = {
      from: CONF.nodemailer.user,
      to: 'bonjour@super-responsable.org',
      subject: `ABONNEMENT ${subscriptionType}: Réussi`,
      text: `La marque ${vendor.name} viens de valider un abonnement ${subscriptionType} (${subscription.id})`,
      html: `<h2>La marque ${vendor.name} viens de valider un abonnement ${subscriptionType} (${subscription.id})</h2>`
    }
    sendInternalMail(mailOptions)
  } else {
    var mailOptions = {
      from: CONF.nodemailer.user,
      to: 'bonjour@super-responsable.org',
      subject: `ABONNEMENT ${subscriptionType}: Erreur`,
      text: `La marque ${vendor.name} n'a pas pu finaliser un abonnement ${subscriptionType}`,
      html: `<h2>La marque ${vendor.name} n'a pas pu finaliser un abonnement ${subscriptionType}</h2>`
    }
    sendInternalMail(mailOptions)
  }

  return subscription;

}


export async function retryInvoice(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let { invoiceId, paymentMethodId } = args;

  try {
    let vendor = await Vendor.findOne({ _id: ObjectID(id) });
    let { customerId } = vendor;
    if(!paymentMethodId) {
      paymentMethodId = vendor.paymentMethodId
    }
    await stripe.paymentMethods.attach(paymentMethodId, {
      customer: customerId,
    });
    await stripe.customers.update(customerId, {
      invoice_settings: {
        default_payment_method: paymentMethodId,
      },
    });
  } catch (error) {
    // in case card_decline error
    return error;
  }

  const invoice = await stripe.invoices.retrieve(invoiceId, {
    expand: ['payment_intent'],
  });
  return invoice;
}

export async function retrieveDashboardSubscription(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  try {
    let vendor = await Vendor.findOne({ _id: ObjectID(id) });
    let { subscriptionDashboardId } = vendor;
    if (subscriptionDashboardId) {
      const subscription = await stripe.subscriptions.retrieve(subscriptionDashboardId, { expand: ['latest_invoice.payment_intent', 'pending_setup_intent', 'default_payment_method', 'items.data.price.product'] });
      let paymentMethod = {
        card: {
          brand: '',
          last4: ''
        }
      }
      if(subscription.latest_invoice.payment_intent && subscription.latest_invoice.payment_intent.payment_method) {
        paymentMethod = await stripe.paymentMethods.retrieve(subscription.latest_invoice.payment_intent.payment_method);
      }
      let result = {
        id: subscription.id,
        created: subscription.created,
        customer: subscription.customer,
        current_period_end: subscription.current_period_end,
        current_period_start: subscription.current_period_start,
        status: subscription.status,
        price: subscription.items.data[0].price.id,
        unit_amount: subscription.items.data[0].price.unit_amount,
        recurring: subscription.items.data[0].price.recurring.interval,
        currency: subscription.items.data[0].price.currency,
        latest_invoice: subscription.latest_invoice,
        pending_setup_intent: subscription.pending_setup_intent,
        payment_method: {
          brand: paymentMethod.card.brand,
          last4: paymentMethod.card.last4
        },
        product: {
          name: subscription.items.data[0].price.product.name
        }
      }
      return result;
    } else {
      return null;
    }
  } catch (error) {
    // in case card_decline error
    throw error;
  }
}


export async function cancelSubscription(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  try {
    let vendor = await Vendor.findOne({ _id: ObjectID(id) });
    let { subscriptionDashboardId } = vendor;
    const deletedSubscription = await stripe.subscriptions.del(subscriptionDashboardId);
    return deletedSubscription && deletedSubscription.status === "canceled";
  } catch (error) {
    throw error;
  }
}

export async function updateSubscription(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  try {
    let {subscriptionType, proration} = args;
    let priceId = getPriceId(subscriptionType);
    let vendor = await Vendor.findOne({ _id: ObjectID(id) });
    let { subscriptionDashboardId, customerId } = vendor;
    const subscriptionCurrent = await stripe.subscriptions.retrieve(subscriptionDashboardId);

    let items = [{
      id: subscriptionCurrent.items.data[0].id,
      price: priceId,
    }]

    let subscription = await stripe.subscriptions.update(subscriptionDashboardId, {
      cancel_at_period_end: false,
      proration_behavior: proration ? 'always_invoice' :  'none',
      items
    });

    let paymentMethod = {
      card: {
        brand: '',
        last4: ''
      }
    }

    if(subscription.latest_invoice.payment_intent && subscription.latest_invoice.payment_intent.payment_method) {
      paymentMethod = await stripe.paymentMethods.retrieve(subscription.latest_invoice.payment_intent.payment_method);
    }

    let result = {
      id: subscription.id,
      created: subscription.created,
      customer: subscription.customer,
      current_period_end: subscription.current_period_end,
      current_period_start: subscription.current_period_start,
      status: subscription.status,
      price: subscription.items.data[0].price.id,
      unit_amount: subscription.items.data[0].price.unit_amount,
      recurring: subscription.items.data[0].price.recurring.interval,
      currency: subscription.items.data[0].price.currency,
      latest_invoice: subscription.latest_invoice,
      pending_setup_intent: subscription.pending_setup_intent,
      payment_method: {
        brand: paymentMethod.brand,
        last4: paymentMethod.last4
      },
      product: {
        name: subscription.items.data[0].price.product.name
      }
    }
    let vendorUpdated = await Vendor.updateOne({_id: ObjectID(id)}, {$set: {subscriptionType}})
    if(vendorUpdated.n < 1) {
      let mailOptions = {
        from: CONF.nodemailer.user,
        to: 'bonjour@super-responsable.org',
        subject: `ABONNEMENT ${subscriptionType}: Erreur`,
        text: `La marque ${id} viens de n'a pas pu mettre à jour son abonnement pour ${subscriptionType} (vendeur non updaté) (${subscription.id})`,
        html: `<h2>La marque ${id} viens de n'a pas pu mettre à jour son abonnement pour ${subscriptionType} (vendeur non updaté) (${subscription.id})</h2>`
      }
      sendInternalMail(mailOptions)
      throw new Error('subscription.vendorUpdate')
    } else {
      if(paymentMethod.card.brand !== '' && (subscription.status === "active" || subscription.status === "trialing" )) {

        let mailOptions = {
          from: CONF.nodemailer.user,
          to: 'bonjour@super-responsable.org',
          subject: `ABONNEMENT ${subscriptionType}: Réussi`,
          text: `La marque ${id} viens de mettre à jour son abonnement pour ${subscriptionType} (${subscription.id})`,
          html: `<h2>La marque ${id} viens de valider son abonnement pour ${subscriptionType} (${subscription.id})</h2>`
        }
        sendInternalMail(mailOptions)
      } else {
        let mailOptions = {
          from: CONF.nodemailer.user,
          to: 'bonjour@super-responsable.org',
          subject: `ABONNEMENT ${subscriptionType}: En attente`,
          text: `La marque ${id} viens de tenter de mettre à jour son abonnement pour ${subscriptionType} (paiement en attente) (${subscription.id})`,
          html: `<h2>La marque ${id} viens de tenter de mettre à jour son abonnement pour ${subscriptionType} (paiement en attente)  (${subscription.id})</h2>`
        }
        sendInternalMail(mailOptions)
      }
    }

    return result;
  } catch (error) {
    throw error;
  }
}

export async function calculateProration(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  try {
    let {subscriptionType, proration} = args;
    let priceId = getPriceId(subscriptionType);
    let vendor = await Vendor.findOne({ _id: ObjectID(id) });
    let { subscriptionDashboardId, customerId } = vendor;
    const subscriptionCurrent = await stripe.subscriptions.retrieve(subscriptionDashboardId);

    let items = [{
      id: subscriptionCurrent.items.data[0].id,
      price: priceId,
    }]

    let finalPrice = 0;
    let price = 0;
    let finalTaxes = 0;

    if(proration) {
      const proration_date = Math.floor(Date.now() / 1000);

      const invoice = await stripe.invoices.retrieveUpcoming({
        customer: customerId,
        subscription_cancel_at_period_end: false,
        subscription: subscriptionDashboardId,
        subscription_items: items,
        subscription_proration_behavior: 'always_invoice',
        subscription_proration_date: proration_date,
      });

      finalPrice = invoice.total / 100;
      finalTaxes = invoice.tax / 100;
      price = invoice.lines.data[0].amount / 100
    }

    return {finalPrice, price,finalTaxes};
  } catch (error) {
    throw error;
  }
}