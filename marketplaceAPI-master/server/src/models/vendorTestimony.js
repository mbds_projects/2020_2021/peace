import _ from 'lodash'
import VendorTestimony from '../schemas/vendorTestimony'

export async function vendorTestimonies(root, args, context, info) {
  let results = []
  try {
    results = await VendorTestimony.find()
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving vendor testimonies: ${err.message}`);
  }
}
