import _ from 'lodash'
import Interview from '../schemas/interview'

export async function interviews(root, args, context, info) {
  let results = {}
  try {
    results = await Interview.find({}, null, {sort: '-date'})
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving interviews: ${err.message}`);
  }
}