import Reward from '../schemas/reward'
import Vendor from '../schemas/vendor'
import _ from 'lodash'
import mongoose from 'mongoose';
import { checkToken } from '../modules/auth'
const ObjectID = mongoose.Types.ObjectId;

export async function rewards(root, args, context, info) {
  let results = []
  try {
    let vendorsDisabled = await Vendor.find({ $or: [{ published: false}, {activated: false }]}, {id: 1})
    let vendorsDisabledIds = vendorsDisabled.map(v => v._id)
    results = await Reward.find({}).populate('vendor');
    let cleanedResult = results.filter(r => !vendorsDisabledIds.includes(r.vendor._id))
    return results ? cleanedResult : []
  } catch (err) {
    throw new Error(`Error during retrieving rewards: ${err.message}`);
  }
}

export async function rewardsOfVendor(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let results = []
  try {
    results = await Reward.find({vendor: ObjectID(id)}).populate('vendor');
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving rewards of logged vendor: ${err.message}`);
  }
}


export async function rewardsAdminVendor(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if (role !== "admin") {
    throw new Error("token.adminRight");
  }
  let { vendorId } = args
  let results = []
  try {
    results = await Reward.find({vendor: ObjectID(vendorId)}).populate('vendor');
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving rewards of admin vendor: ${err.message}`);
  }
}

export async function createReward(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  let { input } = args
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  try {
    let reward = await Reward.create({...input, vendor: id});
    await Vendor.updateOne(
      {_id:ObjectID(id)},
      {
        $push: { rewards: ObjectID(reward.id)}
      });
    return !!reward
  } catch (err) {
    throw new Error(`Error during creation of reward: ${err.message}`);
  }
}

export async function createRewardAdmin(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { input, vendorId } = args
  if (role !== "admin") {
    throw new Error("token.adminRight");
  }
  try {
    let reward = await Reward.create({...input, vendor: vendorId});
    await Vendor.updateOne(
      {_id:ObjectID(vendorId)},
      {
        $push: { rewards: ObjectID(reward.id)}
      });
    return !!reward
  } catch (err) {
    throw new Error(`Error during creation of reward admin: ${err.message}`);
  }
}

export async function disableReward(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { rewardId } = args
  if (role !== "vendor" && role !== "admin") {
    throw new Error("token.vendorRight");
  }
  try {
    let reward = await Reward.updateOne(
      {_id: ObjectID(rewardId)},
      { $set: {activated: false}});
    return !!reward
  } catch (err) {
    throw new Error(`Error during disabling of reward: ${err.message}`);
  }
}

export async function enableReward(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { rewardId } = args
  if (role !== "vendor" && role !== "admin") {
    throw new Error("token.vendorRight");
  }
  try {
    let reward = await Reward.updateOne(
      {_id: ObjectID(rewardId)},
      { $set: {activated: true}});
    return !!reward
  } catch (err) {
    throw new Error(`Error during enabling of reward: ${err.message}`);
  }
}


