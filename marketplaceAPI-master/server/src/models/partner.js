import _ from 'lodash'
import Partner from '../schemas/partner'

export async function partners(root, args, context, info) {
  let results = {}
  try {
    results = await Partner.find({})
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving partners: ${err.message}`);
  }
}