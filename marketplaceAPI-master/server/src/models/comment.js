import _ from 'lodash'
import Comment from '../schemas/comment'
import Vendor from '../schemas/vendor'
import User from '../schemas/user'
import Achievement from '../schemas/achievement'

import { checkToken } from '../modules/auth'
import mongoose from 'mongoose'

import { CONF } from '../config'
import {sendEmail} from '../modules/sendinBlue'

const limitCom = 3;
const ObjectID = mongoose.Types.ObjectId;

export async function commentsByVendor(root, args, context, info) {
  let result = {}

  let {vendorId, page} = args;
  let limit = limitCom;
  let skip = page * limit;
  try {
    result = await Comment.find({vendor: ObjectID(vendorId)}, null, {limit: limit, skip: skip, sort: '-date'})
    .populate('postedBy')
    .populate('vendor');
    return result ? result : []
  } catch (err) {
    throw new Error(`Error during retrieving comments of vendor: ${err.message}`);
  }
}

export async function bestComments(root, args, context, info) {
  let result = {}

  let limit = limitCom;
  try {
    result = await Comment.find({ rate: { $gte: 3 } }, null, {limit: limit}).sort([['date', 'desc'],['rate', 'desc']])
    .populate('postedBy')
    .populate('vendor');
    return result ? result : []
  } catch (err) {
    throw new Error(`Error during retrieving comments of vendor: ${err.message}`);
  }
}

export async function createComment(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {id} = logUser;
  let {input} = args;
  let {vendorId} = input;
  try {
    let comment = await Comment.create({
      ...input,
      vendor: ObjectID(vendorId),
      date: new Date(),
      postedBy: ObjectID(id)
    });

    comment = await comment.populate('postedBy').populate('vendor').execPopulate()

    let vendor = await Vendor.findOne({_id: ObjectID(vendorId)});

    let vendorCommentsLength = (vendor.comments && vendor.comments.length > 0 ? vendor.comments.length : 0)
    
    let rate = (((vendor.rate ? vendor.rate : 0) * vendorCommentsLength) + input.rate) / (vendorCommentsLength + 1)

    await Vendor.updateOne(
      {_id: ObjectID(vendorId)},
      { $set: {rate: rate},
        $push: { comments: ObjectID(comment.id)}
      });

      var urlWebApp = CONF.webAppUrl;
      var urlValidation = `${urlWebApp}vendors/${vendor.id}?name=${vendor.name}`

      await sendEmail(9, vendor.mail, { urlValidation: urlValidation });


    let user = await User.findOne({_id: ObjectID(id)});

    let countPostedBy = await Comment.count({"postedBy": ObjectID(id)});
    if(countPostedBy > 0) {
      let achievements = [];
      let achievementIdFirstRate= await Achievement.findOne({subtype: "first_rate"}, {id: 1, activated: 1});
      if(achievementIdFirstRate && !user.achievements.includes(achievementIdFirstRate._id)) {
        achievements.push(achievementIdFirstRate);
      }
      if(countPostedBy >= 5) {
        let achievementIdFiveRate = await Achievement.findOne({subtype: "five_rate"}, {id: 1, activated: 1});
        if(achievementIdFiveRate && !user.achievements.includes(achievementIdFiveRate._id)) {
          achievements.push(achievementIdFiveRate);
        }
        if(countPostedBy >= 10) {
          let achievementIdTenRate = await Achievement.findOne({subtype: "ten_rate"}, {id: 1, activated: 1});
          if(achievementIdTenRate && !user.achievements.includes(achievementIdTenRate._id)) {
            achievements.push(achievementIdTenRate);
          }
          if(countPostedBy >= 20) {
            let achievementIdTwentyRate = await Achievement.findOne({subtype: "twenty_rate"}, {id: 1, activated: 1});
            if(achievementIdTwentyRate && !user.achievements.includes(achievementIdTwentyRate._id)) {
              achievements.push(achievementIdTwentyRate);
            }
            if(countPostedBy >= 50) {
              let achievementIdFiftyRate = await Achievement.findOne({subtype: "fifty_rate"}, {id: 1, activated: 1});
              if(achievementIdFiftyRate && !user.achievements.includes(achievementIdFiftyRate._id)) {
                achievements.push(achievementIdFiftyRate);
              }
            }
          }
        }
      }
      achievements = _.uniq(achievements);
      
      user = await User.findOneAndUpdate(
        {_id: ObjectID(id)},
        {$push: { achievements: { $each: achievements.map(ac => ac._id)}}}, 
        {returnOriginal: false}
      );

      if(achievements.length > 0 && achievements.filter(ac => ac.activated === true).length > 0) {
        await sendEmail(4, user.mail, {firstname:user.name, lastname:user.surname});
      }
    }

    return comment ? comment : {};
  } catch (err) {
    throw new Error(`Error during comment creation: ${err.message}`);
  }
}

export async function reportComment(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {id} = logUser;
  let {commentId} = args;
  try {
    const res = await Comment.findOneAndUpdate({ _id: ObjectID(commentId) }, {$push: {"reportedBy": id}}, {returnOriginal: false})
                .populate('postedBy')
                .populate('vendor');
    return res;
  } catch(err) {
    throw new Error(`Error during reporting a comment: ${err.message}`);
  
  }
}


export async function commentsReported(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  try {
    if(role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    const comments = await Comment.find({reportedBy: { $exists: true, $not: {$size: 0} }})
    .populate('postedBy')
    .populate('vendor');
    return comments ? comments : [];
  } catch(err) {
    throw new Error(`Error during retrieving comments reported: ${err.message}`);
  
  }
}

export async function deleteComment(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  let {commentId} = args;
  try {
    if(role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let comment = await Comment.findOneAndDelete({
      _id: ObjectID(commentId)
    });

    let vendor = await Vendor.findOne({_id: ObjectID(comment.vendor)});

    let rate = ((vendor.rate * vendor.comments.length) - comment.rate) / (vendor.comments.length - 1)

    await Vendor.updateOne(
      {_id: ObjectID(vendor.id)},
      { $set: {rate: rate},
        $pull: { comments: ObjectID(comment.id)}
      });

    return true;
  } catch (err) {
    throw new Error(`Error during comment deletion: ${err.message}`);
  }
}

