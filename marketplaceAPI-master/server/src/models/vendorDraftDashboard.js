import _ from 'lodash'
import VendorDashboardDraft from '../schemas/vendorDashboardDraft'
import Vendor from '../schemas/vendor'
import { checkToken } from '../modules/auth'
import { CONF } from '../config'
import { sendImageToOvh, removeImageToOvh } from '../modules/ovhObjectStorage'
import fs from 'fs';
import { format} from 'date-fns'

import { isAllowedToPublish } from '../modules/vendor'
import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;

export async function vendorDraftDashboard(root, args, context, info) {
  let result = {}
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  try {
    if (role !== "vendor") {
      throw new Error("token.vendorRight");
    }
    result = await VendorDashboardDraft.findOne({ vendorId: ObjectID(id) })
      .populate('labels')
      .populate({
        path: 'labels',
        populate: {
          path: 'logo'
        }
      })
      .populate('locations')
      .populate('rewards')
    if(result) {
      return result;
    } else {
      let vendor = await Vendor.findOne({ _id: ObjectID(id) }).lean();
      if(vendor) {
        let newDashboardDraft = await VendorDashboardDraft.create({
          vendorId: ObjectID(id),
          ...vendor
        });
        if(newDashboardDraft) {
          return newDashboardDraft;
        } else {
          throw new Error(`Error during draft creation`);
        }
      } else {
        throw new Error(`Error during vendor retrieving`);
      }
    }
  } catch (err) {
    throw new Error(`Error during retrieving vendor dashboard draft: ${err.message}`);
  }
}

export async function updateInfoVendorDraftDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  let { input } = args
  try {
    if (role !== "vendor") {
      throw new Error("token.vendorRight");
    }
    let vendorUpdated = await VendorDashboardDraft.updateOne(
      { vendorId: ObjectID(id) },
      { $set: input });
    return !!vendorUpdated
  } catch (err) {
    throw new Error(`Error during vendor info draft dashboard updating: ${err.message}`);
  }
}

export async function updateCarouselPictureVendorDraftDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let { file, indexOfImage } = args
  try {
    if (indexOfImage > 2) {
      throw new Error("vendor.carouselIndexTooBig");
    }

    let { carouselPicturesUrl } = await VendorDashboardDraft.findOne({ vendorId: ObjectID(id) })
    let originalCarouselPicturesUrl = [...carouselPicturesUrl]
    let { createReadStream, filename, mimetype } = await file

    if (!mimetype.includes('image/')) {
      throw new Error("vendor.invalidUpload");
    }

    let stream = createReadStream()

    let extension = filename.split('.').pop();

    let date = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")

    let path = `${id}/carousel${indexOfImage}-${date.toString()}.${extension}`
    
    if (!fs.existsSync('upload')){
      fs.mkdirSync('upload');
    }
    stream.pipe(fs.createWriteStream(`upload/${filename}`))
    let updatedFilePath = await sendImageToOvh(`upload/${filename}`, path)
    fs.unlinkSync(`upload/${filename}`);
    if (updatedFilePath) {
      let carouselPicturesUrlTmp;
      if (carouselPicturesUrl) {
        carouselPicturesUrlTmp = carouselPicturesUrl;
        if (carouselPicturesUrl.length > indexOfImage) {
          carouselPicturesUrlTmp[indexOfImage] = updatedFilePath;
        } else {
          carouselPicturesUrlTmp.push(updatedFilePath);
        }
      } else {
        carouselPicturesUrlTmp = [updatedFilePath];
      }
      let vendor = await VendorDashboardDraft.findOneAndUpdate({ vendorId: ObjectID(id) }, { carouselPicturesUrl: carouselPicturesUrlTmp });
      if (!vendor) {
        throw new Error("vendor.uploadFail");
      } else {
        if(originalCarouselPicturesUrl && originalCarouselPicturesUrl.length > indexOfImage) {
          await removeImageToOvh(originalCarouselPicturesUrl[indexOfImage]);
        }
      }
      return !!updatedFilePath;
    } else {
      throw new Error("vendor.uploadFail");
    }
  } catch (err) {
    throw new Error(`Error during vendor carousel picture draft update: ${err.message}`);
  }
}

export async function updateFeaturedProductPictureVendorDraftDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let { file, indexOfImage, link, price } = args
  try {
    if (indexOfImage > 5) {
      throw new Error("vendor.featuredProductIndexTooBig");
    }

    let { featuredProductsUrl } = await VendorDashboardDraft.findOne({ vendorId: ObjectID(id) })
    let originalFeaturedProductsUrl = [...featuredProductsUrl]
    let { createReadStream, filename, mimetype } = await file

    if (!mimetype.includes('image/')) {
      throw new Error("vendor.invalidUpload");
    }

    let stream = createReadStream()

    let extension = filename.split('.').pop();

    let date = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")

    let path = `${id}/featuredProduct${indexOfImage}-${date.toString()}.${extension}`

    if (!fs.existsSync('upload')){
      fs.mkdirSync('upload');
    }
    stream.pipe(fs.createWriteStream(`upload/${filename}`))
    let updatedFilePath = await sendImageToOvh(`upload/${filename}`, path)
    fs.unlinkSync(`upload/${filename}`);
    if (updatedFilePath) {
      let featuredProductsUrlTmp;
      if (featuredProductsUrl) {
        featuredProductsUrlTmp = featuredProductsUrl;
        if (featuredProductsUrl.length > indexOfImage) {
          featuredProductsUrlTmp[indexOfImage] = {
            imgUrl: updatedFilePath,
            link,
            price
          };
        } else {
          featuredProductsUrlTmp.push({
            imgUrl: updatedFilePath,
            link,
            price
          });
        }
      } else {
        featuredProductsUrlTmp = [{
          imgUrl: updatedFilePath,
          link,
          price
        }];
      }
      let vendor = await VendorDashboardDraft.updateOne({ vendorId: ObjectID(id) },
        { $set: { featuredProductsUrl: featuredProductsUrlTmp } });
      
      if (!vendor.n > 0) {
        throw new Error("vendor.uploadFail");
      } else {
        if(originalFeaturedProductsUrl && originalFeaturedProductsUrl.length > indexOfImage) {
          await removeImageToOvh(originalFeaturedProductsUrl[indexOfImage].imgUrl);
        }
        return true;
      }
    } else {
      throw new Error("vendor.uploadFail");
    }
  } catch (err) {
    throw new Error(`Error during vendor featured product picture draft update: ${err.message}`);
  }
}


export async function updateFeaturedProductInfoVendorDraftDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let { indexOfProduct, link, price } = args
  try {
    if (indexOfProduct > 5) {
      throw new Error("vendor.featuredProductIndexTooBig");
    }

    let { featuredProductsUrl } = await VendorDashboardDraft.findOne({ vendorId: ObjectID(id) })

    let featuredProductsUrlTmp;
    if (featuredProductsUrl) {
      featuredProductsUrlTmp = featuredProductsUrl;
      if (featuredProductsUrl.length > indexOfProduct) {
        featuredProductsUrlTmp[indexOfProduct] = {
          imgUrl: featuredProductsUrl[indexOfProduct].imgUrl,
          link,
          price
        };
      } else {
        throw new Error("vendor.featuredProduct.notExist");
      }
    } else {
      throw new Error("vendor.featuredProduct.notExist");
    }
    let vendor = await VendorDashboardDraft.updateOne({ vendorId: ObjectID(id) },
      { $set: { featuredProductsUrl: featuredProductsUrlTmp } });
    if (!vendor.n > 0) {
      throw new Error("vendor.featuredProduct.editFail");
    } else {
      return true;
    }
  } catch (err) {
    throw new Error(`Error during vendor featured product info draft update: ${err.message}`);
  }
}

export async function publishDraftDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  try {
    let vendor = await Vendor.findOne({ _id: ObjectID(id) });
    let vendorDraft = await VendorDashboardDraft.findOne({vendorId: ObjectID(id) }).lean()
    let {ableToPublish, reason} = await isAllowedToPublish(vendor, vendorDraft.locations.length);
    if (ableToPublish) {
      let vendorUpdated = await Vendor.updateOne({ _id: ObjectID(id) },
        { $set: { published: true, ...vendorDraft } });
      if (!vendorUpdated.n > 0) {
        throw new Error("vendor.publishFail");
      } else {
        let diffCarousel =  _.difference(vendor.carouselPicturesUrl, vendorDraft.carouselPicturesUrl);
        if(diffCarousel.length > 0) {
          for(let i = 0; i < diffCarousel.length; i++) {
            await removeImageToOvh(diffCarousel[i])
          }
        }
        let diffFeaturedProducts =  _.differenceBy(vendor.featuredProductsUrl, vendorDraft.featuredProductsUrl, 'imgUrl');
        if(diffFeaturedProducts.length > 0) {
          for(let i = 0; i < diffFeaturedProducts.length; i++) {
            await removeImageToOvh(diffFeaturedProducts[i].imgUrl)
          }
        }
        return true;
      }
    }
    else {
      throw new Error(`vendor.notAllowedToPublish.${reason}`);
    }
  } catch (err) {
    throw new Error(`Error during vendor draft publishing: ${err.message}`);
  }
}