import _ from 'lodash'
import { CONF } from '../config'

var rp = require('request-promise');

let limit = 3
export async function youtubeRecent(root, args, context, info) {
  try {
    let url = `https://www.googleapis.com/youtube/v3/search?key=${CONF.youtubeAPIKey}&channelId=UC4jKsBxGE6v2YgNfgWMSBNg&part=snippet,id&order=date&maxResults=${limit}`
    return rp(url)
        .then(async function(res) {
          let items = JSON.parse(res).items;
          let results = items.map((item) => {
            let link = `https://www.youtube.com/watch?v=${item.id.videoId}`
            let media = item.snippet.thumbnails.high.url;
            let title = item.snippet.title
            return {link,media, title}
          });
          return results;
        })
        .catch(function(err) {
          throw new Error(`Error during retrieving youtube recent: ${err.message}`);
        })
  } catch (err) {
    throw new Error(`Error during retrieving youtube recent: ${err.message}`);
  }
}