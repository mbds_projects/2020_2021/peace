import _ from 'lodash'
import User from '../schemas/user'
import Vendor from '../schemas/vendor'
import Comment from '../schemas/comment'
import Achievement from '../schemas/achievement'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import { CONF } from '../config'
import { sendEmail, createContact, updateContact } from '../modules/sendinBlue'
import { checkToken, createAuthentJwt } from '../modules/auth'
import CustomerOpinion from '../schemas/customerOpinion'
import mongoose from 'mongoose';
import { sendImageToOvh, removeImageToOvh } from '../modules/ovhObjectStorage'
import fs from 'fs';
import { format } from 'date-fns'

const ObjectID = mongoose.Types.ObjectId;

var rp = require('request-promise');

export async function user(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { id } = logUser;
  try {
    let result = await User.findOne({ _id: ObjectID(id) }).populate('keywords').populate('achievements')
    return result
  } catch (err) {
    throw new Error(`Error during retrieving user: ${err.message}`);
  }
}

export async function userFavorites(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { id } = logUser;
  try {
    let results = await User.findOne({ _id: ObjectID(id) }).populate('favorites');
    return results && results.favorites ? results.favorites : []
  } catch (err) {
    throw new Error(`Error during retrieving favorites: ${err.message}`);
  }
}

export async function updateUser(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { id } = logUser;
  let { input } = args;
  try {
    let user = await User.findOneAndUpdate({ _id: ObjectID(id) }, input, { returnOriginal: false })
    if (user) {
      let achievements = [];
      //let achievementsActivated = await Achievement.find({activated: true}, {id: 1}).map(ac => ac._id);
      if (user.newsletter) {
        let achievementIdNewsletter = await Achievement.findOne({ subtype: "newsletter" }, { id: 1, activated: 1 });
        if (achievementIdNewsletter && !user.achievements.includes(achievementIdNewsletter._id)) {
          achievements.push(achievementIdNewsletter);
        }
      }
      if (user.favorites && user.favorites.length > 0) {
        let achievementIdFirstFavorite = await Achievement.findOne({ subtype: "first_favorite" }, { id: 1, activated: 1 });
        if (achievementIdFirstFavorite && !user.achievements.includes(achievementIdFirstFavorite._id)) {
          achievements.push(achievementIdFirstFavorite);
        }
        if (user.favorites.length >= 5) {
          let achievementIdFiveFavorite = await Achievement.findOne({ subtype: "five_favorite" }, { id: 1, activated: 1 });
          if (achievementIdFiveFavorite && !user.achievements.includes(achievementIdFiveFavorite._id)) {
            achievements.push(achievementIdFiveFavorite);
          }
        }
      }
      let completion = 0;
      if (!!user.keywords && !!user.keywords.length > 0) {
        if (user.keywords.length >= 5) { completion += 50 }
        else {
          completion += user.keywords.length * 10;
        }
      }
      if (!!user.profilePictureUrl) { completion += 25; }
      if (!!user.birthday) { completion += 25; }
      if (completion === 100) {
        let achievementIdCompleteProfile = await Achievement.findOne({ subtype: "complete_profile" }, { id: 1, activated: 1 });
        if (achievementIdCompleteProfile && !user.achievements.includes(achievementIdCompleteProfile._id)) {
          achievements.push(achievementIdCompleteProfile);
        }
      }
      achievements = _.uniq(achievements);

      user = await User.findOneAndUpdate(
        { _id: ObjectID(id) },
        { $push: { achievements: { $each: achievements.map(ac => ac._id) } } },
        { returnOriginal: false }
      ).populate('keywords').populate('achievements');

      if (achievements.length > 0 && achievements.filter(ac => ac.activated === true).length > 0) {
        await sendEmail(4, user.mail, { firstname: user.name, lastname: user.surname });
      }

      if (input.hasOwnProperty('newsletter') || input.hasOwnProperty('birthday') || input.hasOwnProperty('keywords')) {
        let keywordsName = user.keywords.map((el) => el.name);
        await updateContact(4, user.mail, user.birthday, user.newsletter, keywordsName)
      }

      return user
    }
  } catch (err) {
    throw new Error(`Error during update user: ${err.message}`);
  }
}

export async function updatePasswordUser(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { id } = logUser;
  let { input } = args
  let { password, newPassword } = input;
  try {
    let user = await User.findOne({ _id: ObjectID(id) });
    let { hash } = user;

    if (bcrypt.compareSync(password, hash)) {
      var regexPwd = new RegExp("(?=.*[a-z])(?=.*[A-Z]).{8,}");
      if (!regexPwd.test(newPassword)) throw new Error("password.new.malformed");
      let newHash = bcrypt.hashSync(newPassword, 10);
      let user = await User.findOneAndUpdate({ _id: ObjectID(id) }, { hash: newHash });
      if (user) {
        return true;
      }
      throw new Error("error.unknown");
    } else {
      throw new Error("password.current.wrong");
    }
  } catch (err) {
    throw new Error(`Error during user password update: ${err.message}`);
  }
}

export async function updateForgottenPasswordUser(root, args, context, info) {
  let { input } = args
  let { password, token } = input;
  let logUser = await checkToken(token)
  let { id } = logUser;
  try {

    var regexPwd = new RegExp("(?=.*[a-z])(?=.*[A-Z]).{8,}");
    if (!regexPwd.test(password)) throw new Error("password.malformed");
    let newHash = bcrypt.hashSync(password, 10);
    let user = await User.findOneAndUpdate({ _id: ObjectID(id) }, { hash: newHash });
    if (user) {
      const token = createAuthentJwt(user);
      return {
        token
      };
    }
    throw new Error("error.unknown");
  } catch (err) {
    throw new Error(`Error during user password forgotten update: ${err.message}`);
  }
}

export async function createUserFacebook(root, args, context, info) {
  //let result = {}
  let { input } = args

  const { mail, facebookID, facebookAccessToken, ...restrictedUser } = input;

  var regexMail = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$')
  if (!regexMail.test(mail.toLowerCase() )) throw new Error("mail.malformed");
  try {

    let user = null;

    try {
      let fbAccess = await rp(`https://graph.facebook.com/me?access_token=${facebookAccessToken}`)
      if (fbAccess) {
        let userFb = await User.findOne({ facebookId: facebookID });
        if (!!userFb) {
          throw new Error("facebook.alreadyExist");
        } else {

          let achievements = [];
          let achievementIdRegistration = await Achievement.findOne({ subtype: "registration" }, { id: 1 });
          achievements.push(achievementIdRegistration._id)

          let customerOpinionMail = await CustomerOpinion.findOne({ mail: mail });
          if (customerOpinionMail) {
            let achievementIdSiteOpinion = await Achievement.findOne({ subtype: "site_opinion" }, { id: 1, activated: 1 });
            achievements.push(achievementIdSiteOpinion._id)
          }
          user = await User.create({
            ...restrictedUser,
            mail: mail.toLowerCase() ,
            defaultColor: '#' + Math.floor(Math.random() * 16777215).toString(16),
            facebookId: facebookID,
            achievements: achievements
          });

          await sendEmail(8, mail, { firstname: restrictedUser.name, lastname: restrictedUser.surname });

          let listIds = [4]
          if (restrictedUser.newsletter) listIds = [4, 14]
          await createContact(listIds, mail, restrictedUser.name, restrictedUser.surname, restrictedUser.birthday, restrictedUser.newsletter)

          const optionsStart = {
            uri: `https://graph.facebook.com/${facebookID}/picture?type=large`,
            method: "GET",
            encoding: null,
            headers: {
              "Content-type": "application/jpeg"
            }
          };

          try {
            let fbPicture = await rp(optionsStart);
            if (fbPicture) {
              if (!fs.existsSync('upload')) {
                fs.mkdirSync('upload');
              }
              let file = fs.writeFileSync(`upload/${user.id}_profilePictureFb.jpeg`, fbPicture);
              if(fs.existsSync(`upload/${user.id}_profilePictureFb.jpeg`)) {
                let updatedFilePath = await sendImageToOvh(`upload/${user.id}_profilePictureFb.jpeg`, `${user.id}_profilePictureFb.jpeg`)
                if (updatedFilePath) {
                  await User.updateOne({ _id: ObjectID(user.id) }, { $set: { profilePictureUrl: updatedFilePath } });
                }
                fs.unlinkSync(`upload/${user.id}_profilePictureFb.jpeg`);
              }
            }
          } catch (e) {
          }

          const token = createAuthentJwt(user);

          return {
            token
          };
        }
      }
    } catch (e) {
      throw new Error(`Error during user facebook authentication: ${err.message}`);
    }
  } catch (err) {
    if (err.errors && err.errors.hasOwnProperty("mail") && err.errors.mail.kind === "unique") {
      throw new Error("mail.alreadyExist");
    }
    if (err.errors && err.errors.hasOwnProperty("facebookId") && err.errors.facebookId.kind === "unique") {
      throw new Error("facebook.alreadyExist");
    }
    throw new Error(`Error during user creation: ${err.message}`);

  }
}

export async function createUser(root, args, context, info) {       
  let { input } = args

  const { password, mail, ...restrictedUser } = input;

  var regexMail = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$')
  if (!regexMail.test(mail.toLowerCase() )) throw new Error("mail.malformed");
  try {
    let user = null;

    var regexPwd = new RegExp("(?=.*[a-z])(?=.*[A-Z]).{8,}");
    if (!regexPwd.test(password)) throw new Error("password.malformed");
    let achievements = [];
    let achievementIdRegistration = await Achievement.findOne({ subtype: "registration" }, { id: 1 });
    achievements.push(achievementIdRegistration._id)

    let customerOpinionMail = await CustomerOpinion.findOne({ mail: mail });
    if (customerOpinionMail) {
      let achievementIdSiteOpinion = await Achievement.findOne({ subtype: "site_opinion" }, { id: 1, activated: 1 });
      achievements.push(achievementIdSiteOpinion._id)
    }

    user = await User.create({
      ...restrictedUser,
      mail: mail.toLowerCase() ,
      defaultColor: '#' + Math.floor(Math.random() * 16777215).toString(16),
      hash: bcrypt.hashSync(password, 10),
      achievements: achievements
    });

    await sendEmail(8, mail, { firstname: restrictedUser.name, lastname: restrictedUser.surname });
    let listIds = [4]
    if (restrictedUser.newsletter) listIds = [4, 14]
    await createContact(listIds, mail, restrictedUser.name, restrictedUser.surname, restrictedUser.birthday, restrictedUser.newsletter)
    const token = createAuthentJwt(user);

    return {
      token
    };
  } catch (err) {
    if (err.errors && err.errors.hasOwnProperty("mail") && err.errors.mail.kind === "unique") {
      throw new Error("mail.alreadyExist");
    }
    if (err.errors && err.errors.hasOwnProperty("facebookId") && err.errors.facebookId.kind === "unique") {
      throw new Error("facebook.alreadyExist");
    }
    throw new Error(`Error during user creation: ${err.message}`);

  }
}


export async function forgotPassword(root, args, context, info) {
  let { mail } = args

  try {

    let user = await User.findOne({ mail: mail.toLowerCase() });
    if (user) {
      if (user.facebookId && user.facebookId !== "") {
        throw new Error("password.facebook")
      }

      var timeAuth = '30m';
      const TKN_AUTH = {
        expiresIn: timeAuth
      }

      const token = jwt.sign({
        id: user.id
      }, CONF.secret, TKN_AUTH);

      var urlWebApp = CONF.webAppUrl;

      var urlValidation = `${urlWebApp}createForgottenPassword/${token}`

      await sendEmail(5, mail, { urlValidation: urlValidation });


      return true;

    } else {
      throw new Error("mail.notFound");
    }

  } catch (err) {
    throw new Error(`Error during password forgotten request: ${err.message}`);

  }
}

export async function authenticate(root, args, context, info) {
  let { mail, password } = args;
  try {
    let user = await User.findOne({ mail: mail.toLowerCase()  });
    if (!user) {
      throw new Error("user.notExist")
    }
    if ((!user['hash'] || user['hash'] == '') && (user['facebookId'] && user['facebookId'] !== '')) {
      throw new Error("user.facebookAccount")
    }
    let { hash } = user;

    if (!!user.blockedSince) {
      throw new Error("user.blocked");
    }

    if (bcrypt.compareSync(password, hash)) {
      const token = createAuthentJwt(user);
      return {
        token
      };
    } else {
      throw new Error("user.invalidCredential");
    }


  } catch (err) {
    throw new Error(`Error during user authentication: ${err.message}`);
  }
}

export async function isFacebookAccountRegistered(root, args, context, info) {
  let { userID } = args;
  let user = await User.findOne({ facebookId: userID });
  return !!user;
}

export async function facebookAuthenticate(root, args, context, info) {
  let { userID, accessToken } = args;
  return rp(`https://graph.facebook.com/me?access_token=${accessToken}`)
    .then(async function () {
      let user = await User.findOne({ facebookId: userID });
      if (!user) {
        throw new Error("user.notExist")
      }
      if ((user['facebookId'] && user['facebookId'] !== '') && (!user['hash'] || user['hash'] == '')) {
        if (user['facebookId'] === userID) {
          const token = createAuthentJwt(user);
          return {
            token
          };
        } else {
          throw new Error("user.otherFacebookAccount")
        }
      } else {
        throw new Error("user.internalAccount")
      }
    })
    .catch(function (err) {
      throw new Error(`Error during user facebook authentication: ${err.message}`);
    })
}

export async function updateProfilePictureUser(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { id } = logUser;
  let { file } = args
  try {
    let { profilePictureUrl } = await User.findOne({ _id: ObjectID(id) })
    let originalProfilePictureUrl = profilePictureUrl
    let { createReadStream, filename, mimetype } = await file

    if (!mimetype.includes('image/')) {
      throw new Error("user.invalidUpload");
    }

    let stream = createReadStream()

    let extension = filename.split('.').pop();

    let date = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")

    let path = `users/${id}-${date.toString()}.${extension}`

    if (!fs.existsSync('upload')) {
      fs.mkdirSync('upload');
    }
    stream.pipe(fs.createWriteStream(`upload/${filename}`))
    let updatedFilePath = await sendImageToOvh(`upload/${filename}`, path)
    fs.unlinkSync(`upload/${filename}`);
    if (updatedFilePath) {
      let user = await User.findOneAndUpdate({ _id: ObjectID(id) }, { profilePictureUrl: updatedFilePath });
      if (!user) {
        throw new Error("user.uploadFail");
      } else {
        if (!!originalProfilePictureUrl) {
          await removeImageToOvh(originalProfilePictureUrl);
        }
      }
    } else {
      throw new Error("user.uploadFail");
    }
    return updatedFilePath
  } catch (err) {
    throw new Error(`Error during user picture update: ${err.message}`);
  }
}

export async function userComments(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { id } = logUser;
  try {
    let results = await Comment.find({ postedBy: ObjectID(id) }).populate('vendor');
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving comments of user: ${err.message}`);
  }
}

export async function reportUser(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { userId } = args
  try {
    if (role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let user = await User.findOneAndUpdate({ _id: ObjectID(userId) }, { $inc: { "reported": 1 } }, { returnOriginal: false });
    return !!user;
  } catch (err) {
    throw new Error(`Error during retrieving comments of user: ${err.message}`);
  }
}

export async function usersReported(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { minReport = 0 } = args
  try {
    if (role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let results = await User.find({ reported: { $gte: minReport }, blockedSince: { $exists: false } }).populate('keywords').populate('achievements')
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving users reported: ${err.message}`);
  }
}

export async function usersBlocked(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  try {
    if (role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let results = await User.find({ blockedSince: { $exists: true } }).populate('keywords').populate('achievements')
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving users blocked: ${err.message}`);
  }
}

export async function blockUser(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { userId } = args
  try {
    if (role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let user = await User.findOneAndUpdate({ _id: ObjectID(userId) }, { blockedSince: new Date() }, { returnOriginal: false })
    return !!user;
  } catch (err) {
    throw new Error(`Error during block a user: ${err.message}`);
  }
}

export async function unblockUser(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { userId } = args
  try {
    if (role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let user = await User.findOneAndUpdate({ _id: ObjectID(userId) }, { $unset: { blockedSince: 1 } }, { returnOriginal: false }).populate('keywords').populate('achievements')
    return !!user;
  } catch (err) {
    throw new Error(`Error during unblock a user: ${err.message}`);
  }
}

export async function checkNewsletter(root, args, context, info) {
  let { mail } = args
  try {
    let user = await User.findOne({ mail: mail.toLowerCase() });
    return user && user.newsletter
  } catch (err) {
    throw new Error(`Error newsletter checking: ${err.message}`);
  }
}

export async function setNewsletter(root, args, context, info) {
  let { mail } = args
  try {
    let user = await User.findOne({ mail: mail.toLowerCase() });
    if (user) {

      let achievementIdNewsletter = await Achievement.findOne({ subtype: "newsletter" }, { id: 1, activated: 1 });
      let updateCmd = {
        $set: { newsletter: true }
      }
      if (!user.achievements.includes(achievementIdNewsletter._id)) {
        updateCmd['$push'] = { achievements: ObjectID(achievementIdNewsletter._id) }
      }
      await User.updateOne({ _id: ObjectID(user._id) }, updateCmd);
    }
    await createContact([14], mail, user && user.name ? user.name : "", user && user.surname ? user.surname : "", user && user.birthday ? user.birthday : null, true)
    return true;
  } catch (err) {
    throw new Error(`Error newsletter checking: ${err.message}`);
  }
}

export async function isAccountRegistered(root, args, context, info) {
  let { mail } = args;
  let user = await User.findOne({ mail: mail.toLowerCase() });
  let vendor = await Vendor.findOne({ mail: mail.toLowerCase()  });;
  return {
    isUser: !!user,
    isFacebookUser: !!user && !!user.facebookId && !_.isEmpty(user.facebookId),
    isVendor: !!vendor
  };
}

