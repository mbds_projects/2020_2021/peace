import _ from 'lodash'
import ProfessionalView from '../schemas/professionalView'


const limitView = 3;

export async function professionalViews(root, args, context, info) {
  let results = []

  let {minRate} = args;
  let limit = limitView;

  try {
    results = await ProfessionalView.find({rate: {$gte: minRate}}, null, {limit: limit, sort: '-date'})
    .populate('vendor');
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving professional views: ${err.message}`);
  }
}
