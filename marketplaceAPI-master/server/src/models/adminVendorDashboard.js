import _ from 'lodash'
import Vendor from '../schemas/vendor'
import { checkToken } from '../modules/auth'
import { sendImageToOvh, removeImageToOvh } from '../modules/ovhObjectStorage'
import fs from 'fs';
import { format } from 'date-fns'
import jwt from 'jsonwebtoken'

import { CONF } from '../config'

import { sendEmail } from '../modules/sendinBlue'
import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;

export async function adminProfileVendorDashboard(root, args, context, info) {
  let result = {}
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  try {
    if(role !== "admin") {
      throw new Error("token.adminRight");
    }
    let {id} = args
    result = await Vendor.findOne({ _id: ObjectID(id) })
      //.populate('logo')
      .populate('categories')
      .populate('keywords')
      .populate('genders')
      .populate('labels')
      .populate('locations')
      .populate('rewards')

    return result ? result : {}
  } catch (err) {
    throw new Error(`Error during retrieving profile vendor limited admin: ${err.message}`);
  }
}

export async function updateInfoAdminVendorDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  let { input, vendorId } = args
  try {
    if (role !== "admin") {
      throw new Error("token.adminRight");
    }
    let vendorUpdated = await Vendor.updateOne(
      { _id: ObjectID(vendorId) },
      { $set: input });
    return !!vendorUpdated.ok
  } catch (err) {
    throw new Error(`Error during vendor info admin dashboard updating: ${err.message}`);
  }
}

export async function updateCarouselPictureAdminVendorDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if (role !== "admin") {
    throw new Error("token.adminRight");
  }
  let { file, indexOfImage, vendorId } = args
  try {
    if (indexOfImage > 2) {
      throw new Error("vendor.carouselIndexTooBig");
    }

    let { carouselPicturesUrl } = await Vendor.findOne({ _id: ObjectID(vendorId) })
    let originalCarouselPicturesUrl = [...carouselPicturesUrl]
    let { createReadStream, filename, mimetype } = await file

    if (!mimetype.includes('image/')) {
      throw new Error("vendor.invalidUpload");
    }

    let stream = createReadStream()

    let extension = filename.split('.').pop();

    let date = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")

    let path = `${vendorId}/carousel${indexOfImage}-${date.toString()}.${extension}`
    
    if (!fs.existsSync('upload')){
      fs.mkdirSync('upload');
    }
    stream.pipe(fs.createWriteStream(`upload/${filename}`))
    let updatedFilePath = await sendImageToOvh(`upload/${filename}`, path)
    fs.unlinkSync(`upload/${filename}`);
    if (updatedFilePath) {
      let carouselPicturesUrlTmp;
      if (carouselPicturesUrl) {
        carouselPicturesUrlTmp = carouselPicturesUrl;
        if (carouselPicturesUrl.length > indexOfImage) {
          carouselPicturesUrlTmp[indexOfImage] = updatedFilePath;
        } else {
          carouselPicturesUrlTmp.push(updatedFilePath);
        }
      } else {
        carouselPicturesUrlTmp = [updatedFilePath];
      }
      let vendor = await Vendor.findOneAndUpdate({ _id: ObjectID(vendorId) }, { carouselPicturesUrl: carouselPicturesUrlTmp });
      if (!vendor) {
        throw new Error("vendor.uploadFail");
      } else {
        if(originalCarouselPicturesUrl && originalCarouselPicturesUrl.length > indexOfImage) {
          await removeImageToOvh(originalCarouselPicturesUrl[indexOfImage]);
        }
      }
      return !!updatedFilePath;
    } else {
      throw new Error("vendor.uploadFail");
    }
  } catch (err) {
    throw new Error(`Error during vendor carousel picture admin update: ${err.message}`);
  }
}


export async function updateFeaturedProductPictureAdminVendorDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if (role !== "admin") {
    throw new Error("token.adminRight");
  }
  let { file, indexOfImage, link, price, vendorId } = args
  try {
    if (indexOfImage > 5) {
      throw new Error("vendor.featuredProductIndexTooBig");
    }

    let { featuredProductsUrl } = await Vendor.findOne({ _id: ObjectID(vendorId) })
    let originalFeaturedProductsUrl = [...featuredProductsUrl]
    let { createReadStream, filename, mimetype } = await file

    if (!mimetype.includes('image/')) {
      throw new Error("vendor.invalidUpload");
    }

    let stream = createReadStream()

    let extension = filename.split('.').pop();

    let date = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")

    let path = `${vendorId}/featuredProduct${indexOfImage}-${date.toString()}.${extension}`

    if (!fs.existsSync('upload')){
      fs.mkdirSync('upload');
    }
    stream.pipe(fs.createWriteStream(`upload/${filename}`))
    let updatedFilePath = await sendImageToOvh(`upload/${filename}`, path)
    fs.unlinkSync(`upload/${filename}`);
    if (updatedFilePath) {
      let featuredProductsUrlTmp;
      if (featuredProductsUrl) {
        featuredProductsUrlTmp = featuredProductsUrl;
        if (featuredProductsUrl.length > indexOfImage) {
          featuredProductsUrlTmp[indexOfImage] = {
            imgUrl: updatedFilePath,
            link,
            price
          };
        } else {
          featuredProductsUrlTmp.push({
            imgUrl: updatedFilePath,
            link,
            price
          });
        }
      } else {
        featuredProductsUrlTmp = [{
          imgUrl: updatedFilePath,
          link,
          price
        }];
      }
      let vendor = await Vendor.updateOne({ _id: ObjectID(vendorId) },
        { $set: { featuredProductsUrl: featuredProductsUrlTmp } });
      
      if (!vendor.n > 0) {
        throw new Error("vendor.uploadFail");
      } else {
        if(originalFeaturedProductsUrl && originalFeaturedProductsUrl.length > indexOfImage) {
          await removeImageToOvh(originalFeaturedProductsUrl[indexOfImage].imgUrl);
        }
        return true;
      }
    } else {
      throw new Error("vendor.uploadFail");
    }
  } catch (err) {
    throw new Error(`Error during vendor featured product picture admin update: ${err.message}`);
  }
}


export async function updateFeaturedProductInfoAdminVendorDashboard(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role } = logUser;
  if (role !== "admin") {
    throw new Error("token.adminRight");
  }
  let { indexOfProduct, link, price, vendorId } = args
  try {
    if (indexOfProduct > 5) {
      throw new Error("vendor.featuredProductIndexTooBig");
    }

    let { featuredProductsUrl } = await Vendor.findOne({ _id: ObjectID(vendorId) })

    let featuredProductsUrlTmp;
    if (featuredProductsUrl) {
      featuredProductsUrlTmp = featuredProductsUrl;
      if (featuredProductsUrl.length > indexOfProduct) {
        featuredProductsUrlTmp[indexOfProduct] = {
          imgUrl: featuredProductsUrl[indexOfProduct].imgUrl,
          link,
          price
        };
      } else {
        throw new Error("vendor.featuredProduct.notExist");
      }
    } else {
      throw new Error("vendor.featuredProduct.notExist");
    }
    let vendor = await Vendor.updateOne({ _id: ObjectID(vendorId) },
      { $set: { featuredProductsUrl: featuredProductsUrlTmp } });
    if (!vendor.n > 0) {
      throw new Error("vendor.featuredProduct.editFail");
    } else {
      return true;
    }
  } catch (err) {
    throw new Error(`Error during vendor featured product info admin update: ${err.message}`);
  }
}


export async function createTokenVendorPassword(root, args, context, info) {
  let { id, toAdmin } = args
  try {
    let vendor = await Vendor.findOne({ _id: ObjectID(id) });
    if (vendor) {
      var timeAuth = '7d';
      const TKN_AUTH = {
        expiresIn: timeAuth
      }

      const token = jwt.sign({
        id: vendor.id
      }, CONF.secret, TKN_AUTH);

      var urlWebApp = CONF.webAppUrl;

      var urlValidation = `${urlWebApp}createVendorPassword/${token}`

      await sendEmail(2, toAdmin ? 'bonjour@super-responsable.org' : vendor.mail, { urlValidation: urlValidation });

      return true;
    } else {
      throw new Error("vendor.notFound");
    }
  } catch (err) {
    throw new Error(`Error during password token creation of vendor request: ${err.message}`)
  }
}