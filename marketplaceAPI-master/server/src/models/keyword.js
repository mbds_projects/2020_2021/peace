import _ from 'lodash'
import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;
import Keyword from '../schemas/keyword'
import User from '../schemas/user'
import Moral from '../schemas/moral'
import { checkToken } from '../modules/auth'
import { sendImageToOvh, removeImageToOvh } from '../modules/ovhObjectStorage'
import fs from 'fs';
import { format} from 'date-fns'

export async function keywords(root, args, context, info) {
  let results = {}
  try {
    results = await Keyword.find({}).populate('moral')
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving keywords: ${err.message}`);
  }
}

export async function keywordsUsed(root, args, context, info) {
  let results = {}
  try {
    results = await Keyword.find({used: true}).populate('moral')
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving keywords used: ${err.message}`);
  }
}

export async function keywordsPopular(root, args, context, info) {
  let result = []
  try {
    let resultUserKeywords = await User.aggregate([
      { $project: { keywords: 1 } }, 
      { $unwind: '$keywords' }, 
      { $group: {
          _id: { keywords: '$keywords' }, 
          count: { $sum: 1 } 
        }
      },
      { $sort: { 'count': -1 } }
    ]);

    let resultUserKeywordsObjectIds = resultUserKeywords.map((e) => ObjectID(e._id.keywords));

    let query = [
      {$match: {_id: {$in: resultUserKeywordsObjectIds}}},
      {$addFields: {"id":"$_id" , "__order": {$indexOfArray: [resultUserKeywordsObjectIds, "$_id" ]}}},
      {$sort: {"__order": 1}},
      { $limit: 6 }
     ];

    result = await Keyword.aggregate(query);

    return result ? result : []
  } catch (err) {
    throw new Error(`Error during retrieving popular keywords: ${err.message}`);
  }
}

export async function updateKeywordDesc(root, args, context, info)  {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  if(role !== "admin") {
    throw new Error("token.adminRight");
  }
  let {descFr, descEn, id} = args
  try {
    let lang = [
      {
        locale: "fr",
        value: descFr
      },
      {
        locale: "en-GB",
        value: descEn
      }
    ]
    let kwUpdated = await Keyword.updateOne(
      {_id: ObjectID(id)},
      { $set: {lang: lang}});
    return !!kwUpdated
  } catch(e) {
    throw new Error(`Error during desc update of keyword: ${err.message}`);
  }
}

export async function updatePictureKeyword(root, args, context, info)  {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  if(role !== "admin") {
    throw new Error("token.adminRight");
  }
  let {file, id} = args
  try {
    let { createReadStream, filename, mimetype } = await file
    
    let { pictureUrl } = await Keyword.findOne({ _id: ObjectID(id) })
    let originalPictureUrl = pictureUrl.toString()

    if(!mimetype.includes('image/')) {
      throw new Error("keyword.invalidUpload");
    }
    
    let stream = createReadStream()

    let extension = filename.split('.').pop();

    let date = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")

    let path = `/keywords/${id}-${date.toString()}.${extension}`
    
    if (!fs.existsSync('upload')){
      fs.mkdirSync('upload');
    }
    stream.pipe(fs.createWriteStream(`upload/${filename}`))
    let updatedFilePath = await sendImageToOvh(`upload/${filename}`, path)
    fs.unlinkSync(`upload/${filename}`);
    if (updatedFilePath) {
      let updated = await Keyword.updateOne({_id: ObjectID(id)}, {$set:{ pictureUrl: updatedFilePath}});
      if(!updated.n > 0) {
        throw new Error("keyword.uploadFail");
      } else {
        if(originalPictureUrl) {
          await removeImageToOvh(originalPictureUrl);
        }
        return true;
      }
    } else {
      throw new Error("keyword.uploadFail");
    }
  } catch (err) {
    throw new Error(`Error during picture update of keyword: ${err.message}`);
  }
}