import _ from 'lodash'
import { CONF } from '../config'

import mongoose from 'mongoose';
import Vendor from '../schemas/vendor'
import VendorDashboardDraft from '../schemas/vendorDashboardDraft'
import { checkToken } from '../modules/auth'
import { format } from 'date-fns'
import { sendImageToOvh, removeImageToOvh } from '../modules/ovhObjectStorage'
import fs from 'fs';
import Comment from '../schemas/comment'

const ObjectID = mongoose.Types.ObjectId;
const stripe = require('stripe')(CONF.stripe.sk);

export async function vendorAccount(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let result = {}
  try {
    result = await Vendor.findOne({ _id: ObjectID(id)})

    return result ? result : {}
  } catch (err) {
    throw new Error(`Error during retrieving vendor account: ${err.message}`);
  }
}

export async function retrieveAllInvoices(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let result = []
  try {
    let vendor = await Vendor.findOne({ _id: ObjectID(id) });
    let { customerId, subscriptionDashboardId } = vendor;
    if (customerId && subscriptionDashboardId) {
      const invoicesTmp = await stripe.invoices.list({
        limit: 12,
        customer: customerId,
        subscription: subscriptionDashboardId,
        status: 'paid'
      });
      result = invoicesTmp.data.map(i => {
        return {
          id: i.id,
          amount_paid: i.amount_paid,
          period_end: i.lines.data[0].period.end,
          period_start: i.lines.data[0].period.start,
          invoice_pdf: i.invoice_pdf,
          currency: i.currency
        }
      });
    }
    return result;
  } catch (err) {
    throw new Error(`Error during retrieve invoices: ${err.message}`);
  }
}

export async function updateProfilePictureVendor(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let { file } = args
  try {

    let { profilePictureUrl } = await Vendor.findOne({ _id: ObjectID(id) })
    let originalProfilePictureUrl = profilePictureUrl
    let { createReadStream, filename, mimetype } = await file

    if (!mimetype.includes('image/')) {
      throw new Error("vendor.invalidUpload");
    }

    let stream = createReadStream()

    let extension = filename.split('.').pop();

    let date = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")

    let path = `${id}/profilePicture-${date.toString()}.${extension}`

    if (!fs.existsSync('upload')) {
      fs.mkdirSync('upload');
    }
    stream.pipe(fs.createWriteStream(`upload/${filename}`))
    let updatedFilePath = await sendImageToOvh(`upload/${filename}`, path)
    fs.unlinkSync(`upload/${filename}`);
    if (updatedFilePath) {
      let vendor = await Vendor.findOneAndUpdate({ _id: ObjectID(id) }, { profilePictureUrl: updatedFilePath });
      if (!vendor) {
        throw new Error("vendor.uploadFail");
      } else {
        if (!!originalProfilePictureUrl) {
          await removeImageToOvh(originalProfilePictureUrl);
        }
      }
      return !!updatedFilePath;
    } else {
      throw new Error("vendor.uploadFail");
    }
  } catch (err) {
    throw new Error(`Error during vendor profile picture update: ${err.message}`);
  }
}

export async function updateLogoVendor(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let { file } = args
  try {

    let { logoUrl } = await Vendor.findOne({ _id: ObjectID(id) })
    let originalLogoUrl = logoUrl
    let { createReadStream, filename, mimetype } = await file

    if (!mimetype.includes('image/')) {
      throw new Error("vendor.invalidUpload");
    }

    let stream = createReadStream()

    let extension = filename.split('.').pop();

    let date = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")

    let path = `${id}/logo-${date.toString()}.${extension}`

    if (!fs.existsSync('upload')) {
      fs.mkdirSync('upload');
    }
    stream.pipe(fs.createWriteStream(`upload/${filename}`))
    let updatedFilePath = await sendImageToOvh(`upload/${filename}`, path)
    fs.unlinkSync(`upload/${filename}`);
    if (updatedFilePath) {
      let vendor = await Vendor.findOneAndUpdate({ _id: ObjectID(id) }, { logoUrl: updatedFilePath });
      if (!vendor) {
        throw new Error("vendor.uploadFail");
      } else {
        await VendorDashboardDraft({ vendorId: ObjectID(id) }, { logoUrl: updatedFilePath })
        if (!!originalLogoUrl) {
          await removeImageToOvh(originalLogoUrl);
        }
      }
      return !!updatedFilePath;
    } else {
      throw new Error("vendor.uploadFail");
    }
  } catch (err) {
    throw new Error(`Error during vendor logo update: ${err.message}`);
  }
}

export async function updateDeliverInfoVendor(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  let { input } = args
  try {
    let vendorUpdated = await Vendor.updateOne(
      { _id: ObjectID(id) },
      { $set: input });
    return !!vendorUpdated
  } catch (err) {
    throw new Error(`Error during vendor deliver information update: ${err.message}`);
  }
}