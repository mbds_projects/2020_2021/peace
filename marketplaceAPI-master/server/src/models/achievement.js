import Achievement from '../schemas/achievement'

export async function achievements(root, args, context, info) {
  let results = []
  try {
    results = await Achievement.find({})
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving achievements: ${err.message}`);
  }
}