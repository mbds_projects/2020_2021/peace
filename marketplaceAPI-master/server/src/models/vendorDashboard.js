import _ from 'lodash'
import Vendor from '../schemas/vendor'
import VendorDashboardDraft from '../schemas/vendorDashboardDraft'
import { checkToken, createAuthentJwt } from '../modules/auth'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import { CONF } from '../config'
import { sendEmail } from '../modules/sendinBlue'
import { sendImageToOvh } from '../modules/ovhObjectStorage'
import fs from 'fs';
import { isAllowedToPublish} from '../modules/vendor'

import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;

export async function vendorLogged(root, args, context, info) {
  let token = context.request.header.token
  let logVendor = await checkToken(token)
  let { id } = logVendor;
  try {
    let result = await Vendor.findOne({ _id: ObjectID(id) })
    return result
  } catch (err) {
    throw new Error(`Error during retrieving vendor logged: ${err.message}`);
  }
}

export async function profileVendorDashboard(root, args, context, info) {
  let result = {}
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  try {
    if (role !== "vendor") {
      throw new Error("token.vendorRight");
    }
    result = await Vendor.findOne({ _id: ObjectID(id) })
      .populate('labels')
      .populate({
        path: 'labels',
        populate: {
          path: 'logo'
        }
      })
      .populate('locations')
      .populate('rewards')

    return result ? result : {}
  } catch (err) {
    throw new Error(`Error during retrieving profile vendor limited: ${err.message}`);
  }
}

export async function authenticateVendor(root, args, context, info) {
  let { mail, password } = args;
  try {
    let vendor = await Vendor.findOne({ mail: mail });
    let { hash } = vendor;


    if (bcrypt.compareSync(password, hash)) {
      const token = createAuthentJwt(vendor);
      return {
        token
      };
    } else {
      throw new Error("vendor.invalidCredential");
    }
  } catch (err) {
    throw new Error(`Error during vendor authentication: ${err.message}`);
  }
}



export async function createVendorPassword(root, args, context, info) {
  let { input } = args
  let { password, token } = input;
  let logVendor = await checkToken(token)
  let { id } = logVendor;
  try {
    var regexPwd = new RegExp("(?=.*[a-z])(?=.*[A-Z]).{8,}");
    if (!regexPwd.test(password)) throw new Error("password.malformed");
    let newHash = bcrypt.hashSync(password, 10);
    let vendor = await Vendor.findOneAndUpdate({ _id: ObjectID(id) }, { hash: newHash });
    if (vendor) {
      const token = createAuthentJwt(vendor);
      return {
        token
      };
    }
    throw new Error("error.unknown");
  } catch (err) {
    throw new Error(`Error during vendor password creation: ${err.message}`);
  }
}

export async function forgotPasswordVendor(root, args, context, info) {
  let { mail } = args

  try {

    let vendor = await Vendor.findOne({ mail: mail });
    if (vendor) {
      var timeAuth = '30m';
      const TKN_AUTH = {
        expiresIn: timeAuth
      }

      const token = jwt.sign({
        id: vendor.id
      }, CONF.secret, TKN_AUTH);

      var urlWebApp = CONF.webAppUrl;

      var urlValidation = `${urlWebApp}createForgottenPasswordVendor/${token}`

      await sendEmail(1, mail, { urlValidation: urlValidation });


      return true;

    } else {
      throw new Error("mail.notFound");
    }

  } catch (err) {
    throw new Error(`Error during password vendor forgotten request: ${err.message}`);

  }
}

export async function allowedToPublish(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let { role, id } = logUser;
  if (role !== "vendor") {
    throw new Error("token.vendorRight");
  }
  try {
    let vendor = await Vendor.findOne({ _id: ObjectID(id) });
    let vendorDraft = await VendorDashboardDraft.findOne({vendorId: ObjectID(id) }).lean()
    let {ableToPublish} = await isAllowedToPublish(vendor, vendorDraft.locations.length);
    return ableToPublish;
  } catch (err) {
    throw new Error(`Error during check allow of vendor publishing: ${err.message}`);
  }
}