import _ from 'lodash'

import {sendInternalMail} from '../modules/internalMail'

import {createContact} from '../modules/sendinBlue'

export async function contactUs(root, args, context, info) {

  let {input} = args

  const { mail, selectedQuestion, message, name, surname, newsletter } = input;
  
  const questionsOptions = [
    "Candidature",
    "Support technique",
    "Enseigne",
    "Contact",
    "Devis"
];

  try {
    var mailOptions = {
      from: `${name} ${surname ? surname : ''} <${mail}>`,
      to: 'bonjour@super-responsable.org',
      subject: `CONTACT: [${questionsOptions[selectedQuestion]}]`,
      text: `${name} ${surname ? surname : ''} a envoyé un message via le formulaire de contact : "${message}" --- répondre à ${mail}`,
      html: `<h2>${name} ${surname ? surname : ''} a envoyé un message via le formulaire de contact</h2>
            <p>${message}</p>
            <p>répondre à ${mail}</p>`
    };

    sendInternalMail(mailOptions)

    let listId = 4;
    if(selectedQuestion === 2 || selectedQuestion === 4) {
      listId = 5;
    }

    await createContact(listId, mail, name, surname, null, newsletter)
    return true;
    
  } catch (err) {
    throw new Error(`Error during contact: ${err.message}`);
  }
}
