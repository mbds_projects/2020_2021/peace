import _ from 'lodash'
import Influencer from '../schemas/influencer'

export async function influencers(root, args, context, info) {
  let results = {}
  try {
    results = await Influencer.find({})
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving influencers: ${err.message}`);
  }
}