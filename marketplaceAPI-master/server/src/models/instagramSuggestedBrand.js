import _ from 'lodash'
import InstagramSuggestedBrand from '../schemas/instagramSuggestedBrand'
import SuggestedBrand from '../schemas/suggestedBrand'

import { checkToken } from '../modules/auth'
import mongoose from 'mongoose';
const ObjectID = mongoose.Types.ObjectId;

var rp = require('request-promise');

async function instagramPost(url) {
  return rp(url)
  .then(async function(res) {
    let tags = [];
    let indexOfTimestamp = res.indexOf('taken_at_timestamp":');
    let indexOfEndTimestamp = res.indexOf(',', indexOfTimestamp);
    let timestamp = res.slice(indexOfTimestamp + 20, indexOfEndTimestamp);
    let date = new Date(timestamp * 1000);
    

    let indexOfArticle = res.indexOf('accessibility_caption');
    let indexOfTagging = res.indexOf('tagging', indexOfArticle)
    if(indexOfTagging > - 1) {
      let indexOfEndTagging = res.indexOf('. ', indexOfTagging)
      let tagging = res.slice(indexOfTagging + 8, indexOfEndTagging).toLowerCase();
      tags = tagging.split(', ');
    }


    var re = /\(\@(?!.*\.\.)(?!.*\.$)[^\W][\w.]{0,29}\) on Instagram/g;
    var account = res.match(re);
    let tagAccount = "";
    if(account){
      let indexOfTagAccountStart = account[0].indexOf('@');
      let indexOfEndTagAccountStart = account[0].indexOf(')', indexOfTagAccountStart);
      tagAccount = account[0].slice(indexOfTagAccountStart, indexOfEndTagAccountStart).toLowerCase()     
      if(!tags.includes(tagAccount)) {
        tags.push(tagAccount);
      }
    }

    let indexOfTaggingInText = res.indexOf('edge_media_to_caption', indexOfArticle)
    let indexOfEndTaggingInText = res.indexOf('caption_is_edited', indexOfTaggingInText)
    let content = res.slice(indexOfTaggingInText, indexOfEndTaggingInText)
    if(indexOfTaggingInText > -1){
      while(content.includes("@")) {
        let startTag = content.indexOf("@");
        let endTag = content.indexOf(" ", startTag);
        let tag = content.slice(startTag, endTag).toLowerCase();
        if(!tags.includes(tag)) {
          tags.push(tag);
        }
        content = content.slice(endTag + 1)
      }
    }
    return {date: date, tags: tags, account: tagAccount}
  })
}

export async function registerSuggestedBrand(arrayInsta, onlyNew) {
    let res = [];
    for(let i = 0; i < arrayInsta.length; i++) {
      let lastPost = await instagramPost(arrayInsta[i].link);
      let latestInstagramSuggestedBrand = await InstagramSuggestedBrand.findOne({}, {}, { sort: { 'latestPostDate' : -1 } });
      let shouldBeAdded = (onlyNew && (!latestInstagramSuggestedBrand || latestInstagramSuggestedBrand.latestPostDate.getTime() <= lastPost.date.getTime()) || !onlyNew)
      if(shouldBeAdded) {
        for(let j = 0; j < lastPost.tags.length; j++) {
          let tag = lastPost.tags[j];
          let addedHimself = tag == lastPost.account;
          let instagramSuggestedBrandSearch = await InstagramSuggestedBrand.findOne({name: tag});
          if(!instagramSuggestedBrandSearch) {
            let created = await InstagramSuggestedBrand.create({
              latestPostDate: lastPost.date,
              name: tag,
              occurrence: 1,
              checked: false,
              addedHimself: addedHimself
            });
            res.push(created);
          } else {
            let alreadyAddedAtThisDate = instagramSuggestedBrandSearch.latestPostDate.getTime() === lastPost.date.getTime();
            
            if(!alreadyAddedAtThisDate) {
              let setDate = instagramSuggestedBrandSearch.latestPostDate < lastPost.date ? lastPost.date : instagramSuggestedBrandSearch.latestPostDate
              let updated = await InstagramSuggestedBrand.findOneAndUpdate(
                {name: tag},
                { 
                  $inc: { occurrence: 1 },
                  $set: {latestPostDate: setDate, addedHimself: addedHimself}
                }, {returnOriginal: false}
              );
              res.push(updated);
            }

            if(instagramSuggestedBrandSearch.checked) {
              let searchSuggestedBrandInstaId = await SuggestedBrand.findOne({instagramId: tag});
              let alreadyAddedHimself = searchSuggestedBrandInstaId.addedHimself ? searchSuggestedBrandInstaId.addedHimself : addedHimself;
              if(searchSuggestedBrandInstaId) {
                SuggestedBrand.updateOne(
                  {_id: ObjectID(searchSuggestedBrandInstaId.id)}, 
                  {
                    $inc: { occurrence: 1 },
                    $set: {addedHimself: alreadyAddedHimself}
                  })
              }
            }
          }
        };
      }
    };
    return res;
}

export async function detectAndSaveInstagramSuggestedBrands(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  if(role !== "admin") {
    throw new Error("token.adminRight");
  }
  let { input } = args  
  const { url, onlyNew } = input;
  try {
    let res = await registerSuggestedBrand([{link: url}], onlyNew);
    return res ? res : []
  } catch(err) {
    throw new Error(`Error during detection and saving instagram suggested brands: ${err.message}`);
  }
}

export async function instagramSuggestedBrands(root, args, context, info) {
  let results = []
  try {
    results = await InstagramSuggestedBrand.find({})
    return results ? results : []
  } catch (err) {
    throw new Error(`Error during retrieving instagram suggested brands: ${err.message}`);
  }
}

export async function instagramSuggestedBrand(root, args, context, info) {
  let result = {}
  let {id} = args;
  try {
    result = await InstagramSuggestedBrand.findOne({_id: ObjectID(id)})
    return result ? result : {}
  } catch (err) {
    throw new Error(`Error during retrieving an instagram suggested brand: ${err.message}`);
  }
}

export async function updateInstagramSuggestedBrand(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  let {input} = args
  try {    
    if(role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let instagramSuggestedBrand = await InstagramSuggestedBrand.updateOne(
      {_id: ObjectID(input.id)},
      { $set: input}
    );
    return !!instagramSuggestedBrand;
  } catch (err) {
    throw new Error(`Error during instagram suggested brand update: ${err.message}`);
  }
}

export async function deleteInstagramSuggestedBrand(root, args, context, info) {
  let token = context.request.header.token
  let logUser = await checkToken(token)
  let {role} = logUser;
  let {instagramSuggestedBrandId} = args;
  try {
    if(role !== "admin") {
      throw new Error(`Admin access error: logged user is not admin`);
    }
    let deleteQuery = await InstagramSuggestedBrand.deleteOne({
      _id: ObjectID(instagramSuggestedBrandId)
    });
    return deleteQuery.ok;
  } catch (err) {
    throw new Error(`Error during instagram suggested brand deletion: ${err.message}`);
  }
}