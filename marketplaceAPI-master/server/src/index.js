import {version} from '../package.json';
import {ENV} from './config';
import app from './api'


console.log('======================================================================');
console.log(`== API CONTACT V ${version} 🚀   |  ENV: ${ENV}  |  NODE_ENV: ${process.env.NODE_ENV}`)
console.log('======================================================================');
