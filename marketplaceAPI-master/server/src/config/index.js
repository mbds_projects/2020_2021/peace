import { prod } from './env/prod';
import { dev } from './env/dev';
import { local } from './env/local';


export const ENV = process.env.ENV || 'local'

export const ISPROD = (process.env.ENV === 'production' || process.env.ENV === 'prod')
export const ISDEV = (process.env.ENV === 'development' || process.env.ENV === 'dev')
export const ISLOCAL= process.env.ENV === 'local'

switch(ENV) {
  case 'production':
    var config = prod;
    break;
  case 'prod':
    var config = prod;
    break;  
  case 'development':
    var config = dev;
    break;
  case 'dev':
    var config = dev;
    break;
  default:
    var config = local;
}
export const CONF = config