export const dev = {
  ip:process.env.IP,
  port:process.env.PORT,
  database: process.env.DATABASE,
  secret: process.env.PORT,
  webAppUrl: process.env.WEBAPPURL,
  sendinBlueAPIKey: process.env.SENDINBLUEAPIKEY,
  sendinBlueAPIKeyInternal: process.env.SENDINBLUEAPIKEY_INTERNAL,
  nodemailer: {
    user: process.env.MAILUSER,
    pass: process.env.MAILPASS
  },
  youtubeAPIKey: process.env.YOUTUBEAPIKEY,
  ovh: {
    username: process.env.OVHUSER,
    password: process.env.OVHPASS,
    authURL:  process.env.OVHURL,
    tenantId: process.env.OVHTID,
    region:   process.env.OVHREGION
  },
  stripe: {
    sk: process.env.STRIPESK,
    webhook:   process.env.STRIPEWEBHOOK,
    taxes: [process.env.TAXE_FR, process.env.NO_TAXE],
    prices: {
      basic: [process.env.BASIC_MONTH, process.env.BASIC_YEAR, process.env.BASIC_WETAKECARE],
      super: [process.env.SUPER_MONTH, process.env.SUPER_YEAR, process.env.SUPER_WETAKECARE],
      superPlus: [process.env.SUPERPLUS_MONTH, process.env.SUPERPLUS_YEAR, process.env.SUPERPLUS_WETAKECARE],
    }
  }
}
