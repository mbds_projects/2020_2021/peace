import {
  createMuiTheme
} from '@material-ui/core/styles';

const theme = createMuiTheme({
    colors: {
        strawberry: '#ff5757',
        almond: '#1BE391',
        facebook: '#3b5998',
        gold: '#efb813',
        instagram: "radial-gradient(circle at 30% 107%, #fdf497 0%, #fdf497 5%, #fd5949 45%,#d6249f 60%,#285AEB 90%)",
        youtube: "#e52d27",
        linkedin: "#0e76a8"
  },
});

export default theme;