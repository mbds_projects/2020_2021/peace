import React, { useState } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';

import TextareaAutosize from '@material-ui/core/TextareaAutosize';

import Button from '@material-ui/core/Button';

import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import SendIcon from '@material-ui/icons/Send';

import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) =>
  createStyles({
    sendCommentButton: {
      height: "fit-content",
      margin: "0px 10px"
    },
    selectStar: {
      cursor: "pointer"
    },
    fieldsetComment: {
      border: `${theme.colors.strawberry} 2px solid`,
      padding: "15px",
      "& legend": {
        width: "auto"
      }
    },
    commentTextarea: {
      width: "100%"
    },
    validComment: {
      textAlign: "center",
      margin: "10px 0px"
    }
  }),
);


export default function AddCommentItem(props) {
  const { t } = useTranslation();
  const classes = useStyles();

  const [rateSelected, setRateSelected] = useState(-1);
  const [commentTextarea, setCommentTextarea] = useState('');

  const textareaChange= (event) => {
    setCommentTextarea(event.target.value);
  }

  const sendComment = () => {
    props.sendComment(commentTextarea, rateSelected);
  }
  
  const chooseRate = () => {
    let iconsStar = [];
    for(let i = 1; i <= 5; i++) {
      if(i <= rateSelected) {
        iconsStar.push(<StarIcon key={`star_chooseRate_${i}`} onClick={() => setRateSelected(i)} className={classes.selectStar}/>)
      } else {
        iconsStar.push(<StarBorderIcon key={`star_chooseRate_${i}`} onClick={() => setRateSelected(i)} className={classes.selectStar}/>)
      
      }
     }
    return iconsStar;
  }

  return (
    <fieldset className={classes.fieldsetComment}>
      <legend>{t('shareComments.title')}</legend>
      <div>{t('shareComments.rate')}{chooseRate()}</div>
      {t('shareComments.text')}
        <TextareaAutosize
        rows={4}
        onChange={textareaChange}
        aria-label="maximum height"
        placeholder={t('shareComments.textPlaceholder')}
        className={classes.commentTextarea}
        value={commentTextarea}
        />
      <div className={classes.validComment}>
      <Button
        variant="contained"
        color="primary"
        disabled={rateSelected <= 0}
            onClick={sendComment}
            className={classes.sendCommentButton}
            endIcon={<SendIcon/>}
            >
            {t('shareComments.button')}
            </Button>
      </div>
    </fieldset>
  );
}
      
      