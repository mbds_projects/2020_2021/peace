import * as React from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import StorefrontIcon from '@material-ui/icons/Storefront';
import ExploreOffIcon from '@material-ui/icons/ExploreOff';

import ReactGA from 'react-ga';
import Grid from '@material-ui/core/Grid';

import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(() =>
  createStyles({
    root: {

    },
    img: {
      width: "100%"
    },
    icons: {
      "& p": {
          "& svg": {
              marginRight: "5px",
              position: "relative",
              top: "5px"
          }
      }
    },
  }),
);

export default function OnlineShopCard(props) {

  let imgSrc = "";
  if(props.logoUrl) {
    imgSrc = props.logoUrl;
  }

  const { t } = useTranslation();

  let classes = useStyles();

  const GADetectWebSiteClick = () => {
    ReactGA.event({
      category: 'Online Shop',
      action: `Click on website`,
      label: `Vendor ${props.vendorName}`
    });
  }

  return (
    <div className={classes.root}>
      <Grid container direction="row" justify="center" alignItems="center">
        <Grid item xs={4}>
          <img alt={`${props.vendorName} logo`} src={imgSrc} className={classes.img} />
        </Grid>
        
        <Grid container direction="column" item xs={6}>
          <Grid item className={classes.icons}>
            <p><StorefrontIcon/><a onClick={GADetectWebSiteClick} target="_blank" rel="noopener noreferrer" href={props.webSite}>{t('onlineShop.website')}</a></p>
          </Grid>
          <Grid item className={classes.icons}>
            <p><ExploreOffIcon/>{t('onlineShop.noPointOfSale')}</p>
          </Grid>
        </Grid>
      </Grid>
    </div>

  )
}