import * as React from 'react';

import { Route, Redirect } from 'react-router';
import { toast } from 'react-toastify';
import Loading from '../../pages/Loading/LoadingPage';

import { useTranslation } from 'react-i18next';
import { useSelector } from "react-redux";

export default function UserRoute(props) {
    const loggedInUser = useSelector((state) => state.authentication.currentUser);
    const isLoading = useSelector((state) => state.authentication.isLoading);
    const isVendor = (loggedInUser != null && loggedInUser.role === "vendor")
    const isAuthenticated = (loggedInUser != null)
    const { t } = useTranslation();
    if (isLoading) {
        return <Loading />
    }

    if (!isAuthenticated) {
        toast.warn(t('error.shouldBeConnected'), {
            position: toast.POSITION.TOP_RIGHT
        });
    }

    if (isAuthenticated && isVendor) {
        toast.error(t('error.notAllowed'), {
            position: toast.POSITION.TOP_RIGHT
        });
    }

    if (isAuthenticated && !isVendor) {
        return <Route {...props}/>;
    } else {
        const renderComponent = () => (<Redirect to={{pathname: props.redirectionPath}}/>);
        return <Route {...props} component={renderComponent} render={undefined}/>;
    }
}