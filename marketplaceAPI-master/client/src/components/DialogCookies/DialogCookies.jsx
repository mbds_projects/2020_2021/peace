import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { NavLink } from 'react-router-dom';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { Trans } from 'react-i18next';
import { useTranslation } from 'react-i18next';
import { useLocation} from 'react-router-dom';

const useStyles = makeStyles((theme) =>
  createStyles({
    icon: {
      width: "25px",
      marginRight: "5px"
    },
    readMore: {
      color: "black"
    },
    colorAlmond: {
      backgroundColor: `${theme.colors.almond} !important`,
      "& hover": {
        backgroundColor: `${theme.colors.almond} !important`,
    },
    }
  }),
);
export default function DialogCookies(props) {
  const { t } = useTranslation();
  const location = useLocation();

  let classes = useStyles();
  return (
    <Dialog open={props.open && location.pathname !== "/cookies"}>
      <DialogTitle>
        <img alt="" className={classes.icon} src="/assets/images/icons/cookie.png" />
        {t('dialog.cookies.title')}
      </DialogTitle>
      <DialogContent>
        <Trans i18nKey="dialog.cookies.info"
          components={[<br />, <NavLink to="/cookies" className={classes.readMore}></NavLink>]} />
        <div style={{ textAlign: "right" }}>
          <Button variant="contained" className={classes.colorAlmond} color="primary" onClick={props.validAllCookies}>{t('dialog.cookies.button')}</Button>
        </div>
      </DialogContent>
    </Dialog>

  );
}