import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import ButtonBase from '@material-ui/core/ButtonBase';

import Link from '@material-ui/core/Link';

import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: '10px',
      margin: 'auto',
      maxWidth: 500,
    },
    image: {
      width: "100%",
      height: "100%",
    },
    img: {
      margin: 'auto',
      display: 'block',
      maxWidth: '100%',
      maxHeight: '100%',
    },
    subTitle: {
      fontSize: '14px'
    },
    date: {
      fontSize: '12px',
      textAlign: "center",
      paddingTop: '10px'
    },
    author: {
      color: theme.colors.almond,
      fontSize: '20px'
    },
    video: {
      width: "100%"
    }
  }),
);

export default function InterviewCard(props) {
  const { i18n } = useTranslation();
  const classes = useStyles();

  let interview = props.interview
  let date = new Date(interview.date);
  let month = date.toLocaleString(i18n.language, { month: 'long' });
  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Grid container direction="column">
          {
            interview.type === "video" ? 
              <Grid item xs={12}>
                <iframe title="video" className={classes.video} width="560" height="215" src={interview.link}></iframe>
              </Grid>
            :
            interview.type === "article" &&
            <Grid container spacing={2}>
            <Grid item xs={12} sm={5}>
              <ButtonBase 
                className={classes.image} 
                component={Link}
                href={interview.link}
                target="_blank" rel="noopener noreferrer"
                >
                <img className={classes.img} alt="complex" src={interview.imgUrl} />
              </ButtonBase>
            </Grid>
            <Grid item xs={12} sm >
              <a href={interview.link} target="_blank" rel="noopener noreferrer">
                <h4 className={classes.author}>{interview.author}</h4>
              </a>
              <span className={classes.subTitle}>
                {interview.title}<br/>
                {interview.subTitle}
              </span>
            </Grid>
          </Grid>
          }
          
          
          
          <Grid item xs={12} className={classes.date}>
            <span >
            {date.getDate()} {month} {date.getFullYear()}
            </span>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
}