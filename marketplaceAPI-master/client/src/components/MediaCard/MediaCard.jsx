import * as React from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

//import Link from '@material-ui/core/Link';

import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      margin: '10px'
    },
    link: {
      cursor: "pointer",
      "&:hover": {
        textDecoration: "none"
      }
    },

    grid: (props) => ({
      height: "200px",
      //borderRadius: "15px",
      backgroundImage: `linear-gradient(0deg, rgba(0, 0, 0, 0.5) 0%, rgba(255,255,255,0) 50%), url("${props.background}")`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      backgroundSize: "cover"
    }),
    name: {
      color: "white",
      fontSize: "18px",
      fontWeight: "bold",
      /*wordBreak: "break-word"*/
    },
    bottomCard: {
      padding: "5px 10px"
    }
  }),
);

export default function MediaCard(props) {

  function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

  let classes = useStyles({ background: props.element.media });
  return (
    <div className={classes.root}>
      <a /*onClick={handleClick}*/ className={classes.link}
        href={props.element.link} target="_blank" rel="noopener noreferrer">
        <Grid container direction="column" justify="flex-end" className={classes.grid}>
          <Grid container direction="row" alignItems="flex-end" className={classes.bottomCard}>
            <Grid item xs={12} className={classes.name}>{decodeHtml(props.element.title)}</Grid>
          </Grid>
        </Grid>
      </a>
    </div>

  )
}