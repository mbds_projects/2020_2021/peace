import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { useTranslation } from 'react-i18next';
import LoginPageContainer from './../../pages/Login/LoginPageContainer'

export default function DialogShouldBeConnected(props) {
    const { t } = useTranslation();


    let close = ( ) => {
        props.close();
    }
    return (
        <Dialog
            open={props.open}
            fullWidth={true}
            onClose={close}>
            <DialogContent>
                <LoginPageContainer inDialog={true} close={props.close} />
            </DialogContent>
        </Dialog>
    )
}