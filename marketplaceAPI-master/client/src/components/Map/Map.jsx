//import './styles.css';

import React from 'react'
import { Map, TileLayer, Marker/*, Tooltip*/, Popup } from 'react-leaflet'
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import * as L from 'leaflet';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import ReactGA from 'react-ga';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import PhoneIcon from '@material-ui/icons/Phone';
import MailIcon from '@material-ui/icons/Mail';
import WebIcon from '@material-ui/icons/Web';
import MyLocationIcon from '@material-ui/icons/MyLocation';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const CustomMarker = (props) => {

  const openPopup = (marker) => {
      if (marker) {
          marker.leafletElement.openPopup()
      }
  }

  return (
      <Marker ref={el => openPopup(el)} {...props} />
  )
}

const useStyles = makeStyles(() =>
  createStyles({
    root: (props) => ({
      height: props.mobile ? "300px" : "400px",
      width: props.mobile ? "90%" : "100%",
      margin: "0 auto",
      marginTop: "20px"
    }),
    avatar: {
      width: "35px",
      height: "35px",
      margin: "auto",
      border: "1px solid grey"
    },
    icons: (props) => ({
      "& p": {
          padding: "0px",
          marginBottom: "0px",
          marginTop: "0px",
          maxWidth: props.mobile ? "250px" : "400px",
          whiteSpace: "nowrap",
          overflow: "hidden",
          textOverflow: "ellipsis",
          "& svg": {
              marginRight: "5px",
              position: "relative",
              top: "5px"
          }
      }
    }),
  }),
);

const iconPerson = new L.Icon.Default({imagePath: '/assets/images/icons/'});

export default function MapComponent(props) {
    const matches = useMediaQuery('(max-width:768px)');
    let classes = useStyles({mobile: matches});
    const latLng = L.latLng(props.lat, props.long);

    const latLngUpper = L.latLng(props.lat + 0.0002, props.long);
    const zoom= props.zoom;

    const GADetectPointOfSaleClick = (pointOfSale, info) => {
      ReactGA.event({
        category: 'Point Of Sale',
        action: `Click on ${pointOfSale} ${info}`,
        label: `Vendor ${props.vendorName}`
      });
    }
    
    const firstLocation = props.firstLocation;

    let imgSrc  = "";

    let avatarText = `${props.vendorName.charAt(0)}${props.vendorName.charAt(1)}`
    if(props.logoUrl) {
      imgSrc = props.logoUrl;
      avatarText = "";
    }

    return (
        <Map center={latLngUpper} zoom={zoom} className={classes.root}>
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <CustomMarker position={latLng} icon={iconPerson}>
            <Popup direction="top" offset={[0, -10]} opacity={1} permanent>
              <Grid container direction="row" justify="center" alignItems="center" spacing={1}>
                  <Grid item>
                    <Avatar className={classes.avatar} src={imgSrc}>{avatarText}</Avatar>
                  </Grid>
                  <Grid item> 
                    <Grid item><b>{firstLocation.shopName || props.vendorName}</b></Grid>
                  </Grid>
                <Grid item xs={12}>
                  <Grid item className={classes.icons}>
                    <p><MyLocationIcon/>{firstLocation.address}, {firstLocation.postalCode} {firstLocation.city}</p>
                    {firstLocation.openingHours && <p><AccessTimeIcon/> {firstLocation.openingHours}</p>}
                    {firstLocation.phoneNumber && <p><PhoneIcon /><a onClick={() => GADetectPointOfSaleClick(firstLocation.shopName, "phoneNumber")} target="_blank" rel="noopener noreferrer" href={"tel:" +  firstLocation.phoneNumber}>{firstLocation.phoneNumber}</a></p>}
                    {firstLocation.mail && <p><MailIcon /><a onClick={() => GADetectPointOfSaleClick(firstLocation.shopName, "mail")} target="_blank" rel="noopener noreferrer" href={"mailto:" + firstLocation.mail}>{firstLocation.mail}</a></p>}
                    {firstLocation.webSite && <p><WebIcon /><a onClick={() => GADetectPointOfSaleClick(firstLocation.shopName, "website")} target="_blank" rel="noopener noreferrer" href={firstLocation.webSite}>{firstLocation.webSite}</a></p>}
                    </Grid>
                </Grid>
              </Grid>
            </Popup>
          </CustomMarker>
        </Map>
      )
}