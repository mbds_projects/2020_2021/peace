import React, { useState, useEffect } from 'react';
import { fade, makeStyles, createStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';

import { toast } from 'react-toastify';
import { setGeoloc } from '../../shared/store/actions/user.actions';
import store from '../../shared/store/store';

import Grid from '@material-ui/core/Grid';
//import Button from '@material-ui/core/Button';

import * as _ from 'lodash';

import * as Autosuggest from 'react-autosuggest';

import SearchIcon from '@material-ui/icons/Search';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import MenuItem from '@material-ui/core/MenuItem';
import ClearIcon from '@material-ui/icons/Clear';

import Checkbox from '@material-ui/core/Checkbox';
//import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExploreIcon from '@material-ui/icons/Explore';


import Switch from '@material-ui/core/Switch';
import Slider from '@material-ui/core/Slider';

import useMediaQuery from '@material-ui/core/useMediaQuery';

import { useTranslation } from 'react-i18next';
import ReactGA from 'react-ga';

const parse = require('autosuggest-highlight/parse');

const useStyles = makeStyles((theme) =>
  createStyles({
    sideBarFiltersContent: {
      padding: "15px"
    },
    sliderDistance: {
      width: "80%"
    },
    suggestionLight: {
      fontWeight: "normal"
    },
    suggestionStrong: {
      fontWeight: "bolder",
      color: theme.colors.strawberry
    },
    suggestionPaper: {
      "& ul": {
        listStyle: "none",
        margin: "0px",
        padding: "0px",
        "& li > div": {
          paddingLeft: "55px"
        }
      }
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.black, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.black, 0.25),
      },
      width: '100%',
      color: "black",
    },
    searchIcon: {
      width: theme.spacing(7),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      color: "black"
    },
    inputRoot: {
      color: 'inherit',
      height: '50px',
      width: "80%"
    },
    inputRootMobile: {
      color: 'inherit',
      height: '50px',
      width: "300px"
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 7),
      transition: theme.transitions.create('width'),
      width: '100%'
    }
  }),
);

export default function SidebarFilter(props) {
  const marks = [
    {
      value: 5,
      label: '5km',
    },
    {
      value: 10,
      label: '10km',
    },
    {
      value: 20,
      label: '20km',
    },
    {
      value: 30,
      label: '30km',
    }
  ];

  const matches = useMediaQuery('(max-width:768px)');

  const { t, i18n } = useTranslation();

  const [expanded, setExpanded] = React.useState(props.catCheckboxes ? 'panel1' : 'panel3');
  const [cityDistance, setCityDistance] = useState(30);
  const [suggestions, setSuggestions] = useState([]);
  const [value, setValue] = useState('');
  const [valuesChecked, setValuesChecked] = React.useState(false);
  const [cityDistanceCommited, setCityDistanceCommited] = useState(30);

  const getSuggestionValue = (suggestion) => {
    props.handleToggleCity(suggestion.value, true)
    return '';
  }

  const getSuggestions = (value, suggestions) => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0 ? [] : suggestions.filter(lang =>
      lang.name ? lang.name.toLowerCase().includes(inputValue) : false
    );
  };

  const onSuggestionsFetchRequested = ({ value }) => {
    setSuggestions(getSuggestions(value, props.suggestions));
  };

  const onSuggestionsClearRequested = () => {
    setSuggestions([]);
  };

  const handleChangeDistance = (event, value) => {
    setCityDistance(value);
  };

  const handleChangeCommitedDistance = (event, value) => {
    if (cityDistanceCommited !== value) {
      setCityDistanceCommited(value);
      props.handlePositionDistance(value);
      if (props.from === "Vendors") props.setCurrentPage(1);
      ReactGA.event({
        category: 'Geolocation',
        action: `Set diameter of search to ${value} kms`,
        label: props.from
      });
    }
  };

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  let classes = useStyles();

  const handleValuesChecked = (event) => {
    if (!props.isLogged) {
      setValuesChecked(false);
      toast.error(t('vendorList.shouldBeLogged'), {
        position: toast.POSITION.TOP_RIGHT
      });
    } else {
      setValuesChecked(event.target.checked);
      if (props.from === "Vendors") props.setCurrentPage(1);
      props.toggleUserValues(event.target.checked);
    }
  }

  useEffect(() => {
    let didCancel = false;
    if(props.aroundMePath) {
      !didCancel && handleCityChecked({target: {checked: true}})
    }
    return () => { didCancel = true }
  }, [props.aroundMePath])

  const handleCityChecked = (event) => {
    if (!props.geolocAccepted) {
      navigator.geolocation.getCurrentPosition((pos) => {
        var crd = pos.coords;
        store.dispatch(setGeoloc({ latitude: crd.latitude, longitude: crd.longitude }, true));
        props.setCityChecked(true);
        setCityDistance(30);
        props.handlePositionCheck(true, { latitude: crd.latitude, longitude: crd.longitude });
        props.dropCityFilter();
        if (props.from === "Vendors") props.setCurrentPage(1);
        ReactGA.event({
          category: 'Geolocation',
          action: `Accepted`,
          label: props.from
        });
      }, () => {
        toast.error(t('vendorList.userDeniedGeoloc'), {
          position: toast.POSITION.TOP_RIGHT
        });
        props.setCityChecked(false);
        ReactGA.event({
          category: 'Geolocation',
          action: `Refused`,
          label: props.from
        });
      }, {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      });

    } else {
      props.setCityChecked(event.target.checked);
      props.handlePositionCheck(event.target.checked);
      if (event.target.checked) {
        props.dropCityFilter();
      }
      setCityDistance(30);
      if (props.from === "Vendors") props.setCurrentPage(1);
    }
    ReactGA.event({
      category: 'Geolocation',
      action: `Filter by display vendors around me`,
      label: props.from
    });
  };

  const handleOnlineVendorsChecked = (event) => {
    props.setOnlineVendors(event.target.checked);
    props.handleOnlineVendorCheck(event.target.checked);
    if (props.from === "Vendors") props.setCurrentPage(1);
    ReactGA.event({
      category: 'Filter',
      action: `${event.target.checked ? 'Filter' : 'Remove filter'} by online vendors`,
      label: props.from
    });
  };

  const handleClickAndCollectVendorsChecked = (event) => {
    props.setClickAndCollectVendors(event.target.checked);
    props.handleClickAndCollectVendorCheck(event.target.checked);
    if (props.from === "Vendors") props.setCurrentPage(1);
    ReactGA.event({
      category: 'Filter',
      action: `${event.target.checked ? 'Filter' : 'Remove filter'} by click and collect vendors`,
      label: props.from
    });
  };

  const handleRewardChecked = (event) => {
    props.setReward(event.target.checked);
    props.handleRewardCheck(event.target.checked);
    if (props.from === "Vendors") props.setCurrentPage(1);
    ReactGA.event({
      category: 'Filter',
      action: `${event.target.checked ? 'Filter' : 'Remove filter'} by rewards`,
      label: props.from
    });
  };


  const constructFilterArray = (field) => {
    let fieldToSearch = props.filterUrlArgs[field];
    return fieldToSearch ? _.isArray(fieldToSearch) ? fieldToSearch : [fieldToSearch] : []
  }

  let filterKw = constructFilterArray('kw')
  let filterGd = constructFilterArray('gd');
  let filterCat = constructFilterArray('cat')


  let toggleCategory = (cat, event) => {
    props.handleToggleCategory(cat, event.target.checked);
    if (props.from === "Vendors") props.setCurrentPage(1);
    ReactGA.event({
      category: 'Filter',
      action: `${event.target.checked ? 'Filter' : 'Remove filter'} by category ${cat}`,
      label: props.from
    });
  }

  const categoriesCheckboxes = props.categories
    .map((cat) => {
      let catName = cat.lang.find((l) => l.locale === i18n.language).value;
      return (
        <FormControlLabel key={cat.id}
          control={
            <Checkbox name={cat.name} checked={filterCat.includes(cat.id)} value={cat.id}
              onChange={(event) => { toggleCategory(cat, event) }} />
          }
          label={catName}
        />)
    });

    let toggleGender = (gd, event) => {
      props.handleToggleGender(gd, event.target.checked);
      if (props.from === "Vendors") props.setCurrentPage(1);
      ReactGA.event({
        category: 'Filter',
        action: `${event.target.checked ? 'Filter' : 'Remove filter'} by gender ${gd}`,
        label: props.from
      });
    }

  const gendersCheckboxes = props.genders
    .map((gd) => {
      let gdName = gd.lang.find((l) => l.locale === i18n.language).value;
      return (
        <FormControlLabel key={gd.id}
          control={
            <Checkbox name={gd.name} checked={filterGd.includes(gd.id)} value={gd.id}
              onChange={(event) => { toggleGender(gd, event) }} />
          }
          label={gdName}
        />)
    });

  let toggleKeyword = (kw, event) => {
    props.handleToggleKeyword(kw, event.target.checked);
    if (props.from === "Vendors") props.setCurrentPage(1);
    ReactGA.event({
      category: 'Filter',
      action: `${event.target.checked ? 'Filter' : 'Remove filter'} by value ${kw}`,
      label: props.from
    });
  }

  let healthyKw = props.keywords.filter(kw => kw.moral && kw.moral.name === "Sain");
  const healthyCheckboxes = healthyKw
    .map((kw) => {
      let keyName = kw.lang.find((l) => l.locale === i18n.language).value;
      return (
        <FormControlLabel key={kw.id}
          control={
            <Checkbox name={kw.name} checked={filterKw.includes(kw.id)} value={kw.id}
              onChange={(event) => { toggleKeyword(kw, event) }} />
          }
          label={keyName}
        />)
    });

  let ethicalKw = props.keywords.filter(kw => kw.moral && kw.moral.name === "Ethique");
  const ethicalCheckboxes = ethicalKw
    .map((kw) => {
      let keyName = kw.lang.find((l) => l.locale === i18n.language).value;
      return (
        <FormControlLabel key={kw.id}
          control={
            <Checkbox name={kw.name} checked={filterKw.includes(kw.id)} value={kw.id}
              onChange={(event) => { toggleKeyword(kw, event) }} />
          }
          label={keyName}
        />)
    });

  let sustainableKw = props.keywords.filter(kw => kw.moral && kw.moral.name === "Durable");
  const sustainableCheckboxes = sustainableKw
    .map((kw) => {
      let keyName = kw.lang.find((l) => l.locale === i18n.language).value;
      return (
        <FormControlLabel key={kw.id}
          control={
            <Checkbox name={kw.name} checked={filterKw.includes(kw.id)} value={kw.id}
              onChange={(event) => { toggleKeyword(kw, event) }} />
          }
          label={keyName}
        />)
    });

  function renderSuggestion(suggestion, { query, isHighlighted }) {
    const parts = parse(suggestion.name, [[suggestion.name.toLowerCase().indexOf(query.toLowerCase()), suggestion.name.toLowerCase().indexOf(query.toLowerCase()) + query.length]]);
    return (
      <MenuItem selected={isHighlighted} component="div">
        <div>
          {parts.map((part, index) =>
            part.highlight ? (
              <span key={String(index)} className={classes.suggestionStrong}>
                {part.text}
              </span>
            ) : (
                <span key={String(index)} className={classes.suggestionLight}>
                  {part.text}
                </span>
              ),
          )}
        </div>
      </MenuItem>
    );
  }


  const revertSearch = () => {
    setValue('');
    setSuggestions([]);
  }

  const onChange = (event, { newValue }) => {
    setValue(newValue);
  };


  const inputProps = {
    placeholder: t('vendorProfile.searchPlaceholder'),
    value,
    onChange: onChange
  };

  const renderInputComponent = (inputProps) => (
    <div className={classes.search}>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        classes={{
          root: matches ? classes.inputRootMobile : classes.inputRoot,
          input: classes.inputInput,
        }}
        {...inputProps}
      />
      {
        value !== "" &&
        <IconButton aria-label="Clear" onClick={revertSearch}>
          <ClearIcon />
        </IconButton>
      }
    </div>
  );
  return (
    <div className={classes.sideBarFiltersContent}>
      <FormGroup row>
        <FormControlLabel
          control={<Switch checked={props.onlineVendors} onChange={handleOnlineVendorsChecked} value={props.onlineVendors} />}
          label={t('vendorList.onlineShop')}
        />
      </FormGroup>
      <FormGroup row>
        <FormControlLabel
          control={<Switch checked={props.clickAndCollectVendors} onChange={handleClickAndCollectVendorsChecked} value={props.clickAndCollectVendors} />}
          label={t('vendorList.clickAndCollect')}
        />
      </FormGroup>
      {props.from === "Vendors" &&
        <FormGroup row>
          <FormControlLabel
            control={<Switch checked={props.reward} onChange={handleRewardChecked} value={props.reward} />}
            label={t('vendorList.reward')}
          />
        </FormGroup>
      }
      <FormGroup row>
        <FormControlLabel
          control={<Switch checked={valuesChecked} onChange={handleValuesChecked} value={valuesChecked} />}
          label={t('vendorList.values')}
        />
      </FormGroup>
      <FormGroup row>
        <FormControlLabel
          control={<Switch checked={props.cityChecked} onChange={handleCityChecked} value={props.cityChecked} />}
          label={t('vendorList.aroundMe')}
        />
      </FormGroup>
      {
        props.cityChecked && <Grid container spacing={2}>
          <Grid item>
            <ExploreIcon />
          </Grid>
          <Grid item xs>
            <Slider
              className={classes.sliderDistance}
              step={null}
              marks={marks}
              max={30}
              min={5}
              value={cityDistance}
              onChange={handleChangeDistance}
              onChangeCommitted={handleChangeCommitedDistance}
            />
          </Grid>
        </Grid>
      }
      {
        !props.cityChecked &&
        <Autosuggest
          suggestions={suggestions.slice(0, 5)}
          onSuggestionsFetchRequested={onSuggestionsFetchRequested}
          onSuggestionsClearRequested={onSuggestionsClearRequested}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          inputProps={inputProps}
          renderInputComponent={renderInputComponent}
          renderSuggestionsContainer={options => (
            <Paper {...options.containerProps} className={classes.suggestionPaper} square>
              {options.children}
            </Paper>
          )}
        />
      }
      {props.catCheckboxes && <ExpansionPanel expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
        >
          <Typography>{t('vendorList.categories')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <FormControl component="fieldset" className="formControlFilter">
            <FormGroup>{categoriesCheckboxes}</FormGroup>
          </FormControl>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      }
      {props.gdCheckboxes &&
        <ExpansionPanel expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
          >
            <Typography>{t('vendorList.genders')}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <FormControl component="fieldset" className="formControlFilter">
              <FormGroup>{gendersCheckboxes}</FormGroup>
            </FormControl>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      }
      <ExpansionPanel expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
        >
          <Typography>#{t('vendorList.healthy')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <FormControl component="fieldset" className="formControlFilter">
            <FormGroup>{healthyCheckboxes}</FormGroup>
          </FormControl>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel expanded={expanded === 'panel4'} onChange={handleChange('panel4')}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
        >
          <Typography>#{t('vendorList.ethical')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <FormControl component="fieldset" className="formControlFilter">
            <FormGroup>{ethicalCheckboxes}</FormGroup>
          </FormControl>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel expanded={expanded === 'panel5'} onChange={handleChange('panel5')}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
        >
          <Typography>#{t('vendorList.sustainable')}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <FormControl component="fieldset" className="formControlFilter">
            <FormGroup>{sustainableCheckboxes}</FormGroup>
          </FormControl>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  );
}