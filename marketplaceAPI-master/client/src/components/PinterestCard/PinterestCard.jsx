import React from 'react';
import {createStyles, makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';

import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      overflow: 'hidden',
      //backgroundColor: theme.palette.background.paper,
    },
    gridList: (props) => ({
      width: props.widthGrid || 500,
      height: 500,
      "&::-webkit-scrollbar": {
        width: props.scrollbarVisible ? "1em" : "auto"
      },
      "&::-webkit-scrollbar-track": {
        "-webkit-box-shadow": props.scrollbarVisible ? "inset 0 0 6px rgba(0,0,0,0.3)" : "none"
      },
      "&::-webkit-scrollbar-thumb": {
        borderRadius: props.scrollbarVisible ? "4px" : "auto",
        backgroundColor: props.scrollbarVisible ? "rgba(0,0,0,.3)": "none",
        "-webkit-box-shadow": props.scrollbarVisible ? "0 0 1px rgba(255,255,255,.5)": "none"
      }
    }),
    gridItem: {
      padding: "5px !important"
    },
    titleBar: {
      background:'none',
      width: '100%',
      height: '100%',
      cursor: "pointer"
    }
  }),
);

export default function PinterestCard(props) {

  const editClick = (index) => {
    if(props.editFunction) props.editFunction(index);
  }

  let indexOfFirstEmpty = props.items.findIndex((item) => item.empty);

  const classes = useStyles({widthGrid: props.width, scrollbarVisible: props.scrollbarVisible});
  return (
    <div className={classes.root}>
      <GridList cellHeight={160} className={classes.gridList} cols={props.column}>
        {props.items.map((item, index) => {
          let imgSrc =  item.imgUrl;
          return (
            <GridListTile 
              key={`item_pinterest_${index}`} 
              cols={item.col || 1} 
              className={classes.gridItem}>
              <img alt="" src={imgSrc}/>
              {props.edit && (!item.empty || indexOfFirstEmpty === index) ? 
                <GridListTileBar
                title=" "
                titlePosition="top"
                className={classes.titleBar}
                actionIcon={
                  <IconButton onClick={() => {editClick(index)}}>
                    <EditIcon />
                  </IconButton>}
                actionPosition="left"
              /> : 
              <a href={item.link} target="_blank" rel="noopener noreferrer" onClick={() => props.GADetectClick(index)}>
                <GridListTileBar
                  title=" "
                  titlePosition="top"
                  className={classes.titleBar}
                />
              </a>}
            </GridListTile>
          )
        })}
      </GridList>
    </div>
  );
}