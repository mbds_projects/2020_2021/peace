import * as React from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import Avatar from '@material-ui/core/Avatar';

import Grid from '@material-ui/core/Grid';

import Paper from '@material-ui/core/Paper';

import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
const useStyles = makeStyles((theme) =>
  createStyles({
    grid: {
      paddingTop: "20px"
    },
    rate: {
      color: theme.colors.almond
    },
    avatar: {
      width: "60px",
      height: "60px",
      margin: "auto",
      border: "1px solid grey"
    },
    text: {
      padding: "20px",
      fontSize: "20px"
    },
    paper: {
      height: "250px"
    }
  }),
);

export default function ProfessionalViewCard(props) {
  
  const calculRate = (rate, id) => {
    let iconsStar = [];
    for(let i = 0; i < 5; i++) {
      if(i < rate) {
        iconsStar.push(<StarIcon className="icons star" key={`star-${id}-${i}`} />)
      } else {
        iconsStar.push(<StarBorderIcon className="icons star_border" key={`star-${id}-${i}`}/>)
      }
    }
    return iconsStar;
  };

  const handleClick = () => {
    if (props.onClick) {
      props.onClick(props.vendor);
    }
  }


    let imgSrc  = "";

    let avatarText = `${props.vendor.name.charAt(0)}${props.vendor.name.charAt(1)}`
      if(props.vendor && props.vendor.profilePictureUrl) {
        imgSrc = props.vendor.profilePictureUrl;
        avatarText = "";
      }

    let classes = useStyles();

    let rate = props.rate ? props.rate : 0;

    return(
      <Paper className={classes.paper} onClick={handleClick}>
        <Grid container direction="row" justify="space-between" alignItems="center" className={classes.grid}>
          <Grid item xs={4}>
            <Avatar className={classes.avatar} src={imgSrc}>{avatarText}</Avatar>
          </Grid>
          <Grid item xs={8}> 
            <Grid container direction="column">
              <Grid item><h3>{props.vendor.name}</h3></Grid>
              <Grid item className={classes.rate}>{calculRate(rate, props.vendor.id)}</Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Grid item className={classes.text}>{props.text}</Grid>
          </Grid>
          
        </Grid>
        </Paper>
      
    )
  }