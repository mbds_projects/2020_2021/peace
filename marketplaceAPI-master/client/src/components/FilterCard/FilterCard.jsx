import React, { useState } from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

//import Link from '@material-ui/core/Link';

import Grid from '@material-ui/core/Grid';

import { useTranslation } from 'react-i18next';

import routerHistory from '../../shared/router-history/router-history';

import useMediaQuery from '@material-ui/core/useMediaQuery';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      margin: "10px",
      "&:hover": {
        textDecoration: "none"
      }
    },
    grid: (props) => ({
      backgroundImage: `linear-gradient(0deg, rgba(0, 0, 0, 0.5) 0%, rgba(255,255,255,0) 100%), url("${props.background}")`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      backgroundSize: "cover",
      cursor: "pointer",
      width: props.round ? ((props.size === "small") ? "100px" : "150px") : "auto",
      height: props.round ? ((props.size === "small") ? "100px" : "150px") : ((props.size === "small") ? "100px" : props.height),
      borderRadius: props.round ? "50%" : "0%",
      margin: "auto"
    }),
    gridActivate: (props) => ({
      border: props.select ? `8px solid ${theme.colors.strawberry}`: '0px',
      transition: theme.transitions.create('border'),
    }),
    gridHover: (props) => ({
      "&:hover": {
        border: props.select ? `8px solid ${theme.colors.strawberry}`: '0px',
        transition: theme.transitions.create('border'),
      }
    }),
    name: {
      color: "white",
      fontSize: "16px",
      fontWeight: "bold",
      wordBreak: "break-word",
      textAlign: "center"
    },
    centerCard: {
      padding: "5px 10px"
    }
  }),
);

export default function FilterCard(props) {

  const matches = useMediaQuery('(max-width:768px)');

  const [selected, setSelected] = useState(props.activated);

  const { i18n } = useTranslation();

  let name = props.filter.lang.find((l) => l.locale === i18n.language).value;

  //let name = value ? value.name : "";

  let background = "";

  if(props.filter.pictureUrl && props.filter.pictureUrl !== "") {
    background = props.filter.pictureUrl
  }

  let classes = useStyles({ 
    background: background, 
    size: matches ? "small" : "large", 
    height: props.height ? props.height : "200px", 
    round: props.round, select: props.selectStyle 
  });
  const handleClick = () => {
    if(props.handleClick){
      props.handleClick();
      setSelected(!selected);
    } else {
      routerHistory.push(`/vendors?${props.type}=${props.filter.id}`);
    }
  }

  return (
    <div onClick={handleClick} className={classes.root}>
      <Grid container direction="column" justify="center" className={`${classes.grid} ${matches ? '' : classes.gridHover} ${selected ? classes.gridActivate : ''}`}>
        <Grid container direction="row" className={classes.centerCard}>
            <Grid item xs={12} className={classes.name}>
              {props.type === "kw" ? `#${name}` : name}
            </Grid>    
          </Grid>
      </Grid>
    </div>
  )
}