import * as React from 'react';
import Avatar from '@material-ui/core/Avatar';

import { useSelector } from "react-redux";

import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import FlagIcon from '@material-ui/icons/Flag';

import { useTranslation } from 'react-i18next';
import { makeStyles, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) =>
  createStyles({
    flagRed: {
      color: "red !important",
      cursor: "pointer",
      position: "relative",
      top: "5px",
      fontSize: "20px"
    },
    flagDark: {
      color: "black !important",
      position: "relative",
      top: "5px",
      fontSize: "20px"
    },
    commentItem: {
      display: "flex"
    },
    commentContent: {
      marginLeft: "10px"
    },
    star: {
      position: "relative",
      top: "5px",
      fontSize: "20px",
      color: theme.colors.almond
    }
  }),
);

export default function CommentItem(props) {
  const { t, i18n } = useTranslation();
  const classes = useStyles();

  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  
  const calculRate = (rate, id) => {
    let iconsStar = [];
    for(let i = 0; i < 5; i++) {
      if(i < rate) {
        iconsStar.push(<StarIcon key={`star_${id}_${i}`} className={classes.star} />)
      } else {
        iconsStar.push(<StarBorderIcon key={`star_${id}_${i}`} className={classes.star} />)
      }
    }
    return iconsStar;
  };

  const reportComment = (commentId ) => {
    if(props.reportComment) props.reportComment(commentId);
  }

  let comment = props.comment;
  let id = comment.id;
  let postedBy = comment.postedBy;
  let rate = comment.rate
  let text = comment.text;

  let imgSrc  = "";
  let avatarText = ""
        
  if(postedBy) {
    avatarText = `${postedBy.name.charAt(0)} ${postedBy.surname.charAt(0)}`
    if(postedBy && postedBy.profilePictureUrl) {
      imgSrc = postedBy.profilePictureUrl;
      avatarText = "";
    }
  }
    
  let date = new Date(comment.date);
  let month = date.toLocaleString(i18n.language, { month: 'long' });

  return (
    <div className={classes.commentItem}>
      <Avatar style={{backgroundColor: postedBy ? postedBy.defaultColor: "grey"}} className="avatar" src={imgSrc}>{avatarText}</Avatar>
      <div className={classes.commentContent}>
        <span>
          {postedBy ? <b>{postedBy.name}</b> : t('comments.userDeleted')} {month} {date.getFullYear()}
          {calculRate(rate, id)}
          {
            !!loggedInUser && comment.postedBy && 
            loggedInUser.id !== comment.postedBy.id && 
            ((comment.reportedBy && !comment.reportedBy.includes(loggedInUser.id)) || !comment.reportedBy) &&
            <FlagIcon className={classes.flagRed} onClick={() => reportComment(id)} />      
          }
          {
            !!loggedInUser && comment.postedBy &&
            loggedInUser.id !== comment.postedBy.id && 
            comment.reportedBy && comment.reportedBy.includes(loggedInUser.id) &&
            <FlagIcon className={classes.flagDark}/>           
          }
        </span>
        <p>{text}</p>
      </div>
    </div>
  );
}
