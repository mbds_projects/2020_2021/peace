import React, { useState } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Select from 'react-select';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import {
  DatePicker
} from '@material-ui/pickers';

import frLocale from "date-fns/locale/fr";
import enLocale from "date-fns/locale/en-GB";
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) =>
  createStyles({
    date: {
      margin: "15px 0px 15px",
      width: "100%",
      "& div input": {
        margin: "0px"
      }
    },
    input: {
      "& label": {
        fontSize: "14px"
      },
      "& .MuiFormLabel-root.Mui-error": {
        color: "grey"
      },
      "& fieldset": {
        borderColor: "lightgrey !important"
      }
    },
    littleStar: {
      color: "red",
      fontSize: "10px",
      verticalAlign: "top"
    },
    select: {
      zIndex: "2"
    },
  }),
);

const locale = {
  fr: frLocale,
  en: enLocale
};

export default function DialogCreateReward(props) {
  let classes = useStyles();
  const { t, i18n } = useTranslation();

  const [message, setMessage] = useState("");
  const [value, setValue] = useState("");
  const [code, setCode] = useState("");
  const [selectedDate, setSelectedDate] = useState(null);
  const [selectedType, setSelectedType] = React.useState(null);

  const createReward = () => {
    let input = {
      code, message, value, validity: selectedDate, type: selectedType.value
    };
    props.createReward({ input });
  }

  let rewardsOptions = ["voucher", "gift", "event", "other"].map((reward) => {
    let typeName = t(`reward.${reward}`);
    return {
      value: reward,
      label: typeName
    }
  }
  );

  return (
    <Dialog open={props.openNewReward} onClose={() => props.setOpenNewReward(false)}>
      <DialogTitle>{t('reward.create')}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          <Select
            name="rewardInput"
            options={rewardsOptions}
            placeholder={<span>{t('reward.type')} <span className={classes.littleStar}>*</span></span>}
            className={classes.select}
            value={selectedType}
            onChange={(type) => setSelectedType(type)}
          />
          <TextField
            value={value}
            onChange={(e) => setValue(e.target.value)}
            fullWidth
            required
            label={t('reward.value')}
            margin="dense"
            variant="filled"
            className={classes.input}
            type="url"
          />
          <TextField
            value={message}
            onChange={(e) => setMessage(e.target.value)}
            fullWidth
            required
            label={t('reward.message')}
            margin="dense"
            variant="filled"
            className={classes.input}
            type="url"
          />
          <TextField
            value={code}
            onChange={(e) => setCode(e.target.value)}
            fullWidth
            required
            label={t('reward.code')}
            margin="dense"
            variant="filled"
            className={classes.input}
            type="url"
          />

          <MuiPickersUtilsProvider utils={DateFnsUtils} locale={locale[i18n.language]} >
            <DatePicker
              disablePast
              openTo="date"
              format="dd/MM/yyyy"
              label={t('reward.validity')}
              views={["date", "month", "year"]}
              value={selectedDate}
              onChange={(e) => setSelectedDate(e)}
              className={classes.date}
              variant="inline"
              inputVariant="outlined" />
          </MuiPickersUtilsProvider>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={createReward} disabled={!selectedType || !code || !message || !value} color="primary">
          {t('reward.create')}
        </Button>
        <Button onClick={() => props.setOpenNewReward(false)} color="primary">
          {t('reward.cancel')}
        </Button>
      </DialogActions>
    </Dialog>
  );

}