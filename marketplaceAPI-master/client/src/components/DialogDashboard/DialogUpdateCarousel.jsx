import React, { useState, useRef, useCallback } from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import Select from 'react-select';

import { useTranslation } from 'react-i18next';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

import ReactCrop from "react-image-crop";
import 'react-image-crop/dist/ReactCrop.css';

const useStyles = makeStyles((theme) =>
  createStyles({
    imgContent: {
      maxHeight: "300px",
      maxWidth: "450px",
      overflow: "hidden",
      width: "100%"
    }
  }),
);

export default function DialogUpdateCarousel(props) {
  const { t } = useTranslation();  

  const vendor = props.vendor;

  let indexOfFirstEmpty = vendor.carouselPicturesUrl && vendor.carouselPicturesUrl.length > 0 ? vendor.carouselPicturesUrl.length : 0;

  const numbers = ['first', 'second', 'third']

  const carouselTypes = [];
  for(let i = 0; i < indexOfFirstEmpty; i++) {
    carouselTypes.push({value: i, label: t(`vendorDashboard.carousel.${numbers[i]}`)})
  }
  if(indexOfFirstEmpty < 3) {
    carouselTypes.push({value: indexOfFirstEmpty, label: t(`vendorDashboard.carousel.${numbers[indexOfFirstEmpty]}`)})
  }


  const [upImg, setUpImg] = useState();
  const imgRef = useRef(null);
  const [crop, setCrop] = useState({ unit: "px", width: 450, height: 300 });
  const [previewUrl, setPreviewUrl] = useState();
  const [file, setFile] = useState();
  const [carouselNumber, setCarouselNumber] = React.useState();
  const [carouselType, setCarouselType] = React.useState();

  const classes = useStyles();

  const onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  const makeClientCrop = async (crop) => {
    if (imgRef.current && crop.width && crop.height) {
      createCropPreview(imgRef.current, crop, "newFile.png");
    }
  };

  const createCropPreview = async (image, crop, fileName) => {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = 450;
    canvas.height = 300;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      450,
      300
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          reject(new Error("Canvas is empty"));
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(previewUrl);
        setPreviewUrl(window.URL.createObjectURL(blob));

        const file = new File([blob], `${vendor.id}_carousel${carouselNumber}.png`
        ,{type:"image/png", lastModified:new Date()})
        setFile(file);
      }, "image/png");
    });
  };

  const handleCarouselType = (carouselType) => {
    setCarouselType(carouselType)
    let index = carouselType.value;
    setCarouselNumber(index);
    if(vendor.carouselPicturesUrl && vendor.carouselPicturesUrl.length > index) {
      setPreviewUrl(vendor.carouselPicturesUrl[index])
    } else {
      setPreviewUrl(null);
    }
  };

  const resetCarouselUpload = () => {
    setFile(null);
    setPreviewUrl('')
    setUpImg(null);
    setCarouselType(null)
    setCrop({ unit: "px", width: 450, height: 300 })
  }

  const handleCloseCarousel = () => {
    props.setOpenEditCarousel(false);
    resetCarouselUpload();
  }

  const handleEditCarousel = async () => {
    let upload = await props.uploadCarouselPicture(file, carouselNumber);
    if(upload) {
      props.setOpenEditCarousel(false);
      resetCarouselUpload();
    }
  }

  return (
    <Dialog
      fullWidth={true}
      open={props.openEditCarousel}
      onClose={handleCloseCarousel}>
      <DialogTitle>{t('vendorDashboard.carousel.edit')}</DialogTitle>
      <DialogContent>
        <p>{t('vendorDashboard.carousel.title')}</p>
        <p>{t('vendorDashboard.carousel.info')}</p>

        <Select
          options={carouselTypes}
          placeholder={t('vendorDashboard.carousel.select')}
          value={carouselType}
          onChange={handleCarouselType}
          styles={{ menu: base => ({ ...base, position: 'relative' }) }}
          multi={false}
        />
        <div>
          <input type="file" accept="image/*" onChange={onSelectFile} />
          <ReactCrop
            src={upImg}
            onImageLoaded={onLoad}
            crop={crop}
            onChange={c => setCrop(c)}
            onComplete={makeClientCrop}
          />
          {previewUrl && <img alt="Crop preview" className={classes.imgContent} src={previewUrl} />}
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseCarousel} color="primary">
          {t('vendorDashboard.cancel')}
        </Button>
        <Button
          disabled={!previewUrl && !carouselNumber}
          onClick={handleEditCarousel} color="primary">
          {t('vendorDashboard.update')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
          
