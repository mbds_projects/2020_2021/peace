import React, { useState } from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import Select from 'react-select';

import { useTranslation } from 'react-i18next';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

import TextField from '@material-ui/core/TextField';

import { toast } from 'react-toastify';

const useStyles = makeStyles((theme) =>
  createStyles({
    input: {
      "& .MuiFormLabel-root.Mui-error": {
        color: "grey"
      },
      "& fieldset": {
        borderColor: "lightgrey !important"
      },
      "& .MuiFilledInput-input": {
        padding: "10px 0px"
      }
    }
  }),
);

export default function DialogUpdateVideo(props) {
  const { t } = useTranslation();  

  const [videoType, setVideoType] = useState(null);
  const [video, setVideo] = useState('');

  const videoTypes = [
    {value: 'youtube', label: "YouTube"}, 
    {value: 'dailymotion', label: "Dailymotion"},
    {value: 'vimeo', label: "Vimeo"}
  ];

  const classes = useStyles();

  const handleCloseVideo = () => {
    props.setOpenEditVideo(false);
    setVideoType(null)
    setVideo('');
  }

  const handleChangeVideoType = (type) => {
    setVideoType(type);
    setVideo('')
  }

  const handleChangeVideo = (event) => {
    setVideo(event.target.value);
  }

  const handleEditVideo = () => {
    let videoYoutubeId = "";
    let videoDailymotionId = "";
    let videoVimeoId = "";
    let video_id = "";
    let ampersandPosition = -1;
    let questionMarkPosition = -1;
    let correctLink = false;
    switch(videoType.value) {
      case "youtube": 
        let splitYoutube = video.split('v=');
        if(splitYoutube.length > 1) {
          video_id = splitYoutube[1];
          ampersandPosition = video_id.indexOf('&');
          if(ampersandPosition !== -1) {
            video_id = video_id.substring(0, ampersandPosition);
          }
          videoYoutubeId = video_id;
          correctLink = true;
        } else {
          toast.error(t('vendorDashboard.toast.youtubeMalformed'), {
            position: toast.POSITION.TOP_RIGHT
          });
        }
        break;
      case "dailymotion": 
        let splitDailymotion = video.split('dailymotion.com/video/');
        if(splitDailymotion.length > 1) {
          video_id = splitDailymotion[1];
          ampersandPosition = video_id.indexOf('&');
          if(ampersandPosition !== -1) {
            video_id = video_id.substring(0, ampersandPosition);
          }
          questionMarkPosition = video_id.indexOf('?');
          if(questionMarkPosition !== -1) {
            video_id = video_id.substring(0, questionMarkPosition);
          }
          videoDailymotionId = video_id;
          correctLink = true;
        } else {
          toast.error(t('vendorDashboard.toast.dailymotionMalformed'), {
            position: toast.POSITION.TOP_RIGHT
          });
        }
        break;
      case "vimeo":  
        let splitVimeo = video.split('vimeo.com/');
        if(splitVimeo.length > 1) {
          video_id = splitVimeo[1];
          ampersandPosition = video_id.indexOf('&');
          if(ampersandPosition !== -1) {
            video_id = video_id.substring(0, ampersandPosition);
          }
          questionMarkPosition = video_id.indexOf('?');
          if(questionMarkPosition !== -1) {
            video_id = video_id.substring(0, questionMarkPosition);
          }
          videoVimeoId = video_id;
          correctLink = true;
        } else {
          toast.error(t('vendorDashboard.toast.vimeoMalformed'), {
            position: toast.POSITION.TOP_RIGHT
          });
        }
        break;
      default:
        break;
    }
    if(correctLink) {
      let vendorAttribute = { videoYoutubeId, videoDailymotionId, videoVimeoId }
      props.updateInfoVendor(vendorAttribute)
      props.setOpenEditVideo(false);
      setVideoType(null);
      setVideo("");
    }
  }

  return (
    <Dialog
      fullWidth={true}
      open={props.openEditVideo}
      onClose={handleCloseVideo}>
      <DialogTitle>{t('vendorDashboard.video.edit')}</DialogTitle>
      <DialogContent>
        <p>{t('vendorDashboard.video.title')}</p>
        <Select
          options={videoTypes}
          placeholder={t('vendorDashboard.video.select')}
          value={videoType}
          onChange={handleChangeVideoType}
          styles={{ menu: base => ({ ...base, position: 'relative' }) }}
          multi={false}
        />
        {
          videoType &&
          <TextField
            value={video}
            onChange={handleChangeVideo}
            fullWidth
            margin="dense"
            variant="filled"
            className={classes.input}
            type="url"
          />
        }
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseVideo} color="primary">
          {t('vendorDashboard.cancel')}
        </Button>
        <Button onClick={handleEditVideo} color="primary">
          {t('vendorDashboard.update')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
          
