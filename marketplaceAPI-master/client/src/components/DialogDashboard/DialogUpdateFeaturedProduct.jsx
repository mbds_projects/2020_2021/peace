import React, { useState, useEffect, useRef, useCallback } from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import { useTranslation } from 'react-i18next';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

import EuroIcon from '@material-ui/icons/Euro';
import LinkIcon from '@material-ui/icons/Link';

import ReactCrop from "react-image-crop";
import 'react-image-crop/dist/ReactCrop.css';

import { toast } from 'react-toastify';
const useStyles = makeStyles((theme) =>
  createStyles({
    imgContentSquare: {
      maxHeight: "300px",
      maxWidth: "300px",
      overflow: "hidden",
      width: "100%"
    },
    imgContentRectangle: {
      maxHeight: "300px",
      maxWidth: "450px",
      overflow: "hidden",
      width: "100%"
    },
    icon: {
      marginTop: "20px"
    }
  }),
);

export default function DialogUpdateFeaturedProduct(props) {
  const { t } = useTranslation();  

  const vendor = props.vendor;

  const [upImg, setUpImg] = useState();
  const imgRef = useRef(null);

  let initCrop = { unit: "px", width: (props.featuredProductNumber === 0 || props.featuredProductNumber === 3 || props.featuredProductNumber === 4) ? 450 : 300, height: 300 }
  const [crop, setCrop] = useState(initCrop);
  const [previewUrl, setPreviewUrl] = useState();
  const [file, setFile] = useState();


  const [price, setPrice] = useState('');
  const [link, setLink] = useState('');

  const classes = useStyles();

  useEffect(() => {
    let didCancel = false;
    let initCropAtUseEffect = { unit: "px", width: (props.featuredProductNumber === 0 || props.featuredProductNumber === 3 || props.featuredProductNumber === 4) ? 450 : 300, height: 300 }
    !didCancel && setCrop(initCropAtUseEffect);
    if(vendor && vendor.featuredProductsUrl && vendor.featuredProductsUrl.length > props.featuredProductNumber) {
      !didCancel && setPreviewUrl(vendor.featuredProductsUrl[props.featuredProductNumber].imgUrl)
      !didCancel && setPrice(vendor.featuredProductsUrl[props.featuredProductNumber].price)
      !didCancel && setLink(vendor.featuredProductsUrl[props.featuredProductNumber].link)
    } else {
      !didCancel && setPreviewUrl(null)
      !didCancel && setPrice('')
      !didCancel && setLink('')
    }
    return () => { didCancel = true }
  }, [vendor, props.featuredProductNumber])

  const onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  const makeClientCrop = async (crop) => {
    if (imgRef.current && crop.width && crop.height) {
      createCropPreview(imgRef.current, crop, "newFile.png");
    }
  };

  const createCropPreview = async (image, crop, fileName) => {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    if(props.featuredProductNumber === 0 || props.featuredProductNumber === 3 || props.featuredProductNumber === 4){
      canvas.width = 450;
      canvas.height = 300;
    } else {      
      canvas.width = 300;
      canvas.height = 300;
    }
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      canvas.width,
      canvas.height
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          reject(new Error("Canvas is empty"));
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(previewUrl);
        setPreviewUrl(window.URL.createObjectURL(blob));

        const file = new File([blob], `${vendor.id}_featuredProduct${props.featuredProductNumber}.png`
        ,{type:"image/png", lastModified:new Date()})
        setFile(file);
      }, "image/png");
    });
  };

  const resetFeaturedProductUpload = () => {
    setFile(null);
    setPreviewUrl('')
    setUpImg(null);
    setCrop({ unit: "px", width: 300, height: 300 })
  }

  const handleCloseFeaturedProduct = () => {
    props.setOpenEditFeaturedProduct(false);
    resetFeaturedProductUpload();
  }

  const handleEditFeaturedProduct = async () => {
    let res;

    if(!(link.startsWith("http://") || link.startsWith("https://"))) {
      toast.error(t('vendorDashboard.toast.linkMalformed'), {
        position: toast.POSITION.TOP_RIGHT
      });
    } else {
      if(file) {
        res = await props.uploadFeaturedProductPicture(file, props.featuredProductNumber, link, price);
      } else {
        res = await props.uploadFeaturedProductInfo(props.featuredProductNumber, link, price)
      }
      if(res) {
        props.setOpenEditFeaturedProduct(false);
        resetFeaturedProductUpload();
      }
    }
  }

  const handleLink = (event) => {
    setLink(event.target.value);
  };

  const handlePrice = (event) => {
    setPrice(parseFloat(event.target.value));
  };


  return (
    <Dialog
      fullWidth={true}
      open={props.openEditFeaturedProduct}
      onClose={handleCloseFeaturedProduct}>
      <DialogTitle>{t('vendorDashboard.featuredProduct.edit')}</DialogTitle>
      <DialogContent>
        <p>{t('vendorDashboard.featuredProduct.title')}</p>
        <p>{t(`vendorDashboard.featuredProduct.info${(props.featuredProductNumber === 0 || props.featuredProductNumber === 3 || props.featuredProductNumber === 4) ? 'Rectangle': 'Square'}`)}</p>
        <div>
          <Grid container direction="row">
            <Grid item xs={2} sm={1}>
              <EuroIcon className={classes.icon} />
            </Grid>
            <Grid item xs={10} sm={11}>
              <TextField
                value={price}
                onChange={handlePrice}
                label="prix"
                fullWidth
                margin="dense"
                variant="filled"
                className={classes.input}
                type="number"
              />
            </Grid>
            <Grid item xs={2} sm={1}>
              <LinkIcon className={classes.icon} />
            </Grid>
            <Grid item xs={10} sm={11}>
              <TextField
                value={link}
                onChange={handleLink}
                fullWidth
                label="lien de redirection"
                margin="dense"
                variant="filled"
                className={classes.input}
                type="url"
              />
            </Grid>
          </Grid>
          <input type="file" accept="image/*" onChange={onSelectFile} />
          <ReactCrop
            src={upImg}
            onImageLoaded={onLoad}
            crop={crop}
            onChange={c => setCrop(c)}
            onComplete={makeClientCrop}
          />
          {previewUrl && <img alt="Crop preview" className={classes[`imgContent${(props.featuredProductNumber === 0 || props.featuredProductNumber === 3 || props.featuredProductNumber === 4) ? 'Rectangle': 'Square'}`]} src={previewUrl} />}
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseFeaturedProduct} color="primary">
          {t('vendorDashboard.cancel')}
        </Button>
        <Button
          disabled={!(previewUrl && price && link)}
          onClick={handleEditFeaturedProduct} color="primary">
          {t('vendorDashboard.update')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
          
