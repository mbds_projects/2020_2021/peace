import React, { useState, useRef, useCallback } from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

import ReactCrop from "react-image-crop";
import 'react-image-crop/dist/ReactCrop.css';

const useStyles = makeStyles((theme) =>
  createStyles({
    imgContent: {
      maxHeight: "200px",
      maxWidth: "200px",
      overflow: "hidden",
      width: "100%"
    }
  }),
);

export default function DialogUpdateLogo(props) {
  const { t } = useTranslation();  

  const [upImg, setUpImg] = useState();
  const imgRef = useRef(null);
  const [crop, setCrop] = useState({ unit: "px", width: 200, height: 200 });
  const [previewUrl, setPreviewUrl] = useState();
  const [file, setFile] = useState();

  const classes = useStyles();

  const onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  const makeClientCrop = async (crop) => {
    if (imgRef.current && crop.width && crop.height) {
      createCropPreview(imgRef.current, crop, "newFile.png");
    }
  };

  const createCropPreview = async (image, crop, fileName) => {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = 200;
    canvas.height = 200;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      200,
      200
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          reject(new Error("Canvas is empty"));
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(previewUrl);
        setPreviewUrl(window.URL.createObjectURL(blob));

        const file = new File([blob], `${props.vendor.id}_logo.png`
        ,{type:"image/png", lastModified:new Date()})
        setFile(file);
      }, "image/png");
    });
  };


  const resetLogoUpload = () => {
    setFile(null);
    setPreviewUrl('')
    setUpImg(null);
    setCrop({ unit: "px", width: 200, height: 200 })
  }

  const handleCloseLogo = () => {
    props.setOpenEditLogo(false);
    resetLogoUpload();
  }

  const handleEditLogo = async () => {
    let upload = await props.uploadLogo(file);
    if(upload) {
      props.setOpenEditLogo(false);
      resetLogoUpload();
    }
  }

  return (
    <Dialog
      open={props.openEditLogo}
      onClose={handleCloseLogo}>
      <DialogTitle>{t('vendorAccount.logo.title')}</DialogTitle>
      <DialogContent>
        <p>{t('vendorAccount.logo.info')}</p>

        <div>
          <input type="file" accept="image/*" onChange={onSelectFile} />
          <ReactCrop
            src={upImg}
            onImageLoaded={onLoad}
            crop={crop}
            onChange={c => setCrop(c)}
            onComplete={makeClientCrop}
          />
          {previewUrl && <img alt="Crop preview" className={classes.imgContent} src={previewUrl} />}
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseLogo} color="primary">
          {t('vendorDashboard.cancel')}
        </Button>
        <Button
          disabled={!previewUrl}
          onClick={handleEditLogo} color="primary">
          {t('vendorDashboard.update')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
          
