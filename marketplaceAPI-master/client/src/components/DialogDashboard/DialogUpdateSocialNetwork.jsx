import React, { useState, useEffect } from 'react';

import Grid from '@material-ui/core/Grid';

import { useTranslation } from 'react-i18next';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

import TextField from '@material-ui/core/TextField';

import IconButton from '@material-ui/core/IconButton';
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import LanguageIcon from '@material-ui/icons/Language';
import YouTubeIcon from '@material-ui/icons/YouTube';
import TwitterIcon from '@material-ui/icons/Twitter';
import PinterestIcon from '@material-ui/icons/Pinterest';

import { toast } from 'react-toastify';

import { useStyles } from './StylesSocialNetworks';

export default function DialogUpdateSocialNetwork(props) {
  const { t } = useTranslation();  

  const vendor = props.vendor;

  const [fb, setFb] = useState('');
  const [insta, setInsta] = useState('');
  const [twitter, setTwitter] = useState('');
  const [pinterest, setPinterest] = useState('');
  const [youtube, setYoutube] = useState('');
  const [site, setSite] = useState('');

  const classes = useStyles();

  useEffect(() => {
    let didCancel = false;
    if(vendor.socialNetworks.find((el) => el.type === "facebook")) !didCancel && setFb(vendor.socialNetworks.find((el) => el.type === "facebook").url || '');
    if(vendor.socialNetworks.find((el) => el.type === "instagram")) !didCancel && setInsta(vendor.socialNetworks.find((el) => el.type === "instagram").url || '');
    if(vendor.socialNetworks.find((el) => el.type === "twitter")) !didCancel && setTwitter(vendor.socialNetworks.find((el) => el.type === "twitter").url || '');
    if(vendor.socialNetworks.find((el) => el.type === "pinterest")) !didCancel && setPinterest(vendor.socialNetworks.find((el) => el.type === "pinterest").url || '');
    if(vendor.socialNetworks.find((el) => el.type === "youtube")) !didCancel && setYoutube(vendor.socialNetworks.find((el) => el.type === "youtube").url || '');
    if(vendor.site || (vendor.socialNetworks.find(sn => sn.type === "website"))) !didCancel && setSite(vendor.site || vendor.socialNetworks.find(el => el.type === "website").url || '');
  
    return () => { didCancel = true }
  }, [vendor])

  const handleCloseEditNetworks = () => {
    props.setOpenEditNetworks(false);
  }

  const handleChangeInsta = (event) => {
    setInsta(event.target.value);
  }

  const handleChangeFb = (event) => {
    setFb(event.target.value);
  }

  const handleChangeTwitter = (event) => {
    setTwitter(event.target.value);
  }

  const handleChangePinterest = (event) => {
    setPinterest(event.target.value);
  }

  const handleChangeYoutube = (event) => {
    setYoutube(event.target.value);
  }

  const handleChangeSite = (event) => {
    setSite(event.target.value);
  }

  const handleEditNetworks = () => {
    let socialNetworks = [];
    if(site !== '') {
      if(site.startsWith("http://") || site.startsWith("https://")) {
        socialNetworks.push({type: "website", url: site})
      } else {
        toast.error(t('vendorDashboard.toast.websiteMalformed'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    if(fb !== '') {
      if(fb.startsWith("https://www.facebook.com/")) {
        socialNetworks.push({type: "facebook", url: fb})
      } else {
        toast.error(t('vendorDashboard.toast.facebookMalformed'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    if(twitter !== '') {
      if(twitter.startsWith("https://twitter.com/")) {
        socialNetworks.push({type: "twitter", url: twitter})
      } else {
        toast.error(t('vendorDashboard.toast.twitterMalformed'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    if(insta !== '') {
      if(insta.startsWith("https://www.instagram.com/")) {
        socialNetworks.push({type: "instagram", url: insta})
      } else {
        toast.error(t('vendorDashboard.toast.instagramMalformed'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    if(pinterest !== '') {
      if(pinterest.startsWith("https://www.pinterest.fr/")) {
        socialNetworks.push({type: "pinterest", url: pinterest})
      } else {
        toast.error(t('vendorDashboard.toast.pinterestMalformed'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    if(youtube !== '') {
      if(youtube.startsWith("https://www.youtube.com/channel/")) {
        socialNetworks.push({type: "youtube", url: youtube})
      } else {
        toast.error(t('vendorDashboard.toast.youtubeChannelMalformed'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    let vendorAttribute = {
      socialNetworks: socialNetworks,
      site: site
    }
    props.updateInfoVendor(vendorAttribute)
    props.setOpenEditNetworks(false);
  }

  return (
    <Dialog
    fullWidth={true}
      open={props.openEditNetworks}
      onClose={handleCloseEditNetworks}>
      <DialogTitle>{t('vendorDashboard.networks.edit')}</DialogTitle>
      <DialogContent>
        <p>{t('vendorDashboard.networks.title')}</p>
        <Grid container direction="row">
          <Grid item xs={2} sm={1}>
            <IconButton
              color="primary"
              classes={{ colorPrimary: (vendor.socialNetworks.find((el) => el.type === "instagram") ? classes.instagram : classes.socialNetworkEmpty) }}
            >
              <InstagramIcon />
            </IconButton>
          </Grid>
          <Grid item xs={10} sm={11}>
            <TextField
              value={insta}
              onChange={handleChangeInsta}
              fullWidth
              margin="dense"
              variant="filled"
              className={classes.input}
              type="url"
            />
          </Grid>
          <Grid item xs={2} sm={1}>
            <IconButton
              color="primary"
              classes={{ colorPrimary: (vendor.socialNetworks.find((el) => el.type === "facebook") ? classes.facebook : classes.socialNetworkEmpty) }}
            >
              <FacebookIcon />
            </IconButton>
          </Grid>
          <Grid item xs={10} sm={11}>
            <TextField
              value={fb}
              onChange={handleChangeFb}
              fullWidth
              margin="dense"
              variant="filled"
              className={classes.input}
              type="url"
            />
          </Grid>
          <Grid item xs={2} sm={1}>
            <IconButton
              color="primary"
              classes={{ colorPrimary: (vendor.socialNetworks.find((el) => el.type === "twitter") ? classes.twitter : classes.socialNetworkEmpty) }}
            >
              <TwitterIcon />
            </IconButton>
          </Grid>
          <Grid item xs={10} sm={11}>
            <TextField
              value={twitter}
              onChange={handleChangeTwitter}
              fullWidth
              margin="dense"
              variant="filled"
              className={classes.input}
              type="url"
            />
          </Grid>
          <Grid item xs={2} sm={1}>
            <IconButton
              color="primary"
              classes={{ colorPrimary: (vendor.socialNetworks.find((el) => el.type === "pinterest") ? classes.pinterest : classes.socialNetworkEmpty) }}
            >
              <PinterestIcon />
            </IconButton>
          </Grid>
          <Grid item xs={10} sm={11}>
            <TextField
              value={pinterest}
              onChange={handleChangePinterest}
              fullWidth
              margin="dense"
              variant="filled"
              className={classes.input}
              type="url"
            />
          </Grid>
          <Grid item xs={2} sm={1}>
            <IconButton
              color="primary"
              classes={{ colorPrimary: (vendor.socialNetworks.find((el) => el.type === "youtube") ? classes.youtube : classes.socialNetworkEmpty) }}
            >
              <YouTubeIcon />
            </IconButton>
          </Grid>
          <Grid item xs={10} sm={11}>
            <TextField
              value={youtube}
              onChange={handleChangeYoutube}
              fullWidth
              margin="dense"
              variant="filled"
              className={classes.input}
              type="url"
            />
          </Grid>
          <Grid item xs={2} sm={1}>
            <IconButton
              color="primary"
              classes={{ colorPrimary: (vendor.socialNetworks.find((el) => el.type === "website") ? classes.website : classes.socialNetworkEmpty) }}
            >
              <LanguageIcon />
            </IconButton>
          </Grid>
          <Grid item xs={10} sm={11}>
            <TextField
              value={site}
              onChange={handleChangeSite}
              fullWidth
              margin="dense"
              variant="filled"
              className={classes.input}
              type="url"
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseEditNetworks} color="primary">
          {t('vendorDashboard.cancel')}
        </Button>
        <Button onClick={handleEditNetworks} color="primary">
          {t('vendorDashboard.update')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
          
