import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) =>
  createStyles({
    socialNetworkEmpty: {
      marginLeft: "0px",
      "& .MuiIconButton-label": {
        color: "white",
        padding: "2px",
        borderRadius: "3px",
        background: "lightgrey"
      }
    },
    instagram: {
      marginLeft: "0px",
      "& .MuiIconButton-label": {
        color: "white",
        padding: "2px",
        borderRadius: "3px",
        background: theme.colors.instagram
      }
    },
    facebook: {
      "& .MuiIconButton-label": {
        color: "white",
        padding: "2px",
        borderRadius: "3px",
        backgroundColor: theme.colors.facebook
      }
    },
    twitter: {
      "& .MuiIconButton-label": {
        color: "white",
        padding: "2px",
        borderRadius: "3px",
        backgroundColor: "#00acee"
      }
    },
    pinterest: {
      "& .MuiIconButton-label": {
        color: "white",
        padding: "2px",
        borderRadius: "3px",
        backgroundColor: "#c8232c"
      }
    },
    youtube: {
      "& .MuiIconButton-label": {
        color: "white",
        padding: "2px",
        borderRadius: "3px",
        backgroundColor: theme.colors.youtube
      }
    },
    website: {
      "& .MuiIconButton-label": {
        color: "white",
        padding: "2px",
        borderRadius: "3px",
        backgroundColor: theme.colors.almond
      }
    },
    followTitle: {
      fontWeight: "bold",
      fontSize: "22px"
    },
    socialNetworksMobile: {
      paddingLeft: "10px"
    },
    input: {
      "& .MuiFormLabel-root.Mui-error": {
        color: "grey"
      },
      "& fieldset": {
        borderColor: "lightgrey !important"
      },
      "& .MuiFilledInput-input": {
        padding: "10px 0px"
      }
    }
  }),
);