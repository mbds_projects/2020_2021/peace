import React, { useState, useEffect } from 'react';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { makeStyles, createStyles } from '@material-ui/core/styles';

import { useTranslation } from 'react-i18next';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) =>
  createStyles({
    textarea: {
        width: '100%'
    }
  }),
);

export default function DialogUpdateDesc(props) {
  const { t } = useTranslation();  
  
  const [descFr, setDescFr] = useState('');
  const [descEn, setDescEn] = useState('');

  const vendor = props.vendor;

  useEffect(() => {
    let didCancel = false;
    if(vendor.desc.lang.find((l) => l.locale === "fr")) !didCancel && setDescFr(vendor.desc.lang.find((l) => l.locale === "fr").value || '');
    if(vendor.desc.lang.find((l) => l.locale === "en-GB")) !didCancel && setDescEn(vendor.desc.lang.find((l) => l.locale === "en-GB").value || '');
    
    return () => { didCancel = true }
  }, [vendor])

  const classes = useStyles();

  const handleDescFr = (event) => {
    setDescFr(event.target.value)
  }

  const handleDescEn = (event) => {
    setDescEn(event.target.value)
  }

  const handleEditDesc = () => {
    let descFrTmp = descFr;
    descFrTmp = descFrTmp.replace(/[\n\r]/g, '\\n');
    let descEnTmp = descEn;
    descEnTmp = descEnTmp.replace(/[\n\r]/g, '\\n');
    let vendorAttribute = {
      desc: {
        lang: [{
            locale: "fr",
            value: descFrTmp
        }, {
            locale: "en-GB",
            value: descEnTmp
        }]
      }
    }
    props.updateInfoVendor(vendorAttribute);
    props.setOpenEditDesc(false);
  }

  const handleCloseEditDesc = () => {
    props.setOpenEditDesc(false);
  }

  return (
    <Dialog
    fullWidth={true}
      open={props.openEditDesc}
      onClose={handleCloseEditDesc}>
      <DialogTitle>{t('vendorDashboard.desc.edit')}</DialogTitle>
      <DialogContent>
        <p>{t('vendorDashboard.desc.title')}</p>
        <TextareaAutosize
          rows={4}
          onChange={handleDescFr}
          placeholder={t('vendorDashboard.desc.fr')}
          value={descFr.replace(/\\n/g, '\n')}
          className={classes.textarea}
        />
        <br />

        <TextareaAutosize
          rows={4}
          onChange={handleDescEn}
          placeholder={t('vendorDashboard.desc.en')}
          value={descEn.replace(/\\n/g, '\n')}
          className={classes.textarea}
        />

      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseEditDesc} color="primary">
          {t('vendorDashboard.cancel')}
        </Button>
        <Button onClick={handleEditDesc} color="primary">
          {t('vendorDashboard.update')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
          
