import React from 'react';
import Button from '@material-ui/core/Button';
import { useTranslation } from 'react-i18next';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


export default function DialogDisableReward(props) {
  const { t } = useTranslation();

  const handleCloseDisable = () => {
    props.setOpenDisableReward(false);
  };

  const handleDisableReward = () => {
    props.setOpenDisableReward(false);
    props.doAction();
  }

  return (
    <Dialog open={props.openDisableReward} onClose={handleCloseDisable}>
      <DialogTitle>{t('reward.disableTitle')}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {t(`reward.disableInfo`)}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleDisableReward} color="primary">
          {t('reward.disable')}
        </Button>
        <Button onClick={handleCloseDisable} color="primary">
          {t('reward.cancel')}
        </Button>
      </DialogActions>
    </Dialog>
  )
}