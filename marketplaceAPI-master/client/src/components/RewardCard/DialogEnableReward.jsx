import React from 'react';
import Button from '@material-ui/core/Button';
import { useTranslation } from 'react-i18next';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function DialogEnableReward(props) {
  const { t } = useTranslation();

  const handleCloseEnable = () => {
    props.setOpenEnableReward(false);
  };

  const handleEnableReward = () => {
    props.setOpenEnableReward(false);
    props.doAction();
  }

  return (
    <Dialog open={props.openEnableReward} onClose={handleCloseEnable}>
      <DialogTitle>{t('reward.enableTitle')}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {t(`reward.enableInfo`)}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleEnableReward} color="primary">
          {t('reward.enable')}
        </Button>
        <Button onClick={handleCloseEnable} color="primary">
          {t('reward.cancel')}
        </Button>
      </DialogActions>
    </Dialog>
  )
}