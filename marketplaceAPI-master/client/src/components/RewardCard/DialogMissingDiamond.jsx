import React, { useState } from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import { useTranslation } from 'react-i18next';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Trans } from 'react-i18next';

const useStyles = makeStyles((theme) =>
  createStyles({
    howToClick: {
      fontStyle: "italic",
      textDecoration: "underline",
      cursor: "pointer"
    }
  }),
);

export default function DialogMissingDiamond(props) {
  const { t } = useTranslation();

  let classes = useStyles();

  const handleCloseMissingDiamond = (howToOpen = false) => {
    props.setMissingDiamond(false);
    if (howToOpen) props.howToOpen();
  }

  return (
    <Dialog open={props.missingDiamond} onClose={() => handleCloseMissingDiamond(false)}>
      <DialogTitle>{t('reward.missingDiamondTitle')}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {t('reward.missingDiamond')}
          <Trans i18nKey="reward.diamondExplanation"
            components={[<br />, <span onClick={() => handleCloseMissingDiamond(true)} className={classes.howToClick}></span>]} />

        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => handleCloseMissingDiamond(false)} color="primary">
          {t('reward.close')}
        </Button>
      </DialogActions>
    </Dialog>

  )
}