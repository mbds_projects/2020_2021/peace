import React, { useState } from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import routerHistory from '../../shared/router-history/router-history';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import LoyaltyIcon from '@material-ui/icons/Loyalty';
import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import EventIcon from '@material-ui/icons/Event';
import EmojiEventsIcon from '@material-ui/icons/EmojiEvents';
import TimerIcon from '@material-ui/icons/Timer';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import ClearIcon from '@material-ui/icons/Clear';

import { useSelector } from "react-redux";
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';

import DialogUnlockReward from './DialogUnlockReward'
import DialogMissingDiamond from './DialogMissingDiamond'
import DialogDisableReward from './DialogDisableReward'
import DialogEnableReward from './DialogEnableReward'

import copy from 'clipboard-copy'

import { formatDistanceToNow } from 'date-fns'

import ReactGA from 'react-ga';

import frLocale from "date-fns/locale/fr";
import enLocale from "date-fns/locale/en-GB";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: (props) => ({
      margin: "10px",
      height: "200px",
      //borderRadius: "15px",
      backgroundImage: `linear-gradient(0deg, rgba(0, 0, 0, 0.5) 0%, rgba(255,255,255,0) 30%), url("${props.background}")`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "top",
      backgroundSize: "cover"
    }),
    vendorName: {
      color: "white",
      fontSize: "18px",
      fontWeight: "bold",
      textAlign: "center",
      textShadow: "0.1em 0.1em 0.2em black",
      overflow: "hidden",
      whiteSpace: "nowrap",
      textOverflow: "ellipsis",
      cursor: "pointer"
      /*wordBreak: "break-word"*/
    },
    bottomCard: {
      padding: "5px 10px",
      cursor: "pointer"
    },
    grow: {
      flexGrow: 1
    },
    opacity: {
      backgroundColor: "rgba(255,255,255,0.7)",
      height: "200px"
    },
    icon: {
      fontSize: "20px"
    },
    loyaltyButton: {
      backgroundColor: "white",
      fontSize: "15px",
      width: "100%",
      top: "50%",
      height: "60px",
      marginTop: "-50px"
    },
    loyaltyIcon: {
      fontSize: "20px",
      position: "relative",
      top: "5px",
      left: "5px"
    },
    explanation: {
      textAlign: "center",
      fontWeight: "bold",
      marginTop: "5px"
      //backgroundColor: "white",
      //border: "1px solid black",
      //width: "70%"
    },
    code: {
      backgroundColor: "white",
      border: "1px solid black",
      padding: "10px",
      width: "70%"
    },
    message: {
      fontSize: "12px",
      //fontStyle: "italic",
      padding: "10px",
      fontWeight: "normal"
    },
    copyIcon: {
      padding: "10px 0px 10px 5px"
    },
    untilDate: {
      width: "100%",
      textAlign: "right",
      paddingRight: "5px",
      "& svg": {
        right: "5px",
        position: "relative",
        top: "5px"
      }
    },
    rewardTitle: {
      width: "100%",
      textAlign: "left",
      paddingRight: "5px",
      "& svg": {
        left: "5px",
        position: "relative",
        top: "5px"
      }
    },
    shortCode: {
      overflow: "hidden",
      whiteSpace: "nowrap",
      textOverflow: "ellipsis",
      width: "150px",
      display: "inline-block",
      verticalAlign: "middle"
    },
    around: {
      height: "30px"
    },
    center: {
      height: "140px"
    },
    icon: {
      cursor: 'pointer'
    }
  }),
);


export default function RewardCard(props) {
  const { t, i18n } = useTranslation();

  const handleClickVendor = () => {
    ReactGA.event({
      category: 'Rewards',
      action: `Click on vendor from a reward ${props.reward.value}`,
      label: `Vendor ${props.reward.vendor.name}`
    });
    routerHistory.push('/vendors/' + props.reward.vendor.id + "?name=" + props.reward.vendor.name);
  }

  const GADetectClick = () => {
    ReactGA.event({
      category: 'Rewards',
      action: `Click on reward ${props.reward.value}`,
      label: `Vendor ${props.reward.vendor.name}`
    });
  }

  const loggedInUser = useSelector((state) => state.authentication.currentUser);

  const [isClicked, setIsClicked] = useState(false);
  const [unlockReward, setUnlockReward] = useState(false);
  const [missingDiamond, setMissingDiamond] = useState(false);
  const [openDisableReward, setOpenDisableReward] = useState(false);
  const [openEnableReward, setOpenEnableReward] = useState(false);

  let alreadyUnlock = loggedInUser && loggedInUser.rewards && loggedInUser.rewards.length > 0 && loggedInUser.rewards.includes(props.reward.id)
  let achievementsPoints = loggedInUser && loggedInUser.achievements && loggedInUser.achievements.filter(achievement => achievement.activated).reduce((accumulator, currentValue) => accumulator + currentValue.value, 0) - loggedInUser.rewards.length;
  if (achievementsPoints < 0) achievementsPoints = 0;

  let imgSrc = "";
  if (props.reward.vendor.profilePictureUrl) {
    imgSrc = props.reward.vendor.profilePictureUrl;
  }

  let classes = useStyles({ background: imgSrc });

  const localeMap = {
    fr: frLocale,
    en: enLocale
  };

  let untilDate = props.reward.validity ? formatDistanceToNow(new Date(props.reward.validity), { locale: localeMap[i18n.language], addSuffix: true }) : " "

  const displayCoupon = () => {
    if (loggedInUser) {
      if (achievementsPoints > 0) { setUnlockReward(true); setMissingDiamond(false); }
      else { setMissingDiamond(true); setUnlockReward(false); }
    } else {
      props.checkLogin()
    }
    GADetectClick();
  }

  const copyToClipBoard = () => {
    copy(props.reward.code);

    toast.success(t('reward.copied'), {
      position: toast.POSITION.TOP_RIGHT
    });
  }

  const icons = {
    gift: <CardGiftcardIcon />,
    voucher: <LoyaltyIcon />,
    event: <EventIcon />,
    default: <EmojiEventsIcon />
  }
  return (
    <div className={classes.root}>
      {
        <DialogUnlockReward reward={props.reward} setUnlockReward={setUnlockReward} setIsClicked={setIsClicked} updateRewards={props.updateRewards} unlockReward={unlockReward} />
      }
      {
        <DialogMissingDiamond missingDiamond={missingDiamond} setMissingDiamond={setMissingDiamond} howToOpen={props.howToOpen} />
      }
      {
        <DialogDisableReward openDisableReward={openDisableReward} setOpenDisableReward={setOpenDisableReward} doAction={props.disable} />
      }
      {
        <DialogEnableReward openEnableReward={openEnableReward} setOpenEnableReward={setOpenEnableReward} doAction={props.enable} />
      }
      <Grid container direction="row" justify="center" alignItems="center" className={classes.opacity} >
        {
          props.editMode ?
            <Grid container >
              <Grid item xs={6} className={classes.around}>
                <div>{props.reward.activated ? <ClearIcon onClick={() => setOpenDisableReward(true)} className={classes.icon}/> : <CheckCircleOutlineIcon onClick={() => setOpenEnableReward(true)} className={classes.icon}/>}</div>
              </Grid>
              <Grid item xs={6} className={classes.around}>
                <div className={classes.untilDate}>{props.reward.validity && <TimerIcon />}{untilDate}</div>
              </Grid>
            </Grid>
            :
            <Grid item xs={12} className={classes.around}>
              <div className={classes.untilDate}>{props.reward.validity && <TimerIcon />}{untilDate}</div>
            </Grid>
        }
        {
          (alreadyUnlock || isClicked) ?

            <Grid item xs={10} className={classes.center}>
              <div className={classes.explanation}>
                {props.reward.value}
                <br />
                {
                  props.reward.code &&
                  <span className={classes.code}>
                    <span className={classes.shortCode}>{props.reward.code}</span>
                    <IconButton className={classes.copyIcon} onClick={copyToClipBoard}>
                      <FileCopyIcon />
                    </IconButton><br />
                  </span>
                }
                <span className={classes.message}>{props.reward.message}</span>
              </div>
            </Grid>
            :

            <Grid item xs={9} className={classes.center}>
              <Button
                variant="contained"
                className={classes.loyaltyButton}
                onClick={displayCoupon}
                endIcon={icons[props.reward.type] ? icons[props.reward.type] : icons.default}>
                {props.reward.value}
              </Button>
            </Grid>
        }
        <Grid item xs={10} className={classes.around}>
          <div className={classes.vendorName} onClick={handleClickVendor}>{props.reward.vendor.name}</div>
        </Grid>
      </Grid>
    </div>

  )
}