import React from 'react';
import Button from '@material-ui/core/Button';
import { useTranslation } from 'react-i18next';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import ReactGA from 'react-ga';

export default function DialogUnlockReward(props) {
    const { t } = useTranslation();

    const GADetectUnlock = () => {
      ReactGA.event({
        category: 'Rewards',
        action: `Unlock a reward ${props.reward.value}`,
        label: `Vendor ${props.reward.vendor.name}`
      });
    }

    const GADetectCancel = () => {
      ReactGA.event({
        category: 'Rewards',
        action: `Cancel a reward ${props.reward.value}`,
        label: `Vendor ${props.reward.vendor.name}`
      });
    }


    const handleCloseUnlockReward = () => {
      props.setUnlockReward(false);
      GADetectCancel();
    };

    const handleUnlockReward = () => {
      props.setUnlockReward(false);
      props.setIsClicked(true);
      props.updateRewards(props.reward.id)
      GADetectUnlock();
    }

    return(
          <Dialog open={props.unlockReward} onClose={handleCloseUnlockReward}>
            <DialogTitle>{t('reward.unlockTitle')}</DialogTitle>
            <DialogContent>
              <DialogContentText>
                {t('reward.unlockReward')}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleUnlockReward} color="primary">
                {t('reward.unlock')}
              </Button>
              <Button onClick={handleCloseUnlockReward} color="primary">
                {t('reward.cancel')}
              </Button>
            </DialogActions>
          </Dialog>
    )
  }