import * as React from 'react';

import Slider from "react-slick";

import { makeStyles, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
  createStyles({
    root: (props) => ({
      width: "100%",
      margin: "auto",
      "& .slick-slide img": {
        width: `${props.width} !important`,
        margin: "auto"
      },
      "& .slick-prev:before": {
        color: `${props.arrowsColor}`
      },
      "& .slick-next:before": {
        color: `${props.arrowsColor}`
      }
    })
  }),
);

 
export default function Carousel(props) {
  const classes = useStyles({ 
    width: props.width || "100%",
    arrowsColor: props.arrowsColor
  });

  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", position: "absolute", right: "10px", zIndex: 20 }}
        onClick={onClick}
      />
    );
  }
  
  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", position: "absolute", left: "10px", zIndex: 20 }}
        onClick={onClick}
      />
    );
  }

  
  const settings = {
    dots: props.dots,
    infinite: props.infinite,
    speed: 500,
    slidesToShow: props.slides || 1,
    slidesToScroll: props.slides || 1,
    arrows: props.arrows,
    arrowsColor: props.arrowsColor || "white",
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    autoplay: props.autoplay,
    autoplaySpeed: props.autoplaySpeed || 2000,
  };

  return (
    <Slider {...settings} className={classes.root}>
      {props.carouselComponent}
    </Slider>
);
}
