import React, { useEffect, useState } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useIdleTimer } from 'react-idle-timer'
import { makeStyles, createStyles } from '@material-ui/core/styles';
//import AddCommentItem from "../../components/AddCommentItem/AddCommentItem";
import { useTranslation } from 'react-i18next';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Trans } from 'react-i18next';
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import SendIcon from '@material-ui/icons/Send';
import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import { toast } from 'react-toastify';
import { useCookies } from 'react-cookie';
import { createCustomerOpinionGql } from '../../graphQL/customerOpinion';
import { addMonths } from 'date-fns'
import { setUser } from '../../shared/store/actions/authentication.actions';
import { useDispatch } from "react-redux";

import { userGql } from '../../graphQL/user'
const regexMail = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$')

const useStyles = makeStyles((theme) =>
  createStyles({
    star: {
      width: "25px",
      color: theme.colors.strawberry,
      position: "relative",
      top: "5px"
    },
    giftIcon: {
      width: "25px",
      color: theme.colors.strawberry,
      position: "relative",
      top: "5px",
      left: "10px"
    },
    selectStar: {
      cursor: "pointer",
      color: theme.colors.strawberry
    },
    commentTextarea: {
      width: "100%"
    },
    validComment: {
      textAlign: "center",
      margin: "10px 0px"
    },
    sendCommentButton: {
      height: "fit-content",
      margin: "0px 10px"
    },
    input: {
      "& label": {
        fontSize: "14px"
      },
      "& .MuiFormLabel-root.Mui-error": {
        color: "grey"
      },
      "& fieldset": {
        borderColor: "lightgrey !important"
      }
    },
    title: {
      fontFamily: "'League Spartan', sans-serif",
    },
    strawberryTitle: {
      fontFamily: "'League Spartan', sans-serif",
      color: theme.colors.strawberry
    }
  }),
);

export default function DialogCustomerOpinion(props) {
  const [open, setOpen] = useState(false)
  const [rateSelected, setRateSelected] = useState(-1);
  const [commentTextarea, setCommentTextarea] = useState('');
  const [mail, setMail] = useState('');
  const [customerOpinionsMails, setCustomerOpinionsMails] = useState([]);
  const [cookies, setCookie, removeCookie] = useCookies(['cookies_avis', 'cookies_mail']);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  
  useEffect(() => {
    if(props.loggedInUser) {
      setMail(props.loggedInUser.mail)
    }
  }, [props.loggedInUser])

  useEffect(() => {
    if(cookies['cookies_avis'] && cookies['cookies_avis'] === "true") {
      props.dismissCustomerOpinionDialog();
    }
    if(!mail && !!cookies['cookies_mail']) {
      setMail(cookies['cookies_mail'])
    } 
  }, [cookies])

  useEffect(() => {
    if(props.customerOpinions) {
      let mails = props.customerOpinions.map(c => c.mail);
      setCustomerOpinionsMails(mails)
    }
  }, [props.customerOpinions])

  const sendComment = async () => {
    try {
      let customerOpinion = await createCustomerOpinionGql({ input: { rate: rateSelected, text: commentTextarea, mail: mail } })
      if(customerOpinion) {
        let newDate = addMonths(new Date(), 13);
        setCookie('cookies_avis', true, { path: '/', expires: newDate });
        setCookie('cookies_mail', mail, { path: '/', expires: newDate });
        if(props.loggedInUser) {
          let user = await userGql()
          dispatch(setUser(user));
        }
      } else {
        toast.error(t('error.somethingWrongAppend'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      props.dismissCustomerOpinionDialog();
      if(!props.loggedInUser) {
        props.openCreateAccountDialog();
      }
    } catch (e) {
      toast.error(t('error.somethingWrongAppend'), {
        position: toast.POSITION.TOP_RIGHT
      });
    }
  }

  const chooseRate = () => {
    let iconsStar = [];
    for (let i = 1; i <= 5; i++) {
      if (i <= rateSelected) {
        iconsStar.push(<StarIcon key={`star_chooseRate_${i}`} onClick={() => setRateSelected(i)} className={classes.selectStar} />)
      } else {
        iconsStar.push(<StarBorderIcon key={`star_chooseRate_${i}`} onClick={() => setRateSelected(i)} className={classes.selectStar} />)

      }
    }
    return iconsStar;
  }
  const handleOnAction = (e) => {
    if (getElapsedTime() >= 45000 && !open) {
      setOpen(true);
    }

  }

  let testMail = () => {
    if(customerOpinionsMails.includes(mail)) {
      props.opinionAlreadyWrite();
      let newDate = addMonths(new Date(), 13);
      setCookie('cookies_avis', true, { path: '/', expires: newDate });
      setCookie('cookies_mail', mail, { path: '/', expires: newDate });
      return false;
    } else {
      return true;
    }
  }

  const { getElapsedTime, pause } = useIdleTimer({
    timeout: 1000 * 60 * 15,
    onAction: handleOnAction,
    debounce: 500
  })

  let classes = useStyles();

  return (
    <Dialog fullWidth={true} open={open} onClose={() => props.dismissCustomerOpinionDialog()}>
      <DialogTitle>
         <span className={classes.title}>
           <Trans i18nKey="dialog.customerOpinion.title"
          components={[<span className={classes.strawberryTitle} />]} /></span>
        <CardGiftcardIcon className={classes.giftIcon} />
      </DialogTitle>
      <DialogContent>
        <div>{chooseRate()}</div>
        <TextareaAutosize
          rows={4}
          onChange={(event) => setCommentTextarea(event.target.value)}
          aria-label="maximum height"
          placeholder={t('dialog.customerOpinion.info')}
          className={classes.commentTextarea}
          value={commentTextarea}
        />

        <TextField
          id="mail"
          label={t('form.mail.label')}
          value={mail}
          onChange={(event) => setMail(event.target.value)}
          fullWidth
          required
          margin="dense"
          helperText={mail !== '' && !regexMail.test(mail) ? t('form.mail.helper') : ( !testMail() ? t('dialog.customerOpinion.already') :'')}
          error={!regexMail.test(mail) || customerOpinionsMails.includes(mail)}
          variant="outlined"
          className={classes.input}
        />
      </DialogContent>

      <div className={classes.validComment}>
        <Button
          variant="contained"
          color="primary"
          disabled={rateSelected <= 0 || !mail || !regexMail.test(mail) || customerOpinionsMails.includes(mail)}
          onClick={sendComment}
          className={classes.sendCommentButton}
          endIcon={<SendIcon />}
        >
          {t('shareComments.button')}
        </Button>
      </div>
    </Dialog>

  );
}