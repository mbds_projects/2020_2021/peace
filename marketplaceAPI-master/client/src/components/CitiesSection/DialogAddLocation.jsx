import React, { useState } from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';

import { useTranslation } from 'react-i18next';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

import Divider from '@material-ui/core/Divider';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import { searchLocationDashboardGql } from '../../graphQL/location';
import { toastError } from '../../shared/utils/error';

const useStyles = makeStyles((theme) =>
  createStyles({
    input: {
      "& .MuiFormLabel-root.Mui-error": {
        color: "grey"
      },
      "& fieldset": {
        borderColor: "lightgrey !important"
      },
      "& .MuiFilledInput-input": {
        padding: "10px 0px"
      }
    }
  }),
);

export default function DialogAddLocation(props) {
  const { t } = useTranslation();

  const classes = useStyles();
  const [address, setAddress] = useState('');
  const [resultSearch, setResultSearch] = useState();

  const [radio, setRadio] = useState("");

  const [chosenResult, setChosenResult] = useState(false);

  const [confirmShopName, setConfirmShopName] = useState("");
  const [confirmAddress, setConfirmAddress] = useState("");
  const [confirmCity, setConfirmCity] = useState("");
  const [confirmPostalCode, setConfirmPostalCode] = useState("");
  const [confirmCounty, setConfirmCounty] = useState("");
  const [confirmState, setConfirmState] = useState("");
  const [confirmPhoneNumber, setConfirmPhoneNumber] = useState("");
  const [confirmWebSite, setConfirmWebSite] = useState("");
  const [confirmOpeningHours, setConfirmOpeningHours] = useState("");
  const [confirmMail, setConfirmMail] = useState("");

  const handleRadio = (event) => {
    setRadio(event.target.value);
  };

  const resetDialog = () => {
    setConfirmShopName("");
    setConfirmAddress("");
    setConfirmCity("");
    setConfirmPostalCode("");
    setConfirmCounty("");
    setConfirmState("");
    setConfirmPhoneNumber("");
    setConfirmWebSite("");
    setConfirmOpeningHours("");
    setConfirmMail("");
    setChosenResult(false);
    setRadio("");
    setResultSearch(null);
    setAddress("");
  }

  const handleCloseLocation = () => {
    props.setOpenAddLocation(false);
    resetDialog();
  }

  const handleChangeAddress = (event) => {
    setAddress(event.target.value)
  }

  const chooseLocation = () => {
    let index = parseInt(radio.replace("result_", ""))
    let loc = resultSearch[index];
    setConfirmAddress(loc.address)
    setConfirmCity(loc.city)
    setConfirmPostalCode(loc.postalCode)
    setConfirmShopName(loc.shopName)
    setConfirmCounty(loc.county)
    setConfirmState(loc.state)
    if(loc.openingHours) setConfirmOpeningHours(loc.openingHours)
    if(loc.phoneNumber) setConfirmPhoneNumber(loc.phoneNumber)
    if(loc.webSite) setConfirmWebSite(loc.webSite)
    setChosenResult(true);
  }

  const handleConfirmShopName = (event) => {
    setConfirmShopName(event.target.value)
  }

  const handleConfirmOpeningHours = (event) => {
    setConfirmOpeningHours(event.target.value)
  }

  const handleConfirmPhoneNumber = (event) => {
    setConfirmPhoneNumber(event.target.value)
  }

  const handleConfirmWebSite = (event) => {
    setConfirmWebSite(event.target.value)
  }

  const handleConfirmMail = (event) => {
    setConfirmMail(event.target.value)
  }

  const addLocation = () => {
    let index = parseInt(radio.replace("result_", ""))
    let loc = resultSearch[index];
    let input = {
      address: confirmAddress,
      city: confirmCity,
      shopName: confirmShopName,
      postalCode: confirmPostalCode,
      phoneNumber: confirmPhoneNumber,
      webSite: confirmWebSite,
      mail: confirmMail,
      openingHours: confirmOpeningHours,
      county: confirmCounty,
      state: confirmState,
      location: {
        type: "Point",
        coordinates: loc.location.coordinates
      }
    }
    props.createLocation(input)
    props.setOpenAddLocation(false);
    resetDialog();
  }

  const searchLocation = async (input) => {
    try {
      let result = await searchLocationDashboardGql({ address });
      setResultSearch(result);
    } catch (e) {
      toastError(e.message, t);
    }
  }


  return (
    <Dialog
      open={props.openAddLocation}
      onClose={handleCloseLocation}>
      <DialogTitle>{t('vendorDashboard.location.add.title')}</DialogTitle>
      <DialogContent>
        {
          !chosenResult ?
          <div>
            <p>{t('vendorDashboard.location.add.search')}</p>
            <TextField
              value={address}
              onChange={handleChangeAddress}
              fullWidth
              margin="dense"
              variant="filled"
              className={classes.input}
            />
            {resultSearch && <Divider variant="middle" />}
            {
              resultSearch && resultSearch.length > 0 &&

              <FormControl component="fieldset">
                <FormLabel component="legend">{t('vendorDashboard.location.add.results')}</FormLabel>
                <RadioGroup name="search" value={radio} onChange={handleRadio}>
                  {
                    resultSearch.map((res, index) => {
                      let disabled = props.locations && props.locations.length > 0 
                      && !!props.locations.find(loc => loc.location.coordinates[0] === res.location.coordinates[0] && loc.location.coordinates[1] === res.location.coordinates[1])
                      return <FormControlLabel
                        disabled={disabled}
                        key={`result_${index}`} value={`result_${index}`} control={<Radio />}
                        label={
                          <div>
                            <span>{res.address}, {res.postalCode} {res.city} {disabled && `- ${t('vendorDashboard.location.add.alreadyAdded')}`}</span>
                            {res.shopName && <span><br />{res.shopName}</span>}
                            {res.openingHours && !disabled && <span><br />{res.openingHours}</span>}
                            {res.phoneNumber && !disabled && <span><br /><a target="_blank" rel="noopener noreferrer" href={"tel:" + res.phoneNumber}>{res.phoneNumber}</a></span>}
                            {res.webSite && !disabled && <span><br /><a target="_blank" rel="noopener noreferrer" href={res.webSite}>{res.webSite}</a></span>}
                          </div>
                        } />
                    })
                  }
                </RadioGroup>
              </FormControl>
            }
            {
              resultSearch && resultSearch.length <= 0 &&
              <div>{t('vendorDashboard.location.add.noResult')}</div>
            }
          </div>
          :
          <div>
            <p>{t('vendorDashboard.location.add.info')}</p>
            <TextField
              value={confirmShopName}
              onChange={handleConfirmShopName}
              label={t('vendorDashboard.location.shopName')}
              fullWidth
              margin="dense"
              variant="outlined"
              required={true}
              className={classes.input}
            />
            <TextField
              value={confirmAddress}
              disabled={true}
              label={t('vendorDashboard.location.address')}
              fullWidth
              margin="dense"
              variant="outlined"
              className={classes.input}
            />
            <TextField
              value={confirmPostalCode}
              disabled={true}
              label={t('vendorDashboard.location.postalCode')}
              fullWidth
              margin="dense"
              variant="outlined"
              className={classes.input}
            />
            <TextField
              value={confirmCity}
              disabled={true}
              label={t('vendorDashboard.location.city')}
              fullWidth
              margin="dense"
              variant="outlined"
              className={classes.input}
            />
            <TextField
              value={confirmCounty}
              disabled={true}
              label={t('vendorDashboard.location.county')}
              fullWidth
              margin="dense"
              variant="outlined"
              className={classes.input}
            />
            <TextField
              value={confirmState}
              disabled={true}
              label={t('vendorDashboard.location.state')}
              fullWidth
              margin="dense"
              variant="outlined"
              className={classes.input}
            />
            <TextField
              value={confirmOpeningHours}
              onChange={handleConfirmOpeningHours}
              label={t('vendorDashboard.location.openingHours')}
              fullWidth
              margin="dense"
              variant="outlined"
              className={classes.input}
            />
            <TextField
              value={confirmPhoneNumber}
              onChange={handleConfirmPhoneNumber}
              label={t('vendorDashboard.location.phoneNumber')}
              fullWidth
              margin="dense"
              variant="outlined"
              className={classes.input}
            />
            <TextField
              value={confirmWebSite}
              onChange={handleConfirmWebSite}
              label={t('vendorDashboard.location.webSite')}
              fullWidth
              margin="dense"
              variant="outlined"
              className={classes.input}
            />
            <TextField
              value={confirmMail}
              onChange={handleConfirmMail}
              label={t('vendorDashboard.location.mail')}
              fullWidth
              margin="dense"
              variant="outlined"
              className={classes.input}
            />
          </div>
        }

      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseLocation} color="primary">
          {t('vendorDashboard.cancel')}
        </Button>
        {!chosenResult && <Button onClick={searchLocation} disabled={address === ""} color="primary">
          {t('vendorDashboard.search')}
        </Button>
        }
        {
          resultSearch && resultSearch.length > 0 && !chosenResult &&
          <Button onClick={chooseLocation} disabled={radio === ""} color="primary">
            {t('vendorDashboard.select')}
          </Button>
        }
        {
          chosenResult &&
          <Button onClick={addLocation} disabled={confirmShopName.trim() === ""} color="primary">
            {t('vendorDashboard.add')}
          </Button>
        }
      </DialogActions>
    </Dialog>
  );
}

