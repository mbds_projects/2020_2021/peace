import React, { useState, useEffect } from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';

import { useTranslation } from 'react-i18next';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) =>
  createStyles({
    input: {
      "& .MuiFormLabel-root.Mui-error": {
        color: "grey"
      },
      "& fieldset": {
        borderColor: "lightgrey !important"
      },
      "& .MuiFilledInput-input": {
        padding: "10px 0px"
      }
    }
  }),
);

export default function DialogUpdateLocation(props) {
  const { t } = useTranslation();

  const classes = useStyles();

  const [confirmShopName, setConfirmShopName] = useState("");
  const [confirmAddress, setConfirmAddress] = useState("");
  const [confirmCity, setConfirmCity] = useState("");
  const [confirmPostalCode, setConfirmPostalCode] = useState("");
  const [confirmCounty, setConfirmCounty] = useState("");
  const [confirmState, setConfirmState] = useState("");
  const [confirmPhoneNumber, setConfirmPhoneNumber] = useState("");
  const [confirmWebSite, setConfirmWebSite] = useState("");
  const [confirmOpeningHours, setConfirmOpeningHours] = useState("");
  const [confirmMail, setConfirmMail] = useState("");

  useEffect(() => {
    let didCancel = false

    if (props.locationSelected) {
      !didCancel && setConfirmShopName(props.locationSelected.shopName);
      !didCancel && setConfirmAddress(props.locationSelected.address);
      !didCancel && setConfirmCity(props.locationSelected.city);
      !didCancel && setConfirmPostalCode(props.locationSelected.postalCode);
      !didCancel && setConfirmCounty(props.locationSelected.county);
      !didCancel && setConfirmState(props.locationSelected.state);
      if(!!props.locationSelected.phoneNumber) !didCancel && setConfirmPhoneNumber(props.locationSelected.phoneNumber);
      if(!!props.locationSelected.webSite) !didCancel && setConfirmWebSite(props.locationSelected.webSite);
      if(!!props.locationSelected.openingHours) !didCancel && setConfirmOpeningHours(props.locationSelected.openingHours);
      if(!!props.locationSelected.mail) !didCancel && setConfirmMail(props.locationSelected.mail);
    }

    return () => { didCancel = true }
  }, [])

  const handleCloseLocation = () => {
    props.setOpenEditLocation(false);
  }

  const editLocation = () => {
    let input = {
      address: confirmAddress,
      city: confirmCity,
      shopName: confirmShopName,
      postalCode: confirmPostalCode,
      phoneNumber: confirmPhoneNumber,
      webSite: confirmWebSite,
      mail: confirmMail,
      openingHours: confirmOpeningHours,
      county: confirmCounty,
      state: confirmState,
      location: {
        type: "Point",
        coordinates: props.locationSelected.location.coordinates
      }
    }
    props.updateLocation(input, props.locationSelected.id)
    props.setOpenEditLocation(false);
  }

  return (
    <Dialog
      open={props.openEditLocation}
      onClose={handleCloseLocation}>
      <DialogTitle>{t('vendorDashboard.location.edit.title')}</DialogTitle>
      <DialogContent>
        <p>{t('vendorDashboard.location.edit.info')}</p>

        <div>
          <TextField
            value={confirmShopName}
            onChange={(event) => setConfirmShopName(event.target.value)}
            label={t('vendorDashboard.location.shopName')}
            fullWidth
            margin="dense"
            variant="outlined"
            required={true}
            className={classes.input}
          />
          <TextField
            value={confirmAddress}
            disabled={true}
            label={t('vendorDashboard.location.address')}
            fullWidth
            margin="dense"
            variant="outlined"
            className={classes.input}
          />
          <TextField
            value={confirmPostalCode}
            disabled={true}
            label={t('vendorDashboard.location.postalCode')}
            fullWidth
            margin="dense"
            variant="outlined"
            className={classes.input}
          />
          <TextField
            value={confirmCity}
            disabled={true}
            label={t('vendorDashboard.location.city')}
            fullWidth
            margin="dense"
            variant="outlined"
            className={classes.input}
          />
          <TextField
            value={confirmCounty}
            disabled={true}
            label={t('vendorDashboard.location.county')}
            fullWidth
            margin="dense"
            variant="outlined"
            className={classes.input}
          />
          <TextField
            value={confirmState}
            disabled={true}
            label={t('vendorDashboard.location.state')}
            fullWidth
            margin="dense"
            variant="outlined"
            className={classes.input}
          />
          <TextField
            value={confirmOpeningHours}
            onChange={(event) => setConfirmOpeningHours(event.target.value)}
            label={t('vendorDashboard.location.openingHours')}
            fullWidth
            margin="dense"
            variant="outlined"
            className={classes.input}
          />
          <TextField
            value={confirmPhoneNumber}
            onChange={(event) => setConfirmPhoneNumber(event.target.value)}
            label={t('vendorDashboard.location.phoneNumber')}
            fullWidth
            margin="dense"
            variant="outlined"
            className={classes.input}
          />
          <TextField
            value={confirmWebSite}
            onChange={(event) => setConfirmWebSite(event.target.value)}
            label={t('vendorDashboard.location.webSite')}
            fullWidth
            margin="dense"
            variant="outlined"
            className={classes.input}
          />
          <TextField
            value={confirmMail}
            onChange={(event) => setConfirmMail(event.target.value)}
            label={t('vendorDashboard.location.mail')}
            fullWidth
            margin="dense"
            variant="outlined"
            className={classes.input}
          />
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseLocation} color="primary">
          {t('vendorDashboard.cancel')}
        </Button>
        <Button onClick={editLocation} disabled={confirmShopName.trim() === ""} color="primary">
          {t('vendorDashboard.update')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

