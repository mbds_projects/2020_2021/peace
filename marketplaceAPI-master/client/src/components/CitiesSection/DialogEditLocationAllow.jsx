import React from 'react';

import routerHistory from '../../shared/router-history/router-history';

import Button from '@material-ui/core/Button';
import CardMembershipIcon from '@material-ui/icons/CardMembership';
import { makeStyles, createStyles, withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) =>
    createStyles({
        centerButton: {
            textAlign: "center"
        }
    }),
);

const ColorButtonAlmond = withStyles((theme) => ({
    root: {
        color: "white",
        fontSize: "20px",
        fontWeight: "bolder",
        backgroundColor: theme.colors.almond
    },
}))(Button);

export default function DialogEditLocationAllow(props) {
    const { t } = useTranslation();

    const classes = useStyles();

    const goToSubscription = () => {
        routerHistory.push('/subscribe');
    }

    const handleCloseLocationAllow = () => {
        props.setOpenEditLocationAllow(false);
    }

    return (
        <Dialog
            open={props.openEditLocationAllow}
            onClose={handleCloseLocationAllow}>
            <DialogTitle>{t('vendorDashboard.location.allow.title')}</DialogTitle>
            <DialogContent>
                {
                    !props.subscription && 
                    <p>{t('vendorDashboard.location.allow.infoNotSubscribe')}</p>
                }
                {
                    props.subscription &&
                    <span>
                        {
                            (props.subscription.product.name === "Super" && props.numberOfPointOfSale >= 20) &&
                            <p>{t('vendorDashboard.location.allow.infoSuper')}</p>
                        }
                        {
                            (props.subscription.product.name === "Basic" && props.numberOfPointOfSale >= 6 && !props.published ) &&
                            <p>{t('vendorDashboard.location.allow.infoBasic')}</p>
                        }
                        {
                            (props.subscription.product.name === "Basic" && props.published ) &&
                            <p>{t('vendorDashboard.location.allow.infoBasicPublished')}</p>
                        }
                    </span>
                }
                <div className={classes.centerButton}>
                <ColorButtonAlmond
                    variant="contained"
                    color="primary"
                    onClick={goToSubscription}
                    endIcon={<CardMembershipIcon />}
                >
                    {(props.subscription && props.subscription.product.name === "Super" && props.numberOfPointOfSale >= 6) ? t('vendorDashboard.subscription.doPlus') : t('vendorDashboard.subscription.do') }
                </ColorButtonAlmond>
                </div>
            </DialogContent>
        </Dialog>
    )
}