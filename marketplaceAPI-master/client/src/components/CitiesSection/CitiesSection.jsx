import React, { useState, useEffect } from 'react';

import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTranslation } from 'react-i18next';

import ClearIcon from '@material-ui/icons/Clear';

import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import Paper from '@material-ui/core/Paper';

import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

import MenuItem from '@material-ui/core/MenuItem';

import RoomIcon from '@material-ui/icons/Room';
import StoreIcon from '@material-ui/icons/Store';
import IconButton from '@material-ui/core/IconButton';

import DialogAddLocation from "./DialogAddLocation"
import DialogUpdateLocation from "./DialogUpdateLocation"
import DialogEditLocationAllow from "./DialogEditLocationAllow"

import * as Autosuggest from 'react-autosuggest';

import { fade, makeStyles, createStyles } from '@material-ui/core/styles';
import MapComp from "../../components/Map/Map";
import OnlineShopCard from '../../components/OnlineShopCard/OnlineShopCard';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

const parse = require('autosuggest-highlight/parse');

const useStyles = makeStyles((theme) =>
    createStyles({
        inputRoot: {
            color: 'inherit',
            height: '50px',
            width: "90%"
        },
        inputRootMobile: {
            color: 'inherit',
            height: '50px',
            width: "300px"
        },

        suggestionLight: {
            fontWeight: "normal"
        },
        suggestionStrong: {
            fontWeight: "bolder",
            color: theme.colors.strawberry
        },
        suggestionPaper: {
            /*position: "relative",
            marginRight: theme.spacing(4),
            marginLeft: 0,*/
            // width: '100%',
            width: "auto",
            minWidth: "300px",
            /*[theme.breakpoints.up('md')]: {
              marginLeft: theme.spacing(3),
              width: 310,
            },*/
            "& ul": {
                listStyle: "none",
                margin: "0px",
                padding: "0px",
                "& li > div": {
                    paddingLeft: "55px"
                }
            }
        },
        search: {
            position: 'relative',
            borderRadius: theme.shape.borderRadius,
            backgroundColor: fade(theme.palette.common.black, 0.15),
            '&:hover': {
                backgroundColor: fade(theme.palette.common.black, 0.25),
            },
            //marginRight: theme.spacing(4),
            //marginLeft: '0px !important',
            width: '100%',
            /*[theme.breakpoints.up('sm')]: {
              marginLeft: theme.spacing(3),
              width: 'auto',
              height: '50px'
            },*/
            color: "black",
        },
        searchIcon: {
            width: theme.spacing(7),
            height: '100%',
            position: 'absolute',
            pointerEvents: 'none',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            color: "black"
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 7),
            transition: theme.transitions.create('width'),
            width: '100%',
            /*[theme.breakpoints.up('md')]: {
              width: 200,
            },*/
        },
        autoSuggestDiv: {
            marginTop: "20px"
        },
        tilde: {
            color: 'lightgrey'
        },
        img: {
            width: "100%"
        },

        cityClickable: {
            "&:hover": {
                cursor: "pointer",
                textDecoration: "underline"
            }
        },
        cityTarget: {
            color: theme.colors.strawberry
        },
        citiesPartMobile: {
            marginLeft: "10px",
            marginBottom: "40px"
        },
        iconNoHover: {
            position: "relative",
            top: "5px"
        },
    })
);

export default function CitiesSection(props) {
    const { t } = useTranslation();
    const matches = useMediaQuery('(max-width:768px)');
    const classes = useStyles();

    const [suggestions, setSuggestions] = useState([]);

    const [citySelected, setCitySelected] = useState(0);

    const [cityClick, setCityClick] = useState(0);
    const [value, setValue] = useState('');

    const [openAddLocation, setOpenAddLocation] = useState(false);

    const [openEditLocationAllow, setOpenEditLocationAllow] = useState(false);

    const [openEditLocation, setOpenEditLocation] = useState(false);

    const [openRemoveLoc, setOpenRemoveLoc] = useState(false)
    const vendor = props.vendor
    const { locations } = vendor;

    useEffect(() => {
        let didCancel = false;
        !didCancel && setCityClick(0);
        !didCancel && setCitySelected(0);
        !didCancel && setValue('');

        return () => { didCancel = true }
    }, [])

    const handleOpenAddLocation = async () => {
        let isAllowed = await props.isAllowedToCreateLocation();
        if(isAllowed) {
            setOpenAddLocation(true);
        } else {
            setOpenEditLocationAllow(true);
        }

    }

    const handleOpenEditLocation = async () => {
        let isAllowed = await props.isAllowed();
        if(isAllowed) {
            setOpenEditLocation(true);
        } else {
            setOpenEditLocationAllow(true);
        }
    }

    const handleCloseRemoveLoc = () => {
        setOpenRemoveLoc(false)
    }

    const handleRemoveLoc = async () => {
        let isAllowed = await props.isAllowed();
        if(isAllowed) {
            setOpenRemoveLoc(true);
        } else {
            setOpenEditLocationAllow(true);
        }
    }

    const removeLoc = () => {
        setOpenRemoveLoc(false)
        props.removeLocation(locationSelected.id)
        setCityClick(0);
        setCitySelected(0);
        setValue('');
    }

    const clickCity = (cityIndex) => {
        setCityClick(cityIndex);
        //props.handleCityClick(cityIndex);
    };

    const getSuggestionValue = (suggestion) => {
        setCitySelected(suggestion.value);
        setCityClick(0);
        return '';
    }


    const getSuggestions = (value, suggestions) => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;

        return inputLength === 0 ? [] : suggestions.filter(lang =>
            lang.name ? lang.name.toLowerCase().includes(inputValue) : false
        );
    };


    const onSuggestionsFetchRequested = ({ value }) => {
        setSuggestions(getSuggestions(value, props.suggestions));
    };

    const onSuggestionsClearRequested = () => {
        setSuggestions([]);
    };

    function renderSuggestion(suggestion, { query, isHighlighted }) {
        const parts = parse(suggestion.name, [[suggestion.name.toLowerCase().indexOf(query.toLowerCase()), suggestion.name.toLowerCase().indexOf(query.toLowerCase()) + query.length]]);
        return (
            <MenuItem selected={isHighlighted} component="div">
                <div>
                    {parts.map((part, index) =>
                        part.highlight ? (
                            <span key={String(index)} className={classes.suggestionStrong}>
                                {part.text}
                            </span>
                        ) : (
                                <span key={String(index)} className={classes.suggestionLight}>
                                    {part.text}
                                </span>
                            )
                    )}
                </div>
            </MenuItem>
        );
    }

    const revertSearch = () => {
        setValue('');
        setSuggestions([]);
    }

    const onChange = (event, { newValue }) => {
        setValue(newValue);
    };

    const inputProps = {
        placeholder: t('vendorDashboard.searchPlaceholder'),
        value,
        onChange: onChange
    };

    const renderInputComponent = (inputProps) => (
        <div className={classes.search}>
            <div className={classes.searchIcon}>
                <SearchIcon />
            </div>
            <InputBase
                classes={{
                    root: matches ? classes.inputRootMobile : classes.inputRoot,
                    input: classes.inputInput,
                }}
                {...inputProps}
            />
            {
                value !== "" &&
                <IconButton aria-label="Clear" onClick={revertSearch}>
                    <ClearIcon />
                </IconButton>
            }
        </div>
    );

    let locationSelected;

    if (props.countiesAndCitiesSuggestions && props.countiesAndCitiesSuggestions.length > 0) {
        locationSelected = props.countiesAndCitiesSuggestions[citySelected].shops[cityClick]
    }

    const mapSection = locationSelected && locationSelected.location ?
        <MapComp lat={locationSelected.location.coordinates[0]} long={locationSelected.location.coordinates[1]} zoom={18} firstLocation={locationSelected} vendorName={vendor.name} logoUrl={vendor.logoUrl}/>
        : <div><img className={classes.img} src="/assets/images/addLocation.png" alt="Add location" /></div>

    const citiesSection = props.countiesAndCitiesSuggestions && props.countiesAndCitiesSuggestions.length > 0 &&
        <div className={classes.citiesPartMobile}>
            <RoomIcon className={classes.iconNoHover} />
            <span className={classes.cityTarget} > {props.countiesAndCitiesSuggestions[citySelected].loc}</span>
            {props.citiesSuggestions.length - 1 > 0 && <span> + {props.citiesSuggestions.length - 1} {t('vendorDashboard.cities')} </span>}
            <br />
            <StoreIcon className={classes.iconNoHover} />
            {props.countiesAndCitiesSuggestions[citySelected].shops.map((shop, index, shops) => {
                if (index === cityClick) {
                    return (<span key={`shop${index}`}>
                        <span className={classes.cityTarget} key={index}> {shop.shopName}
                            {props.editModeChecked &&
                                <IconButton onClick={handleOpenEditLocation}>
                                    <EditIcon />
                                </IconButton>
                            }
                            {props.editModeChecked &&
                                <IconButton onClick={handleRemoveLoc}>
                                    <DeleteIcon />
                                </IconButton>
                            }
                        </span>
                        <span className={classes.tilde}>{index < shops.length - 1 && ' ~'}
                        </span>
                    </span>)
                } else {
                    return (<span>
                        <span className={classes.cityClickable} onClick={() => clickCity(index)} key={`${shop.shopName}_${index}`}> {shop.shopName}
                            {
                                props.editModeChecked && <IconButton onClick={handleOpenEditLocation}>
                                    <EditIcon />
                                </IconButton>
                            }
                            {
                                props.editModeChecked &&
                                <IconButton onClick={handleRemoveLoc}>
                                    <DeleteIcon />
                                </IconButton>
                            }
                        </span>
                        <span className={classes.tilde}>{index < shops.length - 1 && ' ~'}</span>
                    </span>)
                };
            })}

            {
                props.countiesAndCitiesSuggestions.length > 1 &&
                <div className={classes.autoSuggestDiv}>
                    <Autosuggest
                        suggestions={suggestions.slice(0, 5)}
                        onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                        onSuggestionsClearRequested={onSuggestionsClearRequested}
                        getSuggestionValue={getSuggestionValue}
                        renderSuggestion={renderSuggestion}
                        inputProps={inputProps}
                        renderInputComponent={renderInputComponent}
                        renderSuggestionsContainer={options => (
                            <Paper {...options.containerProps} className={classes.suggestionPaper} square>
                                {options.children}
                            </Paper>
                        )}
                    />
                </div>
            }


        </div>;


    return (<span>
        <b>{(t('vendorDashboard.map'))}</b>
        {
            props.editModeChecked &&
            <IconButton onClick={handleOpenAddLocation}>
                <EditIcon />
            </IconButton>
        }
        {mapSection}
        {locations.length === 0 && vendor.onlineShop && <OnlineShopCard logoUrl={vendor.logoUrl} webSite={vendor.site} vendorName={vendor.name} />}
        {citiesSection}

        <DialogAddLocation
            locations={vendor.locations}
            openAddLocation={openAddLocation}
            setOpenAddLocation={setOpenAddLocation}
            createLocation={props.createLocation}
        />

        {openEditLocation && <DialogUpdateLocation
            openEditLocation={openEditLocation}
            setOpenEditLocation={setOpenEditLocation}
            locationSelected={locationSelected}
            updateLocation={props.updateLocation}
        />}


        <DialogEditLocationAllow
            published={props.published}
            subscription={props.subscription}
            numberOfPointOfSale={props.numberOfPointOfSale}
            openEditLocationAllow={openEditLocationAllow}
            setOpenEditLocationAllow={setOpenEditLocationAllow}
        />

        <Dialog
            open={openRemoveLoc}
            onClose={handleCloseRemoveLoc}>
            <DialogTitle>{t('vendorDashboard.location.remove.title')}</DialogTitle>
            <DialogContent>
                <p>{t('vendorDashboard.location.remove.info')}</p>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleCloseRemoveLoc} color="primary">
                    {t('vendorDashboard.cancel')}
                </Button>
                <Button onClick={removeLoc} color="primary">
                    {t('vendorDashboard.remove')}
                </Button>
            </DialogActions>
        </Dialog>
    </span>)
}
