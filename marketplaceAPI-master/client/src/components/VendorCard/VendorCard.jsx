import React, { useState, useEffect } from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';

import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';

import routerHistory from '../../shared/router-history/router-history';
import { useSelector } from "react-redux";
import { toggleFavorite } from '../../core/modules/user.module'
import { toastError } from '../../shared/utils/error';
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';

import EditIcon from '@material-ui/icons/Edit';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      margin: "10px"
    },
    grid: (props) => ({
      height: "200px",
      //borderRadius: "15px",
      backgroundImage: `linear-gradient(0deg, rgba(0, 0, 0, 0.5) 0%, rgba(255,255,255,0) 30%), url("${props.background}")`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "top",
      backgroundSize: "cover"
    }),
    vendorName: {
      color: "white",
      fontSize: "18px",
      fontWeight: "bold",
      /*wordBreak: "break-word"*/
    },
    bottomCard: {
      padding: "5px 10px",
      cursor: "pointer"
    },
    rate: {
      color: "white",
      textAlign: "right",
      "& i": {
        fontSize: "15px"
      }
    },
    grow: {
      flexGrow: 1,
      cursor: "pointer"
    },
    favorite: {
      color: theme.colors.strawberry,
      cursor: "pointer"
    },
    icon: {
      fontSize: "20px"
    },
    edit: {
      cursor: "pointer"
    }
  }),
);

export default function VendorCard(props) {
  const { t } = useTranslation();

  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  const [isFavorite, setIsFavorite] = useState(false);

  useEffect(() => {
    let didCancel = false
    if(!props.editMode) {
      if (loggedInUser && loggedInUser.favorites && loggedInUser.favorites.includes(props.vendor.id)) {
        !didCancel && setIsFavorite(true);
      } else {
        !didCancel && setIsFavorite(false);
      }
    }
    return () => { didCancel = true }
  }, [loggedInUser, props.editMode])


  const handleClick = () => {
    routerHistory.push('/vendors/' + props.vendor.id + '?name=' + props.vendor.name);
    window.scrollTo(0, 0);
  }

  const handleFavoriteClick = () => {
    if (loggedInUser) {
      toggleFavorite(loggedInUser.favorites, props.vendor.id, props.vendor.name)
        .catch((e) => toastError(e.message, t));
    } else {
      toast.error(t('error.shouldBeConnected'), {
        position: toast.POSITION.TOP_RIGHT
      });
      routerHistory.push('/login');
    }
  }

  const calculRate = (rate, id) => {
    let iconsStar = [];
    for (let i = 0; i < 5; i++) {
      if (i < rate) {
        iconsStar.push(<StarIcon key={`star_${id}_${i}`} className={classes.icon} />)
      } else {
        iconsStar.push(<StarBorderIcon key={`star_${id}_${i}`} className={classes.icon} />)
      }
    }
    return iconsStar;
  };

  let imgSrc = "";
  if (props.vendor.profilePictureUrl) {
    imgSrc = props.vendor.profilePictureUrl;
  }

  let classes = useStyles({ background: imgSrc });

  let rate = props.vendor.rate ? props.vendor.rate : 0;

  return (
    <div className={classes.root}>
      <Grid container direction="column" justify="space-between" className={classes.grid}>
        <Grid container direction="row" justify="flex-end">
          <Grid item>
            {
              props.editMode ?
                <EditIcon onClick={props.edit} className={classes.edit} />
                :
                <span>
                  {isFavorite ?
                    <FavoriteIcon onClick={handleFavoriteClick} className={classes.favorite} />
                    : <FavoriteBorderIcon onClick={handleFavoriteClick} className={classes.favorite} />
                  }
                </span>
            }
          </Grid>
        </Grid>
        <div onClick={handleClick} className={classes.grow} />
        <Grid onClick={handleClick} container direction="row" alignItems="flex-end" className={classes.bottomCard}>
          <Grid item xs={6} className={classes.vendorName}>{props.vendor.name}</Grid>
          <Grid item xs={6} className={classes.rate}>{calculRate(rate, props.vendor.id)}</Grid>
        </Grid>
      </Grid>
    </div>

  )
}