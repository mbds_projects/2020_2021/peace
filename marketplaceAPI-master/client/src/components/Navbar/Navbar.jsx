import React, { useState } from 'react';
import { fade, makeStyles, createStyles, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import PersonIcon from '@material-ui/icons/Person';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import Badge from '@material-ui/core/Badge';
import DialogNewAchievement from '../DialogNewAchievement/DialogNewAchievement'
import DialogCreateAccount from '../DialogCreateAccount/DialogCreateAccount'
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

import NewsletterBanner from './../NewsletterBanner/NewsletterBanner'
import Link from '@material-ui/core/Link';
import { NavLink } from 'react-router-dom';

import routerHistory from '../../shared/router-history/router-history';

import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import StoreIcon from '@material-ui/icons/Store';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import SecurityIcon from '@material-ui/icons/Security';
import ContactSupportIcon from '@material-ui/icons/ContactSupport';
import GavelIcon from '@material-ui/icons/Gavel';
import DescriptionIcon from '@material-ui/icons/Description';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear';
import MoreIcon from '@material-ui/icons/MoreVert';
import FaceIcon from '@material-ui/icons/Face';
//import LanguageIcon from '@material-ui/icons/Language';
//import GroupIcon from '@material-ui/icons/Group';
import RecordVoiceOverIcon from '@material-ui/icons/RecordVoiceOver';
import PublicIcon from '@material-ui/icons/Public';
import DashboardIcon from '@material-ui/icons/Dashboard';
import LockIcon from '@material-ui/icons/Lock';
import EmojiObjectsIcon from '@material-ui/icons/EmojiObjects';
import AssignmentIcon from '@material-ui/icons/Assignment';

import InputBase from '@material-ui/core/InputBase';
import Paper from '@material-ui/core/Paper';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import { useTranslation } from 'react-i18next';

import * as Autosuggest from 'react-autosuggest';

import Slide from '@material-ui/core/Slide';


import ReactGA from 'react-ga';
const parse = require('autosuggest-highlight/parse');

const KeepOnScroll = ({ children }) => {
  return (
    <Slide appear={true} in={true}>
      {children}
    </Slide>
  )
}

const useStyles = makeStyles((theme) =>
  createStyles({
    list: {
      width: '100%',
      maxWidth: 360,
      border: '1px solid #d3d4d5',
      backgroundColor: 'theme.palette.background.paper',
      marginTop: "60px"
    },
    nested: {
      paddingLeft: theme.spacing(4),
      "& span": {
        color: "black"
      }
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: '33.33%',
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
    root: (props) =>  ({
      flexGrow: 1,
      marginBottom: props.newsletterMargin ? '60px' : '0px',
      fontFamily: "'Open Sans', sans-serif"
    }),
    rootMobile: (props) => ({
      flexGrow: 1,
      marginBottom: props.newsletterMargin ? '60px' : '0px',
      fontFamily: "'Open Sans', sans-serif"
    }),
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
      color: "black",
      '& a': {
        color: 'black'
      }
    },
    titleDesktop: {
      color: "black",
      textTransform: "uppercase",
      lineHeight: "0.6",
      '& a': {
        color: 'black',
        fontSize: "14px",
        fontWeight: "bold",
        lineHeight: "1"
      },
      margin: "0px 20px"
    },
    titleDesktopSideBarOpen: {

      margin: '0px',
      marginRight: "10px",
      "& a": {
        fontSize: "12px"
      }
    },
    logo: {
      '& img': {
        height: '60px'
      }
    },
    colorPrimary: {
      backgroundColor: "white"
    },
    colorSecondary: {
      color: "black"
    },
    revealShop: {
      backgroundColor: theme.colors.gold,
      color: "white !important"
    },
    suggestBrand: {
      backgroundColor: theme.colors.strawberry,
      color: "white !important"
    },
    expandMoreProfile: {
      color: "black"
    },
    sideBarOpen: {
      "& header > div": {
        marginLeft: "20%"
      }
    },
    secondAppBarMobile: {
      marginTop: "60px"
    },
    iconMobile: {
      width: "25%",
      textAlign: "center"
    },
    subList: {
      marginTop: "60px",
      position: "fixed",
      width: "100%",
      backgroundColor: "white",
      zIndex: 2
    },
    avatar: {
      margin: "auto"
    },
    searchInputMobile: {
      marginTop: "0px"
    },
    dialogSearchIcon: {
      fontSize: "30px",
      position: "relative",
      top: "5px"
    },

    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.black, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.black, 0.25),
      },
      marginRight: theme.spacing(4),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
        height: '50px'
      },
      color: "black",
    },
    searchIcon: {
      width: theme.spacing(7),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      color: "black"
    },
    inputRoot: {
      color: 'inherit',
      height: '50px'
    },
    inputRootMobile: {
      color: 'inherit',
      height: '50px',
      width: "200px"
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 7),
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: 200,
      },
    },
    grow: {
      flexGrow: 1,
    },
    suggestionLight: {
      fontWeight: "normal"
    },
    suggestionStrong: {
      fontWeight: "bolder",
      color: theme.colors.strawberry
    },
    suggestionPaper: {
      position: "absolute",
      marginRight: theme.spacing(4),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('md')]: {
        marginLeft: theme.spacing(3),
        width: 310,
      },
      "& ul": {
        listStyle: "none",
        margin: "0px",
        padding: "0px",
        "& li > div": {
          paddingLeft: "55px"
        }
      }
    },
    suggestionPaperMobile: {
      position: "relative",
      marginRight: theme.spacing(4),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('md')]: {
        marginLeft: theme.spacing(3),
        width: 200,
      },
      "& ul": {
        listStyle: "none",
        margin: "0px",
        padding: "0px",
        "& li > div": {
          paddingLeft: "55px"
        }
      }
    },
    flagSmall: {
      width: '24px',
      height: '24px'
    },
    badge: {
      borderRadius: "50%",
      width: "10px"
    },
    almond: {
      backgroundColor: theme.colors.almond
    },
    badgeImg: {
      height: "20px",
      width: "20px"
    },
    newsletterBar: {
      marginTop: "65px",
      color: "white",
      backgroundColor: theme.colors.almond
    }

  })
);

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

export default function Navbar(props) {
  const { t, i18n } = useTranslation();

  const [anchorEl, setAnchorEl] = useState(null);
  const [anchorElLang, setAnchorElLang] = useState(null);

  const open = Boolean(anchorEl);
  const openLang = Boolean(anchorElLang);

  const [openProfileMobile, setOpenProfileMobile] = useState(false);
  const [openMoreMobile, setOpenMoreMobile] = useState(false);
  const [openSearchMobile, setOpenSearchMobile] = useState(false);

  const [suggestions, setSuggestions] = useState([]);

  const [value, setValue] = useState('');

  const classes = useStyles({newsletterMargin: !props.openNewsletterBanner});

  const matches = useMediaQuery('(max-width:768px)');

  const scrollUp = () => window.scrollTo(0, 0);

  const handleClickOpenSearchMobile = () => {
    setOpenSearchMobile(true);
    setOpenMoreMobile(false);
    setOpenProfileMobile(false);
  };

  const handleCloseSearchMobile = () => {
    setOpenSearchMobile(false);
  };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuLang = (event) => {
    setAnchorElLang(event.currentTarget);
  };


  const onChange = (event, { newValue }) => {
    setValue(newValue);
  };

  const handleMenuMobile = () => {
    setOpenProfileMobile(!openProfileMobile);
    setOpenMoreMobile(false);
  };

  const handleClose = () => {
    setAnchorEl(null);
    //scrollUp();
  };

  const handleCloseScrollUp = () => {
    setAnchorEl(null);
    scrollUp();
  };

  const handleMenuLangClose = () => {
    setAnchorElLang(null);
  };


  const handleCloseProfileMobile = () => {
    setOpenProfileMobile(false);
    scrollUp();
  };

  const handleFooterMobile = () => {
    setOpenMoreMobile(!openMoreMobile);
    setOpenProfileMobile(false);
  }

  const handleCloseMoreMobile = () => {
    setOpenMoreMobile(false);
    scrollUp();
  };

  const handleOnLogout = () => {
    props.onLogout();
    scrollUp();
  }

  const GADetectClickLang = (lang) => {
    ReactGA.event({
      category: 'Lang',
      action: `Switch lang to ${lang}`,
      label: 'Global'
    });
  }

  const switchLang = (lang) => {
    i18n.changeLanguage(lang);
    setAnchorElLang(null);
    setOpenMoreMobile(false);
    GADetectClickLang(lang);
  };

  let imgSrc = "";
  let avatarText = props.loggedInUser ?
    `${props.loggedInUser.name.charAt(0)} ${props.loggedInUser.surname ? props.loggedInUser.surname.charAt(0) : ''}`
    : '';

  if (props.loggedInUser && props.loggedInUser.role === "vendor") {
    imgSrc = props.loggedInUser.logoUrl;
    avatarText = "";
  } else if (props.loggedInUser && props.loggedInUser.profilePictureUrl) {
    imgSrc = props.loggedInUser.profilePictureUrl;
    avatarText = "";
  }

  const inputProps = {
    placeholder: t('navbar.searchPlaceholder'),
    value,
    onChange: onChange
  };


  function renderSuggestion(suggestion, { query, isHighlighted }) {
    if (suggestion.isAddNew) {
      return (
        <MenuItem selected={isHighlighted} component="div">
          <div>
            <span className={classes.suggestionLight}>
              {t('navbar.search')}:
              </span>
            <span className={classes.suggestionStrong}>
              {suggestion.name}
            </span>
          </div>
        </MenuItem>
      )
    } else {

      const parts = parse(suggestion.name, [[suggestion.name.toLowerCase().indexOf(query.toLowerCase()), suggestion.name.toLowerCase().indexOf(query.toLowerCase()) + query.length]]);
      return (
        <MenuItem selected={isHighlighted} component="div">
          <div>
            {parts.map((part, index) =>
              part.highlight ? (
                <span key={String(index)} className={classes.suggestionStrong}>
                  {part.text}
                </span>
              ) : (
                  <span key={String(index)} className={classes.suggestionLight}>
                    {part.text}
                  </span>
                ),
            )}
          </div>
        </MenuItem>
      );
    }
  }

  const revertSearch = () => {
    setValue('');
    setSuggestions([]);
  }


  const renderInputComponent = (inputProps) => (
    <div className={classes.search}>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        classes={{
          root: matches ? classes.inputRootMobile : classes.inputRoot,
          input: classes.inputInput
        }}
        {...inputProps}
      />
      {
        value !== "" &&
        <IconButton aria-label="Clear" onClick={revertSearch}>
          <ClearIcon />
        </IconButton>
      }
    </div>
  );

  const getSuggestionValue = (suggestion) => {
    if (suggestion.isAddNew) {
      setOpenSearchMobile(false);
      ReactGA.event({
        category: 'Search',
        action: `Search ${suggestion.name}`,
        label: `Global`
      });
      routerHistory.push(`/vendors?name=${suggestion.name}`);
    } else {
      setOpenSearchMobile(false);
      ReactGA.event({
        category: 'Search',
        action: `Go to ${suggestion.name}`,
        label: `Global`
      });
      routerHistory.push(`/vendors/${suggestion.value}?name=${suggestion.name}`);
    }
    return '';
  }

  const getSuggestions = (value, suggestions) => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    let suggestionsFiltered = suggestions.filter(lang =>
      lang.name.toLowerCase().includes(inputValue)
    );
    suggestionsFiltered.push({ isAddNew: true, name: value });

    return inputLength === 0 ? [] : suggestionsFiltered;
  };

  const onSuggestionsFetchRequested = ({ value }) => {
    setSuggestions(getSuggestions(value, props.suggestions));
  };

  const onSuggestionsClearRequested = () => {
    setSuggestions([]);
  };


  return (

    <div className={`${matches ? classes.rootMobile : classes.root} ${props.sideBarOpen && !matches ? classes.sideBarOpen : ''}`}>

      <KeepOnScroll {...props}>
        <AppBar position="fixed" color="primary" classes={{
          colorPrimary: classes.colorPrimary
        }} >
          <DialogCreateAccount open={props.openCreateAccount} setOpen={props.setOpenCreateAccount} />

          <DialogNewAchievement open={props.openNewAchievement} setOpen={props.setOpenNewAchievement} />

          {
            !matches ? (
              <Toolbar >
                <Typography variant="h6" className={`${classes.titleDesktop} ${props.sideBarOpen ? classes.titleDesktopSideBarOpen : ''}`}>
                  <NavLink to="/" className={classes.logo} onClick={scrollUp}>
                    <img alt="" src="https://blog.super-responsable.org/wp-content/uploads/2019/07/SuperResponsableLogoSSFond-e1562943299440.png" />

                  </NavLink>
                </Typography>



                <Autosuggest
                  suggestions={suggestions}
                  onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                  onSuggestionsClearRequested={onSuggestionsClearRequested}
                  getSuggestionValue={getSuggestionValue}
                  renderSuggestion={renderSuggestion}
                  inputProps={inputProps}
                  renderInputComponent={renderInputComponent}
                  renderSuggestionsContainer={options => (
                    <Paper {...options.containerProps} className={classes.suggestionPaper} square>
                      {options.children}
                    </Paper>
                  )}
                />


                <Typography variant="h6" className={`${classes.titleDesktop} ${props.sideBarOpen ? classes.titleDesktopSideBarOpen : ''}`}>
                  <NavLink to="/vendors" onClick={scrollUp}>
                    {t('navbar.shops')}
                  </NavLink>
                </Typography>


                {/*<Typography variant="h6" className={`${classes.titleDesktop} ${props.sideBarOpen ? classes.titleDesktopSideBarOpen : ''}`}>
                <a href="https://blog.super-responsable.org" target="_blank" rel="noopener noreferrer" onClick={scrollUp}>
                  {t('navbar.blog')}
                </a>
              </Typography>

              <Typography variant="h6" className={`${classes.titleDesktop} ${props.sideBarOpen ? classes.titleDesktopSideBarOpen : ''}`}>
                <NavLink to="/aboutUs" onClick={scrollUp}>
                  {t('navbar.aboutUs')}
                </NavLink>
                </Typography>*/}

                <Typography variant="h6" className={`${classes.titleDesktop} ${props.sideBarOpen ? classes.titleDesktopSideBarOpen : ''}`}>
                  <NavLink to="/user/rewards" onClick={scrollUp}>
                    {t('navbar.rewards')}
                  </NavLink>
                </Typography>

                <div className={classes.grow} />
                {/*<Typography variant="h6"  className={`${classes.titleDesktop} ${props.sideBarOpen ? classes.titleDesktopSideBarOpen : ''}`}>
                
                <Button 
                  variant="contained" 
                  className={classes.revealShop}
                  to="/vendorCandidate"
                  component={NavLink}
                  onClick={scrollUp}
                  startIcon={<Avatar className={classes.flagSmall} src="/assets/images/icons/diamond.png" />}>
                  {t('navbar.revealShop')}
                </Button>
              </Typography>*/}


                <Typography variant="h6" className={`${classes.titleDesktop} ${props.sideBarOpen ? classes.titleDesktopSideBarOpen : ''}`}>

                  <Button
                    variant="contained"
                    className={classes.suggestBrand}
                    to="/suggestBrand"
                    component={NavLink}
                    onClick={scrollUp}
                    startIcon={<LocationOnIcon />}>
                    {t('navbar.suggestBrand')}
                  </Button>
                </Typography>

                {
                  props.loggedInUser ? (
                    <div
                      onClick={handleMenu}>
                      {
                        props.newAchievement ?
                          <Badge
                            badgeContent={
                              <Avatar className={classes.badgeImg} src="/assets/images/icons/diamond.png" />}
                            color="primary"
                            overlap="circle"
                            classes={{
                              badge: classes.badge,
                              colorPrimary: classes.almond
                            }}
                            anchorOrigin={{
                              vertical: 'top',
                              horizontal: 'right',
                            }}>
                            <Avatar
                              className="avatar"
                              style={{ backgroundColor: props.loggedInUser.defaultColor }}
                              src={imgSrc}
                            >
                              {avatarText}
                            </Avatar>
                          </Badge>
                          :
                          <Avatar
                            className="avatar"
                            style={{ backgroundColor: props.loggedInUser.defaultColor }}
                            src={imgSrc}
                          >
                            {avatarText}
                          </Avatar>
                      }

                    </div>
                  ) :
                    (
                      <div>
                        <IconButton
                          component={NavLink}
                          to="/login"
                          color="secondary"
                          classes={{ colorSecondary: classes.colorSecondary }}
                          onClick={scrollUp}
                        >
                          <PersonIcon />
                        </IconButton>
                      </div>
                    )
                }
                {
                  props.loggedInUser &&
                  <IconButton
                    color="secondary"
                    onClick={handleMenu}
                    classes={{ colorSecondary: classes.colorSecondary }}
                  >
                    {open ? <ExpandLess /> : <ExpandMore />}

                  </IconButton>
                }
                {

                  props.loggedInUser &&
                  <ClickAwayListener onClickAway={handleClose}>
                    <StyledMenu
                      id="customized-menu"
                      anchorEl={anchorEl}
                      keepMounted
                      open={Boolean(anchorEl)}
                      onClose={handleClose}
                    >
                      {props.loggedInUser.role === "vendor" &&
                        <MenuItem component={NavLink} to="/vendor/dashboard" onClick={handleCloseScrollUp}>
                          <ListItemIcon>
                            <DashboardIcon fontSize="small" />
                          </ListItemIcon>
                          <ListItemText primary={t('navbar.dashboard')} />
                        </MenuItem>
                      }
                      {props.loggedInUser.role === "vendor" &&
                        <MenuItem component={NavLink} to="/vendor/account" onClick={handleCloseScrollUp}>
                          <ListItemIcon>
                            <PersonIcon fontSize="small" />
                          </ListItemIcon>
                          <ListItemText primary={t('navbar.account')} />
                        </MenuItem>
                      }
                      {props.loggedInUser.role === "vendor" &&
                        <MenuItem component={NavLink} to="/vendor/rewards" onClick={handleCloseScrollUp}>
                          <ListItemIcon>
                            <CardGiftcardIcon fontSize="small" />
                          </ListItemIcon>
                          <ListItemText primary={t('navbar.myRewards')} />
                        </MenuItem>
                      }
                      {props.loggedInUser.role === "vendor" &&
                        <MenuItem component={NavLink} to="/vendor/tutorials" onClick={handleCloseScrollUp}>
                          <ListItemIcon>
                            <AssignmentIcon fontSize="small" />
                          </ListItemIcon>
                          <ListItemText primary={t('navbar.tutorial')} />
                        </MenuItem>
                      }
                      {(props.loggedInUser.role === "user" || props.loggedInUser.role === "admin") &&
                        <MenuItem
                          component={NavLink}
                          to="/user"
                          onClick={handleCloseScrollUp}>
                          <ListItemIcon>
                            {
                              props.newAchievement ?

                                <Badge
                                  color="primary"
                                  variant="dot"
                                  classes={{
                                    badge: classes.badge,
                                    colorPrimary: classes.almond
                                  }}
                                  anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                  }}>
                                  <PersonIcon fontSize="small" />
                                </Badge>
                                :
                                <PersonIcon fontSize="small" />
                            }
                          </ListItemIcon>
                          <ListItemText primary={t('navbar.profile')} />
                        </MenuItem>
                      }

                      {(props.loggedInUser.role === "user" || props.loggedInUser.role === "admin") &&

                        <MenuItem component={NavLink} to="/user/favorites" onClick={handleCloseScrollUp}>
                          <ListItemIcon>
                            <FavoriteIcon fontSize="small" />
                          </ListItemIcon>
                          <ListItemText primary={t('navbar.favorites')} />
                        </MenuItem>
                      }

                      {(props.loggedInUser.role === "user" || props.loggedInUser.role === "admin") &&

                        <MenuItem component={NavLink} to="/user/suggestedForYou" onClick={handleCloseScrollUp}>
                          <ListItemIcon>
                            <EmojiObjectsIcon fontSize="small" />
                          </ListItemIcon>
                          <ListItemText primary={t('navbar.suggestedForYou')} />
                        </MenuItem>
                      }

                      {(props.loggedInUser.role === "user" || props.loggedInUser.role === "admin") &&
                        <MenuItem component={NavLink} to="/user/rewards" onClick={handleCloseScrollUp}>
                          <ListItemIcon>
                            <CardGiftcardIcon fontSize="small" />
                          </ListItemIcon>
                          <ListItemText primary={t('navbar.rewards')} />
                        </MenuItem>
                      }

                      {props.loggedInUser.role === "admin" &&
                        <MenuItem component={NavLink} to="/admin" onClick={handleCloseScrollUp}>
                          <ListItemIcon>
                            <SecurityIcon fontSize="small" />
                          </ListItemIcon>
                          <ListItemText primary={t('navbar.adminDashboard')} />
                        </MenuItem>
                      }

                      <MenuItem onClick={handleOnLogout}>
                        <ListItemIcon>
                          <ExitToAppIcon fontSize="small" />
                        </ListItemIcon>
                        <ListItemText primary={t('navbar.logout')} />
                      </MenuItem>
                    </StyledMenu>
                  </ClickAwayListener>
                }

                <div>
                  <IconButton
                    aria-label="lang"
                    onClick={handleMenuLang}
                    color="secondary"
                    classes={{ colorSecondary: classes.colorSecondary }}
                  >
                    {i18n.language === "fr" ?
                      <Avatar
                        className={classes.flagSmall}
                        src="/assets/images/icons/fr-flag.png" />
                      :
                      <Avatar
                        className={classes.flagSmall}
                        src="/assets/images/icons/uk-flag.png" />
                    }
                  </IconButton>

                  <ClickAwayListener onClickAway={handleMenuLangClose}>
                    <StyledMenu
                      id="customized-menu-lang"
                      anchorEl={anchorElLang}
                      keepMounted
                      open={openLang}
                      onClose={handleMenuLangClose}
                    >
                      <MenuItem onClick={() => { switchLang("fr") }}>
                        <ListItemAvatar>
                          <Avatar
                            className={classes.flagSmall}
                            src="/assets/images/icons/fr-flag.png" />
                        </ListItemAvatar>
                        <ListItemText primary="Fr" />
                      </MenuItem>

                      <MenuItem onClick={() => { switchLang("en-GB") }}>
                        <ListItemAvatar>
                          <Avatar
                            className={classes.flagSmall}
                            src="/assets/images/icons/uk-flag.png" />
                        </ListItemAvatar>
                        <ListItemText primary="En" />
                      </MenuItem>
                    </StyledMenu>

                  </ClickAwayListener>
                </div>
              </Toolbar>) : (


                <Toolbar>
                  <Typography variant="h6" className={classes.title}>
                    <NavLink to="/" className={classes.logo}
                      onClick={scrollUp}>
                      <img alt="" src="https://blog.super-responsable.org/wp-content/uploads/2019/07/SuperResponsableLogoSSFond-e1562943299440.png" />

                    </NavLink>
                  </Typography>
                  <div className={classes.iconMobile}>
                    <IconButton
                      color="secondary"
                      classes={{ colorSecondary: classes.colorSecondary }}
                      onClick={handleClickOpenSearchMobile}>
                      <SearchIcon />
                    </IconButton>
                  </div>
                  <Dialog
                    open={openSearchMobile}
                    onClose={handleCloseSearchMobile}
                    aria-labelledby="form-dialog-title">

                    <DialogTitle id="form-dialog-title">{t('navbar.searchDialog')}</DialogTitle>
                    <DialogContent>
                      <Autosuggest
                        suggestions={suggestions}
                        onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                        onSuggestionsClearRequested={onSuggestionsClearRequested}
                        getSuggestionValue={getSuggestionValue}
                        renderSuggestion={renderSuggestion}
                        inputProps={inputProps}
                        renderInputComponent={renderInputComponent}
                        renderSuggestionsContainer={options => (
                          <Paper {...options.containerProps} className={classes.suggestionPaperMobile} square>
                            {options.children}
                          </Paper>
                        )}
                      />
                    </DialogContent>
                  </Dialog>
                  {
                    !props.loggedInUser &&
                    <div className={classes.iconMobile}>
                      <IconButton
                        component={NavLink}
                        to="/login"
                        color="secondary"
                        onClick={scrollUp}
                        classes={{ colorSecondary: classes.colorSecondary }}
                      >
                        <PersonIcon />
                      </IconButton>
                    </div>
                  }
                  {
                    props.loggedInUser &&
                    <div className={classes.iconMobile}>
                      <Avatar
                        className={classes.avatar}
                        style={{ backgroundColor: props.loggedInUser.defaultColor }}
                        src={imgSrc}
                        onClick={handleMenuMobile}
                      >
                        {avatarText}
                      </Avatar>
                    </div>
                  }
                  <div className={classes.iconMobile}>
                    <IconButton
                      color="secondary"
                      classes={{ colorSecondary: classes.colorSecondary }}
                      onClick={handleFooterMobile}>
                      <MoreIcon />
                    </IconButton>
                  </div>

                </Toolbar>)
          }


        </AppBar>
      </KeepOnScroll>
      {props.openNewsletterBanner && <NewsletterBanner sideBarOpen={props.sideBarOpen} setNewsletter={props.setNewsletter} closeNewsletter={props.closeNewsletter}/>}
      {
        matches && openProfileMobile &&
        <ClickAwayListener onClickAway={handleCloseProfileMobile}>
          <List component="div" disablePadding className={classes.subList} >
            {(props.loggedInUser.role === "user" || props.loggedInUser.role === "admin") &&
              <ListItem button className={classes.nested} component={NavLink} to="/user" onClick={handleCloseProfileMobile}>
                <ListItemIcon>
                  <PersonIcon />
                </ListItemIcon>
                <ListItemText primary={t('navbar.profile')} />
              </ListItem>
            }
            {(props.loggedInUser.role === "user" || props.loggedInUser.role === "admin") &&
              <ListItem button className={classes.nested} component={NavLink} to="/user/favorites" onClick={handleCloseProfileMobile}>
                <ListItemIcon>
                  <FavoriteIcon />
                </ListItemIcon>
                <ListItemText primary={t('navbar.favorites')} />
              </ListItem>
            }
            {(props.loggedInUser.role === "user" || props.loggedInUser.role === "admin") &&
              <ListItem button className={classes.nested} component={NavLink} to="/user/rewards" onClick={handleCloseProfileMobile}>
                <ListItemIcon>
                  <CardGiftcardIcon />
                </ListItemIcon>
                <ListItemText primary={t('navbar.rewards')} />
              </ListItem>
            }
            {(props.loggedInUser.role === "user" || props.loggedInUser.role === "admin") &&
              <ListItem button className={classes.nested} component={NavLink} to="/user/suggestedForYou" onClick={handleCloseProfileMobile}>
                <ListItemIcon>
                  <EmojiObjectsIcon />
                </ListItemIcon>
                <ListItemText primary={t('navbar.suggestedForYou')} />
              </ListItem>
            }
            {props.loggedInUser.role === "vendor" &&
              <ListItem button className={classes.nested} component={NavLink} to="/vendor/dashboard" onClick={handleCloseProfileMobile}>
                <ListItemIcon>
                  <DashboardIcon />
                </ListItemIcon>
                <ListItemText primary={t('navbar.dashboard')} />
              </ListItem>
            }
            {props.loggedInUser.role === "vendor" &&
              <ListItem button className={classes.nested} component={NavLink} to="/vendor/tutorials" onClick={handleCloseProfileMobile}>
                <ListItemIcon>
                  <AssignmentIcon />
                </ListItemIcon>
                <ListItemText primary={t('navbar.tutorial')} />
              </ListItem>
            }
            {props.loggedInUser.role === "vendor" &&
              <ListItem button className={classes.nested} component={NavLink} to="/vendor/account" onClick={handleCloseProfileMobile}>
                <ListItemIcon>
                  <PersonIcon />
                </ListItemIcon>
                <ListItemText primary={t('navbar.account')} />
              </ListItem>
            }
            {props.loggedInUser.role === "vendor" &&
              <ListItem button className={classes.nested} component={NavLink} to="/vendor/rewards" onClick={handleCloseProfileMobile}>
                <ListItemIcon>
                  <CardGiftcardIcon />
                </ListItemIcon>
                <ListItemText primary={t('navbar.myRewards')} />
              </ListItem>
            }
            {
              props.loggedInUser.role === "admin" &&
              <ListItem button className={classes.nested}
                component={NavLink} to="/admin"
                onClick={handleCloseProfileMobile}>
                <ListItemIcon>
                  <SecurityIcon />
                </ListItemIcon>
                <ListItemText primary={t('navbar.adminDashboard')} />
              </ListItem>
            }
            <ListItem button className={classes.nested} onClick={handleOnLogout}>
              <ListItemIcon>
                <ExitToAppIcon />
              </ListItemIcon>
              <ListItemText primary={t('navbar.logout')} />
            </ListItem>
          </List>
        </ClickAwayListener>
      }
      {
        matches && openMoreMobile &&
        <ClickAwayListener onClickAway={handleCloseMoreMobile}>
          <List component="div" disablePadding className={classes.subList} >
            <ListItem button className={classes.nested}
              component={NavLink} to="/aboutUs"
              onClick={handleCloseMoreMobile}>
              <ListItemIcon>
                <FaceIcon />
              </ListItemIcon>
              <ListItemText primary={t('footer.aboutUs')} />
            </ListItem>
            <ListItem button className={classes.nested}
              component={Link} href="https://blog.super-responsable.org/"
              target="_blank" rel="noopener noreferrer"
              onClick={handleCloseMoreMobile}>
              <ListItemIcon>
                <PublicIcon />
              </ListItemIcon>
              <ListItemText primary={t('footer.actualities')} />
            </ListItem>
            <ListItem button className={classes.nested}
              component={Link} href="https://blog.super-responsable.org/wp-content/uploads/2019/12/Charte-responsable.pdf"
              target="_blank" rel="noopener noreferrer"
              onClick={handleCloseMoreMobile}>
              <ListItemIcon>
                <DescriptionIcon />
              </ListItemIcon>
              <ListItemText primary={t('footer.charter')} />
            </ListItem>
            <ListItem
              button className={classes.nested}
              component={NavLink} to="/vendorCandidate"
              onClick={handleCloseMoreMobile}>
              <ListItemIcon>
                <StoreIcon />
              </ListItemIcon>
              <ListItemText primary={t('footer.vendorCandidate')} />
            </ListItem>
            {/*<ListItem 
            button className={classes.nested} 
            component={NavLink} to="/partners" 
            onClick={handleCloseMoreMobile}>
              <ListItemIcon>
                <GroupIcon />
              </ListItemIcon>
              <ListItemText primary={t('footer.partners')} />
            </ListItem>*/}
            <ListItem
              button className={classes.nested}
              component={NavLink} to="/talkAboutUs"
              onClick={handleCloseMoreMobile}>
              <ListItemIcon>
                <RecordVoiceOverIcon />
              </ListItemIcon>
              <ListItemText primary={t('footer.talkOfUs')} />
            </ListItem>
            <ListItem
              button className={classes.nested}
              component={NavLink} to="/contactUs"
              onClick={handleCloseMoreMobile}>
              <ListItemIcon>
                <ContactSupportIcon />
              </ListItemIcon>
              <ListItemText primary={t('footer.contact')} />
            </ListItem>
            {i18n.language === "fr" ?
              <ListItem
                button className={classes.nested}
                onClick={() => { switchLang("en-GB") }}>
                <ListItemAvatar>
                  <Avatar
                    className={classes.flagSmall}
                    src="/assets/images/icons/uk-flag.png" />
                </ListItemAvatar>
                <ListItemText primary="English site" />
              </ListItem>
              :
              <ListItem
                button className={classes.nested}
                onClick={() => { switchLang("fr") }}>
                <ListItemAvatar>
                  <Avatar
                    className={classes.flagSmall}
                    src="/assets/images/icons/fr-flag.png" />
                </ListItemAvatar>
                <ListItemText primary="Site français" />
              </ListItem>
            }
            <ListItem
              button className={classes.nested}
              component={NavLink} to="/cgu"
              onClick={handleCloseMoreMobile}>
              <ListItemIcon>
                <GavelIcon />
              </ListItemIcon>
              <ListItemText primary={t('footer.cgu')} />
            </ListItem>
            <ListItem
              button className={classes.nested}
              component={NavLink} to="/rgpd"
              onClick={handleCloseMoreMobile}>
              <ListItemIcon>
                <LockIcon />
              </ListItemIcon>
              <ListItemText primary={t('footer.rgpd')} />
            </ListItem>
          </List>
        </ClickAwayListener>
      }
    </div>


  );
}