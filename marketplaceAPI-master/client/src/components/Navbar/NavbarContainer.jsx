import React, { useState, useEffect, useRef } from 'react';

import { useDispatch, useSelector } from "react-redux";

import { vendorsMinimalGql } from '../../graphQL/vendor';
import { checkNewsletterGql, setNewsletterGql } from '../../graphQL/user';

import { useLocation} from 'react-router-dom';

import { toast } from 'react-toastify';
import { logout } from '../../core/modules/authentication.module';
import { logout as logoutFromStore } from '../../shared/store/actions/authentication.actions';
import Navbar  from './Navbar';
import { addMonths } from 'date-fns'
import { useCookies } from 'react-cookie';
import { useTranslation } from 'react-i18next';
import { setUser } from '../../shared/store/actions/authentication.actions';
import { userGql } from '../../graphQL/user'

export default function NavbarContainer(props) {
  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  const sideBarOpen = useSelector((state) => state.sidebar.open);
  const [newAchievement, setNewAchievement] = useState(false);

  const [cookies, setCookie, removeCookie] = useCookies(['cookies_mail', 'cookies_newsletter', 'cookies_necessary']);
  const location = useLocation();
  const dispatch = useDispatch();
  const prevLoggedinUserRef = useRef();
  useEffect(() => {
    prevLoggedinUserRef.current = loggedInUser;
  });
  const prevLoggedinUser = prevLoggedinUserRef.current;

  const [suggestions, setSuggestions] = useState([]);
  const [openNewAchievement, setOpenNewAchievement] = useState(false);
  const [openCreateAccount, setOpenCreateAccount] = useState(false);
  const [openNewsletterBanner, setOpenNewsletterBanner] = useState(false);

  const { t } = useTranslation();

  useEffect(() => {
    let didCancel = false
    let newDate = addMonths(new Date(), 13);
    if(cookies['cookies_newsletter'] && cookies['cookies_newsletter'] === "true") {
      !didCancel && setOpenNewsletterBanner(false)
    } else {
      if(loggedInUser) {
        if(loggedInUser.newsletter) {
          if(cookies['cookies_necessary'] && cookies['cookies_necessary'] === "true") {
            setCookie('cookies_newsletter', true, { path: '/', expires: newDate });
            if(!cookies['cookies_mail'] || cookies['cookies_mail'] !== loggedInUser.mail) {
              setCookie('cookies_mail', loggedInUser.mail, { path: '/', expires: newDate });
            }
          }
        }
        !didCancel && setOpenNewsletterBanner(!loggedInUser.newsletter)
      } else {
        if(!!cookies['cookies_mail']) {
          let mail = cookies['cookies_mail'];
          try {
            checkNewsletterGql({mail: mail}).then(res => {
              if(res) {
                setCookie('cookies_newsletter', true, { path: '/', expires: newDate });
              }
              !didCancel && setOpenNewsletterBanner(!res);
            })
          } catch(e) {
            !didCancel && setOpenNewsletterBanner(true);
          }
        } else {
          !didCancel && setOpenNewsletterBanner(true);
        }
      }
    }
    return () => { didCancel = true }
  }, [cookies, loggedInUser])

  useEffect(() => {
    let didCancel = false

    async function retrieveSuggestions() {
      let vendors = await vendorsMinimalGql();
      let suggestions = vendors.map((vendor) => {
        return {
          value: vendor.id,
          name: vendor.name
        }
      });
      !didCancel && setSuggestions(suggestions)
    }

    retrieveSuggestions();
    return () => { didCancel = true }
  }, []);

  useEffect(() => {
    let didCancel = false
    if(loggedInUser && !!prevLoggedinUser && prevLoggedinUser !== undefined && prevLoggedinUser.role !== "vendor" && loggedInUser.role !== "vendor" && loggedInUser.achievements.length > prevLoggedinUser.achievements.length) {
      if(location.pathname !== "/user") {
        !didCancel && setNewAchievement(true);
      }
      !didCancel && props.dismissCustomerOpinionDialog();
      !didCancel && setOpenNewAchievement(true);
    }

    return () => { didCancel = true }
  }, [loggedInUser])


  useEffect(() => {
    let didCancel = false
    if(newAchievement && location.pathname === "/user") {
      !didCancel && setNewAchievement(false);
    }

    return () => { didCancel = true }
  }, [location])

  const handleLogout = () => {
    dispatch(logoutFromStore())
    logout();
    window.location.reload();
  }

  const closeNewsletter = () => {
    setOpenNewsletterBanner(false);
  }

  const setNewsletter = async (mail) => {
    let newDate = addMonths(new Date(), 13);
    if(cookies['cookies_necessary'] && cookies['cookies_necessary'] === "true") {
      setCookie('cookies_mail', mail, { path: '/', expires: newDate });
    }
    try {
      await setNewsletterGql({mail})
      if(loggedInUser) {
        let user = await userGql()
        dispatch(setUser(user));
      } else {
        setOpenCreateAccount(true);
      }
      if(cookies['cookies_necessary'] && cookies['cookies_necessary'] === "true") {
        setCookie('cookies_newsletter', true, { path: '/', expires: newDate });
      }
      setOpenNewsletterBanner(false);
    } catch(e) {
      toast.error(t('newsletter.toast.subscribeError'), {
        position: toast.POSITION.TOP_RIGHT
      });
    }
  }

  let propsChild = {
    loggedInUser,
    onLogout: handleLogout,
    sideBarOpen,
    suggestions,
    openNewAchievement,
    setOpenNewAchievement,
    newAchievement,
    openNewsletterBanner,
    setNewsletter,
    openCreateAccount,
    setOpenCreateAccount,
    closeNewsletter
  }

  return (
    <Navbar {...propsChild} />
  );
}
