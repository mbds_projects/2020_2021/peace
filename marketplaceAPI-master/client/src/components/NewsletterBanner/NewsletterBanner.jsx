import React, { useState, useEffect } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { useCookies } from 'react-cookie';
import { useTranslation } from 'react-i18next';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';

const regexMail = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$')

const useStyles = makeStyles((theme) =>
    createStyles({
        newsletterBar: {
            marginTop: "65px",
            color: "white",
            backgroundColor: theme.colors.gold,
            zIndex: 1
        },
        newsletterBarMobile: {
            marginTop: "65px",
            color: "white",
            backgroundColor: theme.colors.gold,
            zIndex: 1,
            padding: "10px"
        },
        title: {
            paddingTop: "10px",
            fontWeight: "bold",
            fontFamily: "'League Spartan', sans-serif",
        },
        text: {
            fontSize: "12px",
            paddingBottom: "10px",
            paddingRight: "10px",
            fontWeight: "bold",
            fontFamily: "'Open Sans', sans-serif",
        },
        input: {
            "& label": {
                fontSize: "10px"
            },
            "& .MuiFormLabel-root.Mui-error": {
                color: "grey"
            },
            "& fieldset": {
                borderColor: "white !important"
            },
            "& .MuiOutlinedInput-inputMarginDense": {
                paddingTop: "5px",
                paddingBottom: "5px",
                color: "white"
            },
        },
        button: {
            backgroundColor: "white",
            color: "black",
            fontSize: "10px"
        },
        cross: {
            textAlign: "center"
        },
        noMargin: {
            marginLeft: "0px !important"
        }
    })
);

export default function NewsletterBanner(props) {
    const classes = useStyles();
    const [mail, setMail] = useState('');
    const [cookies, setCookie, removeCookie] = useCookies(['cookies_mail']);
    const { t } = useTranslation();
    const matches = useMediaQuery('(max-width:768px)');
    useEffect(() => {
        if (!mail && !!cookies['cookies_mail']) {
            setMail(cookies['cookies_mail'])
        }
    }, [cookies])

    return (
        <AppBar position="relative" color="primary" classes={{
            colorPrimary: classes.colorPrimary
        }} className={matches ? classes.newsletterBarMobile : classes.newsletterBar} >
            {
                matches ?
                    <Grid container
                        direction="row"
                        justify="center"
                        alignItems="center">
                        <Grid item xs={10}>
                            <div className={classes.title}>{t('newsletter.title')}</div>
                        </Grid>
                        <Grid item xs={2} style={{textAlign: "right"}}>
                            <IconButton style={{padding: "0px"}} onClick={props.closeNewsletter}>
                                <CloseIcon />
                            </IconButton>
                        </Grid>
                        <Grid item xs={12}>
                            <div className={classes.text}>{t('newsletter.info')}</div>
                        </Grid>
                        <Grid container item xs={12} spacing={2}
                            direction="row"
                            justify="center"
                            alignItems="center">
                            <Grid item xs={6}>
                                <TextField
                                    size="small"
                                    id="mail"
                                    value={mail}
                                    onChange={(event) => setMail(event.target.value)}
                                    fullWidth
                                    required
                                    margin="none"
                                    helperText={mail !== '' && !regexMail.test(mail) ? t('form.mail.helper') : ''}
                                    error={!regexMail.test(mail)}
                                    variant="outlined"
                                    className={classes.input}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <Button
                                    variant="contained"
                                    disabled={!mail || !regexMail.test(mail)}
                                    onClick={() => { props.setNewsletter(mail) }}
                                    className={classes.button}
                                >
                                    {t('newsletter.button')}
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                    :
                    <Grid container
                        direction="row"
                        justify="center"
                        alignItems="center"
                        className={classes.noMargin}>
                        <Grid item xs={1}></Grid>
                        <Grid container
                            direction="row"
                            justify="center"
                            alignItems="center"
                            item xs={10}>
                            <Grid container
                                direction="row"
                                justify="center"
                                alignItems="center"
                                item xs={12}>
                                <Grid item xs={3}>
                                    <div className={classes.title}>{t('newsletter.title')}</div>
                                </Grid>
                                <Grid item xs={3}></Grid>
                            </Grid>
                            <Grid container
                                direction="row"
                                justify="center"
                                alignItems="center"
                                item xs={12}
                            >
                                <Grid item xs={3}>
                                    <div className={classes.text}>{t('newsletter.info')}</div>
                                </Grid>
                                <Grid container item xs={3} spacing={2}
                                    direction="row"
                                    justify="center"
                                    alignItems="center">
                                    <Grid item xs={6}>
                                        <TextField
                                            size="small"
                                            id="mail"
                                            value={mail}
                                            onChange={(event) => setMail(event.target.value)}
                                            fullWidth
                                            required
                                            margin="none"
                                            helperText={mail !== '' && !regexMail.test(mail) ? t('form.mail.helper') : ''}
                                            error={!regexMail.test(mail)}
                                            variant="outlined"
                                            className={classes.input}
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Button
                                            variant="contained"
                                            disabled={!mail || !regexMail.test(mail)}
                                            onClick={() => { props.setNewsletter(mail) }}
                                            className={classes.button}
                                        >
                                            {t('newsletter.button')}
                                        </Button>
                                    </Grid>
                                </Grid>

                            </Grid>
                        </Grid>

                        <Grid item xs={1} className={classes.cross}>
                            <IconButton onClick={props.closeNewsletter}>
                                <CloseIcon />
                            </IconButton>
                        </Grid>

                    </Grid>
            }
        </AppBar>
    )
}