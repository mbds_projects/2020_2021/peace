import React, { useState } from 'react';

import Grid from '@material-ui/core/Grid';

import { NavLink } from 'react-router-dom';
import Slide from '@material-ui/core/Slide';
import Link from '@material-ui/core/Link';
import routerHistory from '../../shared/router-history/router-history';

import useMediaQuery from '@material-ui/core/useMediaQuery';

import IconButton from '@material-ui/core/IconButton';
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import ShareIcon from '@material-ui/icons/Share';
import YouTubeIcon from '@material-ui/icons/YouTube';
import LinkedInIcon from '@material-ui/icons/LinkedIn';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import StorefrontIcon from '@material-ui/icons/Storefront';

import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import { useTranslation } from 'react-i18next';

import DialogShouldBeConnected from './../DialogShouldBeConnected/DialogShouldBeConnected'
import { makeStyles, createStyles } from '@material-ui/core/styles';

const KeepOnScroll = ({ children }) => {
    return (
        <Slide appear={true} direction="top" in={true}>
            {children}
        </Slide>
    )
}

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            display: 'flex',
            background: theme.colors.almond,
            padding: '20px',
            color: 'white',
            minHeight: "150px",
            fontFamily: "'League Spartan', sans-serif",
            "& a": {
                color: 'white',
            }
        },
        leftFooter: {
            textAlign: 'left',
            paddingLeft: '20px !important'
        },
        centerFooter: {
            textAlign: 'center'
        },
        rightFooter: {
            textAlign: 'right',
            paddingRight: '20px !important'
        },
        sideBarOpen: {
            marginLeft: "20%"
        },
        iconMobile: {
            width: "25%",
            textAlign: "center",
            padding: "0px"
        },
        appBar: {
            top: 'auto',
            bottom: 0,
        },
        grow: {
            flexGrow: 1,
        },

        fabButton: {
            position: 'absolute',
            zIndex: 1,
            top: -30,
            left: 0,
            right: 0,
            margin: '0 auto',
        },

        colorSecondary: {
            color: "black !important"
        },

        colorPrimary: {
            color: "white !important"
        },
        bgColorPrimary: {
            backgroundColor: "white !important"
        },
        subButton: {
            color: "black",
            fontSize: "11px"
        }
    }),
);

export default function Footer(props) {
    const classes = useStyles();
    const { t } = useTranslation();
    const matches = useMediaQuery('(max-width:768px)');
    const [openDialogShouldBeConnected, setOpenDialogShouldBeConnected] = useState(false);

    const scrollUp = () => window.scrollTo(0, 0);

    const goTo = (path) => {
        routerHistory.push(path);
    }

    return (
        <div className={`${!matches ? classes.root : ''} ${props.sideBarOpen && !matches ? classes.sideBarOpen : ''}`}>
            {
                !matches ?
                    (<Grid container spacing={3} justify="space-around" alignItems="center">
                        <Grid container direction="column" item sm={2} xs={12} className={classes.leftFooter}>
                            <Grid item>
                                <NavLink to="/aboutUs" onClick={scrollUp}>
                                    {t('footer.aboutUs')}
                                </NavLink>
                            </Grid>
                            <Grid item>
                                <a href="https://blog.super-responsable.org/wp-content/uploads/2019/12/Charte-responsable.pdf" target="_black">{t('footer.charter')}</a>
                            </Grid>
                            <Grid item>
                                <NavLink to="/contactUs?q=candidate" onClick={scrollUp}>
                                    {t('footer.candidate')}
                                </NavLink>
                            </Grid>
                            <Grid item>
                                <a href="https://blog.super-responsable.org" target="_black">{t('footer.blog')}</a>
                            </Grid>
                            <Grid item>
                                <NavLink to="/talkAboutUs" onClick={scrollUp}>
                                    {t('footer.talkOfUs')}
                                </NavLink>
                            </Grid>
                        </Grid>
                        <Grid container direction="column" item sm={4} xs={12} className={classes.centerFooter}>
                            <Grid item>
                                <IconButton
                                    aria-label="instagram"
                                    component={Link}
                                    href="https://www.instagram.com/super_responsable/"
                                    target="_blank" rel="noopener noreferrer"
                                    color="primary"
                                    classes={{ colorPrimary: classes.colorPrimary }}>
                                    <InstagramIcon />
                                </IconButton>
                                <IconButton
                                    aria-label="facebook"
                                    component={Link}
                                    href="https://www.facebook.com/Superresponsable"
                                    target="_blank" rel="noopener noreferrer"
                                    color="primary"
                                    classes={{ colorPrimary: classes.colorPrimary }}>
                                    <FacebookIcon />
                                </IconButton>
                                <IconButton
                                    aria-label="youtube"
                                    component={Link}
                                    href="https://www.youtube.com/channel/UC4jKsBxGE6v2YgNfgWMSBNg"
                                    target="_blank" rel="noopener noreferrer"
                                    color="primary"
                                    classes={{ colorPrimary: classes.colorPrimary }}>
                                    <YouTubeIcon />
                                </IconButton>
                                <IconButton
                                    aria-label="linkedin"
                                    component={Link}
                                    href="https://www.linkedin.com/company/super-responsable"
                                    target="_blank" rel="noopener noreferrer"
                                    color="primary"
                                    classes={{ colorPrimary: classes.colorPrimary }}>
                                    <LinkedInIcon />
                                </IconButton>
                                <IconButton
                                    aria-label="blog"
                                    component={Link}
                                    href="https://blog.super-responsable.org/"
                                    target="_blank" rel="noopener noreferrer"
                                    color="primary"
                                    classes={{ colorPrimary: classes.colorPrimary }}>
                                    <ShareIcon />
                                </IconButton>
                            </Grid>
                            <Grid item>
                                {t('footer.stayTuned')}
                            </Grid>
                        </Grid>
                        <Grid container direction="column" item sm={2} xs={12} className={classes.rightFooter}>
                            <Grid item>
                                <NavLink to="/suggestBrand" onClick={scrollUp}>
                                    {t('footer.suggestBrand')}
                                </NavLink>
                            </Grid>
                            <Grid item>
                                <NavLink to="/vendorCandidate" onClick={scrollUp}>
                                    {t('footer.vendorCandidate')}
                                </NavLink>
                            </Grid>
                            <Grid item>
                                <NavLink to="/contactUs?q=support" onClick={scrollUp}>
                                    {t('footer.needHelp')}
                                </NavLink>
                            </Grid>
                            <Grid item>
                                <NavLink to="/contactUs?q=contact" onClick={scrollUp}>
                                    {t('footer.contact')}
                                </NavLink>
                            </Grid>
                            <Grid item>
                                <NavLink to="/cgu" onClick={scrollUp}>
                                    {t('footer.cgu')}
                                </NavLink>
                            </Grid>
                            <Grid item>
                                <NavLink to="/rgpd" onClick={scrollUp}>
                                    {t('footer.rgpd')}
                                </NavLink>
                            </Grid>
                        </Grid>
                    </Grid>) : (
                        <KeepOnScroll>
                            <AppBar position="fixed" color="primary" className={classes.appBar} classes={{
                                colorPrimary: classes.bgColorPrimary
                            }} >
                                <Toolbar>
                                    <Grid container direction="column" alignItems="center" onClick={() => goTo('/vendors')}>
                                        <IconButton
                                            color="secondary"
                                            className={classes.iconMobile}
                                            classes={{ colorSecondary: classes.colorSecondary }}
                                            onClick={scrollUp}
                                        >
                                            <StorefrontIcon />
                                        </IconButton>
                                        <span className={classes.subButton}>{t('footer.directory')}</span>
                                    </Grid>
                                    <Grid container direction="column" alignItems="center" onClick={() => goTo("/user/rewards")}>
                                        <IconButton
                                            color="secondary"
                                            className={classes.iconMobile}
                                            classes={{ colorSecondary: classes.colorSecondary }}
                                            onClick={scrollUp}
                                        >
                                            <CardGiftcardIcon />
                                        </IconButton>
                                        <span className={classes.subButton}>{t('footer.rewards')}</span>
                                    </Grid>
                                    <Grid container direction="column" alignItems="center" onClick={() => goTo("/suggestBrand")}>
                                        <IconButton
                                            color="secondary"
                                            className={classes.iconMobile}
                                            classes={{ colorSecondary: classes.colorSecondary }}
                                            onClick={scrollUp}
                                        >
                                            <AddCircleOutlineIcon />
                                        </IconButton>
                                        <span className={classes.subButton}>{t('footer.contribute')}</span>
                                    </Grid>
                                    <Grid container direction="column" alignItems="center" onClick={() => props.loggedInUser ? goTo("/user/favorites") : setOpenDialogShouldBeConnected(true)}>

                                        {openDialogShouldBeConnected && <DialogShouldBeConnected open={openDialogShouldBeConnected} close={() => setOpenDialogShouldBeConnected(false)} />}

                                        <IconButton
                                            color="secondary"
                                            className={classes.iconMobile}
                                            classes={{ colorSecondary: classes.colorSecondary }}
                                            onClick={scrollUp}
                                        >
                                            <FavoriteBorderIcon />
                                        </IconButton>

                                        <span className={classes.subButton}>{t('footer.favorites')}</span>
                                    </Grid>

                                </Toolbar>
                            </AppBar>
                        </KeepOnScroll>
                    )

            }
        </div>
    );
}