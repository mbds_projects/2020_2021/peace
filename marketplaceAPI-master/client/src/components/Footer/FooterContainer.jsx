import * as React from 'react';

import Footer from './Footer';

import { useSelector } from "react-redux";

export default function FooterContainer() {

  const sidebar = useSelector((state) => state.sidebar.open);
  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  return (<Footer sideBarOpen={sidebar} loggedInUser={loggedInUser} />);

}