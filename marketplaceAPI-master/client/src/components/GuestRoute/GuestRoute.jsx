import * as React from 'react';

import { Route, Redirect } from 'react-router';
import Loading from '../../pages/Loading/LoadingPage';

import { useSelector } from "react-redux";

export default function GuestRoute(props) {
    const loggedInUser = useSelector((state) => state.authentication.currentUser);
    const isLoading = useSelector((state) => state.authentication.isLoading);
    const isAuthenticated = (loggedInUser != null)
    if (isLoading) {
        return <Loading />
    }

    if (isAuthenticated) {
        const renderComponent = () => (<Redirect to={{pathname: props.redirectionPath}}/>);
        return <Route {...props} component={renderComponent} render={undefined}/>;
    } else {
        return <Route {...props}/>;
    }
}