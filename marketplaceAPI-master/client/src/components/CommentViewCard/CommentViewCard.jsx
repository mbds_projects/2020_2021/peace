import * as React from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import Avatar from '@material-ui/core/Avatar';

import Grid from '@material-ui/core/Grid';

import routerHistory from '../../shared/router-history/router-history';

import Paper from '@material-ui/core/Paper';

import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';

import Truncate from 'react-truncate';

import useMediaQuery from '@material-ui/core/useMediaQuery';

import { useTranslation } from 'react-i18next';
const useStyles = makeStyles((theme) =>
  createStyles({
    grid: {
      paddingTop: "20px"
    },
    rate: {
      color: theme.colors.almond
    },
    avatar: {
      width: "60px",
      height: "60px",
      margin: "auto",
      border: "1px solid grey"
    },
    text: {
      //padding: "20px",
      fontSize: "16px",
      /*textOverflow: "ellipsis",*/
      whiteSpace: "nowrap",
      /*overflow: "hidden"*/
    },
    paper: {
      height: "250px"
    },
    about: {
      color: theme.colors.strawberry,
      padding: "20px",
      fontSize: "20px",
      cursor: "pointer"
    },
    padding: {
      paddingRight: "20px",
      paddingLeft: "20px"
      /*width: "80%"*/
    }
  }),
);

export default function CommentViewCard(props) {
  
  const { t } = useTranslation();

  const matches1540 = useMediaQuery('(max-width:1540px)');

  const matches1250 = useMediaQuery('(max-width:1250px)');

  const matches1040 = useMediaQuery('(max-width:1040px)');

  const matches = useMediaQuery('(max-width:768px)');

  const calculRate = (rate, id) => {
    let iconsStar = [];
    for(let i = 0; i < 5; i++) {
      if(i < rate) {
        iconsStar.push(<StarIcon className="icons star" key={`star-${id}-${i}`} />)
      } else {
        iconsStar.push(<StarBorderIcon className="icons star_border" key={`star-${id}-${i}`}/>)
      }
    }
    return iconsStar;
  };


  const handleClick = () => {
    routerHistory.push('/vendors/' + props.comment.vendor.id + "?name=" + props.comment.vendor.name);
  }


    let imgSrc  = "";

    let avatarText = "";
        
    if(props.comment.postedBy) {
      avatarText = `${props.comment.postedBy.name.charAt(0)}${props.comment.postedBy.surname.charAt(0)}`
      if(props.comment.postedBy && props.comment.postedBy.profilePictureUrl) {
        imgSrc = props.comment.postedBy.profilePictureUrl
        avatarText = "";
      }
    }

    let classes = useStyles();

    let rate = props.comment.rate ? props.comment.rate : 0;

    let widthTruncate = 300;
    if(matches1540) widthTruncate = 250;
    if(matches1250) widthTruncate = 200;
    if(matches1040) widthTruncate = 150;
    if(matches) widthTruncate = 0;

    return(
      <Paper className={classes.paper}>
        <Grid container direction="row" justify="space-between" alignItems="center" className={classes.grid}>
          <Grid item xs={4}>
            <Avatar className={classes.avatar} src={imgSrc}>{avatarText}</Avatar>
          </Grid>
          <Grid item xs={8}> 
            <Grid container direction="column">
              <Grid item><b>{props.comment.postedBy ? <b>{props.comment.postedBy.name}</b> : t('comments.userDeleted')}</b></Grid>
              <Grid item className={classes.rate}>{calculRate(rate, props.comment.id)}</Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Grid item className={classes.about} onClick={handleClick}>{t('comments.about')}{props.comment.vendor.name}</Grid>
          </Grid>
          <Grid item xs={12} className={classes.padding}>
            {/*<div className={classes.text}>{props.comment.text}</div>*/}
            <Truncate lines={3} width={widthTruncate} className={classes.text}>
              {props.comment.text}
            </Truncate>
          </Grid>
          
        </Grid>
        </Paper>
      
    )
  }