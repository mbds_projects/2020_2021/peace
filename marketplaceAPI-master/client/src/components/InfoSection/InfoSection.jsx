import React, { useState, useEffect } from 'react';

import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTranslation } from 'react-i18next';

import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import EmojiEventsOutlinedIcon from '@material-ui/icons/EmojiEventsOutlined';

import Chip from '@material-ui/core/Chip';

import Grid from '@material-ui/core/Grid';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import { Trans } from 'react-i18next';
import Avatar from '@material-ui/core/Avatar';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import { compareAsc } from 'date-fns'

const useStyles = makeStyles((theme) =>
    createStyles({
        infoSectionTitle: {
            fontSize: "30px"
        },
        chip: {
            margin: "5px",
            borderColor: theme.colors.almond
        },
        infoMobile: {
            padding: "20px"
        },
        moreFilters: {
            position: "relative",
            top: "10px",
            marginLeft: "5px"
        },
        labelMobile: {
            width: "100px"
        },
        rewardsMobile: {
            textAlign: "center"
        },
        howToClick: {
            fontStyle: "italic",
            textDecoration: "underline",
            cursor: "pointer"
        },
        diamond: {
            width: '60px',
            height: '60px',
            backgroundColor: theme.colors.almond,
            //borderRadius: "50%"
        },
    })
);
export default function InfoSection(props) {
    const { t, i18n } = useTranslation();
    const matches = useMediaQuery('(max-width:768px)');
    const vendor = props.vendor;
    const classes = useStyles();

    const [displayAllKeywords, setDisplayAllKeywords] = useState(false);
    const [openDialogLabels, setOpenDialogLabels] = useState(false);
    const [openDialogRewards, setOpenDialogRewards] = useState(false);

    useEffect(() => {
        let didCancel = false;
        !didCancel && setDisplayAllKeywords(false);
        return () => { didCancel = true }
    }, [vendor])

    const handleDisplayAllKeywords = () => {
        setDisplayAllKeywords(!displayAllKeywords)
    }
    const handleClickLabelsMobile = () => {
        setOpenDialogLabels(true);
    }

    const handleCloseLabelsMobile = () => {
        setOpenDialogLabels(false);
    }

    const handleClickRewardsMobile = () => {
        setOpenDialogRewards(true);
    }

    const handleCloseRewardsMobile = () => {
        setOpenDialogRewards(false);
    }

    const rewardsCount = vendor.rewards && vendor.rewards.length > 0 ? vendor.rewards.filter(reward => (reward.activated && !(reward.validity && compareAsc(new Date(reward.validity), new Date()) < 0))).length : 0;

    const vendorInfoSection =
        <div>
            <div className={classes.infoSectionTitle}>
                {vendor.name}
            </div>

            {
                props.categories && props.categories.map((el) => {
                    let catName = el.lang.find((l) => l.locale === i18n.language).value;
                    return (
                        <Chip
                            key={el.id}
                            label={catName}
                            className={classes.chip}
                            variant="outlined"
                        />
                    )
                })
            }
            {
                props.keywords && props.keywords.map((el) => {
                    let keyName = el.lang.find((l) => l.locale === i18n.language).value;
                    return (
                        <Chip
                            key={el.id}
                            label={keyName}
                            className={classes.chip}
                            variant="outlined"
                        />
                    )
                })
            }
        </div>;

    const vendorInfoSectionMobile =
        <div className={classes.infoMobile}><h2>
            {vendor.name}
        </h2>

            <Grid container direction="row">
                {vendor.labels && vendor.labels.length > 0 &&
                    <Grid item>
                        <Chip
                            avatar={<EmojiEventsOutlinedIcon />}
                            label={vendor.labels.length}
                            className={classes.chip}
                            variant="outlined"
                            onClick={handleClickLabelsMobile}
                        />
                    </Grid>

                }

                <Grid item>
                    <Chip
                        avatar={<CardGiftcardIcon />}
                        label={rewardsCount}
                        className={classes.chip}
                        variant="outlined"
                        onClick={handleClickRewardsMobile}
                    />
                </Grid>
                {
                    props.categories && props.categories.length > 0 &&
                    props.categories.map((cat) => {
                        let label = cat.lang.find((l) => l.locale === i18n.language).value;
                        return (
                            <Chip
                                key={`chip-${cat.id}`}
                                label={label}
                                className={classes.chip}
                                variant="outlined"
                            />
                        )
                    })
                }
                {
                    props.keywords && props.keywords.length > 0 && !displayAllKeywords &&
                    <Grid item>
                        <Chip
                            label={props.keywords[0].lang.find((l) => l.locale === i18n.language).value}
                            className={classes.chip}
                            variant="outlined"
                        />
                    </Grid>
                }
                {
                    vendor.keywords && vendor.keywords.length > 1 && !displayAllKeywords &&
                    <Grid item>
                        <div className={classes.moreFilters} onClick={handleDisplayAllKeywords}>+ {t('vendorProfile.filters')}</div>
                    </Grid>
                }
                {
                    props.keywords && props.keywords.length > 0 && displayAllKeywords &&
                    props.keywords.map((kw) => {
                        let label = kw.lang.find((l) => l.locale === i18n.language).value;
                        return (<Grid item key={`chip-${kw.id}`}>
                            <Chip
                                label={label}
                                className={classes.chip}
                                variant="outlined" $
                            />
                        </Grid>);
                    })

                }
                {
                    vendor.keywords && vendor.keywords.length > 0 && displayAllKeywords &&
                    <Grid item>
                        <div className={classes.moreFilters} onClick={handleDisplayAllKeywords}>- {t('vendorProfile.filters')}</div>
                    </Grid>
                }

            </Grid>

            <Dialog
                open={openDialogLabels && matches}
                onClose={handleCloseLabelsMobile}>
                <DialogTitle>{t('vendorProfile.labels.title')} {vendor.name}</DialogTitle>
                <DialogContent>
                    <p>{t('vendorProfile.labels.desc')}</p>
                    <Grid container direction="row">

                        {
                            vendor.labels && vendor.labels.map((el) => {
                                let imgSrc = '';
                                if (el.logo) {
                                    imgSrc = el.pictureUrl;
                                    return (
                                        <Grid item xs={6} key={`label-${el.id}`}>
                                            <img className={classes.labelMobile} key={el.id} src={imgSrc} alt={el.name} />
                                        </Grid>
                                    )
                                } else {
                                    return '';
                                }
                            })
                        }
                    </Grid>

                </DialogContent>
            </Dialog>

            <Dialog
                open={openDialogRewards && matches}
                onClose={handleCloseRewardsMobile}>
                <DialogTitle><Trans i18nKey="vendorProfile.rewards.title"
                    values={{ vendorName: vendor.name }}
                    components={[]} /></DialogTitle>
                <DialogContent>
                    <Grid container direction="row" justify="center" alignItems="center" className={classes.rewardsMobile}>
                        <Grid item xs={2}>
                            <Avatar className={classes.diamond} src="/assets/images/icons/diamond.png" />
                        </Grid>
                        <Grid item xs={12}>
                            {rewardsCount > 1 ?
                                <Trans i18nKey="vendorProfile.rewards.explanations"
                                    values={{ points: rewardsCount }}
                                    components={[<br />, <span onClick={props.goRewards} className={classes.howToClick}></span>]} />
                                :

                                <Trans i18nKey="vendorProfile.rewards.explanation"
                                    values={{ points: rewardsCount }}
                                    components={[<br />, <span onClick={props.goRewards} className={classes.howToClick}></span>]} />
                            }
                        </Grid>
                    </Grid>

                </DialogContent>
            </Dialog>

        </div>;

    return (<span>{matches ? vendorInfoSectionMobile : vendorInfoSection}</span>)
}
