import * as React from 'react';

import Avatar from '@material-ui/core/Avatar';

export default function UserCard(props) {
    const user = props.user;

    let imgSrc  = "";
    let avatarText = `${user.name.charAt(0)} ${user.surname.charAt(0)}`
      
    if(user.profilePictureUrl) {
      imgSrc = user.profilePictureUrl;
      avatarText = "";
    }
     return (
      <div>
        <Avatar style={{backgroundColor: user.defaultColor}} src={imgSrc}>{avatarText}</Avatar>                
        <h4>{user.name} {user.surname}</h4>
      </div>
    );
}
