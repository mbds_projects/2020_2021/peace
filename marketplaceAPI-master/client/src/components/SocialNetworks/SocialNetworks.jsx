import React, { useState } from 'react'

import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTranslation } from 'react-i18next';
import { useStyles } from './StylesSocialNetworks';

import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import LanguageIcon from '@material-ui/icons/Language';
import YouTubeIcon from '@material-ui/icons/YouTube';
import TwitterIcon from '@material-ui/icons/Twitter';
import PinterestIcon from '@material-ui/icons/Pinterest';
import EditIcon from '@material-ui/icons/Edit';

import IconButton from '@material-ui/core/IconButton';

import DialogUpdateSocialNetwork from "../../components/DialogDashboard/DialogUpdateSocialNetwork"

export default function SocialNetworks(props) {
  const { t } = useTranslation();

  const matches = useMediaQuery('(max-width:768px)');
  const vendor = props.vendor;
  const classes = useStyles();

  const [openEditNetworks, setOpenEditNetworks] = useState(false);

  const handleOpenEditNetworks = () => {
    setOpenEditNetworks(true);
  };

  return (
    <div className={matches ? classes.socialNetworksMobile : ''}>
      {
        matches ?
          <div className={classes.followTitle}>
            {(t('vendorProfile.follow'))} {vendor.name}
            {props.editModeChecked &&
              <IconButton onClick={handleOpenEditNetworks}>
                <EditIcon />
              </IconButton>
            }
          </div>
          :
          <span>
            <b>{(t('vendorProfile.follow'))} {vendor.name}</b>
            {
              props.editModeChecked &&
              <IconButton onClick={handleOpenEditNetworks}>
                <EditIcon />
              </IconButton>
            }
          </span>
      }
      <div>

        <IconButton
          color="primary"
          classes={{ colorPrimary: (vendor.socialNetworks.find((el) => el.type === "instagram") ? classes.instagram : classes.socialNetworkEmpty) }}
        >
          <InstagramIcon />
        </IconButton>

        <IconButton
          color="primary"
          classes={{ colorPrimary: (vendor.socialNetworks.find((el) => el.type === "facebook") ? classes.facebook : classes.socialNetworkEmpty) }}
        >
          <FacebookIcon />
        </IconButton>

        <IconButton
          color="primary"
          classes={{ colorPrimary: (vendor.socialNetworks.find((el) => el.type === "twitter") ? classes.twitter : classes.socialNetworkEmpty) }}
        >
          <TwitterIcon />
        </IconButton>

        <IconButton
          color="primary"
          classes={{ colorPrimary: (vendor.socialNetworks.find((el) => el.type === "pinterest") ? classes.pinterest : classes.socialNetworkEmpty) }}
        >
          <PinterestIcon />
        </IconButton>

        <IconButton
          color="primary"
          classes={{ colorPrimary: (vendor.socialNetworks.find((el) => el.type === "youtube") ? classes.youtube : classes.socialNetworkEmpty) }}
        >
          <YouTubeIcon />
        </IconButton>

        <IconButton
          color="primary"
          classes={{ colorPrimary: (vendor.socialNetworks.find((el) => el.type === "website") ? classes.website : classes.socialNetworkEmpty) }}
        >
          <LanguageIcon />
        </IconButton>

      </div>
      <DialogUpdateSocialNetwork
        openEditNetworks={openEditNetworks}
        setOpenEditNetworks={setOpenEditNetworks}
        vendor={vendor}
        updateInfoVendor={props.updateInfoVendor}
      />
    </div>
  )
}
