import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CardMembershipIcon from '@material-ui/icons/CardMembership';
import { fade, makeStyles, createStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import MuiTableCell from "@material-ui/core/TableCell";
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useTranslation } from 'react-i18next';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

const TableCell = withStyles({
    root: {
        borderBottom: "none"
    }
})(MuiTableCell);

const useStyles = makeStyles((theme) =>
    createStyles({
        table: {
            fontSize: "18px",
        },
        cell: {
            color: "grey",
            padding: "0px 10px",
            fontWeight: "bold",
            textAlign: "center",
            fontSize: "18px"
        },
        cellTitle: {
            color: "black",
            padding: "0px 10px",
            fontWeight: "bold",
            textAlign: "center",
            fontSize: "18px"
        },

        cellButton: {
            color: "black",
            padding: "20px 10px",
            fontWeight: "bold",
            textAlign: "center",
            fontSize: "18px"
        },
        gold: {
            color: theme.colors.gold,
            fontWeight: "bold",
            fontSize: "18px"
        },
        goldCheck: {
            color: theme.colors.gold,
            fontWeight: "bolder",
            fontSize: "25px"
        },
        subTitle: {
            backgroundColor: "#E4F8ED",
            padding: "0px 10px",
            fontSize: "18px"
        },
        weTakeCare: {
            fontFamily: "'Open Sans', sans-serif",
            fontSize: "18px"
        },
        almondBg: {
            backgroundColor: theme.colors.almond,
            color: "white",
            padding: "0px 10px",
            fontWeight: "bold",
            textAlign: "left",
            fontSize: "18px"
        },
        almondBgCenter: {
            backgroundColor: theme.colors.almond,
            color: "white",
            padding: "0px 10px",
            fontWeight: "bold",
            textAlign: "center",
            fontSize: "18px"
        },
        goldBg: {
            backgroundColor: theme.colors.gold,
            color: "white",
            padding: "0px 10px",
            fontWeight: "bold",
            textAlign: "left",
            fontSize: "18px"
        },
        lightAlmondCardBg: {
            backgroundColor: fade("#E4F8ED", 0.7),
            cursor: "pointer",
            "&:hover": {
                backgroundColor: "#E4F8ED"
            }
        },
        almondCardBg: {
            backgroundColor: fade(theme.colors.almond, 0.7),
            cursor: "pointer",
            "&:hover": {
                backgroundColor:theme.colors.almond
            }
        },
        goldCardBg: {
            backgroundColor: fade(theme.colors.gold, 0.7),
            cursor: "pointer",
            "&:hover": {
                backgroundColor: theme.colors.gold
            }
        },
        dialogPrice: {
            fontSize: "18px",
            fontWeight: "bold"
        }
    }));

const ColorButtonAlmond = withStyles((theme) => ({
    root: {
        color: "white",
        fontSize: "20px",
        fontWeight: "bolder",
        backgroundColor: theme.colors.almond
    },
}))(Button);


export default function PriceList(props) {
    const classes = useStyles();

    const { t } = useTranslation();
    const [openDialog, setOpenDialog] = useState(false);
    const [selectedSubscription, setSelectedSubscription] = useState(null);

    function createData(name, basicDetail, superDetail, superDetailPlus) {
        return { name, basicDetail, superDetail, superDetailPlus };
    }

    const rowsPage = [
        createData(t('priceList.page.pilot.name'), t('priceList.page.pilot.basic'), t('priceList.unlimited'), t('priceList.unlimited')),
        createData(t('priceList.page.pointOfSale'), 6, 20, t('priceList.unlimited')),
        createData(t('priceList.page.featuredProducts'), 6, 6, 6),
        createData(t('priceList.page.carousel'), 3, 3, 3),
        createData(t('priceList.page.storyTelling'), <CheckIcon />, <CheckIcon />, <CheckIcon />),
        createData(t('priceList.page.links'), <CheckIcon />, <CheckIcon />, <CheckIcon />),
        createData(t('priceList.page.weTakeCare'), <CheckIcon className={classes.goldCheck} />, <CheckIcon className={classes.goldCheck} />, <CheckIcon className={classes.goldCheck} />),
    ];

    const rowsCom = [
        createData(t('priceList.com.posts'), t('priceList.limited'), <CheckIcon />, <CheckIcon />),
        createData(t('priceList.com.newsletter'), <CloseIcon />, <CheckIcon />, <CheckIcon />),
        createData(t('priceList.com.articlesAndVideos'), t('priceList.limited'), <CheckIcon />, <CheckIcon />),
        createData(t('priceList.com.articleOrVideoDedicated.name'), <CloseIcon />, t('priceList.com.articleOrVideoDedicated.super'), <CheckIcon />),
        createData(t('priceList.com.redactionOpinion'), <CloseIcon />, <CloseIcon />, <CheckIcon className={classes.goldCheck} />)
    ];

    const rowsRewards = [
        createData(t('priceList.rewards.manage'), <CheckIcon />, <CheckIcon />, <CheckIcon />),
        createData(t('priceList.rewards.stats'), <CloseIcon />, <CheckIcon />, <CheckIcon />),
        createData(t('priceList.rewards.monthly'), <CloseIcon />, <CheckIcon className={classes.goldCheck} />, <CheckIcon className={classes.goldCheck} />),
        createData(t('priceList.rewards.weTakeCare'), <CheckIcon className={classes.goldCheck} />, <CheckIcon className={classes.goldCheck} />, <CheckIcon className={classes.goldCheck} />)
    ];

    const rowsMarketing = [
        createData(t('priceList.marketing.studies'), <CheckIcon />, <CheckIcon />, <CheckIcon />),
        createData(t('priceList.marketing.stats'), <CloseIcon />, <CheckIcon />, <CheckIcon />),
        createData(t('priceList.marketing.weTakeCare'), <CloseIcon />, <CloseIcon />, <CheckIcon className={classes.goldCheck} />)
    ];

    const rowsMarketplace = [
        createData(t('priceList.marketplace.integrate'), <CloseIcon />, <CheckIcon />, <CheckIcon />),
        createData(t('priceList.marketplace.stats'), <CloseIcon />, <CheckIcon />, <CheckIcon />),
        createData(t('priceList.marketplace.create'), <CloseIcon />, 20, t('priceList.unlimited')),
        createData(t('priceList.marketplace.statsWeTakeCare'), <CloseIcon />, <CheckIcon className={classes.goldCheck} />, <CheckIcon className={classes.goldCheck} />),
        createData(t('priceList.marketplace.weTakeCare'), <span className={classes.gold}>6</span>, <CheckIcon className={classes.goldCheck} />, <CheckIcon className={classes.goldCheck} />)
    ];

    const data = [
        {
            title: t('priceList.page.name'),
            rows: rowsPage
        },
        {
            title: t('priceList.com.name'),
            rows: rowsCom
        },
        {
            title: t('priceList.rewards.name'),
            rows: rowsRewards
        },
        {
            title: t('priceList.marketing.name'),
            rows: rowsMarketing
        },
        {
            title: t('priceList.marketplace.name'),
            rows: rowsMarketplace
        }
    ]

    const selectSubscription = (type) => {
        setSelectedSubscription(type)
        setOpenDialog(true);
    }

    const buttonRows = ["basic", "super", "superPlus"].map((el) => (
        <TableCell key={`subscribe_${el}`} align="right" className={classes.cellButton}>
            <ColorButtonAlmond
                variant="contained"
                color="primary"
                onClick={() => { selectSubscription(el) }}
                size="small"
                style={{ fontSize: "12px" }}
                endIcon={<CardMembershipIcon />}
            >
                {t('priceList.subscribe')}
            </ColorButtonAlmond>
        </TableCell>
    ))

    const priceDetails =
        <TableHead>
            <TableRow>
                <TableCell component="th" scope="row" style={{ padding: "0px" }}>

                </TableCell>
                <TableCell align="right" className={classes.almondBgCenter}><span>{t('priceList.basic.name')}</span></TableCell>
                <TableCell align="right" className={classes.almondBgCenter}><span>{t('priceList.super.name')}</span></TableCell>
                <TableCell align="right" className={classes.almondBgCenter}><span>{t('priceList.superPlus.name')}</span></TableCell>
            </TableRow>
            <TableRow>
                <TableCell align="right" className={classes.almondBg}><span>{t('priceList.monthly')}</span></TableCell>
                <TableCell align="right" className={classes.cellTitle}><span>{t('priceList.basic.monthly')}</span></TableCell>
                <TableCell align="right" className={classes.cellTitle}><span>{t('priceList.super.monthly')}</span></TableCell>
                <TableCell align="right" className={classes.cellTitle}><span>{t('priceList.superPlus.monthly')}</span></TableCell>
            </TableRow>
            <TableRow>
                <TableCell align="right" className={classes.almondBg}><span>{t('priceList.yearly')}</span></TableCell>
                <TableCell align="right" className={classes.cellTitle}><span>{t('priceList.basic.yearly')}</span></TableCell>
                <TableCell align="right" className={classes.cellTitle}><span>{t('priceList.super.yearly')}</span></TableCell>
                <TableCell align="right" className={classes.cellTitle}><span>{t('priceList.superPlus.yearly')}</span></TableCell>
            </TableRow>
            <TableRow>
                <TableCell align="right" className={classes.goldBg}><span>{t('priceList.weTakeCare')}</span></TableCell>
                <TableCell align="right" className={classes.cellTitle}><span className={classes.gold}>{t('priceList.basic.weTakeCare')}</span></TableCell>
                <TableCell align="right" className={classes.cellTitle}><span className={classes.gold}>{t('priceList.super.weTakeCare')}</span></TableCell>
                <TableCell align="right" className={classes.cellTitle}><span className={classes.gold}>{t('priceList.superPlus.weTakeCare')}</span></TableCell>
            </TableRow>
            <TableRow>
                <TableCell align="right"></TableCell>
                {buttonRows}
            </TableRow>
        </TableHead>

    const handleSelectedSubscriptionDetail = (choice) => {
        setOpenDialog(false);
        props.setSelectedSubscriptionDetail(choice);
    }

    const dialog =
        <Dialog fullWidth="true" open={openDialog} onClose={() => setOpenDialog(false)}>
            <DialogTitle>{t(`priceList.subscription`)} {t(`priceList.${selectedSubscription}.name`)}</DialogTitle>
            <DialogContent>
                <Grid container justify="center" spacing={2}>
                    <Grid item xs={4} onClick={() => handleSelectedSubscriptionDetail(`${selectedSubscription}.monthly`)}>
                        <Card variant="outlined" className={classes.lightAlmondCardBg}>
                            <CardContent>
                                <span className={classes.dialogPrice}>{t(`priceList.${selectedSubscription}.monthly`)} / {t(`priceList.byMonth`)}</span><br/>
                                <span>{t(`priceList.noObligation`)}</span>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={4} onClick={() => handleSelectedSubscriptionDetail(`${selectedSubscription}.yearly`)}>
                        <Card variant="outlined" className={classes.almondCardBg}>
                            <CardContent>
                                <span className={classes.dialogPrice}>{t(`priceList.${selectedSubscription}.yearly`)} / {t(`priceList.byYear`)}</span><br/>
                                <span>{t(`priceList.oneMonthOffer`)}</span>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={4} onClick={() => handleSelectedSubscriptionDetail(`${selectedSubscription}.weTakeCare`)}>
                        <Card variant="outlined" className={classes.goldCardBg}>
                            <CardContent>
                                <span className={classes.dialogPrice}>{t(`priceList.${selectedSubscription}.weTakeCare`)} / {t(`priceList.byYear`)}</span><br/>
                                <span>{t(`priceList.oneMonthOffer`)}</span><br/>
                                <span>{t(`priceList.withWeTakeCare`)}</span>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </DialogContent>
        </Dialog>

    return (
        <div>

            {dialog}
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    {priceDetails}
                    {
                        data.map((el, index) => (
                            <TableBody key={`table_${index}`}>
                                <TableRow>
                                    <TableCell className={classes.subTitle} colSpan={4}>{el.title}</TableCell>
                                </TableRow>
                                {el.rows.map((row, index2) => (
                                    <TableRow key={`table_row_${index2}`}>
                                        <TableCell component="th" scope="row" style={{ padding: "0px 10px", fontSize: "18px" }}>
                                            {row.name}
                                        </TableCell>
                                        <TableCell align="right" className={classes.cell}>{row.basicDetail}</TableCell>
                                        <TableCell align="right" className={classes.cell}>{row.superDetail}</TableCell>
                                        <TableCell align="right" className={classes.cell}>{row.superDetailPlus}</TableCell>
                                    </TableRow>
                                ))}
                                <TableRow>
                                    <TableCell align="right"></TableCell>
                                </TableRow>
                            </TableBody>
                        ))}

                </Table>
            </TableContainer>
        </div>
    );
}
