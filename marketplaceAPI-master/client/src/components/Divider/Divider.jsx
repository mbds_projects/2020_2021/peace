import React from "react";

import { makeStyles, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
        display: "flex",
        alignItems: "center"
      },
      border: {
        borderBottom: "1px solid black",
        width: "100%"
      },
      content: {
        padding: "0 10px 0 10px"
      }
  }),
);

export default function Divider(props) {
    let classes = useStyles();
  return (
    <div className={classes.container}>
      <div className={classes.border} />
      <span className={classes.content}>
        {props.text}
      </span>
      <div className={classes.border}/>
    </div>
  );
};
