import React, { useState } from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';

import ReCAPTCHA from 'react-google-recaptcha'
import { CONF } from '../../config/index'

const regexMail = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$')

const sitekey = CONF.sitekey;

const useStyles = makeStyles((theme) =>
  createStyles({
    paperForm: {
      padding: "20px"
    },
    input: {
      "& label": {
        fontSize: "14px"
      },
      "& .MuiFormLabel-root.Mui-error": {
        color: "grey"
      },
      "& fieldset": {
        borderColor: "lightgrey !important"
      }
    },
    littleStar: {
      color: "red",
      fontSize: "10px",
      verticalAlign: "top"
    },
    sendIcon: {
      marginTop: "15px",
      textAlign: "center"
    },
  }),
);

export default function VendorCandidateForm(props) {
  const { t, i18n } = useTranslation();

  const classes = useStyles();
  return (
    <Paper id="formCandidate" className={classes.paperForm}>
      <h2>{t('vendorCandidate.form.subtitle')}</h2>
      <TextField
        id="name"
        label={t('form.enterpriseName.label')}
        value={props.name}
        onChange={(event) => props.setName(event.target.value)}
        fullWidth
        required
        margin="dense"
        error={props.name.trim() === ""}
        helperText={!!props.name && props.name.trim() === "" ? t('form.enterpriseName.helper') : ''}
        variant="outlined"
        className={classes.input}
      />

      <TextField
        id="mail"
        label={t('form.mail.label')}
        value={props.mail}
        onChange={(event) => props.setMail(event.target.value)}
        fullWidth
        required
        margin="dense"
        helperText={props.mail !== '' && !regexMail.test(props.mail) ? t('form.mail.helper') : ''}
        error={!regexMail.test(props.mail)}
        variant="outlined"
        className={classes.input}
      />

      <TextField
        id="contactName"
        label={t('form.contactName.label')}
        value={props.contactName}
        onChange={(event) => props.setContactName(event.target.value)}
        fullWidth
        margin="dense"
        variant="outlined"
        className={classes.input}
      />

      <TextField
        id="phoneNumber"
        label={t('form.phone.label')}
        value={props.phoneNumber}
        onChange={(event) => { props.setPhoneNumber(event.target.value) }}
        fullWidth
        margin="dense"
        variant="outlined"
      />

      <TextField
        id="site"
        label={t('form.site.label')}
        value={props.site}
        onChange={(event) => { props.setSite(event.target.value) }}
        fullWidth
        margin="dense"
        variant="outlined"
      />

      {sitekey !== "" && <ReCAPTCHA
        sitekey={sitekey}
        onChange={(response) => props.setCaptchaChecked(true)}
      />
      }
    </Paper>
  )
}