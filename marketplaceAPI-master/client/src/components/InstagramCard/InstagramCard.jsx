import * as React from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

//import Link from '@material-ui/core/Link';

import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      margin: "0px",
      cursor: "pointer",
      "&:hover": {
        textDecoration: "none"
      }
    },

    grid: (props) => ({
      height: props.height,
      margin: "5px",
      //borderRadius: "15px",
      backgroundImage: `url("${props.background}")`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      backgroundSize: "cover"
    }),
  }),
);

export default function InstagramCard(props) {

  let classes = useStyles({ background: props.instagram.media, height: props.size ? props.size === "small" ? "100px": "200px" : '200px' });
  return (
    <a /*onClick={handleClick}*/ className={classes.root}
      href={props.instagram.link} target="_blank" rel="noopener noreferrer">
      <Paper className={classes.grid}/>
    </a>

  )
}