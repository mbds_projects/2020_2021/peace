import React, { useEffect, useState } from 'react';
import { Router } from 'react-router-dom';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { toast } from 'react-toastify';

import DialogCookies from '../DialogCookies/DialogCookies'
import DialogCustomerOpinion from '../DialogCustomerOpinion/DialogCustomerOpinion'

import routerHistory from '../../shared/router-history/router-history';
import { makeStyles, createStyles } from '@material-ui/core/styles';

import NavbarContainer from '../Navbar/NavbarContainer';
import FooterContainer from '../Footer/FooterContainer'

import useMediaQuery from '@material-ui/core/useMediaQuery';

import { addMonths } from 'date-fns'

import Grid from '@material-ui/core/Grid';

import { ThemeProvider } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import theme from '../../theme'
import { CONF } from '../../config/index'
import { Routes } from '../../routes'
import ReactGA from 'react-ga';
import { useCookies } from 'react-cookie';

import { useTranslation } from 'react-i18next';
import DialogCreateAccount from '../DialogCreateAccount/DialogCreateAccount'

import { Helmet } from "react-helmet";

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      height: "100%",
      display: "flex",
      flexDirection: "column",
      /*"& div": {
        flex: 1
      },*/
      "& .Toastify": {
        position: "absolute"
      }
    },
    flex: {
      flex: 1,
      width: "100%"
    },
    bodyContent: {
      width: "65%",
      margin: "auto",
      height: "calc(100% - 200px)",
      //marginTop: "100px",
      fontFamily: "'Open Sans', sans-serif",
    },
    bodyContentMobile: () => ({
      width: "100%",
      margin: "auto",
      fontFamily: "'Open Sans', sans-serif",
      //marginTop: "80px"
      marginBottom: "40px",
      height: "100%"
    })
  }),
);
export default function App(props) {
  const { t } = useTranslation();

  const [cookies, setCookie, removeCookie] = useCookies(['cookies_statistics', 'cookies_necessary', 'cookies_avis', 'cookies_mail']);
  const [openCookiesDialog, setOpenCookiesDialog] = useState(!(cookies['cookies_statistics'] === "true"));
  const [cookiesStatistics, setCookiesStatistics] = useState(cookies['cookies_statistics'] === "true");
  const [cookiesNecessary, setCookiesNecessary] = useState(cookies['cookies_necessary'] === "true");
  const [openCustomerOpinion, setOpenCustomerOpinion] = useState(true)
  const [openCreateAccount, setOpenCreateAccount] = useState(false);
  const [isAlreadyOpen, setIsAlreadyOpen] = useState(false)

  useEffect(() => {
    let didCancel = false
    let newDate = addMonths(new Date(), 13);
    if (!!cookies['cookies_necessary']) {
      let cookiesNecessaryAccepted = (cookies['cookies_necessary'] === "true")
      !didCancel && setCookiesNecessary(cookiesNecessaryAccepted);
      if (cookiesNecessaryAccepted) {
        if (props.loggedInUser) {
          if (!cookies['cookies_mail'] || cookies['cookies_mail'] !== props.loggedInUser.mail) {
            setCookie('cookies_mail', props.loggedInUser.mail, { path: '/', expires: newDate });
          }
          if (props.customerOpinions && props.customerOpinions.length > 0) {
            let mails = props.customerOpinions.map(c => c.mail);
            let isIncluded = mails.includes(props.loggedInUser.mail);
            if (isIncluded && (!cookies['cookies_avis'] || (cookies['cookies_avis'] === "false"))) {
              setCookie('cookies_avis', true, { path: '/', expires: newDate });
            }
            !didCancel && setOpenCustomerOpinion(!isIncluded);
          }
        } else {
          if (cookies['cookies_avis'] && cookies['cookies_avis'] === "true") {
            !didCancel && setOpenCustomerOpinion(false);
          } else {
            if (!!cookies['cookies_mail']) {
              let mail = cookies['cookies_mail'];
              let mails = props.customerOpinions.map(c => c.mail);
              let isIncluded = mails.includes(mail);
              if (isIncluded) {
                setCookie('cookies_avis', true, { path: '/', expires: newDate });
              }
              !didCancel && setOpenCustomerOpinion(!isIncluded);
            } else {
              !didCancel && setOpenCustomerOpinion(true);
            }
          }
        }
      } else {
        !didCancel && setOpenCustomerOpinion(false);
      }
    } else {
      !didCancel && setOpenCustomerOpinion(false);
    }


    if (!!cookies['cookies_statistics']) {
      !didCancel && setCookiesStatistics((cookies['cookies_statistics'] === "true"));
      if (!(cookies['cookies_statistics'] === "true")) {
        removeCookie('_ga');
        removeCookie('_gat');
        removeCookie('_gid');
        removeCookie('cookie_azameo_id');
        window.azameoSilent = 1
      } else {
        window.azameoSilent = 0
        if (window.azameoTag) window.azameoTag.Navigation();
      }
      !didCancel && setOpenCookiesDialog(false);
    } else {
      !didCancel && setOpenCookiesDialog(true);
    }

    return () => { didCancel = true }
  }, [cookies, props.customerOpinions, props.loggedInUser])

  useEffect(() => {
    let didCancel = false
    if (cookiesStatistics) {
      !didCancel && setOpenCookiesDialog(false);
      ReactGA.initialize(CONF.GA);
      let newDate = addMonths(new Date(), 13);
      setCookie('cookies_statistics', true, { path: '/', expires: newDate });
    }
    return () => { didCancel = true }
  }, [cookiesStatistics])


  useEffect(() => {
    if (cookiesNecessary) {
      let newDate = addMonths(new Date(), 13);
      setCookie('cookies_necessary', true, { path: '/', expires: newDate });
    }
  }, [cookiesNecessary])

  const validAllCookies = () => {
    let newDate = addMonths(new Date(), 13);
    setCookie('cookies_statistics', true, { path: '/', expires: newDate });
    setCookie('cookies_necessary', true, { path: '/', expires: newDate });
  }
  //ReactGA.pageview(window.location.pathname + window.location.search);

  useEffect(() => {
    if (isAlreadyOpen) {
      toast.success(t('dialog.customerOpinion.already'), {
        position: toast.POSITION.TOP_RIGHT
      });
    }
  }, [isAlreadyOpen])

  const opinionAlreadyWrite = () => {
    if (!isAlreadyOpen) {
      setIsAlreadyOpen(true);
    }
  }

  const matches = useMediaQuery('(max-width:768px)');

  const dismissCustomerOpinionDialog = () => {
    setOpenCustomerOpinion(false)
  }
  let classes = useStyles();
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <div className={classes.root}>
        <Router history={routerHistory}>
          <Helmet>
            <meta charSet="utf-8" />
            <title>Super Responsable</title>
            <link rel="canonical" href="https://www.super-responsable.org/" />
            <meta property="og:title" content="Super Responsable - Notre site communautaire est en ligne !" />
            <meta property="og:description" content="Pas besoin de super pouvoirs pour devenir un héros du quotidien" />
            <meta property="og:url" content="https://www.super-responsable.org/" />
            <meta property="og:image" content="https://www.super-responsable.org/assets/images/metadataSite.png" />
            <meta name="fb:page_id" content="2685293161696289" />
          </Helmet>
          {openCustomerOpinion && <DialogCustomerOpinion openCreateAccountDialog={() => setOpenCreateAccount(true)} dismissCustomerOpinionDialog={dismissCustomerOpinionDialog} customerOpinions={props.customerOpinions} loggedInUser={props.loggedInUser} opinionAlreadyWrite={opinionAlreadyWrite} />}
          <DialogCookies open={openCookiesDialog} validAllCookies={validAllCookies} />
          {openCreateAccount && <DialogCreateAccount open={openCreateAccount} setOpen={setOpenCreateAccount} />}
          <Grid container direction="column" justify="space-between" className={classes.flex}>
            <Grid item xs={12} className={classes.flex}>
              <NavbarContainer dismissCustomerOpinionDialog={dismissCustomerOpinionDialog} />
              <div className={matches ? classes.bodyContentMobile : classes.bodyContent}>
                <Routes />
              </div>
              <Grid item xs={12}>
                <FooterContainer />
              </Grid>
            </Grid>
          </Grid>
        </Router>
        <ToastContainer />
      </div>
    </ThemeProvider>
  );
}