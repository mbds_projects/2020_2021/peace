import React, { useEffect, useState } from 'react';
import ReactGA from 'react-ga';
import App from './App';
import { customerOpinionsGql } from '../../graphQL/customerOpinion';
import { setGeoloc } from '../../shared/store/actions/user.actions';
import { useSelector, useDispatch } from "react-redux";

export default function AppContainer() {
  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  const [customerOpinions, setCustomerOpinions] = useState([])
  const dispatch = useDispatch();
  useEffect(() => {
    let didCancel = false

    async function retrieveCustomerOpinions() {
      let customerOpinionsTmp = await customerOpinionsGql();
      !didCancel && setCustomerOpinions(customerOpinionsTmp)
    }

    retrieveCustomerOpinions();
    return () => { didCancel = true }
  }, [])

  navigator.geolocation.getCurrentPosition((pos) => {
    var crd = pos.coords;
    dispatch(setGeoloc({ latitude: crd.latitude, longitude: crd.longitude }, true));
    ReactGA.event({
      category: 'Geolocation',
      action: `Accepted`,
      label: `Global`
    });
  }, () => {
    let nicePosition = {
      latitude: 43.7031300,
      longitude: 7.2660800
    }
    dispatch(setGeoloc(nicePosition, false));
    ReactGA.event({
      category: 'Geolocation',
      action: `Refused`,
      label: `Global`
    });
  }, {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  });

  return (<App
    customerOpinions={customerOpinions}
    loggedInUser={loggedInUser}
    />);
}