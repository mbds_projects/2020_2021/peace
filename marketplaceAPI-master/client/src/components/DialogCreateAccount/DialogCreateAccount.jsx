import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { Trans } from 'react-i18next';
import routerHistory from '../../shared/router-history/router-history';

const useStyles = makeStyles((theme) =>
  createStyles({
    icon: {
      width: "150px"
    },
    title: {
      fontFamily: "'League Spartan', sans-serif",
      color: theme.colors.almond,
      fontSize: "30px"
    },
    text: {
      fontSize: "30px",
      fontFamily: "'League Spartan', sans-serif",
    },
    center: {
      textAlign: "center"
    },
    button: {
      backgroundColor: theme.colors.almond,
      color: "white"
    }
  }),
);
export default function DialogCreateAccount(props) {
  const { t } = useTranslation();

  let classes = useStyles();

  const handleClick = () => {
    props.setOpen(false);
    routerHistory.push('/login');
  }

  return (
    <Dialog open={props.open} onClose={() => props.setOpen(false)} className={classes.center}>
      <DialogTitle >
        <img alt="" className={classes.icon} src="/assets/images/icons/achievement.png" /><br />
        <span className={classes.title}>SUPER</span>
      </DialogTitle>
      <DialogContent>
        <span className={classes.text}>
          <Trans i18nKey="dialog.createAccount.info"
            components={[<br />]} />
        </span>
        <br />
        <br />

        <Button
          variant="contained"
          color="primary"
          onClick={handleClick}
          className={classes.button}
        >
          {t('dialog.createAccount.button')}
        </Button>
      </DialogContent>
    </Dialog>

  );
}