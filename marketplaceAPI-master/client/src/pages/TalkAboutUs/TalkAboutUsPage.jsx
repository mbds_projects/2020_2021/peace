import * as React from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import InterviewCard from '../../components/InterviewCard/InterviewCard';

import Grid from '@material-ui/core/Grid';

import { useTranslation } from 'react-i18next';

import ReactGA from 'react-ga';

ReactGA.pageview('/talkAboutUs');

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      margin: "auto", 
      padding: "20px"
    },
    marginBottom: {
      marginBottom: "100px"
    }
  }),
);

export default function TalkAboutUsPage(props) {
  let classes = useStyles();

  const { t } = useTranslation();

  let byNumber = Math.ceil(props.interviews.length/3);
  let grids = [];
  let interviewsTmp = [...props.interviews];
  for(let i = 0; i < byNumber - 1; i++) {
    grids.push(interviewsTmp.splice(0, byNumber));
  }
  return (
    <div className={classes.root}>
      <h2>{t('footer.talkOfUs')}</h2>
      {grids.length > 0 &&
        <Grid container direction="row" justify="flex-start" spacing={3} className={classes.marginBottom}>
          {
            grids.map((grid, index) => {
              return (<Grid item xs={12} sm={4} key={`grid_${index}`}>
                <Grid container direction="column" justify="flex-start" spacing={3} item xs={12}>
                  {grid.map((interview) => {
                    return(
                      <Grid item xs={12} sm={12} key={interview.link}>
                          <InterviewCard interview={interview} type={interview.type} />
                      </Grid>)
                  })}
                </Grid>
              </Grid>)
            })
          }
          
        {/*<Grid item xs={12} sm={4}><Grid container direction="column" justify="flex-start" spacing={3} item xs={12}>
            {grids[1].map((interview) => {
              return(
                <Grid item xs={12} sm={12}>
                    <InterviewCard interview={interview} type={interview.type} />
                </Grid>)
            })}
        </Grid></Grid>
        <Grid item xs={12} sm={4}><Grid container direction="column" justify="flex-start" spacing={3} item xs={12}>
            {grids[2].map((interview) => {
              return(
                <Grid item xs={12} sm={12}>
                    <InterviewCard interview={interview} type={interview.type} />
                </Grid>)
            })}
          </Grid></Grid>*/}
        </Grid>}
        
    </div>

  )
}