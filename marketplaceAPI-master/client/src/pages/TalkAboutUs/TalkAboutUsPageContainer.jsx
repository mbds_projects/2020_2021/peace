import React, { useState, useEffect } from 'react';

import TalkAboutUsPage from './TalkAboutUsPage';

import { interviewsGql } from '../../graphQL/interview';

export default function TalkAboutUsPageContainer() {

    const [interviews, setInterviews] = useState([]);
    useEffect(() => {

        async function retrieveInterviews() {

        let interviewsTmp = await interviewsGql();
        if(interviews.length === 0) {
            setInterviews(interviewsTmp)
        }
        }

        retrieveInterviews();
    });

    return (<TalkAboutUsPage interviews={interviews} />);
}
