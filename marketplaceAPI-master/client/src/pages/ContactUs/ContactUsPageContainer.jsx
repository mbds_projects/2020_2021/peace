import * as React from 'react';

import { contactUsGql } from '../../graphQL/contactUs'
import ContactUsPage from './ContactUsPage';

import { useSelector } from "react-redux";

import queryString from 'query-string';

import { useLocation} from 'react-router-dom';
  
import { updateUserGql } from '../../graphQL/user'

import store from '../../shared/store/store';

import { setUser } from '../../shared/store/actions/authentication.actions';

import { toast } from 'react-toastify';

import { useTranslation } from 'react-i18next';
import { toastError } from '../../shared/utils/error';

export default function ContactUsPageContainer() {
    
    const location = useLocation();
    const { t } = useTranslation();
    let search = queryString.parse(location.search)
    let query = ""
    if(search["q"]) {
        query = search["q"].toString()
    }

    const perfomFormAttempt = async (params) => {
        try {
            let contact = await contactUsGql({input: params})
            if(contact) {
                toast.success(t('contact.toast.messageSent'), {
                    position: toast.POSITION.TOP_RIGHT
                });
            } else {
                toast.error(t('contact.toast.errorSent'), {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
            if(loggedInUser && loggedInUser.newsletter !== params.newsletter) {
                let user = await updateUserGql({input: {newsletter: params.newsletter}}).then(res => res).catch(e => toastError(e.message, t));
                if(user) {
                    store.dispatch(setUser(user));
                }
            }
        } catch(e) {
            toast.error(t('contact.toast.errorSent'), {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }

    const handleFormAttempt = (params) => {
        perfomFormAttempt(params);
    }


  const loggedInUser = useSelector((state) => state.authentication.currentUser);
    
    return (<ContactUsPage 
        handleFormAttempt={handleFormAttempt} 
        loggedInUser={loggedInUser}
        query={query}/>);
      
}