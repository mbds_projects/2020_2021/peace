import React, { useState, useEffect } from 'react';
import { createStyles, makeStyles, withStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

import InputBase from '@material-ui/core/InputBase';

import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Switch from '@material-ui/core/Switch';

import ReCAPTCHA from 'react-google-recaptcha'
import { CONF } from '../../config/index'

import { useTranslation } from 'react-i18next';

import ReactGA from 'react-ga';

ReactGA.pageview('/contactUs');
const sitekey = CONF.sitekey;

const regexMail = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$')

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      padding: "15px",
      width: "99%",
      margin: "0 auto"
    },
    textarea: {
      width: '100%'
    },
    textareaRequired: {
      width: '100%',
      borderColor: "red"
    },
    questionsSelect: {
      width: "100%",
      marginBottom: '20px'
    },
    questionsSelectRequired: {
      width: "100%",
      marginBottom: '20px',
      "& .MuiSelect-select": {
        borderColor: "red"
      }
    },
    linkFbGroup: {
      '& a': {
        color: "rgba(0, 0, 0, 0.54)",
        margin: 0,
        fontSize: "0.75rem",
        marginTop: "8px",
        minHeight: "1em",
        textAlign: "left",
        fontWeight: 400,
        lineHeight: "1em",
        letterSpacing: "0.03333em"
      }
    },
    sendButton: {
      marginTop: "20px"
    }
  }),
);


const BootstrapInput = withStyles((theme) =>
  createStyles({
    root: {
      'label + &': {
        marginTop: theme.spacing(3),
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #ced4da',
      fontSize: 16,
      padding: '10px 26px 10px 12px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      // Use the system font instead of the default Roboto font.
      fontFamily: "'Open Sans', sans-serif",
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
  }),
)(InputBase);

export default function ContactUsPage(props) {
  const { t } = useTranslation();

  const [mail, setMail] = useState('');
  const [name, setName] = useState('');
  const [surname, setSurname] = useState('');
  const [message, setMessage] = useState('');
  const [selectedQuestion, setSelectedQuestion] = React.useState('');
  const [newsletter, setNewsletter] = useState(false);
  const [sent, setSent] = useState(false);
  const [captchaChecked, setCaptchaChecked] = useState(false);

  useEffect(() => {
    if (props.loggedInUser && props.loggedInUser.newsletter !== undefined) setNewsletter(props.loggedInUser.newsletter);
  }, [props.loggedInUser]);

  if (props.loggedInUser) {
    if (mail === '') setMail(props.loggedInUser.mail);
    if (name === '') setName(props.loggedInUser.name);
    if (surname === '') setSurname(props.loggedInUser.surname);
  }

  const questionsOptions = [
    t('contact.questions.candidate'),
    t('contact.questions.support'),
    t('contact.questions.brand'),
    t('contact.questions.contact'),
    t('contact.questions.cost')
  ];

  useEffect(() => {
    switch (props.query) {
      case 'candidate':
        setSelectedQuestion("0")
        break;
      case 'support':
        setSelectedQuestion("1")
        break;
      case 'brand':
        setSelectedQuestion("2")
        break;
      case 'contact':
        setSelectedQuestion("3")
        break;
      case 'cost':
        setSelectedQuestion("4")
        break;
      default:
        break;
    }
    window.scrollTo(0, 0);
  }, [props.query]);

  const classes = useStyles();

  const handleMailAttempt = () => {
    let selectedQuestionInt = parseInt(selectedQuestion)
    props.handleFormAttempt({
      mail, message, selectedQuestion: selectedQuestionInt, name, surname, newsletter
    });
    setMessage("");
    setSent(true);
  }

  const handleChange = (event) => {
    setMail(event.target.value);
    setSent(false);
  };

  const handleName = (event) => {
    setName(event.target.value);
    setSent(false);
  };

  const handleSurname = (event) => {
    setSurname(event.target.value);
    setSent(false);
  };

  const handleChangeMessage = (event) => {
    setMessage(event.target.value);
    setSent(false);
  };

  const handleQuestionSelect = (event) => {
    setSelectedQuestion(String(event.target.value));
    setSent(false);
  };

  const handleChangeNewsletter = (event) => {
    setNewsletter(event.target.checked);
    setSent(false);
  }

  const handleCaptchaResponseChange = (response) => {
    setCaptchaChecked(true);
    setSent(false);
  }


  const questionsItem = questionsOptions.map((value, index, array) => {
    return <MenuItem value={index} key={index}>{value}</MenuItem>
  });

  return (

    <div className={classes.root}>
      <Paper className={classes.root}>
        <h2>{t('contact.title')}</h2>
        <Select

          className={selectedQuestion === "" ? classes.questionsSelectRequired : classes.questionsSelect}
          value={selectedQuestion}
          displayEmpty
          onChange={handleQuestionSelect}
          input={<BootstrapInput />}
        >
          <MenuItem value="" disabled>
            {t('contact.subject')}
          </MenuItem>
          {questionsItem}
        </Select>
        <TextareaAutosize
          rows={4}
          onChange={handleChangeMessage}
          placeholder={t('contact.message')}
          className={message === "" ? classes.textareaRequired : classes.textarea}
          required
          value={message}
        />


        <TextField
          id="name"
          label={props.loggedInUser && props.loggedInUser.role === "vendor" ? t('form.brand.label') : t('form.name.label')}
          value={name}
          onChange={handleName}
          fullWidth
          required={!props.loggedInUser}
          margin="normal"
          disabled={!!props.loggedInUser}

          error={!props.loggedInUser && name.trim() === ""}
          helperText={!!name && name.trim() === "" ? (props.loggedInUser && props.loggedInUser.role === "vendor" ? t('form.brand.helper') : t('form.name.helper')) : ''}

          variant="outlined"
        />

        {!(props.loggedInUser && props.loggedInUser.role === "vendor") &&
          <TextField
            id="surname"
            label={t('form.surname.label')}
            value={surname}
            onChange={handleSurname}
            fullWidth
            required={!props.loggedInUser}
            disabled={!!props.loggedInUser}
            margin="normal"
            error={!props.loggedInUser && surname.trim() === ""}
            helperText={!!surname && surname.trim() === "" ? t('form.surname.helper') : ''}
            variant="outlined"
          />
        }

        <TextField
          id="mail"
          label={t('form.mail.label')}
          value={mail}
          onChange={handleChange}
          fullWidth
          required={!props.loggedInUser}
          disabled={!!props.loggedInUser}
          margin="normal"
          helperText={mail !== '' && !regexMail.test(mail) ? t('form.mail.helper') : ''}
          error={!props.loggedInUser && !regexMail.test(mail)}
          variant="outlined"
        />

        {
          !props.loggedInUser && sitekey !== "" &&
          <ReCAPTCHA
            sitekey={sitekey}
            onChange={handleCaptchaResponseChange}
          />
        }

        <FormGroup>
          <FormControlLabel
            control={<Switch checked={newsletter} onChange={handleChangeNewsletter} value={newsletter} />}
            label={t('form.newsletter.label')}
          />
          <FormHelperText>
            <span className={classes.linkFbGroup}>
              <a href="https://www.facebook.com/groups/256271651950647/" target="_blank" rel="noopener noreferrer">
                {t('link.facebookGroupLink')}
              </a>
            </span>
          </FormHelperText>
        </FormGroup>

        <Button
          disabled={
            mail === '' || (mail !== '' && !regexMail.test(mail))
            || message === ''
            || selectedQuestion === ''
            || (!(props.loggedInUser && props.loggedInUser.role === "vendor") && (!surname || surname.trim() === ""))
            || (!name || name.trim() === "")
            || sent
            || (!props.loggedInUser && sitekey !== "" && !captchaChecked)
          }
          variant="contained"
          onClick={handleMailAttempt}
          id="send-btn"
          fullWidth
          className={classes.sendButton}>
          {t('contact.button')}
        </Button>
      </Paper>
    </div>

  );
}
