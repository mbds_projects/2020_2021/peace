import React, { useState, useEffect } from 'react';

import VendorTutorialsPage from './VendorTutorialsPage';

import { articlesB2BGql } from '../../graphQL/article';

import LoadingPage from '../Loading/LoadingPage';

export default function VendorTutorialsPageContainer() {
  const [articles, setArticles] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    let didCancel = false
    async function retrieveArticles() {
      let articles = await articlesB2BGql();
      !didCancel && setArticles(articles)
      setLoading(false);
    }
    setLoading(true);
    retrieveArticles();
    return () => { didCancel = true }
  }, [])

  if(loading) return <LoadingPage />
  return <VendorTutorialsPage articles={articles}/>
}
