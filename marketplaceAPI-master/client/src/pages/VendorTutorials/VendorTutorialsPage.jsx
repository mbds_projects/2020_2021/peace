import React, { useState, useEffect } from 'react';

import MediaCard from '../../components/MediaCard/MediaCard';

import Grid from '@material-ui/core/Grid';
import { NavLink } from 'react-router-dom';

import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import { useTranslation } from 'react-i18next';

import { Trans } from 'react-i18next';

import ReactGA from 'react-ga';
ReactGA.pageview('/vendor/tutorials');

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      marginTop: "25px",
      height: "100%",
    },
    rootEmpty: {
      height: "100%",
      marginTop: "25px",
      backgroundImage: `url("/assets/images/bgSuggestedForYou.png")`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "top",
      backgroundSize: "cover"
    },
    bgOpacity: {
      height: "100%",
      background: "rgba(255, 255, 255, 0.7)"
    },
    noCards: {
      width: "100%"
    },
    emptyState: {
      textAlign: "center",
      "& img": {
        width: "50px"
      },
      "& a": {
        color: "grey"
      }
    },
    favIcon: {
      fontSize: "50px",
      color: theme.colors.strawberry
    },
    quote: {
      fontSize: "15px"
    }
  }),
);

export default function VendorTutorialsPage(props) {

  const classes = useStyles();
  const { t } = useTranslation();

  const GADetectClick = (category, action) => {
    ReactGA.event({
      category: category,
      action: action,
      label: 'Home'
    });
  }

  const returnCard = (element, id, GAtitle, GAsubTitle, xs, sm) => {
    return (
      <Grid item xs={xs} sm={sm} key={id} onClick={() => GADetectClick(GAtitle, GAsubTitle)}>
        {element}
      </Grid>
    )
  }


  let articles = props.articles ? props.articles.map((article, index) => returnCard(<MediaCard element={article} />, `article_${index}`, 'Articles',  `Click on article ${article.link}`, 12, 4)) : []

  return (
    <div className={classes.root}>
      <h2><b>{t('articlesB2B.title')}</b></h2>
      <Trans i18nKey="articlesB2B.subTitle" components={[<br/>]}/>
      
          {
            articles.length > 0 ?
            <Grid container spacing={3}>{articles}</Grid>
            :
            <Grid container>   
              <div className={classes.noCards}>
                  <div className={classes.emptyState}>
                    <FavoriteBorderIcon className={classes.favIcon}/>
                    <br/><br/>
                    <h3>{t('articlesB2B.notFound.title')}</h3>
                    <p>{t('articlesB2B.notFound.subTitle')}</p>
                  </div>
              </div>
            </Grid>
          }
      
    </div>
  );
}