import React, { useState, useCallback, useRef } from "react";
import _ from 'lodash'
import Button from '@material-ui/core/Button';

import TextField from '@material-ui/core/TextField';

import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import StorefrontIcon from '@material-ui/icons/Storefront';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import { TableSortLabel } from '@material-ui/core';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import EditIcon from '@material-ui/icons/Edit';
import PublishIcon from '@material-ui/icons/Publish';

import ReactCrop from "react-image-crop";
import 'react-image-crop/dist/ReactCrop.css';

import FilterCard from '../../components/FilterCard/FilterCard';

const useStyles = makeStyles({
  root: {

  },
  table: {
    minWidth: 650,
  },
  search: {
    marginBottom: "20px"
  },
  imgPreview: {
    width: "400px"
  },
  filterCard: {
    width: "200px",
    "& div": {
      width: "200px"
    }
  }
});

export default function AdminDashboardVendorCandidatePage(props) {
  const classes = useStyles();
  const { t } = useTranslation();

  const [openShop, setOpenShop] = React.useState(false);

  const [selectedVendorCandidateId, setSelectedVendorCandidateId] = useState();

  const [sortByName, setSortByName] = React.useState(true);
  const [sortBy, setSortBy] = React.useState({name: 'name', value: 'desc'});
  const [search, setSearch] = React.useState('');
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [sortByCreatedAt, setSortByCreatedAt] = React.useState(true);
  const [sortByUpdatedAt, setSortByUpdatedAt] = React.useState(true);

  const handleChangeSortByName = (event) => {
    setSortByName(!sortByName);    
    setSortBy({
      name: "name",
      value: sortByName ? 'desc' : 'asc'
    })  
    setSortByUpdatedAt(true);
    setSortByCreatedAt(true);
  };

  const handleChangeSortByCreatedAt = (event) => {
    setSortByCreatedAt(!sortByCreatedAt);
    setSortBy({
      name: "createdAt",
      value: sortByCreatedAt ? 'desc' : 'asc'
    })

    setSortByName(true);   
    setSortByUpdatedAt(true);   
  };

  const handleChangeSortByUpdatedAt = (event) => {
    setSortByUpdatedAt(!sortByUpdatedAt);
    setSortBy({
      name: "updatedAt",
      value: sortByUpdatedAt ? 'desc' : 'asc'
    })

    setSortByName(true);     
    setSortByCreatedAt(true);   
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleShopOpen = (vendorCandidateId) => {
    setOpenShop(true);
    setSelectedVendorCandidateId(vendorCandidateId)
  }

  const handleCloseShop = () => {
    setOpenShop(false);
  }

  const handleTransformShop = () => {
    props.transformVendorCandidate(selectedVendorCandidateId);
  }

  return (
      <div className={classes.root}>
        <IconButton onClick={props.goBack}>
          <ArrowBackIcon />
        </IconButton>
        <h4>{t('adminDashboard.vendorsCandidate')}</h4>

        <TextField
          margin="dense"
          id="search"
          label={t('adminDashboard.search')}
          fullWidth
          type="text"
          value={search}
          variant="outlined"
          onChange={handleSearch}
          className={classes.search}
        />
      
        {
          <Dialog open={openShop} onClose={handleCloseShop} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">{t('adminDashboard.transform')}</DialogTitle>
            <DialogContent>
              <DialogContentText>
              {t('adminDashboard.brandToShop')}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleCloseShop} color="primary">
              {t('adminDashboard.cancel')}
              </Button>
              <Button onClick={handleTransformShop} color="primary">
              {t('adminDashboard.transform')}
              </Button>
            </DialogActions>
          </Dialog>
        }
        
        <TableContainer component={Paper}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>                

          <TableCell>{t('adminDashboard.transform')}</TableCell>
            <TableCell sortDirection={sortByName ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByName ? 'desc' : 'asc'}
                onClick={handleChangeSortByName}
              >
                {t('adminDashboard.name')}
              </TableSortLabel>
            </TableCell>
            <TableCell >
                {t('adminDashboard.mail')}
            </TableCell>
            <TableCell >
                {t('adminDashboard.subscriptionType')}
            </TableCell>
            <TableCell >
                {t('adminDashboard.subscriptionFrequency')}
            </TableCell>
            <TableCell align="right" sortDirection={sortByCreatedAt ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByCreatedAt ? 'desc' : 'asc'}
                onClick={handleChangeSortByCreatedAt}
              >
                {t('adminDashboard.createdAt')}
              </TableSortLabel>
            </TableCell>
            <TableCell align="right" sortDirection={sortByUpdatedAt ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByUpdatedAt ? 'desc' : 'asc'}
                onClick={handleChangeSortByUpdatedAt}
              >
                {t('adminDashboard.updatedAt')}
              </TableSortLabel>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            _.orderBy(_.filter(props.vendorsCandidate, function(vendorCandidate) { return vendorCandidate.name.toLowerCase().includes(search.toLowerCase()) })
              , [sortBy.name]
              , [sortBy.value])
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((vendorCandidate) => {
              let subscription = vendorCandidate.subscriptionType ? vendorCandidate.subscriptionType.split(".") : ['', '']
              return (
              <TableRow key={vendorCandidate.name}>
              <TableCell component="th" scope="row">{vendorCandidate.vendor ? 'Created' : <StorefrontIcon onClick={() => handleShopOpen(vendorCandidate.id)}/>}</TableCell>
                <TableCell component="th" scope="row">{vendorCandidate.name}</TableCell>
                <TableCell component="th" scope="row">{vendorCandidate.mail}</TableCell>
                <TableCell component="th" scope="row">{subscription[0]}</TableCell>
                <TableCell component="th" scope="row">{subscription[1]}</TableCell>
                <TableCell align="right">{vendorCandidate.createdAt}</TableCell>
                <TableCell align="right">{vendorCandidate.updatedAt}</TableCell>
              </TableRow>
            )}
          )}
        </TableBody>
      </Table>
    </TableContainer>
    <TablePagination
      rowsPerPageOptions={[5, 10, 25]}
      component="div"
      count={(_.filter(props.vendorsCandidate, function(vendorCandidate) { return vendorCandidate.name.toLowerCase().includes(search.toLowerCase()) })).length}
      rowsPerPage={rowsPerPage}
      page={page}
      onChangePage={handleChangePage}
      onChangeRowsPerPage={handleChangeRowsPerPage}
    />
      </div>

  );
}