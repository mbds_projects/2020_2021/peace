import React, { useState, useEffect } from 'react';

import AdminDashboardVendorCandidatePage from './AdminDashboardVendorCandidatePage';
import LoadingPage from '../Loading/LoadingPage';

import { toastError } from '../../shared/utils/error';

import { toast } from 'react-toastify';

import { useSelector } from "react-redux";

import { vendorsCandidateGql, transformVendorCandidateGql } from '../../graphQL/vendorCandidate'

import routerHistory from '../../shared/router-history/router-history';

import { useTranslation } from 'react-i18next';
export default function AdminDashboardVendorCandidatePageContainer() {
  const { t } = useTranslation();
  const loggedInUser = useSelector(state => state.authentication.currentUser);
  const [vendorsCandidate, setVendorsCandidate] = useState([]);

  const goBack = () => {
    routerHistory.push('/admin/');
  }

  const updateData = async() => {
    let vendorsCandidateTmp = await vendorsCandidateGql();
    setVendorsCandidate(vendorsCandidateTmp);
  }

  useEffect(() => {
    updateData();
  }, []);

  const transformVendorCandidate = async (id) => {
    try {
      let transformVendorCandidate = await transformVendorCandidateGql({id})
      if(transformVendorCandidate) {
        updateData();
        toast.success(t('adminDashboard.toast.suggestedBrandTransformed'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('adminDashboard.toast.suggestedBrandTransformedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return transformVendorCandidate;
    } catch(e) {
      toast.error(t('adminDashboard.toast.suggestedBrandTransformedError'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }
  }

  
  if (!loggedInUser) {
    return <LoadingPage />
  }

  let propsChild = {
    transformVendorCandidate,
    goBack,
    vendorsCandidate
  }

  return (<AdminDashboardVendorCandidatePage {...propsChild} />);
}
