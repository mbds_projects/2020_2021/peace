import React, { useState, useEffect } from 'react';

import UserSuggestedForYouPage from './UserSuggestedForYouPage';

import {vendorsAssimilateGql } from '../../graphQL/vendor';

import LoadingPage from '../Loading/LoadingPage';

import { useSelector } from "react-redux";

export default function UserSuggestedForYouPageContainer() {
  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  const [vendorsAssimilate, setVendorsAssimilate] = useState([]);  
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function retrieveSuggestedForYou() {
      let keywords = loggedInUser.keywords ? loggedInUser.keywords.map((el) => el.id) : []
      let vendorsAssimilate = await vendorsAssimilateGql({categories: [], keywords, vendorId: null, limit: 9});
      setVendorsAssimilate(vendorsAssimilate);
      setLoading(false)
    }

    if(!!loggedInUser) {
      setLoading(true)
      retrieveSuggestedForYou();
    }
  }, [loggedInUser])

  if(loading) return <LoadingPage />
  return <UserSuggestedForYouPage vendors={vendorsAssimilate} isLogged={!!loggedInUser}/>
}
