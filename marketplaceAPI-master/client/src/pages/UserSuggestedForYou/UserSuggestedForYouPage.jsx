import React, { useState, useEffect } from 'react';

import VendorCard from '../../components/VendorCard/VendorCard';

import Grid from '@material-ui/core/Grid';
import { NavLink } from 'react-router-dom';

import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FormatQuoteIcon from '@material-ui/icons/FormatQuote';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import { useTranslation } from 'react-i18next';

import { Trans } from 'react-i18next';

import ReactGA from 'react-ga';
import DialogShouldBeConnected from '../../components/DialogShouldBeConnected/DialogShouldBeConnected'

ReactGA.pageview('/user/suggestedForYou');

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      marginTop: "25px",
      height: "100%",
    },
    rootEmpty: {
      height: "100%",
      marginTop: "25px",
      backgroundImage: `url("/assets/images/bgSuggestedForYou.png")`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "top",
      backgroundSize: "cover"
    },
    bgOpacity: {
      height: "100%",
      background: "rgba(255, 255, 255, 0.7)"
    },
    noCards: {
      width: "100%"
    },
    emptyState: {
      textAlign: "center",
      "& img": {
        width: "50px"
      },
      "& a": {
        color: "grey"
      }
    },
    favIcon: {
      fontSize: "50px",
      color: theme.colors.strawberry
    },
    quote: {
      fontSize: "15px"
    }
  }),
);

export default function UserSuggestedForYou(props) {

  const classes = useStyles();
  const { t } = useTranslation();
  const [openDialogShouldBeConnected, setOpenDialogShouldBeConnected] = useState(!props.isLogged);

  const vendorsCards = props.vendors.map(vendor => {
    return <Grid item xs={12} sm={4} key={vendor.id}>
            <VendorCard
              vendor={vendor}
            />
          </Grid>
  });

  return (
    <div className={classes.root}>
      <h2><b>{t('suggestedForYou.title')}</b></h2>
      {openDialogShouldBeConnected && 
        <DialogShouldBeConnected open={openDialogShouldBeConnected} close={() => setOpenDialogShouldBeConnected(false)} />
      }
      
      <Trans i18nKey="suggestedForYou.subTitle" components={[<br/>]}/>
      {
        openDialogShouldBeConnected ? 
        <div className={classes.rootEmpty}>
          <div className={classes.bgOpacity}>
          </div>
        </div>
        :
        <span>
          {
            vendorsCards.length > 0 ?
            <Grid container spacing={3}>{vendorsCards}</Grid>
            :
            <Grid container>   
              <div className={classes.noCards}>
                  <div className={classes.emptyState}>
                    <FavoriteBorderIcon className={classes.favIcon}/>
                    <br/><br/>
                    <h3>{t('suggestedForYou.notFound.title')}</h3>
                    <p>{t('suggestedForYou.notFound.subTitle')}</p>
                    <NavLink to="/user">{t('suggestedForYou.notFound.link')}</NavLink>
                  </div>
              </div>
            </Grid>
          }
        </span>
      }
      
    </div>
  );
}