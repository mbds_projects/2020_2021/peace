import * as React from 'react';

import routerHistory from '../../shared/router-history/router-history';

import PasswordRecreatePage from './PasswordRecreatePage';
import { updateForgottenPasswordGql } from '../../graphQL/user'

import { login } from '../../core/modules/authentication.module';
import { login as loginStore } from '../../shared/store/actions/authentication.actions';

import { useDispatch } from "react-redux";

import { toast } from 'react-toastify';

import { useTranslation } from 'react-i18next';

export default function PasswordRecreatePageContainer() {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const handleIncorrectIdParameter = () => {
    routerHistory.replace('/404');
  }

  const performPasswordAttempt = async(password) => {

    const token = window.location.pathname.split('/createForgottenPassword/')[1];
    if(!token) {
      handleIncorrectIdParameter();
    }
    let tokenRetrieved = await updateForgottenPasswordGql({input: {password: password, token: token}})
    if(tokenRetrieved) {
      let user = await login(tokenRetrieved.token);
      dispatch(loginStore(user));
    } else {
      toast.error(t('error.somethingWrongAppends'), {
        position: toast.POSITION.TOP_RIGHT
      });
    }
  }

  const handlePasswordAttempt = (password) => {
    performPasswordAttempt(password);
  }

  return (<PasswordRecreatePage onPasswordAttempt={handlePasswordAttempt}/>);

}