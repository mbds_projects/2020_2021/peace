import React, { useState } from 'react';

import { NavLink } from 'react-router-dom';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import { useTranslation } from 'react-i18next';

import ReactGA from 'react-ga';

ReactGA.pageview('/createForgottenPassword');

const regexPassword = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}");

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      padding: "15px"
    }
  }),
);

export default function PasswordRecreatePage(props) {
  const [password, setPassword] = useState('');

  const classes = useStyles();
  const { t } = useTranslation();

  const handlePasswordAttempt = () => {
    if(regexPassword.test(password)) {
      props.onPasswordAttempt(password);
    }
  }

  const handleChange = (event) => {
    setPassword(event.target.value);
  };

  return (
    <Grid
    container
    spacing={0}
    direction="column"
    alignItems="center"
    justify="center"
  >
    <Grid item xs={10}>

    <h2>{t('passwordRecreate.title')}</h2>
    <Paper className={classes.root}>
      <TextField
              id="password"
              label={t('form.password.label')}
              value={password}
              onChange={handleChange}
              fullWidth
              required
              margin="normal"

              type="password"
              helperText={password !== '' && !regexPassword.test(password) ? t('form.password.helper') : ''}
              error={!regexPassword.test(password)}
              variant="outlined"
            />

        <Button disabled={
            password==='' || (password!=='' && !regexPassword.test(password))
            } 
              variant="contained" 
              onClick={handlePasswordAttempt}
              id="recreatePwd-btn"
              fullWidth>
                {t('passwordRecreate.button')}
             </Button>
        <div className="link"><NavLink to="/login">{t('link.loginLink')}</NavLink></div>
        </Paper>
        </Grid>  

    </Grid>
  );
}