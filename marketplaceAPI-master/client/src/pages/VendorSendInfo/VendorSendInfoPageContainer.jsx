import React, { useState, useEffect } from 'react';

import VendorSendInfoPage from './VendorSendInfoPage';

import { vendorsLimitedAdminGql, vendorLimitedAdminGql } from '../../graphQL/vendor'

import { vendorCandidateSaveInfoGql } from '../../graphQL/vendorCandidate'

import { toast } from 'react-toastify';

import { toastError } from '../../shared/utils/error';

import { useTranslation } from 'react-i18next';

import routerHistory from '../../shared/router-history/router-history';

import { useSelector, useDispatch } from "react-redux";

import { categoriesGql } from '../../graphQL/category'

import { keywordsGql } from '../../graphQL/keyword'

import { gendersGql } from '../../graphQL/gender'

import { labelsGql } from '../../graphQL/label'


import { setCategories as setCategoriesInternal } from '../../shared/store/actions/category.actions';

import { setKeywords as setKeywordsInternal } from '../../shared/store/actions/keyword.actions';

import { setGenders as setGendersInternal } from '../../shared/store/actions/gender.actions';

import { useParams } from 'react-router-dom';
import { decode } from 'jsonwebtoken';
import { useHistory } from 'react-router-dom';

export default function VendorSendInfoPageContainer() {
  const { t } = useTranslation();

  const [vendors, setVendors] = useState([]);
  const [categories, setCategories] = useState([]);
  const [keywords, setKeywords] = useState([]);
  const [genders, setGenders] = useState([]);
  const [labels, setLabels] = useState([]);
  const dispatch = useDispatch();

  const categoriesInternal = useSelector((state) => state.category.categories);
  const keywordsInternal = useSelector((state) => state.keyword.keywords);
  const gendersInternal = useSelector((state) => state.gender.genders);

  const { id } = useParams();
  const [name, setName] = useState("");

  const goBack = () => {
    routerHistory.push('/admin/');
  }

  useEffect(() => {
    let didCancel = false;
    let tokenResult = decode(id);
    !didCancel && setName(tokenResult.name)
    async function retrieveVendors() {
      let vendors = await vendorsLimitedAdminGql();
      !didCancel && setVendors(vendors)
    }

    const getCategories = async () => {
      if (categoriesInternal.length <= 0) {
        let categories = await categoriesGql();
        dispatch(setCategoriesInternal(categories));
        setCategories(categories)
      } else {
        setCategories(categoriesInternal)
      }
    }

    const getGenders = async () => {
      if (gendersInternal.length <= 0) {
        let genders = await gendersGql();
        dispatch(setGendersInternal(genders));
        setGenders(genders)
      } else {
        setGenders(gendersInternal)
      }
    }

    const getKeywords = async () => {
      if (keywordsInternal.length <= 0) {
        let keywords = await keywordsGql();
        dispatch(setKeywordsInternal(keywords));
        setKeywords(keywords);
      } else {
        setKeywords(keywordsInternal)
      }
    }

    const getLabels = async () => {
      let labels = await labelsGql();
      setLabels(labels);
    }
   

    Promise.all([getCategories(), getKeywords(), getGenders(), getLabels()])

    return () => { didCancel = true }
  }, [id])

  const retrieveVendor = async (id) => {
    let profileVendor = await vendorLimitedAdminGql({ id });
    return profileVendor;
  }

  const displayInput = (props) => {
    return (
      <h1>Bienvenue !</h1>
    )
}
const history = useHistory();
  const saveInfo = async (input) => {
    try {
      let vendorId = decode(id).id
      let save = await vendorCandidateSaveInfoGql({ input, vendorId })
      if (save) {
      
        toast.success(t('vendorCandidate.toast.creationSuccess'), {
          position: toast.POSITION.TOP_RIGHT
        }, 
        );
      } else {
        toast.error(t('error.somethingWrongAppends'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    } catch (e) {
      toastError(e.message, t)
    }
    history.push("/Home");
  }
 

  let propsChild = { retrieveVendor, goBack, vendors, categories, keywords, genders, labels, saveInfo, name, displayInput}

  return (<VendorSendInfoPage {...propsChild} />);
}
