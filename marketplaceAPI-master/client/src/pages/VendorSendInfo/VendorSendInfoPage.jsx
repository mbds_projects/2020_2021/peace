import React, { useState, useEffect } from "react";

import * as _ from 'lodash';
import Button from '@material-ui/core/Button';


import { useTranslation } from 'react-i18next';

import Select from 'react-select';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    width: "99%",
    flexGrow: 1,
    backgroundColor: "white",
  },
  avatar: {
    width: "100px !important",
    height: "100px !important",
    margin: "auto",
    fontSize: "50px",
    border: "2px solid lightgrey"
  },
});




  export default function VendorSendInfoPage(props) { 
  const { t, i18n } = useTranslation();

  const classes = useStyles();

  const [selectedVendor, setSelectedVendor] = useState({});
  const [selectedCompleteVendor, setSelectedCompleteVendor] = useState({});
  const [selectedCategories, setSelectedCategories] = useState([]);
  const [selectedKeywords, setSelectedKeywords] = useState([]);
  const [selectedGenders, setSelectedGenders] = useState([]);
  const [selectedLabels, setSelectedLabels] = useState([]);

  const [openEditProfilePicture, setOpenEditProfilePicture] = useState(false);
  const [openEditLogo, setOpenEditLogo] = useState(false);

  const [state, setState] = useState({
    onlineShop: false,
    clickAndCollect: false,
    published: false,
    activated: false,
    compteur:[]
  });

 
  
  let vendorsOptions = props.vendors.map((vendor) => {
    return {
      value: vendor.id,
      label: vendor.name
    }
  });

  let categoriesOptions = props.categories.map((category) => {

    let catName = category.lang.find((l) => l.locale === i18n.language).value;
    return {
      value: category.id,
      label: catName
    }
  }
  );

  let keywordsOptions = props.keywords.map((keyword) => {
    let keyName = keyword.lang.find((l) => l.locale === i18n.language).value;
    return {
      value: keyword.id,
      label: keyName
    }
  });

  let gendersOptions = props.genders.map((gender) => {
    let gdName = gender.lang.find((l) => l.locale === i18n.language).value;
    return {
      value: gender.id,
      label: gdName
    }
  });

  let labelsOptions = props.labels.map((label) => {
    return {
      value: label.id,
      label: label.name
    }
  });

  
 

  const handleSelectVendor = async (vendor) => {
    setSelectedVendor(vendor);
    let completeVendor = await props.retrieveVendor(vendor.value);
    setSelectedCompleteVendor(completeVendor)
    if (completeVendor.categories && completeVendor.categories.length > 0) {
      let catOptsTmp = completeVendor.categories.map(cat => categoriesOptions.find(co => co.value === cat));
      setSelectedCategories(catOptsTmp)
    }
    if (completeVendor.keywords && completeVendor.keywords.length > 0) {
      let kwOptsTmp = completeVendor.keywords.map(kw => keywordsOptions.find(ko => ko.value === kw));
      setSelectedKeywords(kwOptsTmp)
    }
    if (completeVendor.genders && completeVendor.genders.length > 0) {
      let gdOptsTmp = completeVendor.genders.map(go => gendersOptions.find(gd => gd.value === go));
      setSelectedGenders(gdOptsTmp)
    }
    if (completeVendor.labels && completeVendor.labels.length > 0) {


// laboptsTmp return le id du label
      let labOptsTmp = completeVendor.labels.map(lo => labelsOptions.find(lab => lab.value === lo));
      // setSelectedLabels()affiche par le suite le label correspondant a cet id
      setSelectedLabels(labOptsTmp)
      

    }
  


const aff =  <div> <input name="labelFile" type="file"/> </div> ;

    let vendorState = {
      onlineShop: completeVendor.onlineShop || false,
      clickAndCollect: completeVendor.clickAndCollect || false,
      published: completeVendor.published || false,
      activated: completeVendor.activated || false
    }
    setState(vendorState);
  }

  const handleSave = async () => {
    let input = {
      categories: selectedCategories.map(c => c.value),
      keywords: selectedKeywords.map(k => k.value),
      genders: selectedGenders.map(g => g.value),
      labels: selectedLabels.map(l => l.value)
    }
    props.saveInfo(input)
  }

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };


/////////////////////
const addLabel = (labOptsTmp)=>{
 setSelectedLabels(labOptsTmp)
  setState({compteur: [...state.compteur, ""]})
}
///////////



var i=0

let test= state.compteur.map((c,index)=>{
 
  if(selectedLabels == 'undefined'){
    console.log(selectedLabels)
    selectedLabels = []
  }
  if(selectedLabels.length >= i+1 && selectedLabels.length !=0 ){

    return(
      
      <div  key={index}>
        
         
            <label>{selectedLabels.map(l => l.label)[i++]}    </label>
        
          <input  name="LabelFile"
        
        type="file"
        accept=".jpg, .jpeg, .png, .pdf"    
         />   
 
      </div>
   
  )
  }
 
  
  

      
})

  
////////////////////
 

  return (
    <div className={classes.root}>
      <br></br> <br></br>
      <h3>FORMULAIRE ENSEIGNE</h3>
      <br></br>
      
        <h2>Bonjour {props.name} veuillez saisir vos données </h2>
 <br/> 

        <div>
          <Select
            name="categoryInput"
            options={categoriesOptions}
            placeholder={t('vendorCandidate.categories')}
            isMulti
            value={selectedCategories}
            onChange={(categories) => setSelectedCategories(categories)}
            multi={true}
          />
 <br/>
          <Select
            name="keywordInput"
            
            options={keywordsOptions}
            placeholder={t('vendorCandidate.keywords')}
            isMulti
            value={selectedKeywords}
            onChange={(keywords) => setSelectedKeywords(keywords)}
            multi={true}
          />
   <br/>
          <Select
            name="genderInput"
            options={gendersOptions}
            placeholder={t('genders')}
            isMulti
            value={selectedGenders}
            onChange={(genders) => setSelectedGenders(genders)}
            multi={true}
          />
    <br/>
    
          <Select
            name="labelInput"
            options={labelsOptions}
            placeholder={t('labels')}
            isMulti
            value={selectedLabels}

            onChange={(labels)=>addLabel(labels)}
            
          
            multi={true}
          />  
          
<br/> 


{test}

        </div>
  
  
      <br/>
      <Button
        variant="contained"
        color="primary"
        onClick={handleSave}>   Save   </Button>



    </div>
  );
}