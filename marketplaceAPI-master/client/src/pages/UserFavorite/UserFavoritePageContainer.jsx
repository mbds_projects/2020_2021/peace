import React, { useState, useEffect } from 'react';

import UserFavoritePage from './UserFavoritePage';

import { userFavoritesGql } from '../../graphQL/user'

import LoadingPage from '../Loading/LoadingPage';

import { useSelector } from "react-redux";

export default function UserFavoritePageContainer() {
  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  const [favorites, setFavorites] = useState([]);  
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function retrieveFavorites() {
      let favorites = await userFavoritesGql();
      setFavorites(favorites);
      setLoading(false)
    }

    if(!!loggedInUser) {
      setLoading(true)
      retrieveFavorites();
    }
  }, [loggedInUser])

  if(loading) return <LoadingPage />
  return <UserFavoritePage vendors={favorites} isLogged={!!loggedInUser}/>
}
