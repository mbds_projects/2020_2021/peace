import React, { useState, useEffect } from 'react';

import AdminDashboardVendorInfoPage from './AdminDashboardVendorInfoPage';

import { vendorsLimitedAdminGql, vendorLimitedAdminGql } from '../../graphQL/vendor'

import { adminVendorInfoUpdateGql, adminVendorProfilePictureUpdateGql, adminVendorLogoUpdateGql } from '../../graphQL/adminVendorInfo'

import { toast } from 'react-toastify';

import { toastError } from '../../shared/utils/error';

import { useTranslation } from 'react-i18next';

import routerHistory from '../../shared/router-history/router-history';

import { useSelector, useDispatch } from "react-redux";

import { categoriesGql } from '../../graphQL/category'

import { keywordsGql } from '../../graphQL/keyword'

import { gendersGql } from '../../graphQL/gender'

import { labelsGql } from '../../graphQL/label'

import { setCategories as setCategoriesInternal } from '../../shared/store/actions/category.actions';

import { setKeywords as setKeywordsInternal } from '../../shared/store/actions/keyword.actions';

import { setGenders as setGendersInternal } from '../../shared/store/actions/gender.actions';

export default function AdminDashboardVendorInfoPageContainer() {
  const { t } = useTranslation();

  const [vendors, setVendors] = useState([]);
  const [categories, setCategories] = useState([]);
  const [keywords, setKeywords] = useState([]);
  const [genders, setGenders] = useState([]);
  const [labels, setLabels] = useState([]);
  const dispatch = useDispatch();

  const categoriesInternal = useSelector((state) => state.category.categories);
  const keywordsInternal = useSelector((state) => state.keyword.keywords);
  const gendersInternal = useSelector((state) => state.gender.genders);

  const goBack = () => {
    routerHistory.push('/admin/');
  }

  useEffect(() => {
    let didCancel = false;

    async function retrieveVendors() {
      let vendors = await vendorsLimitedAdminGql();
      !didCancel && setVendors(vendors)
    }

    const getCategories = async () => {
      if (categoriesInternal.length <= 0) {
        let categories = await categoriesGql();
        dispatch(setCategoriesInternal(categories));
        setCategories(categories)
      } else {
        setCategories(categoriesInternal)
      }
    }

    const getGenders = async () => {
      if (gendersInternal.length <= 0) {
        let genders = await gendersGql();
        dispatch(setGendersInternal(genders));
        setGenders(genders)
      } else {
        setGenders(gendersInternal)
      }
    }

    const getKeywords = async () => {
      if (keywordsInternal.length <= 0) {
        let keywords = await keywordsGql();
        dispatch(setKeywordsInternal(keywords));
        setKeywords(keywords);
      } else {
        setKeywords(keywordsInternal)
      }
    }

    const getLabels = async () => {
      let labels = await labelsGql();
      setLabels(labels);
    }


    Promise.all([retrieveVendors(), getCategories(), getKeywords(), getGenders(), getLabels()])

    return () => { didCancel = true; };
  }, [])

  const retrieveVendor = async (id) => {
    let profileVendor = await vendorLimitedAdminGql({ id });
    return profileVendor;
  }

  const saveInfo = async (input, vendorId) => {
    try {
      let update = await adminVendorInfoUpdateGql({ input, vendorId })
      if (update) {
        toast.success(t('adminDashboard.toast.vendorInfoUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('error.somethingWrongAppend'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    } catch (e) {
      toastError(e.message, t)
    }
  }

  const uploadProfilePicture = async (file, vendorId) => {
    try {
      let res = await adminVendorProfilePictureUpdateGql({ file, vendorId });
      if (res) {
        toast.success(t('vendorDashboard.toast.imageUploaded'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('vendorDashboard.toast.imageUploadedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return res;
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const uploadLogo = async (file, vendorId) => {
    try {
        let res = await adminVendorLogoUpdateGql({ file, vendorId });
        if (res) {
            toast.success(t('vendorDashboard.toast.imageUploaded'), {
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            toast.error(t('vendorDashboard.toast.imageUploadedError'), {
                position: toast.POSITION.TOP_RIGHT
            });
        }
        return res;
    } catch (e) {
        toastError(e.message, t);
    }
}

  let propsChild = { retrieveVendor, goBack, vendors, categories, keywords, genders, labels, saveInfo, uploadProfilePicture, uploadLogo }

  return (<AdminDashboardVendorInfoPage {...propsChild} />);
}
