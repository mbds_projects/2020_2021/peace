import React, { useState, useEffect } from "react";

import * as _ from 'lodash';
import Button from '@material-ui/core/Button';

import { useTranslation } from 'react-i18next';

import Select from 'react-select';

import { makeStyles } from '@material-ui/core/styles';

import VendorCard from '../../components/VendorCard/VendorCard';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import DialogUpdateProfilePicture from '../../components/DialogDashboard/DialogUpdateProfilePicture';
import DialogUpdateLogo from '../../components/DialogDashboard/DialogUpdateLogo';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import PhotoCamera from '@material-ui/icons/PhotoCamera';

import Grid from '@material-ui/core/Grid';
import Switch from '@material-ui/core/Switch';

import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const useStyles = makeStyles({
  root: {
    width: "99%",
    flexGrow: 1,
    backgroundColor: "white",
  },
  avatar: {
    width: "100px !important",
    height: "100px !important",
    margin: "auto",
    fontSize: "50px",
    border: "2px solid lightgrey"
  },
});

export default function AdminDashboardVendorInfoPage(props) {
  const { t, i18n } = useTranslation();

  const classes = useStyles();

  const [selectedVendor, setSelectedVendor] = useState({});
  const [selectedCompleteVendor, setSelectedCompleteVendor] = useState({});
  const [selectedCategories, setSelectedCategories] = useState([]);
  const [selectedKeywords, setSelectedKeywords] = useState([]);
  const [selectedGenders, setSelectedGenders] = useState([]);
  const [selectedLabels, setSelectedLabels] = useState([]);

  const [openEditProfilePicture, setOpenEditProfilePicture] = useState(false);
  const [openEditLogo, setOpenEditLogo] = useState(false);

  const [state, setState] = useState({
    onlineShop: false,
    clickAndCollect: false,
    published: false,
    activated: false
  });

  let vendorsOptions = props.vendors.map((vendor) => {
    return {
      value: vendor.id,
      label: vendor.name
    }
  });

  let categoriesOptions = props.categories.map((category) => {

    let catName = category.lang.find((l) => l.locale === i18n.language).value;
    return {
      value: category.id,
      label: catName
    }
  }
  );

  let keywordsOptions = props.keywords.map((keyword) => {
    let keyName = keyword.lang.find((l) => l.locale === i18n.language).value;
    return {
      value: keyword.id,
      label: keyName
    }
  });

  let gendersOptions = props.genders.map((gender) => {
    let gdName = gender.lang.find((l) => l.locale === i18n.language).value;
    return {
      value: gender.id,
      label: gdName
    }
  });

  let labelsOptions = props.labels.map((label) => {
    return {
      value: label.id,
      label: label.name
    }
  });

  const handleSelectVendor = async (vendor) => {
    setSelectedVendor(vendor);
    let completeVendor = await props.retrieveVendor(vendor.value);
    setSelectedCompleteVendor(completeVendor)
    if (completeVendor.categories && completeVendor.categories.length > 0) {
      let catOptsTmp = completeVendor.categories.map(cat => categoriesOptions.find(co => co.value === cat));
      setSelectedCategories(catOptsTmp)
    }
    if (completeVendor.keywords && completeVendor.keywords.length > 0) {
      let kwOptsTmp = completeVendor.keywords.map(kw => keywordsOptions.find(ko => ko.value === kw));
      setSelectedKeywords(kwOptsTmp)
    }
    if (completeVendor.genders && completeVendor.genders.length > 0) {
      let gdOptsTmp = completeVendor.genders.map(go => gendersOptions.find(gd => gd.value === go));
      setSelectedGenders(gdOptsTmp)
    }
    if (completeVendor.labels && completeVendor.labels.length > 0) {
      let labOptsTmp = completeVendor.labels.map(lo => labelsOptions.find(lab => lab.value === lo));
      setSelectedLabels(labOptsTmp)
    }


    let vendorState = {
      onlineShop: completeVendor.onlineShop || false,
      clickAndCollect: completeVendor.clickAndCollect || false,
      published: completeVendor.published || false,
      activated: completeVendor.activated || false
    }
    setState(vendorState);
  }

  const handleSave = async () => {
    let input = {
      categories: selectedCategories.map(c => c.value),
      keywords: selectedKeywords.map(k => k.value),
      genders: selectedGenders.map(g => g.value),
      labels: selectedLabels.map(l => l.value),
      onlineShop: state.onlineShop,
      clickAndCollect: state.clickAndCollect,
      published: state.published,
      activated: state.activated
    }
    props.saveInfo(input, selectedVendor.value)
    let completeVendor = await props.retrieveVendor(selectedVendor.value);
    setSelectedCompleteVendor(completeVendor)
  }

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const uploadProfilePicture = async (file) => {
    await props.uploadProfilePicture(file, selectedVendor.value);
    let completeVendor = await props.retrieveVendor(selectedVendor.value);
    setSelectedCompleteVendor(completeVendor)
  }


  let dialogProfilePictureProps = {
    openEditProfilePicture,
    setOpenEditProfilePicture,
    uploadProfilePicture,
    vendor: selectedCompleteVendor
  }


  const uploadLogo = async (file) => {
    await props.uploadLogo(file, selectedVendor.value);
    let completeVendor = await props.retrieveVendor(selectedVendor.value);
    setSelectedCompleteVendor(completeVendor)
  }


  let dialogLogoProps = {
    openEditLogo,
    setOpenEditLogo,
    uploadLogo,
    vendor: selectedCompleteVendor
  }

  let imgSrc = "";
  let avatarText = '';
  if (!_.isEmpty(selectedCompleteVendor)) {
    if (selectedCompleteVendor.logoUrl) imgSrc = selectedCompleteVendor.logoUrl
    else {
      avatarText = `${selectedCompleteVendor.name.charAt(0)}`
    }
  }


  return (
    <div className={classes.root}>

      <IconButton onClick={props.goBack}>
        <ArrowBackIcon />
      </IconButton>
      <h3>{t('adminDashboard.vendor.info')}</h3>
      <div>
        <Select
          name="vendorId"
          options={vendorsOptions}
          placeholder={t('adminDashboard.vendor.select')}
          value={selectedVendor}
          onChange={handleSelectVendor}
          /*className={classes.select}*/
          styles={{ menu: base => ({ ...base, position: 'relative' }) }}
          multi={false}
        />
      </div>

      {!_.isEmpty(selectedVendor) &&
        <div>
          <Select
            name="categoryInput"
            options={categoriesOptions}
            placeholder={t('vendorCandidate.categories')}
            isMulti
            value={selectedCategories}
            onChange={(categories) => setSelectedCategories(categories)}
            multi={true}
          />

          <Select
            name="keywordInput"
            options={keywordsOptions}
            placeholder={t('vendorCandidate.keywords')}
            isMulti
            value={selectedKeywords}
            onChange={(keywords) => setSelectedKeywords(keywords)}
            multi={true}
          />

          <Select
            name="genderInput"
            options={gendersOptions}
            placeholder={t('vendorCandidate.genders')}
            isMulti
            value={selectedGenders}
            onChange={(genders) => setSelectedGenders(genders)}
            multi={true}
          />

          <Select
            name="labelInput"
            options={labelsOptions}
            placeholder={t('vendorCandidate.labels')}
            isMulti
            value={selectedLabels}
            onChange={(labels) => setSelectedLabels(labels)}
            multi={true}
          />

          <FormControl component="fieldset">
            <FormLabel component="legend">{t('vendorAccount.deliver.title')}</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={<Switch checked={state.onlineShop} onChange={handleChange} name="onlineShop" />}
                label={t('vendorAccount.deliver.onlineShop')}
              />
              <FormControlLabel
                control={<Switch checked={state.clickAndCollect} onChange={handleChange} name="clickAndCollect" />}
                label={t('vendorAccount.deliver.clickAndCollect')}
              />
              <FormControlLabel
                control={<Switch checked={state.activated} onChange={handleChange} name="activated" />}
                label={t('adminDashboard.activated')}
              />
              <FormControlLabel
                control={<Switch checked={state.published} onChange={handleChange} name="published" />}
                label={t('adminDashboard.published')}
              />
            </FormGroup>
          </FormControl>
        </div>
      }

      <Button
        variant="contained"
        color="primary"
        disabled={_.isEmpty(selectedVendor)}
        onClick={handleSave}>Save</Button>

      {!_.isEmpty(selectedVendor) &&
        <div>
          <DialogUpdateProfilePicture {...dialogProfilePictureProps} />
          <DialogUpdateLogo {...dialogLogoProps} />
          <Grid container direction="row" >
            <Grid item sm={4} xs={12} >
              <VendorCard vendor={selectedCompleteVendor} editMode={true} edit={() => setOpenEditProfilePicture(true)} />
            </Grid>
            <Grid item sm={4} xs={12} >
              <Card>
                <CardContent style={{ textAlign: "center" }}>
                  <Avatar style={{ backgroundColor: "lightgrey" }} className={classes.avatar} src={imgSrc}>{avatarText}</Avatar>
                  <br />
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => setOpenEditLogo(true)}
                    endIcon={<PhotoCamera />}
                  >
                    {t('vendorAccount.logo.title')}
                  </Button>
                </CardContent>
              </Card>
            </Grid>

          </Grid>
        </div>
      }

    </div>
  );
}