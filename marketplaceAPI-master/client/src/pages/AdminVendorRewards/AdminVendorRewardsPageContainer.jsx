import React, { useState, useEffect } from 'react';
import { rewardsAdminVendorGql, createRewardAdminGql, disableRewardGql, enableRewardGql } from '../../graphQL/reward';
import { useSelector } from "react-redux";
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';
import routerHistory from '../../shared/router-history/router-history';

import AdminVendorRewardsPage from './AdminVendorRewardsPage'

import { vendorsLimitedAdminGql } from '../../graphQL/vendor'

export default function AdminVendorRewardsPageContainer(props) {
    const [rewards, setRewards] = useState(null);
    const { t } = useTranslation();
    const [vendors, setVendors] = useState([]);

    useEffect(() => {
        let didCancel = false

        async function retrieveVendors() {
            let vendors = await vendorsLimitedAdminGql();
            !didCancel && setVendors(vendors)
        }

        Promise.all([retrieveVendors()])

        return () => { didCancel = true }
    }, [])

    const retrieveRewards = async (vendorId) => {
        let rewards = await rewardsAdminVendorGql({ vendorId });
        return rewards;
    }

    const createReward = async (input, vendorId) => {
        try {
            let res = await createRewardAdminGql({ ...input, vendorId });
            if (!res) {
                toast.error(t('reward.toast.creationFail'), {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        } catch (e) {
            toast.error(t('reward.toast.creationFail'), {
                position: toast.POSITION.TOP_RIGHT
            });

        }
    }


    const disableReward = async (rewardId) => {
        try {
            let res = await disableRewardGql({ rewardId: rewardId });
            if (!res) {
                toast.error(t('reward.toast.disableFail'), {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        } catch (e) {
            toast.error(t('reward.toast.disableFail'), {
                position: toast.POSITION.TOP_RIGHT
            });

        }
    }


    const enableReward = async (rewardId) => {
        try {
            let res = await enableRewardGql({ rewardId: rewardId });
            if (!res) {
                toast.error(t('reward.toast.enableFail'), {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        } catch (e) {
            toast.error(t('reward.toast.enableFail'), {
                position: toast.POSITION.TOP_RIGHT
            });

        }
    }

    const goBack = () => {
        routerHistory.push('/admin/');
    }

    let propsChild = { goBack, vendors, rewards, createReward, disableReward, enableReward, retrieveRewards }
    return (
        <AdminVendorRewardsPage {...propsChild} />
    )
}