import * as React from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';
import { Trans } from 'react-i18next';

import ReactGA from 'react-ga';

ReactGA.pageview('/cgu');

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      margin: "auto", 
      padding: "20px"
    },
    head: {
      textAlign: 'center'
    }
  })
);

export default function RGPDPage() {
  let classes = useStyles();

  return (
    <div className={classes.root}>
      <Trans i18nKey="rgpd"
            components={[
                <h2></h2>,
                <p>
                    <b></b>
                    <a href="https://www.super-responsable.org"></a>
                    <strong></strong>
                    <br />
                </p>,
                <div>
                    <p>
                        <a href="https://fr.orson.io/1371/generateur-mentions-legales" title="générateur gratuit offert par Orson.io"></a>
                    </p>
                    <a href="https://fr.orson.io/1371/generateur-mentions-legales" title="générateur gratuit offert par Orson.io"></a>
                    <h3></h3>
                    <br/>
                </div>,
                <h3></h3>,
                <ul>
                    <li>
                        <a href="https://www.super-responsable.org"></a>
                    </li>
                </ul>,
                <br/>]}/>
    </div>

  )
}