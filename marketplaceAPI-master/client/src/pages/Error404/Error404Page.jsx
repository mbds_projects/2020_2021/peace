import * as React from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import { useTranslation } from 'react-i18next';

import ReactGA from 'react-ga';

ReactGA.pageview('/error404');

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      textAlign: "center"
    }
  }),
);


export default function Error404Page() {
    
  const classes = useStyles();
  const { t } = useTranslation();
    return (
      <div className={classes.root}>
          <h1>{t('404.title')}</h1>
          <div>
            <p>{t('404.subTitle')}</p>
          </div>
        </div>
    );
}
