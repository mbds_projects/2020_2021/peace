import React, { useState, useEffect } from 'react';

import HomePage from './HomePage';

import { categoriesGql } from '../../graphQL/category'

import { articlesGql } from '../../graphQL/article';
import { setArticles as setArticlesInternal} from '../../shared/store/actions/article.actions';

import { youtubeRecentGql } from '../../graphQL/youtube';
import { setYoutubeRecent as setYoutubeRecentInternal} from '../../shared/store/actions/youtube.actions';

import { imagesCarouselUrlGql } from '../../graphQL/imageCarouselUrl';
import { setImagesCarousel as setImageCarouselInternal} from '../../shared/store/actions/imageCarousel.actions';

import {aroundPositionVendorsGql, latestVendorsGql, moreFavoritesVendorsGql, bestRateVendorsGql, vendorsAssimilateGql } from '../../graphQL/vendor';
import { setLatestVendors as setLatestVendorsInternal, 
  setAroundPositionVendors as setAroundPositionVendorsInternal  } from '../../shared/store/actions/vendor.actions';
import { useDispatch, useSelector } from "react-redux";

import { gendersGql } from '../../graphQL/gender';
import { setGenders as setGendersInternal} from '../../shared/store/actions/gender.actions';
import { keywordsPopularGql } from '../../graphQL/keyword';
import { bestCommentsGql } from '../../graphQL/comment';

//import { instagramPopularGql } from '../../graphQL/instagram';
//import { setInstagramRecent as setInstagramInternal} from 'src/shared/store/actions/instagram.actions';

//import useMediaQuery from '@material-ui/core/useMediaQuery';
import routerHistory from '../../shared/router-history/router-history';

export default function HomePageContainer() {
  const dispatch = useDispatch();

  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  const latestVendorsInternal = useSelector((state) => state.vendor.latestVendors);
  const aroundPositionVendorsInternal = useSelector((state) => state.vendor.aroundPositionVendors);
  const categoriesInternal = useSelector((state) => state.category.categories);
  const articlesInternal = useSelector((state) => state.article.articles);
  const gendersInternal = useSelector((state) => state.gender.genders);
  //const instagramInternal = useSelector((state:any) => state.instagram.instagramRecent);
  const imagesCarouselInternal = useSelector((state) => state.imageCarousel.imagesCarousel);
  const youtubeRecentInternal = useSelector((state) => state.youtube.youtubeRecent);

  const [categories, setCategories] = useState(null);
  const [imagesCarousel, setImagesCarousel] = useState(null);
  const [aroundPositionVendors, setAroundPositionVendors] = useState(null);  
  const [latestVendors, setLatestVendors] = useState(null);
  const [genders, setGenders] = useState(null);
  const [moreFavoritesVendors, setMoreFavoritesVendors] = useState(null);  
  const [bestRateVendors, setBestRateVendors] = useState(null);  
  const [keywordsPopular, setKeywordsPopular] = useState(null);
  const [vendorsAssimilate, setVendorsAssimilate] = useState(null);
  const [bestComments, setBestComments] = useState(null);
  //const [instagramRecent, setInstagramRecent] = useState<any[] | null>(null);
  const [articles, setArticles] = useState(null);
  const [youtubeRecent, setYoutubeRecent] = useState(null);

  const geoloc = useSelector((state) => state.user.position);
  const geolocAccepted = useSelector((state) => state.user.geolocAccepted)

  //const matches = useMediaQuery('(max-width:768px)');

  const handleClickVendors = () => {
    routerHistory.push(`/vendors`);
  }

  const handleClickCat = (type, id) => {
    routerHistory.push(`/vendors?${type}=${id}`);
  }

  useEffect(() => {
    let didCancel = false
    async function retrieveCategories() {
      let categories;
      if(categoriesInternal.length > 0) {
        categories = categoriesInternal;
      } else {
        categories = await categoriesGql();
      }
      !didCancel && setCategories(categories)
    }

    async function retrieveArticles() {
    let articles;
      if(articlesInternal.length > 0) {
        articles = articlesInternal;
      } else {
        articles = await articlesGql();
        !didCancel && dispatch(setArticlesInternal(articles))
      }
      !didCancel && setArticles(articles)
    }
  

    async function retrieveLatestVendors() {
      let latestVendors;
      if(latestVendorsInternal.length > 0) {
        latestVendors = latestVendorsInternal;
      } else {
        latestVendors = await latestVendorsGql();
        !didCancel && dispatch(setLatestVendorsInternal(latestVendors))
      }
      !didCancel && setLatestVendors(latestVendors);
    }

    /*async function retrieveInstagramRecent() {
      let instagramRecent;
      if(instagramInternal.length > 0) {
        instagramRecent = instagramInternal;
      } else {
        instagramRecent = await instagramPopularGql();
        !didCancel && dispatch(setInstagramInternal(instagramRecent))
      }
      !didCancel && setInstagramRecent(instagramRecent);
    }*/

    async function retrieveYoutubeRecent() {
      let youtubeRecent;
      if(youtubeRecentInternal.length > 0) {
        youtubeRecent = youtubeRecentInternal;
      } else {
        try {
          youtubeRecent = await youtubeRecentGql();
          !didCancel && dispatch(setYoutubeRecentInternal(youtubeRecent))
        } catch(e) {
          youtubeRecent = [];
        }
      }
      !didCancel && setYoutubeRecent(youtubeRecent);
    }

    async function retrieveMoreFavoritesVendors() {  
      let moreFavoritesVendors = await moreFavoritesVendorsGql();
      !didCancel && setMoreFavoritesVendors(moreFavoritesVendors);
    }

    async function retrieveImagesCarousel() {
      let imagesCarousel;
      if(imagesCarouselInternal.length > 0) {
        imagesCarousel = imagesCarouselInternal;
      } else {
        imagesCarousel = await imagesCarouselUrlGql();
        !didCancel && dispatch(setImageCarouselInternal(imagesCarousel))
      }
      //let filtersByMatches = imagesCarousel.filter((i) => i.mobile === matches);
      !didCancel && setImagesCarousel(imagesCarousel)
    }
  
    async function retrieveGenders() { 
      let genders; 
      if(gendersInternal.length > 0) {
        genders = gendersInternal;
      } else {
        genders = await gendersGql();
        !didCancel && dispatch(setGendersInternal(genders));
      }
      !didCancel && setGenders(genders);
    }

    async function retrieveBestRateVendors() {  
      let bestRateVendors = await bestRateVendorsGql();
      !didCancel && setBestRateVendors(bestRateVendors);
    }

    async function retrieveKeywordsPopular() {
      let keywordsPopular = await keywordsPopularGql();
      !didCancel && setKeywordsPopular(keywordsPopular);
    }

    async function retrieveBestComment() {
      let bestComments = await bestCommentsGql();
      !didCancel && setBestComments(bestComments);
    }
  
    Promise.all([retrieveImagesCarousel(), retrieveCategories(), retrieveArticles(), retrieveLatestVendors(), 
      retrieveMoreFavoritesVendors(), retrieveGenders(), retrieveBestRateVendors(), retrieveYoutubeRecent(),
      retrieveKeywordsPopular(), retrieveBestComment()/*, retrieveInstagramRecent()*/])
    return () => { didCancel = true }
  }, [])

  useEffect(() => {
    let didCancel = false

    async function updateLoggedInUserAssimilateVendors() {
      let keywords = loggedInUser.keywords.map((el) => el.id)
      let vendorsAssimilate = await vendorsAssimilateGql({categories: [], keywords, vendorId: null, limit: 3});
      !didCancel && setVendorsAssimilate(vendorsAssimilate)
    }

    if(loggedInUser && loggedInUser.keywords && loggedInUser.keywords.length > 0) {
      updateLoggedInUserAssimilateVendors();
    }

    return () => { didCancel = true }
  }, [loggedInUser])

  
  useEffect(() => {
    let didCancel = false

    async function updateAroundPositionVendors() {
      let aroundPositionVendors;
      if(aroundPositionVendorsInternal.length > 0) {
        aroundPositionVendors = aroundPositionVendorsInternal
      } else {
        aroundPositionVendors = await aroundPositionVendorsGql({position: geoloc, limit: 3})
        !didCancel && dispatch(setAroundPositionVendorsInternal(aroundPositionVendors))
      }
      !didCancel && setAroundPositionVendors(aroundPositionVendors)
    }

    if(geolocAccepted) {
      updateAroundPositionVendors();
    } else {
      !didCancel && setAroundPositionVendors([])
    }

    return () => { didCancel = true }
  }, [geolocAccepted])


  return (
    <HomePage
    categories={categories}
    articles={articles}
    aroundPositionVendors={aroundPositionVendors}
    latestVendors={latestVendors}
    genders={genders}
    moreFavoritesVendors={moreFavoritesVendors}
    bestRateVendors={bestRateVendors}
    keywordsPopular={keywordsPopular}
    vendorsAssimilate={vendorsAssimilate}
    bestComments={bestComments}
    instagramRecent={/*instagramRecent*/ null}
    loggedInUser={loggedInUser}
    geolocAccepted={geolocAccepted}
    imagesCarousel={imagesCarousel}
    handleClickVendors={handleClickVendors}
    youtubeRecent={youtubeRecent}
    handleClickCat={handleClickCat}
    />);
}
