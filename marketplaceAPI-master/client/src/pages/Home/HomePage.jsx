import * as React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
//import './styles.css';
import Carousel from "../../components/Carousel/Carousel";
import Grid from '@material-ui/core/Grid';

import { NavLink } from 'react-router-dom';
import FilterCard from '../../components/FilterCard/FilterCard';

import MediaCard from '../../components/MediaCard/MediaCard';

import VendorCard from '../../components/VendorCard/VendorCard';

import CommentViewCard from '../../components/CommentViewCard/CommentViewCard';

import IconButton from '@material-ui/core/IconButton';
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';

import Avatar from '@material-ui/core/Avatar';
//import InstagramCard from '../../components/InstagramCard/InstagramCard';

import LoadingPage from '../Loading/LoadingPage';

import useMediaQuery from '@material-ui/core/useMediaQuery';

import ReactGA from 'react-ga';

ReactGA.pageview('/home');

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      marginBottom: "20px"
    },
    containerTitle: {
      textAlign: "center",
      fontWeight: 600,
      fontSize: "larger",
      margin: "40px 0px 0px"
    },
    containerTitleMobile: {
      textAlign: "center",
      fontWeight: 600,
      fontSize: "larger",
      margin: "20px 0px 15px"
    },
    charter: {
      backgroundColor: theme.colors.almond,
      color: "white",
      fontSize: "20px",
      lineHeight: 1.6,
      padding: "20px",
      textAlign: "center",
      "& a": {
        color: "white",
        //fontSize: "24px",
        fontWeight: "bold"
      }
    },
    fbGroup: {
      backgroundImage: `url("/assets/images/groupCover.jpg")`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      backgroundSize: "cover",
      color: "white",
      fontSize: "20px",
      lineHeight: 1.6,
      padding: "20px",
      textAlign: "center",
      "& a": {
        color: "white",
        //fontSize: "24px",
        fontWeight: "bold"
      }
    },
    chip: {
      margin: '5px',
      padding: '10px',
      fontSize: "20px"
    },
    chipCat: {
      textAlign: "center",
      marginTop: '40px'

    },
    insta: {
      margin: "0px"
    },
    vendors: {
      marginBottom: "40px",
      marginTop: "20px"
    },
    marginHomeCarousel: {
      marginBottom: "20px"
    },
    imgContent: {
      /*maxHeight: "400px",
      overflow: "hidden",
      width: "100%"*/
    },
    doubleArrow: {
      textAlign: "center"
    },
    doubleArrowIcon: {
      color: theme.colors.strawberry
    },
    flagSmall: {
      width: '40px',
      height: '40px'
    }
  }),
);

export default function HomePage(props) {
  const classes = useStyles();
  const { t, i18n } = useTranslation();
  const matches = useMediaQuery('(max-width:768px)');

  const GADetectClick = (category, action) => {
    ReactGA.event({
      category: category,
      action: action,
      label: 'Home'
    });
  }

  const returnCard = (element, id, GAtitle, GAsubTitle, xs, sm) => {
    return (
      <Grid item xs={xs} sm={sm} key={id} onClick={() => GADetectClick(GAtitle, GAsubTitle)}>
        {element}
      </Grid>
    )
  }

  let imagesCarouselMobile = props.imagesCarousel ? props.imagesCarousel.filter((i) => /*i.mobile === true*/ i.mode === "mobile" && i.lang === i18n.language) : null

  let imagesCarouselDesktop = props.imagesCarousel ? props.imagesCarousel.filter((i) => /*i.mobile === false*/ i.mode === "desktop" && i.lang === i18n.language) : null

  let carouselPicturesMobile = imagesCarouselMobile ? imagesCarouselMobile.map((img, index) => {
    if(!!img.href) {
      return (<div className={classes.imgContent} key={index}>
        <a href={img.href} target="_blank" rel="noopener noreferrer">
          <img alt={`Carousel ${img.mode} ${img.lang} ${img.pos}`} src={img.url}/>
        </a>
      </div>)
    } else if(!!img.to) {
      return (<div className={classes.imgContent} key={index}>
        <NavLink to={img.to}>
          <img alt={`Carousel ${img.mode} ${img.lang} ${img.pos}`} src={img.url}/>
        </NavLink>
      </div>)
    } else {
      return (
      <div className={classes.imgContent} key={index}>
        <img alt={`Carousel ${img.mode} ${img.lang} ${img.pos}`} src={img.url}/>
      </div>)}
    })
    : [];

  let carouselPicturesDesktop = imagesCarouselDesktop ? imagesCarouselDesktop.map((img, index) => {
    if(!!img.href) {
      return (<div className={classes.imgContent} key={index}>
        <a href={img.href} target="_blank" rel="noopener noreferrer">
          <img alt="" src={img.url}/>
        </a>
      </div>)
    } else if(!!img.to) {
      return (<div className={classes.imgContent} key={index}>
        <NavLink to={img.to}>
          <img alt="" src={img.url}/>
        </NavLink>
      </div>)
    } else {
      return (
      <div className={classes.imgContent} key={index}>
        <img alt="" src={img.url}/>
      </div>)}
    })
    : [];
  

  let moreFavoritesVendors = props.moreFavoritesVendors && props.moreFavoritesVendors.map((vendor) => returnCard(<VendorCard vendor={vendor} />, vendor.id, 'Vendors',  `Click on vendor ${vendor.name} from more favorites`, 12, 4));
  let latestVendors = props.latestVendors && props.latestVendors.map((vendor) => returnCard(<VendorCard vendor={vendor} />, vendor.id, 'Vendors',  `Click on vendor ${vendor.name} from latest vendors`, 12, 4));
  let articles = props.articles && props.articles.map((article, index) => returnCard(<MediaCard element={article} />, `article_${index}`, 'Articles',  `Click on article ${article.link}`, 12, 4))
  let youtubeRecent = props.youtubeRecent && props.youtubeRecent.map((video, index) => returnCard(<MediaCard element={video} />, `youtube_${index}`, 'Youtube',  `Click on youtube video ${video.link}`, 12, 4))
  
  let aroundPositionVendors = props.aroundPositionVendors && props.aroundPositionVendors.map((vendor) => returnCard(<VendorCard vendor={vendor} />, vendor.id, 'Vendors',  `Click on vendor ${vendor.name} from around position`, 12, 4))
  let bestRateVendors = props.bestRateVendors && props.bestRateVendors.map((vendor) => returnCard(<VendorCard vendor={vendor} />, vendor.id, 'Vendors',  `Click on vendor ${vendor.name} from best rate`, 12, 4)) 
  let vendorsAssimilate = props.vendorsAssimilate && props.vendorsAssimilate.map((vendor) => returnCard(<VendorCard vendor={vendor} />, vendor.id, 'Vendors',  `Click on vendor ${vendor.name} from user suggestion`, 12, 4))
  let bestComments = props.bestComments && props.bestComments.map((comment) => returnCard(<CommentViewCard comment={comment} />, comment.id, 'Vendors',  `Click on vendor ${comment.vendor.name} from best comments`, 12, 4))
                    
  return (
    <div className={classes.root}>
      <Grid container direction="row" justify="flex-end">
        <Grid item xs={12}  className={classes.marginHomeCarousel} >
        {props.imagesCarousel ? 
          matches ? 
          <Carousel carouselComponent={carouselPicturesMobile} arrowsColor="grey" arrows={true} />
          : <Carousel carouselComponent={carouselPicturesDesktop} arrowsColor="grey" arrows={true}  width="1000px"/>
          : <LoadingPage/>
        }
        </Grid>
      </Grid>
      

      <Grid container>
        <Grid item xs={12}>
          <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
            {t('home.categories')}
          </div>
        </Grid>
        {
          props.categories ?
            matches ? 
            <Grid container justify="space-evenly" item spacing={3}>
            {
              props.categories.map((cat) => {
              return (<IconButton
                      key={cat.id}
                      onClick={() => props.handleClickCat('cat', cat.id)}
                    >
                      <Avatar 
                        className={classes.flagSmall} 
                        src={`/assets/images/icons/${cat.name}.png`} />
                  </IconButton>)
              })
            }
            
            </Grid>  
            : props.categories.map((cat) => returnCard(<FilterCard filter={cat} type="cat" height="150px"/>, cat.id, 'Categories',  `Click on category ${cat.name}`, 6, 3))
            : <LoadingPage />
        }
      </Grid> 
      <Grid container className={!matches ? classes.vendors : ''}>
        <Grid item xs={12}>
          <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
            {t('home.moreFavoritesVendors')}
          </div>
        </Grid>
        {
          props.moreFavoritesVendors && moreFavoritesVendors ?
            matches ? <Carousel carouselComponent={moreFavoritesVendors}  arrows={true} /> : moreFavoritesVendors
            : <LoadingPage />
        }
      </Grid>
      <Grid container className={!matches ? classes.vendors : ''}>
        <Grid item xs={12}>
          <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
            {t('home.latestVendorsAdded')}
          </div>
        </Grid>
        {
          props.latestVendors && latestVendors ?
            matches ? <Carousel carouselComponent={latestVendors} arrows={true} /> : latestVendors
            : <LoadingPage />
        }
      </Grid> 
      <Grid container>
        <Grid item xs={12}>
          <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
            {t('home.articles')}
          </div>
        </Grid>
        {
          props.articles && articles ?
            matches ? <Carousel carouselComponent={articles} arrows={true} /> : articles
            : <LoadingPage />
        }
      </Grid> 
      <Grid container>
        <Grid item xs={12}>
          {
            (!(props.youtubeRecent && youtubeRecent) || youtubeRecent.length > 0) &&
              <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
                {t('home.youtube')}
              </div>
          }
        </Grid>
        {
          props.youtubeRecent && youtubeRecent ?
            matches ? <Carousel carouselComponent={youtubeRecent} arrows={true} /> : youtubeRecent
            : <LoadingPage />
        }
      </Grid> 
      {
        props.geolocAccepted &&
          <Grid container className={!matches ? classes.vendors : ''}>
            <Grid item xs={12}>
              <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
                {t('home.aroundPositionVendors')}
              </div>
            </Grid>
            {
              props.aroundPositionVendors && aroundPositionVendors ?
                matches ? <Carousel carouselComponent={aroundPositionVendors} arrows={true} /> : aroundPositionVendors
                : <LoadingPage />
            }
          </Grid> 
      }
      <Grid container>
        <Grid item xs={12}>
          <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
            {t('home.charter.title')}
          </div>
        </Grid>
        <Grid item xs={12}>
          <div className={classes.charter}>
            {t('home.charter.text')}
            <a href="https://blog.super-responsable.org/wp-content/uploads/2019/12/Charte-responsable.pdf" target="_black" onClick={() => GADetectClick('Charter', `Click on charter`)}>{t('home.charter.link')}</a>
          </div>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={12}>
          <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
            {t('home.genders')}
          </div>
        </Grid>
        {
          props.genders ?
            matches ? 
            <Carousel carouselComponent={props.genders.map((gender) => returnCard(<FilterCard filter={gender} type="gd" round={true} />, gender.id, 'Genders',  `Click on gender ${gender.name}`, 4, 4))} arrows={true} infinite={true} slides={3}/>
            :
              props.genders.map((gender) => returnCard(<FilterCard filter={gender} type="gd" round={true}/>, gender.id, 'Genders',  `Click on gender ${gender.name}`, 6, 3))
            : <LoadingPage />
        }
      </Grid> 
      <Grid container className={(!props.bestRateVendors || props.bestRateVendors.length > 0) && !matches ? classes.vendors : ''}>
        <Grid item xs={12}>
          {
            (!props.bestRateVendors || props.bestRateVendors.length > 0) && 
            <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
              {t('home.bestRateVendors')}
            </div>
          }
        </Grid>
        {
          props.bestRateVendors && bestRateVendors ?
            matches ? <Carousel carouselComponent={bestRateVendors} arrows={true} /> : bestRateVendors
            : <LoadingPage />
        }
      </Grid>
      <Grid container>
        <Grid item xs={12}>
          <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
            {t('home.keywords')}
          </div>
        </Grid>
        {
          props.keywordsPopular ?
            props.keywordsPopular.map((keyword) => returnCard(<FilterCard filter={keyword} type="kw" />, keyword.id, 'Keywords',  `Click on keyword ${keyword.name}`, 6, 4))
            : <LoadingPage />
        }
        <Grid item xs={12} className={classes.doubleArrow}>
          <IconButton onClick={props.handleClickVendors} className={classes.doubleArrowIcon}>
            <DoubleArrowIcon />
          </IconButton>
        </Grid>
      </Grid>
      {
        props.loggedInUser &&
        <Grid container className={!matches ? classes.vendors : ''}>
          <Grid item xs={12}>
            <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
              {t('home.vendorsAssimilate')}
            </div>
          </Grid>
          {
            props.vendorsAssimilate && vendorsAssimilate ?
              matches ? <Carousel carouselComponent={vendorsAssimilate} arrows={true} /> : vendorsAssimilate
              : <LoadingPage />
          }
      </Grid>
      }

      <Grid container>
        <Grid item xs={12}>
          {
            (!props.bestComments || props.bestComments.length > 0) &&
            <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
              {t('home.bestComments')}
            </div>
          }
        </Grid>
        {
          props.bestComments && bestComments ?
            matches ? <Carousel carouselComponent={bestComments} arrows={true} /> : bestComments
            : <LoadingPage />
        }
      </Grid>

      <Grid container>
        <Grid item xs={12}>
          <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
            {t('home.fbGroup.title')}
          </div>
        </Grid>
        <Grid item xs={12}>
          <div className={classes.fbGroup}>
            {t('home.fbGroup.text')}
            <a href="https://www.facebook.com/groups/256271651950647/" target="_black" onClick={() => GADetectClick('Facebook Group', `Click on group`)}>{t('home.fbGroup.link')}</a>
            {t('home.fbGroup.text2')}
          </div>
        </Grid>
      </Grid>
      {/*<Grid container>
        <Grid item xs={12}>
          <div className={matches ? classes.containerTitleMobile : classes.containerTitle}>
            <a href="https://www.instagram.com/explore/tags/monshopresponsable/" target="_blank" rel="noopener noreferrer" rel="noopener noreferrer" onClick={() => {GADetectClick('Instagram', 'Click on #monshopresponsable')}}>#monShopResponsable</a> - {t('home.instaRecent')}
          </div>
        </Grid>
        {
          props.instagramRecent ?
            props.instagramRecent.map((instagram, index) => returnCard(<InstagramCard instagram={instagram} size={matches ? "small" : "large"}/>, `instagram_${index}`, 'Instagram',  `Click on instagram picture ${instagram.link}`, 4, 2))
            : <LoadingPage />
        }
      </Grid>
      */}
    </div>
  );
}