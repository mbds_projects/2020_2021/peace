import React, { useState, useEffect } from 'react';

import routerHistory from '../../shared/router-history/router-history';

import VendorProfilePage from './VendorProfilePage';
import LoadingPage from '../Loading/LoadingPage';

import { toggleFavorite, setComments } from '../../core/modules/user.module'

import { vendorsAssimilateGql, vendorsLimitedGql, profileVendorLimitedGql } from '../../graphQL/vendor';
import { reportCommentGql } from '../../graphQL/comment';

import { locationsOfVendorByCityGql, locationsOfVendorByCountyGql } from '../../graphQL/location';

import { commentsByVendorGql, createCommentGql } from '../../graphQL/comment'

import { toast } from 'react-toastify';

import { useDispatch, useSelector } from "react-redux";

import { useParams} from 'react-router-dom';

import * as _ from "lodash";

import { setUser } from '../../shared/store/actions/authentication.actions';
import { setProfileVendors, setProfileVendorsAssimilate, setVendors } from '../../shared/store/actions/vendor.actions';

import { useTranslation } from 'react-i18next';
import { toastError } from '../../shared/utils/error';

import { userGql } from '../../graphQL/user'

import ReactGA from 'react-ga';

export default function VendorProfilePageContainer() {
  const dispatch = useDispatch();
  let {id} = useParams();

  const { t } = useTranslation();
  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  const profileVendorsInternal = useSelector((state) => state.vendor.profileVendors);
  const profileVendorsAssimilateInternal = useSelector((state) => state.vendor.profileVendorsAssimilate);
  const vendorsInternal = useSelector((state) => state.vendor.vendors);
  const categoriesInternal = useSelector((state) => state.category.categories);
  const keywordsInternal = useSelector((state) => state.keyword.keywords);

  const geoloc = useSelector((state) => state.user.position);
  const geolocAccepted = useSelector((state) => state.user.geolocAccepted)

  const [profileVendor, setProfileVendor] = useState(null);
  const [cityToDisplay, setCityToDisplay] = useState(0);
  const [descCollapsed, setDescCollapsed] = useState(true);  
  const [commentsActivePage, setCommentsActivePage] = useState(0);
  const [currentComments, setCurrentComments] = useState([]);
  const [vendorsAssimilate, setVendorsAssimilate] = useState([]);
  const [alsoAvailableOn, setAlsoAvailableOn] = useState([]);
  const [isAllowToComment, setIsAllowToComment] = useState(false);  
  const [loadingState, setLoadingState] = useState(true);  
  const [commentLoadingState, setCommentLoadingState] = useState(true);  
  const [vendorsAssimilateLoadingState, setVendorsAssimilateLoadingState] = useState(true);  
  const [alsoAvailableOnLoadingState, setAlsoAvailableOnLoadingState] = useState(true);  
  const [suggestions, setSuggestions] = useState([]);
  const [citiesSuggestions, setCitiesSuggestions] = useState([]);
  const [countiesSuggestions, setCountiesSuggestions] = useState([]);
  const [countiesAndCitiesSuggestions, setCountiesAndCitiesSuggestions] = useState([]);
  const [citiesLoadingState, setCitiesLoadingState] = useState(true);

  const [categories, setCategories] = useState([]);
  const [keywords, setKeywords] = useState([]);

  useEffect(() => {
    let didCancel = false
    if(profileVendor) {
      let cats = categoriesInternal.filter((cat) => profileVendor.categories.includes(cat.id));
      !didCancel && setCategories(cats);
    }
    return () => { didCancel = true }
  }, [categoriesInternal, profileVendor])

  useEffect(() => {

    let didCancel = false
    if(profileVendor) {
      let kws = keywordsInternal.filter((kw) => profileVendor.keywords.includes(kw.id));
      !didCancel && setKeywords(kws);
    }
    return () => { didCancel = true }
  }, [keywordsInternal, profileVendor])

  const handleIncorrectIdParameter = () =>{
    routerHistory.replace('/404');
  }


  async function retrieveVendor(id, didCancel) {
    !didCancel && setLoadingState(true);
    let profileVendor;
    if(profileVendorsInternal.length > 0) {
      let curVendor = profileVendorsInternal.find((v) => v.id === id);
      if(curVendor) {
        profileVendor = curVendor;
      } else {
        profileVendor = await profileVendorLimitedGql({id: id})
        let profileVendors = profileVendorsInternal;
        profileVendors.push(profileVendor)
        !didCancel && dispatch(setProfileVendors(profileVendors))
      }
    } else {
      profileVendor = await profileVendorLimitedGql({id: id})
      !didCancel && dispatch(setProfileVendors([profileVendor]))
    }
    !didCancel && setProfileVendor(profileVendor);
    ReactGA.pageview(`/vendor/${profileVendor.name}`);
    !didCancel && setLoadingState(false);
  }

  async function allowComment(id, didCancel) {
    let isAllowToCommentTmp = await isLoggedAndNotAlreadyComment(id);
    if(isAllowToCommentTmp !== isAllowToComment) {
      !didCancel && setIsAllowToComment(isAllowToCommentTmp);
    }
  }

  useEffect(() => {
    let didCancel = false

    if (!id) {
      handleIncorrectIdParameter();
      return;
    } else {
      Promise.all([retrieveVendor(id, didCancel), allowComment(id, didCancel)]);
    }

    return () => { didCancel = true }
  }, []);


  useEffect(() => {
    let didCancel = false

    if (!id) {
      handleIncorrectIdParameter();
      return;
    } else {
      Promise.all([retrieveVendor(id, didCancel), allowComment(id, didCancel)]);
    }

    return () => { didCancel = true }
  }, [id]);

  useEffect(() => {
    let didCancel = false

    async function allowComment(id) {
      let isAllowToCommentTmp = await isLoggedAndNotAlreadyComment(id);
      if(isAllowToCommentTmp !== isAllowToComment) {
        !didCancel && setIsAllowToComment(isAllowToCommentTmp);
      }
    }

    if (!id) {
      handleIncorrectIdParameter();
      return;
    } else {
      allowComment(id);
    }

    return () => { didCancel = true }
  }, [loggedInUser]);

  useEffect(() => {
    let didCancel = false

    async function allowComment(vendor) {
      let isAllowToCommentTmp = await isLoggedAndNotAlreadyComment(vendor.id);
      if(isAllowToCommentTmp !== isAllowToComment) {
        !didCancel && setIsAllowToComment(isAllowToCommentTmp);
      }
    }

    async function retrieveComment(vendor) {
        !didCancel && setCommentLoadingState(true);
        const currentComments = await commentsByVendorGql({vendorId: vendor.id, page: 0})
        !didCancel && setCurrentComments(currentComments);
        !didCancel && setCommentLoadingState(false);
    }

    async function retrieveVendorsAssimilate(vendor) {
      !didCancel && setVendorsAssimilateLoadingState(true);
      let catId = vendor.categories.map((el) => el.id);
      let kwId = vendor.keywords.map((el) => el.id);
      let vendorsAssimilate;
      if(profileVendorsAssimilateInternal.length > 0) {
        let profileVendorsAssimilate = profileVendorsAssimilateInternal.find((v) => v.id === id);
        if(profileVendorsAssimilate) {
          vendorsAssimilate = profileVendorsAssimilate.vendorsAssimilate;
        } else {
          vendorsAssimilate = await vendorsAssimilateGql({categories: catId, keywords: kwId, vendorId: vendor.id, limit: 3})
          let profileVendorsAssimilateTmp = profileVendorsAssimilateInternal;
          profileVendorsAssimilateTmp.push({id: vendor.id, vendorsAssimilate: vendorsAssimilate})
          !didCancel && dispatch(setProfileVendorsAssimilate(profileVendorsAssimilateTmp))
        }
      } else {
        vendorsAssimilate = await vendorsAssimilateGql({categories: catId, keywords: kwId, vendorId: vendor.id, limit: 3})
        !didCancel && dispatch(setProfileVendorsAssimilate([{id: vendor.id, vendorsAssimilate: vendorsAssimilate}]))
      }
      !didCancel && setVendorsAssimilate(vendorsAssimilate);
      !didCancel && setVendorsAssimilateLoadingState(false);
    }

    async function retrieveAlsoAvailable(vendor) {
      !didCancel && setAlsoAvailableOnLoadingState(true);
      if(vendor.alsoAvailableOn.length > 0) {
        let alsoAvailableOn;
        if(vendorsInternal.length > 0) {
          alsoAvailableOn = vendorsInternal.filter((v) => vendor.alsoAvailableOn.includes(v.id));
        } else {
          let vendors = await vendorsLimitedGql();
          alsoAvailableOn = vendors.filter((v) => vendor.alsoAvailableOn.includes(v.id));
          
          !didCancel && setVendors(vendors);
          !didCancel && dispatch(setVendors(vendors))
        }
        !didCancel && setAlsoAvailableOn(alsoAvailableOn);
        !didCancel && setAlsoAvailableOnLoadingState(false);
      } else {
        !didCancel && setAlsoAvailableOn([]);
        !didCancel && setAlsoAvailableOnLoadingState(false);
      }
    }

    async function retrieveCitiesSuggestions(vendor) {
      !didCancel && setCitiesLoadingState(true);
      if(vendor.locations.length > 0) {
        let citiesSuggestions;
        if(geolocAccepted) {
          citiesSuggestions = await locationsOfVendorByCityGql({vendorId: vendor.id, position: geoloc});
        } else {
          citiesSuggestions = await locationsOfVendorByCityGql({vendorId: vendor.id});

        }
        
        let countiesSuggestions = await locationsOfVendorByCountyGql({vendorId: vendor.id});


        citiesSuggestions = citiesSuggestions.map((loc) => {return {shops: loc.shops, loc: loc.city}})
        countiesSuggestions = countiesSuggestions.map((loc) => {return {shops: loc.shops, loc: loc.county}})

        !didCancel && setCitiesSuggestions(citiesSuggestions);

        !didCancel && setCountiesSuggestions(countiesSuggestions);

        let suggestionsCitiesCounties = _.unionBy(citiesSuggestions, countiesSuggestions, 'loc');

        !didCancel && setCountiesAndCitiesSuggestions(suggestionsCitiesCounties);

        let suggestions = suggestionsCitiesCounties.map((el, index) => {
          return {
            value: index,
            name: el.loc
          }
        });
        
        !didCancel && setSuggestions(suggestions);
      } else {
        !didCancel && setCitiesSuggestions([]);        
        !didCancel && setCountiesSuggestions([]);   
        !didCancel && setCountiesAndCitiesSuggestions([]);
        !didCancel && setSuggestions([]);
      }

      !didCancel && setCitiesLoadingState(false);
    }
    if(profileVendor) {
      Promise.all([retrieveComment(profileVendor), allowComment(profileVendor), retrieveVendorsAssimilate(profileVendor), retrieveAlsoAvailable(profileVendor), retrieveCitiesSuggestions(profileVendor)]);
    }

    return () => { didCancel = true }
  }, [profileVendor]);


  
  const handleFavoriteClick = (vendor) =>  {
    if(loggedInUser) {
      toggleFavorite(loggedInUser.favorites, vendor.id, vendor.name)
      .catch((e) => {
        toastError(e.message, t);
      });
    } else {
      toast.error(t('error.shouldBeConnected'), {
        position: toast.POSITION.TOP_RIGHT
      });
      routerHistory.push('/login');
    }
  }
  
  const handleCityClick = (cityIndex) => {
    if(cityToDisplay !== cityIndex && profileVendor) {
      setCityToDisplay(cityIndex);
      ReactGA.event({
        category: 'Cities',
        action: `Click on city ${profileVendor.locations[cityIndex]}`,
        label: `Vendor ${profileVendor.name}`
      });
    }
  }
  
  const handlePageClick = async (page) => {
    if(profileVendor) {
      const currentComments = await commentsByVendorGql({vendorId: profileVendor.id, page: page})
      setCurrentComments(currentComments);
      setCommentsActivePage(page)
    }
  }
  
  const handleToggleDesc = () => {
    setDescCollapsed(!descCollapsed);
  }
  
  
  const isLoggedAndNotAlreadyComment = async(vendorId) => {
    let isAllowed = false;
    if(loggedInUser) {
      if(!loggedInUser.comments) {
        await setComments(loggedInUser);
      }
      let comment = loggedInUser.comments.find((comment) => comment.vendor.id === vendorId); 
      if(!comment) {
        isAllowed = true;
      }
    }
    return isAllowed;
  }
  
  const redirectToLogin = () => {
    routerHistory.push('/login');
  }

  const goRewards = () => {
    routerHistory.push("/user/rewards")
  }

  
  const isFavorite = (vendor) => {
    if(loggedInUser && loggedInUser.favorites && loggedInUser.favorites.includes(vendor.id)) {
      return true;
    } else {
      return false;
    }
  }
  
  const sendComment = async (comment, rate) => { 
    if(profileVendor) {
      try{
        let commentCreated = await createCommentGql({input: {
            text: comment,
            vendorId: profileVendor.id,
            rate: rate
          }});
          let curCom = currentComments;
          curCom.unshift(commentCreated);

          let vendorTmp = { ...profileVendor };
          let previousRate = (!!profileVendor.rate ? profileVendor.rate : 0);
          let commentLength = (!!profileVendor.comments ? profileVendor.comments.length : 0);
          vendorTmp.rate = (((previousRate * commentLength) + rate) / (commentLength +1))

          let commentsTmp = !!vendorTmp.comments ? vendorTmp.comments : [];
          commentsTmp.push(commentCreated.id)
          vendorTmp.comments = commentsTmp;
          
          setProfileVendor(vendorTmp)
          setCurrentComments(curCom);
          setIsAllowToComment(false);

          /* dispatch user with new com */
          //let user = loggedInUser;
          //user.comments.unshift(commentCreated);
          //dispatch(setUser(user));
          ReactGA.event({
            category: 'Comment',
            action: 'Add a comment',
            label: `Vendor ${profileVendor.name}`
          });


          userGql().then(user => dispatch(setUser(user)));

          return commentCreated;
      }catch(e) {
          toastError(e.message, t, 'vendorProfile.toast.commentNotSend')
      }
    }
    
  }
  
  const reportComment = async (commentId) => {
    if(profileVendor) {
      reportCommentGql({commentId: commentId})
      .then(comment => {
        let currentCommentsTmp = currentComments;
        let indexOfComment = _.findIndex(currentCommentsTmp, function(o) { return o.id === commentId; });
        currentCommentsTmp[indexOfComment] = comment;
        setCurrentComments(currentCommentsTmp)
        ReactGA.event({
          category: 'Comment',
          action: 'Report a comment',
          label: `Vendor ${profileVendor.name}`
        });
        toast.info(t('vendorProfile.toast.reportComment'), {
          position: toast.POSITION.TOP_RIGHT
        });
      })
      .catch(e => {
        toastError(e.message, t, 'vendorProfile.toast.reportCommentError')
      });
    }
  }

  const handleCatChipClick = (cat) => {
    routerHistory.push(`/vendors?cat=${cat}`);
  }

  const handleKwChipClick = (kw) => {
    routerHistory.push(`/vendors?kw=${kw}`);
  }
  
  if (!profileVendor) {
    return <LoadingPage />
  }
  
  return (<VendorProfilePage
    onFavoriteClick={handleFavoriteClick}
    profileVendor={profileVendor}
    isFavorite={isFavorite}
    cityToDisplay={cityToDisplay}
    handleCityClick={handleCityClick}
    handleToggleDesc={handleToggleDesc}
    handlePageClick={handlePageClick}
    descCollapsed={descCollapsed}
    commentsActivePage={commentsActivePage}
    currentComments={currentComments}
    vendorsAssimilate={vendorsAssimilate}
    isAllowToComment= {isAllowToComment}
    sendComment={sendComment}
    isLogged={!!loggedInUser}
    redirectToLogin={redirectToLogin}
    goRewards={goRewards}
    reportComment={reportComment}
    onCatChipClick={handleCatChipClick}
    onKwChipClick={handleKwChipClick}
    loadingState={loadingState}
    commentLoadingState={commentLoadingState}
    vendorsAssimilateLoadingState={vendorsAssimilateLoadingState}
    alsoAvailableOn={alsoAvailableOn}
    alsoAvailableOnLoadingState={alsoAvailableOnLoadingState}
    citiesSuggestions={citiesSuggestions}
    countiesSuggestions={countiesSuggestions}
    suggestions={suggestions}
    countiesAndCitiesSuggestions={countiesAndCitiesSuggestions}
    citiesLoadingState={citiesLoadingState}
    keywords={keywords}
    categories={categories}
    />);
  }