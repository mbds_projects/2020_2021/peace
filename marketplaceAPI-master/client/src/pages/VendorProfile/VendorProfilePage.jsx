import React, { useState, useEffect } from 'react';

import VendorCard from '../../components/VendorCard/VendorCard';

import Carousel from "../../components/Carousel/Carousel";
import PinterestCard from "../../components/PinterestCard/PinterestCard";
import OnlineShopCard from '../../components/OnlineShopCard/OnlineShopCard';

import { compareAsc } from 'date-fns'

import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import EmojiEventsOutlinedIcon from '@material-ui/icons/EmojiEventsOutlined';

import { fade, makeStyles, createStyles } from '@material-ui/core/styles';
//import TextField from '@material-ui/core/TextField';
import MapComp from "../../components/Map/Map";

import MenuItem from '@material-ui/core/MenuItem';


import { Trans } from 'react-i18next';

import Avatar from '@material-ui/core/Avatar';

import AddCommentItem from "../../components/AddCommentItem/AddCommentItem";

import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';

import RoomIcon from '@material-ui/icons/Room';
import AddCommentIcon from '@material-ui/icons/AddComment';
import StarHalfIcon from '@material-ui/icons/StarHalf';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarIcon from '@material-ui/icons/Star';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import StoreIcon from '@material-ui/icons/Store';

import CommentItem from '../../components/CommentItem/CommentItem'
import Pagination from '@material-ui/lab/Pagination';

import Grid from '@material-ui/core/Grid';

import { useTranslation } from 'react-i18next';

import useMediaQuery from '@material-ui/core/useMediaQuery';

import Truncate from 'react-truncate';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import Link from '@material-ui/core/Link';
import IconButton from '@material-ui/core/IconButton';
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import LanguageIcon from '@material-ui/icons/Language';
import YouTubeIcon from '@material-ui/icons/YouTube';
import TwitterIcon from '@material-ui/icons/Twitter';
import PinterestIcon from '@material-ui/icons/Pinterest';
import ClearIcon from '@material-ui/icons/Clear';

import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import Paper from '@material-ui/core/Paper';

import Loading from '../Loading/LoadingPage';

import ReactGA from 'react-ga';

import { NavLink } from 'react-router-dom';

import * as Autosuggest from 'react-autosuggest';
import { Helmet } from "react-helmet";
import ShareIcon from '@material-ui/icons/Share';

const parse = require('autosuggest-highlight/parse');

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      margin: "auto",
      marginTop: "100px",
      width: "99%",
      "& a:hover": {
        color: theme.colors.strawberry
      },
      "& .container-title": {
        textAlign: "center",
        fontWeight: 600,
        marginTop: "-10px !important"
      }
    },
    rootMobile: {
      marginTop: "0px"
    },
    cityClickable: {
      "&:hover": {
        cursor: "pointer",
        textDecoration: "underline"
      }
    },
    cityTarget: {
      color: theme.colors.strawberry
    },
    sliderContainer: {
      minHeight: "0px",
      minWidth: "0px",
      "& .SliderContent .slick-slide img": {
        width: "inherit",
        height: "400px",
        margin: "auto"
      }
    },
    sliderContainerMobile: {
      "& .SliderContent .slick-slide img": {
        width: "inherit",
        height: "300px",
        margin: "auto"
      }
    },
    youtubeVideo: {
      width: "100%"
    },
    chip: {
      margin: "5px",
      borderColor: theme.colors.almond
    },
    label: {
      margin: "5px",
      maxWidth: "100px"
    },
    labelMobile: {
      width: "100px"
    },
    activePage: {
      color: theme.colors.almond,
      marginTop: "10px",
      padding: "14px 16px"
    },
    commentSectionTitle: {
      fontWeight: "bold",
      fontSize: "22px",
      paddingRight: "10px"
    },
    commentNumbers: {
      padding: "16px"
    },
    commentHeader: {
      marginTop: "10px",
      marginBottom: "10px"
    },
    vendorsAssimilateTitle: {
      width: "100%"
    },
    vendorsAssimilateTitleMobile: {
      width: "100%",
      textAlign: "center"
    },
    addCommentButton: {
      margin: "20px 10px"
    },
    citiesPart: {
      textAlign: "center"
    },
    citiesPartMobile: {
      marginLeft: "10px",
      marginBottom: "40px"
    },
    icon: {
      position: "relative",
      top: "5px",
      "&:hover": {
        cursor: "pointer"
      }
    },
    iconNoHover: {
      position: "relative",
      top: "5px"
    },
    favorite: {
      color: theme.colors.strawberry,
      cursor: "pointer",
      left: "10px"
    },
    descNoWrap: {
      whiteSpace: "nowrap",
      width: "80%"
    },
    desc: {
      paddingRight: "20px"
    },
    descMobile: {
      padding: "20px",
      textAlign: "justify"
    },
    toggleDesc: {
      padding: 0,
      float: "right",
      color: "black !important",
      fontWeight: "bold",
      "&:hover": {
        cursor: "pointer"
      }
    },
    moreFilters: {
      position: "relative",
      top: "10px",
      marginLeft: "5px"
    },
    infoMobile: {
      padding: "20px"
    },
    commentHeaderMobile: {
      marginTop: "10px",
      marginLeft: "10px",
      marginBottom: "10px"
    },
    commentSectionMobile: {
      padding: "10px"
    },
    commentItem: {
      padding: "10px"
    },
    socialNetworksIcon: {
      padding: "0px",
      margin: "5px"
    },
    instagram: {
      marginLeft: "0px",
      "& .MuiIconButton-label": {
        color: "white",
        padding: "2px",
        borderRadius: "3px",
        background: theme.colors.instagram
      }
    },
    facebook: {
      "& .MuiIconButton-label": {
        color: "white",
        padding: "2px",
        borderRadius: "3px",
        backgroundColor: theme.colors.facebook
      }
    },
    twitter: {
      "& .MuiIconButton-label": {
        color: "white",
        padding: "2px",
        borderRadius: "3px",
        backgroundColor: "#00acee"
      }
    },
    pinterest: {
      "& .MuiIconButton-label": {
        color: "white",
        padding: "2px",
        borderRadius: "3px",
        backgroundColor: "#c8232c"
      }
    },
    youtube: {
      "& .MuiIconButton-label": {
        color: "white",
        padding: "2px",
        borderRadius: "3px",
        backgroundColor: theme.colors.youtube
      }
    },
    website: {
      "& .MuiIconButton-label": {
        color: "white",
        padding: "2px",
        borderRadius: "3px",
        backgroundColor: theme.colors.almond
      }
    },
    followTitle: {
      fontWeight: "bold",
      fontSize: "22px"
    },
    socialNetworksMobile: {
      paddingLeft: "10px"
    },
    loadingState: {
      textAlign: "center"
    },
    imgContent: {
      maxHeight: "400px",
      overflow: "hidden",
      width: "100%"
    },
    alsoAvailableOnTitle: {
      paddingLeft: "10px"
    },
    alsoAvailable: {
      margin: "auto"
    },
    suggestionLight: {
      fontWeight: "normal"
    },
    suggestionStrong: {
      fontWeight: "bolder",
      color: theme.colors.strawberry
    },
    suggestionPaper: {
      /*position: "relative",
      marginRight: theme.spacing(4),
      marginLeft: 0,*/
      // width: '100%',
      width: "auto",
      minWidth: "300px",
      /*[theme.breakpoints.up('md')]: {
        marginLeft: theme.spacing(3),
        width: 310,
      },*/
      "& ul": {
        listStyle: "none",
        margin: "0px",
        padding: "0px",
        "& li > div": {
          paddingLeft: "55px"
        }
      }
    },
    suggestionPaperMobile: {
      position: "relative",
      marginRight: theme.spacing(4),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('md')]: {
        marginLeft: theme.spacing(3),
        width: 200,
      },
      "& ul": {
        listStyle: "none",
        margin: "0px",
        padding: "0px",
        "& li > div": {
          paddingLeft: "55px"
        }
      }
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.black, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.black, 0.25),
      },
      //marginRight: theme.spacing(4),
      //marginLeft: '0px !important',
      width: '100%',
      /*[theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
        height: '50px'
      },*/
      color: "black",
    },
    searchIcon: {
      width: theme.spacing(7),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      color: "black"
    },
    inputRoot: {
      color: 'inherit',
      height: '50px',
      width: "90%"
    },
    inputRootMobile: {
      color: 'inherit',
      height: '50px',
      width: "300px"
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 7),
      transition: theme.transitions.create('width'),
      width: '100%',
      /*[theme.breakpoints.up('md')]: {
        width: 200,
      },*/
    },
    autoSuggestDiv: {
      marginTop: "20px"
    },
    tilde: {
      color: 'lightgrey'
    },
    infoSR: {
      border: `solid ${theme.colors.almond}`,
      borderRadius: '3px',
      marginTop: "20px",
      marginBottom: "20px"
    },
    infoSRMobile: {
      border: `solid ${theme.colors.almond}`,
      borderRadius: '3px',
      margin: "20px 10px",
      padding: "5px"
    },
    imgInfo: {
      width: "60px"
    },
    imgInfoMobile: {
      float: "left",
      margin: "10px 10px 10px 0px",
      width: "60px"
    },
    infoSectionTitle: {
      fontSize: "30px",
      marginBottom: "10px"
    },
    diamond: {
      width: '60px',
      height: '60px',
      backgroundColor: theme.colors.almond,
      //borderRadius: "50%"
    },
    rewards: {
      marginTop: "15px",
      marginBottom: "20px"
    },
    rewardsMobile: {
      textAlign: "center"
    },
    howToClick: {
      fontStyle: "italic",
      textDecoration: "underline",
      cursor: "pointer"
    },
    fbButton: {
      marginLeft: "15px"
    }
  }),
);

export default function VendorProfilePage(props) {
  const { t, i18n } = useTranslation();
  const matches1560 = useMediaQuery('(max-width:1560px)');
  const matches1250 = useMediaQuery('(max-width:1250px)');
  const matches1040 = useMediaQuery('(max-width:1120px)');
  const matches = useMediaQuery('(max-width:768px)');
  const [displayAllKeywords, setDisplayAllKeywords] = useState(false);
  const [openDialogLabels, setOpenDialogLabels] = useState(false);
  const [openDialogRewards, setOpenDialogRewards] = useState(false);

  //const [search, setSearch] = React.useState('');
  const [suggestions, setSuggestions] = useState([]);
  const [citySelected, setCitySelected] = useState(0);

  const [cityClick, setCityClick] = useState(0);
  const [value, setValue] = useState('');

  /*const handleSearch = (event) => {
    setSearch(event.target.value);
  };*/

  const classes = useStyles();

  useEffect(() => {
    ReactGA.pageview(`/vendor/${props.profileVendor.name}`);
    setDisplayAllKeywords(false);
    setCityClick(0);
    setCitySelected(0);
    setValue('');
  }, [props.profileVendor])

  const handleClickCatChip = (cat) => {
    props.onCatChipClick(cat);
  }

  const handleClickKwChip = (kw) => {
    props.onKwChipClick(kw);
  }
  const handleDisplayAllKeywords = () => {
    setDisplayAllKeywords(!displayAllKeywords)
  }
  const handleClickLabelsMobile = () => {
    setOpenDialogLabels(true);
  }

  const handleCloseLabelsMobile = () => {
    setOpenDialogLabels(false);
  }

  const handleClickRewardsMobile = () => {
    setOpenDialogRewards(true);
  }

  const handleCloseRewardsMobile = () => {
    setOpenDialogRewards(false);
  }
  const handleFavoriteClick = (vendor) => {
    props.onFavoriteClick(vendor);
  }

  const toggleDescription = () => {
    props.handleToggleDesc();
  }

  const clickCity = (cityIndex) => {
    setCityClick(cityIndex);
    //props.handleCityClick(cityIndex);
  };

  const getSuggestionValue = (suggestion) => {
    setCitySelected(suggestion.value);
    setCityClick(0);
    return '';
  }

  const handleChangePage = (event, newPage) => {
    props.handlePageClick(newPage - 1);
  };

  const calculRate = (rate, id) => {
    let iconsStar = [];
    let isFloat = !Number.isInteger(rate);
    let floor = -1;
    if (isFloat) {
      floor = Math.floor(rate);
    }
    for (let i = 0; i < 5; i++) {
      if (i < rate) {
        if (i === floor) {
          iconsStar.push(<StarHalfIcon key={`star_${id}_${i}`} className={classes.iconNoHover} />)

        } else {
          iconsStar.push(<StarIcon key={`star_${id}_${i}`} className={classes.iconNoHover} />)

        }
      } else {
        iconsStar.push(<StarBorderIcon key={`star_${id}_${i}`} className={classes.iconNoHover} />)
      }
    }
    return iconsStar;
  };

  const redirectToLogin = () => {
    props.redirectToLogin();
  }

  const vendor = props.profileVendor;
  let { locations } = vendor;


  let mapLocation;
  let firstLocation;


  if (props.countiesAndCitiesSuggestions && props.countiesAndCitiesSuggestions.length > 0) {
    firstLocation = props.countiesAndCitiesSuggestions[citySelected].shops[cityClick]

    if (firstLocation) {
      mapLocation = {
        lat: firstLocation.location.coordinates[0],
        lng: firstLocation.location.coordinates[1],
        zoom: 18,
      }
    }
  }

  let descLang = vendor.desc.lang.find((l) => l.locale === i18n.language);

  const mapSection = mapLocation && <MapComp lat={mapLocation.lat} long={mapLocation.lng} zoom={mapLocation.zoom} firstLocation={firstLocation} vendorName={vendor.name} logoUrl={vendor.logoUrl} />

  const logo = locations.length === 0 && vendor.onlineShop && <OnlineShopCard logoUrl={props.profileVendor.logoUrl} webSite={vendor.site} vendorName={vendor.name} />

  const getSuggestions = (value, suggestions) => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0 ? [] : suggestions.filter(lang =>
      lang.name ? lang.name.toLowerCase().includes(inputValue) : false
    );
  };

  const onSuggestionsFetchRequested = ({ value }) => {
    setSuggestions(getSuggestions(value, props.suggestions));
  };

  const onSuggestionsClearRequested = () => {
    setSuggestions([]);
  };

  function renderSuggestion(suggestion, { query, isHighlighted }) {
    const parts = parse(suggestion.name, [[suggestion.name.toLowerCase().indexOf(query.toLowerCase()), suggestion.name.toLowerCase().indexOf(query.toLowerCase()) + query.length]]);
    return (
      <MenuItem selected={isHighlighted} component="div">
        <div>
          {parts.map((part, index) =>
            part.highlight ? (
              <span key={String(index)} className={classes.suggestionStrong}>
                {part.text}
              </span>
            ) : (
                <span key={String(index)} className={classes.suggestionLight}>
                  {part.text}
                </span>
              )
          )}
        </div>
      </MenuItem>
    );
  }


  const revertSearch = () => {
    setValue('');
    setSuggestions([]);
  }

  const onChange = (event, { newValue }) => {
    setValue(newValue);
  };


  const inputProps = {
    placeholder: t('vendorProfile.searchPlaceholder'),
    value,
    onChange: onChange
  };

  const renderInputComponent = (inputProps) => (
    <div className={classes.search}>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        classes={{
          root: matches ? classes.inputRootMobile : classes.inputRoot,
          input: classes.inputInput,
        }}
        {...inputProps}
      />
      {
        value !== "" &&
        <IconButton aria-label="Clear" onClick={revertSearch}>
          <ClearIcon />
        </IconButton>
      }
    </div>
  );


  const citiesSection = props.countiesAndCitiesSuggestions && props.countiesAndCitiesSuggestions.length > 0 &&
    <div className={classes.citiesPartMobile}>
      <RoomIcon className={classes.iconNoHover} />
      <span className={classes.cityTarget} > {props.countiesAndCitiesSuggestions[citySelected].loc}</span>
      {props.citiesSuggestions.length - 1 > 0 && <span> + {props.citiesSuggestions.length - 1} {t('vendorProfile.cities')} </span>}
      <br />
      <StoreIcon className={classes.iconNoHover} />
      {props.countiesAndCitiesSuggestions[citySelected].shops.map((shop, index, shops) => {
        if (index === cityClick) {
          return (<span><span className={classes.cityTarget} key={index}> {shop.shopName}</span><span className={classes.tilde}>{index < shops.length - 1 && ' ~'}</span></span>)
        } else {
          return (<span><span className={classes.cityClickable} onClick={() => clickCity(index)} key={`${shop.shopName}_${index}`}> {shop.shopName}</span><span className={classes.tilde}>{index < shops.length - 1 && ' ~'}</span></span>)
        };
      })}

      {
        props.countiesAndCitiesSuggestions.length > 1 &&
        <div className={classes.autoSuggestDiv}>
          <Autosuggest
            suggestions={suggestions.slice(0, 5)}
            onSuggestionsFetchRequested={onSuggestionsFetchRequested}
            onSuggestionsClearRequested={onSuggestionsClearRequested}
            getSuggestionValue={getSuggestionValue}
            renderSuggestion={renderSuggestion}
            inputProps={inputProps}
            renderInputComponent={renderInputComponent}
            renderSuggestionsContainer={options => (
              <Paper {...options.containerProps} className={classes.suggestionPaper} square>
                {options.children}
              </Paper>
            )}
          />
        </div>
      }


    </div>;

  const GADetectSocialNetworkClicksClick = (socialNetwork) => {
    ReactGA.event({
      category: 'SocialNetwork',
      action: `Click on ${socialNetwork}`,
      label: `Vendor ${vendor.name}`
    });
  }

  const socialNetworks = <div>
    {
      vendor.socialNetworks.find((el) => el.type === "instagram") &&
      <IconButton
        component={Link}
        href={vendor.socialNetworks.find((el) => el.type === "instagram").url}
        target="_blank" rel="noopener noreferrer"
        color="primary"
        classes={{ colorPrimary: classes.instagram }}
        onClick={() => GADetectSocialNetworkClicksClick("instagram")}>
        <InstagramIcon />
      </IconButton>
    }
    {
      vendor.socialNetworks.find((el) => el.type === "facebook") &&
      <IconButton
        component={Link}
        href={vendor.socialNetworks.find((el) => el.type === "facebook").url}
        target="_blank" rel="noopener noreferrer"
        color="primary"
        classes={{ colorPrimary: classes.facebook }}
        onClick={() => GADetectSocialNetworkClicksClick("facebook")}>
        <FacebookIcon />
      </IconButton>
    }
    {
      vendor.socialNetworks.find((el) => el.type === "twitter") &&
      <IconButton
        component={Link}
        href={vendor.socialNetworks.find((el) => el.type === "twitter").url}
        target="_blank" rel="noopener noreferrer"
        color="primary"
        classes={{ colorPrimary: classes.twitter }}
        onClick={() => GADetectSocialNetworkClicksClick("twitter")}>
        <TwitterIcon />
      </IconButton>
    }  {
      vendor.socialNetworks.find((el) => el.type === "pinterest") &&
      <IconButton
        component={Link}
        href={vendor.socialNetworks.find((el) => el.type === "pinterest").url}
        target="_blank" rel="noopener noreferrer"
        color="primary"
        classes={{ colorPrimary: classes.pinterest }}
        onClick={() => GADetectSocialNetworkClicksClick("pinterest")}>
        <PinterestIcon />
      </IconButton>
    }
    {
      vendor.socialNetworks.find((el) => el.type === "youtube") &&
      <IconButton
        component={Link}
        href={vendor.socialNetworks.find((el) => el.type === "youtube").url}
        target="_blank" rel="noopener noreferrer"
        color="primary"
        classes={{ colorPrimary: classes.youtube }}
        onClick={() => GADetectSocialNetworkClicksClick("youtube")}>
        <YouTubeIcon />
      </IconButton>
    }
    {
      vendor.socialNetworks.find((el) => el.type === "website") &&
      <IconButton
        component={Link}
        href={vendor.socialNetworks.find((el) => el.type === "website").url}
        target="_blank" rel="noopener noreferrer"
        color="primary"
        classes={{ colorPrimary: classes.website }}
        onClick={() => GADetectSocialNetworkClicksClick("website")}>
        <LanguageIcon />
      </IconButton>
    }
  </div>;

  const socialNetworksSection = vendor.socialNetworks && vendor.socialNetworks.length > 0 &&
    <div/* className="socialNetworks"*/>
      <b>{(t('vendorProfile.follow'))} {vendor.name}</b>
      {socialNetworks}

    </div>

  const socialNetworksSectionMobile = vendor.socialNetworks && vendor.socialNetworks.length > 0 &&
    <div className={classes.socialNetworksMobile}>
      <div className={classes.followTitle}>{(t('vendorProfile.follow'))} {vendor.name}</div>
      {socialNetworks}

    </div>

  const vendorInfoSection = <div /*className="vendorInfo"*/>
    <div className={classes.infoSectionTitle}>
      {vendor.name}
      {props.isFavorite(vendor) ?
        <FavoriteIcon onClick={() => handleFavoriteClick(props.profileVendor)} className={`${classes.icon} ${classes.favorite}`} />
        : <FavoriteBorderIcon onClick={() => handleFavoriteClick(props.profileVendor)} className={`${classes.icon} ${classes.favorite}`} />}

        <Button
        size="small"
        variant="contained"
        color="primary"
        className={classes.fbButton}
            onClick={() => window.open(`https://www.facebook.com/sharer/sharer.php?u=https://www.super-responsable.org/vendors/${vendor.id}?name=${vendor.name}`, "_blank")}
            startIcon={<FacebookIcon/>}
        >
            {t('vendorProfile.share')}
        </Button>
    </div>

    {
      props.categories && props.categories.map((el) => {
        let catName = el.lang.find((l) => l.locale === i18n.language).value;
        return (
          <Chip
            key={el.id}
            label={catName}
            className={classes.chip}
            onClick={() => { handleClickCatChip(el.id) }}
            variant="outlined"
          />
        )
      })
    }
    {
      props.keywords && props.keywords.map((el) => {
        let keyName = el.lang.find((l) => l.locale === i18n.language).value;
        return (
          <Chip
            key={el.id}
            label={keyName}
            className={classes.chip}
            onClick={() => { handleClickKwChip(el.id) }}
            variant="outlined"
          />
        )
      })
    }
  </div>;

  let rewardsCount = vendor.rewards && vendor.rewards.length > 0 ? vendor.rewards.filter(reward => (reward.activated && !(reward.validity && compareAsc(new Date(reward.validity), new Date()) < 0))).length : 0;

  const vendorInfoSectionMobile = <div className={classes.infoMobile}><h2>
    {vendor.name}
    {props.isFavorite(vendor) ?
      <FavoriteIcon onClick={() => handleFavoriteClick(props.profileVendor)} className={`${classes.icon} ${classes.favorite}`} />
      : <FavoriteBorderIcon onClick={() => handleFavoriteClick(props.profileVendor)} className={`${classes.icon} ${classes.favorite}`} />}
    
    <IconButton onClick={() => window.open(`https://www.facebook.com/sharer/sharer.php?u=https://www.super-responsable.org/vendors/${vendor.id}?name=${vendor.name}`, "_blank")}>
      <ShareIcon  
        color="primary"/>
    </IconButton>

  </h2>

    <Grid container direction="row">
      {vendor.labels && vendor.labels.length > 0 &&
        <Grid item>
          <Chip
            avatar={<EmojiEventsOutlinedIcon />}
            label={vendor.labels.length}
            className={classes.chip}
            variant="outlined"
            onClick={handleClickLabelsMobile}
          />
        </Grid>

      }

      {rewardsCount > 0 &&
        <Grid item>
          <Chip
            avatar={<CardGiftcardIcon />}
            label={rewardsCount}
            className={classes.chip}
            variant="outlined"
            onClick={handleClickRewardsMobile}
          />
        </Grid>

      }
      {
        props.categories && props.categories.length > 0 &&
        props.categories.map((cat) => {
          let label = cat.lang.find((l) => l.locale === i18n.language).value;
          return (
            <Chip
              key={`chip-${cat.id}`}
              label={label}
              className={classes.chip}
              variant="outlined"
              onClick={() => { handleClickCatChip(cat.id) }}
            />
          )
        })
      }
      {
        props.keywords && props.keywords.length > 0 && !displayAllKeywords &&
        <Grid item>
          <Chip
            label={props.keywords[0].lang.find((l) => l.locale === i18n.language).value}
            className={classes.chip}
            variant="outlined"
            onClick={() => { handleClickKwChip(props.keywords[0].id) }}
          />
        </Grid>
      }
      {
        props.keywords && props.keywords.length > 1 && !displayAllKeywords &&
        <Grid item>
          <div className={classes.moreFilters} onClick={handleDisplayAllKeywords}>+ {t('vendorProfile.filters')}</div>
        </Grid>
      }
      {
        props.keywords && props.keywords.length > 0 && displayAllKeywords &&
        props.keywords.map((kw) => {
          let label = kw.lang.find((l) => l.locale === i18n.language).value;
          return (<Grid item key={`chip-${kw.id}`}>
            <Chip
              label={label}
              className={classes.chip}
              variant="outlined"
              onClick={() => { handleClickKwChip(kw.id) }}
            />
          </Grid>);
        })

      }
      {
        props.keywords && props.keywords.length > 0 && displayAllKeywords &&
        <Grid item>
          <div className={classes.moreFilters} onClick={handleDisplayAllKeywords}>- {t('vendorProfile.filters')}</div>
        </Grid>
      }

    </Grid>

    <Dialog
      open={openDialogLabels && matches}
      onClose={handleCloseLabelsMobile}>
      <DialogTitle>{t('vendorProfile.labels.title')} {vendor.name}</DialogTitle>
      <DialogContent>
        <p>{t('vendorProfile.labels.desc')}</p>
        <Grid container>
          {
            vendor.labels && vendor.labels.map((el) => {
              let imgSrc = el.pictureUrl;
              return (
                  <Grid item xs={6} key={`label-${el.id}`}>
                    <img className={classes.labelMobile} key={el.id} src={imgSrc} alt={el.name} />
                  </Grid>
              )
            })
          }
        </Grid>

      </DialogContent>
    </Dialog>

    <Dialog
      open={openDialogRewards && matches}
      onClose={handleCloseRewardsMobile}>
      <DialogTitle><Trans i18nKey="vendorProfile.rewards.title"
        values={{ vendorName: vendor.name }}
        components={[]} /></DialogTitle>
      <DialogContent>
        <Grid container direction="row" justify="center" alignItems="center" className={classes.rewardsMobile}>
          <Grid item xs={2}>
            <Avatar className={classes.diamond} src="/assets/images/icons/diamond.png" />
          </Grid>
          <Grid item xs={12}>
            {rewardsCount > 1 ?
              <Trans i18nKey="vendorProfile.rewards.explanations"
                values={{ points: rewardsCount }}
                components={[<br />, <span onClick={props.goRewards} className={classes.howToClick}></span>]} />
              :

              <Trans i18nKey="vendorProfile.rewards.explanation"
                values={{ points: rewardsCount }}
                components={[<br />, <span onClick={props.goRewards} className={classes.howToClick}></span>]} />
            }
          </Grid>
        </Grid>

      </DialogContent>
    </Dialog>


  </div>;

  let videoSection;
  if (vendor.videoYoutubeId) videoSection = <iframe title="youtube video" className={classes.youtubeVideo} width="560" height="315" src={`https://www.youtube.com/embed/${vendor.videoYoutubeId}`}></iframe>
  if (vendor.videoDailymotionId) videoSection = <iframe title="dailymotion video" className={classes.youtubeVideo} width="560" height="315" src={`https://www.dailymotion.com/embed/video/${vendor.videoDailymotionId}`}></iframe>
  if (vendor.videoVimeoId) videoSection = <iframe title="vimeo-player" className={classes.youtubeVideo} width="560" height="290" src={`https://player.vimeo.com/video/${vendor.videoVimeoId}`}></iframe>

  let widthTruncate = 500;
  if (matches1560) widthTruncate = 400;
  if (matches1250) widthTruncate = 350;
  if (matches1040) widthTruncate = 300;
  if (matches) widthTruncate = 300;

  const descSection = descLang && descLang.value &&
    <div className={matches ? classes.descMobile : classes.desc} >
      <b>{t('vendorProfile.ourStory')}</b><br />
      {(props.descCollapsed ?
        <Truncate className={classes.descNoWrap} lines={3} width={widthTruncate} ellipsis={
          <span onClick={toggleDescription} className={classes.toggleDesc}>+ {t('vendorProfile.more')}</span>}>
          {descLang.value.split('\\n').map((line, i, arr) => {
            const newLine = <span key={i}>{line}</span>;

            if (i === arr.length - 1) {
              return newLine;
            } else {
              return [newLine, <br key={i + 'br'} />];
            }
          })}
        </Truncate>
        :
        <span>
          {descLang.value.split('\\n').map((line, i, arr) => {
            const newLine = <span key={i}>{line}</span>;

            if (i === arr.length - 1) {
              return newLine;
            } else {
              return [newLine, <br key={i + 'br'} />];
            }
          })}
          <span onClick={toggleDescription} className={classes.toggleDesc}>- {t('vendorProfile.less')}</span>
        </span>
      )}</div>

  const commentSection = <div className={matches ? classes.commentSectionMobile : ""}>
    <div className={matches ? classes.commentHeaderMobile : classes.commentHeader}>
      <span className={classes.commentSectionTitle}>{t('vendorProfile.comments')}</span>
      {calculRate(vendor.rate ? vendor.rate : 0, "rateAvg")}
      <span className={classes.commentNumbers}>{vendor.comments && vendor.comments.length > 0 ? vendor.comments.length : 0}</span>
    </div>
    {
      !props.isLogged &&
      <Button
        variant="contained"
        color="primary"
        className={classes.addCommentButton}
        onClick={redirectToLogin}
        endIcon={<AddCommentIcon />}
      >
        {t('vendorProfile.addComment')}
      </Button>
    }
    {
      props.isAllowToComment &&
      <AddCommentItem sendComment={props.sendComment} />
    }
    {
      props.isAllowToComment && <br />
    }

    {
      vendor.comments && vendor.comments.length > 0 && props.currentComments.map((el) => {
        return (
          <CommentItem
            comment={el}
            reportComment={props.reportComment}
            key={el.id} />
        );
      })
    }
    {
      vendor.comments && vendor.comments.length > 0 &&
      <Grid container direction="row" justify="center">
        <Grid item>
          <Pagination
            count={Math.ceil(vendor.comments.length / 3)}
            page={props.commentsActivePage + 1}
            onChange={handleChangePage}
            /*boundaryCount={5} */
            variant="outlined"
            shape="rounded"
          />
        </Grid>
      </Grid>
    }
  </div>

  const GADetectClick = (category, action) => {
    ReactGA.event({
      category: category,
      action: action,
      label: `Vendor ${vendor.name}`
    });
  }


  const returnCard = (element, id, GAtitle, GAsubTitle, xs, sm) => {
    return (
      <Grid item xs={xs} sm={sm} key={id} onClick={() => GADetectClick(GAtitle, GAsubTitle)}>
        {element}
      </Grid>
    )
  }

  let alsoAvailableOnCards = props.alsoAvailableOn && props.alsoAvailableOn.map((vendor) => returnCard(<VendorCard vendor={vendor} />, vendor.id, 'Vendors', `Click on vendor ${vendor.name} who sell the vendor ${props.profileVendor.name} from his page`, 12, 12))

  const alsoAvailableOnSection = props.alsoAvailableOn && props.alsoAvailableOn.length > 0 &&
    <div>
      <Grid container direction="row">
        {
          props.alsoAvailableOnLoadingState ?
            <div className={classes.loadingState}>
              <Loading />
            </div>
            : matches ?
              <Carousel carouselComponent={alsoAvailableOnCards} arrows={true} />
              : <Carousel carouselComponent={alsoAvailableOnCards} infinite={true} autoplay={true} dots={true} />
        }
      </Grid>

    </div>

  const vendorAssimilateTitle = props.vendorsAssimilate && props.vendorsAssimilate.length > 0 &&
    <div>
      <h2 className={matches ? classes.vendorsAssimilateTitleMobile : classes.vendorsAssimilateTitle}>{t('vendorProfile.vendorsAssimilate')}</h2>
    </div>;


  const GADetectClickFeaturedProduct = (index) => {
    ReactGA.event({
      category: 'Featured Products',
      action: `Click on product number ${index + 1}`,
      label: `Vendor ${vendor.name}`
    });
  }

  let vendorsAssimilate = props.vendorsAssimilate && props.vendorsAssimilate.map((vendor) => returnCard(<VendorCard vendor={vendor} />, vendor.id, 'Vendors', `Click on vendor ${vendor.name} from assimilates vendors`, 12, 4))

  let carouselPictures = []

  if (vendor.carouselPicturesUrl && vendor.carouselPicturesUrl.length > 0) {
    carouselPictures = vendor.carouselPicturesUrl.map((imgSrc, index) => {
      return (<div className={classes.imgContent} key={index}>
        <img alt={`Carousel image ${index}`} src={imgSrc} />
      </div>);
    })
  }

  let featuredProducts = [];

  if (vendor.featuredProductsUrl && vendor.featuredProductsUrl.length > 0) {
    for (let i = 0; i < vendor.featuredProductsUrl.length; i++) {
      let col = 1;
      if (i === 0 || i === 3 || i === 4) {
        col = 2;
      }
      featuredProducts.push({
        link: vendor.featuredProductsUrl[i].link,
        imgUrl: vendor.featuredProductsUrl[i].imgUrl,
        col
      })
    }
  }

  let rewards = rewardsCount > 0 &&
    <Grid container direction="row" justify="center" alignItems="center" className={classes.rewards}>
      <Grid item xs={12}>
        <Trans i18nKey="vendorProfile.rewards.title"
          values={{ vendorName: vendor.name }}
          components={[<b></b>, <br />]} />
      </Grid>
      <Grid item xs={2}>
        <Avatar className={classes.diamond} src="/assets/images/icons/diamond.png" />
      </Grid>
      <Grid item xs={10}>
        {rewardsCount > 1 ?
          <Trans i18nKey="vendorProfile.rewards.explanations"
            values={{ points: rewardsCount }}
            components={[<br />, <span onClick={props.goRewards} className={classes.howToClick}></span>]} />
          :

          <Trans i18nKey="vendorProfile.rewards.explanation"
            values={{ points: rewardsCount }}
            components={[<br />, <span onClick={props.goRewards} className={classes.howToClick}></span>]} />
        }
      </Grid>
    </Grid>
  return (
    <div className={matches ? `${classes.root} ${classes.rootMobile}` : classes.root}>

        <Helmet>
            <meta charSet="utf-8" />
            <title>Super Responsable</title>
            <link rel="canonical" href="https://www.super-responsable.org/" />
            <meta property="og:title" content={`Super Responsable - Découvre la marque ${vendor.name}, ses tops ventes et ses bons plans`} />
            <meta property="og:description" content="Pas besoin de super pouvoirs pour devenir un héros du quotidien" />
            <meta property="og:url" content={`https://www.super-responsable.org/vendors/${vendor.id}?name=${vendor.name}`} />
            <meta property="og:image" content={vendor.profilePictureUrl} />
            <meta name="keywords" content={`${props.keywords.map(kw => kw.name).join(', ')}, ${props.categories.map(cat => cat.name).join(', ')}`}/>
          </Helmet>
      {
        props.loadingState ?

          <div className={classes.loadingState}>
            <Loading />
          </div>
          :
          matches ?
            <Grid container direction="column" justify="center">
              <Grid item xs={12}>
                {vendorInfoSectionMobile}
              </Grid>
              <Grid item xs={12} className={classes.sliderContainerMobile}>
                {carouselPictures && carouselPictures.length > 0 && <Carousel carouselComponent={carouselPictures} infinite={true} autoplay={true} dots={true} />}
              </Grid>
              <Grid item xs={12}>
                {descSection}
              </Grid>
              <Grid item xs={12}>
                {
                  props.citiesLoadingState ?
                    <div className={classes.loadingState}>
                      <Loading />
                    </div>
                    :
                    <span>
                      {mapLocation && <b>{(t('vendorProfile.map'))}</b>}
                      {mapSection}
                      {logo}
                      {citiesSection}</span>}
              </Grid>
              <Grid item sm={12}>
                {props.alsoAvailableOn && props.alsoAvailableOn.length > 0 && <b className={matches ? classes.alsoAvailableOnTitle : ''}>{(t('vendorProfile.alsoAvailableOn'))}</b>}
              </Grid>
              <Grid item xs={12}>
                {alsoAvailableOnSection}
              </Grid>
              <Grid item xs={12}>
                {videoSection}
              </Grid>
              <Grid item xs={12}>
                {featuredProducts && featuredProducts.length > 0 && <PinterestCard items={featuredProducts} column={3} GADetectClick={GADetectClickFeaturedProduct} />}
              </Grid>
              <Grid item xs={12}>
                {socialNetworksSectionMobile}
              </Grid>
              <Grid item xs={12}>
                {props.commentLoadingState ?
                  <div className={classes.loadingState}>
                    <Loading />
                  </div>
                  : commentSection}
              </Grid>
              <Grid item sm={10} className={classes.infoSRMobile}>

                <img alt="Super Responsable logo" src="/assets/images/icons/sr-logo-v3.png" className={classes.imgInfoMobile} />

                <Trans i18nKey="vendorProfile.info"
                  components={[<NavLink to={`/contactUs?q=contact`}></NavLink>]} />

              </Grid>
              <Grid item xs={12}>
                {vendorAssimilateTitle}
              </Grid>

              <Grid container item xs={12} className={classes.sliderContainer}>
                {props.vendorsAssimilateLoadingState ?
                  <div className={classes.loadingState}>
                    <Loading />
                  </div>
                  : <Carousel carouselComponent={vendorsAssimilate} arrows={true} />
                }
              </Grid>
            </Grid>
            :
            <Grid container direction="row" spacing={3} justify="center">
              <Grid container direction="column" spacing={3} justify="center" item sm={6}>
                <Grid item sm={12}>
                  {vendorInfoSection}<br />
                  {descSection}<br />
                  {socialNetworksSection}<br />
                  {featuredProducts && featuredProducts.length > 0 && <PinterestCard items={featuredProducts} column={3} GADetectClick={GADetectClickFeaturedProduct} />}
                  {vendor.labels && vendor.labels.length > 0 &&
                    <div>
                      <br />
                      <b>{t('vendorProfile.labels.title')} {vendor.name}</b><br />

                      {t('vendorProfile.labels.desc')}<br />
                      {
                        vendor.labels && vendor.labels.map((el) => {
                          let imgSrc = el.pictureUrl;
                          return (
                            imgSrc !== '' ? <img className={classes.label} key={el.id} src={imgSrc} alt={el.name} /> : ''
                          )
                        })
                      }
                      <br /><br />
                    </div>
                  }
                  {rewards}
                  <Grid item sm={12}>
                    {props.alsoAvailableOn && props.alsoAvailableOn.length > 0 && <b className={matches ? classes.alsoAvailableOnTitle : ''}>{(t('vendorProfile.alsoAvailableOn'))}</b>}
                  </Grid>
                  <Grid item sm={9} className={classes.alsoAvailable}>
                    {alsoAvailableOnSection}
                  </Grid>
                </Grid>
              </Grid>
              <Grid container direction="column" spacing={3} justify="center" item sm={6}>
                <Grid item sm={12} className={classes.sliderContainer}>
                  {
                    props.citiesLoadingState ?
                      <div className={classes.loadingState}>
                        <Loading />
                      </div>
                      :
                      <span>
                        {mapLocation && <b>{(t('vendorProfile.map'))}</b>}
                        {mapSection}
                        {logo}
                        {citiesSection}<br /></span>
                  }

                  {carouselPictures && carouselPictures.length > 0 && <Carousel carouselComponent={carouselPictures} infinite={true} autoplay={true} dots={true} />}
                  <br /><br />
                  {videoSection}
                </Grid>
              </Grid>
              <Grid item sm={12}>
                {props.commentLoadingState ?
                  <div className={classes.loadingState}>
                    <Loading />
                  </div>
                  : commentSection}
              </Grid>
              <Grid container item sm={10} className={classes.infoSR} spacing={1} justify="center">
                <Grid item xs={1}>
                  <img alt="Super Responsable logo" src="/assets/images/icons/sr-logo-v3.png" className={classes.imgInfo} />
                </Grid>
                <Grid item xs={11}>

                  <Trans i18nKey="vendorProfile.info"
                    components={[<NavLink to={`/contactUs?q=contact`}></NavLink>]} />

                </Grid>
              </Grid>
              <Grid item sm={12}>
                {vendorAssimilateTitle}
              </Grid>
              {props.vendorsAssimilateLoadingState ?
                <div className={classes.loadingState}>
                  <Loading />
                </div>
                : vendorsAssimilate}
            </Grid>
      }
    </div>
  );
}

