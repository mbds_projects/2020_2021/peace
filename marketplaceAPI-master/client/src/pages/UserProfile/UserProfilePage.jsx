import React, { useState, useEffect, useRef } from 'react';

import Avatar from '@material-ui/core/Avatar';

import Grid from '@material-ui/core/Grid';

import Badge from '@material-ui/core/Badge';

import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Switch from '@material-ui/core/Switch';

import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import Select from 'react-select';

import TextField from '@material-ui/core/TextField';

import PhotoCamera from '@material-ui/icons/PhotoCamera';

import SaveIcon from '@material-ui/icons/Save';

import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import frLocale from "date-fns/locale/fr";
import enLocale from "date-fns/locale/en-GB";
// pick a date util library
import DateFnsUtils from '@date-io/date-fns';
import {
  DatePicker
} from '@material-ui/pickers';

import Chip from '@material-ui/core/Chip';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import * as _ from 'lodash';

import { useTranslation } from 'react-i18next';

import ReactGA from 'react-ga';

import Divider from '@material-ui/core/Divider';

import routerHistory from '../../shared/router-history/router-history';

ReactGA.pageview('/user');

const useStyles = makeStyles((theme) =>
  createStyles({
    chip: {
      margin: '5px'
    },
    root: {
      width: "99%",
      "& body": {
        fontFamily: "'Open Sans', sans-serif",
        "-webkit-font-smoothing": "antialiased",
        "-moz-osx-font-smoothing": "grayscale"
      },
      "& .MuiPaper-root": {
        padding: "20px",
        margin: "25px 15px"
      }
    },
    avatar: {
      width: "200px !important",
      height: "200px !important",
      margin: "auto"
    },
    inputUpload: {
      display: "none"
    },
    saveButton: {
      textAlign: "center",
      marginBottom: "20px"
    },
    avatarPaper: {
      textAlign: "center"
    },
    birthday: {
      margin: "15px 0px 15px",
      width: "100%",
      "& div input": {
        margin: "0px"
      }
    },
    linkFbGroup: {
      "& a": {
        textDecoration: "underline",
        color: "rgba(0, 0, 0, 0.54)",
        margin: "0",
        fontSize: "0.75rem",
        marginTop: "8px",
        minHeight: "1em",
        textAlign: "left",
        fontFamily: "'Open Sans', sans-serif",
        fontWeight: "400",
        lineHeight: "1em",
        letterSpacing: "0.03333em"
      }
    },
    diamond: {
      width: '80px',
      height: '80px',
      backgroundColor: theme.colors.almond,
      //borderRadius: "50%"
    },
    achievementSmall: {
      width: '40px',
      height: '40px',
      margin: "5px"
    },
    achievementSmallOpacity: {
      width: '40px',
      height: '40px',
      margin: "5px",
      opacity: "0.3"
    },
    badge: {
      height: "30px",
      padding: "0px 10px",
      borderRadius: "50%",
      fontSize: "20px"
    },
    divider: {
      marginTop: "10px",
      marginBottom: "10px"
    },
    buttonUseDiamond: {
      fontSize: "14px",
      padding: "0px 5px",
      marginTop: "15px"
    },
    keywordsFilter: {
      zIndex: "2"
    }
  }),
);

const regexPassword = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}");

const localeMap = {
  fr: frLocale,
  en: enLocale
};

const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop)

function LinearProgressWithLabel(props) {
  return (
    <Box display="flex" alignItems="center">
      <Box width="100%" mr={1}>
        <LinearProgress variant="determinate" {...props} />
      </Box>
      <Box minWidth={35}>
        <Typography variant="body2" color="textSecondary">{`${Math.round(
          props.value,
        )}%`}</Typography>
      </Box>
    </Box>
  );
}

export default function UserProfilePage(props) {
  const classes = useStyles();
  const { t, i18n } = useTranslation();


  const GADetectKAddKeyword = (keywordLabel) => {
    ReactGA.event({
      category: 'Keywords',
      action: `Add the value ${keywordLabel} to profile`,
      label: `User profile`
    });
  }

  const GADetectKRemoveKeyword = (keywordLabel) => {
    ReactGA.event({
      category: 'Keywords',
      action: `Remove the value ${keywordLabel} to profile`,
      label: `User profile`
    });
  }

  const myRef = useRef(null)
  const executeScroll = () => scrollToRef(myRef)

  const [selectedDate, setSelectedDate] = useState(null);
  const [currentPassword, setCurrentPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [keepFile, setKeepFile] = useState(null);
  const [parametersUpdated, setParametersUpdated] = useState(false);
  const [dateDisabled, setDateDisabled] = useState(!!props.profileUser.birthday);

  const [selectedKeywords, setSelectedKeywords] = useState([]);
  const [selectedKeyword, setSelectedKeyword] = useState(null);

  const [keywordsOptions, setKeywordsOptions] = useState([]);

  const [previewUrl, setPreviewUrl] = useState('');

  useEffect(() => {
    const keywordsOptionsTmp = props.keywords.map(keyword => {
      let keyName = keyword.lang.find(l => l.locale === i18n.language).value;        
      return{
      value: keyword.id,
      label: keyName
      }}).sort(function(a, b){
        if(a.label < b.label) { return -1; }
        if(a.label > b.label) { return 1; }
        return 0;
    });
  
    
    let slKwsInit = [];
    props.profileUser.keywords.forEach(el => {
      let keyword = props.keywords.find((kw) => kw.id === el.id );
      if(keyword) {
        let keyName = keyword.lang.find(l => l.locale === i18n.language).value;        
      
        slKwsInit.push({
          value: keyword.id,
          label: keyName
          });
      }
    });
  
    slKwsInit.forEach(el => {
      let i = _.findIndex(keywordsOptionsTmp, function(o) { return o.value === el.value; });
      keywordsOptionsTmp.splice(i, 1)
    })

    setKeywordsOptions(keywordsOptionsTmp);
    setSelectedKeywords(slKwsInit);
  }, [props.keywords, i18n.language])

  useEffect(()=> {
    if(parametersUpdated)executeScroll()
  }, [parametersUpdated])

  const handleKeywordsSelect = (keyword) => {
    let kws = [...selectedKeywords];
    kws.push(keyword)
    setSelectedKeywords(kws);

    const values = [...keywordsOptions];
    let i = _.findIndex(values, function(o) { return o.value === keyword.value; });
    values
    .splice(i, 1)
    .sort(function(a, b){
      if(a.label < b.label) { return -1; }
      if(a.label > b.label) { return 1; }
      return 0;
    });
    setKeywordsOptions(values);

    setSelectedKeyword(null);

    let slKeywordsTmp = kws.map(keyword => keyword.value)

    let diff = _.differenceWith(slKeywordsTmp, props.profileUser.keywords, _.isEqual);
    let diff2 = _.differenceWith(props.profileUser.keywords, slKeywordsTmp, _.isEqual);

    if(diff.length > 0 || diff2.length > 0) {
      setParametersUpdated(true);
      GADetectKAddKeyword(keyword.label)
    }
  }

  const handleRemoveKeyword = (keyword)  =>{
    const kws = [...selectedKeywords];
    let i = _.findIndex(kws, function(o) { return o.value === keyword.value; });
    kws
    .splice(i, 1)
    .sort(function(a, b){
      if(a.label < b.label) { return -1; }
      if(a.label > b.label) { return 1; }
      return 0;
    });
    setSelectedKeywords(kws);


    const optionsTmp = [...keywordsOptions];
    optionsTmp.push(keyword);
    optionsTmp.sort(function(a, b){
      if(a.label < b.label) { return -1; }
      if(a.label > b.label) { return 1; }
      return 0;
    });
    setKeywordsOptions(optionsTmp);


    let slKeywordsTmp = kws.map(keyword => keyword.value)

    let diff = _.differenceWith(slKeywordsTmp, props.profileUser.keywords, _.isEqual);
    let diff2 = _.differenceWith(props.profileUser.keywords, slKeywordsTmp, _.isEqual);

    if(diff.length > 0 || diff2.length > 0) {
      setParametersUpdated(true);
      GADetectKRemoveKeyword(keyword.label);
    }

  }

  const handleDateChange = (e) => {
    setSelectedDate(e);
    setParametersUpdated(true);
  }

  const handleChangePicture = async ( event ) => {
    const {
      target: {
        validity,
        files: [file],
      }
    } = event;

    var reader = new FileReader();
      
    reader.onload = function(e) {
      setPreviewUrl(e.target.result);
    }
      
    reader.readAsDataURL(file); // convert to base64 string
    
    
    if (validity.valid) {
      setKeepFile(file);
      setParametersUpdated(true);
      //executeScroll();
    }
  };

  const handleUpdateAttempt = (e) => {
    e.preventDefault();
    let hasPwd = currentPassword !=='';
    let hasNewPwd = newPassword !=='';
    let toUpdate = {};
    if (hasPwd || hasNewPwd) {
      props.updatePassword(currentPassword, newPassword);
      setCurrentPassword("");
      setNewPassword("");
    } 

    if (selectedDate !== null && !dateDisabled) {
      //props.updateBirthday(selectedDate);
      toUpdate.birthday = selectedDate;
      setDateDisabled(true);
    } 

    if(keepFile) {
      props.uploadPicture(keepFile);
      setKeepFile(null);
    }

    let slKeywordsTmp = selectedKeywords.map(keyword => keyword.value);

    let keywordsIds = props.profileUser.keywords.map(keyword => keyword.id);
    if(JSON.stringify(slKeywordsTmp) !== JSON.stringify(keywordsIds)) {
      toUpdate.keywords = slKeywordsTmp;
    }
    if(!_.isEmpty(toUpdate)) {
      props.updateUser({input: toUpdate});
    }
    setParametersUpdated(false);
  }

  const handleChangeCurrentPassword = (event) => {
    setCurrentPassword(event.target.value);

    setParametersUpdated(true);
  };

  const handleChangeNewPassword = (event) => {
    setNewPassword(event.target.value);

    setParametersUpdated(true);
  };
  
  const chipsKw = [];
    selectedKeywords.forEach((keyword) => {
      chipsKw.push( 
          <Chip
            key={keyword.value}
            label={keyword.label}
            className={classes.chip}
            variant="outlined"
            onDelete={() => handleRemoveKeyword(keyword)}
          />
        )
    })


  const user = props.profileUser;
    let imgSrc  = "";
    let avatarText = `${user.name.charAt(0)} ${user.surname.charAt(0)}`
    
    if(previewUrl !== '') {
      imgSrc = previewUrl;
      avatarText = "";
    }

    if(previewUrl === '' && user && user.profilePictureUrl) {
      imgSrc = user.profilePictureUrl
      avatarText = "";
    }

    const goRewards = () => {
      routerHistory.push("/user/rewards")
    }

    let achievementsPoints = user.achievements.filter(achievement => achievement.activated).reduce((accumulator, currentValue) => accumulator + currentValue.value, 0) - user.rewards.length;
    if(achievementsPoints < 0) achievementsPoints = 0;
    return (
      <div className={classes.root}>
        <Grid container>
          {
            (!user.achievements || !user.achievements.find(a => a.type === "complete_profile" && a.activated)) &&
            <Grid item xs={12}>
              <Paper>
                <b>{t('profile.completeProfile')}</b><br/>
                {props.nextStepCompletion}
                <LinearProgressWithLabel value={props.completion} />
              </Paper>
            </Grid>
          }
          <Grid item xs={12} sm={4}>
            <Grid item xs={12}>

              <Paper className={classes.avatarPaper}>
                <Avatar style={{backgroundColor: user.defaultColor}} className={classes.avatar} src={imgSrc}>{avatarText}</Avatar>
                <input accept="image/*" className={classes.inputUpload} id="icon-button-file" type="file" onChange={event => handleChangePicture(event)} />
                <label htmlFor="icon-button-file">
                  <IconButton color="primary" component="span">
                    <PhotoCamera />
                  </IconButton>
                </label>
              </Paper>
            </Grid>
            <Grid item xs={12}>
              
            <Paper>
              <Grid container direction="column" justify="center" alignItems="center">
                <Grid xs={4}>
                  <Badge 
                    badgeContent={achievementsPoints} 
                    showZero
                    color="primary"
                    overlap="circle"
                    classes={{badge: classes.badge}}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'right',
                    }}>
                    <Avatar className={classes.diamond} src="/assets/images/icons/diamond.png" />
                  </Badge>
                </Grid>

                <Button
                  variant="contained"
                  color="primary"
                  size="large"
                  className={classes.buttonUseDiamond}
                  onClick={goRewards}>
                  {achievementsPoints > 0 ? t('profile.useDiamonds') : t('profile.winDiamonds')}
                </Button>
                <Divider variant="middle" className={classes.divider}/>
                { user.achievements.filter(achievement => achievement.activated).length > 0 && <b>{t('profile.achievements')}</b>}
                <Grid xs={12}>
                {
                  user.achievements.filter(achievement => achievement.activated)
                  .sort((a,b) => {
                    return a.level - b.level;
                  })/*.sort((a,b) => {
                    let achievementUserIds = user.achievements.map(ac => ac.id);
                    
                    let aInclude = achievementUserIds.includes(a.id);
                    let bInclude = achievementUserIds.includes(b.id);
                    if(aInclude && !bInclude) return -1;
                    if(bInclude && !aInclude) return 1;
                    return 0;
                  })*/.map(achievement => { 
                    //let achievementUserIds = user.achievements.map(a => a.id);
                    return (
                    <img 
                    alt={t(`achievements.${achievement.subtype}`)}
                    key={achievement.subtype}
                    className={/*achievementUserIds.includes(achievement.id) ? */classes.achievementSmall /*: classes.achievementSmallOpacity*/}
                    title={t(`achievements.${achievement.subtype}`)}
                    src={achievement.icon} />)
                  }
                  )
                }
                </Grid>
              </Grid>
              
            </Paper>

            </Grid>

          </Grid>
          <Grid item xs={12} sm={8}>
            <Grid item xs={12}>
              <Paper>
                <b>{t('profile.personalDetails')}</b>
                <TextField
                  id="name"
                  label={t('form.name.label')}
                  value={props.profileUser.name}
                  fullWidth
                  disabled
                  margin="normal"
                  variant="outlined"
                />
                <TextField
                  id="surname"
                  label={t('form.surname.label')}
                  value={props.profileUser.surname}
                  fullWidth
                  disabled
                  margin="normal"
                  variant="outlined"
                />
                  <MuiPickersUtilsProvider utils={DateFnsUtils} locale={localeMap[i18n.language]} >
                    <DatePicker 
                      disableFuture
                      openTo="date"
                      format="dd/MM/yyyy"
                      label={t('form.birthday.label')}
                      views={["date", "month", "year" ]}
                      value={props.profileUser.birthday ? props.profileUser.birthday : selectedDate} 
                      disabled={dateDisabled}
                      onChange={props.profileUser.birthday ? ()=> {} : handleDateChange}
                      className={classes.birthday}
                      variant="inline"
                      inputVariant="outlined" />
                  </MuiPickersUtilsProvider>
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Paper>
                <b>{t("profile.contact")}</b>
                <TextField
                  id="mail"
                  label={t('form.mail.label')}
                  value={props.profileUser.mail}
                  fullWidth
                  disabled
                  margin="normal"
                  variant="outlined"
                />
              </Paper>
            </Grid>

            <Grid item xs={12}>
              
              <Paper>
                <b>{t('profile.values')}</b><br/><br/>
                <Select 
                  name="keywordInput"
                  options={keywordsOptions} 
                  placeholder={t('profile.keywords')} 
                  className={classes.keywordsFilter}
                  //isMulti
                  value={selectedKeyword}
                  onChange={handleKeywordsSelect}
                  multi={true}
                />
                {chipsKw}
              </Paper>

            </Grid>
            {
             (!props.profileUser.facebookId || props.profileUser.facebookId === "") &&
              <Grid item xs={12}>
              <Paper>
                <b>{t('profile.updatePassword')}</b>
                <TextField
                    id="password"
                    label={t('profile.currentPassword')}
                    value={currentPassword}
                    onChange={handleChangeCurrentPassword}
                    fullWidth
                    margin="normal"

                    type="password"
                    helperText={
                      (
                        currentPassword !=="" 
                        || newPassword !==""
                      ) 
                      && !regexPassword.test(currentPassword) 
                      ? t('form.password.helper')
                      : ''
                    }
                    error={
                      (currentPassword !=="" || newPassword !=="") 
                      && !regexPassword.test(currentPassword)
                    }
                    variant="outlined"
                  />

                <TextField
                  id="new-password"
                    label={t('profile.newPassword')}
                    value={newPassword}
                    onChange={handleChangeNewPassword}
                    fullWidth
                    margin="normal"

                    type="password"
                    helperText={
                      (
                        currentPassword !=="" 
                        || newPassword !==""
                      ) 
                      && !regexPassword.test(newPassword) 
                      ? t('form.password.helper')
                      : ''
                    }
                    
                    error={
                      (currentPassword !=="" || newPassword !=="") 
                      && !regexPassword.test(newPassword)
                    }
                    variant="outlined"
                />
              </Paper>
            </Grid>
            }
          </Grid>
          <Grid item xs={12} 
            className={classes.saveButton}>
            <Button
              variant="contained"
              color="primary"
              size="large"
              id="saveButton"
              onClick={handleUpdateAttempt}
              disabled={
                !parametersUpdated
              }
              ref={myRef}
              startIcon={<SaveIcon />}>
              {t('profile.button')}
            </Button>
          </Grid>
        </Grid>

        
      </div>
    );
  
}