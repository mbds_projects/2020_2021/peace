import React, { useState, useEffect } from 'react';

import routerHistory from '../../shared/router-history/router-history';

import UserProfilePage from './UserProfilePage';
import LoadingPage from '../Loading/LoadingPage';

import { keywordsGql } from '../../graphQL/keyword'
import { uploadPictureGql, updatePasswordGql, updateUserGql } from '../../graphQL/user'

import { achievementsGql } from '../../graphQL/achievement'

import { useDispatch, useSelector } from "react-redux";

import { toast } from 'react-toastify';

import { setUser } from '../../shared/store/actions/authentication.actions';

import { useTranslation } from 'react-i18next';

import { toastError } from '../../shared/utils/error'

export default function UserProfilePageContainer() {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const loggedInUser = useSelector(state => state.authentication.currentUser);

  const keywordsInternal = useSelector(state => state.keyword.keywords);

  const [profileUser, setProfileUser] = useState(null);
  const [keywords, setKeywords] = useState([]);
  const [isRequiredNewPassword, setIsRequiredNewPassword] = useState(false);
  const [isRequiredPassword, setIsRequiredPassword] = useState(false);
  const [progress, setProgress] = useState(0);
  const [textCompletion, setTextCompletion] = useState("")
  const [achievements, setAchievements] = useState([])

  const handleIncorrectIdParameter = () => {
    routerHistory.replace('/404');
  }

  useEffect(() => {
    let didCancel = false

    async function retrieveKeywords() {
      let keywords;
      if(keywordsInternal.length > 0) {
        keywords = keywordsInternal;
      } else {
        keywords = await keywordsGql();
      }
      !didCancel && setKeywords(keywords)
    }

    Promise.all([retrieveKeywords()])

    return () => { didCancel = true }
  }, [])

  const uploadPicture = async (file) => {
    try {
      let path = await uploadPictureGql({file: file});
      if(path) {
        let user = profileUser;
        user.profilePictureUrl = path;
        setProfileUser(user);
        dispatch(setUser(user));
      } else {
        toast.error(t('error.somethingWrongAppends'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    } catch(e) {
      toastError(e.message,t);
    }
  }

  const updatePassword = (password, newPassword) => {
    let isRequiredPassword = password !== '';

    let isRequiredNewPassword = newPassword !== '';
    if(isRequiredPassword && isRequiredNewPassword) {
      updatePasswordGql(
        {
          input: 
          {
            password: password, newPassword: newPassword
          }
      }).then(bool => {
        if(bool) {
          password = "";
          newPassword = "";
        } else{
          toast.error(t('error.somethingWrongAppends'), {
            position: toast.POSITION.TOP_RIGHT
          });
        }
      }).catch(err => {
        toastError(err.message,t);
    });
    } else {
      if(!isRequiredPassword) {
        setIsRequiredPassword(true);
      }
      if(!isRequiredNewPassword) {
        setIsRequiredNewPassword(true);
      }

    }
    

  }

  const updateUser = async (input) => {
    updateUserGql(input)
    .then(user => {
      if(user) {
        setProfileUser(user);
        dispatch(setUser(user))
      } else {
        toast.error(t('error.somethingWrongAppends'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    })
    .catch(err => {
      toastError(err.message,t);
    })
    
  }

  const calculProgression = () => {
    let completion = 0;
    let textCompletion = "";
    if(!!loggedInUser.keywords && !!loggedInUser.keywords.length > 0) {
      if(loggedInUser.keywords.length >= 5) { completion += 50 }
      else { 
        completion += loggedInUser.keywords.length * 10;
        textCompletion = `${t('profile.nextStep.text')} ${5 - loggedInUser.keywords.length} ${t('profile.nextStep.valuesRemaining')}`
      } 
    } else { textCompletion= `${t('profile.nextStep.text')} 5 ${t('profile.nextStep.valuesRemaining')}`}
    if(!!loggedInUser.profilePictureUrl) {completion += 25;}  else { textCompletion = `${t('profile.nextStep.text')} ${t('profile.nextStep.profilePic')}`}
    if(!!loggedInUser.birthday) {completion += 25;} else { textCompletion = `${t('profile.nextStep.text')} ${t('profile.nextStep.birthday')}`}
    return { completion, textCompletion}
  }

  useEffect(() => {
    if (!loggedInUser) {
      handleIncorrectIdParameter();
    } else {
      setProfileUser(loggedInUser);
      if(!(loggedInUser.achievements && loggedInUser.achievements.find(a => a.type === "complete_profile" && a.activated))) {
        let progressCalcul = calculProgression();
        setProgress(progressCalcul.completion);
        setTextCompletion(progressCalcul.textCompletion);
      }
    }
  }, [loggedInUser]);

  useEffect(() => {
    let didCancel = false

    async function handleUser() {
      !didCancel && setProfileUser(loggedInUser);
      if(!(loggedInUser.achievements && loggedInUser.achievements.find(a => a.type === "complete_profile" && a.activated))) {
        let progressCalcul = calculProgression();
        !didCancel && setProgress(progressCalcul.completion);
        !didCancel && setTextCompletion(progressCalcul.textCompletion);
      }
      let achievementsTmp = await achievementsGql();
      !didCancel && setAchievements(achievementsTmp)
    }

    if (!loggedInUser) {
      handleIncorrectIdParameter();
    } else {
      handleUser()
    }

    return () => { didCancel = true }
  }, [loggedInUser]);

  if (!profileUser) {
    return <LoadingPage />
  }

  return (<UserProfilePage
          profileUser={profileUser}
          uploadPicture={uploadPicture}
          updatePassword={updatePassword}
          isRequiredPassword={isRequiredPassword}
          isRequiredNewPassword={isRequiredNewPassword}
          keywords={keywords}
          completion={progress}
          nextStepCompletion={textCompletion}
          achievements={achievements}
          updateUser={updateUser}
          />);
}
