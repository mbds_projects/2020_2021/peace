import * as React from 'react';

import CommentItem from '../../components/CommentItem/CommentItem'
import Button from '@material-ui/core/Button';

import UserCard from '../../components/UserCard/UserCard';
import { NavLink } from 'react-router-dom';

import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const useStyles = makeStyles({
  root: {

  },
  userReported: {
    "& UserCard": {
      width: "10%"
    }
  }
});

export default function AdminDashboardUserPage(props) {
  const classes = useStyles();
  const { t, i18n } = useTranslation();

  return (
      <div className={classes.root}>
        <IconButton onClick={props.goBack}>
          <ArrowBackIcon />
        </IconButton>
        <h4>{t('adminDashboard.reportedComments')}</h4>
        {props.commentsReported.length > 0 && props.commentsReported.map((el) => {
                return (
                  <div key={el.id}>
                    <CommentItem
                    comment={el}
                     />
                     <NavLink to={`/vendors/${el.vendor.id}?name=${el.vendor.name}`}>{t('adminDashboard.linkToVendor')}</NavLink>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => {props.deleteCommentAndReportUser(el.id, el.postedBy.id)}}
                    >
                      {t('adminDashboard.deleteComAndReportUser')}
                    </Button>
                  </div>
                );
              })
        }
      {
        props.commentsReported.length === 0 &&
        <div>{t('adminDashboard.noCommentsReported')}</div>
      }
      <h4>{t('adminDashboard.usersReportedMoreThan3')}</h4>
      {props.usersReported && props.usersReported.length > 0 && props.usersReported.map((el) => {
                return (
                  <div className={classes.userReported} key={el.id}>
                    <UserCard user={el} />
                    <div>{t('adminDashboard.reported')} {el.reported} {t('adminDashboard.time')}</div>
                    
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => {props.blockUser(el.id)}}
                    >
                      {t('adminDashboard.blockUser')}
                    </Button>
                  </div>
                );
              })
        }
      {
        (!props.usersReported || props.usersReported.length === 0) &&
        <div>{t('adminDashboard.noUsersReported')}</div>
      }
      <h4>{t('adminDashboard.usersBlocked')}</h4>
      {props.usersBlocked.length > 0 && props.usersBlocked.map((el) => {
          let date = new Date(el.blockedSince);
          let month = date.toLocaleString(i18n.language, { month: 'long' });
                return (
                  <div className={classes.userReported} key={el.id}>
                    <UserCard user={el} />
                    <div>{t('adminDashboard.blockedSince')}{date.getDate()} {month} {date.getFullYear()}</div>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => {props.unblockUser(el.id)}}
                    >
                      {t('adminDashboard.unblockUser')}
                    </Button>
                  </div>
                );
              })
        }
      {
        props.usersBlocked.length === 0 &&
        <div>{t('adminDashboard.noUsersBlocked')}</div>
      }
      </div>

  );
}