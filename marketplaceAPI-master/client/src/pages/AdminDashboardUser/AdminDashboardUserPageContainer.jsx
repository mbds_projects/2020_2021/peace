import React, { useState, useEffect } from 'react';

import AdminDashboardUserPage from './AdminDashboardUserPage';
import LoadingPage from '../Loading/LoadingPage';

import { reportUserGql, usersReportedGql, blockUserGql, usersBlockedGql, unblockUserGql } from '../../graphQL/user'

import { commentsReportedGql, deleteCommentGql } from '../../graphQL/comment'

import { toast } from 'react-toastify';

import { useSelector } from "react-redux";

import routerHistory from '../../shared/router-history/router-history';

import { useTranslation } from 'react-i18next';
export default function AdminDashboardBrandUserPageContainer() {
  const { t } = useTranslation();
  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  
  const [commentsReported, setCommentsReported] = useState([]);
  const [usersReported, setUsersReported] = useState([]);
  const [usersBlocked, setUsersBlocked] = useState([]);

  const goBack = () => {
    routerHistory.push('/admin/');
  }

  const deleteCommentAndReportUser = async (commentId, userId) => {
    try {
      let deleteComment = await deleteCommentGql({commentId: commentId});
      if(deleteComment) {
        toast.success(t('adminDashboard.toast.commentDeleted'), {
          position: toast.POSITION.TOP_RIGHT
        });
        let reportUser = await reportUserGql({userId: userId});
        if(reportUser) {
          updateData();
          toast.success(t('adminDashboard.toast.userReported'), {
            position: toast.POSITION.TOP_RIGHT
          });
        } else {
          toast.error(t('adminDashboard.toast.userReportError'), {
            position: toast.POSITION.TOP_RIGHT
          });
        }
        return reportUser;
      } else {
        toast.error(t('adminDashboard.toast.commentDeletionError'), {
          position: toast.POSITION.TOP_RIGHT
        });
        return false;
      }
    } catch(e) {
      toast.error(t('adminDashboard.toast.commentDeletionError'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }

  }

  const blockUser = async(userId) => {
    try {
      let blockUser = await blockUserGql({userId: userId})
      if(blockUser) {
        updateData();
        toast.success(t('adminDashboard.toast.userBlocked'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('adminDashboard.toast.userBlockError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return blockUser;
    } catch(e) {
      toast.error(t('adminDashboard.toast.userBlockError'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }

  }

  const unblockUser = async(userId) => {
    try {
      let unblockUser = await unblockUserGql({userId: userId})
      if(unblockUser) {
        updateData();
        toast.success(t('adminDashboard.toast.userUnblocked'), {
          position: toast.POSITION.TOP_RIGHT
        });
        
      } else {
        toast.error(t('adminDashboard.toast.commuserUnblockErrorentDeleted'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return unblockUser;
    } catch(e) {
      toast.error(t('adminDashboard.toast.commuserUnblockErrorentDeleted'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }

  }

  const updateData = async() => {
    let commentsReported = await commentsReportedGql();
    let usersReported = await usersReportedGql({minReport: 3})
    let usersBlocked = await usersBlockedGql();
    setCommentsReported(commentsReported);
    setUsersReported(usersReported);
    setUsersBlocked(usersBlocked);
  }

  useEffect(() => {
    updateData();
  }, []);

  if (!loggedInUser) {
    return <LoadingPage />
  }

  return (<AdminDashboardUserPage
    commentsReported={commentsReported}
    usersReported={usersReported}
    usersBlocked={usersBlocked}
    deleteCommentAndReportUser={deleteCommentAndReportUser}
    blockUser={blockUser}
    unblockUser={unblockUser}
    goBack={goBack}
  />);
}
