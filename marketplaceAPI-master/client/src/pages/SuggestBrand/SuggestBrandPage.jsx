import React, { useState } from 'react';
import * as Autosuggest from 'react-autosuggest';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Select from 'react-select';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import VendorCard from '../../components/VendorCard/VendorCard';
import AddCommentItem from "../../components/AddCommentItem/AddCommentItem";
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import ClearIcon from '@material-ui/icons/Clear';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import { useTranslation } from 'react-i18next';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import DialogShouldBeConnected from './../../components/DialogShouldBeConnected/DialogShouldBeConnected'
import { Trans } from 'react-i18next';
import ReactGA from 'react-ga';

ReactGA.pageview('/suggestBrand');

const match = require('autosuggest-highlight/match');
const parse = require('autosuggest-highlight/parse');

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            color: "black",
            width: "99%",
            height: "100%",
            backgroundImage: `url("/assets/images/suggestBrandCover.png")`,
            backgroundPosition: "center",
            backgroundSize: "cover",
            "& input.react-autosuggest__input": {
                width: "100%"
            },
            "& .react-autosuggest__suggestions-list, .react-autosuggest__suggestions-list li": {
                margin: 0,
                padding: 0,
                listStyleType: "none"
            },
            "& label.MuiFormLabel-root": {
                padding: "5px"
            }
        },
        title: {
            fontWeight: "bolder"
        },
        grid: {
            height: "100%"
        },
        suggestionLight: {
            fontWeight: "normal"
        },
        suggestionStrong: {
            fontWeight: "bolder",
            color: theme.colors.strawberry
        },
        searchBarInput: {
            width: "90%",
            "& input": {
                padding: "10px"
            },
            color: "black"
        },
        marginFilter: {
            marginBottom: "10px"
        },
        addSuggestionButtonValid: {
            backgroundColor: theme.colors.strawberry,
            color: "white",
            margin: "20px 0px"
        },
        addSuggestionButton: {
            margin: "20px 0px"
        },
        paperInfo: {
            margin: "20px",
            padding: "20px"
        },
        favorite: {
            position: "relative",
            top: "0px",
            color: theme.colors.strawberry
        },
        textarea: {
            marginTop: "0px",
            width: "100%",
            padding: "10px"
        },
        paperForm: {
            padding: "25px",
            marginBottom: "20px",
            marginTop: "20px"
        },
        paperFormMobile: {
            padding: "15px",
            marginBottom: "20px",
            marginTop: "20px",
            backgroundColor: "rgba(255,255,255, 0.8)"
        },
        site: {
            "& label.MuiFormLabel-root": {
                padding: "0px",
                color: "hsl(0,0%,50%)"
            },
            marginBottom: "10px",
            marginTop: "0px"
        },
        favoriteText: {
            color: theme.colors.strawberry,
        },
        littleStar: {
            fontStyle: "italic",
            fontSize: "14px",
            color: "red",
            verticalAlign: "top",
            opacity: 0.8
        },
        autoSuggest: {
            marginBottom: "10px"
        }
    }),
);

const getSuggestions = (value, suggestions) => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0 ? [] : suggestions.filter(lang =>
        lang.name.toLowerCase().slice(0, inputLength) === inputValue
    );
};


export default function SuggestBrandPage(props) {
    const classes = useStyles();
    const { t, i18n } = useTranslation();
    const matchesMobile = useMediaQuery('(max-width:768px)');

    const [value, setValue] = useState('');
    const [message, setMessage] = useState('');
    const [suggestions, setSuggestions] = useState([]);
    const [selectedCategories, setSelectedCategories] = useState([]);
    const [selectedKeywords, setSelectedKeywords] = useState([]);
    const [displayVendor, setDisplayVendor] = useState(false);
    const [site, setSite] = useState('');

    const [vendor, setVendor] = useState(null);
    const [isAlreadySuggested, setIsAlreadySuggested] = useState(false);
    const [brandAddedOccurrence, setBrandAddedOccurrence] = useState(-1);

    const [openDialogShouldBeConnected, setOpenDialogShouldBeConnected] = useState(false);

    function renderSuggestion(suggestion, { query, isHighlighted }) {
        const matches = match(suggestion.name, query);
        const parts = parse(suggestion.name, matches);
        return (
            <MenuItem selected={isHighlighted} component="div">
                <div>
                    {parts.map((part, index) =>
                        part.highlight ? (
                            <span key={String(index)} className={classes.suggestionStrong}>
                                {part.text}
                            </span>
                        ) : (
                                <span key={String(index)} className={classes.suggestionLight}>
                                    {part.text}
                                </span>
                            ),
                    )}
                </div>
            </MenuItem>
        );
    }

    const onChange = (event, { newValue }) => {
        setValue(newValue);
    };

    const onSuggestionsFetchRequested = ({ value }) => {
        setSuggestions(getSuggestions(value, props.suggestions));
    };

    const onSuggestionsClearRequested = () => {
        setSuggestions([]);
    };

    const updateVendor = (suggestion) => {
        async function getVendor() {
            let vendor = await props.getVendor(suggestion)
            setVendor(vendor);
            setDisplayVendor(true);
            ReactGA.event({
                category: 'Suggestion',
                action: 'Click on vendor already referenced',
                label: `Suggestion ${suggestion.name}`
            });
        }

        getVendor();
    };


    const getSuggestionValue = (suggestion) => {
        if (suggestion.added) {
            updateVendor(suggestion);
        } else {
            let brand = props.getBrand(suggestion);
            if (brand) {
                if (props.isAlreadyAdded(brand)) {
                    setIsAlreadySuggested(true);
                    ReactGA.event({
                        category: 'Suggestion',
                        action: 'Click on brand already suggested',
                        label: `Suggestion ${suggestion.name}`
                    });
                } else {
                    ReactGA.event({
                        category: 'Suggestion',
                        action: 'New brand suggested',
                        label: `Suggestion ${suggestion.name}`
                    });
                    let categories = brand.categories.map((el) => {
                        return props.categories.find((c) => c.id === el);
                    })
                    let selectedCategories = [];
                    for (const element of categories) {
                        if (element) {
                            selectedCategories.push(
                                {
                                    value: element.id,
                                    label: element.name
                                })
                        }
                    }

                    let keywords = brand.keywords.map((el) => {
                        return props.keywords.find((c) => c.id === el);
                    })
                    let selectedKeywords = [];
                    for (const element of keywords) {
                        if (element) {
                            selectedKeywords.push(
                                {
                                    value: element.id,
                                    label: element.name
                                })
                        }
                    }
                    setSelectedCategories(selectedCategories)
                    setSelectedKeywords(selectedKeywords)
                }
            }
        }
        return suggestion.name;
    }

    const renderInputComponent = (inputProps) => (
        <Paper>
            <InputBase {...inputProps} className={classes.searchBarInput} endAdornment={<span className={classes.littleStar}>{t('suggest.required')}</span>} />
            {
                value !== "" &&
                <IconButton onClick={revertSearch}>
                    <ClearIcon />
                </IconButton>
            }
        </Paper>
    );

    const revertSearch = () => {
        setValue('');
        setSuggestions([]);
        setDisplayVendor(false);
        setVendor(null);
        setMessage('');
        setSelectedCategories([]);
        setSelectedKeywords([]);
        setIsAlreadySuggested(false);
        setBrandAddedOccurrence(-1);
    }

    const addSuggestion = async () => {
        let suggestion = {
            name: value,
            message: message,
            site: site,
            categories: selectedCategories.map((e) => e.value),
            keywords: selectedKeywords.map((e) => e.value)
        }
        let brandAddedOccurrence = await props.addBrand(suggestion);
        setBrandAddedOccurrence(brandAddedOccurrence);
    }

    const inputProps = {
        placeholder: `${t('suggest.searchPlaceholder')} *`,
        value,
        onChange: onChange
    };

    let categoriesOptions = props.categories.map((category) => {
        let catName = category.lang.find((l) => l.locale === i18n.language).value;
        return {
            value: category.id,
            label: catName
        }
    }
    );

    let keywordsOptions = props.keywords.map((keyword) => {
        let keyName = keyword.lang.find((l) => l.locale === i18n.language).value;
        return {
            value: keyword.id,
            label: keyName
        }
    });

    let checkLogin = () => {
        if (!props.loggedInUser) {
            setOpenDialogShouldBeConnected(true);
        }
    }

    return (
        <div className={classes.root} onClick={checkLogin}>
            {openDialogShouldBeConnected && <DialogShouldBeConnected open={openDialogShouldBeConnected} close={() => setOpenDialogShouldBeConnected(false)} />}
            <Grid
                container
                spacing={0}
                direction="row"
                alignItems="center"
                justify="center"
                className={classes.grid}
            >
                <Grid item sm={6} xs={12}>
                    <Paper className={matchesMobile ? classes.paperFormMobile : classes.paperForm}>
                        <h1 className={classes.title}><Trans i18nKey="suggest.title"
                            components={[<span className={classes.favoriteText}></span>]} /></h1>
                        <br />
                        <form noValidate autoComplete="off">
                            <Autosuggest
                                suggestions={suggestions}
                                onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                                onSuggestionsClearRequested={onSuggestionsClearRequested}
                                getSuggestionValue={getSuggestionValue}
                                renderSuggestion={renderSuggestion}
                                inputProps={inputProps}
                                renderInputComponent={renderInputComponent}
                                renderSuggestionsContainer={options => (
                                    <Paper {...options.containerProps} square className={classes.autoSuggest}>
                                        {options.children}
                                    </Paper>
                                )}
                            />

                            {
                                displayVendor && vendor &&
                                <div className="vendorSection">
                                    <VendorCard
                                        vendor={vendor}
                                    />
                                </div>
                            }
                            {
                                isAlreadySuggested &&
                                <Paper className={classes.paperInfo}>
                                    {t('suggest.alreadySuggested')}
                                    <br />
                                    {t('suggest.suggestOther')}
                                </Paper>
                            }
                            {
                                brandAddedOccurrence > 0 &&
                                <Paper className={classes.paperInfo}>
                                    <h3>{t('suggest.newSuggested')}  {brandAddedOccurrence === 1 ? t('suggest.first') : brandAddedOccurrence + t('suggest.th')} !</h3>
                                    <p>{t('suggest.suggestOther')} </p>
                                </Paper>
                            }
                            {
                                !displayVendor && !isAlreadySuggested && brandAddedOccurrence === -1 &&
                                <div>

                                    <TextField
                                        margin="dense"
                                        id="site"
                                        label={t('suggest.site')}
                                        size="small"
                                        fullWidth
                                        type="text"
                                        value={site}
                                        variant="outlined"
                                        onChange={(event) => setSite(event.target.value)}
                                        className={classes.site}
                                    />

                                    <Select
                                        name="categoryInput"
                                        options={categoriesOptions}
                                        placeholder="Catégories"
                                        className={classes.marginFilter}
                                        isMulti
                                        value={selectedCategories}
                                        onChange={(categories) => setSelectedCategories(categories)}
                                        multi={true}
                                    />

                                    <TextareaAutosize
                                        rows={matchesMobile ? 2 : 4}
                                        onChange={(event) => setMessage(event.target.value)}
                                        placeholder={t('suggest.comment')}
                                        className={classes.textarea}
                                        value={message}
                                    />
                                    <Button disabled={value === ''}
                                        className={value !== '' ? classes.addSuggestionButtonValid : classes.addSuggestionButton}
                                        variant="contained"
                                        onClick={addSuggestion}>
                                        {t('suggest.button')}
                                    </Button>
                                </div>
                            }
                        </form>
                    </Paper>
                </Grid>
            </Grid>
        </div>
    );
}
