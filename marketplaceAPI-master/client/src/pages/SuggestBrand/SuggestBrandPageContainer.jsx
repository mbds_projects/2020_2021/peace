import React, { useState, useEffect } from 'react';
import SuggestBrandPage from './SuggestBrandPage';

import { suggestedBrandsGql, createSuggestedBrandGql } from '../../graphQL/suggestedBrand';
import { vendorsMinimalGql, vendorLimitedGql } from '../../graphQL/vendor';
import { setUser } from '../../shared/store/actions/authentication.actions';

import { useTranslation } from 'react-i18next';

import { useDispatch, useSelector } from "react-redux";

import { toastError } from '../../shared/utils/error'

import { userGql } from '../../graphQL/user'

export default function SuggestBrandPageContainer() {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const [suggestions, setSuggestions] = useState([]);
  const [suggestedBrands, setSuggestedBrands] = useState([]);
  
  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  const categories = useSelector((state) => state.category.categories);
  const keywords = useSelector((state) => state.keyword.keywords);

  useEffect(() => {
    async function initData() {
      let suggestedBrandsTmp = await suggestedBrandsGql();
      setSuggestedBrands(suggestedBrandsTmp)

      let vendors = await vendorsMinimalGql();
      if(suggestions.length === 0 || suggestions.length < suggestedBrandsTmp.length + vendors.length) {
        let suggestions = [];
        
        if(suggestedBrandsTmp.length > 0) {
          let suggestedBrandsTmpNotAdded = suggestedBrandsTmp.filter((brand) => brand.vendor === null);
          let brands = suggestedBrandsTmpNotAdded.map((brand) => {
            return {
              value: brand.id,
              added: false,
              name: brand.name
            }
          })
          suggestions = suggestions.concat(brands);
        }
        if(vendors.length > 0) {
          let vendorsTmp = vendors.map((vendor) => {
            return {
              value: vendor.id,
              added: true,
              name: vendor.name
            }
          });
          suggestions= suggestions.concat(vendorsTmp);
        }
    
        if(suggestions.length > 0) {
          setSuggestions(suggestions)
        }
      }
    }

    initData();

  }, []);

  const getVendor = async (suggestion) => {
    let vendor = await vendorLimitedGql({id: suggestion.value})
    return vendor ? vendor : null;
  }

  const getBrand = (suggestion) => {
    let brand = suggestedBrands.find((brand) => brand.id === suggestion.value);
    return brand ? brand : null;
  }

  const isAlreadyAdded = (brand) => {
    return brand.suggestedBy && brand.suggestedBy.length > 0 && !!brand.suggestedBy.find(element => element.user === loggedInUser.id)
  }

  const addBrand = async(suggestion) => {
    return createSuggestedBrandGql({input: suggestion})
    .then(suggestedBrandCreated => {
        let indexBrand = suggestedBrands.findIndex((brand) => brand.id === suggestedBrandCreated.id);
        let suggestedBrandsTmp = suggestedBrands;
        if(indexBrand >= 0) {
          suggestedBrandsTmp[indexBrand] = suggestedBrandCreated;
        } else {
          suggestedBrandsTmp.push(suggestedBrandCreated);
        }
        setSuggestedBrands(suggestedBrandsTmp);
        let brand = {
            value: suggestedBrandCreated.id,
            added: false,
            name: suggestedBrandCreated.name
          };
        
          if(indexBrand < 0) {

            let suggestionsTmp = [...suggestions]
            suggestionsTmp.push(brand);
            setSuggestions(suggestionsTmp);
          }

          userGql().then(user => dispatch(setUser(user)));

          window.azameoTagEvent = {
            name : "address_sharing",
            category: "address",
            ref : "address" + new Date().getTime(),
            type : "lead"
          };
          if(window.azameoTag) window.azameoTag.Conversion();

        return suggestedBrandCreated.occurrence;
      }) 
    .catch((e) => {
      toastError(e.message, t)
      return 0;
    });
  }

  let propsChild = { suggestions, loggedInUser, categories, keywords, getVendor, getBrand, addBrand, isAlreadyAdded }

  return (<SuggestBrandPage {...propsChild} />);
}