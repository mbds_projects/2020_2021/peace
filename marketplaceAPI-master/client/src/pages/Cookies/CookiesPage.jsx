import React, { useState, useEffect } from 'react';

import { makeStyles, createStyles, withStyles } from '@material-ui/core/styles';
import { Trans } from 'react-i18next';
import ReactGA from 'react-ga';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import { addMonths } from 'date-fns'
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useTranslation } from 'react-i18next';

import { useCookies } from 'react-cookie';

ReactGA.pageview('/cookies');

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      margin: "auto",
      padding: "20px"
    },
    head: {
      textAlign: 'center'
    },
    icon: {
      width: "25px",
      marginRight: "5px"
    },
    colorAlmond: {
      backgroundColor: `${theme.colors.almond} !important`,
      "& hover": {
        backgroundColor: `${theme.colors.almond} !important`,
      },
      color: "white"
    },
  })
);


const AlmondSwitch = withStyles((theme) =>
  createStyles({
    switchBase: {
        color: theme.colors.almond,
        '&$checked': {
          color: theme.colors.almond,
        },
        '&$checked + $track': {
          backgroundColor: theme.colors.almond,
        },
      },
      checked: {},
      track: {},
  }),
)(Switch);

export default function CookiesPage() {
  let classes = useStyles();
  const [necessary, setNecessary] = useState(true);
  const [statistics, setStatistics] = useState(true);
  const [cookies, setCookie] = useCookies(['cookies_necessary', 'cookies_statistics']);
  const [areYouSure, setAreYouSure] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const { t } = useTranslation();

  const disabledValidation = () => {
    setDisabled((necessary === (cookies['cookies_necessary'] === "true")) && (statistics === (cookies['cookies_statistics'] === "true")))
  }

  useEffect(() => {
    if (cookies['cookies_statistics']) {
      setStatistics((cookies['cookies_statistics'] === "true"));
    }
    if (cookies['cookies_necessary']) {
      setNecessary((cookies['cookies_necessary'] === "true"));
    }
    disabledValidation();
  }, [cookies])

  useEffect(() => {
    disabledValidation();
  }, [statistics, necessary])

  const setCookies = () => {
    setAreYouSure(false);
    let newDate = addMonths(new Date(), 13);
    setCookie('cookies_statistics', statistics, { path: '/', expires: newDate });
    setCookie('cookies_necessary', necessary, { path: '/', expires: newDate });
    disabledValidation();
  }

  const validSelection = () => {
    if (!statistics || !necessary) {
      setAreYouSure(true);
    } else {
      setCookies();
    }
  }

  return (
    <div className={classes.root}>

      <Dialog open={areYouSure}>
        <DialogTitle>
          <img alt="" className={classes.icon} src="/assets/images/icons/cookie.png" />
          {t('dialog.cookies.areYouSure.title')}
        </DialogTitle>
        <DialogContent>
          <Trans i18nKey="dialog.cookies.areYouSure.info"
            components={[]} />
            <br />
          <div style={{ textAlign: "right" }}>
            <Button  onClick={setCookies}>{t('dialog.cookies.areYouSure.yes')}</Button>
            <Button variant="contained" className={classes.colorAlmond} onClick={() => setAreYouSure(false)}>{t('dialog.cookies.areYouSure.no')}</Button>
          </div>
        </DialogContent>
      </Dialog>
      <Trans i18nKey="cookies.info"
        components={[<h2></h2>, <br />]} />
      <Grid container>
        <Grid container item xs={12} sm={6} direction="column">
          <FormControlLabel
            control={<AlmondSwitch color="primary" checked={necessary} onChange={(event) => setNecessary(event.target.checked)} value={necessary} />}
            label={t('cookies.necessary.title')}
          />
          {t('cookies.necessary.info')}
        </Grid>

        <Grid container item xs={12} sm={6} direction="column">
          <FormControlLabel
            control={<AlmondSwitch color="primary" checked={statistics} onChange={(event) => setStatistics(event.target.checked)} value={statistics} />}
            label={t('cookies.statistics.title')}
          />
          {t('cookies.statistics.info')}
        </Grid>

      </Grid>
      <br />
      <div style={{ textAlign: "center" }}><Button className={disabled ? '' : classes.colorAlmond} disabled={disabled} onClick={validSelection} variant="contained" color="primary">Valider</Button></div>
      <br />
      <Trans i18nKey="cnil"
        components={[<a href="http://www.cnil.fr/vos-obligations/sites-web-cookies-et-autres-traceurs/que-dit-la-loi/" target="_blank"></a>]} />
    </div>

  )
}