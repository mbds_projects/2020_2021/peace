import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { fade, makeStyles, useTheme, createStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import FilterListIcon from '@material-ui/icons/FilterList';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
//import SearchIcon from '@material-ui/icons/Search';

import RewardCard from '../../components/RewardCard/RewardCard';
import { compareAsc } from 'date-fns'
import Badge from '@material-ui/core/Badge';
import { Trans } from 'react-i18next';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import Loading from '../Loading/LoadingPage';

import Grid from '@material-ui/core/Grid';
//import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import Chip from '@material-ui/core/Chip';

import * as _ from 'lodash';

import { NavLink } from 'react-router-dom';

import useMediaQuery from '@material-ui/core/useMediaQuery';

import { useTranslation } from 'react-i18next';

import Avatar from '@material-ui/core/Avatar';

import DialogShouldBeConnected from './../../components/DialogShouldBeConnected/DialogShouldBeConnected'
import ReactGA from 'react-ga';
import SidebarFilter from '../../components/SidebarFilter/SidebarFilter'

ReactGA.pageview('/user/rewards');

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: 'flex'
    },
    drawer: (props) => ({
      width: props.drawerWidth,
      flexShrink: 0,
    }),
    drawerPaper: (props) => ({
      width: props.drawerWidthPaper,
    }),
    header: {
      marginTop: "20px",
      marginBottom: "20px"
    },
    drawerHeader: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
      justifyContent: 'flex-end',
    },
    content: (props) => ({
      width: "45%",
      flexGrow: 1,
      padding: theme.spacing(3),
      paddingTop: '0px',
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: -`calc(${props.content}/2)`,
    }),
    contentShift: {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    },
    chip: {
      margin: '5px'
    },
    filterCat: {
      marginRight: '20px'
    },
    noCards: {
      width: "100%"
    },
    emptyState: {
      textAlign: "center",
      "& img": {
        width: "50px"
      },
      "& a": {
        color: "grey"
      }
    },
    loadingState: {
      textAlign: "center"
    },
    colorSecondary: {
      color: "white !important",
      backgroundColor: `white !important`,
      "& hover": {
        backgroundColor: `${theme.colors.strawberry} !important`,
      },
      padding: "5px",

      width: '45px',
      height: '45px'
    },
    colorSecondarySelected: {
      color: "white !important",
      backgroundColor: `${theme.colors.strawberry} !important`,
      "& hover": {
        backgroundColor: `${theme.colors.strawberry} !important`,
      },
      //marginRight: "5px",
      padding: "5px",
      paddingRight: "4px",
      paddingLeft: "4px",
      width: "44px",
      paddingTop: "4px",
      height: "44px"
    },
    flagSmall: {
      width: '40px',
      height: '40px'
    },
    badge: {
      height: "30px",
      padding: "0px 10px",
      borderRadius: "50%",
      fontSize: "20px"
    },
    diamond: {
      width: '80px',
      height: '80px',
      backgroundColor: theme.colors.almond,
      //borderRadius: "50%"
    },
    achievementSmall: {
      width: '40px',
      height: '40px',
      "& img": {
        width: '30px',
        height: '30px',
      }
    },
    achievementSmallOpacity: {
      width: '40px',
      height: '40px',
      opacity: "0.3",
      "& img": {
        width: '30px',
        height: '30px',
      }
    },
    howToClick: {
      fontStyle: "italic",
      textDecoration: "underline",
      cursor: "pointer"
    },
    listButton: {
      padding: "0px"
    },
    slowActions: {
      fontStyle: "italic",
      textAlign: "right",
      fontSize: "12px"
    },
    diamondExplanation: {
      paddingLeft: "20px"
    }
  }),
);

export default function UserRewardPage(props) {
  const matches = useMediaQuery('(max-width:768px)');

  const { t, i18n } = useTranslation();

  const theme = useTheme();

  const [open, setOpen] = useState(false);
  const [cityChecked, setCityChecked] = React.useState(false);
  const [onlineVendors, setOnlineVendors] = React.useState(false);
  const [clickAndCollectVendors, setClickAndCollectVendors] = React.useState(false);
  const [openHowTo, setOpenHowTo] = useState(false);
  const [openDialogShouldBeConnected, setOpenDialogShouldBeConnected] = useState(false);

  let checkLogin = () => {
    if (!props.isLogged) {
      setOpenDialogShouldBeConnected(true);
    }
  }
  const howToOpen = () => {
    /*if (!props.isLogged) {
        setOpenDialogShouldBeConnected(true);
    } else {
      */setOpenHowTo(true);
    //}
  }

  const howToOpenClose = () => {
    setOpenHowTo(false);
  }

  let achievementsPoints = props.user && props.user.achievements && props.user.achievements.filter(achievement => achievement.activated).reduce((accumulator, currentValue) => accumulator + currentValue.value, 0) - props.user.rewards.length;
  if (achievementsPoints < 0) achievementsPoints = 0;
  let achievementsGroupedByType = _.groupBy(props.achievements.filter((achievement) => {
    let achievementUserIds = props.user && props.user.achievements ? props.user.achievements.filter(ac => ac.activated).map(ac => ac.id) : []
    return !achievementUserIds.includes(achievement.id) && achievement.activated
  }), 'type');

  let firstElementsByLevel = [];

  for (const achievementsGrouped in achievementsGroupedByType) {
    firstElementsByLevel.push(achievementsGroupedByType[`${achievementsGrouped}`].sort((b, a) => b.level - a.level)[0]);
  }

  const rewardCardsNotAlreadyUnlock = props.rewards && props.rewards.filter(reward =>
    !(props.user && props.user.rewards && props.user.rewards.length > 0
      && props.user.rewards.includes(reward.id))
    && !(reward.validity && compareAsc(new Date(reward.validity), new Date()) < 0)
    && reward.activated
  ).map(reward => {
    return <Grid item xs={12} sm={4} key={reward.id}>
      <RewardCard
      checkLogin={checkLogin}
        reward={reward}
        howToOpen={howToOpen}
        updateRewards={props.updateRewards}
      />
    </Grid>
  });

  const rewardCardsAlreadyUnlock = props.rewards && props.rewards.filter(reward =>
    props.user && props.user.rewards && props.user.rewards.length > 0
    && props.user.rewards.includes(reward.id)
    && !(reward.validity && compareAsc(new Date(reward.validity), new Date()) < 0)
    && reward.activated
  ).map(reward => {
    return <Grid item xs={12} sm={4} key={reward.id}>
      <RewardCard
      checkLogin={checkLogin}
        reward={reward}
        howToOpen={howToOpen}
        updateRewards={props.updateRewards}
      />
    </Grid>
  });

  let classes = useStyles({
    drawerWidth: matches ? "0px" : open ? "5%" : "0%",
    drawerWidthPaper: matches ? "100%" : "20%",
    content: matches ? "240px" : "20%"
  });

  useEffect(() => {
    setOpen(!matches);

    props.dispatchSideBar(!matches);
  }, [matches]);

  useEffect(() => {
    return () => {
      props.dispatchSideBar(false);
    }
  }, []);

  const handleDrawerOpen = () => {
    setOpen(true);
    props.dispatchSideBar(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
    props.dispatchSideBar(false);
  };

  const handleCityDelete = (event) => {
    setCityChecked(false);
    props.handlePositionCheck(false);
    props.dropCityFilter();
    ReactGA.event({
      category: 'Geolocation',
      action: `Disable display vendors around me`,
      label: 'Rewards'
    });
  }
  
  const handleOnlineVendorsDelete = (event) => {
    setOnlineVendors(false);
    props.handleOnlineVendorCheck(false);
    ReactGA.event({
      category: 'Filter',
      action: `Disable filter by online vendors`,
      label: 'Rewards'
    });
  };

  const handleClickAndCollectVendorsDelete = (event) => {
    setClickAndCollectVendors(false);
    props.handleClickAndCollectVendorCheck(false);
    ReactGA.event({
      category: 'Filter',
      action: `Disable filter by click and collect vendors`,
      label: 'Rewards'
    });
  };

  const constructFilterArray = (field) => {
    let fieldToSearch = props.filterUrlArgs[field];
    return fieldToSearch ? _.isArray(fieldToSearch) ? fieldToSearch : [fieldToSearch] : []
  }

  let filterKw = constructFilterArray('kw')
  let filterGd = constructFilterArray('gd');
  let filterCat = constructFilterArray('cat')

  const chipsKw = [];
  filterKw.forEach((el) => {
    let keyword = props.keywords.find((kw) => kw.id === el);
    if (keyword) {
      let keyName = keyword.lang.find((l) => l.locale === i18n.language).value;
      chipsKw.push(
        <Chip
          key={keyword.id}
          label={keyName}
          className={classes.chip}
          variant="outlined"
          onDelete={() => { props.handleToggleKeyword(keyword, false) }}
        />
      )
    }
  })

  const chipsCat = [];
  filterCat.forEach((el) => {
    let category = props.categories.find((cat) => cat.id === el);
    if (category) {
      let catName = category.lang.find((l) => l.locale === i18n.language).value;
      chipsCat.push(
        <Chip
          key={category.id}
          label={catName}
          className={classes.chip}
          variant="outlined"
          onDelete={() => { props.handleToggleCategory(category, false) }}
        />
      )
    }
  })

  const chipsGd = [];
  filterGd.forEach((el) => {
    let gender = props.genders.find((gd) => gd.id === el);
    if (gender) {
      let gdName = gender.lang.find((l) => l.locale === i18n.language).value;
      chipsGd.push(
        <Chip
          key={gender.id}
          label={gdName}
          className={classes.chip}
          variant="outlined"
          onDelete={() => { props.handleToggleGender(gender, false) }}
        />
      )
    }
  })

  let filterCity = constructFilterArray('city')
  const chipsCity = filterCity.map((city) => {

    return (
      <Chip
        key={city}
        label={city}
        className={classes.chip}
        variant="outlined"
        onDelete={() => { props.handleToggleCity(city, false) }}
      />
    )
  }
  );

  return (
    <div className={classes.root}>
      {openDialogShouldBeConnected && <DialogShouldBeConnected open={openDialogShouldBeConnected} close={() => setOpenDialogShouldBeConnected(false)} />}
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />
        <SidebarFilter from="Rewards" catCheckboxes={true} gdCheckboxes={true}
            setCityChecked={setCityChecked}
            cityChecked={cityChecked}
            setOnlineVendors={setOnlineVendors}
            setClickAndCollectVendors={setClickAndCollectVendors}
            onlineVendors={onlineVendors}
            clickAndCollectVendors={clickAndCollectVendors}
            {...props}/>
      </Drawer>
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
        {
          <Dialog open={openHowTo} onClose={howToOpenClose}>
            <DialogTitle>{t('reward.howToObtainDiamond')}</DialogTitle>
            <DialogContent>
              <DialogContentText>

                <List component="nav"
                  subheader={
                    <ListSubheader component="div">
                      {t('reward.nextActions')}
                    </ListSubheader>
                  }>
                  {
                    firstElementsByLevel.length > 0 && firstElementsByLevel.map((achievement) => {
                      if (achievement.link.includes("http")) {

                        return (<ListItem button
                          component="a"
                          target="_blank" rel="noopener noreferrer"
                          href={achievement.link}
                          key={achievement.subtype}
                          className={classes.listButton}>
                          <ListItemIcon>
                            <Avatar
                              className={classes.achievementSmallOpacity}
                              src={achievement.icon} />

                          </ListItemIcon>
                          <ListItemText primary={t(`achievements.${achievement.subtype}`)} />
                        </ListItem>)
                      } else {

                        if (props.user) {
                          return (<ListItem button

                            component={NavLink}
                            to={achievement.link}
                            key={achievement.subtype}
                            className={classes.listButton}>
                            <ListItemIcon>
                              <Avatar
                                className={classes.achievementSmallOpacity}
                                src={achievement.icon} />

                            </ListItemIcon>
                            <ListItemText primary={t(`achievements.${achievement.subtype}`)} />
                          </ListItem>)
                        } else {
                          return (<ListItem button
                            onClick={() => setOpenDialogShouldBeConnected(true)}
                            key={achievement.subtype}
                            className={classes.listButton}>
                            <ListItemIcon>
                              <Avatar
                                className={classes.achievementSmallOpacity}
                                src={achievement.icon} />
                            </ListItemIcon>
                            <ListItemText primary={t(`achievements.${achievement.subtype}`)} />
                          </ListItem>)
                        }
                      }

                    }
                    )
                  }
                  {
                    firstElementsByLevel.length <= 0 &&
                    <ListItem button={false}
                      className={classes.listButton}>
                      <ListItemText primary={t(`achievements.noNextActions`)} />
                    </ListItem>
                  }
                </List>
                {
                  props.user &&
                  <span>
                    <Divider />
                    <List component="nav"
                      subheader={
                        <ListSubheader component="div">
                          {t('reward.alreadyAchieved')}
                        </ListSubheader>
                      }>
                      {
                        props.user && props.user.achievements && props.user.achievements.filter(ac => ac.activated).sort((a, b) => {
                          return a.level - b.level;
                        }).map(achievement => (
                          <ListItem button
                            key={achievement.subtype}
                            className={classes.listButton}>
                            <ListItemIcon>
                              <Avatar
                                className={classes.achievementSmall}
                                src={achievement.icon} />

                            </ListItemIcon>
                            <ListItemText primary={t(`achievements.${achievement.subtype}`)} />
                          </ListItem>
                        )
                        )
                      }
                      {
                        props.user && props.user.achievements && props.user.achievements.filter(ac => ac.activated).length <= 0 &&
                        <ListItem button={false}
                          className={classes.listButton}>
                          <ListItemText primary={t(`achievements.noActionsAlready`)} />
                        </ListItem>
                      }
                    </List>
                  </span>
                }

                <br />
                <span className={classes.slowActions}>{t(`achievements.slowActions`)}</span>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={howToOpenClose} color="primary">
                {t('reward.close')}
              </Button>
            </DialogActions>
          </Dialog>
        }
        <Grid container direction="row" justify="center" className={classes.header}>
          <Grid xs={3} sm={1}>
            <Badge
              badgeContent={achievementsPoints}
              showZero
              color="primary"
              overlap="circle"
              classes={{ badge: classes.badge }}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}>
              <Avatar className={classes.diamond} src="/assets/images/icons/diamond.png" />
            </Badge>
          </Grid>
          <Grid xs={9} sm={7} className={classes.diamondExplanation}>
            <Trans i18nKey="reward.diamondExplanation"
              components={[<br />, <span onClick={howToOpen} className={classes.howToClick}></span>]} />
          </Grid>
        </Grid>

        <div className={classes.drawerHeader} >


          <Grid container direction="row" justify="flex-start" className={classes.filterCat}>


            <Grid item xs={12}>
              {
                !open &&

                <Fab size="small"
                  variant="extended"
                  onClick={handleDrawerOpen}>
                  <FilterListIcon />
                  {!matches && t('vendorList.filterButton')}
                </Fab>
              }
              {chipsKw}
              {chipsCat}
              {chipsGd}
              {chipsCity}
              {onlineVendors &&
                <Chip
                  key="onlineVendors"
                  label={t('vendorList.onlineShop')}
                  className={classes.chip}
                  variant="outlined"
                  onDelete={handleOnlineVendorsDelete}
                />
              }
              {clickAndCollectVendors &&
                <Chip
                  key="clickAndCollectVendors"
                  label={t('vendorList.clickAndCollect')}
                  className={classes.chip}
                  variant="outlined"
                  onDelete={handleClickAndCollectVendorsDelete}
                />
              }
              {cityChecked &&
                <Chip
                  key="aroundMe"
                  label={t('vendorList.aroundMe')}
                  className={classes.chip}
                  variant="outlined"
                  onDelete={handleCityDelete}
                />
              }
            </Grid>
          </Grid>
        </div>
        {props.loadingState || !props.rewards ?
          <Grid container>
            <div className={classes.noCards}>
              <div className={classes.loadingState}>
                <Loading />
              </div>
            </div>
          </Grid>
          :
          (rewardCardsNotAlreadyUnlock && rewardCardsNotAlreadyUnlock.length > 0) || (rewardCardsAlreadyUnlock && rewardCardsAlreadyUnlock.length > 0) ?
            <Grid container>
              {
                rewardCardsNotAlreadyUnlock.length > 0 &&
                <Grid container>
                  <Grid item xs={12}><b>{t("reward.availableToUnlock")}</b></Grid>
                  <Grid container spacing={3}>{rewardCardsNotAlreadyUnlock}</Grid>
                </Grid>
              }
              <Divider variant="middle" className={classes.divider} />
              {
                rewardCardsAlreadyUnlock.length > 0 &&
                <Grid container>
                  <Grid item xs={12}><b>{t("reward.alreadyUnlocked")}</b></Grid>
                  <Grid container spacing={3}>{rewardCardsAlreadyUnlock}</Grid>
                </Grid>
              }

            </Grid>
            :
            <Grid container>
              <div className={classes.noCards}>
                <div className={classes.emptyState}>
                  <img alt="" src="/assets/images/icons/maps.png" />
                  <br /><br />
                  <h3>{t('vendorList.notFound.title')}</h3>
                  <p>{t('vendorList.notFound.subTitle')}</p>
                  <NavLink to="/suggestBrand">{t('vendorList.notFound.link')}</NavLink>
                </div>
              </div>
            </Grid>
        }
      </main>
    </div>
  );
}