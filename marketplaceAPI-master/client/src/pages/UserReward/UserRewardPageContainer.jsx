import React, { useState, useEffect } from 'react';

import { rewardsGql } from '../../graphQL/reward'
import { setRewards as setRewardsInternal} from '../../shared/store/actions/reward.actions';

import { achievementsGql } from '../../graphQL/achievement'

import UserRewardPage from './UserRewardPage';

import { useDispatch, useSelector } from "react-redux";

import { setSidebar } from '../../shared/store/actions/sidebar.actions';

import routerHistory from '../../shared/router-history/router-history';

import * as _ from 'lodash';

import { toast } from 'react-toastify';

import { toastError } from '../../shared/utils/error';

import { Trans } from 'react-i18next';

import { categoriesGql } from '../../graphQL/category'
import { keywordsGql } from '../../graphQL/keyword'

import { gendersGql } from '../../graphQL/gender';
import { setGenders as setGendersInternal} from '../../shared/store/actions/gender.actions';

import { aroundPositionVendorsIdsByKmsGql } from '../../graphQL/vendor'
import { setAroundPositionVendorsIdsByKms as setAroundPositionVendorsIdsByKmsInternal } from '../../shared/store/actions/vendor.actions';

import { useTranslation } from 'react-i18next';

import { locationsByCityGql, locationsByCountyGql } from '../../graphQL/location'

import { updateUserGql } from '../../graphQL/user'
import { setUser } from '../../shared/store/actions/authentication.actions';

import { filter } from '../../shared/utils/filter'

export default function UserRewardPageContainer() {
  const dispatch = useDispatch();

  const toProfile = () => {
    routerHistory.push('/user');
    window.scrollTo(0, 0);
  }

  const { t } = useTranslation();

  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  //const locationsFilter = useSelector((state) => state.location.locationsFilter); 
  const locations = useSelector((state) => state.location.locationsFilter);  
  const categoriesInternal = useSelector((state) => state.category.categories);
  const keywordsInternal = useSelector((state) => state.keyword.keywords);
  const aroundPositionVendorsIdsByKmsInternal = useSelector((state) => state.vendor.aroundPositionVendorsIdsByKms);
  const gendersInternal = useSelector((state) => state.gender.genders);
  const rewardsInternal = useSelector((state) => state.reward.rewards);
  
  const [suggestions, setSuggestions] = useState([]);
  const [countiesAndCitiesSuggestions, setCountiesAndCitiesSuggestions] = useState([]);

  const geoloc = useSelector((state) => state.user.position);
  const geolocAccepted = useSelector((state) => state.user.geolocAccepted)
  
  const [filteredRewards, setFilteredRewards] = useState(null);
  const [categories, setCategories] = useState([]);
  const [keywords, setKeywords] = useState([]);
  const [genders, setGenders] = useState([]);
  const [aroundVendorsIds, setAroundVendorsIds] = useState([]);
  const [loadingState, setLoadingState] = useState(true);
  const [cityChecked, setCityChecked] = React.useState(false);
  const [cityDistance, setCityDistance] = React.useState(30);
  const [onlineVendors, setOnlineVendors] = React.useState(false);
  const [clickAndCollectVendors, setClickAndCollectVendors] = React.useState(false);

  const [rewards, setRewards] = useState(null);  
  const [achievements, setAchievements] = useState([])

  useEffect(() => {
    let didCancel = false

    async function retrieveRewards() {
      let rewards; 
      if(rewardsInternal.length > 0) {
        rewards = rewardsInternal;
      } else {
        rewards = await rewardsGql();
        !didCancel && dispatch(setRewardsInternal(rewards));
      }
      !didCancel && setRewards(rewards);
    }

    async function retrieveAchievements() {
      let achievementsTmp = await achievementsGql();
      !didCancel && setAchievements(achievementsTmp)
    }

    async function retrieveCategories() {
      let categories;
      if(categoriesInternal.length > 0) {
        categories = categoriesInternal;
      } else {
        categories = await categoriesGql();
      }
      !didCancel && setCategories(categories)
    }

    async function retrieveKeywords() {
      let keywords;
      if(keywordsInternal.length > 0) {
        keywords = keywordsInternal;
      } else {
        keywords = await keywordsGql();
      }
      !didCancel && setKeywords(keywords)
    }

    async function retrieveGenders() {
      let genders; 
      if(gendersInternal.length > 0) {
        genders = gendersInternal;
      } else {
        genders = await gendersGql();
        !didCancel && dispatch(setGendersInternal(genders));
      }
      !didCancel && setGenders(genders);
    }

    async function retrieveLocations() {
      let locationsCities = await locationsByCityGql();
      let locationsCounties = await locationsByCountyGql();
      locationsCities = locationsCities.map((loc) => {return {vendors: loc.vendors, loc: loc.city}})
      locationsCounties = locationsCounties.map((loc) => {return {vendors: loc.vendors, loc: loc.county}})
      let suggestionsCitiesCounties = _.unionBy(locationsCities, locationsCounties, 'loc');
      !didCancel && setCountiesAndCitiesSuggestions(suggestionsCitiesCounties);
      let suggestions = suggestionsCitiesCounties.map((el) => {
        return {
          value: el.loc,
          name: el.loc
        }
      });
      
      !didCancel && setSuggestions(suggestions);
    }

    Promise.all([retrieveRewards(), retrieveAchievements(), retrieveCategories(), retrieveKeywords(), retrieveGenders(), retrieveLocations()])

    return () => { didCancel = true }
  }, [])
  
  const [searchFilters, setSearchFilters] = useState({cat: [], kw: [], gd: [], city: []});

  const constructFilterArray = (field) => {
    let fieldToSearch = searchFilters[field];
    return fieldToSearch ? _.isArray(fieldToSearch) ? fieldToSearch : [fieldToSearch] : []
  }

  const toggleCategoryFilter = (category, checked) => {
    if(category.id) { 

      setLoadingState(true);
      let categories = constructFilterArray('cat');
      
      if(checked) {
        categories.push(category.id);
      } else {
        _.remove(categories, (n) => n === category.id)
      }
      setSearchFilters({
          ...searchFilters,
          cat: categories
        })
    }
  }

  const toggleKeywordFilter = (keyword, checked) => {
    if(keyword.id)  { 

    setLoadingState(true);
      let keywords = constructFilterArray('kw');
      if(checked) {
        keywords.push(keyword.id);
      } else {
        _.remove(keywords, (n) => n === keyword.id)
      }
      setSearchFilters({
          ...searchFilters,
          kw: keywords
        })
    }
  }

  const toggleGenderFilter = (gender, checked) => {
    if(gender.id)  { 

    setLoadingState(true);
      let genders = constructFilterArray('gd');
      if(checked) {
        genders.push(gender.id);
      } else {
        _.remove(genders, (n) => n === gender.id)
      }
      setSearchFilters({
          ...searchFilters,
          gd: genders
        })
    }
  }


  const toggleCityFilter = (city, checked) => {
    if(city)  { 
      setLoadingState(true);
      let cities = constructFilterArray('city');
      if(checked) {
        cities.push(city);
        let suggestionsTmp = suggestions;
        _.remove(suggestionsTmp, (c) => c.value === city);
        setSuggestions(suggestionsTmp);
      } else {
        _.remove(cities, (n) => n === city)
        let suggestionsTmp = suggestions;
        suggestionsTmp.push({
          value: city,
          name: city
        })        
        setSuggestions(suggestionsTmp);
      }
      setSearchFilters({
          ...searchFilters,
          city: cities
        })
    }
  }

  const dropCityFilter = () => {
    setLoadingState(true);
    setSearchFilters({
      ...searchFilters,
      city: []
    })
  }

  const updatePositionCheck = (geolocChecked) => {
    setLoadingState(true);

    async function aroundPositionVendorsIds() {
      let vendorsIds;
      if(aroundPositionVendorsIdsByKmsInternal.length > 0) {
        vendorsIds = aroundPositionVendorsIdsByKmsInternal[3];
      } else {
        let vendorsIdsByKms = await aroundPositionVendorsIdsByKmsGql({position: geoloc});
        dispatch(setAroundPositionVendorsIdsByKmsInternal(vendorsIdsByKms));
        vendorsIds = vendorsIdsByKms[3];
      }
      setAroundVendorsIds(vendorsIds);
    }

    if(geolocChecked) {
      aroundPositionVendorsIds();
    }
    setCityChecked(geolocChecked);
  }

  const updateOnlineVendorCheck = (onlineVendors) => {
    setLoadingState(true);
    setOnlineVendors(onlineVendors);
  }

  const updateClickAndCollectVendorCheck = (clickAndCollectVendors) => {
    setLoadingState(true);
    setClickAndCollectVendors(clickAndCollectVendors);
  }
  

  const updatePositionDistance = (distance) => {
    setLoadingState(true);
    async function aroundPositionVendorsIds() {
      let vendorsIds;
      let kms = [5, 10, 20, 30];
      if(aroundPositionVendorsIdsByKmsInternal.length > 0) {
        vendorsIds = aroundPositionVendorsIdsByKmsInternal[kms.indexOf(distance)];
      } else {
        let vendorsIdsByKms = await aroundPositionVendorsIdsByKmsGql({position: geoloc});
        dispatch(setAroundPositionVendorsIdsByKmsInternal(vendorsIdsByKms));
        vendorsIds = vendorsIdsByKms[kms.indexOf(distance)];
      }
      setAroundVendorsIds(vendorsIds);
      setCityDistance(distance)
    }

    if(geolocAccepted) {
      aroundPositionVendorsIds();
    } else { 
      setCityDistance(distance)
    }
  }

  const dispatchSideBar= (open) => {
    dispatch(setSidebar(open));
  }

  const fillUserValues = (fillUser) => {
    if(loggedInUser) {
      setLoadingState(true);
      if(fillUser) {
        let sF = {...searchFilters};
        let previousKw = sF.kw || [];
  
        let keywordsId = keywords.map((el) => el.id);
        let keywordsUserId = loggedInUser.keywords.filter((el) => keywordsId.includes(el.id)).map((el) => el.id);
        if(keywordsUserId.length <= 0) {
          const Msg = () => (
            <Trans i18nKey="vendorList.noValues.message"
                  values={{ linkText: t('vendorList.noValues.linkText')}}
                  components={[<span style={{color: "white", fontWeight: "bold", textDecoration: "underline"}} onClick={toProfile}></span>]}/>
          )
          toast.info(<Msg />, {autoClose: false});
        }
        sF.kw = _.union(keywordsUserId, previousKw)
        setSearchFilters(sF);
      } else {
        let sF = {...searchFilters};
        let previousKw = sF.kw || [];
  
        let keywordsId = keywords.map((el) => el.id);
        let keywordsUserId = loggedInUser.keywords.filter((el) => keywordsId.includes(el.id)).map((el) => el.id);
  
        _.remove(previousKw, (kw) => {
          return keywordsUserId.includes(kw)})
        
        sF.kw = previousKw;
        setSearchFilters(sF);
      }
    }
  }

  useEffect(() => {
    let didCancel = false
    if(rewards) {
      let rewardsTmp = filter(rewards, searchFilters, countiesAndCitiesSuggestions, {cityChecked, onlineVendors, clickAndCollectVendors, aroundVendorsIds}, null)
      if(!filteredRewards || (rewardsTmp.length !== filteredRewards.length && rewardsTmp !== filteredRewards)) {
        !didCancel && setFilteredRewards(rewardsTmp);
      }
      !didCancel && setLoadingState(false);
    } else {
      !didCancel && setLoadingState(true);
    }


    return () => { didCancel = true }

  }, [rewards, cityChecked, cityDistance, aroundVendorsIds, onlineVendors, clickAndCollectVendors, searchFilters]);

  const updateRewards = async (rewardId) => {
    let rewards = loggedInUser.rewards || [];
    rewards.push(rewardId)
    let input = { rewards };
    updateUserGql({input: input})
      .then(user => {
        dispatch(setUser(user));
      })
      .catch(e => {
        toastError(e.message,t);
      });
  }

  return (<UserRewardPage
           filterUrlArgs={searchFilters}
           categories={categories}
           keywords={keywords}
           genders={genders}
           locations={locations}
           handleToggleCategory={toggleCategoryFilter}
           handleToggleKeyword={toggleKeywordFilter}
           handleToggleGender={toggleGenderFilter}
           handleToggleCity={toggleCityFilter}
           dispatchSideBar={dispatchSideBar}
           geoloc={geoloc}
           geolocAccepted={geolocAccepted}
           handlePositionCheck={updatePositionCheck}
           handlePositionDistance={updatePositionDistance}
           handleOnlineVendorCheck={updateOnlineVendorCheck}
           handleClickAndCollectVendorCheck={updateClickAndCollectVendorCheck}
           dropCityFilter={dropCityFilter}
           loadingState={loadingState}
           //citiesSuggestions={citiesSuggestions}
           toggleUserValues={fillUserValues}
           isLogged={!!loggedInUser}
           suggestions={suggestions}
           countiesAndCitiesSuggestions={countiesAndCitiesSuggestions}

           rewards={filteredRewards}
           user={loggedInUser}
           achievements={achievements}
           updateRewards={updateRewards}
          />);

}