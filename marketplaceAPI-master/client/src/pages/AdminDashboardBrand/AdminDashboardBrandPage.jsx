import * as React from 'react';

import SuggestedBrandsManager from './SuggestedBrandsManager'
import PotentialBrandsManager from './PotentialBrandsManager'
import InstagramSuggestedBrandsManager from './InstagramSuggestedBrandsManager'

import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import { useTranslation } from 'react-i18next';

//import { makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import { makeStyles } from '@material-ui/core/styles';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles({
  root: {
    width: "99%",
    flexGrow: 1,
    backgroundColor: "white",
  }
});

export default function AdminDashboardBrandPage(props) {
  const { t } = useTranslation();

  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <IconButton onClick={props.goBack}>
        <ArrowBackIcon />
      </IconButton>
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
          <Tab label={t('adminDashboard.suggestedBrandManager')} {...a11yProps(0)} />
          <Tab label={t('adminDashboard.potentialBrandManager')} {...a11yProps(1)} />
          <Tab label={t('adminDashboard.instagramSuggestedBrandManager')} {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <SuggestedBrandsManager
          suggestedBrands={props.suggestedBrands}
          updateSuggestedBrand={props.updateSuggestedBrand}
          categories={props.categories}
          keywords={props.keywords}
          deleteSuggestedBrand={props.deleteSuggestedBrand}
          contactSuggestedBrand={props.contactSuggestedBrand}
          createSuggestedBrand={props.createSuggestedBrand}
          transformSuggestedBrand={props.transformSuggestedBrand}
        />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <PotentialBrandsManager
          potentialBrands={props.potentialBrands}
          updatePotentialBrand={props.updatePotentialBrand}
          categories={props.categories}
          keywords={props.keywords}
          createPotentialBrand={props.createPotentialBrand}
          deletePotentialBrand={props.deletePotentialBrand}
        />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <InstagramSuggestedBrandsManager
          instagramSuggestedBrands={props.instagramSuggestedBrands}
          updateInstagramSuggestedBrand={props.updateInstagramSuggestedBrand}
          deleteInstagramSuggestedBrand={props.deleteInstagramSuggestedBrand}
        />
      </TabPanel>
    </div>
  );
}