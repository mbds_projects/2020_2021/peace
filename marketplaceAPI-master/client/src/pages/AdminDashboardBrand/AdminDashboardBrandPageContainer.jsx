import React, { useState, useEffect } from 'react';

import AdminDashboardBrandPage from './AdminDashboardBrandPage';
import LoadingPage from '../Loading/LoadingPage';

import routerHistory from '../../shared/router-history/router-history';

import { suggestedBrandsGql, updateSuggestedBrandGql, deleteSuggestedBrandGql, contactSuggestedBrandGql, createSuggestedBrandAdminGql, transformSuggestedBrandGql } from '../../graphQL/suggestedBrand';

import { potentialBrandsGql, updatePotentialBrandGql, createPotentialBrandGql, deletePotentialBrandGql } from '../../graphQL/potentialBrand';

import { instagramSuggestedBrandsGql, updateInstagramSuggestedBrandGql, deleteInstagramSuggestedBrandGql } from '../../graphQL/instagramSuggestedBrand';

import { toast } from 'react-toastify';

import { useSelector } from "react-redux";

import { useTranslation } from 'react-i18next';

export default function AdminDashboardBrandPageContainer() {
  const { t } = useTranslation();
  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  
  const [suggestedBrands, setSuggestedBrands] = useState([]);
  const [potentialBrands, setPotentialBrands] = useState([]);
  const [instagramSuggestedBrands, setInstagramSuggestedBrands] = useState([]);

  const categories = useSelector((state) => state.category.categories);
  const keywords = useSelector((state) => state.keyword.keywords);

  const goBack = () => {
    routerHistory.push('/admin/');
  }

  const updateSuggestedBrand = async(suggestedBrand) => {
    try {
      let updatedSuggestedBrand = await updateSuggestedBrandGql({input: suggestedBrand})
      if(updatedSuggestedBrand) {
        updateData();
        toast.success(t('adminDashboard.toast.suggestedBrandUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('adminDashboard.toast.suggestedBrandUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return updatedSuggestedBrand;
    } catch(e) {
      toast.error(t('adminDashboard.toast.suggestedBrandUpdatedError'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }

  }

  const deleteSuggestedBrand = async(suggestedBrandId) => {
    try {
      let deleteSuggestedBrand = await deleteSuggestedBrandGql({suggestedBrandId: suggestedBrandId})
      if(deleteSuggestedBrand) {
        updateData();
        toast.success(t('adminDashboard.toast.suggestedBrandDeleted'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('adminDashboard.toast.suggestedBrandDeletedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return deleteSuggestedBrand;
    } catch(e) {
      toast.error(t('adminDashboard.toast.suggestedBrandDeletedError'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }

  }

  const updatePotentialBrand = async(potentialBrand) => {
    try {
      let updatedPotentialBrand = await updatePotentialBrandGql({input: potentialBrand})
      if(updatedPotentialBrand) {
        updateData();
        toast.success(t('adminDashboard.toast.potentialBrandUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('adminDashboard.toast.potentialBrandUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return updatedPotentialBrand;
    } catch(e) {
      toast.error(t('adminDashboard.toast.potentialBrandUpdatedError'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }

  }

  const createPotentialBrand = async(potentialBrand) => {
    try {
      let createPotentialBrand = await createPotentialBrandGql({input: potentialBrand})
      if(createPotentialBrand) {
        updateData();
        toast.success(t('adminDashboard.toast.potentialBrandCreated'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('adminDashboard.toast.potentialBrandCreatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return createPotentialBrand;
    } catch(e) {
      toast.error(t('adminDashboard.toast.potentialBrandCreatedError'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }

  }

  const deletePotentialBrand = async(potentialBrandId) => {
    try {
      let deletePotentialBrand = await deletePotentialBrandGql({potentialBrandId: potentialBrandId})
      if(deletePotentialBrand) {
        updateData();
        toast.success(t('adminDashboard.toast.potentialBrandDeleted'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('adminDashboard.toast.potentialBrandDeletedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return deletePotentialBrand;
    } catch(e) {
      toast.error(t('adminDashboard.toast.potentialBrandDeletedError'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }
  }

  const updateInstagramSuggestedBrand = async(instagramSuggestedBrand) => {
    try {
      let updatedInstagramSuggestedBrand = await updateInstagramSuggestedBrandGql({input: instagramSuggestedBrand})
      if(updatedInstagramSuggestedBrand) {
        updateData();
        toast.success(t('adminDashboard.toast.instagramSuggestedBrandUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('adminDashboard.toast.instagramSuggestedBrandUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return updatedInstagramSuggestedBrand;
    } catch(e) {
      toast.error(t('adminDashboard.toast.instagramSuggestedBrandUpdatedError'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }
  }

  const deleteInstagramSuggestedBrand = async(instagramSuggestedBrandId) => {
    try {
      let deleteInstagramSuggestedBrand = await deleteInstagramSuggestedBrandGql({instagramSuggestedBrandId: instagramSuggestedBrandId})
      if(deleteInstagramSuggestedBrand) {
        updateData();
        toast.success(t('adminDashboard.toast.instagramSuggestedBrandDeleted'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('adminDashboard.toast.instagramSuggestedBrandDeletedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return deleteInstagramSuggestedBrand;
    } catch(e) {
      toast.error(t('adminDashboard.toast.instagramSuggestedBrandDeletedError'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }

  }

  const contactSuggestedBrand = async(suggestedBrand) => {
    try {
      let contactSuggestedBrand = await contactSuggestedBrandGql({name: suggestedBrand.name, id: suggestedBrand.id, mail: suggestedBrand.mail})
      if(contactSuggestedBrand) {
        updateData();
        toast.success(t('adminDashboard.toast.suggestedBrandContacted'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('adminDashboard.toast.suggestedBrandContactedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return contactSuggestedBrand;
    } catch(e) {
      toast.error(t('adminDashboard.toast.suggestedBrandContactedError'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }
  }

  const createSuggestedBrand = async(suggestedBrand) => {
    try {
      let createSuggestedBrand = await createSuggestedBrandAdminGql({input: suggestedBrand})
      if(createSuggestedBrand) {
        updateData();
        toast.success(t('adminDashboard.toast.suggestedBrandCreated'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('adminDashboard.toast.suggestedBrandCreatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return createSuggestedBrand;
    } catch(e) {
      toast.error(t('adminDashboard.toast.suggestedBrandCreatedError'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }
  }

  const transformSuggestedBrand = async(brandID) => {
    try {
      let transformSuggestedBrand = await transformSuggestedBrandGql({id: brandID})
      if(transformSuggestedBrand) {
        updateData();
        toast.success(t('adminDashboard.toast.suggestedBrandTransformed'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('adminDashboard.toast.suggestedBrandTransformedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return transformSuggestedBrand;
    } catch(e) {
      toast.error(t('adminDashboard.toast.suggestedBrandTransformedError'), {
        position: toast.POSITION.TOP_RIGHT
      });
      return false;
    }
  }

  const updateData = async() => {
    let suggestedBrands = await suggestedBrandsGql();
    let potentialBrands = await potentialBrandsGql();
    let instagramSuggestedBrands = await instagramSuggestedBrandsGql();
    setSuggestedBrands(suggestedBrands);
    setPotentialBrands(potentialBrands);
    setInstagramSuggestedBrands(instagramSuggestedBrands);
  }

  useEffect(() => {
    updateData();
  }, []);

  if (!loggedInUser) {
    return <LoadingPage />
  }

  return (<AdminDashboardBrandPage
    suggestedBrands={suggestedBrands}
    updateSuggestedBrand={updateSuggestedBrand}
    deleteSuggestedBrand={deleteSuggestedBrand}
    categories={categories}
    keywords={keywords}
    potentialBrands={potentialBrands}
    updatePotentialBrand={updatePotentialBrand}
    createPotentialBrand={createPotentialBrand}
    deletePotentialBrand={deletePotentialBrand}
    instagramSuggestedBrands={instagramSuggestedBrands}
    updateInstagramSuggestedBrand={updateInstagramSuggestedBrand}
    deleteInstagramSuggestedBrand={deleteInstagramSuggestedBrand}
    contactSuggestedBrand={contactSuggestedBrand}
    createSuggestedBrand={createSuggestedBrand}
    goBack={goBack}
    transformSuggestedBrand={transformSuggestedBrand}
  />);
}
