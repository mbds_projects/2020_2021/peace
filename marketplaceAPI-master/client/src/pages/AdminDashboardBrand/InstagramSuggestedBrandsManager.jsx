import * as React from 'react';

import { useTranslation } from 'react-i18next';

import * as _ from 'lodash';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import { TableSortLabel } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

import TextField from '@material-ui/core/TextField';
//import Select from 'react-select';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Switch from '@material-ui/core/Switch';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  select: {
    marginTop: "20px"
  },
  search: {
    marginBottom: "20px"
  }
});

export default function InstagramSuggestedBrandsManager(props) {
  const { t } = useTranslation();

  const classes = useStyles();

  const [open, setOpen] = React.useState(false);
  const [openDelete, setOpenDelete] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [sortByOccurrence, setSortByOccurrence] = React.useState(false);
  const [sortByName, setSortByName] = React.useState(true);
  const [sortByChecked, setSortByChecked] = React.useState(true);
  const [sortByCreatedAt, setSortByCreatedAt] = React.useState(true);
  const [sortByUpdatedAt, setSortByUpdatedAt] = React.useState(true);
  const [sortBy, setSortBy] = React.useState({name: 'occurrence', value: 'desc'});
  const [selectedBrandId, setSelectedBrandId] = React.useState();
  const [selectedBrandName, setSelectedBrandName] = React.useState();
  const [selectedBrandOccurrence, setSelectedBrandOccurrence] = React.useState();
  const [selectedBrandChecked, setSelectedBrandChecked] = React.useState();
  const [search, setSearch] = React.useState('');
  const [deleteBrandId, setDeleteBrandId] = React.useState();

  const handleClickOpen = (brand) => {
    setOpen(true);
    setSelectedBrandId(brand.id)
    setSelectedBrandName(brand.name)
    setSelectedBrandChecked(brand.checked)
    setSelectedBrandOccurrence(brand.occurrence)
  };

  
  const handleClose = () => {
    setOpen(false);
  };

  const handleName = (event) => {
    setSelectedBrandName(event.target.value);
  };

  const handleOccurrence = (event) => {
    setSelectedBrandOccurrence(parseInt(event.target.value));
  };

  const handleChecked = (event) => {
    setSelectedBrandChecked(event.target.checked);
  };

  const handleUpdate = () => {
    let selectedBrand = {
      id: selectedBrandId,
      checked: selectedBrandChecked,
      name: selectedBrandName,
      occurrence: selectedBrandOccurrence
    }
    props.updateInstagramSuggestedBrand(selectedBrand);
    setSelectedBrandName(null);
    setSelectedBrandChecked(null);
    setSelectedBrandId(null)
    setSelectedBrandOccurrence(null)
    setOpen(false);
  };

  const handleDeleteOpen = (brandId) => {
    setOpenDelete(true);
    setDeleteBrandId(brandId)
  }

  const handleCloseDelete = () => {
    setOpenDelete(false);
  };

  const handleDelete = () => {
    props.deleteInstagramSuggestedBrand(deleteBrandId);
    setOpenDelete(false);
    setDeleteBrandId(null);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };

  const handleChangeSortByOccurrence = (event) => {
    setSortByOccurrence(!sortByOccurrence);    
    setSortBy({
      name: "occurrence",
      value: sortByOccurrence ? 'desc' : 'asc'
    })

    setSortByName(true);  
    setSortByChecked(true);   
    setSortByCreatedAt(true);   
    setSortByUpdatedAt(true);   
  };

  const handleChangeSortByName = (event) => {

    setSortByName(!sortByName);    
    setSortBy({
      name: "name",
      value: sortByName ? 'desc' : 'asc'
    })

    setSortByOccurrence(true);  
    setSortByChecked(true);    
    setSortByCreatedAt(true);   
    setSortByUpdatedAt(true);   
  };


  const handleChangeSortByChecked = (event) => {

    setSortByChecked(!sortByChecked);    
    setSortBy({
      name: "checked",
      value: sortByChecked ? 'desc' : 'asc'
    })

    setSortByOccurrence(true);  
    setSortByName(true); 
    setSortByCreatedAt(true);   
    setSortByUpdatedAt(true);   
  };

  const handleChangeSortByCreatedAt = (event) => {
    setSortByCreatedAt(!sortByCreatedAt);
    setSortBy({
      name: "createdAt",
      value: sortByCreatedAt ? 'desc' : 'asc'
    })

    setSortByName(true);   
    setSortByChecked(true);   
    setSortByOccurrence(true);   
    setSortByUpdatedAt(true);   
  };

  const handleChangeSortByUpdatedAt = (event) => {
    setSortByUpdatedAt(!sortByUpdatedAt);
    setSortBy({
      name: "updatedAt",
      value: sortByUpdatedAt ? 'desc' : 'asc'
    })

    setSortByName(true);   
    setSortByChecked(true);   
    setSortByCreatedAt(true);   
    setSortByOccurrence(true);   
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
  <div>
    <TextField
      margin="dense"
      id="search"
      label={t('adminDashboard.search')}
      fullWidth
      type="text"
      value={search}
      variant="outlined"
      onChange={handleSearch}
      className={classes.search}
    />
    {
      <Dialog open={openDelete} onClose={handleCloseDelete} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Delete</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {t('adminDashboard.deleteBrand')}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDelete} color="primary">
            {t('adminDashboard.cancel')}
          </Button>
          <Button onClick={handleDelete} color="primary">
            {t('adminDashboard.delete')}
          </Button>
        </DialogActions>
      </Dialog>
    }
    {
      selectedBrandId && 
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Update</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {t('adminDashboard.updateBrand')}
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('adminDashboard.name')}
            fullWidth
            type="text"
            value={selectedBrandName}
            variant="outlined"
            onChange={handleName}
          />
          <TextField
            autoFocus
            margin="dense"
            id="occurrence"
            label={t('adminDashboard.occurrence')}
            fullWidth
            type="number"
            value={selectedBrandOccurrence}
            variant="outlined"
            onChange={handleOccurrence}
          />
          <Switch checked={selectedBrandChecked} onChange={handleChecked} value={selectedBrandChecked} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            {t('adminDashboard.cancel')}
          </Button>
          <Button onClick={handleUpdate} color="primary">
            {t('adminDashboard.update')}
          </Button>
        </DialogActions>
      </Dialog>
    }
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>{t('adminDashboard.update')}</TableCell>
            <TableCell>{t('adminDashboard.delete')}</TableCell>
            <TableCell sortDirection={sortByName ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByName ? 'desc' : 'asc'}
                onClick={handleChangeSortByName}
              >
                {t('adminDashboard.name')}
              </TableSortLabel>
            </TableCell>
            <TableCell sortDirection={sortByOccurrence ? 'desc' : 'asc'} align="right">
              <TableSortLabel
                active={true}
                direction={sortByOccurrence ? 'desc' : 'asc'}
                onClick={handleChangeSortByOccurrence}
              >
                {t('adminDashboard.occurrence')}
              </TableSortLabel>
            </TableCell>
            <TableCell sortDirection={sortByChecked ? 'desc' : 'asc'} align="right">
              <TableSortLabel
                active={true}
                direction={sortByChecked ? 'desc' : 'asc'}
                onClick={handleChangeSortByChecked}
              >
                {t('adminDashboard.checked')}
              </TableSortLabel>
            </TableCell>
            <TableCell align="right" sortDirection={sortByCreatedAt ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByCreatedAt ? 'desc' : 'asc'}
                onClick={handleChangeSortByCreatedAt}
              >
                {t('adminDashboard.createdAt')}
              </TableSortLabel>
            </TableCell>
            <TableCell align="right" sortDirection={sortByUpdatedAt ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByUpdatedAt ? 'desc' : 'asc'}
                onClick={handleChangeSortByUpdatedAt}
              >
                {t('adminDashboard.updatedAt')}
              </TableSortLabel>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            _.orderBy(_.filter(props.instagramSuggestedBrands, function(brand) { return brand.name.toLowerCase().includes(search.toLowerCase()) })
              , [sortBy.name]
              , [sortBy.value])
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((brand) => {
              return (
              <TableRow key={brand.name}>
                <TableCell component="th" scope="row">< EditIcon  onClick={() => {handleClickOpen(brand)}} /></TableCell>
                <TableCell component="th" scope="row">< DeleteIcon  onClick={() => {handleDeleteOpen(brand.id)}} /></TableCell>
                <TableCell component="th" scope="row">{brand.name}</TableCell>
                <TableCell align="right">{brand.occurrence}</TableCell>
                <TableCell align="right">{brand.checked.toString()}</TableCell>
                <TableCell align="right">{brand.createdAt}</TableCell>
                <TableCell align="right">{brand.updatedAt}</TableCell>
              </TableRow>
            )}
          )}
        </TableBody>
      </Table>
    </TableContainer>
    <TablePagination
      rowsPerPageOptions={[5, 10, 25]}
      component="div"
      count={(_.filter(props.instagramSuggestedBrands, function(brand) { return brand.name.toLowerCase().includes(search.toLowerCase()) })).length}
      rowsPerPage={rowsPerPage}
      page={page}
      onChangePage={handleChangePage}
      onChangeRowsPerPage={handleChangeRowsPerPage}
    />
  </div>
  );
}