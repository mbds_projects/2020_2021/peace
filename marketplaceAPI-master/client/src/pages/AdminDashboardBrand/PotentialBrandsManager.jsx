import * as React from 'react';

import { useTranslation } from 'react-i18next';

import * as _ from 'lodash';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import { TableSortLabel } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

import Select from 'react-select';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  select: {
    marginTop: "20px"
  },
  search: {
    marginBottom: "20px"
  }
});

export default function PotentialBrandsManager(props) {
  const { t, i18n } = useTranslation();

  const classes = useStyles();

  const [open, setOpen] = React.useState(false);
  const [openCreate, setOpenCreate] = React.useState(false);
  const [openDelete, setOpenDelete] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [sortByName, setSortByName] = React.useState(false);
  const [sortByCreatedAt, setSortByCreatedAt] = React.useState(true);
  const [sortByUpdatedAt, setSortByUpdatedAt] = React.useState(true);
  const [sortBy, setSortBy] = React.useState({name: 'name', value: 'desc'});
  const [selectedBrandId, setSelectedBrandId] = React.useState();
  const [selectedBrandName, setSelectedBrandName] = React.useState();
  const [selectedBrandSite, setSelectedBrandSite] = React.useState();
  const [selectedBrandNote, setSelectedBrandNote] = React.useState();
  const [selectedBrandMail, setSelectedBrandMail] = React.useState();
  const [selectedCategories, setSelectedCategories] = React.useState([]);
  const [selectedKeywords, setSelectedKeywords] = React.useState([]);
  const [selectedState, setSelectedState] = React.useState({});
  const [search, setSearch] = React.useState('');
  const [createBrandName, setCreateBrandName] = React.useState('');
  const [createBrandSite, setCreateBrandSite] = React.useState('');
  const [createBrandNote, setCreateBrandNote] = React.useState('');
  const [createBrandMail, setCreateBrandMail] = React.useState('');
  const [createCategories, setCreateCategories] = React.useState([]);
  const [createKeywords, setCreateKeywords] = React.useState([]);
  const [deleteBrandId, setDeleteBrandId] = React.useState();
  const [filterState, setFilterState] = React.useState([]);

  const stateEdit = [
    {value: 'notContacted', label: t('adminDashboard.state.notContacted')}, 
    {value: 'firstContact', label: t('adminDashboard.state.firstContact')},
    {value: 'certificationValidationOngoing', label: t('adminDashboard.state.certificationValidationOngoing')}, 
    {value: 'certificationValidated', label: t('adminDashboard.state.certificationValidated')}, 
    {value: 'dataValidationOngoing', label: t('adminDashboard.state.dataValidationOngoing')}, 
    {value: 'dataValidated', label: t('adminDashboard.state.dataValidated')}, 
    {value: 'dataCreatingOngoing', label: t('adminDashboard.state.dataCreatingOngoing')}, 
    {value: 'dataCreated', label: t('adminDashboard.state.dataCreated')}];

  const handleClickOpen = (brand) => {
    setOpen(true);
    setSelectedBrandId(brand.id)
    setSelectedBrandName(brand.name)
    setSelectedBrandSite(brand.site)
    setSelectedBrandNote(brand.note)
    setSelectedBrandMail(brand.mail)
    setSelectedState({value: brand.state, label: t(`adminDashboard.state.${brand.state}`)})
    let cats = brand.categories.map((cat) => {
      let findCat = _.find(categoriesOptions, { value: cat });
      return findCat;
    })
    setSelectedCategories(cats)
    let kws = brand.keywords.map((kw) => {
      let findKw = _.find(keywordsOptions, { value: kw });
      return findKw;
    })
    setSelectedKeywords(kws);
  };

  const handleAddPotentialBrand = () => {
    setOpenCreate(true);
  }

  const handleDeleteOpen = (brandId) => {
    setOpenDelete(true);
    setDeleteBrandId(brandId)
  }

  const handleClose = () => {
    setOpen(false);
  };

  const handleCloseCreate = () => {
    setOpenCreate(false);
  };

  const handleCloseDelete = () => {
    setOpenDelete(false);
  };

  const handleName = (event) => {
    setSelectedBrandName(event.target.value);
  };

  const handleSite = (event) => {
    setSelectedBrandSite(event.target.value);
  };

  const handleNote = (event) => {
    setSelectedBrandNote(event.target.value);
  };

  const handleMail = (event) => {
    setSelectedBrandMail(event.target.value);
  };

  const handleCategoriesSelect = (categories) => {
    setSelectedCategories(categories)
  }

  const handleKeywordsSelect = (keywords) => {
    setSelectedKeywords(keywords)
  }

  const handleStateSelect = (state) => {
    setSelectedState(state)
  }

  const handleUpdate = () => {
    let categoriesIds = selectedCategories.map((cat) => cat.value);
    let keywordsIds = selectedKeywords.map((kw) => kw.value);
    let selectedBrand = {
      id: selectedBrandId,
      site: selectedBrandSite,
      note: selectedBrandNote,
      mail: selectedBrandMail,
      name: selectedBrandName,
      categories: categoriesIds,
      keywords: keywordsIds,
      state: selectedState.value
    }
    props.updatePotentialBrand(selectedBrand);
    setSelectedBrandName(null);
    setSelectedBrandSite(null);
    setSelectedBrandNote(null);
    setSelectedBrandMail(null);
    setSelectedBrandId(null)
    setOpen(false);
    setSelectedCategories([])
    setSelectedKeywords([])
    setSelectedState({})
  };

  const handleCreateName = (event) => {
    setCreateBrandName(event.target.value);
  };

  const handleCreateSite = (event) => {
    setCreateBrandSite(event.target.value);
  };

  const handleCreateNote = (event) => {
    setCreateBrandNote(event.target.value);
  };

  const handleCreateMail = (event) => {
    setCreateBrandMail(event.target.value);
  };

  const handleCreateCategoriesSelect = (categories) => {
    setCreateCategories(categories)
  }

  const handleCreateKeywordsSelect = (keywords) => {
    setCreateKeywords(keywords)
  }

  const handleCreate = () => {
    let categoriesIds = createCategories.map((cat) => cat.value);
    let keywordsIds = createKeywords.map((kw) => kw.value);
    let createBrand = {
      site: createBrandSite,
      note: createBrandNote,
      mail: createBrandMail,
      name: createBrandName,
      categories: categoriesIds,
      keywords: keywordsIds
    }
    props.createPotentialBrand(createBrand);
    setSelectedBrandName('');
    setSelectedBrandSite('');
    setSelectedBrandNote('');
    setSelectedBrandMail('');
    setOpenCreate(false);
    setSelectedCategories([])
    setSelectedKeywords([])
  };

  const handleDelete = () => {
    props.deletePotentialBrand(deleteBrandId);
    setOpenDelete(false);
    setDeleteBrandId(null);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };

  const handleChangeSortByName = (event) => {

    setSortByName(!sortByName);    
    setSortBy({
      name: "name",
      value: sortByName ? 'desc' : 'asc'
    }) 
    setSortByCreatedAt(true);   
    setSortByUpdatedAt(true);   
  };

  const handleChangeSortByCreatedAt = (event) => {
    setSortByCreatedAt(!sortByCreatedAt);
    setSortBy({
      name: "createdAt",
      value: sortByCreatedAt ? 'desc' : 'asc'
    })

    setSortByName(true);    
    setSortByUpdatedAt(true);   
  };

  const handleChangeSortByUpdatedAt = (event) => {
    setSortByUpdatedAt(!sortByUpdatedAt);
    setSortBy({
      name: "updatedAt",
      value: sortByUpdatedAt ? 'desc' : 'asc'
    })

    setSortByName(true);   
    setSortByCreatedAt(true);    
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
   
  let categoriesOptions = props.categories.map((category) => {    
    let catName = category.lang.find((l) => l.locale === i18n.language).value;
    return {
        value: category.id,
        label: catName
        }
    }
  );

  let keywordsOptions = props.keywords.map((keyword) => {
      let keyName = keyword.lang.find((l) => l.locale === i18n.language).value;
      return{
          value: keyword.id,
          label: keyName
      }
  });

  const handleFilterState = (state) => {
    setFilterState(state)
  }

  return (
  <div>
    <TextField
      margin="dense"
      id="search"
      label={t('adminDashboard.search')}
      fullWidth
      type="text"
      value={search}
      variant="outlined"
      onChange={handleSearch}
      className={classes.search}
    />
    <Select 
      name="stateFilter"
      options={stateEdit} 
      placeholder={t('adminDashboard.state.filter')}
      isMulti
      value={filterState}
      onChange={handleFilterState}
      className={classes.select}
      multi={true}
    />
    <Button onClick={handleAddPotentialBrand} color="primary">{t('adminDashboard.createBrand')}</Button>
    {
      <Dialog open={openDelete} onClose={handleCloseDelete} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">{t('adminDashboard.delete')}</DialogTitle>
        <DialogContent>
          <DialogContentText>
          {t('adminDashboard.deleteBrand')}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDelete} color="primary">
          {t('adminDashboard.cancel')}
          </Button>
          <Button onClick={handleDelete} color="primary">
          {t('adminDashboard.delete')}
          </Button>
        </DialogActions>
      </Dialog>
    }
    {
      <Dialog open={openCreate} onClose={handleCloseCreate} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">{t('adminDashboard.create')}</DialogTitle>
        <DialogContent>
          <DialogContentText>
          {t('adminDashboard.createBrand')}
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('adminDashboard.name')}
            fullWidth
            type="text"
            value={createBrandName}
            variant="outlined"
            onChange={handleCreateName}
          />
          <TextField
            autoFocus
            margin="dense"
            id="mail"
            label={t('adminDashboard.mail')}
            type="email"
            fullWidth
            value={createBrandMail}
            variant="outlined"
            onChange={handleCreateMail}
          />
          <TextField
            autoFocus
            margin="dense"
            id="site"
            label={t('adminDashboard.site')}
            type="url"
            fullWidth
            value={createBrandSite}
            variant="outlined"
            onChange={handleCreateSite}
          />
          <TextField
            autoFocus
            margin="dense"
            id="note"
            label={t('adminDashboard.note')}
            type="text"
            fullWidth
            value={createBrandNote}
            variant="outlined"
            onChange={handleCreateNote}
          />
          <Select 
            name="categoryInput"
            options={categoriesOptions} 
            placeholder={t('vendorCandidate.categories')}
            isMulti
            value={createCategories}
            onChange={handleCreateCategoriesSelect}
            className={classes.select}
            multi={true}
          />
          <Select 
            name="keywordInput"
            options={keywordsOptions} 
            placeholder={t('vendorCandidate.keywords')}
            isMulti
            value={createKeywords}
            onChange={handleCreateKeywordsSelect}
            className={classes.select}
            multi={true}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseCreate} color="primary">
          {t('adminDashboard.cancel')}
          </Button>
          <Button onClick={handleCreate} color="primary">
          {t('adminDashboard.create')}
          </Button>
        </DialogActions>
      </Dialog>
    }
    {
      selectedBrandId && 
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">{t('adminDashboard.update')}</DialogTitle>
        <DialogContent>
          <DialogContentText>
          {t('adminDashboard.updateBrand')}
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('adminDashboard.name')}
            fullWidth
            type="text"
            value={selectedBrandName}
            variant="outlined"
            onChange={handleName}
          />
          <TextField
            autoFocus
            margin="dense"
            id="mail"
            label={t('adminDashboard.mail')}
            type="email"
            fullWidth
            value={selectedBrandMail}
            variant="outlined"
            onChange={handleMail}
          />
          <TextField
            autoFocus
            margin="dense"
            id="site"
            label={t('adminDashboard.site')}
            type="url"
            fullWidth
            value={selectedBrandSite}
            variant="outlined"
            onChange={handleSite}
          />
          <TextField
            autoFocus
            margin="dense"
            id="note"
            label={t('adminDashboard.note')}
            type="text"
            fullWidth
            value={selectedBrandNote}
            variant="outlined"
            onChange={handleNote}
          />
          <Select 
            name="categoryInput"
            options={categoriesOptions} 
            placeholder={t('vendorCandidate.categories')}
            isMulti
            value={selectedCategories}
            onChange={handleCategoriesSelect}
            className={classes.select}
            styles={{ menu: base => ({ ...base, position: 'relative' }) }}
            multi={true}
          />
          <Select 
            name="keywordInput"
            options={keywordsOptions} 
            placeholder={t('vendorCandidate.keywords')}
            isMulti
            value={selectedKeywords}
            onChange={handleKeywordsSelect}
            className={classes.select}
            styles={{ menu: base => ({ ...base, position: 'relative' }) }}
            multi={true}
          />

          <Select 
            name="stateInput"
            options={stateEdit} 
            placeholder={t('adminDashboard.state.title')}
            value={selectedState}
            onChange={handleStateSelect}
            className={classes.select}
            styles={{ menu: base => ({ ...base, position: 'relative' }) }}
            multi={false}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            {t('adminDashboard.cancel')}
          </Button>
          <Button onClick={handleUpdate} color="primary">
            {t('adminDashboard.update')}
          </Button>
        </DialogActions>
      </Dialog>
    }
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>{t('adminDashboard.update')}</TableCell>
            <TableCell>Delete</TableCell>
            <TableCell sortDirection={sortByName ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByName ? 'desc' : 'asc'}
                onClick={handleChangeSortByName}
              >
                {t('adminDashboard.name')}
              </TableSortLabel>
            </TableCell>
            <TableCell align="right">{t('adminDashboard.mail')}</TableCell>
            <TableCell align="right">{t('adminDashboard.site')}</TableCell>
            <TableCell align="right">{t('vendorCandidate.categories')}</TableCell>
            <TableCell align="right">{t('vendorCandidate.keywords')}</TableCell>
            <TableCell align="right">{t('adminDashboard.state.title')}</TableCell>
            <TableCell align="right">{t('adminDashboard.note')}</TableCell>
            <TableCell align="right" sortDirection={sortByCreatedAt ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByCreatedAt ? 'desc' : 'asc'}
                onClick={handleChangeSortByCreatedAt}
              >
                {t('adminDashboard.createdAt')}
              </TableSortLabel>
            </TableCell>
            <TableCell align="right" sortDirection={sortByUpdatedAt ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByUpdatedAt ? 'desc' : 'asc'}
                onClick={handleChangeSortByUpdatedAt}
              >
                {t('adminDashboard.updatedAt')}
              </TableSortLabel>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            _.orderBy(
              _.filter(props.potentialBrands, function(brand) { 
                if(filterState && filterState.length > 0) {
                  let states = filterState.map((el) => el.value);
                  return states.includes(brand.state) && brand.name.toLowerCase().includes(search.toLowerCase()) 
                } else {
                  return brand.name.toLowerCase().includes(search.toLowerCase()) 
                }
              })
              , [sortBy.name]
              , [sortBy.value])
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((brand) => {
              let brandCats = brand.categories.map((cat) => {
                let findCat = _.find(categoriesOptions, { value: cat });
                return findCat ? findCat.label : '';
              })
              let brandCatsString = brandCats.join("; ")
              let brandKws = brand.keywords.map((kw) => {
                let findKw = _.find(keywordsOptions, { value: kw });
                return findKw ? findKw.label : '';
              })
              let brandKwsString = brandKws.join("; ")
              return (
              <TableRow key={brand.name}>
                <TableCell component="th" scope="row">< EditIcon  onClick={() => handleClickOpen(brand)} /></TableCell>
                <TableCell component="th" scope="row">< DeleteIcon  onClick={() => {handleDeleteOpen(brand.id)}} /></TableCell>
                <TableCell component="th" scope="row">{brand.name}</TableCell>
                <TableCell align="right">{brand.mail}</TableCell>
                <TableCell align="right">{brand.site}</TableCell>
                <TableCell align="right">{brandCatsString}</TableCell>
                <TableCell align="right">{brandKwsString}</TableCell>
                <TableCell align="right">{t(`adminDashboard.state.${brand.state}`)}</TableCell>
                <TableCell align="right">{brand.note}</TableCell>
                <TableCell align="right">{brand.createdAt}</TableCell>
                <TableCell align="right">{brand.updatedAt}</TableCell>
              </TableRow>
            )}
          )}
        </TableBody>
      </Table>
    </TableContainer>
    <TablePagination
      rowsPerPageOptions={[5, 10, 25]}
      component="div"
      count={(_.filter(props.potentialBrands, function(brand) { 
        if(filterState && filterState.length > 0) {
          let states = filterState.map((el) => el.value);
          return states.includes(brand.state) && brand.name.toLowerCase().includes(search.toLowerCase()) 
        } else {
          return brand.name.toLowerCase().includes(search.toLowerCase()) 
        }
      })).length}rowsPerPage={rowsPerPage}
      page={page}
      onChangePage={handleChangePage}
      onChangeRowsPerPage={handleChangeRowsPerPage}
    />
  </div>
  );
}