import React, { useState, useEffect } from 'react';

import AdminDashboardGenderPage from './AdminDashboardGenderPage';
import LoadingPage from '../Loading/LoadingPage';

import { toastError } from '../../shared/utils/error';

import { toast } from 'react-toastify';

import { useSelector } from "react-redux";

import { gendersGql, uploadPictureGql, updateDescGql } from '../../graphQL/gender'

import routerHistory from '../../shared/router-history/router-history';

import { useTranslation } from 'react-i18next';
export default function AdminDashboardGenderPageContainer() {
  const { t } = useTranslation();
  const loggedInUser = useSelector(state => state.authentication.currentUser);
  const [genders, setGenders] = useState([]);

  const goBack = () => {
    routerHistory.push('/admin/');
  }

  const updateData = async() => {
    let gendersTmp = await gendersGql();
    setGenders(gendersTmp);
  }

  useEffect(() => {
    updateData();
  }, []);

  const updateDesc = (descFr, descEn, id) => {
    updateDescGql({descFr: descFr, descEn: descEn, id: id}).then(res => {
      if(res) {
        toast.success(t('adminDashboard.toast.genderUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
        updateData();
      } else {
        toast.error(t('adminDashboard.toast.genderUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }).catch(e => {
      toastError(e.message,t);
    });
  }

  const uploadPicture = (file, id) => {
    try {
      let updated = uploadPictureGql({file: file, id: id})
      if(updated) {
        toast.success(t('adminDashboard.toast.imageUploaded'), {
          position: toast.POSITION.TOP_RIGHT
        });
        updateData();
      } else {
        toast.success(t('adminDashboard.toast.imageUploadedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    } catch(e) {
      toastError(e.message,t);
    }
  }

  if (!loggedInUser) {
    return <LoadingPage />
  }

  return (<AdminDashboardGenderPage
    goBack={goBack}
    genders={genders}
    uploadPicture={uploadPicture}
    updateDesc={updateDesc}
  />);
}
