import React, { useState, useCallback, useRef } from "react";
import _ from 'lodash'
import Button from '@material-ui/core/Button';

import TextField from '@material-ui/core/TextField';

import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import { TableSortLabel } from '@material-ui/core';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import EditIcon from '@material-ui/icons/Edit';
import PublishIcon from '@material-ui/icons/Publish';

import ReactCrop from "react-image-crop";
import 'react-image-crop/dist/ReactCrop.css';

import FilterCard from '../../components/FilterCard/FilterCard';

const useStyles = makeStyles({
  root: {

  },
  table: {
    minWidth: 650,
  },
  search: {
    marginBottom: "20px"
  },
  imgPreview: {
    width: "400px"
  },
  filterCard: {
    width: "200px",
    "& div": {
      width: "200px"
    }
  }
});

export default function AdminDashboardGenderPage(props) {
  const classes = useStyles();
  const { t } = useTranslation();

  const [open, setOpen] = React.useState(false);
  const [openUpload, setOpenUpload] = React.useState(false);

  const [selectedGenderId, setSelectedGenderId] = React.useState();
  const [selectedGenderDescFr, setSelectedGenderDescFr] = React.useState();
  const [selectedGenderDescEn, setSelectedGenderDescEn] = React.useState();

  const [upImg, setUpImg] = useState();
  const imgRef = useRef(null);
  const [crop, setCrop] = useState({ unit: "px", width: 400, height: 200 });
  const [previewUrl, setPreviewUrl] = useState();
  const [file, setFile] = useState();

  const [sortByName, setSortByName] = React.useState(true);
  const [sortBy, setSortBy] = React.useState({name: 'name', value: 'desc'});
  const [search, setSearch] = React.useState('');
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [sortByCreatedAt, setSortByCreatedAt] = React.useState(true);
  const [sortByUpdatedAt, setSortByUpdatedAt] = React.useState(true);

  const handleClickOpen = (cat) => {
    setOpen(true);
    setSelectedGenderId(cat.id)
    setSelectedGenderDescFr(cat.lang[0].value)
    setSelectedGenderDescEn(cat.lang[1].value)
  };

  const handleClickOpenUpload = (cat) => {
    setOpenUpload(true);
    setSelectedGenderId(cat.id)
  }

  const onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  const makeClientCrop = async (crop) => {
    if (imgRef.current && crop.width && crop.height) {
      createCropPreview(imgRef.current, crop, "newFile.png");
    }
  };

  const createCropPreview = async (image, crop, fileName) => {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = 400;
    canvas.height = 200;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      400,
      200
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          reject(new Error("Canvas is empty"));
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(previewUrl);
        setPreviewUrl(window.URL.createObjectURL(blob));

        const file = new File([blob], `image.png`,{type:"image/png", lastModified:new Date()})
        setFile(file);
      }, "image/png");
    });
  };

  const sendFile = () => {
    props.uploadPicture(file, selectedGenderId);
    setOpenUpload(false);
    setFile(null);
    setPreviewUrl('')
    setUpImg(null);
    setCrop({ unit: "px", width: 400, height: 200 })
  }

  const handleChangeSortByName = (event) => {
    setSortByName(!sortByName);    
    setSortBy({
      name: "name",
      value: sortByName ? 'desc' : 'asc'
    })  
    setSortByUpdatedAt(true);
    setSortByCreatedAt(true);
  };

  const handleChangeSortByCreatedAt = (event) => {
    setSortByCreatedAt(!sortByCreatedAt);
    setSortBy({
      name: "createdAt",
      value: sortByCreatedAt ? 'desc' : 'asc'
    })

    setSortByName(true);   
    setSortByUpdatedAt(true);   
  };

  const handleChangeSortByUpdatedAt = (event) => {
    setSortByUpdatedAt(!sortByUpdatedAt);
    setSortBy({
      name: "updatedAt",
      value: sortByUpdatedAt ? 'desc' : 'asc'
    })

    setSortByName(true);     
    setSortByCreatedAt(true);   
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleDescFr = (event) => {
    setSelectedGenderDescFr(event.target.value);
  };

  const handleDescEn = (event) => {
    setSelectedGenderDescEn(event.target.value);
  };

  const handleClose = () => {
    setOpen(false);
    setSelectedGenderDescFr('');
    setSelectedGenderDescEn('');
  };

  const handleCloseUpload = () => {
    setOpenUpload(false)
    setFile(null);
    setPreviewUrl('')
    setUpImg(null);
    setCrop({ unit: "px", width: 200, height: 200 })
  }

  const handleUpdate = () => {
    props.updateDesc(selectedGenderDescFr, selectedGenderDescEn, selectedGenderId);
    setSelectedGenderDescFr(null);
    setSelectedGenderDescEn(null);
    setSelectedGenderId(null)
    setOpen(false);
  };

  return (
      <div className={classes.root}>
        <IconButton onClick={props.goBack}>
          <ArrowBackIcon />
        </IconButton>
        <h4>{t('adminDashboard.genders')}</h4>

        <TextField
          margin="dense"
          id="search"
          label={t('adminDashboard.search')}
          fullWidth
          type="text"
          value={search}
          variant="outlined"
          onChange={handleSearch}
          className={classes.search}
        />
      {
      selectedGenderId && 
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Update</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {t('adminDashboard.updateGender')}
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="descFr"
            label={t('adminDashboard.descFr')}
            fullWidth
            type="text"
            value={selectedGenderDescFr}
            variant="outlined"
            onChange={handleDescFr}
          />
          <TextField
            autoFocus
            margin="dense"
            id="descFr"
            label={t('adminDashboard.descEn')}
            fullWidth
            type="text"
            value={selectedGenderDescEn}
            variant="outlined"
            onChange={handleDescEn}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            {t('adminDashboard.cancel')}
          </Button>
          <Button onClick={handleUpdate} color="primary">
            {t('adminDashboard.update')}
          </Button>
        </DialogActions>
      </Dialog>
    }
    {
      selectedGenderId && 
      <Dialog open={openUpload} onClose={handleCloseUpload} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Upload</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {t('adminDashboard.uploadGender')}
          </DialogContentText>
          <div>
            <input type="file" accept="image/*" onChange={onSelectFile} />
            <ReactCrop
              src={upImg}
              onImageLoaded={onLoad}
              crop={crop}
              onChange={c => setCrop(c)}
              onComplete={makeClientCrop}
            />
            {previewUrl && <img alt="Crop preview" src={previewUrl} className={classes.imgPreview}/>}
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseUpload} color="primary">
            {t('adminDashboard.cancel')}
          </Button>
          <Button onClick={sendFile} color="primary">
            {t('adminDashboard.upload')}
          </Button>
        </DialogActions>
      </Dialog>
    }
        <TableContainer component={Paper}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>{t('adminDashboard.update')}</TableCell>
            <TableCell sortDirection={sortByName ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByName ? 'desc' : 'asc'}
                onClick={handleChangeSortByName}
              >
                {t('adminDashboard.descFr')}
              </TableSortLabel>
            </TableCell>
            <TableCell sortDirection={sortByName ? 'desc' : 'asc'}>
              {t('adminDashboard.descEn')}
            </TableCell>

            <TableCell>{t('adminDashboard.upload')}</TableCell>
            <TableCell>{t('adminDashboard.image')}</TableCell>
            <TableCell align="right" sortDirection={sortByCreatedAt ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByCreatedAt ? 'desc' : 'asc'}
                onClick={handleChangeSortByCreatedAt}
              >
                {t('adminDashboard.createdAt')}
              </TableSortLabel>
            </TableCell>
            <TableCell align="right" sortDirection={sortByUpdatedAt ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByUpdatedAt ? 'desc' : 'asc'}
                onClick={handleChangeSortByUpdatedAt}
              >
                {t('adminDashboard.updatedAt')}
              </TableSortLabel>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            _.orderBy(_.filter(props.genders, function(gender) { return gender.lang[0].value.toLowerCase().includes(search.toLowerCase()) || gender.lang[1].value.toLowerCase().includes(search.toLowerCase()) })
              , [sortBy.name]
              , [sortBy.value])
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((gender) => {
              return (
              <TableRow key={gender.lang[0].value}>
                <TableCell component="th" scope="row">< EditIcon  onClick={() => {handleClickOpen(gender)}} /></TableCell>
                <TableCell component="th" scope="row">{gender.lang[0].value}</TableCell>
                <TableCell component="th" scope="row">{gender.lang[1].value}</TableCell>
                <TableCell component="th" scope="row">< PublishIcon  onClick={() => {handleClickOpenUpload(gender)}} /></TableCell>
                <TableCell component="th" scope="row" className={classes.filterCard}><FilterCard filter={gender} type="gd"  height="150px"/></TableCell>
                <TableCell align="right">{gender.createdAt}</TableCell>
                <TableCell align="right">{gender.updatedAt}</TableCell>
              </TableRow>
            )}
          )}
        </TableBody>
      </Table>
    </TableContainer>
    <TablePagination
      rowsPerPageOptions={[5, 10, 25]}
      component="div"
      count={(_.filter(props.genders, function(gender) { return gender.lang[0].value.toLowerCase().includes(search.toLowerCase()) || gender.lang[1].value.toLowerCase().includes(search.toLowerCase()) })).length}
      rowsPerPage={rowsPerPage}
      page={page}
      onChangePage={handleChangePage}
      onChangeRowsPerPage={handleChangeRowsPerPage}
    />
      </div>

  );
}