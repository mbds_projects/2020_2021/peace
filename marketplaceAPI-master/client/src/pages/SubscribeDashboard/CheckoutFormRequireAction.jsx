import React, {useEffect, useState} from 'react';
import { useStripe } from '@stripe/react-stripe-js';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import { retryInvoiceGql } from '../../graphQL/subscriptionDashboard';

import routerHistory from '../../shared/router-history/router-history';

import Paper from '@material-ui/core/Paper';

import LoadingPage from '../Loading/LoadingPage';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import { useTranslation } from 'react-i18next';

import { toast } from 'react-toastify';


const useStyles = makeStyles((theme) =>
    createStyles({
        icon: {
            position: "relative",
            top: "5px"
        },
        paperForm: {
            padding: "25px"
        }
    })
);

export default function CheckoutFormRequireAction(props) {

    let classes = useStyles();
    const [loading, setLoading] = useState(false);
    const {subscription} = props

  const { t } = useTranslation();

    let customerId = subscription && subscription.customer ? subscription.customer : null;
    let invoiceId = subscription && subscription.latest_invoice && subscription.latest_invoice.id ? subscription.latest_invoice.id : null;
    let paymentMethodId = subscription && subscription.latest_invoice && subscription.latest_invoice.payment_intent && subscription.latest_invoice.payment_intent.payment_method ? subscription.latest_invoice.payment_intent.payment_method : null


    const stripe = useStripe();
    useEffect(() => {
        /*if (!stripe) {
            // Stripe.js has not yet loaded.
            // Make sure to disable form submission until Stripe.js has loaded.

            setLoading(false);
            return;
        }*/
        if(stripe && subscription) {

            if(!customerId || !invoiceId || !paymentMethodId){
                //routerHistory.push('/vendor/account');
            }
            retryInvoiceWithNewPaymentMethod({
                customerId,
                paymentMethodId,
                invoiceId,
            });
        }
    }, [stripe, subscription])

    function onSubscriptionComplete(result) {
        // Payment was successful.
        if (result.subscription.status === 'active' || result.subscription.status === 'trialing' || result.status === 'succeeded') {
            toast.success("Payment complete !", {
                position: toast.POSITION.TOP_RIGHT
            });

            localStorage.setItem(
                'latestInvoicePaymentIntentStatus',
                result.subscription.status
            );
            // Change your UI to show a success message to your customer.
            // Call your backend to grant access to your service based on
            // `result.subscription.items.data[0].price.product` the customer subscribed to.
        } else {
            toast.info("Payment pending !", {
                position: toast.POSITION.TOP_RIGHT
            });

        }
        setLoading(false);
        //props.setIncompleteSubscription();
        props.retrieveOnSuccess();
        routerHistory.push('/vendor/account');
    }

    function handlePaymentThatRequiresCustomerAction({
        subscription,
        invoice,
        paymentMethodId,
        isRetry,
    }) {
        if (subscription && subscription.status === 'active') {
            // Subscription is active, no customer actions required.
            return { subscription, paymentMethodId };
        }

        // If it's a first payment attempt, the payment intent is on the subscription latest invoice.
        // If it's a retry, the payment intent will be on the invoice itself.
        let paymentIntent = invoice ? invoice.payment_intent : subscription.latest_invoice.payment_intent;

        if (
            paymentIntent.status === 'requires_action' ||
            (isRetry === true && paymentIntent.status === 'requires_payment_method')
        ) {
            return stripe
                .confirmCardPayment(paymentIntent.client_secret, {
                    payment_method: paymentMethodId,
                })
                .then((result) => {
                    if (result.error) {
                        // Start code flow to handle updating the payment details.
                        // Display error message in your UI.
                        // The card was declined (i.e. insufficient funds, card has expired, etc).
                        throw result;
                    } else {
                        if (result.paymentIntent.status === 'succeeded') {
                            // Show a success message to your customer.
                            // There's a risk of the customer closing the window before the callback.
                            // We recommend setting up webhook endpoints later in this guide.
                            return {
                                subscription: subscription,
                                invoice: invoice,
                                paymentMethodId: paymentMethodId,
                                status: result.paymentIntent.status
                            };
                        }
                    }
                })
                .catch((error) => {
                    toast.error("An error occured !", {
                        position: toast.POSITION.TOP_RIGHT
                    });
                });
        } else {
            // No customer action needed.
            return { subscription, paymentMethodId };
        }
    }


    function retryInvoiceWithNewPaymentMethod({
        customerId,
        paymentMethodId,
        invoiceId,
    }) {
        return (
            retryInvoiceGql({paymentMethodId, invoiceId})
                // If the card is declined, display an error to the user.
                .then((result) => {
                    if (result.error) {
                        // The card had an error when trying to attach it to a customer.
                        throw result;
                    }
                    return result;
                })
                // Normalize the result to contain the object returned by Stripe.
                // Add the additional details we need.
                .then((result) => {
                    return {
                        // Use the Stripe 'object' property on the
                        // returned result to understand what object is returned.
                        invoice: result,
                        paymentMethodId: paymentMethodId,
                        isRetry: true,
                        subscription: subscription
                    };
                })
                // Some payment methods require a customer to be on session
                // to complete the payment process. Check the status of the
                // payment intent to handle these actions.
                .then(handlePaymentThatRequiresCustomerAction)
                // No more actions required. Provision your service for the user.
                .then(onSubscriptionComplete)
                .catch((error) => {
                    // An error has happened. Display the failure to the user here.
                    // We utilize the HTML element we created.
                    //displayError(error);
                    toast.error("An error occured !", {
                        position: toast.POSITION.TOP_RIGHT
                    });
                    setLoading(false);
                })
        );
    }

    return (
        <Paper className={classes.paperForm}>
            <div style={{alignText:"center"}}>{t('subscription.checkoutForm.requireAction')}</div>
            <Dialog open={loading} onClose={() => { }}>
                <DialogTitle>{t('subscription.checkoutForm.processPayment')}</DialogTitle>
                <DialogContent>
                    <LoadingPage />
                </DialogContent>
            </Dialog>
        </Paper>
    );
}
