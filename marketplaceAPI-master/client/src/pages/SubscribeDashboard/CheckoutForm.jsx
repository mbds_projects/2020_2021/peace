import React, { useEffect, useState } from 'react';
import { useStripe, useElements, CardElement } from '@stripe/react-stripe-js';

import { makeStyles, createStyles } from '@material-ui/core/styles';
import CardSection from './CardSection';

import * as _ from 'lodash';

import Select from 'react-select';

import { retryInvoiceGql, createSubscriptionDetailOfVendorGql, createSubscriptionFromVendorGql } from '../../graphQL/subscriptionDashboard';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

import Button from '@material-ui/core/Button';

import CardMembershipIcon from '@material-ui/icons/CardMembership';
import Paper from '@material-ui/core/Paper';

import LoadingPage from '../Loading/LoadingPage';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import routerHistory from '../../shared/router-history/router-history';

import { toast } from 'react-toastify';

import TextField from '@material-ui/core/TextField';
import { Trans } from 'react-i18next';
import { useTranslation } from 'react-i18next';
const useStyles = makeStyles((theme) =>
    createStyles({
        icon: {
            position: "relative",
            top: "5px"
        },
        paperForm: {
            padding: "25px"
        },
        littleStar: {
          color: "red",
          fontSize: "10px",
          verticalAlign: "top"
        },
        select: {
          zIndex: "2"
        },
    })
);

export default function CheckoutForm(props) {
    let classes = useStyles();
    const stripe = useStripe();
    const elements = useElements();
    const { t } = useTranslation();

    const [tvaIntra, setTvaIntra] = useState('');
    const [loading, setLoading] = useState(false);
    const [cardElement, setCardElement] = useState(false);
    const [cardComplete, setCardComplete] = useState(false);

    let splitSubscriptionDetail = props.selectedSubscriptionDetail.split(".");
    let product = props.prices[splitSubscriptionDetail[0]][splitSubscriptionDetail[1]];
    const customerId = props.customerId;

     const [selectedCountry, setSelectedCountry] = React.useState(null);

     let countriesOptions = ["france", "UE", "horsUE"].map((country, index) => {
        let countryName = t(`subscription.checkoutForm.country.${country}`);
        return {
          value: index,
          label: countryName
        }
      }
      );

    useEffect(() => {
        let didCancel = false
    
        if(elements) {
            const cardElementTmp = elements.getElement(CardElement);
            !didCancel && setCardElement(cardElementTmp)
        }

        return () => { didCancel = true }
    }, [elements])

    if(cardElement) {

        cardElement.on('change', function(event) {
            if (event.complete) {
                setCardComplete(true);
            } else if (event.error) {
                setCardComplete(false);
            }
          });
    }

    function onSubscriptionComplete(result) {
        // Payment was successful.
        if (result.subscription && (result.subscription.status === 'active' || result.subscription.status === 'trialing')) {
            toast.success(t('subscription.paymentComplete'), {
                position: toast.POSITION.TOP_RIGHT
            });
            localStorage.setItem(
                'latestInvoicePaymentIntentStatus',
                result.subscription.status
            );
            // Change your UI to show a success message to your customer.
            // Call your backend to grant access to your service based on
            // `result.subscription.items.data[0].price.product` the customer subscribed to.
        }
        setLoading(false);
        routerHistory.push('/vendor/account');
    }

    function handleRequiresPaymentMethod({
        subscription,
        paymentMethodId,
    }) {
        if (subscription.status === 'active' || subscription.status === 'trialing') {
            // subscription is active, no customer actions required.
            return { subscription, paymentMethodId };
        } else if (
            subscription.latest_invoice.payment_intent.status ===
            'requires_payment_method'
        ) {
            // Using localStorage to manage the state of the retry here,
            // feel free to replace with what you prefer.
            // Store the latest invoice ID and status.
            localStorage.setItem('latestInvoiceId', subscription.latest_invoice.id);
            localStorage.setItem(
                'latestInvoicePaymentIntentStatus',
                subscription.latest_invoice.payment_intent.status
            );
            throw { error: { message: 'Your card was declined.' } };
        } else {
            return { subscription, paymentMethodId };
        }
    }

    function handlePaymentThatRequiresCustomerAction({
        subscription,
        invoice,
        paymentMethodId,
        isRetry,
    }) {
        if (subscription && (subscription.status === 'active' || subscription.status === "trialing")) {
            // Subscription is active, no customer actions required.
            return { subscription, paymentMethodId };
        }

        // If it's a first payment attempt, the payment intent is on the subscription latest invoice.
        // If it's a retry, the payment intent will be on the invoice itself.
        let paymentIntent = invoice ? invoice.payment_intent : subscription.latest_invoice.payment_intent;

        if (
            paymentIntent.status === 'requires_action' ||
            (isRetry === true && paymentIntent.status === 'requires_payment_method')
        ) {
            return stripe
                .confirmCardPayment(paymentIntent.client_secret, {
                    payment_method: paymentMethodId,
                })
                .then((result) => {
                    if (result.error) {
                        // Start code flow to handle updating the payment details.
                        // Display error message in your UI.
                        // The card was declined (i.e. insufficient funds, card has expired, etc).
                        throw result;
                    } else {
                        if (result.paymentIntent.status === 'succeeded') {
                            // Show a success message to your customer.
                            // There's a risk of the customer closing the window before the callback.
                            // We recommend setting up webhook endpoints later in this guide.
                            return {
                                subscription: subscription,
                                invoice: invoice,
                                paymentMethodId: paymentMethodId,
                            };
                        }
                    }
                })
        } else {
            // No customer action needed.
            return { subscription, paymentMethodId };
        }
    }
    function createSubscription({ paymentMethodId }) {
        if(props.createNow) {
            return createSubscriptionDetailOfVendorGql({ paymentMethodId, from: selectedCountry.value, tvaIntra, subscriptionType: props.selectedSubscriptionDetail })
            .then((result) => {
                if(result) return createSubscriptionFromVendorGql()
                else throw result
            })
            // If the card is declined, display an error to the user.
            .then((result) => {
                if (result.error) {
                    // The card had an error when trying to attach it to a customer.
                    throw result;
                }
                return result;
            })
            // Normalize the result to contain the object returned by Stripe.
            // Add the additional details we need.
            .then((result) => {
                return {
                    paymentMethodId: paymentMethodId,
                    subscription: result,
                };
            })
            // Some payment methods require a customer to be on session
            // to complete the payment process. Check the status of the
            // payment intent to handle these actions.
            .then(handlePaymentThatRequiresCustomerAction)
            // If attaching this card to a Customer object succeeds,
            // but attempts to charge the customer fail, you
            // get a requires_payment_method error.
            .then(handleRequiresPaymentMethod)
            // No more actions required. Provision your service for the user.
            .then(onSubscriptionComplete)
            .catch((error) => {
                // An error has happened. Display the failure to the user here.
                // We utilize the HTML element we created.
                //showCardError(error);
                let errorMsg = error.message || error.error.message || "An error occured"
                toast.error(errorMsg, {
                    position: toast.POSITION.TOP_RIGHT
                });
                setLoading(false);
            })
        } else {
            props.registerCandidate({ paymentMethodId, from: selectedCountry.value, tvaIntra })
            if(elements) {
                const cardElementTmp = elements.getElement(CardElement);
                cardElementTmp.clear();
            }
            setCardComplete(false);
            setCardElement(false);
            setSelectedCountry(null);
            setTvaIntra('');
            setLoading(false);
        }
        
    }

    function retryInvoiceWithNewPaymentMethod({
        customerId,
        paymentMethodId,
        invoiceId,
    }) {
        return (
            retryInvoiceGql({paymentMethodId, invoiceId})
                // If the card is declined, display an error to the user.
                .then((result) => {
                    if (result.error) {
                        // The card had an error when trying to attach it to a customer.
                        throw result;
                    }
                    return result;
                })
                // Normalize the result to contain the object returned by Stripe.
                // Add the additional details we need.
                .then((result) => {
                    return {
                        // Use the Stripe 'object' property on the
                        // returned result to understand what object is returned.
                        invoice: result,
                        paymentMethodId: paymentMethodId,
                        isRetry: true
                    };
                })
                // Some payment methods require a customer to be on session
                // to complete the payment process. Check the status of the
                // payment intent to handle these actions.
                .then(handlePaymentThatRequiresCustomerAction)
                // No more actions required. Provision your service for the user.
                .then(onSubscriptionComplete)
                .catch((error) => {
                    // An error has happened. Display the failure to the user here.
                    // We utilize the HTML element we created.
                    //displayError(error);
                    let errorMsg = error.message || error.error.message || "An error occured"
                    toast.error(errorMsg, {
                        position: toast.POSITION.TOP_RIGHT
                    });
                    setLoading(false);
                })
        );
    }

    const handleSubmit = async (event) => {
        // We don't want to let default form submission happen here,
        // which would refresh the page.
        event.preventDefault();
        setLoading(true);

        if (!stripe || !elements) {
            // Stripe.js has not yet loaded.
            // Make sure to disable form submission until Stripe.js has loaded.

            setLoading(false);
            return;
        }

        // Get a reference to a mounted CardElement. Elements knows how
        // to find your CardElement because there can only ever be one of
        // each type of element.

        // If a previous payment was attempted, get the latest invoice
        const latestInvoicePaymentIntentStatus = localStorage.getItem(
            'latestInvoicePaymentIntentStatus'
        );

        const { error, paymentMethod } = await stripe.createPaymentMethod({
            type: 'card',
            card: cardElement,
        });

        if (error) {
            let errorMsg = error.message || error.error.message || "An error occured"
            toast.error(errorMsg, {
                position: toast.POSITION.TOP_RIGHT
            });
            setLoading(false);
        } else {
            const paymentMethodId = paymentMethod.id;
            if (latestInvoicePaymentIntentStatus === 'requires_payment_method' && customerId) {
                // Update the payment method and retry invoice payment
                const invoiceId = localStorage.getItem('latestInvoiceId');
                retryInvoiceWithNewPaymentMethod({
                    customerId,
                    paymentMethodId,
                    invoiceId,
                });
            } else {
                // Create the subscription
                createSubscription({ paymentMethodId });
            }
        }
    };


    return (
        <Paper className={classes.paperForm}>
            <Dialog open={loading} onClose={() => { }}>
                <DialogTitle>{t('subscription.checkoutForm.processPayment')}</DialogTitle>
                <DialogContent>
                    <LoadingPage />
                </DialogContent>
            </Dialog>
            <Trans i18nKey="subscription.checkoutForm.cardDetails" components={[<b><br/></b>]}/>
            <br/>
            <ArrowForwardIcon className={classes.icon} /> 
            {t('subscription.checkoutForm.totalDue')} 
            {
                !selectedCountry && <span>{t('subscription.checkoutForm.chooseCountry')}</span>
            }
            {
                selectedCountry && (selectedCountry.value === 0 || (selectedCountry.value === 1 && _.isEmpty(tvaIntra))) &&
                <span>{product.priceTTC}</span>
            }
            {
                selectedCountry && (selectedCountry.value === 2 || (selectedCountry.value === 1 && !_.isEmpty(tvaIntra))) &&
                <span>{product.priceHT}</span>
            }
            <br /><br />
            {t('subscription.checkoutForm.card')} <br />
            <CardSection /><br />
            <Select
                name="rewardInput"
                options={countriesOptions}
                placeholder={<span>{t('subscription.checkoutForm.country.label')} <span className={classes.littleStar}>*</span></span>}
                className={classes.select}
                value={selectedCountry}
                onChange={(country) => setSelectedCountry(country)}
            />
            {
                selectedCountry && selectedCountry.value === 1 && 
                <span>
                {t('subscription.checkoutForm.tvaIntra')} <br />
                <TextField
                    margin="dense"
                    id="tvaIntra"
                    fullWidth
                    type="text"
                    value={tvaIntra}
                    variant="outlined"
                    onChange={e => setTvaIntra(e.target.value)}
                /><br />
                </span>
            }
            <br/>
            <div style={{textAlign: "center"}}>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={handleSubmit}
                    disabled={!stripe || !cardComplete || !selectedCountry || !props.isValidCandidateForm}
                    endIcon={<CardMembershipIcon />}
                >
                    {t('subscription.subscribe')}
                </Button>
            </div>
        </Paper>
    );
}
