import React, { useState, useEffect } from 'react';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import CardMembershipIcon from '@material-ui/icons/CardMembership';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { compareAsc } from 'date-fns'
import CheckoutForm from './CheckoutForm'
import CheckoutFormRequireAction from './CheckoutFormRequireAction'
import SubscriptionSummary from './SubscriptionSummary'
import SubscriptionDetail from './SubscriptionDetail'

import Button from '@material-ui/core/Button';
import CachedIcon from '@material-ui/icons/Cached';

import { toast } from 'react-toastify';
import { createVendorCandidateGql, vendorsendinfoGql } from '../../graphQL/vendorCandidate'

import VendorCandidateForm from '../../components/VendorCandidateForm/VendorCandidateForm'
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import { Trans } from 'react-i18next';

import { format } from 'date-fns'
import frLocale from "date-fns/locale/fr";
import enLocale from "date-fns/locale/en-GB";

import { CONF } from '../../config/index'
import routerHistory from '../../shared/router-history/router-history';


const sitekey = CONF.sitekey;
const stripePromise = loadStripe(CONF.stripePK);

const locale = {
    fr: frLocale,
    en: enLocale
};


const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            width: "100%",
            marginTop: "20px",
            marginBottom: "20px"
        },
        card: {
            minWidth: 275,
        },
        title: {
            fontFamily: "'League Spartan', sans-serif",
        },
        pos: {
            marginBottom: 12,
        },
        ttc: {
            fontSize: 10,
            color: "grey",
            textAlign: "right"
        },
        more: {
            textDecoration: "underline",
            color: theme.colors.strawberry,
            cursor: "pointer"
        },
        economy: {
            color: theme.colors.almond,
            fontStyle: "italic"
        },
        choseNew: {
            textAlign: "center",
            marginBottom: "20px"
        }
    }),
);

export default function SubscribeDashboardPage(props) {

    const { t, i18n } = useTranslation();
    const { subscription } = props
    let paymentMethodId = subscription && subscription.latest_invoice && subscription.latest_invoice.payment_intent && subscription.latest_invoice.payment_intent.payment_method ? subscription.latest_invoice.payment_intent.payment_method : null

    let classes = useStyles();

    const [name, setName] = useState('');
    const [mail, setMail] = useState('');
    const [contactName, setContactName] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [site, setSite] = useState('');
    const [captchaChecked, setCaptchaChecked] = useState(false);

    let propsVendorCandidateForm = {
        name, setName,
        mail, setMail,
        contactName, setContactName,
        phoneNumber, setPhoneNumber,
        site, setSite,
        captchaChecked, setCaptchaChecked
    }

    let isValidCandidateForm = (!(name === "" || mail === ""
        || (sitekey !== "" && !captchaChecked)) || props.vendor)


    const [selectedSubscriptionDetail, setSelectedSubscriptionDetail] = useState(null);
    const [proration, setProration] = useState(null);


    useEffect(() => {
        let didCancel = false
        async function retrieveProration() {
            let proration = await props.calculateProration({subscriptionType: selectedSubscriptionDetail, proration: true});
            !didCancel && setProration(proration)
        }
        if(selectedSubscriptionDetail && props.vendor && props.vendor.subscriptionDashboardId) {
            let withProration = !(fromMonthlyToLowerMonthly() || fromYearlyToMonthly() || fromYearlyToLower())
            if(withProration) retrieveProration();
        }

        return () => { didCancel = true }
    }, [selectedSubscriptionDetail, props.vendor])

    let prices = {
        basic: {
            monthly: {
                priceHT: "1 € HT",
                priceTTC: "1.2 € TTC",
            },
            yearly: {
                priceHT: "11 € HT",
                priceTTC: "13.2 € TTC",
            },
            weTakeCare: {
                priceHT: "77 € HT",
                priceTTC: "92.4 € TTC",
            }
        },
        super: {
            monthly: {
                priceHT: "14 € HT",
                priceTTC: "16.9 € TTC",
            },
            yearly: {
                priceHT: "154 € HT",
                priceTTC: "181.8 € TTC",
            },
            weTakeCare: {
                priceHT: "264 € HT",
                priceTTC: "316.8 € TTC",
            }
        },
        superPlus: {
            monthly: {
                priceHT: "44 € HT",
                priceTTC: "52.8 € TTC",
            },
            yearly: {
                priceHT: "484 € HT",
                priceTTC: "580.8 € TTC",
            },
            weTakeCare: {
                priceHT: "704 € HT",
                priceTTC: "844.8 € TTC",
            }
        },
    }

    let propsSubscription = {
        selectedSubscriptionDetail,
        setSelectedSubscriptionDetail,
        prices
    }


    const registerCandidate = async (options) => {
        let input = { name, contactName, mail, phoneNumber, site, subscriptionType: selectedSubscriptionDetail }
        console.log(input)
        try {
            let vendorCandidate = await createVendorCandidateGql({ input });
            await vendorsendinfoGql({mail})
            if (!vendorCandidate) {
                console.log('***')
                toast.error(t('vendorCandidate.toast.creationError'),
            
                    {
                        position: toast.POSITION.TOP_RIGHT
                    })
            } else {
                toast.success(t('vendorCandidate.toast.creationSuccess'),
                    {
                        position: toast.POSITION.TOP_RIGHT
                    })
                setName('');
                setContactName('');
                setMail('');
                setSite('');
                setPhoneNumber('');
                setCaptchaChecked(false);
            }
        } catch (e) {
            console.log(e)
            toast.error(t('vendorCandidate.toast.creationError'),
                {
                    position: toast.POSITION.TOP_RIGHT
                })
        }

    }

    const goBack = () => {
        setSelectedSubscriptionDetail(null)
    }

    let subValue = ["basic", "super", "superPlus"];
    let freqValue = ["monthly", "yearly", "weTakeCare"]

    const fromYearlyToLower = () => {
        const selectedSubscriptionDetailSplit = selectedSubscriptionDetail.split(".")
        const currentSubscriptionDetailSplit = props.vendor.subscriptionType.split(".")
        const isLowerSub = subValue.indexOf(selectedSubscriptionDetailSplit[0]) < subValue.indexOf(currentSubscriptionDetailSplit[0]);
        const isLowerFreq = freqValue.indexOf(selectedSubscriptionDetailSplit[1]) < freqValue.indexOf(currentSubscriptionDetailSplit[1]);
        const isSameSub = subValue.indexOf(selectedSubscriptionDetailSplit[0]) === subValue.indexOf(currentSubscriptionDetailSplit[0]);
        const isYearly = currentSubscriptionDetailSplit[1] !== freqValue[0]
        return (isYearly && (isLowerSub || (isSameSub && isLowerFreq)))
    }

    const fromYearlyToMonthly = () => {
        const selectedSubscriptionDetailSplit = selectedSubscriptionDetail.split(".")
        const currentSubscriptionDetailSplit = props.vendor.subscriptionType.split(".")
        const isYearly = currentSubscriptionDetailSplit[1] !== freqValue[0]
        const isYearlyChose = selectedSubscriptionDetailSplit[1] !== freqValue[0]
        return ((isYearly && !isYearlyChose))
    }

    const fromMonthlyToLowerMonthly = () => {
        const selectedSubscriptionDetailSplit = selectedSubscriptionDetail.split(".")
        const currentSubscriptionDetailSplit = props.vendor.subscriptionType.split(".")
        const isMonthly = currentSubscriptionDetailSplit[1] === freqValue[0];
        const isLowerSub = subValue.indexOf(selectedSubscriptionDetailSplit[0]) < subValue.indexOf(currentSubscriptionDetailSplit[0]);
        return (isMonthly && isLowerSub);
    }

    const updateSubscription = async () => {
        let withProration = !(fromMonthlyToLowerMonthly() || fromYearlyToMonthly() || fromYearlyToLower())
        let res = await props.updateSubscription({ subscriptionType: selectedSubscriptionDetail, proration: withProration });
        if(res.status === "active" || res.status === "trialing") {
            toast.success(t('subscription.toast.updateSuccess'),
            {
                position: toast.POSITION.TOP_RIGHT
            })
            routerHistory.push('/vendor/account');
        }
    }

    const isCanceled = () => {
        return (props.subscription.status === "canceled")
    }
    
    return (

        <span className={classes.root}>
            {props.displayTitle && <h2 className={classes.title}>{t('subscription.title')}</h2>}
            {(props.vendor && props.subscription && (props.subscription.status === "incomplete" || props.subscription.status === "past_due" )) ?
                <span>
                    {
                        (paymentMethodId) ?
                            <span>

                            <div className={classes.choseNew}>
                                <b> {t('subscription.currentSubscription')}</b><br />
                            <SubscriptionSummary selectedSubscriptionDetail={props.vendor.subscriptionType} />
                            </div>
                            <Elements stripe={stripePromise}>
                                <CheckoutFormRequireAction retrieveOnSuccess={props.retrieveOnSuccess} subscription={props.subscription} />
                            </Elements>
                            </span>
                            :
                            <span>
                                <div className={classes.choseNew}>
                                    <b> {t('subscription.currentSubscription')}</b><br />
                                    <SubscriptionSummary selectedSubscriptionDetail={props.vendor.subscriptionType} />  
                                </div>
                                <Elements stripe={stripePromise}>
                                    <CheckoutForm createNow={true} prices={prices} selectedSubscriptionDetail={props.vendor.subscriptionType} customerId={props.vendor ? props.vendor.customerId : null} subscription={props.subscription} registerCandidate={registerCandidate} isValidCandidateForm={isValidCandidateForm} />
                                </Elements>
                            </span>
                    }
                </span>
                :
                <span>
                    {
                        selectedSubscriptionDetail === null ?
                            <span>
                                {
                                    props.vendor && props.subscription && isCanceled() &&
                                    <span>

                                    <div className={classes.choseNew}>
                                        <SubscriptionSummary selectedSubscriptionDetail={props.vendor.subscriptionType} />
                                        <Trans i18nKey="subscription.canceled"
                                            values={{ expiresIn: format(new Date(props.subscription.current_period_end * 1000), 'PP', { locale: locale[[i18n.language]] }) }}
                                            components={[<br />]} />
                                        </div>
                                        <SubscriptionDetail {...propsSubscription} /></span>
                                }
                                {
                                    props.vendor && (props.subscription && (props.subscription.status === "active" || props.subscription.status === "trialing")) &&
                                    <span>
                                        <div className={classes.choseNew}>
                                            <b> {t('subscription.currentSubscription')}</b><br />
                                            <SubscriptionSummary selectedSubscriptionDetail={props.vendor.subscriptionType} />
                                            <br />
                                            <b> {t('subscription.chose')}</b>
                                        </div>
                                        <SubscriptionDetail {...propsSubscription} />
                                    </span>
                                }
                                {
                                    (!props.vendor || !props.subscription) &&
                                    <SubscriptionDetail {...propsSubscription} />
                                }
                            </span>
                            :
                            <span>
                                {props.vendor ?
                                    <span>
                                        {
                                            (props.subscription && (props.subscription.status === "active" || props.subscription.status === "trailing")) &&

                                            <span>

                                                <IconButton onClick={goBack}>
                                                    <ArrowBackIcon />
                                                </IconButton>

                                                <div className={classes.choseNew}>
                                                    <b> {t('subscription.currentSubscription')}</b><br />
                                                    <SubscriptionSummary selectedSubscriptionDetail={props.vendor.subscriptionType} /><br />
                                                    <b> {t('subscription.selectedSubscription')}</b><br />
                                                    <SubscriptionSummary selectedSubscriptionDetail={selectedSubscriptionDetail} /><br />
                                                    <Button
                                                        variant="contained"
                                                        color="primary"
                                                        disabled={(props.vendor.subscriptionType === selectedSubscriptionDetail)}
                                                        onClick={updateSubscription}
                                                        endIcon={<CachedIcon />}
                                                    >
                                                        {t('subscription.buttonReplace')}
                                                    </Button>
                                                    { proration &&
                                                        <p className={classes.helper}>
                                                        <Trans i18nKey="subscription.proration"
                                                            values={{ prorationFinalPrice: proration.finalPrice + proration.finalTaxes, prorationPrice: proration.price }}
                                                            components={[<br />]} /></p>
                                                    }
                                                    {
                                                        (props.vendor.subscriptionType === selectedSubscriptionDetail) &&
                                                        <p className={classes.helper}>{t('subscription.alreadySubscribe')}</p>
                                                    }
                                                    {
                                                        fromYearlyToMonthly() &&
                                                        <p className={classes.helper}>
                                                            <Trans i18nKey="subscription.fromYearlyToMonthly"
                                                                values={{ expiresIn: format(new Date(props.subscription.current_period_end * 1000), 'PP', { locale: locale[[i18n.language]] }) }}
                                                                components={[<br />]} /></p>
                                                    }
                                                    {
                                                        !fromYearlyToMonthly() && fromYearlyToLower() &&
                                                        <p className={classes.helper}>
                                                            <Trans i18nKey="subscription.fromYearlyToLower"
                                                                values={{ expiresIn: format(new Date(props.subscription.current_period_end * 1000), 'PP', { locale: locale[[i18n.language]] }) }}
                                                                components={[<br />]} /></p>
                                                    }
                                                    {
                                                        fromMonthlyToLowerMonthly() &&
                                                        <p className={classes.helper}>
                                                            <Trans i18nKey="subscription.fromMonthlyToLowerMonthly"
                                                                values={{ expiresIn: format(new Date(props.subscription.current_period_end * 1000), 'PP', { locale: locale[[i18n.language]] }) }}
                                                                components={[<br />]} /></p>
                                                    }

                                                </div>


                                            </span>
                                        }
                                        {props.subscription && isCanceled() &&
                                            <span>
                                                <IconButton onClick={goBack}>
                                                    <ArrowBackIcon />
                                                </IconButton> <br />
                                                <div className={classes.choseNew}>

                                                    <b> {t('subscription.currentSubscription')}</b><br />
                                                    <SubscriptionSummary selectedSubscriptionDetail={props.vendor.subscriptionType} />
                                                    <Trans i18nKey="subscription.canceled"
                                                    values={{ expiresIn: format(new Date(props.subscription.current_period_end * 1000), 'PP', { locale: locale[[i18n.language]] }) }}
                                                    components={[<br />]} />

                                                    <br />
                                                    <b> {t('subscription.selectedSubscription')}</b><br />
                                                    <SubscriptionSummary selectedSubscriptionDetail={selectedSubscriptionDetail} /><br />
                                                </div>
                                                <Elements stripe={stripePromise}>
                                                    <CheckoutForm createNow={true} {...propsSubscription} customerId={props.vendor ? props.vendor.customerId : null} subscription={props.subscription} registerCandidate={registerCandidate} isValidCandidateForm={isValidCandidateForm} />
                                                </Elements>
                                            </span>
                                        }
                                        {
                                            !props.subscription &&
                                            <span>
                                                <IconButton onClick={goBack}>
                                                    <ArrowBackIcon />
                                                </IconButton> <br />

                                                <div className={classes.choseNew}>
                                                <SubscriptionSummary selectedSubscriptionDetail={selectedSubscriptionDetail} />
                                                {
                                                    ((compareAsc(new Date(props.vendor.createdAt), new Date("2020-11-26T00:00:00.0+00:00")) < 0) && !props.vendor.subscriptionDashboardId) &&
                                                        <p className={classes.helper}>
                                                            <Trans i18nKey="subscription.trialPeriod"
                                                                components={[<br />]} />
                                                        </p>
                                                    }
                                                    </div>
                                                <Elements stripe={stripePromise}>
                                                    <CheckoutForm createNow={true} {...propsSubscription} customerId={props.vendor ? props.vendor.customerId : null} subscription={props.subscription} registerCandidate={registerCandidate} isValidCandidateForm={isValidCandidateForm} />
                                                </Elements>
                                            </span>
                                        }
                                    </span>

                                    :
                                    <span>
                                        <IconButton onClick={goBack}>
                                            <ArrowBackIcon />
                                        </IconButton> <br />
                                        <SubscriptionSummary selectedSubscriptionDetail={selectedSubscriptionDetail} />
                                        <VendorCandidateForm subscriptionType={selectedSubscriptionDetail} {...propsVendorCandidateForm} />
                                        {/*<Elements stripe={stripePromise}>
                                            <CheckoutForm {...propsSubscription} customerId={props.vendor ? props.vendor.customerId : null} subscription={props.subscription} registerCandidate={registerCandidate} isValidCandidateForm={isValidCandidateForm} />
                                    </Elements>*/}
                                    <div style={{textAlign: "center", marginTop: "20px"}}>
                                    <Button  variant="contained"  color="primary"  onClick={registerCandidate}  disabled={!isValidCandidateForm} endIcon={<CardMembershipIcon />} > {t('subscription.subscribe')} </Button>

                                    </div>
                                    </span>
                                }
                                {
                                    // todo
                                    // check if vendor is connected => Display message access to my account (need to add change plan !)
                                    // if vendor is not connected

                                    // add discount offer feature
                                    // send automatic mail to ask info
                                }
                            </span>
                    }
                </span>
            }
        </span>
    )
}