import React from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

import PriceList from '../../components/PriceList/PriceList'

import Grid from '@material-ui/core/Grid';
import ReactGA from 'react-ga';

const useStyles = makeStyles((theme) =>
    createStyles({
        card: {
            "&:hover": {
                cursor: "pointer"
            },
            "& .MuiListItemText-primary": {
                fontSize: "18px"
            }
        },
        title: {
            fontSize: 14,
        },
        economy: {
            textAlign: "center",
            //color: theme.colors.almond,
            //fontStyle: "italic",
            //fontWeight: "bold"
        },
        cardSelected: {
            border: `solid 8px lightgrey`,
            "& p": {
                color: "black"
            },
            "& .MuiListItemText-primary": {
                fontSize: "18px"
            }
        },
        avatarStrawberry: {
            backgroundColor: theme.colors.strawberry
        },
        avatarAlmond: {
            backgroundColor: theme.colors.almond
        },
        avatarGold: {
            backgroundColor: theme.colors.gold
        },
        priceGold: {
            color: theme.colors.gold,
            fontWeight: "bold"
        },
        priceAlmond: {
            color: theme.colors.almond,
            fontWeight: "bold"
        },
        price: {
            fontWeight: "bold"
        }
    }),
);

export default function SubscriptionDetail(props) {
    const { t } = useTranslation();

    let classes = useStyles();

    let productsNames = ['basic', 'super', 'superPlus'];

    //todo add google analytic datan price list

    const selectSubscription = (product) => {
        props.setSubscribeProduct(product);
        ReactGA.event({
            category: 'Subscription',
            action: `Click on ${productsNames[product]}`,
            label: `Subscription`
        });
    }

    return (
        <Grid container direction="row" justify="center" alignItems="flex-start" spacing={2}>
            
        <PriceList setSelectedSubscriptionDetail={props.setSelectedSubscriptionDetail} />
        </Grid>
    )
}