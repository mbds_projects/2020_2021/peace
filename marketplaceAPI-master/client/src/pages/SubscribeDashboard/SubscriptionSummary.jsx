import React from 'react';

import { makeStyles, createStyles, fade } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) =>
    createStyles({
        monthlyCardBg: {
            backgroundColor: fade("#E4F8ED", 0.7),
            cursor: "pointer",
            "&:hover": {
                backgroundColor: "#E4F8ED"
            }
        },
        yearlyCardBg: {
            backgroundColor: fade(theme.colors.almond, 0.7),
            cursor: "pointer",
            "&:hover": {
                backgroundColor: theme.colors.almond
            }
        },
        weTakeCareCardBg: {
            backgroundColor: fade(theme.colors.gold, 0.7),
            cursor: "pointer",
            "&:hover": {
                backgroundColor: theme.colors.gold
            }
        },
        dialogPrice: {
            fontSize: "18px",
            fontWeight: "bold"
        }
    }),
);

export default function SubscriptionSummary(props) {
    const { t } = useTranslation();

    let classes = useStyles();

    let detail = props.selectedSubscriptionDetail.slice(props.selectedSubscriptionDetail.indexOf(".") + 1);

    let frequency = "byYear";
    if (detail === "monthly") {
        frequency = "byMonth"
    }
    let takeCare = detail === "weTakeCare"

    return (
        <Grid container direction="row" justify="center" spacing={2}>
            <Grid item xs={4}>
                <Card variant="outlined" className={classes[`${detail}CardBg`]}>
                    <CardContent>
                        <span className={classes.dialogPrice}>{t(`priceList.${props.selectedSubscriptionDetail}`)} / {t(`priceList.${frequency}`)}</span><br />
                        {frequency === "byMonth" && <span>{t(`priceList.noObligation`)}<br /></span>}
                        {frequency === "byYear" && <span>{t(`priceList.oneMonthOffer`)}<br /></span>}
                        {takeCare && <span>{t(`priceList.withWeTakeCare`)}</span>}
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    )
}