import React, {useEffect, useState} from 'react';

import SubscribeDashboardPage from './SubscribeDashboardPage'
import { retrieveSubscriptionGql, updateSubscriptionGql, calculateProrationGql } from '../../graphQL/subscriptionDashboard';

import { useDispatch, useSelector } from "react-redux";
import { useLocation} from 'react-router-dom';

import { setUser } from '../../shared/store/actions/authentication.actions';

export default function SubscribeDashboardPageContainer(props) {
    const loggedInUser = useSelector((state) => state.authentication.currentUser);
    const dispatch = useDispatch();
    const location = useLocation();
    const [subscription, setSubscription] = useState(null);

    useEffect(() => {
        if(location && location.pathname === "/subscribe") {
            window.scrollTo(0, 0);
        }

        let didCancel = false

        async function retrieveSubscription() {
            let subscriptionTmp = await retrieveSubscriptionGql()
            !didCancel && setSubscription(subscriptionTmp);
        }

        if(loggedInUser) retrieveSubscription()

        return () => { didCancel = true }
    }, [loggedInUser, location])

    const updateSubscription = async (input) => {
        let res = await updateSubscriptionGql(input);
        let {subscriptionType} = input
        if(subscription.price !== res.price) {
            let subscriptionTmp = await retrieveSubscriptionGql()
            setSubscription(subscriptionTmp);
            let loggedInUserTmp = {...loggedInUser};
            loggedInUserTmp.subscriptionType = subscriptionType
            dispatch(setUser(loggedInUserTmp));
        }
        //
        return res;
    }

    const calculateProration = async (input) => {
        let proration = await calculateProrationGql(input);
        return proration;
    }

    const retrieveOnSuccess = async () => {
        let subscriptionTmp = await retrieveSubscriptionGql()
        setSubscription(subscriptionTmp);

    }

    let propsChild = {
        calculateProration,
        retrieveOnSuccess,
        subscription,
        updateSubscription
    }

    return (
        <SubscribeDashboardPage {...propsChild} displayTitle={location.pathname === "/subscribe"} vendor={loggedInUser}  />
    )
}