import * as React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) =>
  createStyles({
    grow: {
      flexGrow: 1
    },
    root: {

    },
    colorPrimary: {
        color: theme.colors.almond
    }
  }),
);


export default function LoadingPage(props) {
    const classes = useStyles();

  return (<Grid
    container
    spacing={0}
    direction="column"
    alignItems="center"
    justify="space-between"
    className={classes.root}
  >
  <div className={classes.grow}/>
      <Grid item xs={12} >
      <CircularProgress 
        color="primary"
        classes={{ colorPrimary: classes.colorPrimary}} />
        </Grid>
        <div className={classes.grow}/>
    </Grid>
  );
}
