import React, { useState, useEffect } from 'react';

import VendorListPage from './VendorListPage';

import { useDispatch, useSelector } from "react-redux";

import { setSidebar } from '../../shared/store/actions/sidebar.actions';

import routerHistory from '../../shared/router-history/router-history';

import * as _ from 'lodash';

import { useLocation} from 'react-router-dom';

import { toast } from 'react-toastify';

import { Trans } from 'react-i18next';

import queryString from 'query-string';

import { categoriesGql } from '../../graphQL/category'
import { keywordsGql } from '../../graphQL/keyword'

import { rewardsGql } from '../../graphQL/reward'
import { setRewards as setRewardsInternal} from '../../shared/store/actions/reward.actions';

import { gendersGql } from '../../graphQL/gender';
import { setGenders as setGendersInternal} from '../../shared/store/actions/gender.actions';

import { vendorsLimitedGql, aroundPositionVendorsIdsByKmsGql, vendorsAssimilateGql } from '../../graphQL/vendor'
import { setVendors as setVendorsInternal, setAroundPositionVendorsIdsByKms as setAroundPositionVendorsIdsByKmsInternal } from '../../shared/store/actions/vendor.actions';

import { useTranslation } from 'react-i18next';

import { locationsByCityGql, locationsByCountyGql } from '../../graphQL/location'

import { filter } from '../../shared/utils/filter'

export default function VendorListPageContainer() {
  const dispatch = useDispatch();
  const location = useLocation();
  let search = queryString.parse(location.search)
  let searchName = search["name"] ? search["name"].toString() : '';
  let aroundMePath = !!search["aroundMe"]

  let cat = []
  if(search["cat"]) {
    cat.push(search["cat"].toString())
  }

  let kw = []
  if(search["kw"]) {
    kw.push(search["kw"].toString());
  }

  let gd = []
  if(search["gd"]) {
    gd.push(search["gd"].toString());
  }


  let city = []
  if(search["city"]) {
    city.push(search["city"].toString());
  }

  const toProfile = () => {
    routerHistory.push('/user');
    window.scrollTo(0, 0);
  }

  const { t } = useTranslation();

  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  //const locationsFilter = useSelector((state) => state.location.locationsFilter); 
  const locations = useSelector((state) => state.location.locationsFilter);    
  const vendorsInternal = useSelector((state) => state.vendor.vendors);
  const categoriesInternal = useSelector((state) => state.category.categories);
  const keywordsInternal = useSelector((state) => state.keyword.keywords);
  const aroundPositionVendorsIdsByKmsInternal = useSelector((state) => state.vendor.aroundPositionVendorsIdsByKms);
  const gendersInternal = useSelector((state) => state.gender.genders);
  const rewardsInternal = useSelector((state) => state.reward.rewards);
  
  const [suggestions, setSuggestions] = useState([]);
  const [countiesAndCitiesSuggestions, setCountiesAndCitiesSuggestions] = useState([]);

  const geoloc = useSelector((state) => state.user.position);
  const geolocAccepted = useSelector((state) => state.user.geolocAccepted)
  
  const [filteredVendors, setFilteredVendors] = useState(null);
  const [categories, setCategories] = useState([]);
  const [keywords, setKeywords] = useState([]);
  const [genders, setGenders] = useState([]);
  const [rewards, setRewards] = useState([]);
  const [aroundVendorsIds, setAroundVendorsIds] = useState([]);
  //const [onlyOnlineVendorsIds, setOnlyOnlineVendorsIds] = useState([]);
  const [vendors, setVendors] = useState(null);
  const [loadingState, setLoadingState] = useState(true);
  const [cityChecked, setCityChecked] = useState(aroundMePath);
  const [cityDistance, setCityDistance] = useState(30);
  const [onlineVendors, setOnlineVendors] = useState(false);
  const [rewardChecked, setRewardChecked] = useState(false);
  //const [citiesSuggestions, setCitiesSuggestions] = React.useState([]);
  const [searchFilters, setSearchFilters] = useState({cat: cat, kw: kw, gd: gd, city: city, searchName: searchName});
  const [clickAndCollectVendors, setClickAndCollectVendors] = useState(false);
  const [vendorsAssimilate, setVendorsAssimilate] = useState([]);

  useEffect(() => {
    let didCancel = false
    if(!didCancel && searchFilters.searchName !== searchName){
      setSearchFilters({
        ...searchFilters,
        searchName: searchName
      })
    }

    return () => { didCancel = true }
  }, [searchName])

  useEffect(() => {
    let didCancel = false

    async function updateLoggedInUserAssimilateVendors() {
      let keywords = loggedInUser.keywords.map((el) => el.id)
      let vendorsAssimilate = await vendorsAssimilateGql({categories: [], keywords, vendorId: null, limit: 3});
      !didCancel && setVendorsAssimilate(vendorsAssimilate)
    }

    if(loggedInUser && loggedInUser.keywords && loggedInUser.keywords.length > 0) {
      updateLoggedInUserAssimilateVendors();
    }

    return () => { didCancel = true }
  }, [loggedInUser])

  useEffect(() => {
    let didCancel = false

    async function retrieveCategories() {
      let categories;
      if(categoriesInternal.length > 0) {
        categories = categoriesInternal;
      } else {
        categories = await categoriesGql();
      }
      !didCancel && setCategories(categories)
    }

    async function retrieveKeywords() {
      let keywords;
      if(keywordsInternal.length > 0) {
        keywords = keywordsInternal;
      } else {
        keywords = await keywordsGql();
      }
      !didCancel && setKeywords(keywords)
    }

    async function retrieveGenders() {
      let genders; 
      if(gendersInternal.length > 0) {
        genders = gendersInternal;
      } else {
        genders = await gendersGql();
        !didCancel && dispatch(setGendersInternal(genders));
      }
      !didCancel && setGenders(genders);
    }

    async function retrieveRewards() {
      let rewards; 
      if(rewardsInternal.length > 0) {
        rewards = rewardsInternal;
      } else {
        rewards = await rewardsGql();
        !didCancel && dispatch(setRewardsInternal(rewards));
      }
      !didCancel && setRewards(rewards);
    }

    async function retrieveVendors() {
      let vendors;
      if(vendorsInternal.length > 0 ) {
        vendors = vendorsInternal;
      } else {
        vendors = await vendorsLimitedGql();
      }
      !didCancel && setVendors(vendors);
      !didCancel && vendorsInternal.length <= 0 && dispatch(setVendorsInternal(vendors))
    }

    async function retrieveLocations() {
      let locationsCities = await locationsByCityGql();
      let locationsCounties = await locationsByCountyGql();
      locationsCities = locationsCities.map((loc) => {return {vendors: loc.vendors, loc: loc.city}})
      locationsCounties = locationsCounties.map((loc) => {return {vendors: loc.vendors, loc: loc.county}})
      let suggestionsCitiesCounties = _.unionBy(locationsCities, locationsCounties, 'loc');
      !didCancel && setCountiesAndCitiesSuggestions(suggestionsCitiesCounties);
      let suggestions = suggestionsCitiesCounties.map((el, index) => {
        return {
          value: el.loc,
          name: el.loc
        }
      });
      
      !didCancel && setSuggestions(suggestions);

    }

    Promise.all([retrieveCategories(), retrieveKeywords(), retrieveGenders(), retrieveVendors(), retrieveLocations(), retrieveRewards()])

    return () => { didCancel = true }
  }, [])

  useEffect(() => {
    let didCancel = false
    if(vendors) {
      let vendorsTmp = filter(vendors, searchFilters, countiesAndCitiesSuggestions, {cityChecked, onlineVendors, clickAndCollectVendors, aroundVendorsIds, rewardChecked}, rewards)
      if(!filteredVendors || (vendorsTmp.length !== filteredVendors.length && vendorsTmp !== filteredVendors)) {
        !didCancel && setFilteredVendors(vendorsTmp);
      }
      !didCancel && setLoadingState(false);
    } else {
      !didCancel && setLoadingState(true);
    }


    return () => { didCancel = true }

  }, [vendors, cityChecked, cityDistance, aroundVendorsIds, onlineVendors, rewardChecked, searchFilters, clickAndCollectVendors]);


  const constructFilterArray = (field) => {
    let fieldToSearch = searchFilters[field];
    return fieldToSearch ? _.isArray(fieldToSearch) ? fieldToSearch : [fieldToSearch] : []
  }

  const toggleCategoryFilter = (category, checked) => {
    if(category.id) { 

      setLoadingState(true);
      let categories = constructFilterArray('cat');
      
      if(checked) {
        categories.push(category.id);
      } else {
        _.remove(categories, (n) => n === category.id)
      }
      setSearchFilters({
          ...searchFilters,
          cat: categories
        })
    }
  }

  const toggleKeywordFilter = (keyword, checked) => {
    if(keyword.id)  { 

    setLoadingState(true);
      let keywords = constructFilterArray('kw');
      if(checked) {
        keywords.push(keyword.id);
      } else {
        _.remove(keywords, (n) => n === keyword.id)
      }
      setSearchFilters({
          ...searchFilters,
          kw: keywords
        })
    }
  }

  const toggleGenderFilter = (gender, checked) => {
    if(gender.id)  { 

    setLoadingState(true);
      let genders = constructFilterArray('gd');
      if(checked) {
        genders.push(gender.id);
      } else {
        _.remove(genders, (n) => n === gender.id)
      }
      setSearchFilters({
          ...searchFilters,
          gd: genders
        })
    }
  }


  const toggleCityFilter = (city, checked) => {
    if(city)  { 
      setLoadingState(true);
      let cities = constructFilterArray('city');
      if(checked) {
        cities.push(city);
        let suggestionsTmp = suggestions;
        _.remove(suggestionsTmp, (c) => c.value === city);
        setSuggestions(suggestionsTmp);
      } else {
        _.remove(cities, (n) => n === city)
        let suggestionsTmp = suggestions;
        suggestionsTmp.push({
          value: city,
          name: city
        })        
        setSuggestions(suggestionsTmp);
      }
      setSearchFilters({
          ...searchFilters,
          city: cities
        })
    }
  }

  const dropCityFilter = () => {
    setLoadingState(true);
    setSearchFilters({
      ...searchFilters,
      city: []
    })
  }


  const updatePositionCheck = async (geolocChecked, geolocTmp) => {
    setLoadingState(true);
    let geolocToSend = geoloc ? geoloc : geolocTmp
    if(geolocChecked && geolocToSend) {
      let vendorsIds;
      if(aroundPositionVendorsIdsByKmsInternal.length > 0) {
        vendorsIds = aroundPositionVendorsIdsByKmsInternal[3];
      } else {
        let vendorsIdsByKms = await aroundPositionVendorsIdsByKmsGql({position: geolocToSend});
        dispatch(setAroundPositionVendorsIdsByKmsInternal(vendorsIdsByKms));
        vendorsIds = vendorsIdsByKms[3];
      }
      setAroundVendorsIds(vendorsIds);
    } else {
      setCityDistance(30)
    }
    setCityChecked(geolocChecked);
    
  }

  const updateOnlineVendorCheck = (onlineVendors) => {
    setLoadingState(true);
    setOnlineVendors(onlineVendors);
  }

  const updateClickAndCollectVendorCheck = (clickAndCollectVendors) => {
    setLoadingState(true);
    setClickAndCollectVendors(clickAndCollectVendors);
  }

  const updateRewardCheck = (reward) => {
    setLoadingState(true);
    setRewardChecked(reward);
  }

  const updatePositionDistance = async (distance) => {
    if(geolocAccepted) {
      let vendorsIds;
      let kms = [5, 10, 20, 30];
      if(aroundPositionVendorsIdsByKmsInternal.length > 0) {
        vendorsIds = aroundPositionVendorsIdsByKmsInternal[kms.indexOf(distance)];
      } else {
        let vendorsIdsByKms = await aroundPositionVendorsIdsByKmsGql({position: geoloc});
        dispatch(setAroundPositionVendorsIdsByKmsInternal(vendorsIdsByKms));
        vendorsIds = vendorsIdsByKms[kms.indexOf(distance)];
      }
      setAroundVendorsIds(vendorsIds);
      setCityDistance(distance)
    } else { 
      setCityDistance(distance)
    }
  }

  const dispatchSideBar= (open) => {
    dispatch(setSidebar(open));
  }

  const fillUserValues = (fillUser) => {
    if(loggedInUser) {
      setLoadingState(true);
      if(fillUser) {
        let sF = {...searchFilters};
        let previousKw = sF.kw || [];
  
        let keywordsId = keywords.map((el) => el.id);
        let keywordsUserId = loggedInUser.keywords ? loggedInUser.keywords.filter((el) => keywordsId.includes(el.id)).map((el) => el.id) : [];
        if(keywordsUserId.length <= 0) {
          const Msg = () => (
            <Trans i18nKey="vendorList.noValues.message"
                  values={{ linkText: t('vendorList.noValues.linkText')}}
                  components={[<span style={{color: "white", fontWeight: "bold", textDecoration: "underline"}} onClick={toProfile}></span>]}/>
          )
          toast.info(<Msg />, {autoClose: false});
        }
        sF.kw = _.union(keywordsUserId, previousKw)
        setSearchFilters(sF);
      } else {
        let sF = {...searchFilters};
        let previousKw = sF.kw || [];
  
        let keywordsId = keywords.map((el) => el.id);
        let keywordsUserId = loggedInUser.keywords ? loggedInUser.keywords.filter((el) => keywordsId.includes(el.id)).map((el) => el.id) : []
  
        _.remove(previousKw, (kw) => {
          return keywordsUserId.includes(kw)})
        
        sF.kw = previousKw;
        setSearchFilters(sF);
      }
    }
  }

  const removeSearch = () => {
    routerHistory.push(`/vendors`);
  }

  return (<VendorListPage
           vendors={filteredVendors}
           filterUrlArgs={searchFilters}
           categories={categories}
           keywords={keywords}
           genders={genders}
           locations={locations}
           handleToggleCategory={toggleCategoryFilter}
           handleToggleKeyword={toggleKeywordFilter}
           handleToggleGender={toggleGenderFilter}
           handleToggleCity={toggleCityFilter}
           dispatchSideBar={dispatchSideBar}
           geoloc={geoloc}
           geolocAccepted={geolocAccepted}
           handlePositionCheck={updatePositionCheck}
           handlePositionDistance={updatePositionDistance}
           handleOnlineVendorCheck={updateOnlineVendorCheck}
           handleClickAndCollectVendorCheck={updateClickAndCollectVendorCheck}
           handleRewardCheck={updateRewardCheck}
           dropCityFilter={dropCityFilter}
           loadingState={loadingState}
           //citiesSuggestions={citiesSuggestions}
           toggleUserValues={fillUserValues}
           isLogged={!!loggedInUser}
           suggestions={suggestions}
           countiesAndCitiesSuggestions={countiesAndCitiesSuggestions}
           removeSearch={removeSearch}
           vendorsAssimilate={vendorsAssimilate}
           aroundMePath={aroundMePath}
          />);

}