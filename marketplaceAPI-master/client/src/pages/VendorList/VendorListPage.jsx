import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme, createStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import FilterListIcon from '@material-ui/icons/FilterList';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
//import SearchIcon from '@material-ui/icons/Search';

import VendorCard from '../../components/VendorCard/VendorCard';
import Loading from '../Loading/LoadingPage';

import Grid from '@material-ui/core/Grid';
//import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import Chip from '@material-ui/core/Chip';

import * as _ from 'lodash';

import Pagination from '@material-ui/lab/Pagination';

import { NavLink } from 'react-router-dom';

import useMediaQuery from '@material-ui/core/useMediaQuery';

import { useTranslation } from 'react-i18next';

import FilterCard from '../../components/FilterCard/FilterCard';
import Carousel from "../../components/Carousel/Carousel";

import Avatar from '@material-ui/core/Avatar';

import SidebarFilter from '../../components/SidebarFilter/SidebarFilter'

import ReactGA from 'react-ga';

ReactGA.pageview('/vendors');

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: 'flex'
    },
    drawer: (props) => ({
      width: props.drawerWidth,
      flexShrink: 0,
    }),
    drawerPaper: (props) => ({
      width: props.drawerWidthPaper,
    }),
    drawerHeader: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
      justifyContent: 'flex-end',
    },
    content: (props) => ({
      width: "45%",
      flexGrow: 1,
      padding: theme.spacing(3),
      paddingTop: '0px',
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: -`calc(${props.content}/2)`,
    }),
    contentShift: {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    },
    chip: {
      margin: '5px'
    },
    filterCat: {
      marginRight: '20px'
    },
    noCards: {
      width: "100%"
    },
    emptyState: {
      textAlign: "center",
      "& img": {
        width: "50px"
      },
      "& a": {
        color: "grey"
      }
    },
    loadingState: {
      textAlign: "center"
    },
    gendersBar: {
      //margin: "auto",
      marginTop: "0px",
      marginBottom: "0px"
    },
    categoriesBar: {
      margin: "auto",
      marginTop: "20px",
      textAlign: "center"
    },
    colorSecondary: {
      color: "white !important",
      backgroundColor: `white !important`,
      "& hover": {
          backgroundColor: `${theme.colors.strawberry} !important`,
      },
      //marginRight: "5px",
      padding: "5px",

      width: '45px',
      height: '45px'
    },
    colorSecondarySelected: {
      color: "white !important",
      backgroundColor: `${theme.colors.strawberry} !important`,
      "& hover": {
          backgroundColor: `${theme.colors.strawberry} !important`,
      },
      //marginRight: "5px",
      padding: "5px",
      paddingRight: "4px",
      paddingLeft: "4px",
      width: "44px",
      paddingTop: "4px",
      height: "44px"
    },
    flagSmall: {
      width: '40px',
      height: '40px'
    },
    vendorsAssimilate: {
      width: "100%",
      fontWeight: "bold",
      textAlign: "center",
      fontSize: "18px",
      marginTop: "20px"
    }
  }),
);

export default function VendorListPage(props) {
  const matches = useMediaQuery('(max-width:768px)');

  const { t, i18n } = useTranslation();

  const theme = useTheme();
  
  const [open, setOpen] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [cityChecked, setCityChecked] = useState(false);
  const [clickAndCollectVendors, setClickAndCollectVendors] = useState(false);
  const [onlineVendors, setOnlineVendors] = useState(false);
  const [reward, setReward] = useState(false);

  const handleChangePage = (event, newPage) => {
    setCurrentPage(newPage);
  };

  let classes = useStyles({
    drawerWidth: matches ? "0px" : open ? "5%" : "0%", 
    drawerWidthPaper: matches ? "100%" : "20%", 
    content: matches ? "240px" : "20%"
  });

  useEffect(() => {
    setOpen(!matches);

    props.dispatchSideBar(!matches);
  }, [matches]);

  useEffect(() => {
    return () => {
      props.dispatchSideBar(false);
    }
  }, []);

  const handleDrawerOpen = () => {
    setOpen(true);
    props.dispatchSideBar(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
    props.dispatchSideBar(false);
  };

  const handleCityDelete = (event) => {
    setCityChecked(false);
    props.handlePositionCheck(false);
    props.dropCityFilter();
    setCurrentPage(1);
    ReactGA.event({
      category: 'Geolocation',
      action: `Disable display vendors around me`,
      label: 'Vendors'
    });
  }

  const handleSearchDelete = () => {
    props.removeSearch();
    setCurrentPage(1);
    ReactGA.event({
      category: 'Search',
      action: `Clear the search ${props.filterUrlArgs.searchName}`,
      label: 'Vendors'
    });
  }

  const handleOnlineVendorsDelete = (event) => {
    setOnlineVendors(false);
    props.handleOnlineVendorCheck(false);
    setCurrentPage(1);
    ReactGA.event({
      category: 'Filter',
      action: `Disable filter by online vendors`,
      label: 'Vendors'
    });
  };

  const handleClickAndCollectVendorsDelete = (event) => {
    setClickAndCollectVendors(false);
    props.handleClickAndCollectVendorCheck(false);
    setCurrentPage(1);
    ReactGA.event({
      category: 'Filter',
      action: `Disable filter by click and collect vendors`,
      label: 'Vendors'
    });
  };


  const handleRewardDelete = (event) => {
    setReward(false);
    props.handleRewardCheck(false);
    setCurrentPage(1);
    ReactGA.event({
      category: 'Filter',
      action: `Disable filter by rewards`,
      label: 'Vendors'
    });
  };

  const constructFilterArray = (field) => {
    let fieldToSearch = props.filterUrlArgs[field];
    return fieldToSearch ? _.isArray(fieldToSearch) ? fieldToSearch : [fieldToSearch] : []
  }

  let currentVendors = _.chunk(props.vendors, 9);
  const vendorCards = currentVendors.length > 0 ? currentVendors[currentPage - 1].map(vendor => {
    return <Grid item xs={12} sm={4} key={vendor.id}>
            <VendorCard
              vendor={vendor}
            />
          </Grid> 
  }) : [];

  let removeKw = (keyword) => {
    props.handleToggleKeyword(keyword, false); 
    setCurrentPage(1);
    ReactGA.event({
      category: 'Keywords',
      action: `Remove filter by value ${keyword}`,
      label: 'Vendors'
    });
  }

  let filterKw = constructFilterArray('kw')
  const chipsKw = [];
    filterKw.forEach((el) => {
      let keyword = props.keywords.find((kw) => kw.id === el );
      if(keyword) {
        let keyName = keyword.lang.find((l) => l.locale === i18n.language).value;
        chipsKw.push( 
          <Chip
            key={keyword.id}
            label={keyName}
            className={classes.chip}
            variant="outlined"
            onDelete={() => removeKw(keyword)}
          />
          )
      }
    })


  let removeCity = (city) => {
    props.handleToggleCity(city, false); 
    setCurrentPage(1);
    ReactGA.event({
      category: 'Cities',
      action: `Remove filter by city ${city}`,
      label: 'Vendors'
    });
  }

    let filterCity = constructFilterArray('city')
    const chipsCity = filterCity.map((city) => {
      
        return ( 
          <Chip
            key={city}
            label={city}
            className={classes.chip}
            variant="outlined"
            onDelete={() => removeCity(city)}
          />
          )
      }
    );

    
  let filterGd = constructFilterArray('gd');
  let filterCat = constructFilterArray('cat')

  const GADetectClick = (category, action) => {
    ReactGA.event({
      category: category,
      action: action,
      label: 'Vendors'
    });
  }

  const returnCard = (element, id, GAtitle, GAsubTitle, xs, sm) => {
    return (
      <Grid item xs={xs} sm={sm} key={id} onClick={() => GADetectClick(GAtitle, GAsubTitle)}>
        {element}
      </Grid>
    )
  }

  return (
    <div className={classes.root}>
      
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />
        <SidebarFilter from="Vendors" catCheckboxes={false} gdCheckboxes={false}
            setCityChecked={setCityChecked}
            cityChecked={cityChecked}
            setOnlineVendors={setOnlineVendors}
            setClickAndCollectVendors={setClickAndCollectVendors}
            setReward={setReward}
            onlineVendors={onlineVendors}
            clickAndCollectVendors={clickAndCollectVendors}
            reward={reward}
            setCurrentPage={setCurrentPage}
            {...props}/>
      </Drawer>
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >


        {
        matches ? 
          <Carousel carouselComponent={props.genders.map((gender) => { 
            let activated = filterGd.includes(gender.id);
            return returnCard(<FilterCard filter={gender} type="gd" round={true} handleClick={() => props.handleToggleGender(gender, !activated) } selectStyle={true} activated={activated}/>, gender.id, 'Genders',  `${activated ? 'Filter': 'Remove filter'} by gender ${gender.name}`, 4, 4)
          })} 
          arrows={true} infinite={true} slides={3}/>
          : <Grid container justify="space-between" item spacing={3} className={classes.categoriesBar}>
            {
              props.genders.map((gender) => {
                let activated = filterGd.includes(gender.id);
                return returnCard(<FilterCard filter={gender} type="gd"  round={true} handleClick={() => props.handleToggleGender(gender, !activated) } selectStyle={true} activated={activated} height="150px"/>, gender.id, 'Genders',   `${activated ? 'Filter': 'Remove filter'} by gender ${gender.name}`, 6, 3)
              })
            }
          </Grid>
      }

      {
        matches ? 
          <Grid container justify="space-evenly" item spacing={3} className={classes.gendersBar}>
            {
              props.categories.map((cat) => {
              let activated = filterCat.includes(cat.id);
              return (<IconButton
                  key={cat.id}
                  onClick={() => props.handleToggleCategory(cat, !activated)}
                  color="secondary" 
                  classes={{colorSecondary: activated ? classes.colorSecondarySelected: classes.colorSecondary}}
                >
                  <Avatar 
                    className={classes.flagSmall} 
                    src={`/assets/images/icons/${cat.name}.png`} />
              </IconButton>)
              })
            }
            
          </Grid>
          : <Grid container justify="space-between" item spacing={3} className={classes.gendersBar}>
            { props.categories.map((cat) => { 
              let activated = filterCat.includes(cat.id);
              return returnCard(<FilterCard filter={cat} type="cat"  height="150px" handleClick={() => props.handleToggleCategory(cat, !activated) } selectStyle={true} activated={activated}/>, cat.id, 'Categories',  `${activated ? 'Filter': 'Remove filter' } by category ${cat.name}`, 6, 3)
            })}
          </Grid>
      }

        <div className={classes.drawerHeader} >
        
          <Grid container direction="row" justify="flex-start" className={classes.filterCat}>
            <Grid item xs={12}>
              {
                !open && 

                <Fab size="small"  
                variant="extended" 
                onClick={handleDrawerOpen}>
                  <FilterListIcon />
                  {!matches && t('vendorList.filterButton')}
                </Fab>
              }
                {chipsKw}
                {chipsCity}
                {onlineVendors &&
                  <Chip
                    key="onlineVendors"
                    label={t('vendorList.onlineShop')}
                    className={classes.chip}
                    variant="outlined"
                    onDelete={handleOnlineVendorsDelete}
                  />
                }

              {clickAndCollectVendors &&
                <Chip
                  key="clickAndCollectVendors"
                  label={t('vendorList.clickAndCollect')}
                  className={classes.chip}
                  variant="outlined"
                  onDelete={handleClickAndCollectVendorsDelete}
                />
              }
                {reward &&
                  <Chip
                    key="reward"
                    label={t('vendorList.reward')}
                    className={classes.chip}
                    variant="outlined"
                    onDelete={handleRewardDelete}
                  />
                }
                {cityChecked &&
                  <Chip
                    key="aroundMe"
                    label={t('vendorList.aroundMe')}
                    className={classes.chip}
                    variant="outlined"
                    onDelete={handleCityDelete}
                  />
                }
                {props.filterUrlArgs && props.filterUrlArgs.searchName &&
                  <Chip
                    key="searchName"
                    label={`${t('navbar.search')} : ${props.filterUrlArgs.searchName}`}
                    className={classes.chip}
                    variant="outlined"
                    onDelete={handleSearchDelete}
                  />
                }
            </Grid> 
            </Grid>
        </div>  
        { props.loadingState || !props.vendors ?
          <Grid container>   
              <div className={classes.noCards}>
                <div className={classes.loadingState}>
                <Loading/>
              </div>
            </div>
          </Grid>
          :
            props.vendors && props.vendors.length > 0 ? 
              <Grid container>   
                {vendorCards}
                {
                  Math.ceil(props.vendors.length / 9) > 1 &&
                  <Grid container justify="center" item xs={12}>
                    <Pagination 
                      count={Math.ceil(props.vendors.length / 9)} 
                      page={currentPage} 
                      onChange={handleChangePage} 
                      variant="outlined" 
                      shape="rounded" 
                    />
                  </Grid>
                }
                
              </Grid>
              :
              <Grid container>   
                <div className={classes.noCards}>
                    <div className={classes.emptyState}>
                      <img alt="" src="/assets/images/icons/maps.png"/>
                      <br/><br/>
                      <h3>{t('vendorList.notFound.title')}</h3>
                      <p>{t('vendorList.notFound.subTitle')}</p>
                      <NavLink to="/suggestBrand">{t('vendorList.notFound.link')}</NavLink>
                    </div>
                </div>
                <div className={classes.vendorsAssimilate}>{t('home.vendorsAssimilate')}</div>
                    {
                      props.vendorsAssimilate && props.vendorsAssimilate.length > 0 &&
                      props.vendorsAssimilate.map((vendor) => 
                        <Grid item xs={12} sm={4} key={vendor.id}>
                          <VendorCard
                            vendor={vendor}
                          />
                        </Grid> 
                      )
                    }
              </Grid>
        }
      </main>
    </div>
  );
}