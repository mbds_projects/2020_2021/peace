import React, { useState, useEffect } from 'react';

import VendorAccountPage from './VendorAccountPage'

import { toast } from 'react-toastify';

import { toastError } from '../../shared/utils/error';

import { retrieveSubscriptionGql, cancelSubscriptionGql } from '../../graphQL/subscriptionDashboard';
import { retrieveAllInvoicesGql, uploadProfilePictureGql, uploadLogoGql, updateDeliverInfoVendorGql, vendorAccountGql } from '../../graphQL/vendorAccount';

import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from "react-redux";
import { setUser } from '../../shared/store/actions/authentication.actions';
export default function VendorAccountPageContainer(props) {

    const { t } = useTranslation();
    const dispatch = useDispatch();
    const [subscription, setSubscription] = useState(null);
    const [vendor, setVendor] = useState(null);
    const [invoices, setInvoices] = useState(null);
    const [loading, setLoading] = useState(null);

    const loggedInUser = useSelector((state) => state.authentication.currentUser);

    useEffect(() => {
        window.scrollTo(0, 0);

        let didCancel = false

        async function retrieveSubscription() {
            !didCancel && setLoading(true);
            let subscriptionTmp = await retrieveSubscriptionGql()
            !didCancel && setSubscription(subscriptionTmp);
            !didCancel && setLoading(false);
        }


        async function retrieveAllInvoices() {
            !didCancel && setLoading(true);
            let invoicesTmp = await retrieveAllInvoicesGql()
            !didCancel && setInvoices(invoicesTmp);
            !didCancel && setLoading(false);
        }

        Promise.all([retrieveSubscription(), retrieveAllInvoices()])

        return () => { didCancel = true }
    }, [])

    useEffect(() => {
        let didCancel = false
        async function retrieveVendor() {
            !didCancel && setLoading(true);
            let vendorTmp = await vendorAccountGql()
            !didCancel && setVendor(vendorTmp);
            !didCancel && setLoading(false);
        }

        if (loggedInUser && loggedInUser.id) {
            retrieveVendor();
        }

        return () => { didCancel = true }
    }, [loggedInUser])

    const updateVendorData = async () => {
        let vendorTmp = await vendorAccountGql()
        setVendor(vendorTmp);
        return vendorTmp;
    }

    const uploadProfilePicture = async (file) => {
        try {
            let res = await uploadProfilePictureGql({ file });
            if (res) {
                toast.success(t('vendorDashboard.toast.imageUploaded'), {
                    position: toast.POSITION.TOP_RIGHT
                });
                await updateVendorData();
            } else {
                toast.error(t('vendorDashboard.toast.imageUploadedError'), {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
            return res;
        } catch (e) {
            toastError(e.message, t);
        }
    }

    const uploadLogo = async (file) => {
        try {
            let res = await uploadLogoGql({ file });
            if (res) {
                toast.success(t('vendorDashboard.toast.imageUploaded'), {
                    position: toast.POSITION.TOP_RIGHT
                });
                let vendorUpdated = await updateVendorData();
                let loggedInUserTmp = {...loggedInUser};
                loggedInUserTmp.logoUrl = vendorUpdated.logoUrl
                dispatch(setUser(loggedInUserTmp));
            } else {
                toast.error(t('vendorDashboard.toast.imageUploadedError'), {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
            return res;
        } catch (e) {
            toastError(e.message, t);
        }
    }


    const updateDeliverInfoVendor = async (input) => {
        try {
            let res = await updateDeliverInfoVendorGql({ input });
            if (res) {
                toast.success(t('vendorAccount.toast.deliverInfoUpdated'), {
                    position: toast.POSITION.TOP_RIGHT
                });
                await updateVendorData();
            } else {
                toast.error(t('vendorAccount.toast.deliverInfoUpdatedError'), {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
            return res;
        } catch (e) {
            toastError(e.message, t);
        }
    }

    const cancelSubscription = async () => {
        let canceled = await cancelSubscriptionGql();
        if (canceled) {
            toast.success(t('vendorAccount.toast.canceled'), {
                position: toast.POSITION.TOP_RIGHT
            });

            let subscriptionTmp = await retrieveSubscriptionGql()
            setSubscription(subscriptionTmp);
        } else {
            toast.error(t('vendorAccount.toast.canceledError'), {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }

    let propsChild = { loading, uploadLogo, loggedInUser, subscription, invoices, cancelSubscription, vendor, uploadProfilePicture, updateDeliverInfoVendor }
    return (
        <VendorAccountPage {...propsChild} />
    )
}