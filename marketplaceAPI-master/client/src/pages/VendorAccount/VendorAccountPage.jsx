import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import ReactGA from 'react-ga';

import ReceiptIcon from '@material-ui/icons/Receipt';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import SaveIcon from '@material-ui/icons/Save';

import routerHistory from '../../shared/router-history/router-history';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';

import DialogUpdateProfilePicture from '../../components/DialogDashboard/DialogUpdateProfilePicture';
import DialogUpdateLogo from '../../components/DialogDashboard/DialogUpdateLogo';

import { NavLink } from 'react-router-dom';
import { Trans } from 'react-i18next';
import { makeStyles, createStyles } from '@material-ui/core/styles';

import { format, formatDistance, compareAsc } from 'date-fns'

import frLocale from "date-fns/locale/fr";
import enLocale from "date-fns/locale/en-GB";
import VendorCard from '../../components/VendorCard/VendorCard';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import { useTranslation } from 'react-i18next';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';

import CardMembershipIcon from '@material-ui/icons/CardMembership';
import WarningIcon from '@material-ui/icons/Warning';

import Button from '@material-ui/core/Button';

import Switch from '@material-ui/core/Switch';

import Loading from '../../pages/Loading/LoadingPage';

import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Link from '@material-ui/core/Link';

ReactGA.pageview('/vendorAccount');

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      width: "99%",
      "& body": {
        fontFamily: "'Open Sans', sans-serif",
        "-webkit-font-smoothing": "antialiased",
        "-moz-osx-font-smoothing": "grayscale"
      },
      "& .MuiPaper-root": {
        padding: "20px",
        margin: "25px 15px"
      }
    },
    cardHeaderAction: {
      "& .MuiCardHeader-action": {
        marginTop: "0px",
        marginBottom: "0px"
      }
    },
    warningIcon: {
      position: "relative",
      top: "5px",
      right: "5px",
      color: "gold"
    },
    margin: {
      margin: "25px 15px"
    },
    avatar: {
      width: "100px !important",
      height: "100px !important",
      margin: "auto",
      fontSize: "50px",
      border: "2px solid lightgrey"
    },
    inputUpload: {
      display: "none"
    },
    statePublished: {
      backgroundColor: theme.colors.almond,
      textAlign: "center"
    },
    stateNotPublished: {
      backgroundColor: theme.colors.gold,
      textAlign: "center"
    },
    stateInactivated: {
      backgroundColor: theme.colors.strawberry,
      textAlign: "center"
    },
    state_trialing: {
      backgroundColor: theme.colors.almond,
      fontWeight: "bolder",
      color: "white",
      padding: "5px",
    },
    state_active: {
      backgroundColor: theme.colors.almond,
      fontWeight: "bolder",
      color: "white",
      padding: "5px",
    },
    state_canceled: {
      backgroundColor: theme.colors.gold,
      fontWeight: "bolder",
      color: "white",
      padding: "5px",
    },
    state_incomplete: {
      backgroundColor: "red",
      fontWeight: "bolder",
      color: "white",
      padding: "5px",
    },
    state_incomplete_expired: {
      backgroundColor: "red",
      fontWeight: "bolder",
      color: "white",
      padding: "5px",
    },
    state_past_due: {
      backgroundColor: "red",
      fontWeight: "bolder",
      color: "white",
      padding: "5px",
    },
    state_unpaid: {
      backgroundColor: "red",
      fontWeight: "bolder",
      color: "white",
      padding: "5px",
    },
    state_expired: {
      backgroundColor: "red",
      fontWeight: "bolder",
      color: "white",
      padding: "5px",
    },
    state_disabled: {
      backgroundColor: "red",
      fontWeight: "bolder",
      color: "white",
      padding: "5px",
    },
  }),
);

const locale = {
  fr: frLocale,
  en: enLocale
};

export default function VendorAccountPage(props) {
  const classes = useStyles();
  const { t, i18n } = useTranslation();

  const [activateSubDialog, setActivateSubDialog] = useState(false);
  const [openEditProfilePicture, setOpenEditProfilePicture] = useState(false);
  const [openEditLogo, setOpenEditLogo] = useState(false);

  const [state, setState] = useState({
    onlineShop: false,
    clickAndCollect: false,
  });

  useEffect(() => {
    if (props.vendor) {
      let vendorState = {
        onlineShop: props.vendor.onlineShop,
        clickAndCollect: props.vendor.clickAndCollect
      }
      setState(vendorState);
    }
  }, [props.vendor])

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  let imgSrc = "";
  let avatarText = '';
  if (props.vendor) {
    if (props.vendor.logoUrl) imgSrc = props.vendor.logoUrl
    else {
      avatarText = `${props.loggedInUser.name.charAt(0)}`
    }
  }
  const goToSubscription = () => {
    routerHistory.push('/subscribe');
  }

  const cancelSubscription = () => {
    props.cancelSubscription();
    setActivateSubDialog(false);
  }

  let dialogProfilePictureProps = {
    openEditProfilePicture,
    setOpenEditProfilePicture,
    uploadProfilePicture: props.uploadProfilePicture,
    vendor: props.vendor
  }
  let dialogLogoProps = {
    openEditLogo,
    setOpenEditLogo,
    uploadLogo: props.uploadLogo,
    vendor: props.vendor
  }

  return (
    props.loading ?
      <Loading />
      :
      <div className={classes.root}>

        <DialogUpdateProfilePicture {...dialogProfilePictureProps} />
        <DialogUpdateLogo {...dialogLogoProps} />
        <Grid container>

          <Grid container direction="column" item sm={4} xs={12}>
            {
              props.vendor &&
              <Grid >
                <div className={classes.margin}>
                  <VendorCard vendor={props.vendor} editMode={true} edit={() => setOpenEditProfilePicture(true)} />
                </div>
              </Grid>
            }

            {
              props.vendor &&
              <Grid >
                <Card>
                  <CardContent style={{ textAlign: "center" }}>
                    <Avatar style={{ backgroundColor: props.loggedInUser.defaultColor }} className={classes.avatar} src={imgSrc}>{avatarText}</Avatar>
                    <br />
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => setOpenEditLogo(true)}
                      endIcon={<PhotoCamera />}
                    >
                      {t('vendorAccount.logo.title')}
                    </Button>
                  </CardContent>
                </Card>
              </Grid>
            }
          </Grid>
          <Grid container direction="column" item sm={4} xs={12}>

            {
              props.vendor &&

              <Grid>
                <Card>
                  <CardContent style={{ textAlign: "center" }}>
                    <FormControl component="fieldset">
                      <FormLabel component="legend">{t('vendorAccount.deliver.title')}</FormLabel>
                      <FormGroup>
                        <FormControlLabel
                          control={<Switch checked={state.onlineShop} onChange={handleChange} name="onlineShop" />}
                          label={t('vendorAccount.deliver.onlineShop')}
                        />
                        <FormControlLabel
                          control={<Switch checked={state.clickAndCollect} onChange={handleChange} name="clickAndCollect" />}
                          label={t('vendorAccount.deliver.clickAndCollect')}
                        />
                      </FormGroup>
                    </FormControl>
                    <Button
                      variant="contained"
                      color="primary"
                      disabled={
                        state.clickAndCollect === props.vendor.clickAndCollect
                        &&
                        state.onlineShop === props.vendor.onlineShop
                      }
                      onClick={() => props.updateDeliverInfoVendor({ ...state })}
                      endIcon={<SaveIcon />}
                    >
                      {t('vendorAccount.deliver.button')}
                    </Button>
                  </CardContent>
                </Card>
              </Grid>
            }
            {
              props.vendor &&

              <Grid>
                <Card className={!props.vendor.activated ? classes.stateInactivated : (props.vendor.published ? classes.statePublished: classes.stateNotPublished)}>
                  <CardContent >
                    {
                      !props.vendor.activated ?
                        <p>
                          <Trans i18nKey="vendorAccount.inactivate"
                          components={[ <b></b>,<br/>, <NavLink to="/contactUs?q=contact" className={classes.link}></NavLink>]} />
                        </p>
                        :
                        <span>
                          {
                            props.vendor.published ?
                              
                        <p>
                          <Trans i18nKey="vendorAccount.published"
                          components={[<b></b>, <br/>, <NavLink to="/vendor/dashboard" className={classes.link}></NavLink>]} />
                        </p> :
                              
                        <p>
                        <Trans i18nKey="vendorAccount.notPublished"
                        components={[ <b></b>,<br/>, <NavLink to="/vendor/dashboard" className={classes.link}></NavLink>]} />
                      </p>
                          }
                        </span>
                    }
                  </CardContent>
                </Card>
              </Grid>
            }
          </Grid>
          <Grid container direction="column" item sm={4} xs={12}>
            <Grid>
              {props.subscription &&
                <Dialog
                  open={activateSubDialog}
                  onClose={() => setActivateSubDialog(false)}>
                  <DialogTitle>{t('vendorAccount.subscription.dialog.terminate.title')}</DialogTitle>
                  <DialogContent>

                    <Trans i18nKey="vendorAccount.subscription.dialog.terminate.info"
                      values={{ expirationDate: formatDistance(new Date(), new Date(props.subscription.current_period_end * 1000), { locale: locale[[i18n.language]] }) }}
                      components={[<br />]} />
                  </DialogContent>

                  <DialogActions>
                    <Button
                      color="primary"
                      onClick={cancelSubscription}
                    >
                      {t('vendorAccount.subscription.cancel')}
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => setActivateSubDialog(false)}
                    >
                      {t('vendorAccount.subscription.keep')}
                    </Button>
                  </DialogActions>
                </Dialog>
              }
              {
                props.subscription && props.vendor ?
                  <Card>
                    <CardHeader
                      className={classes.cardHeaderAction}
                      avatar={
                        <Avatar className={classes[`state_${props.vendor.activated ? (props.subscription.status === "canceled" && (compareAsc(new Date(), new Date(props.subscription.current_period_end * 1000)) >= 0) ? "expired" : props.subscription.status) : "disabled"}`]}>
                          <CardMembershipIcon />
                        </Avatar>
                      }
                      /*action={
                        <Switch checked={props.subscription.status === "active" || (props.subscription.status === "canceled" && compareAsc(new Date(), new Date(props.subscription.current_period_end * 1000)) < 0)} onChange={() => handleSwitch(props.subscription.status)} />
                      }*/
                      title={props.subscription.product.name}
                    />
                    <CardContent>
                      <Typography component="div">
                        {t('vendorAccount.subscription.state.title')} : <span className={classes[`state_${props.vendor.activated ? (props.subscription.status === "canceled" && (compareAsc(new Date(), new Date(props.subscription.current_period_end * 1000)) >= 0) ? "expired" : props.subscription.status) : "disabled"}`]}>{t(`vendorAccount.subscription.state.value.${props.subscription.status}`)}</span>
                      </Typography>
                      <Typography /*className={classes.title}*/ color="textSecondary" gutterBottom>
                        {t('vendorAccount.subscription.price')}: {(props.subscription.unit_amount) / 100} {props.subscription.currency} / {t(`vendorAccount.subscription.${props.subscription.recurring}`)}<br />
                        {t('vendorAccount.subscription.startAt')} {format(new Date(props.subscription.created * 1000), 'PP', { locale: locale[[i18n.language]] })} <br />
                        {
                          (props.subscription.status === "active" || props.subscription.status === "trialing") && 
                          <span>{t('vendorAccount.subscription.nextPayment')} {formatDistance(new Date(), new Date(props.subscription.current_period_end * 1000), { locale: locale[[i18n.language]] })}<br />
                            { props.subscription.payment_method.brand !== '' &&  <span>{t('vendorAccount.subscription.creditCard')}: {props.subscription.payment_method.brand} ****{props.subscription.payment_method.last4}</span>}
                          </span>
                        }
                        {
                          props.subscription.status === "canceled" && (compareAsc(new Date(), new Date(props.subscription.current_period_end * 1000)) < 0) &&
                          <span><WarningIcon className={classes.warningIcon} />{t('vendorAccount.subscription.expiresIn')} {formatDistance(new Date(), new Date(props.subscription.current_period_end * 1000), { locale: locale[[i18n.language]] })}</span>
                        }
                        {
                          props.subscription.status === "canceled" && (compareAsc(new Date(), new Date(props.subscription.current_period_end * 1000)) >= 0) &&
                          <span><WarningIcon className={classes.warningIcon} />{t('vendorAccount.subscription.expiredSince')} {formatDistance(new Date(), new Date(props.subscription.current_period_end * 1000), { locale: locale[[i18n.language]] })}</span>
                        }
                        <br/>
                        
                        {
                          (props.subscription.status === "active" || props.subscription.status === "trialing" )  ?
                          <span>
                            <NavLink to="/subscribe">{t('vendorAccount.subscription.change')}</NavLink>
                            <br/>
                            <Link
                              component="button"
                              onClick={() => setActivateSubDialog(true)}
                            >
                              {t('vendorAccount.subscription.terminate')}
                            </Link>
                          </span>
                          :
                          <span>
                            <NavLink to="/subscribe">{t(`vendorAccount.subscription.${props.subscription.status === "canceled" ? 'takeBack' : 'activate'}`)}</NavLink>
                          </span>
                        }
                      </Typography>
                    </CardContent>
                  </Card>
                  :
                  <Card>
                    <CardContent>
                      <Typography component="div">
                        {t('vendorAccount.subscription.noSubscription')}
                        <Button
                          variant="contained"
                          color="primary"
                          onClick={goToSubscription}
                        >
                          {t('vendorAccount.subscription.do')}
                        </Button>
                      </Typography>
                    </CardContent>
                  </Card>
              }
            </Grid>
            <Grid>
              {
                props.invoices && props.invoices.length > 0 &&
                <Card>
                  <CardContent>
                    <Typography component="div">
                      {t('vendorAccount.invoice.title')}
                    </Typography>
                    <div className={classes.demo}>
                      <List>
                        {
                          props.invoices.map((inv) => {
                            return (
                              <ListItem key={inv.id}>
                                <ListItemIcon>
                                  <ReceiptIcon />
                                </ListItemIcon>
                                <ListItemText
                                  primary={`${t('vendorAccount.invoice.produced')} ${format(new Date(inv.period_start * 1000), 'PP', { locale: locale[[i18n.language]] })}`}
                                  secondary={<span>
                                    {inv.amount_paid / 100} {inv.currency} {t('vendorAccount.invoice.paid')}<br />
                                    <a href={inv.invoice_pdf}>{t('vendorAccount.invoice.retrieve')}</a>
                                  </span>}
                                />
                              </ListItem>
                            )
                          })
                        }
                      </List>
                    </div>
                  </CardContent>
                </Card>
              }
            </Grid>
          </Grid>

        </Grid>
      </div>
  );

}