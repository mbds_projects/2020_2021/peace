import * as React from 'react';

import routerHistory from '../../shared/router-history/router-history';

import VendorPasswordCreatePage from './VendorPasswordCreatePage';
import { createVendorPasswordGql } from '../../graphQL/vendorDashboard'
import { createSubscriptionFromVendorGql } from '../../graphQL/subscriptionDashboard'

import { loginVendor } from '../../core/modules/authentication.module';
import { login as loginStore } from '../../shared/store/actions/authentication.actions';

import { useDispatch } from "react-redux";

import { toast } from 'react-toastify';

import { useTranslation } from 'react-i18next';

import { useParams} from 'react-router-dom';

export default function VendorPasswordCreatePageContainer() {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const {id} = useParams();

  const handleIncorrectIdParameter = () => {
    routerHistory.replace('/404');
  }

  const performPasswordAttempt = async(password) => {
    if(!id) {
      handleIncorrectIdParameter();
    }
    try {
      let tokenRetrieved = await createVendorPasswordGql({input: {password: password, token: id}})
      if(!!tokenRetrieved) {
        let vendor = await loginVendor(tokenRetrieved.token);
        dispatch(loginStore(vendor));
        let subscription = await createSubscriptionFromVendorGql();
      } else {
        toast.error(t('error.somethingWrongAppends'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }catch(e) {
      toast.error(t('error.somethingWrongAppends'), {
        position: toast.POSITION.TOP_RIGHT
      });
    }
  }

  const handlePasswordAttempt = (password) => {
    performPasswordAttempt(password);
  }

  return (<VendorPasswordCreatePage onPasswordAttempt={handlePasswordAttempt}/>);

}