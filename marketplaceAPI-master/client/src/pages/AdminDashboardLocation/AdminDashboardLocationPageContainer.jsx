import React, { useState, useEffect } from 'react';

import AdminDashboardLocationPage from './AdminDashboardLocationPage';
import LoadingPage from '../Loading/LoadingPage';

import { toastError } from '../../shared/utils/error';

import { toast } from 'react-toastify';

import { useSelector } from "react-redux";

import { locationsGql, updateLocationGql, createLocationGql } from '../../graphQL/location'

import { vendorsMinimalAdminGql } from '../../graphQL/vendor'

import routerHistory from '../../shared/router-history/router-history';

import { useTranslation } from 'react-i18next';
export default function AdminDashboardLocationPageContainer() {
  const { t } = useTranslation();
  const loggedInUser = useSelector(state => state.authentication.currentUser);
  const [locations, setLocations] = useState([]);

  const [vendors, setVendors] = useState([]);

  const goBack = () => {
    routerHistory.push('/admin/');
  }

  const updateData = async() => {
    let locationsTmp = await locationsGql();
    setLocations(locationsTmp);

    let vendors = await vendorsMinimalAdminGql();
    setVendors(vendors)
  }

  useEffect(() => {
    updateData();
  }, []);

  const updateLocation = (input, id) => {
    updateLocationGql({input: input, id: id}).then(res => {
      if(res) {
        toast.success(t('adminDashboard.toast.locationUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
        updateData();
      } else {
        toast.error(t('adminDashboard.toast.locationUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }).catch(e => {
      toastError(e.message,t);
    });
  }

  const createLocation = (input) => {
    createLocationGql({input: input}).then(res => {
      if(res) {
        toast.success(t('adminDashboard.toast.locationCreated'), {
          position: toast.POSITION.TOP_RIGHT
        });
        updateData();
      } else {
        toast.error(t('adminDashboard.toast.locationCreatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }).catch(e => {
      toastError(e.message,t);
    });
  }


  if (!loggedInUser) {
    return <LoadingPage />
  }

  return (<AdminDashboardLocationPage
    goBack={goBack}
    locations={locations}
    updateLocation={updateLocation}
    createLocation={createLocation}
    vendors={vendors}
  />);
}
