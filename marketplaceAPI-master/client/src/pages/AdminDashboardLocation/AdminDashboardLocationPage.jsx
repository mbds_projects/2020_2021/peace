import React, { useState } from "react";
import _ from 'lodash'
import Button from '@material-ui/core/Button';

import TextField from '@material-ui/core/TextField';

import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import { TableSortLabel } from '@material-ui/core';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import EditIcon from '@material-ui/icons/Edit';

import Select from 'react-select';

const useStyles = makeStyles({
  root: {

  },
  table: {
    minWidth: 650,
  },
  search: {
    marginBottom: "20px"
  }
});

export default function AdminDashboardLocationPage(props) {
  const classes = useStyles();
  const { t } = useTranslation();

  const [open, setOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);

  const [selectedLocationId, setSelectedLocationId] = useState('');
  const [selectedLocationName, setSelectedLocationName] = useState('');
  const [selectedLocationCity, setSelectedLocationCity] = useState('');
  const [selectedLocationAddress, setSelectedLocationAddress] = useState('');
  const [selectedLocationPostalCode, setSelectedLocationPostalCode] = useState('');
  const [selectedLocationOpeningHours, setSelectedLocationOpeningHours] = useState('');
  const [selectedLocationWebSite, setSelectedLocationWebSite] = useState('');
  const [selectedLocationMail, setSelectedLocationMail] = useState('');
  const [selectedLocationPhoneNumber, setSelectedLocationPhoneNumber] = useState('');
  const [selectedLocationState, setSelectedLocationState] = useState('');
  const [selectedLocationCounty, setSelectedLocationCounty] = useState('');
  const [selectedLocationLatitude, setSelectedLocationLatitude] = useState(0);
  const [selectedLocationLongitude, setSelectedLocationLongitude] = useState(0);
  const [selectedVendors, setSelectedVendors] = useState([]);

  const [createLocationName, setCreateLocationName] = useState('');
  const [createLocationCity, setCreateLocationCity] = useState('');
  const [createLocationAddress, setCreateLocationAddress] = useState('');
  const [createLocationPostalCode, setCreateLocationPostalCode] = useState('');
  const [createLocationOpeningHours, setCreateLocationOpeningHours] = useState('');
  const [createLocationWebSite, setCreateLocationWebSite] = useState('');
  const [createLocationMail, setCreateLocationMail] = useState('');
  const [createLocationPhoneNumber, setCreateLocationPhoneNumber] = useState('');
  const [createLocationState, setCreateLocationState] = useState('');
  const [createLocationCounty, setCreateLocationCounty] = useState('');
  const [createLocationLatitude, setCreateLocationLatitude] = useState(0);
  const [createLocationLongitude, setCreateLocationLongitude] = useState(0);
  const [createVendors, setCreateVendors] = useState([]);

  const [sortByName, setSortByName] = useState(true);
  const [sortBy, setSortBy] = useState({name: 'name', value: 'desc'});
  const [search, setSearch] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [page, setPage] = useState(0);
  const [sortByCity, setSortByCity] = useState(true);
  const [sortByCreatedAt, setSortByCreatedAt] = useState(true);
  const [sortByUpdatedAt, setSortByUpdatedAt] = useState(true);

  const handleCreateName = (event) => {
    setCreateLocationName(event.target.value);
  };

  const handleCreateCity = (event) => {
    setCreateLocationCity(event.target.value);
  };

  const handleCreateAddress = (event) => {
    setCreateLocationAddress(event.target.value);
  };

  const handleCreatePostalCode = (event) => {
    setCreateLocationPostalCode(event.target.value);
  };

  const handleCreateWebSite = (event) => {
    setCreateLocationWebSite(event.target.value);
  };

  const handleCreateMail = (event) => {
    setCreateLocationMail(event.target.value);
  };

  const handleCreateOpeningHours = (event) => {
    setCreateLocationOpeningHours(event.target.value);
  };

  const handleCreatePhoneNumber = (event) => {
    setCreateLocationPhoneNumber(event.target.value);
  };

  const handleCreateState = (event) => {
    setCreateLocationState(event.target.value);
  };

  const handleCreateCounty = (event) => {
    setCreateLocationCounty(event.target.value);
  };

  const handleCreateLatitude = (event) => {
    setCreateLocationLatitude(parseFloat(event.target.value));
  };

  const handleCreateLongitude = (event) => {
    setCreateLocationLongitude(parseFloat(event.target.value));
  };

  const handleCreateVendors = (vendors) => {
    setCreateVendors(vendors);
  };


  const handleCreate = () => {
    let vendorsIds = createVendors.map(v => v.value);
    let input = {
      shopName: createLocationName,
      city: createLocationCity,
      address: createLocationAddress,
      postalCode: createLocationPostalCode,
      webSite: createLocationWebSite,
      mail: createLocationMail,
      phoneNumber: createLocationPhoneNumber,
      openingHours: createLocationOpeningHours,
      location: { type: "Point", coordinates: [createLocationLatitude, createLocationLongitude]},
      vendors: vendorsIds,
      state: createLocationState,
      county: createLocationCounty
    }
    props.createLocation(input);
    setOpenCreate(false);
    setCreateLocationName('');
  };

  const handleClickOpen = (loc) => {
    setOpen(true);
    setSelectedLocationId(loc.id)
    setSelectedLocationName(loc.shopName)
    setSelectedLocationOpeningHours(loc.openingHours);
    setSelectedLocationCity(loc.city);
    setSelectedLocationAddress(loc.address);
    setSelectedLocationMail(loc.mail)
    setSelectedLocationPhoneNumber(loc.phoneNumber);
    setSelectedLocationPostalCode(loc.postalCode);
    setSelectedLocationWebSite(loc.webSite);
    setSelectedLocationState(loc.state);
    setSelectedLocationCounty(loc.county)
    setSelectedLocationLatitude(loc.location.coordinates[0])
    setSelectedLocationLongitude(loc.location.coordinates[1])
    let vendorsRegistered = loc.vendors.map(v => vendorsOptions.find(vs => vs.value === v));
    setSelectedVendors(vendorsRegistered);
  };

  const handleChangeSortByName = (event) => {
    setSortByName(!sortByName);    
    setSortBy({
      name: "name",
      value: sortByName ? 'desc' : 'asc'
    })  
    setSortByCity(true);
    setSortByUpdatedAt(true);
    setSortByCreatedAt(true);
  };


  const handleChangeSortByCity = (event) => {
    setSortByCity(!sortByCity);
    setSortBy({
      name: "city",
      value: sortByCity ? 'desc' : 'asc'
    })

    setSortByCreatedAt(true);
    setSortByName(true);   
    setSortByUpdatedAt(true);   
  };

  const handleChangeSortByCreatedAt = (event) => {
    setSortByCreatedAt(!sortByCreatedAt);
    setSortBy({
      name: "createdAt",
      value: sortByCreatedAt ? 'desc' : 'asc'
    })

    setSortByCity(true);
    setSortByName(true);   
    setSortByUpdatedAt(true);   
  };

  const handleChangeSortByUpdatedAt = (event) => {
    setSortByUpdatedAt(!sortByUpdatedAt);
    setSortBy({
      name: "updatedAt",
      value: sortByUpdatedAt ? 'desc' : 'asc'
    })

    setSortByCity(true);
    setSortByName(true);     
    setSortByCreatedAt(true);   
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };


  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleName = (event) => {
    setSelectedLocationName(event.target.value);
  };

  const handleCity = (event) => {
    setSelectedLocationCity(event.target.value);
  };

  const handleAddress = (event) => {
    setSelectedLocationAddress(event.target.value);
  };

  const handlePostalCode = (event) => {
    setSelectedLocationPostalCode(event.target.value);
  };

  const handleWebSite = (event) => {
    setSelectedLocationWebSite(event.target.value);
  };

  const handleMail = (event) => {
    setSelectedLocationMail(event.target.value);
  };

  const handleOpeningHours = (event) => {
    setSelectedLocationOpeningHours(event.target.value);
  };

  const handlePhoneNumber = (event) => {
    setSelectedLocationPhoneNumber(event.target.value);
  };

  const handleState = (event) => {
    setSelectedLocationState(event.target.value);
  };

  const handleCounty = (event) => {
    setSelectedLocationCounty(event.target.value);
  };

  const handleLatitude = (event) => {
    setSelectedLocationLatitude(parseFloat(event.target.value));
  };

  const handleLongitude = (event) => {
    setSelectedLocationLongitude(parseFloat(event.target.value));
  };

  const handleVendors = (vendors) => {
    setSelectedVendors(vendors);
  };

  const handleClose = () => {
    setOpen(false);
    setSelectedLocationName('');
  };

  const handleUpdate = () => {
    let vendorsIds = selectedVendors.map(v => v.value);
    let input = {
      shopName: selectedLocationName,
      city: selectedLocationCity,
      address: selectedLocationAddress,
      postalCode: selectedLocationPostalCode,
      webSite: selectedLocationWebSite,
      mail: selectedLocationMail,
      phoneNumber: selectedLocationPhoneNumber,
      openingHours: selectedLocationOpeningHours,
      location: { type: "Point", coordinates: [selectedLocationLatitude, selectedLocationLongitude]},
      vendors: vendorsIds,
      state: selectedLocationState,
      county: selectedLocationCounty
    }
    props.updateLocation(input, selectedLocationId);
    //props.updateName(selectedLocationName, selectedLocationId);
    setSelectedLocationName(null);
    setSelectedLocationId(null)
    setOpen(false);
  };

  const handleAddLocation = () => {
    setOpenCreate(true);
  }

  const handleCloseCreate = () => {
    setOpenCreate(false);
  };

  const inputProps = {
    step: 0.00001,
  };

  let vendorsOptions = props.vendors.map((vendor) => {    
    return {
      value: vendor.id,
      label: vendor.name
    }
  });


  return (
      <div className={classes.root}>
        <IconButton onClick={props.goBack}>
          <ArrowBackIcon />
        </IconButton>
        <h4>{t('adminDashboard.locations')}</h4>

        <TextField
          margin="dense"
          id="search"
          label={t('adminDashboard.search')}
          fullWidth
          type="text"
          value={search}
          variant="outlined"
          onChange={handleSearch}
          className={classes.search}
        />
        <Button onClick={handleAddLocation} color="primary">{t('adminDashboard.createLocation')}</Button>
        {
          <Dialog open={openCreate} onClose={handleCloseCreate}>
            <DialogTitle id="form-dialog-title">{t('adminDashboard.create')}</DialogTitle>
            <DialogContent>
              <DialogContentText>
                {t('adminDashboard.createLocation')}
              </DialogContentText>
              <TextField
                autoFocus
                margin="dense"
                id="name"
                label={t('adminDashboard.name')}
                fullWidth
                type="text"
                value={createLocationName}
                variant="outlined"
                onChange={handleCreateName}
              />
              <TextField
                autoFocus
                margin="dense"
                id="name"
                label={t('adminDashboard.city')}
                fullWidth
                type="text"
                value={createLocationCity}
                variant="outlined"
                onChange={handleCreateCity}
              />
              <TextField
                autoFocus
                margin="dense"
                id="name"
                label={t('adminDashboard.address')}
                fullWidth
                type="text"
                value={createLocationAddress}
                variant="outlined"
                onChange={handleCreateAddress}
              />

              <TextField
                autoFocus
                margin="dense"
                id="name"
                label={t('adminDashboard.postalCode')}
                fullWidth
                type="text"
                value={createLocationPostalCode}
                variant="outlined"
                onChange={handleCreatePostalCode}
              />


              <TextField
                autoFocus
                margin="dense"
                id="name"
                label={t('adminDashboard.openingHours')}
                fullWidth
                type="text"
                value={createLocationOpeningHours}
                variant="outlined"
                onChange={handleCreateOpeningHours}
              />


              <TextField
                autoFocus
                margin="dense"
                id="name"
                label={t('adminDashboard.phoneNumber')}
                fullWidth
                type="text"
                value={createLocationPhoneNumber}
                variant="outlined"
                onChange={handleCreatePhoneNumber}
              />

              <TextField
                autoFocus
                margin="dense"
                id="name"
                label={t('adminDashboard.location.state')}
                fullWidth
                type="text"
                value={createLocationState}
                variant="outlined"
                onChange={handleCreateState}
              />

              <TextField
                autoFocus
                margin="dense"
                id="name"
                label={t('adminDashboard.location.county')}
                fullWidth
                type="text"
                value={createLocationCounty}
                variant="outlined"
                onChange={handleCreateCounty}
              />

              <TextField
                autoFocus
                margin="dense"
                id="name"
                label={t('adminDashboard.webSite')}
                fullWidth
                type="text"
                value={createLocationWebSite}
                variant="outlined"
                onChange={handleCreateWebSite}
              />

              <TextField
                autoFocus
                margin="dense"
                id="name"
                label={t('adminDashboard.mail')}
                fullWidth
                type="text"
                value={createLocationMail}
                variant="outlined"
                onChange={handleCreateMail}
              />

              <TextField
                autoFocus
                margin="dense"
                id="latitude"
                label={t('adminDashboard.latitude')}
                fullWidth
                type="number"
                inputProps={inputProps}
                value={createLocationLatitude}
                variant="outlined"
                onChange={handleCreateLatitude}
              />

              <TextField
                autoFocus
                margin="dense"
                id="longitude"
                label={t('adminDashboard.longitude')}
                fullWidth
                type="number"
                inputProps={inputProps}
                value={createLocationLongitude}
                variant="outlined"
                onChange={handleCreateLongitude}
              />


            <Select 
              name="vendors"
              options={vendorsOptions}   
              placeholder={t('adminDashboard.location.vendors')}
              value={createVendors}
              onChange={handleCreateVendors}
              styles={{ menu: base => ({ ...base, position: 'relative' }) }}
              multi={true}
              isMulti
            />
              
            </DialogContent>
        
            <DialogActions>
              <Button onClick={handleCloseCreate} color="primary">
              {t('adminDashboard.cancel')}
              </Button>
              <Button onClick={handleCreate} color="primary">
              {t('adminDashboard.create')}
              </Button>
            </DialogActions>
          </Dialog>
        }
      {
      selectedLocationId && 
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Update</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {t('adminDashboard.updateLocation')}
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('adminDashboard.name')}
            fullWidth
            type="text"
            value={selectedLocationName}
            variant="outlined"
            onChange={handleName}
          />
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('adminDashboard.location.city')}
            fullWidth
            type="text"
            value={selectedLocationCity}
            variant="outlined"
            onChange={handleCity}
          />
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('adminDashboard.location.address')}
            fullWidth
            type="text"
            value={selectedLocationAddress}
            variant="outlined"
            onChange={handleAddress}
          />

          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('adminDashboard.location.postalCode')}
            fullWidth
            type="text"
            value={selectedLocationPostalCode}
            variant="outlined"
            onChange={handlePostalCode}
          />


          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('adminDashboard.location.openingHours')}
            fullWidth
            type="text"
            value={selectedLocationOpeningHours}
            variant="outlined"
            onChange={handleOpeningHours}
          />


          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('adminDashboard.location.phoneNumber')}
            fullWidth
            type="text"
            value={selectedLocationPhoneNumber}
            variant="outlined"
            onChange={handlePhoneNumber}
          />

          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('adminDashboard.location.state')}
            fullWidth
            type="text"
            value={selectedLocationState}
            variant="outlined"
            onChange={handleState}
          />

          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('adminDashboard.location.county')}
            fullWidth
            type="text"
            value={selectedLocationCounty}
            variant="outlined"
            onChange={handleCounty}
          />
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('adminDashboard.location.webSite')}
            fullWidth
            type="text"
            value={selectedLocationWebSite}
            variant="outlined"
            onChange={handleWebSite}
          />

          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('adminDashboard.location.mail')}
            fullWidth
            type="text"
            value={selectedLocationMail}
            variant="outlined"
            onChange={handleMail}
          />

          <TextField
            autoFocus
            margin="dense"
            id="latitude"
            label={t('adminDashboard.location.latitude')}
            fullWidth
            type="number"
            inputProps={inputProps}
            value={selectedLocationLatitude}
            variant="outlined"
            onChange={handleLatitude}
          />

          <TextField
            autoFocus
            margin="dense"
            id="longitude"
            label={t('adminDashboard.location.longitude')}
            fullWidth
            type="number"
            inputProps={inputProps}
            value={selectedLocationLongitude}
            variant="outlined"
            onChange={handleLongitude}
          />

          <Select 
            name="vendors"
            options={vendorsOptions}   
            placeholder={t('adminDashboard.location.vendors')}
            value={selectedVendors}
            onChange={handleVendors}
            styles={{ menu: base => ({ ...base, position: 'relative' }) }}
            multi={true}
            isMulti
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            {t('adminDashboard.cancel')}
          </Button>
          <Button onClick={handleUpdate} color="primary">
            {t('adminDashboard.update')}
          </Button>
        </DialogActions>
      </Dialog>
    }
        <TableContainer component={Paper}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>{t('adminDashboard.update')}</TableCell>
            <TableCell sortDirection={sortByName ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByName ? 'desc' : 'asc'}
                onClick={handleChangeSortByName}
              >
                {t('adminDashboard.name')}
              </TableSortLabel>
            </TableCell>
            <TableCell align="right" sortDirection={sortByCity ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByCity ? 'desc' : 'asc'}
                onClick={handleChangeSortByCity}
              >
                {t('adminDashboard.location.city')}
              </TableSortLabel>
            </TableCell>
            <TableCell align="right">
              {t('adminDashboard.location.address')}
            </TableCell>
            <TableCell align="right">
              {t('adminDashboard.location.postalCode')}
            </TableCell>
            <TableCell align="right">
              {t('adminDashboard.location.webSite')}
            </TableCell>
            <TableCell align="right">
              {t('adminDashboard.location.phoneNumber')}
            </TableCell>
            <TableCell align="right">
              {t('adminDashboard.location.mail')}
            </TableCell>
            <TableCell align="right">
              {t('adminDashboard.location.openingHours')}
            </TableCell>
            <TableCell align="right">
              {t('adminDashboard.location.latitude')}
            </TableCell>
            <TableCell align="right">
              {t('adminDashboard.location.longitude')}
            </TableCell>
            <TableCell align="right">
              {t('adminDashboard.location.vendors')}
            </TableCell>
            <TableCell align="right" sortDirection={sortByCreatedAt ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByCreatedAt ? 'desc' : 'asc'}
                onClick={handleChangeSortByCreatedAt}
              >
                {t('adminDashboard.createdAt')}
              </TableSortLabel>
            </TableCell>
            <TableCell align="right" sortDirection={sortByUpdatedAt ? 'desc' : 'asc'}>
              <TableSortLabel
                active={true}
                direction={sortByUpdatedAt ? 'desc' : 'asc'}
                onClick={handleChangeSortByUpdatedAt}
              >
                {t('adminDashboard.updatedAt')}
              </TableSortLabel>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            _.orderBy(_.filter(props.locations, function(loc) { return loc.shopName.toLowerCase().includes(search.toLowerCase()) })
              , [sortBy.name]
              , [sortBy.value])
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((loc) => {
              let vendorsName = vendorsOptions.filter(v => loc.vendors.includes(v.value)).map(v => v.label).join("; ")
              return (
              <TableRow key={loc.id}>
                <TableCell component="th" scope="row">< EditIcon  onClick={() => {handleClickOpen(loc)}} /></TableCell>
                <TableCell component="th" scope="row">{loc.shopName}</TableCell>
                <TableCell component="th" scope="row">{loc.city}</TableCell>
                <TableCell component="th" scope="row">{loc.address}</TableCell>
                <TableCell component="th" scope="row">{loc.postalCode}</TableCell>
                <TableCell component="th" scope="row">{loc.webSite}</TableCell>
                <TableCell component="th" scope="row">{loc.phoneNumber}</TableCell>
                <TableCell component="th" scope="row">{loc.mail}</TableCell>
                <TableCell component="th" scope="row">{loc.openingHours}</TableCell>
                <TableCell component="th" scope="row">{loc.location.coordinates[0]}</TableCell>
                <TableCell component="th" scope="row">{loc.location.coordinates[1]}</TableCell>
                <TableCell component="th" scope="row">{vendorsName}</TableCell>
                <TableCell align="right">{loc.createdAt}</TableCell>
                <TableCell align="right">{loc.updatedAt}</TableCell>
              </TableRow>
            )}
          )}
        </TableBody>
      </Table>
    </TableContainer>
    <TablePagination
      rowsPerPageOptions={[5, 10, 25]}
      component="div"
      count={(_.filter(props.locations, function(loc) { return loc.shopName.toLowerCase().includes(search.toLowerCase()) })).length}
      rowsPerPage={rowsPerPage}
      page={page}
      onChangePage={handleChangePage}
      onChangeRowsPerPage={handleChangeRowsPerPage}
    />
      </div>

  );
}