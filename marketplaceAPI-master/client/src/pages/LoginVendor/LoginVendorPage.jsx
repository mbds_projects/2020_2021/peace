import React, { useState, useEffect } from 'react';
import * as _ from 'lodash';
import { makeStyles, createStyles } from '@material-ui/core/styles';

import { NavLink } from 'react-router-dom';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import Paper from '@material-ui/core/Paper';

import { toast } from 'react-toastify';

import { useTranslation } from 'react-i18next';

import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';

import LoadingPage from '../Loading/LoadingPage';

import ReactGA from 'react-ga';

ReactGA.pageview('/loginVendor');

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      width: "100%",
      marginTop: "20px",
      marginBottom: "20px"
    },
    link: {
      textAlign: "center"
    },
    paperForm: {
      padding: "25px"
    },
    divider: {
      margin: "20px"
    },
    input: {
      "& label": {
        fontSize: "14px"
      },
      "& .MuiFormLabel-root.Mui-error": {
        color: "grey"
      },
      "& fieldset": {
        borderColor: "lightgrey !important"
      }
    }
  }),
);
const regexMail = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$')

const regexPassword = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}");

export default function LoginVendorPage(props) {
  const { t } = useTranslation();
  let classes = useStyles();

  const [mail, setMail] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    if (!_.isEmpty(props.preFilled)) {
      if (props.preFilled.mail) setMail(props.preFilled.mail);
    }
  }, [props.preFilled])

  const handleLoginAttempt = (e) => {
    e.preventDefault();

    if (mail.trim() && password) {
      props.onLoginAttempt(mail.trim(), password);
      ReactGA.event({
        category: 'Login vendor',
        action: 'Internal login',
        label: `Login`
      });
    } else {
      toast.info(t('login.toast.mailAndPasswordRequired'), {
        position: toast.POSITION.TOP_RIGHT
      });
    }

  }

  const handleMailChange = (event) => {
    setMail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  return (

    <Grid container style={{ height: "100%" }} justify="center" alignItems="center">
      <Grid item xs={12} sm={8} >
        <div className={classes.root}>
          <div style={{ textAlign: "center" }}>
            <h3>{t('link.loginVendorLink')}</h3>
          </div>
          {
            props.loadingState && <Paper className={classes.paperForm}><LoadingPage /></Paper>
          }
          {
            !props.loadingState &&
            <Paper className={classes.paperForm}>
              <TextField
                id="mail"
                label={t('form.mail.label')}
                value={mail}
                onChange={handleMailChange}
                fullWidth
                required
                margin="normal"
                className={classes.input}
                helperText={!!mail.trim() && !regexMail.test(mail.trim()) ? t('form.mail.helper') : ''}
                error={!regexMail.test(mail.trim())}
                variant="outlined"
              />

              <TextField
                id="password"
                label={t('form.password.label')}
                value={password}
                onChange={handlePasswordChange}
                fullWidth
                required
                margin="normal"

                className={classes.input}
                type="password"
                helperText={!!password && !regexPassword.test(password) ? t('form.password.helper') : ''}
                error={!regexPassword.test(password)}
                variant="outlined"
              />

              <Button disabled={
                !password
                || (!!password && !regexPassword.test(password))
                || !mail
                || (!!mail.trim() && !regexMail.test(mail.trim()))
              }
                variant="contained"
                onClick={handleLoginAttempt}
                fullWidth>
                {t('login.button')}
              </Button>

              <div className={classes.link}><NavLink to="/forgotPasswordVendor">{t('login.forgotPasswordLink')}</NavLink></div>


              <Divider variant="middle" className={classes.divider} />
              <br /><br />
              <div className={classes.link}><NavLink to="/vendorCandidate">{t('login.candidate')}</NavLink></div>
              <div className={classes.link}><NavLink to="/login">{t('link.loginUserLink')}</NavLink></div>
            </Paper>
          }

        </div>
      </Grid>
    </Grid>
  );
}