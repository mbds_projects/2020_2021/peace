import React, { useState, useEffect } from 'react';

import { toast } from "react-toastify";

import { useTranslation } from 'react-i18next';

import LoginVendorPage from './LoginVendorPage';
import { loginVendor } from '../../core/modules/authentication.module';
import { authenticateVendorGql } from '../../graphQL/vendorDashboard'

import { login as loginStore } from '../../shared/store/actions/authentication.actions';

import { useDispatch } from "react-redux";

import { useLocation} from 'react-router-dom';

export default function LoginVendorPageContainer() {
  const [loadingState, setLoadingState] = useState(false);
  const [preFilled, setPreFilled] = useState({});

  const dispatch = useDispatch();
  const { t } = useTranslation();

  const location = useLocation();

  useEffect(() => {
    let locationDetail = location.detail;
    if(locationDetail) {
      setPreFilled(locationDetail)
    }
  }, [location])

  const onLoginAttempt = async(mail, password) => {
    setLoadingState(true);
    try {
      const loginAttemptVendor = await authenticateVendorGql({mail: mail, password: password});
      if (loginAttemptVendor) {
          //routerHistory.goBack();
          let vendor = await loginVendor(loginAttemptVendor.token);
          dispatch(loginStore(vendor));
      } else {
        toast.error(t('login.toast.invalidLogin'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    catch(e){
      if(e.message.includes("invalidCredential")) {
        toast.error(t('login.toast.invalidLogin'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('login.toast.somethingWrongAppends'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }

    }
    setLoadingState(false);
  }

  const childProps = {
    onLoginAttempt,
    loadingState,
    preFilled
  }

  return (<LoginVendorPage 
    {...childProps} />);
  
}