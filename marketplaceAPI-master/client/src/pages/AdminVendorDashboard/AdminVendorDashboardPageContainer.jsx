import AdminVendorDashboardPage from './AdminVendorDashboardPage';

import routerHistory from '../../shared/router-history/router-history';

import React, { useState, useEffect } from 'react';

import LoadingPage from '../Loading/LoadingPage';
import { makeStyles, createStyles } from '@material-ui/core/styles';

import { vendorsMinimalAdminGql, vendorsLimitedGql } from '../../graphQL/vendor'

import {
  adminProfileVendorDashboardGql, updateInfoAdminVendorDashboardGql, updateCarouselPictureAdminVendorDashboardGql,
  updateFeaturedProductInfoAdminVendorDashboardGql, updateFeaturedProductPictureAdminVendorDashboardGql
} from '../../graphQL/adminVendorDashboard';


import {
  locationsOfVendorByCityGql, locationsOfVendorByCountyGql, 
  createLocationAdminVendorDashboardGql, updateLocationAdminVendorDashboardGql, removeLocationAdminVendorDashboardGql
} from '../../graphQL/location';

import { useDispatch, useSelector } from "react-redux";

import * as _ from "lodash";

import { setVendors as setInternalVendors } from '../../shared/store/actions/vendor.actions';

import { Trans } from 'react-i18next';
import { useTranslation } from 'react-i18next';

import { toast } from 'react-toastify';

import { toastError } from '../../shared/utils/error';

import ReactGA from 'react-ga';

import Switch from '@material-ui/core/Switch';

import Grid from '@material-ui/core/Grid';

import { NavLink } from 'react-router-dom';

const useStyles = makeStyles((theme) =>
  createStyles({
    notPublished: {
      backgroundColor: "lightgrey",
      color: "white",
      padding: "10px",
      fontSize: "16px",
      marginBottom: "20px"
    },
    passToUnlimited: {
      color: "white",
      textDecoration: "underline",
      fontSize: "18px",
      fontWeight: "bold",
      paddingLeft: "5px"
    },
    isValidSubscription: {
      backgroundColor: theme.colors.almond,
      color: "white",
      padding: "10px",
      fontSize: "16px",
      marginBottom: "20px"
    }
  }),
);

export default function AdminVendorDashboardPageContainer() {
  const dispatch = useDispatch();

  const { t } = useTranslation();
  const classes = useStyles();
  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  const vendorsInternal = useSelector((state) => state.vendor.vendors);

  const [vendors, setVendors] = useState([]);
  const [selectedProfileVendor, setSelectedProfileVendor] = useState([]);

  const [suggestions, setSuggestions] = useState([]);
  const [citiesSuggestions, setCitiesSuggestions] = useState([]);
  const [countiesAndCitiesSuggestions, setCountiesAndCitiesSuggestions] = useState([]);

  const [alsoAvailableOn, setAlsoAvailableOn] = useState([]);
  const [alsoAvailableOnLoadingState, setAlsoAvailableOnLoadingState] = useState(true);

  const handleIncorrectIdParameter = () => {
    routerHistory.replace('/404');
  }

  useEffect(() => {
    let didCancel = false

    if (!loggedInUser || loggedInUser.role !== "admin") {
      handleIncorrectIdParameter();
      return;
    } else {
      async function retrieveVendors() {
        let vendors = await vendorsMinimalAdminGql();
        !didCancel && setVendors(vendors)
      }

      Promise.all([retrieveVendors()])
    }

    return () => { didCancel = true }
  }, [loggedInUser]);

  const setSelectedVendor = async (id) => {
    let profileVendor = await adminProfileVendorDashboardGql({ id });
    setSelectedProfileVendor(profileVendor);
  }

  useEffect(() => {
    let didCancel = false

    async function retrieveCitiesSuggestions() {
      if (selectedProfileVendor.locations.length > 0) {
        let citiesSuggestions = await locationsOfVendorByCityGql({ vendorId: selectedProfileVendor.id });

        let countiesSuggestions = await locationsOfVendorByCountyGql({ vendorId: selectedProfileVendor.id });


        citiesSuggestions = citiesSuggestions.map((loc) => { return { shops: loc.shops, loc: loc.city } })
        countiesSuggestions = countiesSuggestions.map((loc) => { return { shops: loc.shops, loc: loc.county } })

        !didCancel && setCitiesSuggestions(citiesSuggestions);

        let suggestionsCitiesCounties = _.unionBy(citiesSuggestions, countiesSuggestions, 'loc');

        !didCancel && setCountiesAndCitiesSuggestions(suggestionsCitiesCounties);

        let suggestions = suggestionsCitiesCounties.map((el, index) => {
          return {
            value: index,
            name: el.loc
          }
        });

        !didCancel && setSuggestions(suggestions);
      } else {
        !didCancel && setCitiesSuggestions([]);
        !didCancel && setCountiesAndCitiesSuggestions([]);
        !didCancel && setSuggestions([]);
      }
    }

    async function retrieveAlsoAvailable() {
      !didCancel && setAlsoAvailableOnLoadingState(true);
      if (selectedProfileVendor.alsoAvailableOn.length > 0) {
        let alsoAvailableOn;
        if (vendorsInternal.length > 0) {
          alsoAvailableOn = vendorsInternal.filter((v) => selectedProfileVendor.alsoAvailableOn.includes(v.id));
        } else {
          let vendors = await vendorsLimitedGql();
          alsoAvailableOn = vendors.filter((v) => selectedProfileVendor.alsoAvailableOn.includes(v.id));
          !didCancel && dispatch(setInternalVendors(vendors))
        }
        !didCancel && setAlsoAvailableOn(alsoAvailableOn);
        !didCancel && setAlsoAvailableOnLoadingState(false);
      } else {
        !didCancel && setAlsoAvailableOn([]);
        !didCancel && setAlsoAvailableOnLoadingState(false);
      }
    }

    if(!_.isEmpty(selectedProfileVendor)) {
      Promise.all([retrieveAlsoAvailable(), retrieveCitiesSuggestions()])
    }

    return () => { didCancel = true }
  }, [selectedProfileVendor])

  const updateInfoVendor = async (input) => {
    try {
      let res = await updateInfoAdminVendorDashboardGql({ input, vendorId: selectedProfileVendor.id });
      if (res) {
        toast.success(t('vendorDashboard.toast.infoUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await setSelectedVendor(selectedProfileVendor.id)
      } else {
        toast.error(t('vendorDashboard.toast.infoUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return res;
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const uploadCarouselPicture = async (file, indexOfImage) => {
    try {
      let res = await updateCarouselPictureAdminVendorDashboardGql({ file, indexOfImage, vendorId: selectedProfileVendor.id });
      if (res) {
        toast.success(t('vendorDashboard.toast.imageUploaded'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await setSelectedVendor(selectedProfileVendor.id)
      } else {
        toast.error(t('vendorDashboard.toast.imageUploadedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return res;
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const uploadFeaturedProductPicture = async (file, indexOfImage, link, price) => {
    try {
      let res = await updateFeaturedProductPictureAdminVendorDashboardGql({ file, indexOfImage, link, price, vendorId: selectedProfileVendor.id });
      if (res) {
        toast.success(t('vendorDashboard.toast.imageUploaded'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await setSelectedVendor(selectedProfileVendor.id)
      } else {
        toast.error(t('vendorDashboard.toast.imageUploadedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return res;
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const uploadFeaturedProductInfo = async (indexOfProduct, link, price) => {
    try {
      let res = await updateFeaturedProductInfoAdminVendorDashboardGql({ indexOfProduct, link, price, vendorId: selectedProfileVendor.id });
      if (res) {
        toast.success(t('vendorDashboard.toast.infoUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await setSelectedVendor(selectedProfileVendor.id)
      } else {
        toast.success(t('vendorDashboard.toast.infoUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return res;
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const createLocation = async (input) => {
    try {
      let res = await createLocationAdminVendorDashboardGql({ input, vendorId: selectedProfileVendor.id });
      if (res) {
        toast.success(t('vendorDashboard.toast.locationAdded'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await setSelectedVendor(selectedProfileVendor.id)
        return true;
      } else {
        toast.error(t('vendorDashboard.toast.locationAddedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
        return false;
      }
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const updateLocation = async (input, locId) => {
    try {
      let res = await updateLocationAdminVendorDashboardGql({ input, locId });
      if (res) {
        toast.success(t('vendorDashboard.toast.locationUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await setSelectedVendor(selectedProfileVendor.id)
        return true;
      } else {
        toast.error(t('vendorDashboard.toast.locationUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
        return false;
      }
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const removeLocation = async (locId) => {
    try {
      let res = await removeLocationAdminVendorDashboardGql({ locId, vendorId: selectedProfileVendor.id });
      if (res) {
        toast.success(t('vendorDashboard.toast.locationRemoved'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await setSelectedVendor(selectedProfileVendor.id)
        return true;
      } else {
        toast.error(t('vendorDashboard.toast.locationRemovedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
        return false;
      }
    } catch (e) {
      toastError(e.message, t);
    }
  }

  let propsChild = { vendors, updateInfoVendor, setSelectedVendor, selectedProfileVendor,
    suggestions, citiesSuggestions, countiesAndCitiesSuggestions, alsoAvailableOn, alsoAvailableOnLoadingState,
    uploadCarouselPicture, uploadFeaturedProductPicture, uploadFeaturedProductInfo,
    createLocation, updateLocation, removeLocation }

  return (
    <div>
      <AdminVendorDashboardPage {...propsChild} />
    </div>)

}
