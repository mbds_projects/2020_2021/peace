import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      margin: "auto",
      marginTop: "30px",
      width: "99%",
      "& a:hover": {
        color: theme.colors.strawberry
      },
      "& .container-title": {
        textAlign: "center",
        fontWeight: 600,
        marginTop: "-10px !important"
      }
    },
    rootMobile: {
      marginTop: "0px"
    },
    sliderContainer: {
      minHeight: "0px",
      minWidth: "0px",
      "& .SliderContent .slick-slide img": {
        width: "inherit",
        height: "400px",
        margin: "auto"
      }
    },
    sliderContainerMobile: {
      "& .SliderContent .slick-slide img" : {
        width: "inherit",
        height: "300px",
        margin: "auto"
      }
    },
    youtubeVideo: {
      width: "100%"
    },
    youtubeVideoPicture: {
      width: "100%",
      cursor: "pointer"
    },
    label: {
      margin: "5px",
      maxWidth: "100px"
    },
    activePage: {
      color: theme.colors.almond,
      marginTop: "10px",
      padding: "14px 16px"
    },
    commentSectionTitle: {
      fontWeight: "bold",
      fontSize: "22px",
      paddingRight: "10px"
    },
    commentNumbers: {
      padding: "16px"
    },
    commentHeader: {
      marginTop: "10px",
      marginBottom: "10px"
    },
    vendorsAssimilateTitle: {
      width: "100%"
    },
    vendorsAssimilateTitleMobile: {
      width: "100%",
      textAlign: "center"
    },
    addCommentButton: {
      margin: "20px 10px"
    },
    icon: {
      position: "relative",
      top: "5px",
      "&:hover": {
        cursor: "pointer"
      }
    },
    descNoWrap: {
      whiteSpace: "nowrap",
      width: "80%"
    },
    desc: {
      paddingRight: "20px"
    },
    descMobile: {
      padding: "20px",
      textAlign: "justify"
    },
    toggleDesc: {
      padding: 0,
      float: "right",
      color: "black !important",
      fontWeight: "bold",
      "&:hover": {
        cursor: "pointer"
      }
    },
    commentHeaderMobile: {
      marginTop: "10px",
      marginLeft: "10px",
      marginBottom: "10px"
    },
    commentSectionMobile: {
      padding: "10px"
    },
    commentItem: {
      padding: "10px"
    },
    loadingState: {
      textAlign: "center"
    },
    imgContent: {
      maxHeight: "400px",
      overflow: "hidden",
      width: "100%"
    },
    alsoAvailableOnTitle: {
      paddingLeft: "10px"
    },
    alsoAvailable: {
      margin: "auto"
    },
    imgInfo: {
      width: "60px"
    },
    imgInfoMobile: {
      float: "left",
      margin: "10px 10px 10px 0px",
      width: "60px"
    },
    diamond: {
      width: '60px',
      height: '60px',
      backgroundColor: theme.colors.almond,
      //borderRadius: "50%"
    },
    rewards: {
      marginTop: "15px",
      marginBottom: "20px"
    },
    howToClick: {
      fontStyle: "italic",
      textDecoration: "underline",
      cursor: "pointer"
    },
    bottomDesktop: {
      marginBottom: "20px"
    },
    publishButton: {
      textAlign: "center"
    },
  }),
);