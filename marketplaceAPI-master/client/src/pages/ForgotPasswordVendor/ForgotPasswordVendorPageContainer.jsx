import * as React from 'react';

import { toast } from 'react-toastify';
import { forgotPasswordVendorGql } from '../../graphQL/vendorDashboard'
import ForgotPasswordVendorPage from './ForgotPasswordVendorPage';

import { useTranslation } from 'react-i18next';

export default function ForgotPasswordVendorPageContainer() {
  const { t } = useTranslation();
  
  const performMailAttempt =  async (mail) => {
    try {
      const passwordAttemptUser = await forgotPasswordVendorGql({mail: mail});
      if (passwordAttemptUser) {
          toast.success(t('forgotPassword.toast.mailSent'), {
            position: toast.POSITION.TOP_RIGHT
          });
      } else {
        toast.error(t('error.somethingWrongAppend'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    catch(e){
      if(e.message.includes("mail.notFound")) {
        toast.error(t('forgotPassword.toast.mailUnknown'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('error.somethingWrongAppend'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }

    }

  }

  const handleMailAttempt = (mail) => {
    performMailAttempt(mail);
  }

  return (<ForgotPasswordVendorPage onMailAttempt={handleMailAttempt} />);

}