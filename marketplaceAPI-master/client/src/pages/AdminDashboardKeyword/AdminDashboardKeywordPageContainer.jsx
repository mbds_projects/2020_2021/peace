import React, { useState, useEffect } from 'react';

import AdminDashboardKeywordPage from '../AdminDashboardKeyword/AdminDashboardKeywordPage'
import LoadingPage from '../Loading/LoadingPage';

import { toastError } from '../../shared/utils/error';

import { toast } from 'react-toastify';

import { useSelector } from "react-redux";

import { keywordsGql, uploadPictureGql, updateDescGql } from '../../graphQL/keyword'
import {vendorsAssimilateMinimalGql } from '../../graphQL/vendor';

import routerHistory from '../../shared/router-history/router-history';

import { useTranslation } from 'react-i18next';
export default function AdminDashboardKeywordPageContainer() {
  const { t } = useTranslation();
  const loggedInUser = useSelector(state => state.authentication.currentUser);
  const [keywords, setKeywords] = useState([]);

  const getVendorsAssimilate = async (keywordId) => {
    let vendorsAssimilate = await vendorsAssimilateMinimalGql({categories: [], keywords: [keywordId], labels: []});
    return vendorsAssimilate;
  }

  const goBack = () => {
    routerHistory.push('/admin/');
  }

  const updateData = async() => {
    let keywordsTmp = await keywordsGql();
    setKeywords(keywordsTmp);
  }

  useEffect(() => {
    updateData();
  }, []);

  const updateDesc = (descFr, descEn, id) => {
    updateDescGql({descFr: descFr, descEn: descEn, id: id}).then(res => {
      if(res) {
        toast.success(t('adminDashboard.toast.keywordUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
        updateData();
      } else {
        toast.error(t('adminDashboard.toast.keywordUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }).catch(e => {
      toastError(e.message,t);
    });
  }

  const uploadPicture = async (file, id) => {
    try {
      let updated =  uploadPictureGql({file: file, id: id});
      if(updated) {
        toast.success(t('adminDashboard.toast.imageUploaded'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('adminDashboard.toast.imageUploadedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    } catch(e) {
      toastError(e.message,t);
    }
  }

  if (!loggedInUser) {
    return <LoadingPage />
  }

  return (<AdminDashboardKeywordPage
    goBack={goBack}
    keywords={keywords}
    uploadPicture={uploadPicture}
    updateDesc={updateDesc}
    getVendorsAssimilate={getVendorsAssimilate}
  />);
}
