import React, { useState, useEffect } from 'react';
import { rewardsOfVendorGql, createRewardGql, disableRewardGql, enableRewardGql } from '../../graphQL/reward';
import { useSelector } from "react-redux";
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';

import VendorRewardsPage from './VendorRewardsPage'
export default function VendorRewardsPageContainer(props) {
    const [rewards, setRewards] = useState(null);
    const [loadingState, setLoadingState] = useState(true);
    const loggedInUser = useSelector((state) => state.authentication.currentUser);
    const { t } = useTranslation();

    useEffect(() => {
        let didCancel = false
        async function retrieveRewardsOfVendor() {
            !didCancel && setLoadingState(true);
            let rewardsTmp = await rewardsOfVendorGql()
            !didCancel && setRewards(rewardsTmp);
            !didCancel && setLoadingState(false);
        }

        if (loggedInUser && loggedInUser.id) {
            retrieveRewardsOfVendor();
        }

        return () => { didCancel = true }
    }, [loggedInUser])

    const createReward = async (input) => {
        try {
            let res = await createRewardGql(input);
            if (res) {
                let rewardsTmp = await rewardsOfVendorGql()
                setRewards(rewardsTmp);
            } else {
                toast.error(t('reward.toast.creationFail'), {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        } catch (e) {
            toast.error(t('reward.toast.creationFail'), {
                position: toast.POSITION.TOP_RIGHT
            });

        }
    }


    const disableReward = async (rewardId) => {
        try {
            let res = await disableRewardGql({rewardId: rewardId});
            if (res) {
                let rewardsTmp = await rewardsOfVendorGql()
                setRewards(rewardsTmp);
            } else {
                toast.error(t('reward.toast.disableFail'), {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        } catch (e) {
            toast.error(t('reward.toast.disableFail'), {
                position: toast.POSITION.TOP_RIGHT
            });

        }
    }


    const enableReward = async (rewardId) => {
        try {
            let res = await enableRewardGql({rewardId: rewardId});
            if (res) {
                let rewardsTmp = await rewardsOfVendorGql()
                setRewards(rewardsTmp);
            } else {
                toast.error(t('reward.toast.enableFail'), {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        } catch (e) {
            toast.error(t('reward.toast.enableFail'), {
                position: toast.POSITION.TOP_RIGHT
            });

        }
    }

    let propsChild = { loadingState, rewards, createReward, vendor: loggedInUser, disableReward, enableReward }
    return (
        <VendorRewardsPage {...propsChild} />
    )
}