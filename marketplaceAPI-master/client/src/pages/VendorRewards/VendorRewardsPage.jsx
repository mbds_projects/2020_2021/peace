import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import ReactGA from 'react-ga';
import RewardCard from '../../components/RewardCard/RewardCard';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import DialogCreateReward from '../../components/DialogDashboard/DialogCreateReward'

import Divider from '@material-ui/core/Divider';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Button from '@material-ui/core/Button';

import { useTranslation } from 'react-i18next';
import Loading from '../Loading/LoadingPage';

ReactGA.pageview('/vendorRewards');

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      width: "99%",
      "& body": {
        fontFamily: "'Open Sans', sans-serif",
        "-webkit-font-smoothing": "antialiased",
        "-moz-osx-font-smoothing": "grayscale"
      }
    },
    noCards: {
      width: "100%"
    },
    emptyState: {
      textAlign: "center",
      "& img": {
        width: "50px"
      },
      "& a": {
        color: "grey"
      }
    },
    opacity: {
      backgroundColor: "rgba(255,255,255,0.7)",
      height: "200px"
    },
    addReward: (props) => ({
      margin: "10px",
      height: "200px",
      //borderRadius: "15px",
      backgroundImage: `linear-gradient(0deg, rgba(0, 0, 0, 0.5) 0%, rgba(255,255,255,0) 30%), url("${props.background}")`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "top",
      backgroundSize: "cover"
    }),
  }),
);

export default function VendorRewardsPage(props) {
  let imgSrc = "";
  if (props.vendor && props.vendor.profilePictureUrl) {
    imgSrc = props.vendor.profilePictureUrl;
  }
  let classes = useStyles({ background: imgSrc });
  const { t } = useTranslation();

  const [openNewReward, setOpenNewReward] = useState(false);

  const disableReward = (id) => {
    props.disableReward(id);
  }

  const enableReward = (id) => {
    props.enableReward(id);
  }

  const rewardCardsEnabled = props.rewards && props.rewards.filter(r => r.activated === true).map(reward => {
    return <Grid item xs={12} sm={4} key={reward.id}>
      <RewardCard
        reward={reward}
        editMode={true}
        disable={() => disableReward(reward.id)}
        enable={() => enableReward(reward.id)}
      />
    </Grid>
  });

  const rewardCardsDisabled = props.rewards && props.rewards.filter(r => r.activated === false).map(reward => {
    return <Grid item xs={12} sm={4} key={reward.id}>
      <RewardCard
        reward={reward}
        editMode={true}
        disable={() => disableReward(reward.id)}
        enable={() => enableReward(reward.id)}
      />
    </Grid>
  });

  const createReward = (input) => {
    props.createReward(input);
    setOpenNewReward(false);
  }

  return (
    props.loading ?
      <Loading />
      :
      <div className={classes.root}>
        <DialogCreateReward openNewReward={openNewReward} createReward={createReward} setOpenNewReward={setOpenNewReward} />
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <h3>{t('reward.activated')}</h3>
          </Grid>
          {
            rewardCardsEnabled && rewardCardsEnabled.length > 0 && rewardCardsEnabled
          }
          <Grid item xs={12} sm={4}>
            <div className={classes.addReward}>
              <Grid container direction="row" justify="center" alignItems="center" className={classes.opacity} >

                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => setOpenNewReward(true)}
                  className={classes.sendCommentButton}
                  endIcon={<AddCircleOutlineIcon />}
                >
                  {t('reward.addButton')}
                </Button>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={12}>
            <Divider variant="middle" className={classes.divider} />
          </Grid>

          <Grid item xs={12}>
            <h3>{t('reward.disabled')}</h3>
          </Grid>
          {
            rewardCardsDisabled && rewardCardsDisabled.length > 0 && rewardCardsDisabled
          }
        </Grid>
      </div>
  );

}