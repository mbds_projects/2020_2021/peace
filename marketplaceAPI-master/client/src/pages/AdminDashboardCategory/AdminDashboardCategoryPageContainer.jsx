import React, { useState, useEffect } from 'react';

import AdminDashboardCategoryPage from './AdminDashboardCategoryPage';
import LoadingPage from '../Loading/LoadingPage';

import { toastError } from '../../shared/utils/error';

import { toast } from 'react-toastify';

import { useSelector } from "react-redux";

import { categoriesGql, uploadPictureGql, updateDescGql } from '../../graphQL/category'
import {vendorsAssimilateMinimalGql } from '../../graphQL/vendor';

import routerHistory from '../../shared/router-history/router-history';

import { useTranslation } from 'react-i18next';
export default function AdminDashboardCategoryPageContainer() {
  const { t } = useTranslation();
  const loggedInUser = useSelector(state => state.authentication.currentUser);
  const [categories, setCategories] = useState([]);

  const getVendorsAssimilate = async (categoryId) => {
    let vendorsAssimilate = await vendorsAssimilateMinimalGql({categories: [categoryId], keywords: [], labels: []});
    return vendorsAssimilate;
  }

  const goBack = () => {
    routerHistory.push('/admin/');
  }

  const updateData = async() => {
    let categoriesTmp = await categoriesGql();
    setCategories(categoriesTmp);
  }

  useEffect(() => {
    updateData();
  }, []);

  const updateDesc = (descFr, descEn, id) => {
    updateDescGql({descFr: descFr, descEn: descEn, id: id}).then(res => {
      if(res) {
        toast.success(t('adminDashboard.toast.categoryUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
        updateData();
      } else {
        toast.error(t('adminDashboard.toast.categoryUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }).catch(e => {
      toastError(e.message,t);
    });
  }

  const uploadPicture = (file, id) => {
    try {
      let updated = uploadPictureGql({file: file, id: id});
      if(updated) {
        toast.success(t('adminDashboard.toast.imageUploaded'), {
          position: toast.POSITION.TOP_RIGHT
        });
        updateData();
      } else {
        toast.error(t('adminDashboard.toast.imageUploadedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    } catch(e) {
      toastError(e.message,t);
    }
  }

  if (!loggedInUser) {
    return <LoadingPage />
  }

  return (<AdminDashboardCategoryPage
    goBack={goBack}
    categories={categories}
    uploadPicture={uploadPicture}
    updateDesc={updateDesc}
    getVendorsAssimilate={getVendorsAssimilate}
  />);
}
