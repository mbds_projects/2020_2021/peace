import React, { useState, useEffect } from 'react';

import AdminDashboardLabelPage from './AdminDashboardLabelPage';
import LoadingPage from '../Loading/LoadingPage';

import { toastError } from '../../shared/utils/error';

import { toast } from 'react-toastify';

import { useSelector } from "react-redux";

import { labelsGql, uploadLogoGql, updateNameGql, createLabelGql } from '../../graphQL/label'
import {vendorsAssimilateMinimalGql } from '../../graphQL/vendor';

import routerHistory from '../../shared/router-history/router-history';

import { useTranslation } from 'react-i18next';
export default function AdminDashboardLabelPageContainer() {
  const { t } = useTranslation();
  const loggedInUser = useSelector(state => state.authentication.currentUser);
  const [labels, setLabels] = useState([]);

  const getVendorsAssimilate = async (labelId) => {
    let vendorsAssimilate = await vendorsAssimilateMinimalGql({categories: [], keywords: [], labels: [labelId]});
    return vendorsAssimilate;
  }

  const goBack = () => {
    routerHistory.push('/admin/');
  }

  const updateData = async() => {
    let labelsTmp = await labelsGql();
    setLabels(labelsTmp);
  }

  useEffect(() => {
    updateData();
  });

  const updateName = async (name, id) => {
    try {
      let updated = updateNameGql({name: name, id: id});
      if(updated) {
        toast.success(t('adminDashboard.toast.labelUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
        updateData();
      } else {
        toast.error(t('adminDashboard.toast.labelUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    } catch (e) {
      toastError(e.message,t);
    }
  }

  const uploadLogo = async (file, id) => {
    try {
      let updated = uploadLogoGql({file: file, id: id});
      if(updated) {
        toast.success(t('adminDashboard.toast.imageUploaded'), {
          position: toast.POSITION.TOP_RIGHT
        });
        updateData();
      } else {
        toast.success(t('adminDashboard.toast.imageUploadedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    } catch (e) {
      toastError(e.message,t);
    }
  }

  const createLabel = async (name, file) => {
    try {
      let created = createLabelGql({file: file, name: name});
      if(created) {
        toast.success(t('adminDashboard.toast.createLabel'), {
          position: toast.POSITION.TOP_RIGHT
        });
        updateData();
      } else {
        toast.error(t('adminDashboard.toast.createLabelError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    } catch(e) {
      toastError(e.message,t);
    }
  }

  if (!loggedInUser) {
    return <LoadingPage />
  }

  return (<AdminDashboardLabelPage
    goBack={goBack}
    labels={labels}
    uploadLogo={uploadLogo}
    updateName={updateName}
    createLabel={createLabel}
    getVendorsAssimilate={getVendorsAssimilate}
  />);
}
