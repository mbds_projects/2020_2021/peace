import React, { useState, useCallback, useRef } from "react";

import Button from '@material-ui/core/Button';
import _ from 'lodash'
import TextField from '@material-ui/core/TextField';

import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import { TableSortLabel } from '@material-ui/core';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import EditIcon from '@material-ui/icons/Edit';
import PublishIcon from '@material-ui/icons/Publish';
import VisibilityIcon from '@material-ui/icons/Visibility';

import ReactCrop from "react-image-crop";
import 'react-image-crop/dist/ReactCrop.css';

const useStyles = makeStyles({
  root: {

  },
  table: {
    minWidth: 650,
  },
  search: {
    marginBottom: "20px"
  }
});

export default function AdminDashboardLabelPage(props) {
  const classes = useStyles();
  const { t } = useTranslation();

  const [open, setOpen] = React.useState(false);
  const [openUpload, setOpenUpload] = React.useState(false);
  const [openCreate, setOpenCreate] = React.useState(false);
  const [openSeeVendors, setOpenSeeVendors] = React.useState(false);

  const [selectedLabelId, setSelectedLabelId] = React.useState();
  const [selectedLabelName, setSelectedLabelName] = React.useState();

  const [upImg, setUpImg] = useState();
  const imgRef = useRef(null);
  const [crop, setCrop] = useState({ unit: "px", width: 200, height: 200 });
  const [previewUrl, setPreviewUrl] = useState();
  const [file, setFile] = useState();

  const [sortByName, setSortByName] = React.useState(true);
  const [sortBy, setSortBy] = React.useState({ name: 'name', value: 'desc' });
  const [search, setSearch] = React.useState('');
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [sortByCreatedAt, setSortByCreatedAt] = React.useState(true);
  const [sortByUpdatedAt, setSortByUpdatedAt] = React.useState(true);
  const [createLabelName, setCreateLabelName] = React.useState('');

  const [assimilateVendors, setAssimilateVendors] = useState([]);

  const handleCreateName = (event) => {
    setCreateLabelName(event.target.value);
  };

  const handleCreate = () => {
    props.createLabel(createLabelName, file);
    setOpenCreate(false);
    setFile(null);
    setPreviewUrl('')
    setUpImg(null);
    setCreateLabelName('');
    setCrop({ unit: "px", width: 200, height: 200 })
  };

  const handleClickOpen = (label) => {
    setOpen(true);
    setSelectedLabelId(label.id)
    setSelectedLabelName(label.name)
  };

  const handleClickOpenUpload = (label) => {
    setOpenUpload(true);
    setSelectedLabelId(label.id)
  }

  const handleClickOpenSeeVendors = async (label) => {
    let res = await props.getVendorsAssimilate(label.id);
    setAssimilateVendors(res);
    setOpenSeeVendors(true);
    setSelectedLabelId(label.id)
  }

  const onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  const makeClientCrop = async (crop) => {
    if (imgRef.current && crop.width && crop.height) {
      createCropPreview(imgRef.current, crop, "newFile.png");
    }
  };

  const createCropPreview = async (image, crop, fileName) => {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = 200;
    canvas.height = 200;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      200,
      200
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          reject(new Error("Canvas is empty"));
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(previewUrl);
        setPreviewUrl(window.URL.createObjectURL(blob));

        const file = new File([blob], `image.png`, { type: "image/png", lastModified: new Date() })
        setFile(file);
      }, "image/png");
    });
  };

  const sendFile = () => {
    props.uploadLogo(file, selectedLabelId);
    setOpenUpload(false);
    setFile(null);
    setPreviewUrl('')
    setUpImg(null);
    setCrop({ unit: "px", width: 200, height: 200 })
  }

  const handleChangeSortByName = (event) => {
    setSortByName(!sortByName);
    setSortBy({
      name: "name",
      value: sortByName ? 'desc' : 'asc'
    })
    setSortByUpdatedAt(true);
    setSortByCreatedAt(true);
  };

  const handleChangeSortByCreatedAt = (event) => {
    setSortByCreatedAt(!sortByCreatedAt);
    setSortBy({
      name: "createdAt",
      value: sortByCreatedAt ? 'desc' : 'asc'
    })

    setSortByName(true);
    setSortByUpdatedAt(true);
  };

  const handleChangeSortByUpdatedAt = (event) => {
    setSortByUpdatedAt(!sortByUpdatedAt);
    setSortBy({
      name: "updatedAt",
      value: sortByUpdatedAt ? 'desc' : 'asc'
    })

    setSortByName(true);
    setSortByCreatedAt(true);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };


  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleName = (event) => {
    setSelectedLabelName(event.target.value);
  };

  const handleClose = () => {
    setOpen(false);
    setSelectedLabelName('');
  };

  const handleCloseUpload = () => {
    setOpenUpload(false)
    setFile(null);
    setPreviewUrl('')
    setUpImg(null);
    setCrop({ unit: "px", width: 200, height: 200 })
  }

  const handleUpdate = () => {
    props.updateName(selectedLabelName, selectedLabelId);
    setSelectedLabelName(null);
    setSelectedLabelId(null)
    setOpen(false);
  };

  const handleAddLabel = () => {
    setOpenCreate(true);
  }

  const handleCloseCreate = () => {
    setOpenCreate(false);
    setFile(null);
    setPreviewUrl('')
    setUpImg(null);
    setCrop({ unit: "px", width: 200, height: 200 })
  };

  return (
    <div className={classes.root}>
      <IconButton onClick={props.goBack}>
        <ArrowBackIcon />
      </IconButton>
      <h4>{t('adminDashboard.labels')}</h4>

      <TextField
        margin="dense"
        id="search"
        label={t('adminDashboard.search')}
        fullWidth
        type="text"
        value={search}
        variant="outlined"
        onChange={handleSearch}
        className={classes.search}
      />
      <Button onClick={handleAddLabel} color="primary">{t('adminDashboard.createLabel')}</Button>
      {
        <Dialog open={openCreate} onClose={handleCloseCreate}>
          <DialogTitle id="form-dialog-title">{t('adminDashboard.create')}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {t('adminDashboard.createLabel')}
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label={t('adminDashboard.name')}
              fullWidth
              type="text"
              value={createLabelName}
              variant="outlined"
              onChange={handleCreateName}
            />
            <div>
              <input type="file" accept="image/*" onChange={onSelectFile} />
              <ReactCrop
                src={upImg}
                onImageLoaded={onLoad}
                crop={crop}
                onChange={c => setCrop(c)}
                onComplete={makeClientCrop}
              />
              {previewUrl && <img alt="Crop preview" src={previewUrl} />}
            </div>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseCreate} color="primary">
              {t('adminDashboard.cancel')}
            </Button>
            <Button onClick={handleCreate} color="primary">
              {t('adminDashboard.create')}
            </Button>
          </DialogActions>
        </Dialog>
      }
      {
        selectedLabelId &&
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Update</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {t('adminDashboard.updateLabel')}
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label={t('adminDashboard.name')}
              fullWidth
              type="text"
              value={selectedLabelName}
              variant="outlined"
              onChange={handleName}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              {t('adminDashboard.cancel')}
            </Button>
            <Button onClick={handleUpdate} color="primary">
              {t('adminDashboard.update')}
            </Button>
          </DialogActions>
        </Dialog>
      }
      {
        selectedLabelId &&
        <Dialog open={openUpload} onClose={handleCloseUpload} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Upload</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {t('adminDashboard.uploadLabel')}
            </DialogContentText>
            <div>
              <input type="file" accept="image/*" onChange={onSelectFile} />
              <ReactCrop
                src={upImg}
                onImageLoaded={onLoad}
                crop={crop}
                onChange={c => setCrop(c)}
                onComplete={makeClientCrop}
              />
              {previewUrl && <img alt="Crop preview" src={previewUrl} />}
            </div>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseUpload} color="primary">
              {t('adminDashboard.cancel')}
            </Button>
            <Button onClick={sendFile} color="primary">
              {t('adminDashboard.upload')}
            </Button>
          </DialogActions>
        </Dialog>
      }
      {
        selectedLabelId &&
        <Dialog open={openSeeVendors} onClose={() => setOpenSeeVendors(false)} aria-labelledby="form-dialog-title">
          <DialogTitle>{t('adminDashboard.vendorsCorresponding')}</DialogTitle>
          <DialogContent>
            <div>
              {assimilateVendors.map(v => `${v.name} ; `)}
            </div>
          </DialogContent>
        </Dialog>
      }
      <TableContainer component={Paper}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>{t('adminDashboard.update')}</TableCell>
              <TableCell sortDirection={sortByName ? 'desc' : 'asc'}>
                <TableSortLabel
                  active={true}
                  direction={sortByName ? 'desc' : 'asc'}
                  onClick={handleChangeSortByName}
                >
                  {t('adminDashboard.name')}
                </TableSortLabel>
              </TableCell>

              <TableCell>{t('adminDashboard.vendors')}</TableCell>
              <TableCell>{t('adminDashboard.upload')}</TableCell>
              <TableCell>{t('adminDashboard.image')}</TableCell>
              <TableCell align="right" sortDirection={sortByCreatedAt ? 'desc' : 'asc'}>
                <TableSortLabel
                  active={true}
                  direction={sortByCreatedAt ? 'desc' : 'asc'}
                  onClick={handleChangeSortByCreatedAt}
                >
                  {t('adminDashboard.createdAt')}
                </TableSortLabel>
              </TableCell>
              <TableCell align="right" sortDirection={sortByUpdatedAt ? 'desc' : 'asc'}>
                <TableSortLabel
                  active={true}
                  direction={sortByUpdatedAt ? 'desc' : 'asc'}
                  onClick={handleChangeSortByUpdatedAt}
                >
                  {t('adminDashboard.updatedAt')}
                </TableSortLabel>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              _.orderBy(_.filter(props.labels, function (label) { return label.name.toLowerCase().includes(search.toLowerCase()) })
                , [sortBy.name]
                , [sortBy.value])
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((label) => {
                  let imgSrc = label.pictureUrl;
                  return (
                    <TableRow key={label.name}>
                      <TableCell component="th" scope="row">< EditIcon onClick={() => { handleClickOpen(label) }} /></TableCell>
                      <TableCell component="th" scope="row">{label.name}</TableCell>
                      <TableCell component="th" scope="row">< VisibilityIcon onClick={() => { handleClickOpenSeeVendors(label) }} /></TableCell>
                      <TableCell component="th" scope="row">< PublishIcon onClick={() => { handleClickOpenUpload(label) }} /></TableCell>
                      <TableCell component="th" scope="row"><img alt="" src={imgSrc} /></TableCell>
                      <TableCell align="right">{label.createdAt}</TableCell>
                      <TableCell align="right">{label.updatedAt}</TableCell>
                    </TableRow>
                  )
                }
                )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={(_.filter(props.labels, function (label) { return label.name.toLowerCase().includes(search.toLowerCase()) })).length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>

  );
}