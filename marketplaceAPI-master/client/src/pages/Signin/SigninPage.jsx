import React, { useState, useEffect } from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import frLocale from "date-fns/locale/fr";
import enLocale from "date-fns/locale/en-GB";
import DateFnsUtils from '@date-io/date-fns';
import {
  DatePicker
} from '@material-ui/pickers';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import Paper from '@material-ui/core/Paper';
import { NavLink } from 'react-router-dom';

import ReCAPTCHA from 'react-google-recaptcha'

import * as _ from 'lodash';

import Checkbox from '@material-ui/core/Checkbox';

import FormControlLabel from '@material-ui/core/FormControlLabel';

import { CONF } from '../../config/index'

import { useTranslation } from 'react-i18next';

import LoadingPage from '../Loading/LoadingPage';

import ReactGA from 'react-ga';
import Grid from '@material-ui/core/Grid';

ReactGA.pageview('/signin');

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      width: "100%",
      marginTop: "20px",
      marginBottom: "20px"
    },
    fbGroupLink: {
      marginLeft: "15px",
      marginTop: "0px",
      "& a": {
        marginTop: "0px",
        padding: "0px 16px",
        marginBottom: "12px"
      }
    },
    link: {
      textAlign: "center"
    },
    birthday: {
      margin: "15px 0px 15px",
      width: "100%",
      "& div input": {
        margin: "0px"
      },
      "& div:before": {
        borderBottom: "0px !important"
      },
      "& label": {
        fontSize: "14px"
      }
    },
    paperForm: {
      padding: "25px"
    },
    fbIcon: {
      position: "relative",
      top: "5px",
      right: "5px"
    },
    facebookButton: {
      backgroundColor: theme.colors.facebook,
      color: "white",
      padding: "5px 20px"
    },
    connectionButton: {
      textAlign: "center",
      margin: "20px"
    },
    divider: {
      margin: "20px"
    },
    input: {
      "& label": {
        fontSize: "14px"
      },
      "& .MuiFormLabel-root.Mui-error": {
        color: "grey"
      },
      "& fieldset": {
        borderColor: "lightgrey !important"
      }
    }
  }),
);


const localeMap = {
  fr: frLocale,
  en: enLocale
};

const sitekey = CONF.sitekey;

const regexMail = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$')

const regexPassword = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}");

export default function SigninPage(props) {
  const { t, i18n } = useTranslation();
  let classes = useStyles();

  const [selectedDate, setSelectedDate] = useState(null);
  const [name, setName] = useState('');
  const [surname, setSurname] = useState('');
  const [mail, setMail] = useState('');
  const [password, setPassword] = useState('');
  const [newsletter, setNewsletter] = useState(false);
  const [captchaChecked, setCaptchaChecked] = useState(false);
  const [userID, setUserID] = useState(null);

  useEffect(() => {
    if (!_.isEmpty(props.preFilled)) {
      if (props.preFilled.mail) setMail(props.preFilled.mail);
      if (props.preFilled.surname) setSurname(props.preFilled.surname);
      if (props.preFilled.name) setName(props.preFilled.name);
      if (props.preFilled.userID) setUserID(props.preFilled.userID);
      
    }
  }, [props.preFilled])

  const handleSigninAttempt = (e) => {
    e.preventDefault();
    let input = { mail, name, surname, birthday: selectedDate, newsletter }
    if (userID) {
      props.onSigninAttemptFacebook(input);
      ReactGA.event({
        category: 'Signin',
        action: 'Facebook signin',
        label: `Signin`
      });
    } else {
      input = { ...input, password }
      props.onSigninAttempt(input);
      ReactGA.event({
        category: 'Signin',
        action: 'Internal signin',
        label: `Signin`
      });
    }

  }

  const handleDateChange = (e) => {
    setSelectedDate(e);
  }

  const handleChangeName = (event) => {
    setName(event.target.value);
  }

  const handleChangeSurname = (event) => {
    setSurname(event.target.value);
  }

  const handleChangeMail = (event) => {
    setMail(event.target.value);
  }

  const handleChangePassword = (event) => {
    setPassword(event.target.value);
  }

  const handleChangeNewsletter = (event) => {
    setNewsletter(event.target.checked);
  }

  const handleCaptchaResponseChange = (response) => {
    setCaptchaChecked(true);
  }

  return (
    <Grid container style={{ height: "100%" }} justify="center" alignItems="center">
      <Grid item xs={12} sm={8} >
        <div className={classes.root}>
          <div style={{ textAlign: "center" }}>
            <h3>{t('signin.title')}</h3>
            <p>{t('signin.subTitle')}</p>
          </div>
          {
            props.loadingState && <Paper className={classes.paperForm}><LoadingPage /></Paper>
          }
          {
            !props.loadingState &&
            <Paper className={classes.paperForm}>

              <TextField
                id="name"
                label={t('form.name.label')}
                value={name}
                onChange={handleChangeName}
                fullWidth
                required
                margin="normal"

                error={name.trim() === ""}
                helperText={!!name && name.trim() === "" ? t('form.name.helper') : ''}
                className={classes.input}
                variant="outlined"
              />

              <TextField
                id="surname"
                label={t('form.surname.label')}
                value={surname}
                onChange={handleChangeSurname}
                fullWidth
                required
                margin="normal"
                error={surname.trim() === ""}
                helperText={!!surname && surname.trim() === "" ? t('form.surname.helper') : ''}
                variant="outlined"
                className={classes.input}
              />

              <MuiPickersUtilsProvider utils={DateFnsUtils} locale={localeMap[i18n.language]} >
                <DatePicker
                  disableFuture
                  openTo="year"
                  format="dd/MM/yyyy"
                  label={t('form.birthday.label')}
                  views={["year", "month", "date"]}
                  value={selectedDate}
                  onChange={handleDateChange}
                  className={classes.birthday}
                  variant="inline"
                  inputVariant="outlined" />
              </MuiPickersUtilsProvider>

              <TextField
                id="mail"
                label={t('form.mail.label')}
                value={mail}
                onChange={handleChangeMail}
                fullWidth
                required
                margin="normal"
                helperText={!!mail && !regexMail.test(mail) ? t('form.mail.helper') : ''}
                error={!regexMail.test(mail)}
                variant="outlined"
                className={classes.input}
              />

              {
                (!userID || userID.trim() === "") &&
                <TextField
                  id="password"
                  label={t('form.password.label')}
                  value={password}
                  onChange={handleChangePassword}
                  fullWidth
                  required

                  type="password"
                  margin="normal"
                  helperText={!!password && !regexPassword.test(password) ? t('form.password.helper') : ''}
                  error={!regexPassword.test(password)}
                  variant="outlined"
                  className={classes.input}
                />
              }


              {sitekey !== "" &&
                <ReCAPTCHA
                  sitekey={sitekey}
                  onChange={handleCaptchaResponseChange}
                />
              }

              <FormControlLabel
                control={
                  <Checkbox name="newsletter" value={newsletter}
                    onChange={handleChangeNewsletter} />
                }
                label={t('form.newsletter.label')}
              />

              {/*<div className={classes.fbGroupLink}><a href="https://www.facebook.com/groups/256271651950647/" target="_blank" rel="noopener noreferrer">{t('link.facebookGroupLink')}</a></div>*/}

              <div style={{ textAlign: "center" }}>
                <Button disabled={
                  ((userID && userID.trim() === "") || (!userID && (!password || (password.trim() && !regexPassword.test(password)))))
                  || (!mail || (!!mail && !regexMail.test(mail)))
                  || name.trim() === ""
                  || surname.trim() === ""
                  || (sitekey !== "" && !captchaChecked)
                }
                  variant="contained"
                  onClick={handleSigninAttempt}>
                  {t('signin.button')}
                </Button>
              </div>
            </Paper>
          }
        </div>
      </Grid>
    </Grid>
  );

}