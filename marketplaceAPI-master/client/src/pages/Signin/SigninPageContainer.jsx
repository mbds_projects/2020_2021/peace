import React, { useEffect, useState } from 'react';

import { toast } from 'react-toastify';
import routerHistory from '../../shared/router-history/router-history';

import SigninPage from './SigninPage';
import { createUserGql, createUserFacebookGql } from '../../graphQL/user'

import { login } from '../../core/modules/authentication.module';

import { login as loginStore } from '../../shared/store/actions/authentication.actions';

import { useDispatch } from "react-redux";

import { useTranslation } from 'react-i18next';

import { Trans } from 'react-i18next';

import { useLocation} from 'react-router-dom';

import { browserType } from '../../browserType'
import { useSelector } from "react-redux";

import ReactGA from 'react-ga';
export default function SigninPageContainer() {
  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  const [loadingState, setLoadingState] = useState(false);
  const [preFilled, setPreFilled] = useState({});
  const { t } = useTranslation();

  const location = useLocation();

  useEffect(() => {
    let locationDetail = location.detail;
    if(locationDetail) {
      setPreFilled(locationDetail)
    }
  }, [location])

  useEffect(() => {
    if(loggedInUser) {
      routerHistory.push('/')
    }
  }, [loggedInUser])

  const dispatch = useDispatch();

  const Msg = () => (
    <Trans i18nKey="login.toast.messageFirefoxSupport"
          values={{ linkTextFirefoxSupport: t('login.toast.linkTextFirefoxSupport')}}
          components={[<a style={{color: "white", fontWeight: "bold", textDecoration: "underline"}} target="_blank" rel="noopener noreferrer" href={t('login.toast.linkFirefoxSupport')}></a>]}/>
  )

  useEffect(() => {
    let browser = browserType();
    if(browser === "Firefox") {
      toast.info(<Msg />, 
        {autoClose: false});
    }

  }, [])

  const onSigninAttemptFacebook = async(args) => {
    setLoadingState(true);
    let input = args;
    if(preFilled) {
      input.facebookID = preFilled.userID;
      input.facebookAccessToken = preFilled.accessToken;
    };
    try {
      const signinAttemptUser = await createUserFacebookGql({input});
      if (signinAttemptUser) {
          let user = await login(signinAttemptUser.token);
          ReactGA.event({
            category: 'Signin',
            action: 'Facebook signin',
            label: `Signin`
          });
          window.azameoTagEvent = {
                   name : "account_creation",
                   category: "account",
                   ref : "account_" + user.id,
                   type : "lead"
          };
          if(window.azameoTag) window.azameoTag.Conversion();
          dispatch(loginStore(user));
          routerHistory.goBack();
      } else {
        toast.error(t('signin.toast.wrongDetails'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    catch(e) {
      if(e.message.includes("mail.alreadyExist")) {
        toast.error(t('signin.toast.mailAlreadyExist'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else if (e.message.includes("facebook.alreadyExist")) {
        toast.error(t('signin.toast.facebookAlreadyExist'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('error.somethingWrongAppends'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    setLoadingState(false);
  }

  const onSigninAttempt = async(args) => {
    setLoadingState(true);
    let input = args;
    try {
      const signinAttemptUser = await createUserGql({input});
      if (signinAttemptUser) {
          let user = await login(signinAttemptUser.token);
          ReactGA.event({
            category: 'Signin',
            action: 'Internal signin',
            label: `Signin`
          });
          window.azameoTagEvent = {
                   name : "account_creation",
                   category: "account",
                   ref : "account_" + user.id,
                   type : "lead"
          };
          if(window.azameoTag) window.azameoTag.Conversion();
          dispatch(loginStore(user));
          routerHistory.goBack();
      } else {
        toast.error(t('signin.toast.wrongDetails'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    catch(e) {
      if(e.message.includes("mail.alreadyExist")) {
        toast.error(t('signin.toast.mailAlreadyExist'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else if (e.message.includes("facebook.alreadyExist")) {
        toast.error(t('signin.toast.facebookAlreadyExist'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('error.somethingWrongAppends'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    setLoadingState(false);
    
  }

  const childProps = {
    onSigninAttempt,
    loadingState,
    preFilled,
    onSigninAttemptFacebook
  }

  return (<SigninPage {...childProps} />);
  
}