import React from 'react';

import routerHistory from '../../shared/router-history/router-history';

import Button from '@material-ui/core/Button';
import CardMembershipIcon from '@material-ui/icons/CardMembership';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles, createStyles, withStyles } from '@material-ui/core/styles';

import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) =>
    createStyles({
        centerButton: {
            textAlign: "center"
        },
        publishLink: {
            textAlign: "center",
            textDecoration: "underline",
            cursor: "pointer",
            padding: "10px"
        }
    }),
);

const ColorButtonAlmond = withStyles((theme) => ({
    root: {
        color: "white",
        fontSize: "20px",
        fontWeight: "bolder",
        backgroundColor: theme.colors.almond
    },
}))(Button);

export default function DialogFirstPublish(props) {
    const { t } = useTranslation();
    const classes = useStyles();

    const goToSubscription = () => {
        routerHistory.push('/subscribe');
    }

    const publish = () => {
        props.setOpenFirstPublish(false);
        props.publishDashboard();
    }

    const handleCloseFirstPublish = () => {
        props.setOpenFirstPublish(false);
    }

    return (
        <Dialog
            open={props.openFirstPublish}
            onClose={handleCloseFirstPublish}>
            <DialogTitle>{t('vendorDashboard.firstPublish.title')}</DialogTitle>
            <DialogContent>
                <p>{t('vendorDashboard.firstPublish.info')}</p>
                <div className={classes.centerButton}>
                    <ColorButtonAlmond
                        size="large"
                        variant="contained"
                        color="primary"
                        onClick={goToSubscription}
                        endIcon={<CardMembershipIcon />}
                    >
                        {t('vendorDashboard.firstPublish.passToUnlimited')}
                    </ColorButtonAlmond>
                </div>
                <div onClick={publish} className={classes.publishLink}>
                    {t('vendorDashboard.firstPublish.do')}
                </div>
            </DialogContent>
        </Dialog>
    )
}