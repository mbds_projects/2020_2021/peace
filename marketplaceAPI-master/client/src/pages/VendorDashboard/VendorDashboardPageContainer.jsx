import VendorDashboardPage from './VendorDashboardPage';
import VendorDraftDashboardPage from './VendorDraftDashboardPage';

import routerHistory from '../../shared/router-history/router-history';

import React, { useState, useEffect } from 'react';

import LoadingPage from '../Loading/LoadingPage';
import { makeStyles, createStyles } from '@material-ui/core/styles';

import { vendorsLimitedGql } from '../../graphQL/vendor';

import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import Button from '@material-ui/core/Button';
import PublishIcon from '@material-ui/icons/Publish';
import { compareAsc } from 'date-fns'

import {
  profileVendorDashboardGql, allowedToPublishGql
} from '../../graphQL/vendorDashboard';

import { retrieveSubscriptionGql } from '../../graphQL/subscriptionDashboard';

import {
  vendorDraftDashboardGql, updateCarouselPictureVendorDraftDashboardGql,
  updateFeaturedProductInfoVendorDraftDashboardGql, updateFeaturedProductPictureVendorDraftDashboardGql,
  updateInfoVendorDraftDashboardGql, publishDraftDashboardGql
} from '../../graphQL/vendorDraftDashboard';

import {
  locationsOfVendorByCityGql, locationsOfVendorByCountyGql, createLocationDashboardGql,
  updateLocationDashboardGql, removeLocationFromVendorDashboardGql, allowedToCreateLocationGql
} from '../../graphQL/location';

import { useDispatch, useSelector } from "react-redux";

import * as _ from "lodash";

import { setVendors } from '../../shared/store/actions/vendor.actions';

import { Trans } from 'react-i18next';
import { useTranslation } from 'react-i18next';

import { toast } from 'react-toastify';

import { toastError } from '../../shared/utils/error';

import ReactGA from 'react-ga';

import Switch from '@material-ui/core/Switch';

import Grid from '@material-ui/core/Grid';

import { NavLink } from 'react-router-dom';

import { setCategories as setCategoriesInternal } from '../../shared/store/actions/category.actions';

import { setKeywords as setKeywordsInternal } from '../../shared/store/actions/keyword.actions';

import { categoriesGql } from '../../graphQL/category'

import { keywordsGql } from '../../graphQL/keyword'
const useStyles = makeStyles((theme) =>
  createStyles({
    notPublished: {
      //backgroundColor: "lightgrey",
      color: "black",
      padding: "10px",
      fontSize: "16px",
      marginBottom: "20px",
      marginTop: "30px",
      border: "2px lightgrey solid"
    },
    passToUnlimited: {
      color: "black",
      textDecoration: "underline",
      fontSize: "18px",
      //fontWeight: "bold",
      paddingLeft: "5px"
    },
    isValidSubscription: {
      border: `2px ${theme.colors.almond} solid`,
      color: "black",
      padding: "10px",
      fontSize: "16px",
      marginBottom: "20px",
      marginTop: "30px",
    },
  }),
);

export default function VendorDashboardPageContainer() {
  const dispatch = useDispatch();

  const { t } = useTranslation();
  const classes = useStyles();
  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  const vendorsInternal = useSelector((state) => state.vendor.vendors);
  const categoriesInternal = useSelector((state) => state.category.categories);
  const keywordsInternal = useSelector((state) => state.keyword.keywords);

  const [profileVendor, setProfileVendor] = useState(null);
  const [draftVendor, setDraftVendor] = useState(null);
  const [cityToDisplay, setCityToDisplay] = useState(0);
  const [descCollapsed, setDescCollapsed] = useState(true);
  const [alsoAvailableOn, setAlsoAvailableOn] = useState([]);
  const [loadingState, setLoadingState] = useState(true);
  const [alsoAvailableOnLoadingState, setAlsoAvailableOnLoadingState] = useState(true);
  const [suggestions, setSuggestions] = useState([]);
  const [citiesSuggestions, setCitiesSuggestions] = useState([]);
  const [countiesSuggestions, setCountiesSuggestions] = useState([]);
  const [countiesAndCitiesSuggestions, setCountiesAndCitiesSuggestions] = useState([]);
  const [editModeChecked, setEditModeChecked] = useState(false);
  const [openSubscription, setOpenSubscription] = useState(false);
  const [openFirstPublish, setOpenFirstPublish] = useState(false);
  const [isValidSubscription, setIsValidSubscription] = useState(false);
  const [subscription, setSubscription] = useState(null);


  const [categories, setCategories] = useState([]);
  const [keywords, setKeywords] = useState([]);

  const handleIncorrectIdParameter = () => {
    routerHistory.replace('/404');
  }

  useEffect(() => {
    let didCancel = false

    async function retrieveVendorPage() {
      !didCancel && setLoadingState(true);
      let profileVendor = await profileVendorDashboardGql()
      !didCancel && setProfileVendor(profileVendor);
      ReactGA.pageview(`/vendor/dashboard/${profileVendor.name}`);
      !didCancel && setLoadingState(false);
    }

    async function retrieveSubscription() {
      !didCancel && setLoadingState(true);
      let subscriptionTmp = await retrieveSubscriptionGql();
      !didCancel && setSubscription(subscriptionTmp);
      let isValidTmp = subscriptionTmp && (subscriptionTmp.status === "active" || subscriptionTmp.status === "trialing" || subscriptionTmp.status === "canceled") && (compareAsc(new Date(), new Date(subscriptionTmp.current_period_end * 1000)) < 0)
      !didCancel && setIsValidSubscription(isValidTmp);
      !didCancel && setLoadingState(false);
    }

    async function retrieveDraftVendor() {
      !didCancel && setLoadingState(true);
      let draftVendor = await vendorDraftDashboardGql()
      !didCancel && setDraftVendor(draftVendor);
      !didCancel && setLoadingState(false);
    }
    
    if (!loggedInUser || loggedInUser.role !== "vendor") {
      handleIncorrectIdParameter();
      return;
    } else {
      Promise.all([retrieveVendorPage(), retrieveSubscription(), retrieveDraftVendor()])
    }

    return () => { didCancel = true }
  }, [loggedInUser]);

  useEffect(() => {
    let didCancel = false

    async function retrieveAlsoAvailable() {
      !didCancel && setAlsoAvailableOnLoadingState(true);
      if (profileVendor.alsoAvailableOn.length > 0) {
        let alsoAvailableOn;
        if (vendorsInternal.length > 0) {
          alsoAvailableOn = vendorsInternal.filter((v) => profileVendor.alsoAvailableOn.includes(v.id));
        } else {
          let vendors = await vendorsLimitedGql();
          alsoAvailableOn = vendors.filter((v) => profileVendor.alsoAvailableOn.includes(v.id));
          !didCancel && dispatch(setVendors(vendors))
        }
        !didCancel && setAlsoAvailableOn(alsoAvailableOn);
        !didCancel && setAlsoAvailableOnLoadingState(false);
      } else {
        !didCancel && setAlsoAvailableOn([]);
        !didCancel && setAlsoAvailableOnLoadingState(false);
      }
    }

    async function retrieveCitiesSuggestions() {
      if (profileVendor.locations.length > 0) {
        let citiesSuggestions = await locationsOfVendorByCityGql({ vendorId: profileVendor.id });

        let countiesSuggestions = await locationsOfVendorByCountyGql({ vendorId: profileVendor.id });


        citiesSuggestions = citiesSuggestions.map((loc) => { return { shops: loc.shops, loc: loc.city } })
        countiesSuggestions = countiesSuggestions.map((loc) => { return { shops: loc.shops, loc: loc.county } })

        !didCancel && setCitiesSuggestions(citiesSuggestions);

        !didCancel && setCountiesSuggestions(countiesSuggestions);

        let suggestionsCitiesCounties = _.unionBy(citiesSuggestions, countiesSuggestions, 'loc');

        !didCancel && setCountiesAndCitiesSuggestions(suggestionsCitiesCounties);

        let suggestions = suggestionsCitiesCounties.map((el, index) => {
          return {
            value: index,
            name: el.loc
          }
        });

        !didCancel && setSuggestions(suggestions);
      } else {
        !didCancel && setCitiesSuggestions([]);
        !didCancel && setCountiesSuggestions([]);
        !didCancel && setCountiesAndCitiesSuggestions([]);
        !didCancel && setSuggestions([]);
      }
    }
    if (profileVendor) {
      Promise.all([retrieveAlsoAvailable(), retrieveCitiesSuggestions()]);
    }

    return () => { didCancel = true }
  }, [profileVendor]);

  useEffect(() => {
    let didCancel = false
    const getCategories = async () => {
      if (categoriesInternal.length <= 0) {
        let categories = await categoriesGql();
        !didCancel && dispatch(setCategoriesInternal(categories));
      }
    }

    const getKeywords = async () => {
      if (keywordsInternal.length <= 0) {
        let keywords = await keywordsGql();
        !didCancel && dispatch(setKeywordsInternal(keywords));
      }
    }

    Promise.all([getCategories(), getKeywords()])

    return () => { didCancel = true }
  }, [categoriesInternal, keywordsInternal]);

  useEffect(() => {
    let didCancel = false
    if(profileVendor) {
      let cats = categoriesInternal.filter((cat) => profileVendor.categories.includes(cat.id));
      !didCancel && setCategories(cats);
    }

    return () => { didCancel = true }
  }, [categoriesInternal, profileVendor])

  useEffect(() => {

    let didCancel = false
    if(profileVendor) {
      let kws = keywordsInternal.filter((kw) => profileVendor.keywords.includes(kw.id));
      !didCancel && setKeywords(kws);
    }
    return () => { didCancel = true }
  }, [keywordsInternal, profileVendor])

  const handleCityClick = (cityIndex) => {
    if (cityToDisplay !== cityIndex && profileVendor) {
      setCityToDisplay(cityIndex);
    }
  }

  const handleToggleDesc = () => {
    setDescCollapsed(!descCollapsed);
  }

  const updateData = async () => {
    let profileVendor = await profileVendorDashboardGql()
    setProfileVendor(profileVendor);
  }


  const updateDraftData = async () => {
    let draftVendor = await vendorDraftDashboardGql()
    setDraftVendor(draftVendor);
  }

  const updateInfoVendor = async (input) => {
    try {
      let res = await updateInfoVendorDraftDashboardGql({ input: input });
      if (res) {
        toast.success(t('vendorDashboard.toast.infoUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await updateDraftData();
      } else {
        toast.error(t('vendorDashboard.toast.infoUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return res;
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const uploadCarouselPicture = async (file, indexOfImage) => {
    try {
      let res = await updateCarouselPictureVendorDraftDashboardGql({ file, indexOfImage });
      if (res) {
        toast.success(t('vendorDashboard.toast.imageUploaded'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await updateDraftData();
      } else {
        toast.error(t('vendorDashboard.toast.imageUploadedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return res;
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const uploadFeaturedProductPicture = async (file, indexOfImage, link, price) => {
    try {
      let res = await updateFeaturedProductPictureVendorDraftDashboardGql({ file, indexOfImage, link, price });
      if (res) {
        toast.success(t('vendorDashboard.toast.imageUploaded'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await updateDraftData();
      } else {
        toast.error(t('vendorDashboard.toast.imageUploadedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return res;
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const uploadFeaturedProductInfo = async (indexOfProduct, link, price) => {
    try {
      let res = await updateFeaturedProductInfoVendorDraftDashboardGql({ indexOfProduct, link, price });
      if (res) {
        toast.success(t('vendorDashboard.toast.infoUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await updateDraftData();
      } else {
        toast.success(t('vendorDashboard.toast.infoUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
      return res;
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const createLocation = async (input) => {
    try {
      let res = await createLocationDashboardGql({ input });
      if (res) {
        toast.success(t('vendorDashboard.toast.locationAdded'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await updateData();
        return true;
      } else {
        toast.error(t('vendorDashboard.toast.locationAddedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
        return false;
      }
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const updateLocation = async (input, locId) => {
    try {
      let res = await updateLocationDashboardGql({ input, locId });
      if (res) {
        toast.success(t('vendorDashboard.toast.locationUpdated'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await updateData();
        return true;
      } else {
        toast.error(t('vendorDashboard.toast.locationUpdatedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
        return false;
      }
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const removeLocation = async (locId) => {
    try {
      let res = await removeLocationFromVendorDashboardGql({ locId });
      if (res) {
        toast.success(t('vendorDashboard.toast.locationRemoved'), {
          position: toast.POSITION.TOP_RIGHT
        });
        await updateData();
        return true;
      } else {
        toast.error(t('vendorDashboard.toast.locationRemovedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
        return false;
      }
    } catch (e) {
      toastError(e.message, t);
    }
  }

  const publishDashboard = async () => {
    try {
      let res = await publishDraftDashboardGql();

      if (res) {
        toast.success(t('vendorDashboard.toast.published'), {
          position: toast.POSITION.TOP_RIGHT
        });
        /*let isValidSubscriptionTmp = await isValidSubscriptionGql();
        setIsValidSubscription(isValidSubscriptionTmp);*/
        await updateData();
        return true;
      } else {
        toast.error(t('vendorDashboard.toast.publishedError'), {
          position: toast.POSITION.TOP_RIGHT
        });
        return false;
      }
    } catch (e) {
      toastError(e.message, t)
    }
  }

  const isAllowed = async () => {
    let allowedToPublishTmp = await allowedToPublishGql();
    return allowedToPublishTmp;
  }

  const isAllowedToCreateLocation = async () => {
    return await allowedToCreateLocationGql();
  }

  const validSubscription = () => {
    let isValidSubscriptionTmp = subscription && (subscription.status === "active" || subscription.status === "trialing" || subscription.status === "canceled") && (compareAsc(new Date(), new Date(subscription.current_period_end * 1000)) < 0)
    setIsValidSubscription(isValidSubscriptionTmp);
    return isValidSubscriptionTmp;
  }

  const publish = async () => {
    let isValidSubscriptionTmp = validSubscription();
    if (isValidSubscriptionTmp) {
      if(subscription.product.name === "Basic" && !profileVendor.published) {
        setOpenFirstPublish(true);
      } else {
        publishDashboard();
      }
    } else {
      setOpenSubscription(true);
    }
  }

  if (!profileVendor) {
    return <LoadingPage />
  }

  let subscriptionInfo = "";
  if(subscription) {
    switch(subscription.product.name) {
      case "Basic": subscriptionInfo = "infoBasic"; break;
      case "Super":  subscriptionInfo = "infoSuper"; break;
      case "Super +":  subscriptionInfo = "infoSuperPlus"; break;
    }
  }

  return (<div>
    <Grid container direction="row" justify="flex-end" spacing={2}>
      {
        !editModeChecked && !profileVendor.published &&
        <Grid item xs={12}>
          <div className={classes.notPublished}>
            <Trans i18nKey="vendorDashboard.goEdit.infoNotPublished"
              components={[<NavLink to="/subscribe" className={classes.passToUnlimited} />]} />
          </div>
        </Grid>
      }
      {
        !editModeChecked && profileVendor.published &&
        <Grid item xs={12}>
          <div className={classes.isValidSubscription}>
            <Trans i18nKey="vendorDashboard.goEdit.infoPublished"
              components={[<span style={{fontWeight: 'bold'}}></span>]} />
          </div>
        </Grid>
      }
      {
        editModeChecked && !isValidSubscription && !subscription &&
        <Grid item xs={12}>
          <div className={classes.notPublished}>
            <Trans i18nKey="vendorDashboard.allowedToPublish.info"
              components={[<NavLink to="/subscribe" className={classes.passToUnlimited} />]} />
          </div>
        </Grid>
      }
      {
        editModeChecked && !isValidSubscription && subscription &&
        <Grid item xs={12}>
          <div className={classes.notPublished}>
            <Trans i18nKey="vendorDashboard.allowedToPublish.infoIncomplete"
              components={[<NavLink to="/subscribe" className={classes.passToUnlimited} />]} />
          </div>
        </Grid>
      }
      {
        editModeChecked && isValidSubscription &&
        <Grid item xs={12}>
          <div className={classes.isValidSubscription}>
            <Trans i18nKey={`vendorDashboard.isValidSubscription.${subscriptionInfo}`}
            values={{status: subscription.product.name}}
            components={[<span style={{fontWeight: 'bold'}}></span>]} />
          </div>
        </Grid>
      }
      {editModeChecked && <Grid item><Button
        variant="contained"
        color="primary"
        onClick={publish}
        endIcon={<PublishIcon />}
      >
        {t('vendorDashboard.publish')}
      </Button></Grid>}
      <Grid item>
        <FormGroup row>
          <FormControlLabel
            control={
              <Switch checked={editModeChecked} onChange={(event) => setEditModeChecked(event.target.checked)} value={editModeChecked} />
            }
            label={t('vendorDashboard.editMode')}
          />
        </FormGroup></Grid>
    </Grid>
    {
      editModeChecked && draftVendor ?
        <VendorDraftDashboardPage
          profileVendor={draftVendor}
          cityToDisplay={cityToDisplay}
          handleCityClick={handleCityClick}
          handleToggleDesc={handleToggleDesc}
          descCollapsed={descCollapsed}
          loadingState={loadingState}
          alsoAvailableOn={alsoAvailableOn}
          alsoAvailableOnLoadingState={alsoAvailableOnLoadingState}
          citiesSuggestions={citiesSuggestions}
          countiesSuggestions={countiesSuggestions}
          suggestions={suggestions}
          countiesAndCitiesSuggestions={countiesAndCitiesSuggestions}
          updateInfoVendor={updateInfoVendor}
          uploadCarouselPicture={uploadCarouselPicture}
          uploadFeaturedProductPicture={uploadFeaturedProductPicture}
          uploadFeaturedProductInfo={uploadFeaturedProductInfo}
          createLocation={createLocation}
          updateLocation={updateLocation}
          removeLocation={removeLocation}
          loggedInUser={loggedInUser}
          publishDashboard={publishDashboard}
          editModeChecked={editModeChecked}
          published={profileVendor.published}
          publish={publish}
          setOpenSubscription={setOpenSubscription}
          openSubscription={openSubscription}
          isAllowed={isAllowed}
          subscription={subscription}
          isAllowedToCreateLocation={isAllowedToCreateLocation}
          categories={categories}
          keywords={keywords}
          setOpenFirstPublish={setOpenFirstPublish}
          openFirstPublish={openFirstPublish}
        />
        :
        <VendorDashboardPage
          profileVendor={profileVendor}
          cityToDisplay={cityToDisplay}
          handleCityClick={handleCityClick}
          handleToggleDesc={handleToggleDesc}
          descCollapsed={descCollapsed}
          loadingState={loadingState}
          alsoAvailableOn={alsoAvailableOn}
          alsoAvailableOnLoadingState={alsoAvailableOnLoadingState}
          citiesSuggestions={citiesSuggestions}
          countiesSuggestions={countiesSuggestions}
          suggestions={suggestions}
          countiesAndCitiesSuggestions={countiesAndCitiesSuggestions}
          loggedInUser={loggedInUser}
          categories={categories}
          keywords={keywords}
        />
    }
  </div>)

}
