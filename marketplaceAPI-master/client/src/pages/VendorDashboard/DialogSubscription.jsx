import React from 'react';

import routerHistory from '../../shared/router-history/router-history';

import Button from '@material-ui/core/Button';
import CardMembershipIcon from '@material-ui/icons/CardMembership';
import { makeStyles, createStyles, withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) =>
    createStyles({
        centerButton: {
            textAlign: "center"
        }
    }),
);

const ColorButtonAlmond = withStyles((theme) => ({
    root: {
        color: "white",
        fontSize: "20px",
        fontWeight: "bolder",
        backgroundColor: theme.colors.almond
    },
}))(Button);

export default function DialogSubscription(props) {
    const { t } = useTranslation();

    const classes = useStyles();

    const goToSubscription = () => {
        routerHistory.push('/subscribe');
    }

    const handleCloseSubscription = () => {
        props.setOpenSubscription(false);
    }

    return (
        <Dialog
            open={props.openSubscription}
            onClose={handleCloseSubscription}>
            <DialogTitle>{t('vendorDashboard.subscription.title')}</DialogTitle>
            <DialogContent>
                <p>{t('vendorDashboard.subscription.info')}</p>

                <div className={classes.centerButton}>
                <ColorButtonAlmond
                    variant="contained"
                    color="primary"
                    onClick={goToSubscription}
                    endIcon={<CardMembershipIcon />}
                >
                    {t('vendorDashboard.subscription.do')}
                </ColorButtonAlmond>
                </div>
            </DialogContent>
        </Dialog>
    )
}