import React from 'react';

import VendorCard from '../../components/VendorCard/VendorCard';

import Carousel from "../../components/Carousel/Carousel";
import PinterestCard from "../../components/PinterestCard/PinterestCard";

import { Trans } from 'react-i18next';

import Avatar from '@material-ui/core/Avatar';

import Grid from '@material-ui/core/Grid';

import { useTranslation } from 'react-i18next';

import useMediaQuery from '@material-ui/core/useMediaQuery';

import Truncate from 'react-truncate';

import Loading from '../Loading/LoadingPage';

import { compareAsc } from 'date-fns'

//import ReactGA from 'react-ga';
import SocialNetworks from "../../components/SocialNetworks/SocialNetworks"
import InfoSection from "../../components/InfoSection/InfoSection"
import CitiesSection from "../../components/CitiesSection/CitiesSection"

import { useStyles } from "./Styles"

export default function VendorDashboardPage(props) {
  const { t, i18n } = useTranslation();
  const matches1560 = useMediaQuery('(max-width:1560px)');
  const matches1250 = useMediaQuery('(max-width:1250px)');
  const matches1040 = useMediaQuery('(max-width:1120px)');
  const matches = useMediaQuery('(max-width:768px)');

  const classes = useStyles();

  const toggleDescription = () => {
    props.handleToggleDesc();
  }

  const vendor = props.profileVendor;
  let descLang = vendor.desc.lang.find((l) => l.locale === i18n.language);
  const rewardsCount = vendor.rewards && vendor.rewards.length > 0 ? vendor.rewards.filter(reward => (reward.activated && !(reward.validity && compareAsc(new Date(reward.validity), new Date()) < 0))).length : 0;

  let videoSection = '';
  if (vendor.videoYoutubeId) videoSection = <iframe title="youtube video" className={classes.youtubeVideo} width="560" height="315" src={`https://www.youtube.com/embed/${vendor.videoYoutubeId}`}></iframe>
  if (vendor.videoDailymotionId) videoSection = <iframe title="dailymotion video" className={classes.youtubeVideo} width="560" height="315" src={`https://www.dailymotion.com/embed/video/${vendor.videoDailymotionId}`}></iframe>
  if (vendor.videoVimeoId) videoSection = <iframe title="vimeo-player" className={classes.youtubeVideo} width="560" height="290" src={`https://player.vimeo.com/video/${vendor.videoVimeoId}`}></iframe>

  let widthTruncate = 500;
  if (matches1560) widthTruncate = 400;
  if (matches1250) widthTruncate = 350;
  if (matches1040) widthTruncate = 300;
  if (matches) widthTruncate = 300;

  const descSection =
    <div className={matches ? classes.descMobile : classes.desc} >
      <b>{t('vendorProfile.ourStory')}</b>
      <br />
      {
        descLang && descLang.value ?
          <span>
            {(props.descCollapsed ?
              <Truncate className={classes.descNoWrap} lines={3} width={widthTruncate} ellipsis={
                <span onClick={toggleDescription} className={classes.toggleDesc}>+ {t('vendorProfile.more')}</span>}>

                {descLang.value.split('\\n').map((line, i, arr) => {
                  const newLine = <span key={i}>{line}</span>;

                  if (i === arr.length - 1) {
                    return newLine;
                  } else {
                    return [newLine, <br key={i + 'br'} />];
                  }
                })}
              </Truncate>
              :
              <span>
                {descLang.value.split('\\n').map((line, i, arr) => {
                  const newLine = <span key={i}>{line}</span>;

                  if (i === arr.length - 1) {
                    return newLine;
                  } else {
                    return [newLine, <br key={i + 'br'} />];
                  }
                })}
                <span onClick={toggleDescription} className={classes.toggleDesc}>- {t('vendorProfile.less')}</span>
              </span>
            )}
          </span>
          :
          <span>{t('vendorDashboard.empty')}</span>
      }
    </div>


  const returnCard = (element, id, GAtitle, GAsubTitle, xs, sm) => {
    return (
      <Grid item xs={xs} sm={sm} key={id} >
        {element}
      </Grid>
    )
  }

  let alsoAvailableOnCards = props.alsoAvailableOn && props.alsoAvailableOn.map((vendor) => returnCard(<VendorCard vendor={vendor} />, vendor.id, 'Vendors', `Click on vendor ${vendor.name} who sell the vendor ${props.profileVendor.name} from his profile`, 12, 12))

  const alsoAvailableOnSection = props.alsoAvailableOn && props.alsoAvailableOn.length > 0 &&
    <div>
      <Grid container direction="row">
        {
          props.alsoAvailableOnLoadingState ?
            <div className={classes.loadingState}>
              <Loading />
            </div>
            : matches ?
              <Carousel carouselComponent={alsoAvailableOnCards} arrows={true} />
              : <Carousel carouselComponent={alsoAvailableOnCards} infinite={true} autoplay={true} dots={true} />
        }
      </Grid>

    </div>

  let carouselPicturesUrl = [];
  for (let i = 0; i < 3; i++) {
    if (vendor.carouselPicturesUrl && i < vendor.carouselPicturesUrl.length) {
      carouselPicturesUrl.push(vendor.carouselPicturesUrl[i])
    } else {
      carouselPicturesUrl.push("/assets/images/addPicture.png");
    }
  }

  const carouselPictures = carouselPicturesUrl.map((imgSrc, index) => {
    return (<div className={classes.imgContent} key={index}>
      <img alt={`Carousel image ${index}`} src={imgSrc} />
    </div>);
  })


  let featuredProducts = [];
  for (let i = 0; i < 6; i++) {
    let col = 1;
    if (i === 0 || i === 3 || i === 4) {
      col = 2;
    }
    if (i < vendor.featuredProductsUrl.length) {
      featuredProducts.push({
        link: vendor.featuredProductsUrl[i].link,
        imgUrl: vendor.featuredProductsUrl[i].imgUrl,
        col,
        empty: false
      })
    } else {
      featuredProducts.push({
        link: "",
        imgUrl: "/assets/images/addPicture.png",
        col,
        empty: true
      })
    }
  }

  let rewards =
    <Grid container direction="row" justify="center" alignItems="center" className={classes.rewards}>
      <Grid item xs={12}>
        <Trans i18nKey="vendorProfile.rewards.title"
          values={{ vendorName: vendor.name }}
          components={[<b></b>, <br />]} />
      </Grid>
      <Grid item xs={2}>
        <Avatar className={classes.diamond} src="/assets/images/icons/diamond.png" />
      </Grid>
      <Grid item xs={10}>
        {rewardsCount > 1 ?
          <Trans i18nKey="vendorProfile.rewards.explanations"
            values={{ points: rewardsCount }}
            components={[<br />, <span onClick={props.goRewards} className={classes.howToClick}></span>]} />
          :

          <Trans i18nKey="vendorProfile.rewards.explanation"
            values={{ points: rewardsCount }}
            components={[<br />, <span onClick={props.goRewards} className={classes.howToClick}></span>]} />
        }
      </Grid>
    </Grid>

  return (
    <div className={matches ? `${classes.root} ${classes.rootMobile}` : classes.root}>
    
      {
        props.loadingState ?
          <div className={classes.loadingState}>
            <Loading />
          </div>
          :
          matches ?
            <Grid container direction="column" justify="center">
              <Grid item xs={12}>
                <InfoSection vendor={vendor} categories={props.categories} keywords={props.keywords} rewards={props.rewards} goRewards={() =>{}} />
              </Grid>
              <Grid item xs={12} className={classes.sliderContainerMobile}>
                <Carousel carouselComponent={carouselPictures} infinite={true} autoplay={true} dots={true} />
              </Grid>
              <Grid item xs={12}>
                {descSection}
              </Grid>
              <Grid item xs={12}>
                <CitiesSection
                  countiesAndCitiesSuggestions={props.countiesAndCitiesSuggestions}
                  citiesSuggestions={props.citiesSuggestions}
                  vendor={vendor}
                  createLocation={props.createLocation}
                  updateLocation={props.updateLocation}
                  removeLocation={props.removeLocation}
                  suggestions={props.suggestions} />
              </Grid>
              <Grid item sm={12}>
                {props.alsoAvailableOn && props.alsoAvailableOn.length > 0 && <b className={matches ? classes.alsoAvailableOnTitle : ''}>{(t('vendorProfile.alsoAvailableOn'))}</b>}
              </Grid>
              <Grid item xs={12}>
                {alsoAvailableOnSection}
              </Grid>
              <Grid item xs={12}>
                {videoSection}
              </Grid>
              <Grid item xs={12}>
                <PinterestCard items={featuredProducts} column={3} GADetectClick={() => {}} />
              </Grid>
              <Grid item xs={12}>
                <SocialNetworks vendor={vendor} updateInfoVendor={props.updateInfoVendor}/>
              </Grid>
            </Grid>
            :
            <Grid container direction="row" spacing={3} justify="center" className={classes.bottomDesktop}>
              <Grid container direction="column" spacing={3} justify="center" item sm={6}>
                <Grid item sm={12}>

                  <InfoSection vendor={vendor} goRewards={() => {}} categories={props.categories}  keywords={props.keywords} rewards={props.rewards}  /><br />
                  {descSection}<br />
                  <SocialNetworks vendor={vendor} updateInfoVendor={props.updateInfoVendor} /><br />
                  <PinterestCard items={featuredProducts} column={3} GADetectClick={() => {}} />
                  {vendor.labels && vendor.labels.length > 0 &&
                    <div>
                      <br />
                      <b>{t('vendorProfile.labels.title')} {vendor.name}</b><br />

                      {t('vendorProfile.labels.desc')}<br />
                      {
                        vendor.labels && vendor.labels.map((el) => {
                          let imgSrc = el.pictureUrl
                          return (
                            imgSrc !== '' ? <img className={classes.label} key={el.id} src={imgSrc} alt={el.name} /> : ''
                          )
                        })
                      }
                      <br /><br />
                    </div>
                  }
                  {rewards}
                  <Grid item sm={12}>
                    {props.alsoAvailableOn && props.alsoAvailableOn.length > 0 && <b className={matches ? classes.alsoAvailableOnTitle : ''}>{(t('vendorProfile.alsoAvailableOn'))}</b>}
                  </Grid>
                  <Grid item sm={9} className={classes.alsoAvailable}>
                    {alsoAvailableOnSection}
                  </Grid>
                </Grid>
              </Grid>
              <Grid container direction="column" spacing={3} justify="center" item sm={6}>
                <Grid item sm={12} className={classes.sliderContainer}>
                  <CitiesSection
                    countiesAndCitiesSuggestions={props.countiesAndCitiesSuggestions}
                    citiesSuggestions={props.citiesSuggestions}
                    vendor={vendor}
                    createLocation={props.createLocation}
                    updateLocation={props.updateLocation}
                    removeLocation={props.removeLocation}
                    suggestions={props.suggestions} /><br />
                  <Carousel carouselComponent={carouselPictures} infinite={true} autoplay={true} dots={true} />
                  <br /><br />
                  {videoSection}
                </Grid>
              </Grid>
            </Grid>
      }
    </div>
  );
}

