import React, { useState, useEffect } from 'react';

import AdminDashboardPage from './AdminDashboardPage';
import LoadingPage from '../Loading/LoadingPage';
import { useSelector, useDispatch } from "react-redux";

import routerHistory from '../../shared/router-history/router-history';

import { categoriesGql } from '../../graphQL/category'

import { keywordsGql } from '../../graphQL/keyword'

import { gendersGql } from '../../graphQL/gender'

import { setCategories } from '../../shared/store/actions/category.actions';

import { setKeywords } from '../../shared/store/actions/keyword.actions';

import { setGenders } from '../../shared/store/actions/gender.actions';

//import { useTranslation } from 'react-i18next';
export default function AdminDashboardPageContainer() {
  //const { t } = useTranslation();
  const loggedInUser = useSelector((state) => state.authentication.currentUser);
  const categoriesInternal = useSelector((state) => state.category.categories);
  const keywordsInternal = useSelector((state) => state.keyword.keywords);
  const gendersInternal = useSelector((state) => state.gender.genders);
  const dispatch = useDispatch();

  useEffect(() => {

    const getCategories = async () => {
      if(categoriesInternal.length <= 0) {
        let categories = await categoriesGql();
        dispatch(setCategories(categories));
      }
    }
    
    const getGenders = async () => {
      if(gendersInternal.length <= 0) {
        let genders = await gendersGql();
        dispatch(setGenders(genders));
      }
    }
    
    const getKeywords = async() => {
      if(keywordsInternal.length <= 0) {
        let keywords = await keywordsGql();
        dispatch(setKeywords(keywords));
      }
    }

    Promise.all([getCategories(), getKeywords(), getGenders()])
  }, [])

  if (!loggedInUser) {
    return <LoadingPage />
  }
  

  const handleClick = (manager) => {
    routerHistory.push('/admin/' + manager);
  }

  return (<AdminDashboardPage handleClick={handleClick} />);
}
