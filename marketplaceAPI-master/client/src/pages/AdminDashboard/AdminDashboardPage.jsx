import * as React from 'react';
//import { useTranslation } from 'react-i18next';

import { makeStyles } from '@material-ui/core/styles';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles({
  root: {
    width: "99%",
    flexGrow: 1,
    backgroundColor: "white",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  card: {
    //height: "150px"
  }
});

export default function AdminDashboardPage(props) {
  //const { t } = useTranslation();

  const classes = useStyles();
  return (
    <Grid container spacing={3} justify="space-around" alignItems="flex-start">
      <Grid container direction="column" item sm={4} xs={12} spacing={3}>
      <Grid item >
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" component="h2">
                Manage Categories
            </Typography>
              <Typography variant="body2" component="p">
                Update picture and descs
            </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={() => props.handleClick("category")}>GO</Button>
            </CardActions>
          </Card>
        </Grid>

        <Grid item >
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" component="h2">
                Manage Keywords
            </Typography>
              <Typography variant="body2" component="p">
                Update picture and descs
            </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={() => props.handleClick("keyword")}>GO</Button>
            </CardActions>
          </Card>
        </Grid>

        <Grid item >
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" component="h2">
                Manage Genders
            </Typography>
              <Typography variant="body2" component="p">
                Update picture and descs
            </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={() => props.handleClick("gender")}>GO</Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item >
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" component="h2">
                Manage Labels
            </Typography>
              <Typography variant="body2" component="p">
                Create label
              <br />
                Update logo and name
            </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={() => props.handleClick("label")}>GO</Button>
            </CardActions>
          </Card>
        </Grid>


      </Grid>
      <Grid container direction="column" item sm={4} xs={12} spacing={3}>
      <Grid item >
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" component="h2">
                Manage Brands
            </Typography>
              <Typography variant="body2" component="p">
                Create, edit, transform and delete brand
              <br />
                Create, edit and delete supplier
              <br />
                Create, edit and delete instagram brand
            </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={() => props.handleClick("brand")}>GO</Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item >
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" component="h2">
                Manage Vendor Candidate
            </Typography>
              <Typography variant="body2" component="p">
                Display vendor candidate
            </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={() => props.handleClick("vendorCandidate")}>GO</Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item >
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" component="h2">
                Manage Locations
            </Typography>
              <Typography variant="body2" component="p">
                Create location
              <br />
                Update location
            </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={() => props.handleClick("location")}>GO</Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item >
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" component="h2">
                Manage User and comments
            </Typography>
              <Typography variant="body2" component="p">
                Block or unblock user
              <br />
                Delete a comment and signal user's comment
            </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={() => props.handleClick("user")}>GO</Button>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
      <Grid container direction="column" item sm={4} xs={12} spacing={3}>
       
        <Grid item >
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" component="h2">
                Send token to vendor
            </Typography>
              <Typography variant="body2" component="p">
                Vendor will receive a mail to create account<br />
                After he can choose a subscription <br />
                And manage his page, good deals ...
            </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={() => props.handleClick("vendorToken")}>GO</Button>
            </CardActions>
          </Card>
        </Grid>

        <Grid item >
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" component="h2">
                Manage Vendors
            </Typography>
              <Typography variant="body2" component="p">
                storytelling, caroussel and featured product for vendor, locations and social networks
            </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={() => props.handleClick("vendor")}>GO</Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item >
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" component="h2">
                Update vendor info
            </Typography>
              <Typography variant="body2" component="p">
                Categories, labels, values, click and collect, delivery, profile picture and logo
            </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={() => props.handleClick("vendorInfo")}>GO</Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item >
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" component="h2">
                Update vendor rewards
            </Typography>
              <Typography variant="body2" component="p">
                Create, enable and disabled
            </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" onClick={() => props.handleClick("vendorRewards")}>GO</Button>
            </CardActions>
          </Card>
        </Grid>

      </Grid>
    </Grid>


  );
}