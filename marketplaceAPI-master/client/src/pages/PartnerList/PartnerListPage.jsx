import * as React from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

//import InstagramCard from '../../components/InstagramCard/InstagramCard';

import Grid from '@material-ui/core/Grid';

//import Avatar from '@material-ui/core/Avatar';

//import Link from '@material-ui/core/Link';

import { useTranslation } from 'react-i18next';

import useMediaQuery from '@material-ui/core/useMediaQuery';

import PinterestCard from "../../components/PinterestCard/PinterestCard";

import ReactGA from 'react-ga';

ReactGA.pageview('/partner');


const useStyles = makeStyles(() =>
  createStyles({
    root: {
      margin: "auto",
      padding: "20px"
    },
    avatar: {
      width: "100px",
      height: "100px"
    },
    influencerTitle: {
      textAlign: "center"
    },
    grid: {
      "& div": {
        "& ul": {
          height: "100%",
          overflow: 'hidden',
        }
      }
    },
    gridListRoot: {
      display: 'flex',
      flexWrap: 'wrap',
      overflow: 'hidden',
      backgroundColor: "white"
    },
    gridList: {
      width: "100%",
      height: "100%",
      overflow: 'hidden',
      "&::-webkit-scrollbar": {
        width: "1em"
      },
      "&::-webkit-scrollbar-track": {
        "-webkit-box-shadow": "inset 0 0 6px rgba(0,0,0,0.3)"
      },
      "&::-webkit-scrollbar-thumb": {
        borderRadius: "4px",
        backgroundColor: "rgba(0,0,0,.3)",
        "-webkit-box-shadow": "0 0 1px rgba(255,255,255,.5)"
      }
    },
    gridItem: {
      padding: "5px !important"
    },
    titleBar: {
      background:'none',
      width: '100%',
      height: '100%',
      cursor: "pointer"
    }
  }),
);

export default function PartnerListPage(props) {
  let classes = useStyles();

  const matches = useMediaQuery('(max-width:768px)');

  const { t } = useTranslation();

  /*const GADetectClickInstagram = (link: string) => {
    ReactGA.event({
      category: 'Instagram',
      action: `Click on instagram picture ${link}`,
      label: `Partner`
    });
  }

  const GADetectClickInstagramInfluencer = (title: string) => {
    ReactGA.event({
      category: 'Instagram',
      action: `Click on instagram influencer ${title}`,
      label: `Partner`
    });
  }

  const GADetectClicMonShopResponsable = () => {
    ReactGA.event({
      category: 'Instagram',
      action: `Click on #monshopresponsable`,
      label: `Partner`
    });
  }


  const returnInstagramCard = (instagram) => {
    return (
      <Grid item xs={4} sm={2} key={instagram.link} onClick={()=>GADetectClickInstagram(instagram.link)}>
        <InstagramCard
          instagram={instagram}
          size={matches ? "small" : "large"}
        />
      </Grid>
    )
  }

  let instagramRecent = props.instagramRecent.map((insta) => returnInstagramCard(insta))


  const returnInfluencerCard = (influencer) => {
    return (
      <Grid container direction="column" justify="center" alignItems="center" 
        item xs={6} sm={3} key={influencer.title} onClick={()=>GADetectClickInstagramInfluencer(influencer.title)}>
        <Grid className={classes.influencerTitle}><Avatar src={influencer.imgUrl} className={classes.avatar}  component={Link} href={influencer.link} target="_blank" rel="noopener noreferrer"/></Grid>
        <Grid className={classes.influencerTitle}><a href={influencer.link} target="_blank" rel="noopener noreferrer"><b>{influencer.title}</b></a></Grid>
      </Grid>
    )
  }

  let influencersCard = props.influencers.map((influencer) => returnInfluencerCard(influencer));
  */
  const GADetectClickPartners = (index) => {
    ReactGA.event({
      category: 'Partners',
      action: `Click on partner number ${index + 1}`,
      label: `Partners`
    });
  }

  return (
    <div className={classes.root}>
      <h2>{t('partners.supportSR')}</h2>
          
        <Grid container direction="row" justify="flex-start" spacing={3} className={classes.grid}>
          <PinterestCard items={props.partners} column={matches ? 3 : 6} GADetectClick={GADetectClickPartners} width="100%" scrollbarVisible={true}/>
        </Grid>
      {/*<br/>
      <h2>{t('partners.supportHashtagSR')} <a href="https://www.instagram.com/explore/tags/monshopresponsable/" target="_blank" rel="noopener noreferrer" onClick={GADetectClicMonShopResponsable}>#monShopResponsable</a>
      </h2>
      <Grid container direction="row" justify="center" spacing={3}>
        <Grid container justify="center" item xs={12}>
          {influencersCard}
        </Grid>
        {instagramRecent}
      </Grid>*/}
    </div>

  )
}