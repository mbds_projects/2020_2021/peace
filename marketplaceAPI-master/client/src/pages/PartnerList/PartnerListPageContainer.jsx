import React, { useState, useEffect } from 'react';

import PartnerListPage from './PartnerListPage';

//import { instagramPopularGql } from '../../graphQL/instagram';
import { partnersGql } from '../../graphQL/partner';
//import { influencersGql } from '../../graphQL/influencer';

export default function PartnerListPageContainer() {

    const [partners, setPartners] = useState([]);

    //const [influencers, setInfluencers] = useState([]);

    //const [instagramRecent, setInstagramRecent] = useState([]);
    useEffect(() => {

        /*async function retrieveInstagramRecent() {
            let instagramRecentTmp = await instagramPopularGql();
            if(instagramRecent.length === 0 && instagramRecentTmp.length > 0) {
                setInstagramRecent(instagramRecentTmp)
            }
        }

        retrieveInstagramRecent();*/

        async function retrievePartners() {
            let partnersTmp = await partnersGql();
            if(partners.length === 0 && partnersTmp.length > 0) {
                setPartners(partnersTmp)
            }
        }

        retrievePartners();

        /*
        async function retrieveInfluencers() {
            let influencersTmp = await influencersGql();
            if(influencers.length === 0 && influencersTmp.length > 0) {
                setInfluencers(influencersTmp)
            }
        }

        retrieveInfluencers();*/
    });

    return (<PartnerListPage /*influencers={influencers}*/ partners={partners} /*instagramRecent={instagramRecent}*/ />);
}
