import * as React from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import { useTranslation } from 'react-i18next';

import { Trans } from 'react-i18next';

import { NavLink } from 'react-router-dom';
import Link from '@material-ui/core/Link';

import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import YouTubeIcon from '@material-ui/icons/YouTube';
import IconButton from '@material-ui/core/IconButton';

import useMediaQuery from '@material-ui/core/useMediaQuery';

import Grid from '@material-ui/core/Grid';

import ReactGA from 'react-ga';

ReactGA.pageview('/aboutUs');

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      backgroundColor: "white"
    },
    margin: {
      margin: "0px 10%",
      paddingTop: "40px"
    },
    marginMobile: {
      margin: "0px 10px",
      paddingTop: "20px"
    },
    center: {
      textAlign: "center",
    },
    gridFollow: {
      textAlign: "center",
      alignContent: "center"
    },
    title: {
      color: "white",
      backgroundColor: theme.colors.almond,
      fontWeight: "bold",
      fontSize: "22px",
      fontFamily: "'League Spartan', sans-serif"
    },
    introduction: {
      color: "black",
      fontSize: "18px",
      marginTop: "20px",
      fontFamily: "'Courgette', sans-serif"
    },
    link: {
      color: "black",
      textDecoration: "underline"
    },
    question: {
      color: theme.colors.strawberry,
      fontSize: "18px",
      fontWeight: "bold",
      marginTop: "20px",
      fontFamily: "'League Spartan', sans-serif"
    },
    answer: {
      fontSize: "16px"
    },
    legend: {
      fontFamily: "'Courgette', sans-serif"
    },
    image: {
      width: "100%"
    },
    image90: {
      width: "90%"
    },
    end: {
      marginTop: "20px"
    },
    seeYou: {
      fontFamily: "'Courgette', sans-serif",
      fontSize: "20px"
    },
    seeYouMobile: {
      fontFamily: "'Courgette', sans-serif",
      fontSize: "16px",
      marginBottom: "10px"
    },
    colorPrimaryLinkedin: {
        color: "white !important",
        backgroundColor: `${theme.colors.linkedin} !important`,
        "& hover": {
            backgroundColor: `${theme.colors.linkedin} !important`,
        },
        marginRight: "5px",
        width: "40px",
        height: "40px"
    },
    colorPrimaryFacebook: {
        color: "white !important",
        backgroundColor: `${theme.colors.facebook} !important`,
        "& hover": {
            backgroundColor: `${theme.colors.facebook} !important`,
        },
        marginRight: "5px",
        width: "40px",
        height: "40px"
    },
    colorPrimaryYoutube: {
        color: "white !important",
        backgroundColor: `${theme.colors.youtube} !important`,
        "& hover": {
            backgroundColor: `${theme.colors.youtube} !important`,
        },
        marginRight: "5px",
        width: "40px",
        height: "40px"
    },
    colorPrimaryInstagram: {
        color: "white !important",
        background: `${theme.colors.instagram} !important`,
        "& hover": {
            background: `${theme.colors.instagram} !important`,
        },
        marginRight: "5px",
        width: "40px",
        height: "40px"
        
    },
  })
);

export default function AboutUsPage() {
  let classes = useStyles();

  const { t } = useTranslation();

  const matches = useMediaQuery('(max-width:768px)');

  /*const GADetectClicMonShopResponsable = () => {
    ReactGA.event({
      category: '',
      action: ``,
      label: ``
    });
  }*/

  return (
    <div className={classes.root}>
      <div className={matches ? classes.marginMobile : classes.margin}>
        <div className={classes.center}><span className={classes.title}>{t('aboutUs.title')}</span></div>
        <div className={classes.introduction}>
          <Trans i18nKey="aboutUs.introduction" components={[<a href="http://paristech-entrepreneurs.fr/" target="_blank" rel="noopener noreferrer" className={classes.link}/>]}/>
        </div>
        <div className={classes.question}>
          <Trans i18nKey="aboutUs.questionOne" components={[<br/>]}/>
        </div>
        <div className={classes.answer}>
          <Trans i18nKey="aboutUs.answerOne" components={[<NavLink to="/vendors" className={classes.link}/>,<NavLink to="/user/rewards" className={classes.link}/>]}/>
        </div>
        <div className={classes.question}>{t('aboutUs.questionTwo')}</div>
        <div className={classes.answer}>
          <Trans i18nKey="aboutUs.answerTwo" components={[<NavLink to="/suggestBrand" className={classes.link}/>,<a href="https://blog.super-responsable.org/rapport-de-sondage-covid-19-les-tendances-de-la-consommation-responsable-en-france" target="_blank" rel="noopener noreferrer" className={classes.link}/>]}/>
        </div>
        <div className={classes.center}>
          <img className={matches ? classes.image : classes.image90} src="/assets/images/aboutUs/us.png" alt="us"/><br/>
          <span className={classes.legend}>{t('aboutUs.legend')}</span>
        </div>
        <div className={classes.question}>{t('aboutUs.questionThree')}</div>
        <div className={classes.answer}>{t('aboutUs.answerThree')}</div>
        <div className={classes.question}>{t('aboutUs.questionFour')}</div>
        <div className={classes.answer}>
          <Trans i18nKey="aboutUs.answerFour" components={[<NavLink to="/" className={classes.link}/>]}/>
        </div>
        <div className={classes.question}>{t('aboutUs.questionFive')}</div>
        <div className={classes.answer}>{t('aboutUs.answerFive')}</div>
        <Grid container direction="row" justify="center" className={classes.end}>
          <Grid item sm={3} xs={2}><img className={classes.image} src="/assets/images/aboutUs/lo.png" alt="Loïse"/></Grid>
          <Grid container item sm={6} xs={8} justify="center" className={classes.gridFollow}>
            <Grid item>
              <div className={matches ? classes.seeYouMobile : classes.seeYou}>{t('aboutUs.seeYou')}</div>
            </Grid>
            <Grid container justify="center">
              <IconButton
                component={Link} 
                href="https://www.facebook.com/Superresponsable" 
                target="_blank" rel="noopener noreferrer"
                color="primary"
                classes={{colorPrimary: classes.colorPrimaryFacebook}} >
                <FacebookIcon />
              </IconButton>
              <IconButton
                component={Link} 
                href="https://www.linkedin.com/company/super-responsable" 
                target="_blank" rel="noopener noreferrer"
                color="primary"
                classes={{colorPrimary: classes.colorPrimaryLinkedin}} >
                <LinkedInIcon />
              </IconButton>
              <IconButton
                component={Link} 
                href="https://www.instagram.com/super_responsable/" 
                target="_blank" rel="noopener noreferrer"
                color="primary"
                classes={{colorPrimary: classes.colorPrimaryInstagram}} >
                <InstagramIcon />
              </IconButton>
              <IconButton
                component={Link} 
                href="https://www.youtube.com/channel/UC4jKsBxGE6v2YgNfgWMSBNg" 
                target="_blank" rel="noopener noreferrer"
                color="primary" 
                classes={{colorPrimary: classes.colorPrimaryYoutube}}>
                <YouTubeIcon />
              </IconButton>
              </Grid>
          </Grid>
          <Grid item sm={3} xs={2}><img className={classes.image} src="/assets/images/aboutUs/alex.png" alt="Alexandra"/></Grid>
        </Grid>
      </div>
    </div>

  )
}