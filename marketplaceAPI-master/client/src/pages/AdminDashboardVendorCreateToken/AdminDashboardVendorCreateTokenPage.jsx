import React, { useState } from "react";

import Button from '@material-ui/core/Button';

import { useTranslation } from 'react-i18next';

import Select from 'react-select';

import { makeStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const useStyles = makeStyles({
  root: {
    width: "99%",
    flexGrow: 1,
    backgroundColor: "white",
  }
});

export default function AdminDashboardVendorCreateTokenPage(props) {
  const { t } = useTranslation();

  const classes = useStyles();

  const [selectedVendor, setSelectedVendor] = useState({});

  let vendorsOptions = props.vendors.map((vendor) => {    
    return {
      value: vendor.id,
      label: vendor.name
    }
  });

  const handleVendor = (vendor) => {
    setSelectedVendor(vendor);
  }


  const createToken = (toAdmin) => {
    props.createToken(selectedVendor.value, toAdmin);
  }

  return (
    <div className={classes.root}>

      <IconButton onClick={props.goBack}>
        <ArrowBackIcon />
      </IconButton>
      <h3>{t('adminDashboard.vendor.createToken')}</h3>
      <div>
        <Select 
            name="vendorId"
            options={vendorsOptions} 
            placeholder={t('adminDashboard.vendor.select')}
            value={selectedVendor}
            onChange={handleVendor}
            /*className={classes.select}*/
            styles={{ menu: base => ({ ...base, position: 'relative' }) }}
            multi={false}
          />
      </div>
      <Button
        variant="contained"
        color="primary"
        disabled={!selectedVendor}
        onClick={() => createToken(false)}
      >{t('adminDashboard.vendor.createToken')}</Button>
      <Button
        variant="contained"
        color="primary"
        disabled={!selectedVendor}
        onClick={() => createToken(true)}
      >{t('adminDashboard.vendor.createTokenToAdmin')}</Button>
    </div>
  );
}