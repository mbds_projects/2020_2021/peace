import React, { useState, useEffect } from 'react';

import AdminDashboardVendorCreateTokenPage from './AdminDashboardVendorCreateTokenPage';

import { vendorsMinimalAdminGql } from '../../graphQL/vendor'

import { createTokenVendorPasswordGql } from '../../graphQL/adminVendorDashboard'

import { toast } from 'react-toastify';

import { toastError } from '../../shared/utils/error';

import { useTranslation } from 'react-i18next';

import routerHistory from '../../shared/router-history/router-history';

export default function AdminDashboardVendorCreateTokenPageContainer() {
  const { t } = useTranslation();

  const [vendors, setVendors] = useState([]);

  const goBack = () => {
    routerHistory.push('/admin/');
  }

  useEffect(() => {
    let mounted = true;

    async function retrieveVendors() {
      let vendors = await vendorsMinimalAdminGql();
      if(mounted) setVendors(vendors)
    }

    Promise.all([retrieveVendors()])

    return () => {mounted = true;};
  }, [])

  const createToken = (id, toAdmin) => {
    createTokenVendorPasswordGql({id, toAdmin}).then(res => {
      toast.success(t('adminDashboard.toast.vendorTokenCreated'), {
        position: toast.POSITION.TOP_RIGHT
    });
    }).catch(e => {
      toastError(e.message,t);
    });
  }

  return (<AdminDashboardVendorCreateTokenPage
    goBack={goBack}
    vendors={vendors}
    createToken={createToken}
    />);
}
