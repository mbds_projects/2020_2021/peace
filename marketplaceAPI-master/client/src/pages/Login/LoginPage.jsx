import React, { useState } from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import { NavLink } from 'react-router-dom';
import Link from '@material-ui/core/Link';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import Paper from '@material-ui/core/Paper';

import { toast } from 'react-toastify';

import { useTranslation } from 'react-i18next';

import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'

//import Divider from '@material-ui/core/Divider';

import LoadingPage from '../Loading/LoadingPage';

import ReactGA from 'react-ga';

import IconButton from '@material-ui/core/IconButton';
import FacebookIcon from '@material-ui/icons/Facebook';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import Divider from '../../components/Divider/Divider'
import Grid from '@material-ui/core/Grid';


ReactGA.pageview('/login');

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      width: "100%",
      marginTop: "20px",
      marginBottom: "20px"
    },
    link: {
      textAlign: "center"
    },
    paperForm: {
      padding: "25px"
    },
    fbIcon: {
      position: "relative",
      top: "5px",
      right: "5px"
    },
    connectionButton: {
      textAlign: "center",
      margin: "20px"
    },
    divider: {
      margin: "20px"
    },
    input: {
      "& label": {
        fontSize: "14px"
      },
      "& .MuiFormLabel-root.Mui-error": {
        color: "grey"
      },
      "& fieldset": {
        borderColor: "lightgrey !important"
      }
    },
    facebook: {
      "& .MuiIconButton-label": {
        color: theme.colors.facebook,
        padding: "2px",
        borderRadius: "3px",
        "& .MuiSvgIcon-root": {
          fontSize: "50px"
        }
      }
    },
  }),
);
const regexMail = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$')

const regexPassword = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}");

export default function LoginPage(props) {
  const { t } = useTranslation();
  let classes = useStyles();

  const [mail, setMail] = useState('');
  const [password, setPassword] = useState('');

  const redirectUri = window.location.href;

  const responseFacebook = (response) => {
    props.facebookClick(response.userID, response.accessToken, response.email, response.first_name, response.last_name)
  }

  const handleLoginAttempt = (e) => {
    e.preventDefault();

    if (mail.trim() && password) {
      props.onLoginAttempt(mail.trim(), password);
      ReactGA.event({
        category: 'Login',
        action: 'Internal login',
        label: `Login`
      });
    } else {
      toast.info(t('login.toast.mailAndPasswordRequired'), {
        position: toast.POSITION.TOP_RIGHT
      });
    }

  }

  const handleMailChange = (event) => {
    setMail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const goBack = () => {
    setMail('');
    props.goBack();
  }

  return (
    <Grid container style={{ height: "100%" }} justify="center" alignItems="center">
      <Grid item xs={12} sm={props.inDialog ? 12 : 8} >

        <div className={classes.root}>
          <div style={{ textAlign: "center" }}>
            <h3>{props.isRegistered ? t('login.title') : t('login.enterMail.title') }</h3>
            <p>{props.isRegistered ? (props.isFacebookUser ?  t('login.facebook.title') : t('login.subTitle')) :  t('login.enterMail.subTitle')}</p>
          </div>

          {
            props.loadingState && <Paper className={classes.paperForm}><LoadingPage /></Paper>
          }
          {
            !props.loadingState &&
            <Paper className={classes.paperForm}>

              {
                props.isRegistered &&
                <IconButton onClick={goBack}>
                  <ArrowBackIcon />
                </IconButton>
              }

              <TextField
                id="mail"
                label={t('form.mail.label')}
                value={mail}
                onChange={handleMailChange}
                fullWidth
                required
                disabled={props.isRegistered}
                margin="normal"
                className={classes.input}
                helperText={!!mail.trim() && !regexMail.test(mail.trim()) ? t('form.mail.helper') : ''}
                error={!regexMail.test(mail.trim())}
                variant="outlined"
              />

              {!props.isRegistered &&
                <div style={{ textAlign: "center" }}>
                  <Button disabled={!mail || (!!mail.trim() && !regexMail.test(mail.trim()))}
                    variant="contained"
                    onClick={() => props.loginClick(mail)}>
                    {t('login.connectionOrCreate')}
                  </Button>
                </div>
              }

              {props.isRegistered && !props.isFacebookUser &&
                <span>
                  <TextField
                    id="password"
                    label={t('form.password.label')}
                    value={password}
                    onChange={handlePasswordChange}
                    fullWidth
                    required
                    margin="normal"

                    className={classes.input}
                    type="password"
                    helperText={!!password && !regexPassword.test(password) ? t('form.password.helper') : ''}
                    error={!regexPassword.test(password)}
                    variant="outlined"
                  />
                  <div className={classes.link}><NavLink to="/forgotPassword">{t('login.forgotPasswordLink')}</NavLink></div>
                  <br /><br />

                  <div style={{ textAlign: "center" }}>
                    <Button disabled={
                      !password
                      || (!!password && !regexPassword.test(password))
                      || !mail
                      || (!!mail.trim() && !regexMail.test(mail.trim()))
                    }
                      variant="contained"
                      onClick={handleLoginAttempt}>
                      {t('login.button')}
                    </Button>

                  </div>
                  {
                    props.isVendor &&
                    <div style={{ textAlign: "center" }}>
                      <br /><br />
                      <Link component="button" onClick={() => props.goToVendorLogin(mail)}>
                        {t('link.loginVendorLink')}
                      </Link>
                    </div>
                  }
                </span>

              }

              {
                props.isRegistered && props.isFacebookUser &&
                <div className={classes.connectionButton}>
                  <p style={{ marginBottom: "0px" }}>{t('login.facebook.subtitle')}</p>
                  <FacebookLogin
                    appId="2778365892254138"
                    fields="first_name,last_name,email,picture"
                    callback={responseFacebook}
                    redirectUri={redirectUri}
                    isMobile={false}
                    disableMobileRedirect={true}
                    render={renderProps => (
                      <IconButton
                        component={Button}
                        target="_blank" rel="noopener noreferrer"
                        color="primary"
                        classes={{ colorPrimary: classes.facebook }}
                        onClick={renderProps.onClick}>
                        <FacebookIcon />
                      </IconButton>
                    )}
                  />
                </div>
              }





              {!props.isRegistered &&
                <span>
                  <br /><br />
                  <Divider variant="middle" text={t('or').toUpperCase()} className={classes.divider} />

                  <div className={classes.connectionButton}>
                    <p style={{ marginBottom: "0px" }}>{t('login.fbButton')}</p>
                    <FacebookLogin
                      appId="2778365892254138"
                      fields="first_name,last_name,email,picture"
                      callback={responseFacebook}
                      redirectUri={redirectUri}
                      isMobile={false}
                      disableMobileRedirect={true}
                      render={renderProps => (
                        <IconButton
                          component={Button}
                          target="_blank" rel="noopener noreferrer"
                          color="primary"
                          classes={{ colorPrimary: classes.facebook }}
                          onClick={renderProps.onClick}>
                          <FacebookIcon />
                        </IconButton>
                      )}
                    />
                  </div>
                </span>
              }
            </Paper>
          }

        </div>
      </Grid>
    </Grid>

  );
}