import React, { useEffect, useState } from 'react';

import { toast } from "react-toastify";

import { useTranslation } from 'react-i18next';

import LoginPage from './LoginPage';
import { login } from '../../core/modules/authentication.module';
import { authenticateGql, facebookAuthenticateGql, isFacebookAccountRegisteredGql, isAccountRegisteredGql } from '../../graphQL/user'


import { browserType } from '../../browserType'

import { login as loginStore } from '../../shared/store/actions/authentication.actions';

import routerHistory from '../../shared/router-history/router-history';

import { useDispatch } from "react-redux";

import { Trans } from 'react-i18next';
import ReactGA from 'react-ga';



export default function LoginPageContainer(props) {
  const [loadingState, setLoadingState] = useState(false);
  const [isRegistered, setIsRegistered] = useState(false);
  const [isVendor, setIsVendor] = useState(false);
  const [isFacebookUser, setIsFacebookUser] = useState(false);


  const Msg = () => (
      <Trans i18nKey="login.toast.messageFirefoxSupport"
            values={{ linkTextFirefoxSupport: t('login.toast.linkTextFirefoxSupport')}}
            components={[<a style={{color: "white", fontWeight: "bold", textDecoration: "underline"}} target="_blank" rel="noopener noreferrer" href={t('login.toast.linkFirefoxSupport')}/>]}/>
  )

  useEffect(() => {
    let browser = browserType();
    if(browser === "Firefox") {
      toast.info(<Msg />, 
        {autoClose: false});
    }

  }, [])

  const dispatch = useDispatch();
  const { t } = useTranslation();

  const goToSignin = () => routerHistory.push('/signin')

  const goToVendorLogin = (mail) => {
    routerHistory.push({pathname: '/loginVendor', detail: {mail}})
  }

  const onLoginAttempt = async(mail, password) => {
    setLoadingState(true);
    try {
      const loginAttemptUser = await authenticateGql({mail: mail, password: password});
      if (loginAttemptUser) {
          let user = await login(loginAttemptUser.token);
          ReactGA.event({
            category: 'Login',
            action: 'Internal login',
            label: `Login`
          });
          dispatch(loginStore(user));
          if(props.inDialog) {
            props.close();
          } else {
            routerHistory.goBack();
          }
      } else {
        toast.error(t('login.toast.invalidLogin'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    catch(e){
      if(e.message.includes("invalidCredential")) {
        toast.error(t('login.toast.invalidLogin'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else if(e.message.includes("user.blocked")) {
        toast.error(t('login.toast.blockedUser'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else if(e.message.includes("user.facebookAccount")) { 
        toast.error(t('login.toast.facebookAccount'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else if(e.message.includes("user.otherFacebookAccount")) { 
        toast.error(t('login.toast.otherFacebookAccount'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else if(e.message.includes("user.internalAccount")) { 
        toast.error(t('login.toast.internalAccount'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else if(e.message.includes("user.notExist")) { 
        toast.error(t('login.toast.notExist'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('login.toast.somethingWrongAppends'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }

    }
    setLoadingState(false);
  }

  const onFacebookLoginAttempt = async(userID, accessToken) => {
    setLoadingState(true);
    try {
      const loginAttemptUser = await facebookAuthenticateGql({userID, accessToken});
      if (loginAttemptUser) {
          let user = await login(loginAttemptUser.token);
          dispatch(loginStore(user));
          ReactGA.event({
            category: 'Login',
            action: 'Facebook login',
            label: `Login`
          });
          if(props.inDialog) {
            props.close();
          } else {
            routerHistory.goBack();
          }
      } else {
        toast.error(t('login.toast.invalidLogin'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    catch(e){
      if(e.message.includes("invalidCredential")) {
        toast.error(t('login.toast.invalidLogin'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else if(e.message.includes("user.blocked")) {
        toast.error(t('login.toast.blockedUser'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }else if(e.message.includes("user.notExist")) { 
        toast.error(t('login.toast.notExist'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }else {
        toast.error(t('login.toast.somethingWrongAppends'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }

    }
    setLoadingState(false);
  }

  const facebookClick = async (userID, accessToken, mail, name, surname) => {
    let isFacebook = await isFacebookAccountRegisteredGql({userID});
    if(isFacebook) {
      await onFacebookLoginAttempt(userID, accessToken);
    } else {
      routerHistory.push({
        pathname: '/signin',
        detail: { userID, accessToken, mail, name, surname }
      });
    }
  }

  const loginClick = async (mail) => {
    let checkIsRegistered = await isAccountRegisteredGql({mail});
    const { isUser, isFacebookUser, isVendor} = checkIsRegistered
    if(isUser) {
      setIsRegistered(true);
      setIsFacebookUser(isFacebookUser);
      if(isVendor) {
        setIsVendor(true);
      }
    } else if(isVendor) {
      goToVendorLogin(mail)
    } else {
      routerHistory.push({
        pathname: '/signin',
        detail: { mail }
    });
    }
  }

  const goBack = () => {
    setIsRegistered(false);
    setIsVendor(false);
    setIsFacebookUser(false);
  }

  const propsChild = {
    goToSignin, loadingState, onLoginAttempt, facebookClick, loginClick, 
    isRegistered, goToVendorLogin, isVendor, isFacebookUser, goBack
  }
  

  return (<LoginPage 
    inDialog={props.inDialog}
    {...propsChild} />);
  
}