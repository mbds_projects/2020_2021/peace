import * as React from 'react';

import { makeStyles, createStyles } from '@material-ui/core/styles';

import { useTranslation } from 'react-i18next';

import { Trans } from 'react-i18next';

import ReactGA from 'react-ga';

ReactGA.pageview('/cgu');

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      margin: "auto", 
      padding: "20px"
    },
    head: {
      textAlign: 'center'
    }
  })
);

export default function CGUPage() {
  let classes = useStyles();

  const { t } = useTranslation();

  return (
    <div className={classes.root}>
      <h2 className={classes.head}>{t('cgu.title')}</h2>
      <p className={classes.head}>{t('cgu.date')}</p>
      <p><Trans i18nKey="cgu.def"
            components={[<b></b>, <br/>]}/></p>

      <h3>{t('cgu.legalNotice.title')}</h3>

      <p><Trans i18nKey="cgu.legalNotice.def"
            components={[<br/>]}/></p>

      <h3>{t('cgu.webSiteAccess.title')}</h3>
      <p><Trans i18nKey="cgu.webSiteAccess.def"
            components={[<br/>]}/></p>

      <h3>{t('cgu.rgpd.title')}</h3>
      <p><Trans i18nKey="cgu.rgpd.def"
            components={[<br/>]}/></p>

      <h3>{t('cgu.credit.title')}</h3>
      <p><Trans i18nKey="cgu.credit.def"
            components={[<br/>]}/></p>

      <h3>{t('cgu.responsability.title')}</h3>
      <p><Trans i18nKey="cgu.responsability.def"
            components={[<br/>]}/></p>

      <h3>{t('cgu.link.title')}</h3>
      <p><Trans i18nKey="cgu.link.def"
            components={[<br/>]}/></p>

      <h3>{t('cgu.cookies.title')}</h3>
      <p><Trans i18nKey="cgu.cookies.def"
            components={[<br/>]}/></p>

      <h3>{t('cgu.publishing.title')}</h3>
      <p><Trans i18nKey="cgu.publishing.def"
            components={[<br/>]}/></p>

      <h3>{t('cgu.law.title')}</h3>
      <p><Trans i18nKey="cgu.law.def"
            components={[<br/>]}/></p>
    </div>

  )
}