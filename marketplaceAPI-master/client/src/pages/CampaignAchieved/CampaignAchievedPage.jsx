import * as React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
//import CircularProgress from '@material-ui/core/CircularProgress';
import Link from '@material-ui/core/Link';

import PersonIcon from '@material-ui/icons/Person';
import AddLocationIcon from '@material-ui/icons/AddLocation';
import MyLocationIcon from '@material-ui/icons/MyLocation';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { useTranslation } from 'react-i18next';


import IconButton from '@material-ui/core/IconButton';
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import YouTubeIcon from '@material-ui/icons/YouTube';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import routerHistory from '../../shared/router-history/router-history';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      width: "100%",
      marginTop: "20px",
      marginBottom: "20px"
    },
    paperForm: {
      padding: "25px",
      "& p": {
        fontSize: "18px"
      }
    },
    colorPrimaryLinkedin: {
        color: "white !important",
        backgroundColor: `${theme.colors.linkedin} !important`,
        "& hover": {
            backgroundColor: `${theme.colors.linkedin} !important`,
        },
        marginRight: "5px"
    },
    colorPrimaryFacebook: {
        color: "white !important",
        backgroundColor: `${theme.colors.facebook} !important`,
        "& hover": {
            backgroundColor: `${theme.colors.facebook} !important`,
        },
        marginRight: "5px"
    },
    colorPrimaryYoutube: {
        color: "white !important",
        backgroundColor: `${theme.colors.youtube} !important`,
        "& hover": {
            backgroundColor: `${theme.colors.youtube} !important`,
        },
        marginRight: "5px"
    },
    colorPrimaryInstagram: {
        color: "white !important",
        background: `${theme.colors.instagram} !important`,
        "& hover": {
            background: `${theme.colors.instagram} !important`,
        },
        marginRight: "5px"
    },
    networks: {
        marginTop: "20px",
        marginBottom: "30px"
    },
    alignCenter: {
      textAlign: "center"
    },
    list: {
      marginLeft: "30px"
    }
  }),
);


export default function CampaignAchievedPage(props) {
  const classes = useStyles();
  const { t } = useTranslation();

  const handleClick = (to) => {
    routerHistory.push(to);
  }

  return (  
      <div className={classes.root}>
        <Paper className={classes.paperForm}>
          <h3>{t('campaignAchieved.title')}</h3>
          <br />
          <p>{t('campaignAchieved.subTitle')}</p>
          <List component="nav" className={classes.list}>
            <ListItem button onClick={() => {handleClick("/signin")}} /*component={NavLink}  to="/signin"*/>
              <ListItemIcon>
                <PersonIcon />
              </ListItemIcon>
              <ListItemText primary={t('campaignAchieved.signin')} />
            </ListItem>
            <ListItem button onClick={() => {handleClick("/suggestBrand")}}  /*component={NavLink} to="/suggestBrand"*/>
              <ListItemIcon>
                <AddLocationIcon />
              </ListItemIcon>
              <ListItemText primary={t('campaignAchieved.link')} />
            </ListItem>
            <ListItem button  onClick={() => {handleClick("/vendors")}} /*component={NavLink} to="/vendors"*/>
              <ListItemIcon>
                <MyLocationIcon />
              </ListItemIcon>
              <ListItemText primary={t('campaignAchieved.discover')} />
            </ListItem>
          </List>
          <br /><br />
          <p className={classes.alignCenter}>{t('campaignAchieved.follow')}</p>
          <Grid container justify="center" className={classes.networks}>
            <IconButton
                aria-label="facebook"
                component={Link} 
                href="https://www.facebook.com/Superresponsable" 
                target="_blank" rel="noopener noreferrer"
                color="primary"
                classes={{colorPrimary: classes.colorPrimaryFacebook}} >
                <FacebookIcon />
            </IconButton>

            <IconButton
                aria-label="linkedin"
                component={Link} 
                href="https://www.linkedin.com/company/super-responsable" 
                target="_blank" rel="noopener noreferrer"
                color="primary"
                classes={{colorPrimary: classes.colorPrimaryLinkedin}} >
                <LinkedInIcon />
            </IconButton>

            <IconButton
                aria-label="instagram"
                component={Link} 
                href="https://www.instagram.com/super_responsable/" 
                target="_blank" rel="noopener noreferrer"
                color="primary"
                classes={{colorPrimary: classes.colorPrimaryInstagram}} >
                <InstagramIcon />
            </IconButton>


            <IconButton
                aria-label="youtube"
                component={Link} 
                href="https://www.youtube.com/channel/UC4jKsBxGE6v2YgNfgWMSBNg" 
                target="_blank" rel="noopener noreferrer"
                color="primary" 
                classes={{colorPrimary: classes.colorPrimaryYoutube}}>
                <YouTubeIcon />
            </IconButton>
            </Grid>
        </Paper>
      </div>
  );
}
