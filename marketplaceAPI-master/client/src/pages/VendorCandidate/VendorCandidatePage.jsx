import React, { useState } from 'react';
import { createStyles /*, withStyles*/, makeStyles } from '@material-ui/core/styles';
import SubscribeDashboardPageContainer from '../../pages/SubscribeDashboard/SubscribeDashboardPageContainer'
import Carousel from "../../components/Carousel/Carousel";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';
import { NavLink } from 'react-router-dom';
import { Trans } from 'react-i18next';

import { useTranslation } from 'react-i18next';
import ReactGA from 'react-ga';

ReactGA.pageview('/vendorCandidate');

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            padding: "15px",
            width: "99%",
            margin: "0 auto",
            backgroundColor: "white"
        },
        logo: {
            textAlign: "center",
            "& img": {
                width: "15%"
            }
        },
        slogan: {
            color: theme.colors.almond,
            textTransform: "uppercase",
            textAlign: "center",
            fontSize: "25px",
            fontWeight: "bold"
        },
        banner: {
            textAlign: "center",
            "& img": {
                width: "100%"
            }
        },
        bannerTop: {
            marginBottom: "20px",
            textAlign: "center",
            "& img": {
                width: "100%"
            }
        },
        img: {
            width: "100%"
        },
        titleSpartan: {
            fontFamily: "'League Spartan', sans-serif",
            fontSize: "25px",
            marginBottom: "15px"
        },
        text: {
            fontFamily: "'Open Sans', sans-serif",
            fontSize: "20px"
        },
        list: {
            fontFamily: "'Open Sans', sans-serif",
            fontSize: "20px",
            "& > li": {
                marginBottom: "15px"
            }
        },
        comingSoon: {
            color: "grey",
            fontStyle: "italic"
        },
        questions: {
            fontFamily: "'Open Sans', sans-serif",
            fontSize: "20px",
            fontWeight: "bold",
            textAlign: "center"
        },
        link: {
            color: "black",
            textDecoration: "underline",
        }, 
        number: {
            fontFamily: "'Open Sans', sans-serif",
            fontWeight: "bold",
            color: theme.colors.strawberry,
            fontSize: "20px"
        },
        numberText: {
            fontFamily: "'Open Sans', sans-serif",
            fontSize: "20px",
            paddingLeft: "10px"
        },
        note: {
            fontSize: "14px"
        }
    }),
);

export default function VendorCandidatePage(props) {
    const classes = useStyles();
    const { t, i18n } = useTranslation();
    //const matches = useMediaQuery('(max-width:768px)');
    
    let carouselTestimonies = props.vendorTestimonies ? 
        props.vendorTestimonies.map((el) => 
            <img alt={`Carousel ${el.vendor.id}`} src={el.testimonyLink.lang.find((l) => l.locale === i18n.language).value}/>
        ) : []

    return (
        <div className={classes.root}>
            <Grid container spacing={3} justify="center">
                <Grid item xs={12} className={classes.logo}>
                    <img alt="Super Responsable logo" src="assets/images/icons/sr-logo-v2.png" />
                </Grid>
                <Grid item xs={12} className={classes.slogan}>
                    {t('vendorCandidate.slogan')}
                </Grid>
                <Grid item xs={12} className={classes.bannerTop}>
                    <img alt="Business Banner" src="assets/images/business/bannerBusiness.png" />
                </Grid>
                <Grid item xs={12} sm={10}>
                    <div className={classes.titleSpartan}>{t('vendorCandidate.catchPhrase.title')}</div>
                    <p className={classes.text}>{t('vendorCandidate.catchPhrase.text')}</p>
                </Grid>
                <Grid container item xs={12} sm={10} spacing={3} style={{marginTop: "20px"}} >
                    <Grid item sm={6} xs={12}>
                        <div className={classes.titleSpartan}>{t('vendorCandidate.reveal.title')}</div>
                        <ul className={classes.list}>
                            <Trans i18nKey="vendorCandidate.reveal.text"
                            components={[<li></li>]} />
                        </ul>
                    </Grid>
                    <Grid item sm={5} xs={12}>
                        <img alt="Reveal" className={classes.img} src="assets/images/business/reveal.png" />
                    </Grid>
                </Grid>
                <Grid container item xs={12} sm={10} spacing={3} style={{marginTop: "20px"}} >
                    <Grid item sm={6} xs={12}>
                        <div className={classes.titleSpartan}>{t('vendorCandidate.analyse.title')}</div>
                        <ul className={classes.list}><Trans i18nKey="vendorCandidate.analyse.text"
                                components={[<li><span className={classes.comingSoon}></span></li>]} /></ul>
                    </Grid>
                    <Grid item sm={5} xs={12}>
                        <img alt="Analyse" className={classes.img} src="assets/images/business/analyse.png" />
                    </Grid>
                </Grid>
                <Grid container item xs={12} sm={10} spacing={3} style={{marginTop: "20px"}} >
                    <Grid item sm={6} xs={12}>
                        <div className={classes.titleSpartan}>{t('vendorCandidate.join.title')}</div>
                        <ul className={classes.list}><Trans i18nKey="vendorCandidate.join.text"
                                components={[<li><span className={classes.comingSoon}></span></li>]} /></ul>
                    </Grid>
                    <Grid item sm={5} xs={12}>
                        <img alt="Join" className={classes.img} src="assets/images/business/join.png" />
                    </Grid>
                </Grid>
                <Grid item xs={12} sm={10} style={{marginTop: "20px"}}>
                    <div className={classes.titleSpartan}>{t('vendorCandidate.integrate.title')}</div>
                    <p className={classes.text}>{t('vendorCandidate.integrate.text')}</p>
                </Grid>
                <Grid item xs={12} sm={10}>
                    <SubscribeDashboardPageContainer />
                </Grid>
                <Grid item xs={12} sm={10}>
                    <p className={classes.questions}>
                        <Trans i18nKey="vendorCandidate.questions"
                        components={[ <NavLink to="/contactUs?q=contact" className={classes.link}></NavLink>]} />
                    </p>
                </Grid>
                {
                    carouselTestimonies && carouselTestimonies.length > 0 &&

                    <Grid item xs={12}>
                        <Carousel carouselComponent={carouselTestimonies} arrowsColor="grey" arrows={true} />
                    </Grid>
                }

                <Grid container item xs={12} sm={10}>
                    <Grid item xs={12} sm={7}>
                        <div className={classes.titleSpartan}>{t('vendorCandidate.aboutUs.title')}</div>
                        <p className={classes.text}>
                            <Trans i18nKey="vendorCandidate.aboutUs.text"
                            components={[ <NavLink to="/aboutUs" className={classes.link}></NavLink>]} />
                        </p>
                    </Grid>
                    <Grid container item xs={12} sm={5}>
                        <Grid item xs={4} style={{textAlign: "right"}}> <span className={classes.number}>{t('vendorCandidate.numbers.reboundRate.value')}</span></Grid>
                        <Grid item xs={8}><span className={classes.numberText} >{t('vendorCandidate.numbers.reboundRate.title')}</span></Grid>
                        <Grid item xs={4} style={{textAlign: "right"}}> <span className={classes.number}>{t('vendorCandidate.numbers.recommendation.value')}</span></Grid>
                        <Grid item xs={8}><span className={classes.numberText} >{t('vendorCandidate.numbers.recommendation.title')}</span></Grid>
                        <Grid item xs={4} style={{textAlign: "right"}}> <span className={classes.number}>{t('vendorCandidate.numbers.county.value')}</span></Grid>
                        <Grid item xs={8}><span className={classes.numberText} >{t('vendorCandidate.numbers.county.title')}</span></Grid>
                        <Grid item xs={4} style={{textAlign: "right"}}> <span className={classes.number}>{t('vendorCandidate.numbers.values.value')}</span></Grid>
                        <Grid item xs={8}><span className={classes.numberText} >{t('vendorCandidate.numbers.values.title')}</span></Grid>
                        <Grid item xs={4} style={{textAlign: "right"}}> <span className={classes.number}>{t('vendorCandidate.numbers.followers.value')}</span></Grid>
                        <Grid item xs={8}><span className={classes.numberText} >{t('vendorCandidate.numbers.followers.title')}</span></Grid>
                        <Grid item xs={4} style={{textAlign: "right"}}> <span className={classes.number}>{t('vendorCandidate.numbers.youtube.value')}</span></Grid>
                        <Grid item xs={8}><span className={classes.numberText} >{t('vendorCandidate.numbers.youtube.title')}</span></Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <div className={classes.note}>{t('vendorCandidate.numbers.note')}</div>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}