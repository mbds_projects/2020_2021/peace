import React, { useState, useEffect } from 'react';

import VendorCandidatePage from './VendorCandidatePage';

import { vendorTestimoniesGql} from '../../graphQL/vendorTestimony'


export default function VendorCandidatePageContainer() {
  const [vendorTestimonies, setVendorTestimonies] = useState([]);
  useEffect(() => {
    async function retrieveVendorTestimonies() {
      let vendorTestimoniesTmp = await vendorTestimoniesGql();
      setVendorTestimonies(vendorTestimoniesTmp);
    }
    retrieveVendorTestimonies();
  });

  return (<VendorCandidatePage 
    vendorTestimonies={vendorTestimonies}
    />);
}