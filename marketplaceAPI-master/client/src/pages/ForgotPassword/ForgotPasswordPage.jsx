import React, { useState } from 'react';

import { NavLink } from 'react-router-dom';
import { makeStyles, createStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import { useTranslation } from 'react-i18next';

import ReactGA from 'react-ga';

ReactGA.pageview('/forgotPassword');

const regexMail = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$')

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      padding: "15px"
    }
  }),
);

export default function ForgotPasswordPage(props) {

  const [mail, setMail] = useState('');
  const classes = useStyles();
  const { t } = useTranslation();

  const handleMailAttempt = () => {
    if(regexMail.test(mail)) {
      props.onMailAttempt(mail);
    }
  }

  const handleChange = (event) => {
    setMail(event.target.value);
  };

  return (
    <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justify="center"
      >
        <Grid item xs={10}>
            <h2>{t('forgotPassword.title')}</h2>
          <Paper className={classes.root}>
            <TextField
                    id="mail"
                    label={t('form.mail.label')}
                    value={mail}
                    onChange={handleChange}
                    fullWidth
                    required
                    margin="normal"
                    
                    helperText={mail !== '' && !regexMail.test(mail) ? t('form.mail.helper'): ''}
                    error={!regexMail.test(mail)}
                    variant="outlined"
                  />
                  <Button disabled={
                    mail==='' || (mail!=='' && !regexMail.test(mail))
                  } 
                    variant="contained" 
                    onClick={handleMailAttempt}
                    id="forgotPwd-btn"
                    fullWidth>
                      {t('forgotPassword.button')}
                  </Button>
              <div className="link"><NavLink to="/login">{t('link.loginLink')}</NavLink></div>
          </Paper>
        </Grid>  

    </Grid>
   
  );
}
