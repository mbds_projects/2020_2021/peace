import * as React from 'react';

import { toast } from 'react-toastify';
import { forgotPasswordGql } from '../../graphQL/user'
import ForgotPasswordPage from './ForgotPasswordPage';

import { useTranslation } from 'react-i18next';

export default function ForgotPasswordPageContainer() {
  const { t } = useTranslation();
  
  const performMailAttempt =  async (mail) => {
    try {
      const passwordAttemptUser = await forgotPasswordGql({mail: mail});
      if (passwordAttemptUser) {
          toast.success(t('forgotPassword.toast.mailSent'), {
            position: toast.POSITION.TOP_RIGHT
          });
      } else {
        toast.error(t('error.somethingWrongAppend'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }
    }
    catch(e){
      if(e.message.includes("mail.notFound")) {
        toast.error(t('forgotPassword.toast.mailUnknown'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else if(e.message.includes("password.facebook")) {
        toast.error(t('forgotPassword.toast.facebookAccount'), {
          position: toast.POSITION.TOP_RIGHT
        });
      } else {
        toast.error(t('error.somethingWrongAppend'), {
          position: toast.POSITION.TOP_RIGHT
        });
      }

    }

  }

  const handleMailAttempt = (mail) => {
    performMailAttempt(mail);
  }

  return (<ForgotPasswordPage onMailAttempt={handleMailAttempt} />);

}