import * as React from 'react';
import * as ReactDOM from 'react-dom';

//import Button from '@material-ui/core/Button';

import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { decode } from 'jsonwebtoken';

import { Provider } from 'react-redux';

import store from './shared/store/store';

import AppContainer from './components/App/AppContainer';

//import { configGql } from './graphQL/config'

import { userGql } from './graphQL/user'

import { vendorLoggedGql } from './graphQL/vendorDashboard'

import { categoriesGql } from './graphQL/category'

import { keywordsGql } from './graphQL/keyword'

import { gendersGql } from './graphQL/gender'



//import { locationsByCityGql, locationsByCountyGql } from './graphQL/location'

//import { vendorsLimitedGql } from './graphQL/vendor'

import { login, toggleLoading as toggleIsLoadingAuthentication } from './shared/store/actions/authentication.actions';

import { setCategories } from './shared/store/actions/category.actions';

import { setKeywords } from './shared/store/actions/keyword.actions';

import { setGenders } from './shared/store/actions/gender.actions';

//import { setLocationsFilter } from './shared/store/actions/location.actions';

//import { setVendors } from './shared/store/actions/vendor.actions';

import i18n from './i18n';

//import { toast } from 'react-toastify';



//var pjson = require('../package.json');

//checkVersion();

/*async function checkVersion() {
  let config = await configGql();
  if(config.webAppVersionBuild !== pjson.version) {
    console.log("pjson.version: ", pjson.version);
    console.log("config.webAppVersionBuild: ", config.webAppVersionBuild)
    let text = i18n.language === "fr" ? 
      "Du nouveau contenu est disponible "
       : "New content is available ";
    let textButton = i18n.language === "fr" ? "mettre à jour" : "update";

    const Msg = () => (
      <div>{text}
        <Button style={{color: "white"}} onClick={() => document.location.reload(true)}>{textButton}</Button>
      </div>
    )
    toast.info(<Msg />, {autoClose: false});
  }
}*/





const setCurrentLoggedInUser = async() => {
  const token = localStorage.getItem('TOKEN');
  if(token) {
    let decodeToken = decode(token);
    var current_time = Date.now() / 1000;
    if(decodeToken) {
      if(decodeToken.hasOwnProperty('exp') && decodeToken['exp'] > current_time) {
        if(decodeToken.role === "vendor") {
          const vendor = await vendorLoggedGql()
          if (vendor) {
            store.dispatch(login(vendor));
          }
        } else {
          const user = await userGql();
          if (user) {
            store.dispatch(login(user));
          }
        }
      } else {
        localStorage.removeItem('TOKEN');
      }
    } else {
      localStorage.removeItem('TOKEN');
    }
    
  }
  store.dispatch(toggleIsLoadingAuthentication(false));
}

const getCategories = async () => {
  let categories = await categoriesGql();
  store.dispatch(setCategories(categories));
}

const getGenders = async () => {
  let genders = await gendersGql();
  store.dispatch(setGenders(genders));
}

const getKeywords = async() => {
  let keywords = await keywordsGql();
  store.dispatch(setKeywords(keywords));
}

/*const getLocationsFilter = async() => {
  let locationsCity = await locationsByCityGql();
  let locationsCounty = await locationsByCountyGql();
  let countiesAndCities = _.concat(locationsCity, locationsCounty)
  store.dispatch(setLocationsFilter(countiesAndCities));
}*/

/*const getLimitedVendors = async() => {
  let vendors = await vendorsLimitedGql();
  store.dispatch(setVendors(vendors));
}*/

Promise.all([/*getLimitedVendors(), */setCurrentLoggedInUser(), getCategories(), getKeywords(), getGenders()/*, getLocationsFilter()*/])


ReactDOM.render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
