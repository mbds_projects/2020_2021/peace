import * as _ from 'lodash';

const filterLocations = (arrayToFilter, searchParse, countiesAndCitiesSuggestions, rewards) => {
    if (searchParse.hasOwnProperty('city')) {
        if ((_.isArray(searchParse.city) && searchParse.city.length <= 0) || searchParse.city === "") {
            return arrayToFilter;
        }

        let cityVendors;
        if (_.isArray(searchParse.city)) {
            cityVendors = searchParse.city.map((city) => {
                return countiesAndCitiesSuggestions.find((loc) => loc.loc === city);
            });
        } else {
            cityVendors = [countiesAndCitiesSuggestions.find((loc) => loc.loc === searchParse.city)];
        }

        let vendorsArrays = [];
        cityVendors.forEach((element) => {
            if (element && element.vendors) vendorsArrays.push(element.vendors);
        });

        let results = []

        if (vendorsArrays.length > 0) {
            let vendorsIds = vendorsArrays[0];
            for (let i = 1; i < vendorsArrays.length; i++) {
                vendorsIds = _.intersection(vendorsArrays[i], vendorsIds)
            }
            vendorsIds.forEach((vendorId) => {
                let vendor = arrayToFilter.find(el => rewards ? el.id === vendorId : el.vendor.id === vendorId);
                if (vendor) results.push(vendor);
            })
        }
        return results;

    } else {
        return arrayToFilter;
    }
}

const filterCategory = (arrayToFilter, searchParse, rewards) => {
    if (searchParse.hasOwnProperty('cat')) {
        if ((_.isArray(searchParse.cat) && searchParse.cat.length <= 0) || searchParse.cat === "") {
            return arrayToFilter;
        }

        return arrayToFilter.filter((el) => {
            if (rewards ? el.categories : el.vendor.categories) {
                let catIds = rewards ? el.categories : el.vendor.categories;
                if (_.isArray(searchParse.cat)) {
                    return JSON.stringify(_.intersection(catIds, searchParse.cat)) === JSON.stringify(searchParse.cat);
                } else {

                    return catIds.includes(searchParse.cat);
                }
            } else {
                return false
            }
        });
    } else {
        return arrayToFilter;
    }
}

const filterKeywords = (arrayToFilter, searchParse, rewards) => {
    if (searchParse.hasOwnProperty('kw')) {

        if ((_.isArray(searchParse.kw) && searchParse.kw.length <= 0) || searchParse.kw === "") {
            return arrayToFilter;
        }

        return arrayToFilter.filter((el) => {
            if ((rewards && el.keywords) || el.vendor.keywords) {
                let kwIds = rewards ? el.keywords : el.vendor.keywords;
                if (_.isArray(searchParse.kw)) {
                    return JSON.stringify(_.intersection(kwIds, searchParse.kw)) === JSON.stringify(searchParse.kw);
                } else {
                    return kwIds.includes(searchParse.kw);
                }
            }
            return false;
        });
    } else {
        return arrayToFilter;
    }
}

const filterGenders = (arrayToFilter, searchParse, rewards) => {
    if (searchParse.hasOwnProperty('gd')) {

        if ((_.isArray(searchParse.gd) && searchParse.gd.length <= 0) || searchParse.gd === "") {
            return arrayToFilter;
        }

        return arrayToFilter.filter((el) => {
            if ((rewards && el.genders) || el.vendor.genders) {
                let gdIds = rewards ? el.genders : el.vendor.genders;
                if (_.isArray(searchParse.gd)) {
                    return JSON.stringify(_.intersection(gdIds, searchParse.gd)) === JSON.stringify(searchParse.gd);
                } else {
                    return gdIds.includes(searchParse.gd);
                }
            } else {
                return false
            }

        });
    } else {
        return arrayToFilter;
    }
}

const filterNames = (arrayToFilter, searchParse) => {
    if (searchParse.hasOwnProperty('searchName') && searchParse.searchName !== '') {
        return arrayToFilter.filter((el) => el.name.trim().toLowerCase().includes(searchParse.searchName.trim().toLowerCase()));
    } else {
        return arrayToFilter;
    }
}

export function filter(array, searchFilters, countiesAndCitiesSuggestions, options, rewards) {
    let arrayTmp = array;
    if (options.cityChecked || (searchFilters.city && searchFilters.city.length > 0)) {
        (options.cityChecked) ?
            arrayTmp = array.filter(el => options.aroundVendorsIds.includes(rewards ? el.id : el.vendor.id))
            : arrayTmp = filterLocations(array, searchFilters, countiesAndCitiesSuggestions, rewards);
    }
    if (options.onlineVendors) {
        arrayTmp = arrayTmp.filter(el => rewards ? el.onlineShop === true : el.vendor.onlineShop);
    }
    if (options.clickAndCollectVendors) {
        arrayTmp = arrayTmp.filter(el => rewards ? el.clickAndCollect === true : el.vendor.clickAndCollect === true);
    }
    if (options.rewardChecked) {
        const rewardVendors = _.uniq(rewards.map(reward => reward.vendor.id));
        arrayTmp = arrayTmp.filter(el => rewardVendors.includes(el.id))
    }
    if (searchFilters.cat && searchFilters.cat.length > 0) arrayTmp = filterCategory(arrayTmp, searchFilters, rewards);
    if (searchFilters.kw && searchFilters.kw.length > 0) arrayTmp = filterKeywords(arrayTmp, searchFilters, rewards);
    if (searchFilters.gd && searchFilters.gd.length > 0) arrayTmp = filterGenders(arrayTmp, searchFilters, rewards);
    if (searchFilters.searchName && searchFilters.searchName !== '') arrayTmp = filterNames(arrayTmp, searchFilters, rewards);

    return arrayTmp;
}