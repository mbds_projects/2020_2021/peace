import { toast } from 'react-toastify';

/*import i18n from '../../i18n';*/

import routerHistory from '../router-history/router-history';

import store from '../store/store';
import { logout as logoutFromStore } from '../store/actions/authentication.actions';

export function authError(message) {
  if(message.includes("user.notLogged")) {
    routerHistory.push('/login');
  } else if(message.includes('CheckTokenAuthError')) {
    localStorage.removeItem('TOKEN');
    store.dispatch(logoutFromStore())
    routerHistory.push('/login');
    //todo toast you've been logout
  }
}

export function toastError(message, t, defaultTarget) {
  if(message.includes("user.notLogged")) {
    toast.error(t('error.shouldBeConnected'), {
      position: toast.POSITION.TOP_RIGHT
    });
  } else if(message.includes("CheckTokenAuthError")) {
    toast.error(t('error.connectionInvalid'), {
      position: toast.POSITION.TOP_RIGHT
    });
  } else if(message.includes('password.current.wrong')) {
    toast.error(t('profile.toast.wrongCurrentPassword'), {
      position: toast.POSITION.TOP_RIGHT
    });
  } else if (message.includes('token.vendorPaidAtExpires')) {
    toast.error(t('vendorDashboard.toast.paidAtExpires'), {
      position: toast.POSITION.TOP_RIGHT
    });
  } else if (message.includes('vendor.notAllowedToPublish')) {
    let indexStart = message.indexOf('vendor.notAllowedToPublish');
    let messageTranslate = message.substring(indexStart)
    toast.error(t(messageTranslate), {
      position: toast.POSITION.TOP_RIGHT
    });
  } else {
    toast.error(t(defaultTarget || 'error.somethingWrongAppends'), {
      position: toast.POSITION.TOP_RIGHT
    });
  }
}