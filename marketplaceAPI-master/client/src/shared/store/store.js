import { combineReducers, createStore } from 'redux';

import authenticationReducer from './reducers/authentication.reducer';
import userReducer from './reducers/user.reducer';
import vendorReducer from './reducers/vendor.reducer';
import categoryReducer from './reducers/category.reducer';
import keywordReducer from './reducers/keyword.reducer';
import locationReducer from './reducers/location.reducer';
import sidebarReducer from './reducers/sidebar.reducer';
import articleReducer from './reducers/article.reducer';
import genderReducer from './reducers/gender.reducer';
import instagramReducer from './reducers/instagram.reducer';
import imageCarouselReducer from './reducers/imageCarousel.reducer';
import youtubeReducer from './reducers/youtube.reducer';
import rewardReducer from './reducers/reward.reducer';

const allReducers = combineReducers({
    authentication: authenticationReducer,
    user: userReducer,
    vendor: vendorReducer,
    category: categoryReducer,
    keyword: keywordReducer,
    location: locationReducer,
    sidebar: sidebarReducer,
    article: articleReducer,
    gender: genderReducer,
    instagram: instagramReducer,
    imageCarousel: imageCarouselReducer,
    youtube: youtubeReducer,
    reward: rewardReducer
});

function configureStore(initialState) {
    return createStore(allReducers, initialState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
}

export default configureStore({
    authentication: { currentUser: null, isLoading: true },
    user: {position: null, geolocAccepted: false},
    vendor: { vendors: [], latestVendors: [], aroundPositionVendors: [], 
        bestRateVendors: [], aroundPositionVendorsIdsByKms:[], profileVendors: [], 
        profileVendorsAssimilate: []},
    category: {categories: []},
    keyword: { keywords: []},
    location: { locationsFilter: []},
    sidebar: {open: false},
    article: {articles: []},
    gender: {genders: []},
    instagram: {instagramRecent: []},
    youtube: {youtubeRecent: []},
    imageCarousel: {imagesCarousel: []},
    reward: { rewards: []}
});