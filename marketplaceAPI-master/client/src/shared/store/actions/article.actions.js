
export const SET_ARTICLES = 'setArticles';

export function setArticles(articles) {
    return {
        type: SET_ARTICLES,
        payload: {
            articles: articles
        }
    };
}