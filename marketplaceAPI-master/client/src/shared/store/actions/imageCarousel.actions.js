export const SET_IMAGES_CAROUSEL = 'setImagesCarousel';

export function setImagesCarousel(imagesCarousel) {
    return {
        type: SET_IMAGES_CAROUSEL,
        payload: {
            imagesCarousel: imagesCarousel
        }
    };
}