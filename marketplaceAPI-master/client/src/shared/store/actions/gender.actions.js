export const SET_GENDERS = 'setGenders';

export function setGenders(genders) {
    return {
        type: SET_GENDERS,
        payload: {
            genders: genders
        }
    };
}