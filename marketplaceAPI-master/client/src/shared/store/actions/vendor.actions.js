export const SET_VENDORS = 'setVendors';
export const SET_LATEST_VENDORS = 'setLatestVendors';
export const SET_AROUND_POSITION_VENDORS = 'setAroundPositionVendors';
export const SET_AROUND_POSITION_VENDORS_IDS_BY_KMS = 'setAroundPositionVendorsIdsByKms';
export const SET_BEST_RATE_VENDORS = 'setBestRateVendors'
export const SET_PROFILE_VENDORS = 'setProfileVendors';
export const SET_PROFILE_VENDORS_ASSIMILATE = 'setProfileVendorsAssimilate';

export function setVendors(vendors) {
    return {
        type: SET_VENDORS,
        payload: {
            vendors: vendors
        }
    };
}

export function setLatestVendors(vendors) {
    return {
        type: SET_LATEST_VENDORS,
        payload: {
            latestVendors: vendors
        }
    };
}

export function setAroundPositionVendors(vendors) {
    return {
        type: SET_AROUND_POSITION_VENDORS,
        payload: {
            aroundPositionVendors: vendors
        }
    };
}

export function setAroundPositionVendorsIdsByKms(vendorsIds) {
    return {
        type: SET_AROUND_POSITION_VENDORS_IDS_BY_KMS,
        payload: {
            aroundPositionVendorsIdsByKms: vendorsIds
        }
    };
}

export function setBestRateVendors(vendors) {
    return {
        type: SET_BEST_RATE_VENDORS,
        payload: {
            bestRateVendors: vendors
        }
    };
}

export function setProfileVendors(vendors) {
    return {
        type: SET_PROFILE_VENDORS,
        payload: {
            profileVendors: vendors
        }
    };
}

export function setProfileVendorsAssimilate(vendors) {
    return {
        type: SET_PROFILE_VENDORS_ASSIMILATE,
        payload: {
            profileVendorsAssimilate: vendors
        }
    };
}