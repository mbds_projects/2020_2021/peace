export const SET_INSTAGRAM_RECENT = 'setInstagramRecent';

export function setInstagramRecent(instagramRecent) {
    return {
        type: SET_INSTAGRAM_RECENT,
        payload: {
            instagramRecent: instagramRecent
        }
    };
}