export const SET_KEYWORDS = 'setKeywords';

export function setKeywords(keywords) {
    return {
        type: SET_KEYWORDS,
        payload: {
            keywords: keywords
        }
    };
}