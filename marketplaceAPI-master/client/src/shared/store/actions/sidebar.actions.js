export const SET_SIDEBAR = 'setSidebar';

export function setSidebar(open) {
    return {
        type: SET_SIDEBAR,
        payload: {
            open: open
        }
    };
}
   