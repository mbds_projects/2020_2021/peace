export const SET_GEOLOC = 'setGeoloc';

export function setGeoloc(position, accepted) {
    return {
        type: SET_GEOLOC,
        payload: {
            position: position,
            geolocAccepted: accepted
        }
    };
}