export const SET_YOUTUBE_RECENT = 'setYoutubeRecent';

export function setYoutubeRecent(youtubeRecent) {
    return {
        type: SET_YOUTUBE_RECENT,
        payload: {
            youtubeRecent: youtubeRecent
        }
    };
}