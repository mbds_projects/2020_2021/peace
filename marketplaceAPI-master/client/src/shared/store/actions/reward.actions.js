export const SET_REWARDS = 'setRewards';

export function setRewards(rewards) {
    return {
        type: SET_REWARDS,
        payload: {
            rewards: rewards
        }
    };
}