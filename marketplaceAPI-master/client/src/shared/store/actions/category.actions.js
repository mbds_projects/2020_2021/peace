export const SET_CATEGORIES = 'setCategories';

export function setCategories(categories) {
    return {
        type: SET_CATEGORIES,
        payload: {
            categories: categories
        }
    };
}