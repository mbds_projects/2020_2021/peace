export const SET_LOCATIONS_FILTER = 'setLocationsFilter';

export function setLocationsFilter(locations) {
    return {
        type: SET_LOCATIONS_FILTER,
        payload: {
            locations: locations
        }
    };
}