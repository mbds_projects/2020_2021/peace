export const LOGIN = 'login';
export const SET_USER = 'setUser';
export const LOGOUT = 'logout';
export const TOGGLE_LOADING = 'toggleLoading';

export function login(user) {
    return {
        type: LOGIN,
        payload: {
            currentUser: user
        }
    };
}

export function setUser(user) {
    return {
        type: SET_USER,
        payload: {
            currentUser: user
        }
    };
}

export function logout() {
    return {
        type: LOGOUT
    }
}

export function toggleLoading(isLoading) {
    return {
        type: TOGGLE_LOADING,
        payload: {
            isLoading: isLoading
        }
    }
}