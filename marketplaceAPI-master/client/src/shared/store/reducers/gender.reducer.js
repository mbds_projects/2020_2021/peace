import { SET_GENDERS } from '../actions/gender.actions';

export default function genderReducer(state = { genders: []}, action) {
    switch (action.type) {
        case SET_GENDERS:
            return { ...state, genders: action.payload.genders };
        default:
            return state;
    }
}