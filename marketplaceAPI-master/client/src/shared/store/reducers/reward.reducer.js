import { SET_REWARDS } from '../actions/reward.actions';

export default function rewardReducer(state = { rewards: []}, action) {
    switch (action.type) {
        case SET_REWARDS:
            return { ...state, rewards: action.payload.rewards };
        default:
            return state;
    }
}