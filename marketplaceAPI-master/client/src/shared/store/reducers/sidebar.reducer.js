import { SET_SIDEBAR} from '../actions/sidebar.actions';

export default function sidebarReducer(state = { open: false}, action) {
    switch (action.type) {
        case SET_SIDEBAR:
            return { ...state, open: action.payload.open };
        default:
            return state;
    }
}