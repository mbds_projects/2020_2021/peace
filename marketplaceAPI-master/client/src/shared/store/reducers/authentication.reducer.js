import { LOGIN, LOGOUT, TOGGLE_LOADING, SET_USER } from '../actions/authentication.actions';

export default function authenticationReducer(state = { currentUser: null }, action) {
    switch (action.type) {
        case SET_USER:
            let currentUser = {...action.payload.currentUser}
            return { ...state, currentUser };
        case LOGIN:
            return { ...state, currentUser: action.payload.currentUser };
        case LOGOUT:
            return { ...state, currentUser: null}
        case TOGGLE_LOADING:
            return { ...state, isLoading: action.payload.isLoading }
        default:
            return state;
    }
}