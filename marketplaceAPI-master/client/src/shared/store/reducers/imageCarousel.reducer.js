import { SET_IMAGES_CAROUSEL } from '../actions/imageCarousel.actions';

export default function imagesCarouselReducer(state = { imagesCarousel: []}, action) {
    switch (action.type) {
        case SET_IMAGES_CAROUSEL:
            return { ...state, imagesCarousel: action.payload.imagesCarousel };
        default:
            return state;
    }
}