import { SET_KEYWORDS } from '../actions/keyword.actions';

export default function keywordReducer(state = { keywords: []}, action) {
    switch (action.type) {
        case SET_KEYWORDS:
            return { ...state, keywords: action.payload.keywords };
        default:
            return state;
    }
}