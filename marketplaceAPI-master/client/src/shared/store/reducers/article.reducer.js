import { SET_ARTICLES } from '../actions/article.actions';

export default function articleReducer(state = { articles: []}, action) {
    switch (action.type) {
        case SET_ARTICLES:
            return { ...state, articles: action.payload.articles };
        default:
            return state;
    }
}