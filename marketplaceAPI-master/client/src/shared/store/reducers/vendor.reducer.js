import { SET_VENDORS, SET_LATEST_VENDORS, SET_AROUND_POSITION_VENDORS, 
    SET_BEST_RATE_VENDORS, SET_AROUND_POSITION_VENDORS_IDS_BY_KMS, 
    SET_PROFILE_VENDORS, SET_PROFILE_VENDORS_ASSIMILATE } from '../actions/vendor.actions';

export default function vendorReducer(state = { vendors: [], latestVendors: [] , aroundPositionVendors: [], 
    bestRateVendors: [], aroundPositionVendorsIds: [], profileVendors: [], profileVendorsAssimilate: [] }, action) {
    switch (action.type) {
        case SET_VENDORS:
            return { ...state, vendors: action.payload.vendors };
        case SET_LATEST_VENDORS:
            return { ...state, latestVendors: action.payload.latestVendors };
        case SET_AROUND_POSITION_VENDORS:
            return { ...state, aroundPositionVendors: action.payload.aroundPositionVendors };
        case SET_AROUND_POSITION_VENDORS_IDS_BY_KMS:
            return { ...state, aroundPositionVendorsIdsByKms: action.payload.aroundPositionVendorsIdsByKms };
        case SET_BEST_RATE_VENDORS: 
            return { ...state, bestRateVendors: action.payload.bestRateVendors };
        case SET_PROFILE_VENDORS:
            return { ...state, profileVendors: action.payload.profileVendors };
        case SET_PROFILE_VENDORS_ASSIMILATE:
            return { ...state, profileVendorsAssimilate: action.payload.profileVendorsAssimilate };
        default:
            return state;
    }
}