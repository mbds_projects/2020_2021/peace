import { SET_GEOLOC } from '../actions/user.actions';

export default function userReducer(state = { position: null, geolocAccepted: false }, action) {
    switch (action.type) {
        case SET_GEOLOC:
            return { ...state, position: action.payload.position, geolocAccepted: action.payload.geolocAccepted };
        default:
            return state;
    }
}