import { SET_LOCATIONS_FILTER } from '../actions/location.actions';

export default function locationReducer(state = { locationsFilter: []}, action) {
    switch (action.type) {
        case SET_LOCATIONS_FILTER:
            return { ...state, locationsFilter: action.payload.locations };
        default:
            return state;
    }
}