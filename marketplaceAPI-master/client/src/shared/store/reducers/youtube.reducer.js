import { SET_YOUTUBE_RECENT } from '../actions/youtube.actions';

export default function instagramReducer(state = { youtubeRecent: []}, action) {
    switch (action.type) {
        case SET_YOUTUBE_RECENT:
            return { ...state, youtubeRecent: action.payload.youtubeRecent };
        default:
            return state;
    }
}