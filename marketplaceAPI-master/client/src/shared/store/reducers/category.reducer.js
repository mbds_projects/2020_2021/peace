import { SET_CATEGORIES } from '../actions/category.actions';

export default function categoryReducer(state = { categories: []}, action) {
    switch (action.type) {
        case SET_CATEGORIES:
            return { ...state, categories: action.payload.categories };
        default:
            return state;
    }
}