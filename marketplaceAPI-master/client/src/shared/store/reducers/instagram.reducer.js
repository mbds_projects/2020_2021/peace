import { SET_INSTAGRAM_RECENT } from '../actions/instagram.actions';

export default function instagramReducer(state = { instagramRecent: []}, action) {
    switch (action.type) {
        case SET_INSTAGRAM_RECENT:
            return { ...state, instagramRecent: action.payload.instagramRecent };
        default:
            return state;
    }
}