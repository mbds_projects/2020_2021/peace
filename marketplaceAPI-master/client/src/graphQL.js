import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { createUploadLink } from 'apollo-upload-client'
import { InMemoryCache } from 'apollo-cache-inmemory';

import {CONF} from './config/index'

let graphQLUrl = CONF.graphQL;


const uploadLink = createUploadLink({
  uri: graphQLUrl, // Apollo Server is served from port 4000
  headers: {
    "keep-alive": "true"
  }
})

const httpLink = createHttpLink({
  uri: graphQLUrl,
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem('TOKEN');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      token: token ? `${token}` : "",
    }
  }
});

export const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});

export const clientUpload = new ApolloClient({
  cache: new InMemoryCache(),
  link: authLink.concat(uploadLink)
})
