import { decode } from 'jsonwebtoken';
import { vendorLoggedGql } from '../../graphQL/vendorDashboard'
import { userGql } from '../../graphQL/user'

const savedUserStorageKey = 'TOKEN';

export function login(token){
    let decodeToken = decode(token);
    localStorage.setItem(savedUserStorageKey, token);
    if(decodeToken) {
      var current_time = Date.now() / 1000;
      if ( decodeToken['exp'] < current_time) {
        throw new Error("Token is expired");
      }
      return userGql()
      .then(user => { return user })
      .catch(() => {
        throw new Error("Token is invalid");
      }) ;
      //return decodeToken['user']
    } else {
      throw new Error("Token is invalid");
    }
    
}

export function logout() {
    localStorage.removeItem(savedUserStorageKey);
}

export function loginVendor(token){
  let decodeToken = decode(token);
  localStorage.setItem(savedUserStorageKey, token);
  if(decodeToken) {
    var current_time = Date.now() / 1000;
    if ( decodeToken['exp'] < current_time) {
      throw new Error("Token is expired");
    }
    return vendorLoggedGql()
    .then(vendor => { return vendor })
    .catch(() => {
      throw new Error("Token is invalid");
    }) ;
  } else {
    throw new Error("Token is invalid");
  }
  
}