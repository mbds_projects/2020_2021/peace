import { updateUserGql, userCommentsGql } from '../../graphQL/user'
import store from '../../shared/store/store';
import { setUser } from '../../shared/store/actions/authentication.actions';

import * as _ from "lodash";

import ReactGA from 'react-ga';

export async function toggleFavorite(favorites, vendorId, vendorName){
    if(favorites) {
        if(favorites.includes(vendorId)) {
          _.remove(favorites, (e) => e === vendorId)
          ReactGA.event({
            category: 'Favorite',
            action: 'Remove to favorite',
            label: `Vendor ${vendorName}`
          });
        } else {
          favorites.push(vendorId);
          ReactGA.event({
            category: 'Favorite',
            action: 'Add to favorite',
            label: `Vendor ${vendorName}`
          });
        }
        let user = await updateUserGql({input: {favorites: favorites}}).then(res => res);
        if(user) {
          store.dispatch(setUser(user));
          return user;
        } else {
          throw new Error("unknown.error")
        }
    } else {
        throw new Error("user.notLogged")
    }
}

export async function setComments(currentUser){
    if(currentUser) {
        let comments = await userCommentsGql().then(res => res);
        if(comments) {
          currentUser.comments = comments;
          store.dispatch(setUser(currentUser));
          return currentUser;
        } else {
          throw new Error("user.comments.error")
        }
        
    } else {
      throw new Error("user.notLogged")
    }
}




