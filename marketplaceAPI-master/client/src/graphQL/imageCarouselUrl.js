import gql from "graphql-tag";
import { client } from '../graphQL'

const IMAGE_CAROUSEL_URL = `{
  id
  pos
  url
  mode
  lang
  href
  to
}`

const IMAGES_CAROUSEL_URL = gql `
  query imagesCarouselUrl {
    imagesCarouselUrl ${IMAGE_CAROUSEL_URL}
  }
`;

export const imagesCarouselUrlGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: IMAGES_CAROUSEL_URL,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.imagesCarouselUrl;
  } catch (err) {
    throw err;
  }
}