import gql from "graphql-tag";
import { client } from '../graphQL'

import { authError } from '../shared/utils/error'

const INSTAGRAM_SUGGESTED_BRAND_TYPE = `{
  id
  name
  occurrence
  latestPostDate
  checked
  addedHimself
  createdAt
  updatedAt
}`


const INSTAGRAM_SUGGESTED_BRANDS = gql `
  query instagramSuggestedBrands {
    instagramSuggestedBrands ${INSTAGRAM_SUGGESTED_BRAND_TYPE}
  }
`;

/*const CREATE_SUGGESTED_BRANDS = gql `
  mutation createSuggestedBrand($input: _SuggestedBrand) {
    createSuggestedBrand(input: $input) ${SUGGESTED_BRAND_TYPE}
  }
`;*/

const UPDATE_INSTAGRAM_SUGGESTED_BRAND = gql `
  mutation updateInstagramSuggestedBrand($input: _InstagramSuggestedBrandUpdate) {
    updateInstagramSuggestedBrand(input: $input)
  }
`;

const DELETE_INSTAGRAM_SUGGESTED_BRAND = gql `
  mutation deleteInstagramSuggestedBrand($instagramSuggestedBrandId: ID!) {
    deleteInstagramSuggestedBrand(instagramSuggestedBrandId: $instagramSuggestedBrandId)
  }
`;

export const instagramSuggestedBrandsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: INSTAGRAM_SUGGESTED_BRANDS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.instagramSuggestedBrands;
  } catch (err) {
    throw err;
  }
}

export const updateInstagramSuggestedBrandGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_INSTAGRAM_SUGGESTED_BRAND,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateInstagramSuggestedBrand;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const deleteInstagramSuggestedBrandGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: DELETE_INSTAGRAM_SUGGESTED_BRAND,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.deleteInstagramSuggestedBrand;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}