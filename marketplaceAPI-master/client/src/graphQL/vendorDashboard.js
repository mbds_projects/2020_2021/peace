import gql from "graphql-tag";
import { client } from '../graphQL'
import { authError } from '../shared/utils/error'

const PROFILE_VENDOR_LIMITED_TYPE = `{
  id
  name
  logoUrl
  desc {
    lang {
      locale
      value
    }
  }
  comments
  rate
  rewards {
    id
    validity
    activated
  }
  site
  locations {
    id
    address
    city
    postalCode
    location {
      coordinates
    }
    openingHours
    phoneNumber
    shopName
    webSite
    mail
    county
    state
  }
  videoYoutubeId
  videoDailymotionId
  videoVimeoId
  categories
  keywords
  genders
  labels { 
    id 
    name 
    pictureUrl
  }
  socialNetworks {
    type
    url
  }
  alsoAvailableOn
  onlineShop
  clickAndCollect
  carouselPicturesUrl
  featuredProductsUrl {
    imgUrl
    price
    link
  }
  published
}`
const VENDOR_LOGGED_TYPE = `{
  id
  name
  role
  published
  logoUrl
  subscriptionDashboardId
  mail
  customerId
  profilePictureUrl
  subscriptionType
  createdAt
}`

const PROFILE_VENDOR_DASHBOARD = gql`
  query profileVendorDashboard {
    profileVendorDashboard ${PROFILE_VENDOR_LIMITED_TYPE}
  }
`;

const TOKEN = `{
  token
}`

const AUTHENTICATE_VENDOR = gql`
  mutation authenticateVendor($mail: String!, $password: String!) {
    authenticateVendor(mail: $mail, password: $password) ${TOKEN}
  }
`;

const VENDOR_CREATE_PASSWORD = gql`
  mutation createVendorPassword($input: _VendorPassword!) {
    createVendorPassword(input: $input) ${TOKEN}
  }
`;

const VENDOR_LOGGED = gql`
  query vendorLogged {
    vendorLogged ${VENDOR_LOGGED_TYPE}
  }
`;


const FORGOT_PASSWORD_VENDOR = gql`
  mutation forgotPasswordVendor($mail: String!) {
    forgotPasswordVendor(mail: $mail)
  }
`;

const ALLOWED_TO_PUBLISH = gql`
  query allowedToPublish {
    allowedToPublish
}
`;

export const profileVendorDashboardGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: PROFILE_VENDOR_DASHBOARD,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.profileVendorDashboard;
  } catch (err) {
    throw err;
  }
}

export const authenticateVendorGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: AUTHENTICATE_VENDOR,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.authenticateVendor;
  } catch (err) {
    throw err;
  }
}


export const createVendorPasswordGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: VENDOR_CREATE_PASSWORD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createVendorPassword;
  } catch (err) {
    throw err;
  }
}

export const vendorLoggedGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDOR_LOGGED,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorLogged;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}


export const forgotPasswordVendorGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: FORGOT_PASSWORD_VENDOR,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.forgotPasswordVendor;
  } catch (err) {
    throw err;
  }
}

export const allowedToPublishGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: ALLOWED_TO_PUBLISH,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.allowedToPublish;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}