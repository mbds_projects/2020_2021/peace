import gql from "graphql-tag";
import { clientUpload } from '../graphQL'
import { authError } from '../shared/utils/error'

const FEATURED_PRODUCT = `{
  id
  picture {
    type
    data
  }
  link
  price
}`

const CREATE_FEATURED_PRODUCT = gql `
  mutation createFeaturedProduct($file: Upload!, $link: String, $price: Float) {
    createFeaturedProduct(file: $file, link: $link, price: $price) ${FEATURED_PRODUCT}
  }
`;

const EDIT_FEATURED_PRODUCT = gql `
  mutation updateFeaturedProduct($file: Upload!, $id: ID, $link: String, $price: Float) {
    updateFeaturedProduct(file: $file, id:$id, link: $link, price: $price) ${FEATURED_PRODUCT}
  }
`;


export const createFeaturedProductGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: CREATE_FEATURED_PRODUCT,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createFeaturedProduct;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}


export const editFeaturedProductGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: EDIT_FEATURED_PRODUCT,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateFeaturedProduct;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

