import gql from "graphql-tag";
import { client } from '../graphQL'

import { authError } from '../shared/utils/error'

const SUGGESTED_BRAND_TYPE = `{
  id
  name
  categories
  keywords
  suggestedBy {
    user {
      name
      surname
    }
    message
  }
  mail
  site
  occurrence
  cities
  instagramId
  contact
  createdAt
  updatedAt
  state
  vendor
}`


const SUGGESTED_BRANDS = gql `
  query suggestedBrands {
    suggestedBrands ${SUGGESTED_BRAND_TYPE}
  }
`;

const CREATE_SUGGESTED_BRAND = gql `
  mutation createSuggestedBrand($input: _SuggestedBrand) {
    createSuggestedBrand(input: $input) ${SUGGESTED_BRAND_TYPE}
  }
`;

const CREATE_SUGGESTED_BRAND_ADMIN = gql `
  mutation createSuggestedBrandAdmin($input: _SuggestedBrandCreate) {
    createSuggestedBrandAdmin(input: $input) ${SUGGESTED_BRAND_TYPE}
  }
`;

const UPDATE_SUGGESTED_BRAND = gql `
  mutation updateSuggestedBrand($input: _SuggestedBrandUpdate) {
    updateSuggestedBrand(input: $input)
  }
`;

const DELETE_SUGGESTED_BRAND = gql `
  mutation deleteSuggestedBrand($suggestedBrandId: ID!) {
    deleteSuggestedBrand(suggestedBrandId: $suggestedBrandId)
  }
`;


const CONTACT_SUGGESTED_BRAND = gql `
  mutation contactSuggestedBrand($mail: String, $name: String, $id: ID) {
    contactSuggestedBrand(mail: $mail, name: $name, id: $id) ${SUGGESTED_BRAND_TYPE}
  }
`;

const TRANSFORM_SUGGESTED_BRAND = gql `
  mutation transformSuggestedBrandToVendor($id: ID) {
    transformSuggestedBrandToVendor(id: $id)
  }
`;


export const suggestedBrandsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: SUGGESTED_BRANDS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.suggestedBrands;
  } catch (err) {
    throw err;
  }
}

export const createSuggestedBrandGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_SUGGESTED_BRAND,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createSuggestedBrand;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const createSuggestedBrandAdminGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_SUGGESTED_BRAND_ADMIN,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createSuggestedBrandAdmin;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const updateSuggestedBrandGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_SUGGESTED_BRAND,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateSuggestedBrand;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const deleteSuggestedBrandGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: DELETE_SUGGESTED_BRAND,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.deleteSuggestedBrand;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}


export const contactSuggestedBrandGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CONTACT_SUGGESTED_BRAND,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.contactSuggestedBrand;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}


export const transformSuggestedBrandGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: TRANSFORM_SUGGESTED_BRAND,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.transformSuggestedBrandToVendor;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}