import gql from "graphql-tag";
import { client, clientUpload } from '../graphQL'
import { authError } from '../shared/utils/error'

const PROFILE_VENDOR_LIMITED_TYPE = `{
  id
  name
  logoUrl
  desc {
    lang {
      locale
      value
    }
  }
  comments
  rate
  rewards {
    id
    validity
    activated
  }
  site
  locations {
    id
    address
    city
    postalCode
    location {
      coordinates
    }
    openingHours
    phoneNumber
    shopName
    webSite
    mail
    county
    state
  }
  videoYoutubeId
  videoDailymotionId
  videoVimeoId
  categories {
    id
    name
    lang {
      locale
      value
    }
  }
  keywords { 
    id 
    name 
    lang {
      locale
      value
    }
  }
  genders {
    id
    name
    lang {
      locale
      value
    }
  }
  labels { 
    id 
    name 
    pictureUrl
  }
  socialNetworks {
    type
    url
  }
  alsoAvailableOn
  onlineShop
  clickAndCollect
  carouselPicturesUrl
  featuredProductsUrl {
    imgUrl
    price
    link
  }
  published
}`

const PROFILE_VENDOR_DASHBOARD = gql`
  query adminProfileVendorDashboard($id: ID!) {
    adminProfileVendorDashboard(id: $id) ${PROFILE_VENDOR_LIMITED_TYPE}
  }
`;

export const adminProfileVendorDashboardGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: PROFILE_VENDOR_DASHBOARD,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.adminProfileVendorDashboard;
  } catch (err) {
    throw err;
  }
}

const UPDATE_INFO__ADMIN_VENDOR__DASHBOARD = gql`
  mutation updateInfoAdminVendorDashboard($input: _InfoVendorAdmin, $vendorId: ID) {
    updateInfoAdminVendorDashboard(input: $input, vendorId: $vendorId)
  }
`;

export const updateInfoAdminVendorDashboardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_INFO__ADMIN_VENDOR__DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateInfoAdminVendorDashboard;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

const UPDATE_CAROUSEL_PICTURES_ADMIN_VENDOR_DASHBOARD = gql`
  mutation updateCarouselPictureAdminVendorDashboard($file: Upload, $indexOfImage: Int, $vendorId: ID) {
    updateCarouselPictureAdminVendorDashboard(file: $file, indexOfImage: $indexOfImage, vendorId: $vendorId)
  }
`;


export const updateCarouselPictureAdminVendorDashboardGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: UPDATE_CAROUSEL_PICTURES_ADMIN_VENDOR_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateCarouselPictureAdminVendorDashboard;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

const UPDATE_FEATURED_PRODUCT_PICTURE_ADMIN_VENDOR_DASHBOARD = gql`
  mutation updateFeaturedProductPictureAdminVendorDashboard($file: Upload, $indexOfImage: Int, $price: Float, $link: String, $vendorId: ID) {
    updateFeaturedProductPictureAdminVendorDashboard(file: $file, indexOfImage: $indexOfImage, price: $price, link: $link, vendorId: $vendorId)
  }
`;

export const updateFeaturedProductPictureAdminVendorDashboardGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: UPDATE_FEATURED_PRODUCT_PICTURE_ADMIN_VENDOR_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateFeaturedProductPictureAdminVendorDashboard;
  } catch (err) {
    throw err;
  }
}

const UPDATE_FEATURED_PRODUCT_INFO_ADMIN_VENDOR_DASHBOARD = gql`
  mutation updateFeaturedProductInfoAdminVendorDashboard($indexOfProduct: Int, $price: Float, $link: String, $vendorId: ID) {
    updateFeaturedProductInfoAdminVendorDashboard(indexOfProduct: $indexOfProduct, price: $price, link: $link, vendorId: $vendorId)
  }
`;

export const updateFeaturedProductInfoAdminVendorDashboardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_FEATURED_PRODUCT_INFO_ADMIN_VENDOR_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateFeaturedProductInfoAdminVendorDashboard;
  } catch (err) {
    throw err;
  }
}

const VENDOR_CREATE_TOKEN_PASSWORD = gql`
  mutation createTokenVendorPassword($id: ID!, $toAdmin: Boolean) {
    createTokenVendorPassword(id: $id, toAdmin:$toAdmin)
  }
`;


export const createTokenVendorPasswordGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: VENDOR_CREATE_TOKEN_PASSWORD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createTokenVendorPassword;
  } catch (err) {
    throw err;
  }
}