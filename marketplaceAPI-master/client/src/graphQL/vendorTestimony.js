import gql from "graphql-tag";
import { client } from '../graphQL'

const VENDOR_TESTIMONY = `{
  vendor
  testimonyLink {
    lang {
      locale
      value
    }
  }
}`


export const VENDOR_TESTIMONIES = gql `
  query vendorTestimonies {
    vendorTestimonies ${VENDOR_TESTIMONY}
  }
`;


export const vendorTestimoniesGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDOR_TESTIMONIES,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorTestimonies;
  } catch (err) {
    throw err;
  }
}