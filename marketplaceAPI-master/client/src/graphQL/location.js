import gql from "graphql-tag";
import { client } from '../graphQL'
import { authError } from '../shared/utils/error'

const LOCATION_FILTER_CITY = `{
  city
  vendors
}`

const LOCATION_FILTER_COUNTY = `{
  county
  vendors
}`

const LOCATION_TYPE = `{
  id
  address
  city
  postalCode
  location {
    coordinates
  }
  openingHours
  phoneNumber
  shopName
  webSite
  mail
  vendors
  state
  county
}`

const LOCATION_BY_CITY = `{
  city
  shops {
    id
    address
    city
    postalCode
    location {
      coordinates
    }
    openingHours
    phoneNumber
    shopName
    webSite
    mail
    county
    state
  }
}`


const LOCATION_BY_COUNTY = `{
  county
  shops {
    id
    address
    city
    postalCode
    location {
      coordinates
    }
    openingHours
    phoneNumber
    shopName
    webSite
    mail
    county
    state
  }
}`

const LOCATION_SEARCH = gql `{
  address
  city
  shopName
  postalCode
  location {
    coordinates
  }
  openingHours
  phoneNumber
  webSite
  county
  state
}`

const LOCATIONS_BY_CITY = gql `
  query locationsByCity {
    locationsByCity ${LOCATION_FILTER_CITY}
  }
`;

const LOCATIONS_BY_COUNTY = gql `
  query locationsByCounty {
    locationsByCounty ${LOCATION_FILTER_COUNTY}
  }
`;

const LOCATIONS_OF_VENDOR_BY_CITY = gql `
  query locationsOfVendorByCity($vendorId: ID, $position: _Position) {
    locationsOfVendorByCity(vendorId: $vendorId, position: $position) ${LOCATION_BY_CITY}
  }
`;

const LOCATIONS_OF_VENDOR_BY_COUNTY = gql `
  query locationsOfVendorByCounty($vendorId: ID) {
    locationsOfVendorByCounty(vendorId: $vendorId) ${LOCATION_BY_COUNTY}
  }
`;

const LOCATIONS = gql `
  query locations {
    locations ${LOCATION_TYPE}
  }
`;

const CREATE_LOCATION_ADMIN = gql `
mutation createLocation($input: _LocationCreationAdmin) {
    createLocation(input: $input) ${LOCATION_TYPE}
  }
`;

const UPDATE_LOCATION_ADMIN = gql `
  mutation updateLocation($input: _LocationAdmin, $id: ID) {
    updateLocation(input: $input, id: $id)
  }
`;

const SEARCH_LOCATION_DASHBOARD = gql `
  mutation searchLocationDashboard($address: String) {
    searchLocationDashboard(address: $address) ${LOCATION_SEARCH}
  }
`;


const CREATE_LOCATION_DASHBOARD = gql `
mutation createLocationDashboard($input: _LocationDashboardCreation) {
  createLocationDashboard(input: $input) ${LOCATION_TYPE}
  }
`;

const UPDATE_LOCATION_DASHBOARD = gql `
  mutation updateLocationDashboard($input: _LocationDashboard, $locId: ID) {
    updateLocationDashboard(input: $input, locId: $locId)
  }
`;

const REMOVE_LOCATION_DASHBOARD = gql `
  mutation removeLocationFromVendorDashboard($locId: ID) {
    removeLocationFromVendorDashboard(locId: $locId)
  }
`;

const CREATE_LOCATION_ADMIN_VENDOR_DASHBOARD = gql `
mutation createLocationAdminVendorDashboard($input: _LocationDashboardCreation, $vendorId: ID) {
  createLocationAdminVendorDashboard(input: $input, vendorId: $vendorId) ${LOCATION_TYPE}
  }
`;

const UPDATE_LOCATION_ADMIN_VENDOR_DASHBOARD = gql `
  mutation updateLocationAdminVendorDashboard($input: _LocationDashboard, $locId: ID) {
    updateLocationAdminVendorDashboard(input: $input, locId: $locId)
  }
`;

const REMOVE_LOCATION_ADMIN_VENDOR_DASHBOARD = gql `
  mutation removeLocationAdminVendorDashboard($locId: ID, $vendorId: ID) {
    removeLocationAdminVendorDashboard(locId: $locId, vendorId: $vendorId)
  }
`;

const ALLOWED_TO_CREATE_LOCATION = gql `
  query allowedToCreateLocation {
    allowedToCreateLocation
  }
`;


export const locationsByCityGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: LOCATIONS_BY_CITY,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.locationsByCity;
  } catch (err) {
    throw err;
  }
}

export const allowedToCreateLocationGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: ALLOWED_TO_CREATE_LOCATION,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.allowedToCreateLocation;
  } catch (err) {
    throw err;
  }
}

export const locationsByCountyGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: LOCATIONS_BY_COUNTY,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.locationsByCounty;
  } catch (err) {
    throw err;
  }
}


export const locationsOfVendorByCityGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: LOCATIONS_OF_VENDOR_BY_CITY,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.locationsOfVendorByCity;
  } catch (err) {
    throw err;
  }
}

export const locationsOfVendorByCountyGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: LOCATIONS_OF_VENDOR_BY_COUNTY,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.locationsOfVendorByCounty;
  } catch (err) {
    throw err;
  }
}

export const locationsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: LOCATIONS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.locations;
  } catch (err) {
    throw err;
  }
}

export const createLocationGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_LOCATION_ADMIN,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createLocation;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const updateLocationGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_LOCATION_ADMIN,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateLocation;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const searchLocationDashboardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: SEARCH_LOCATION_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.searchLocationDashboard;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const createLocationDashboardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_LOCATION_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createLocationDashboard;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const updateLocationDashboardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_LOCATION_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateLocationDashboard;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const removeLocationFromVendorDashboardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: REMOVE_LOCATION_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.removeLocationFromVendorDashboard;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const createLocationAdminVendorDashboardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_LOCATION_ADMIN_VENDOR_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createLocationAdminVendorDashboard;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const updateLocationAdminVendorDashboardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_LOCATION_ADMIN_VENDOR_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateLocationAdminVendorDashboard;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const removeLocationAdminVendorDashboardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: REMOVE_LOCATION_ADMIN_VENDOR_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.removeLocationAdminVendorDashboard;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}