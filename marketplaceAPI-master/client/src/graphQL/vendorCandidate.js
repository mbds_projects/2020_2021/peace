import gql from "graphql-tag";
import { client } from '../graphQL'

import { authError } from '../shared/utils/error'




const VENDOR_CANDIDATE_TYPE = `{
  id
  name
  site
  mail
  phoneNumber
  categories {id name}
  keywords {id name}
  subscriptionType
  createdAt
  updatedAt
  vendor
  labels {id name}
  genders {id name}
}`

const CREATE_VENDOR_CANDIDATE = gql `
  mutation createVendorCandidate($input: _VendorCandidate) {
    createVendorCandidate(input: $input)
  }
`;

export const createVendorCandidateGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_VENDOR_CANDIDATE,
      fetchPolicy: 'no-cache',
    });
    return data.createVendorCandidate;
  } catch (err) {
    throw err;
  }
}


const FOLLOW_NEWSLETTER_DASHBOARD = gql `
  mutation followNewsletterDashboard($input: _VendorCandidateLimited) {
    followNewsletterDashboard(input: $input)
  }
`;

export const followNewsletterDashboardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: FOLLOW_NEWSLETTER_DASHBOARD,
      fetchPolicy: 'no-cache',
    });
    return data.followNewsletterDashboard;
  } catch (err) {
    throw err;
  }
}


const VENDORS_CANDIDATE = gql `
  query vendorsCandidate {
    vendorsCandidate ${VENDOR_CANDIDATE_TYPE}
  }
`;

export const vendorsCandidateGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDORS_CANDIDATE,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorsCandidate;
  } catch (err) {
    throw err;
  }
}

const TRANSFORM_VENDOR_CANDIDATE = gql `
  mutation transformVendorCandidateToVendor($id: ID) {
    transformVendorCandidateToVendor(id: $id)
  }
`;

export const transformVendorCandidateGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: TRANSFORM_VENDOR_CANDIDATE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.transformVendorCandidateToVendor;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

const vendorsendinfo = gql `
  mutation vendorsendinfo($mail: String) {
    vendorsendinfo(mail: $mail)
  }
`;

export const vendorsendinfoGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: vendorsendinfo,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.vendorsendinfo;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

const vendorCandidateSaveInfo = gql`
  mutation vendorCandidateSaveInfo($input: _InfoVendorCandidate, $vendorId: ID) {
    vendorCandidateSaveInfo(input: $input, vendorId: $vendorId)
  }
`;

export const vendorCandidateSaveInfoGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: vendorCandidateSaveInfo,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.vendorCandidateSaveInfo;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}
