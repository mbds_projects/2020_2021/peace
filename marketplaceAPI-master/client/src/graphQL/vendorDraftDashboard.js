import gql from "graphql-tag";
import { client, clientUpload } from '../graphQL'
import { authError } from '../shared/utils/error'

const VENDOR_DRAFT_DASHBOARD_TYPE = `{
  id
  name
  logoUrl
  desc {
    lang {
      locale
      value
    }
  }
  rewards {
    id
    validity
    activated
  }
  site
  locations {
    id
    address
    city
    postalCode
    location {
      coordinates
    }
    openingHours
    phoneNumber
    shopName
    webSite
    mail
    county
    state
  }
  videoYoutubeId
  videoDailymotionId
  videoVimeoId
  categories
  keywords
  genders
  labels { 
    id 
    name 
    pictureUrl
  }
  socialNetworks {
    type
    url
  }
  alsoAvailableOn
  onlineShop
  clickAndCollect
  carouselPicturesUrl
  featuredProductsUrl {
    imgUrl
    price
    link
  }
}`

const VENDOR_DRAFT_DASHBOARD = gql`
  query vendorDraftDashboard {
    vendorDraftDashboard ${VENDOR_DRAFT_DASHBOARD_TYPE}
  }
`;

const UPDATE_INFO_VENDOR_DRAFT_DASHBOARD = gql`
  mutation updateInfoVendorDraftDashboard($input: _InfoVendor) {
    updateInfoVendorDraftDashboard(input: $input)
  }
`;

const UPDATE_CAROUSEL_PICTURES_VENDOR_DRAFT_DASHBOARD = gql`
  mutation updateCarouselPictureVendorDraftDashboard($file: Upload, $indexOfImage: Int) {
    updateCarouselPictureVendorDraftDashboard(file: $file, indexOfImage: $indexOfImage)
  }
`;

const UPDATE_FEATURED_PRODUCT_PICTURE_VENDOR_DRAFT_DASHBOARD = gql`
  mutation updateFeaturedProductPictureVendorDraftDashboard($file: Upload, $indexOfImage: Int, $price: Float, $link: String) {
    updateFeaturedProductPictureVendorDraftDashboard(file: $file, indexOfImage: $indexOfImage, price: $price, link: $link)
  }
`;

const UPDATE_FEATURED_PRODUCT_INFO_VENDOR_DRAFT_DASHBOARD = gql`
  mutation updateFeaturedProductInfoVendorDraftDashboard($indexOfProduct: Int, $price: Float, $link: String) {
    updateFeaturedProductInfoVendorDraftDashboard(indexOfProduct: $indexOfProduct, price: $price, link: $link)
  }
`;

const PUBLISH_DRAFT_DASHBOARD = gql`
  mutation publishDraftDashboard {
    publishDraftDashboard
  }
`;


export const vendorDraftDashboardGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDOR_DRAFT_DASHBOARD,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorDraftDashboard;
  } catch (err) {
    throw err;
  }
}

export const updateInfoVendorDraftDashboardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_INFO_VENDOR_DRAFT_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateInfoVendorDraftDashboard;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const updateCarouselPictureVendorDraftDashboardGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: UPDATE_CAROUSEL_PICTURES_VENDOR_DRAFT_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateCarouselPictureVendorDraftDashboard;
  } catch (err) {
    throw err;
  }
}

export const updateFeaturedProductPictureVendorDraftDashboardGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: UPDATE_FEATURED_PRODUCT_PICTURE_VENDOR_DRAFT_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateFeaturedProductPictureVendorDraftDashboard;
  } catch (err) {
    throw err;
  }
}

export const updateFeaturedProductInfoVendorDraftDashboardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_FEATURED_PRODUCT_INFO_VENDOR_DRAFT_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateFeaturedProductInfoVendorDraftDashboard;
  } catch (err) {
    throw err;
  }
}

export const publishDraftDashboardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: PUBLISH_DRAFT_DASHBOARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.publishDraftDashboard;
  } catch (err) {
    throw err;
  }
}