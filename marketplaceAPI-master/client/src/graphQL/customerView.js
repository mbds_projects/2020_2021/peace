import gql from "graphql-tag";
import { client } from '../graphQL'

const CUSTOMER_VIEW = `{
  id
  text
  postedBy {
    id
    name
    surname
    defaultColor
    profilePictureUrl
  }
  date
  rate
}`


export const CREATE_CUSTOMER_VIEW = gql `
  mutation createCustomerView($input: _CustomerView!) {
    createCustomerView(input: $input) ${CUSTOMER_VIEW}
  }
`;

export const GET_CUSTOMER_VIEW = gql `
  query customerView {
    customerView
  }
`;


export const createCustomerViewGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_CUSTOMER_VIEW,
      fetchPolicy: 'no-cache'
    });
    return data.createCustomerView;
  } catch (err) {
    throw err;
  }
}

export const customerViewGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: GET_CUSTOMER_VIEW,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.customerView;
  } catch (err) {
    throw err;
  }
}