import gql from "graphql-tag";
import { client, clientUpload } from '../graphQL'
import { authError } from '../shared/utils/error'

const GENDER = `{
  id
  name
  used
  lang {
    locale
    value
  }
  pictureUrl
  createdAt
  updatedAt
}`

const IMAGE = `{
  id
  type
  data
}`


const GENDERS = gql `
  query genders {
    genders ${GENDER}
  }
`;

const GENDERS_USED = gql `
  query gendersUsed {
    gendersUsed ${GENDER}
  }
`;

const UPDATE_GENDER_DESC = gql `
  mutation updateGenderDesc($descFr: String, $descEn: String, $id:ID) {
    updateGenderDesc(descFr: $descFr, descEn : $descEn, id: $id)
  }
`;

const UPDATE_GENDER_PICTURE = gql `
  mutation updatePictureGender($file: Upload, $id: ID) {
    updatePictureGender(file: $file, id: $id)
  }
`;

export const gendersGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: GENDERS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.genders;
  } catch (err) {
    throw err;
  }
}

export const gendersUsedGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: GENDERS_USED,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.gendersUsed;
  } catch (err) {
    throw err;
  }
}


export const uploadPictureGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: UPDATE_GENDER_PICTURE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updatePictureGender;
  } catch (err) {
    throw err;
  }
}

export const updateDescGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_GENDER_DESC,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateGenderDesc;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}