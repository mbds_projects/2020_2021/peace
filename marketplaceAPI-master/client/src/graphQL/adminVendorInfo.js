import gql from "graphql-tag";
import { client, clientUpload } from '../graphQL'
import { authError } from '../shared/utils/error'

const ADMIN_VENDOR_INFO_UPDATE = gql`
  mutation adminVendorInfoUpdate($input: _InfoVendorAdmin, $vendorId: ID) {
    adminVendorInfoUpdate(input: $input, vendorId: $vendorId)
  }
`;

export const adminVendorInfoUpdateGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: ADMIN_VENDOR_INFO_UPDATE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.adminVendorInfoUpdate;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

const ADMIN_VENDOR_PROFILE_PICTURE_UPDATE = gql `
  mutation adminVendorProfilePictureUpdate($file: Upload, $vendorId: ID) {
    adminVendorProfilePictureUpdate(file: $file, vendorId: $vendorId)
  }
`;

export const adminVendorProfilePictureUpdateGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: ADMIN_VENDOR_PROFILE_PICTURE_UPDATE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.adminVendorProfilePictureUpdate;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

const ADMIN_VENDOR_LOGO_UPDATE = gql `
  mutation adminVendorLogoUpdate($file: Upload, $vendorId: ID) {
    adminVendorLogoUpdate(file: $file, vendorId: $vendorId)
  }
`;

export const adminVendorLogoUpdateGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: ADMIN_VENDOR_LOGO_UPDATE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.adminVendorLogoUpdate;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

