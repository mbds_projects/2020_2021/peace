import gql from "graphql-tag";
import { client } from '../graphQL'

const CUSTOMER_OPINION = `{
  id
  text
  date
  rate
  mail
  user
}`


export const CREATE_CUSTOMER_OPINION = gql `
  mutation createCustomerOpinion($input: _CustomerOpinion!) {
    createCustomerOpinion(input: $input) ${CUSTOMER_OPINION}
  }
`;

export const CUSTOMER_OPINIONS = gql `
  query customerOpinions {
    customerOpinions ${CUSTOMER_OPINION}
  }
`;


export const createCustomerOpinionGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_CUSTOMER_OPINION,
      fetchPolicy: 'no-cache'
    });
    return data.createCustomerOpinion;
  } catch (err) {
    throw err;
  }
}

export const customerOpinionsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: CUSTOMER_OPINIONS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.customerOpinions;
  } catch (err) {
    throw err;
  }
}