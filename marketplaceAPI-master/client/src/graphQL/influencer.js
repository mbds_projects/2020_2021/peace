import gql from "graphql-tag";
import { client } from '../graphQL'
import _ from 'lodash'

const INFLUENCER = `{
  title
  link
  imgUrl
}`

const INFLUENCERS = gql `
  query influencers {
    influencers ${INFLUENCER}
  }
`;

export const influencersGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: INFLUENCERS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.influencers;
  } catch (err) {
    throw err;
  }
}