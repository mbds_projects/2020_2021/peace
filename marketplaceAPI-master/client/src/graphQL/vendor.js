import gql from "graphql-tag";
import { client, clientUpload } from '../graphQL'
import { authError } from '../shared/utils/error'

const VENDOR_LIMITED_TYPE= `{
  id
  name
  profilePictureUrl
  logoUrl
  comments
  rate
  categories
  genders
  keywords
  labels
  onlineShop
  clickAndCollect
  activated
  published
}`

const PROFILE_VENDOR_LIMITED_TYPE= `{
  id
  name
  logoUrl
  desc {
    lang {
      locale
      value
    }
  }
  comments
  rate
  rewards {
    id
    validity
    activated
  }
  site
  locations {
    id
    address
    city
    postalCode
    location {
      coordinates
    }
    openingHours
    phoneNumber
    shopName
    webSite
    mail
    county
    state
  }
  videoYoutubeId
  videoDailymotionId
  videoVimeoId
  categories
  keywords
  genders
  labels { 
    id 
    name 
    pictureUrl
  }
  socialNetworks {
    type
    url
  }
  alsoAvailableOn
  onlineShop
  clickAndCollect
  carouselPicturesUrl
  featuredProductsUrl {
    imgUrl
    price
    link
  }
  profilePictureUrl
}`

const PROFILE_VENDOR_LIMITED_ADMIN_TYPE= `{
  id
  name
  logoUrl
  desc {
    lang {
      locale
      value
    }
  }
  comments
  rate
  rewards {
    id
    validity
    activated
  }
  site
  locations {
    id
    address
    city
    postalCode
    location {
      coordinates
    }
    openingHours
    phoneNumber
    shopName
    webSite
    mail
    county
    state
  }
  videoYoutubeId
  videoDailymotionId
  videoVimeoId
  categories {
    id
    name
    lang {
      locale
      value
    }
  }
  keywords { 
    id 
    name 
    lang {
      locale
      value
    }
  }
  genders {
    id
    name
    lang {
      locale
      value
    }
  }
  labels { 
    id 
    name 
    pictureUrl
  }
  socialNetworks {
    type
    url
  }
  alsoAvailableOn
  onlineShop
  clickAndCollect
  carouselPicturesUrl
  featuredProductsUrl {
    imgUrl
    price
    link
  }
}`

const VENDOR_MINIMAL = `{
  id
  name
  published
}`

const FEATURED_PRODUCT_TYPE = `{
  id
  picture { data, type}
  link
  price
}`

const VENDORS_MINIMAL = gql `
  query vendorsMinimal {
    vendorsMinimal ${VENDOR_MINIMAL}
  }
`;

const VENDORS_WITHOUT_ACCOUNT = gql `
  query vendorsWithoutAccount {
    vendorsWithoutAccount ${VENDOR_MINIMAL}
  }
`;

const VENDORS_MINIMAL_ADMIN = gql `
  query vendorsMinimalAdmin {
    vendorsMinimalAdmin ${VENDOR_MINIMAL}
  }
`;

const LATEST_VENDORS = gql `
  query latestVendorsAdded {
    latestVendorsAdded ${VENDOR_LIMITED_TYPE}
  }
`;

const VENDORS_AROUND_POSITION = gql `
  query aroundPositionVendors($position: _Position, $maxDistance: Int, $limit: Int) {
    aroundPositionVendors(position: $position, maxDistance: $maxDistance, limit: $limit) ${VENDOR_LIMITED_TYPE}
  }
`;

const VENDORS_AROUND_POSITION_IDS_BY_KMS = gql `
  query aroundPositionVendorsIdsByKms($position: _Position) {
    aroundPositionVendorsIdsByKms(position: $position)
  }
`;

const VENDORS_ONLY_ONLINE_IDS = gql `
  query onlyOnlineVendorsIds($limit: Int) {
    onlyOnlineVendorsIds(limit: $limit)
  }
`;


const BEST_RATE_VENDORS = gql `
  query bestRateVendors {
    bestRateVendors ${VENDOR_LIMITED_TYPE}
  }
`;

const MORE_FAVORITES_VENDORS = gql `
  query moreFavoritesVendors {
    moreFavoritesVendors ${VENDOR_LIMITED_TYPE}
  }
`;

const VENDORS_LIMITED = gql `
  query vendorsLimited {
    vendorsLimited ${VENDOR_LIMITED_TYPE}
  }
`;

const VENDORS_LIMITED_ADMIN = gql `
  query vendorsLimitedAdmin {
    vendorsLimitedAdmin ${VENDOR_LIMITED_TYPE}
  }
`;

const PROFILE_VENDOR_LIMITED = gql `
  query profileVendorLimited($id: ID!) {
    profileVendorLimited(id: $id) ${PROFILE_VENDOR_LIMITED_TYPE}
  }
`;

const PROFILE_VENDOR_LIMITED_ADMIN = gql `
  query profileVendorLimitedAdmin($id: ID!) {
    profileVendorLimitedAdmin(id: $id) ${PROFILE_VENDOR_LIMITED_TYPE}
  }
`;

const VENDORS_ASSIMILATE = gql `
  query vendorsAssimilate($categories: [ID], $keywords: [ID], $vendorId: ID, $limit: Int) {
    vendorsAssimilate(categories: $categories, keywords: $keywords, vendorId: $vendorId, limit: $limit) ${VENDOR_LIMITED_TYPE}
  }
`;

const VENDORS_ASSIMILATE_MINIMAL = gql `
  query vendorsAssimilateMinimal($categories: [ID], $keywords: [ID], $labels: [ID]) {
    vendorsAssimilateMinimal(categories: $categories, keywords: $keywords, labels: $labels) ${VENDOR_MINIMAL}
  }
`;

const PUBLISH_VENDOR = gql `
  mutation publishVendor($vendorId: ID) {
    publishVendor(vendorId: $vendorId)
  }
`;

const UNPUBLISH_VENDOR = gql `
  mutation unpublishVendor($vendorId: ID) {
    unpublishVendor(vendorId: $vendorId)
  }
`;


const UPDATE_INFO_VENDOR = gql `
  mutation updateInfoVendor($vendorId: ID, $input: _InfoVendor) {
    updateInfoVendor(vendorId: $vendorId, input: $input)
  }
`;


export const vendorsWithoutAccountGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDORS_WITHOUT_ACCOUNT,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorsWithoutAccount;
  } catch (err) {
    throw err;
  }
}

export const vendorsMinimalGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDORS_MINIMAL,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorsMinimal;
  } catch (err) {
    throw err;
  }
}

export const vendorsMinimalAdminGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDORS_MINIMAL_ADMIN,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorsMinimalAdmin;
  } catch (err) {
    throw err;
  }
}


export const vendorsLimitedGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDORS_LIMITED,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorsLimited;
  } catch (err) {
    throw err;
  }
}

export const vendorsLimitedAdminGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDORS_LIMITED_ADMIN,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorsLimitedAdmin;
  } catch (err) {
    throw err;
  }
}

export const latestVendorsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: LATEST_VENDORS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.latestVendorsAdded;
  } catch (err) {
    throw err;
  }
}

export const aroundPositionVendorsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDORS_AROUND_POSITION,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.aroundPositionVendors;
  } catch (err) {
    throw err;
  }
}

export const aroundPositionVendorsIdsByKmsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDORS_AROUND_POSITION_IDS_BY_KMS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.aroundPositionVendorsIdsByKms;
  } catch (err) {
    throw err;
  }
}

export const onlyOnlineVendorsIdsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDORS_ONLY_ONLINE_IDS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.onlyOnlineVendorsIds;
  } catch (err) {
    throw err;
  }
}

export const bestRateVendorsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: BEST_RATE_VENDORS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.bestRateVendors;
  } catch (err) {
    throw err;
  }
}

export const moreFavoritesVendorsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: MORE_FAVORITES_VENDORS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.moreFavoritesVendors;
  } catch (err) {
    throw err;
  }
}

const VENDOR_LIMITED = gql `
  query vendorLimited($id: ID!) {
    vendorLimited(id: $id) ${VENDOR_LIMITED_TYPE}
  }
`;

export const vendorLimitedGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDOR_LIMITED,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorLimited;
  } catch (err) {
    throw err;
  }
}
const VENDOR_LIMITED_ADMIN = gql `
  query vendorLimitedAdmin($id: ID!) {
    vendorLimitedAdmin(id: $id) ${VENDOR_LIMITED_TYPE}
  }
`;

export const vendorLimitedAdminGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDOR_LIMITED_ADMIN,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorLimitedAdmin;
  } catch (err) {
    throw err;
  }
}

export const profileVendorLimitedGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: PROFILE_VENDOR_LIMITED,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.profileVendorLimited;
  } catch (err) {
    throw err;
  }
}

export const profileVendorLimitedAdminGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: PROFILE_VENDOR_LIMITED_ADMIN,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.profileVendorLimitedAdmin;
  } catch (err) {
    throw err;
  }
}

export const vendorsAssimilateGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDORS_ASSIMILATE,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorsAssimilate;
  } catch (err) {
    throw err;
  }
}

export const vendorsAssimilateMinimalGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDORS_ASSIMILATE_MINIMAL,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorsAssimilateMinimal;
  } catch (err) {
    throw err;
  }
}

export const publishVendorGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: PUBLISH_VENDOR,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.publishVendor;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const unpublishVendorGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UNPUBLISH_VENDOR,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.unpublishVendor;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const updateInfoVendorGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_INFO_VENDOR,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateInfoVendor;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}