import gql from "graphql-tag";
import { client } from '../graphQL'
import _ from 'lodash'

const INSTAGRAM = `{
  link
  media
}`

const INSTAGRAM_RECENT = gql `
  query instagramRecent {
    instagramRecent ${INSTAGRAM}
  }
`;

const INSTAGRAM_POPULAR = gql `
  query instagramPopular {
    instagramPopular ${INSTAGRAM}
  }
`;

export const instagramRecentGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: INSTAGRAM_RECENT,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.instagramRecent;
  } catch (err) {
    throw err;
  }
}

export const instagramPopularGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: INSTAGRAM_POPULAR,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.instagramPopular;
  } catch (err) {
    throw err;
  }
}