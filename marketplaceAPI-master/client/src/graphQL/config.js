import gql from "graphql-tag";
import { client } from '../graphQL'

const CONFIG_TYPE = `{
  webAppVersionBuild
}`

const CONFIG = gql `
  query config {
    config ${CONFIG_TYPE}
  }
`;

export const configGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: CONFIG,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.config;
  } catch (err) {
    throw err;
  }
}