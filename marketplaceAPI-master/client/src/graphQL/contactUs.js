import gql from "graphql-tag";
import { client } from '../graphQL'

const CONTACT_US = gql `
mutation contactUs($input: _ContactUs) {
  contactUs(input: $input)
}
`;

export const contactUsGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CONTACT_US,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.contactUs;
  } catch (err) {
    throw err;
  }
}