import gql from "graphql-tag";
import { client } from '../graphQL'

const ACHIEVEMENT = `{
  id
  type
  value
  icon
  level
  subtype
  activated
  link
}`

const ACHIEVEMENTS = gql `
  query achievements {
    achievements ${ACHIEVEMENT}
  }
`;

export const achievementsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: ACHIEVEMENTS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.achievements;
  } catch (err) {
    throw err;
  }
}