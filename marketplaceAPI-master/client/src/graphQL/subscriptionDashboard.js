import gql from "graphql-tag";
import { client } from '../graphQL'
import { authError } from '../shared/utils/error'

const SUBSCRIPTION_CREATED_TYPE = `{
  id
  status
  current_period_end
  latest_invoice {
    id
    payment_intent {
      id
      status
      client_secret
      payment_method
    }
  }
}`

const INVOICE_TYPE = `{
    id
    status
    payment_intent {
      id
      status
      client_secret
      payment_method
    }
}`

const SUBSCRIPTION_TYPE = `{
  id
  status
  unit_amount
  currency
  current_period_end
  current_period_start
  recurring
  customer
  price
  created
  latest_invoice {
    id
    payment_intent {
      id
      client_secret
      status
      payment_method
    }
  }
  pending_setup_intent {
    client_secret
    status
  }
  payment_method {
    last4
    brand
  }
  product {
    name
  }
}`

const PRORATION_TYPE = `{
  finalPrice
  price
  finalTaxes
}`

const CREATE_SUBSCRIPTION_DETAIL_OF_VENDOR = gql `
  mutation createSubscriptionDetailOfVendor($paymentMethodId: String, $from: Int, $tvaIntra: String, $subscriptionType: String) {
    createSubscriptionDetailOfVendor(paymentMethodId: $paymentMethodId, from: $from, tvaIntra: $tvaIntra, subscriptionType: $subscriptionType)
  }
`;

export const createSubscriptionDetailOfVendorGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_SUBSCRIPTION_DETAIL_OF_VENDOR,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createSubscriptionDetailOfVendor;
  } catch (err) {
    throw err;
  }
}


const CREATE_SUBSCRIPTION_FROM_VENDOR = gql `
  mutation createSubscriptionFromVendor {
    createSubscriptionFromVendor ${SUBSCRIPTION_CREATED_TYPE}
  }
`;

export const createSubscriptionFromVendorGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_SUBSCRIPTION_FROM_VENDOR,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createSubscriptionFromVendor;
  } catch (err) {
    throw err;
  }
}

const RETRY_INVOICE = gql `
  mutation retryInvoice($paymentMethodId: String, $invoiceId: String) {
    retryInvoice(paymentMethodId: $paymentMethodId, invoiceId: $invoiceId) ${INVOICE_TYPE}
  }
`;

export const retryInvoiceGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: RETRY_INVOICE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.retryInvoice;
  } catch (err) {
    throw err;
  }
}


const RETRIEVE_SUBSCRIPTION = gql `
  query retrieveDashboardSubscription {
    retrieveDashboardSubscription ${SUBSCRIPTION_TYPE}
  }
`;


export const retrieveSubscriptionGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: RETRIEVE_SUBSCRIPTION,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.retrieveDashboardSubscription;
  } catch (err) {
    authError(err.message);
  }
}

const CANCEL_SUBSCRIPTION = gql `
  mutation cancelSubscription {
    cancelSubscription
  }
`;

export const cancelSubscriptionGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CANCEL_SUBSCRIPTION,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.cancelSubscription;
  } catch (err) {
    throw err;
  }
}

const UPDATE_SUBSCRIPTION = gql `
  mutation updateSubscription($subscriptionType: String, $proration: Boolean) {
    updateSubscription(subscriptionType: $subscriptionType, proration: $proration) ${SUBSCRIPTION_TYPE}
  }
`;

export const updateSubscriptionGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_SUBSCRIPTION,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateSubscription;
  } catch (err) {
    throw err;
  }
}

const CALCULATE_SUBSCRIPTION = gql `
  mutation calculateProration($subscriptionType: String, $proration: Boolean) {
    calculateProration(subscriptionType: $subscriptionType, proration: $proration) ${PRORATION_TYPE}
  }
`;

export const calculateProrationGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CALCULATE_SUBSCRIPTION,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.calculateProration;
  } catch (err) {
    throw err;
  }
}