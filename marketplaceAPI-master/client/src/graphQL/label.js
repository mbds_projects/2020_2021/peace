import gql from "graphql-tag";
import { client, clientUpload } from '../graphQL'
import { authError } from '../shared/utils/error'

const LABEL = `{
  id
  name
  pictureUrl
  createdAt
  updatedAt
}`

const LABELS = gql `
  query labels {
    labels ${LABEL}
  }
`;

const UPLOAD_LOGO = gql `
  mutation updateLogoLabel($file: Upload!, $id: ID) {
    updateLogoLabel(file: $file, id: $id)
  }
`;

const UPDATE_NAME = gql `
  mutation updateLabelName($name: String, $id: ID) {
    updateLabelName(name: $name, id: $id)
  }
`;

const CREATE_LABEL = gql `
  mutation createLabel($name: String, $file: Upload!) {
    createLabel(name: $name, file: $file) 
  }
`;

export const labelsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: LABELS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.labels;
  } catch (err) {
    throw err;
  }
}

export const uploadLogoGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: UPLOAD_LOGO,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateLogoLabel;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const updateNameGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_NAME,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateLabelName;
  } catch (err) {
    //authError(err.message);
    throw err;
  }
}

export const createLabelGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: CREATE_LABEL,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createLabel;
  } catch (err) {
    //authError(err.message);
    throw err;
  }
}