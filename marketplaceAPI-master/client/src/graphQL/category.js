import gql from "graphql-tag";
import { client, clientUpload } from '../graphQL'
import { authError } from '../shared/utils/error'

const CATEGORY = `{
  id
  name
  used
  lang {
    locale
    value
  }
  pictureUrl
  createdAt
  updatedAt
}`

const CATEGORIES = gql `
  query categories {
    categories ${CATEGORY}
  }
`;

const CATEGORIES_USED = gql `
  query categoriesUsed {
    categoriesUsed ${CATEGORY}
  }
`;

const UPDATE_CATEGORY_DESC = gql `
  mutation updateCategoryDesc($descFr: String, $descEn: String, $id:ID) {
    updateCategoryDesc(descFr: $descFr, descEn : $descEn, id: $id)
  }
`;

const UPDATE_CATEGORY_PICTURE = gql `
  mutation updatePictureCategory($file: Upload, $id: ID) {
    updatePictureCategory(file: $file, id: $id)
  }
`;

export const categoriesGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: CATEGORIES,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.categories;
  } catch (err) {
    throw err;
  }
}

export const categoriesUsedGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: CATEGORIES_USED,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.categoriesUsed;
  } catch (err) {
    throw err;
  }
}

export const uploadPictureGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: UPDATE_CATEGORY_PICTURE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updatePictureCategory;
  } catch (err) {
    throw err;
  }
}

export const updateDescGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_CATEGORY_DESC,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateCategoryDesc;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}