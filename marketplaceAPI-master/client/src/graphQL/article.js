import gql from "graphql-tag";
import { client } from '../graphQL'

const ARTICLE = `{
  link
  title
  media
}`

const ARTICLES = gql `
  query articles {
    articles ${ARTICLE}
  }
`;

export const articlesGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: ARTICLES,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.articles;
  } catch (err) {
    throw err;
  }
}

const ARTICLES_B2B2 = gql `
  query articlesB2B {
    articlesB2B ${ARTICLE}
  }
`;

export const articlesB2BGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: ARTICLES_B2B2,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.articlesB2B;
  } catch (err) {
    throw err;
  }
}