import gql from "graphql-tag";
import { client, clientUpload } from '../graphQL'
import { authError } from '../shared/utils/error'

const USER_TYPE_TYPE = `{
  isUser
  isFacebookUser
  isVendor
}`


const VENDOR_LIMITED = `{
  id
  name
  profilePictureUrl
  comments
  rate
}`


const USER_TYPE = `{
  id
  mail
  name
  surname
  favorites
  profilePictureUrl
  defaultColor
  role
  blockedSince
  reported
  birthday
  newsletter
  facebookId
  keywords { 
    id 
    name
    lang {
      locale
      value
    }
  }
  achievements {
    id
    type
    value
    icon
    level
    subtype
    activated
    link
  }
  rewards
}`

const TOKEN = `{
  token
}`


const COMMENT = `{
  id
  date
  vendor {
    id
  }
}`

const USER = gql `
  query user {
    user ${USER_TYPE}
  }
`;

const AUTHENTICATE = gql `
  mutation authenticate($mail: String!, $password: String!) {
    authenticate(mail: $mail, password: $password) ${TOKEN}
  }
`;

const FACEBOOK_AUTHENTICATE = gql `
  mutation facebookAuthenticate($userID: String!, $accessToken: String!) {
    facebookAuthenticate(userID: $userID, accessToken: $accessToken) ${TOKEN}
  }
`;



const UPDATE = gql `
  mutation updateUser($input: _User!) {
    updateUser( input : $input ) ${USER_TYPE}
  }
`;

const UPLOAD_PICTURE = gql `
  mutation updateProfilePictureUser($file: Upload!) {
    updateProfilePictureUser(file: $file)
  }
`;

const UPDATE_PASSWORD = gql `
  mutation updatePasswordUser($input: _UserPassword!) {
    updatePasswordUser(input: $input)
  }
`;

const FORGOT_PASSWORD = gql `
  mutation forgotPassword($mail: String!) {
    forgotPassword(mail: $mail)
  }
`;

const UPDATE__FORGOTTEN_PASSWORD = gql `
  mutation updateForgottenPasswordUser($input: _UserForgotPassword!) {
    updateForgottenPasswordUser(input: $input) ${TOKEN}
  }
`;

const USER_COMMENTS = gql `
  query userComments {
    userComments ${COMMENT}
  }
`;

const REPORT_USER = gql `
  mutation reportUser($userId: ID!) {
    reportUser(userId: $userId)
  }
`;

const USERS_REPORTED = gql `
  query usersReported($minReport: Int) {
    usersReported(minReport: $minReport) ${USER_TYPE}
  }
`;

const BLOCK_USER = gql `
  mutation blockUser($userId: ID!) {
    blockUser(userId: $userId)
  }
`;

const USERS_BLOCKED = gql `
  query usersBlocked {
    usersBlocked ${USER_TYPE}
  }
`;

const UNBLOCK_USER = gql `
  mutation unblockUser($userId: ID!) {
    unblockUser(userId: $userId)
  }
`;

const USER_FAVORITES = gql `
  query userFavorites {
    userFavorites ${VENDOR_LIMITED}
  }
`;

const CHECK_NEWSLETTER = gql `
  query checkNewsletter($mail: String!) {
    checkNewsletter (mail: $mail)
  }
`;

const SET_NEWSLETTER = gql `
  mutation setNewsletter($mail: String!) {
    setNewsletter (mail: $mail)
  }
`;

export const userGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: USER,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.user;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}


export const authenticateGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: AUTHENTICATE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.authenticate;
  } catch (err) {
    throw err;
  }
}


export const facebookAuthenticateGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: FACEBOOK_AUTHENTICATE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.facebookAuthenticate;
  } catch (err) {
    throw err;
  }
}

const CREATE = gql `
  mutation createUser($input: _UserCreation!) {
    createUser( input : $input ) ${TOKEN}
  }
`;

export const createUserGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createUser;
  } catch (err) {
    throw err;
  }
}

const CREATE_FACEBOOK = gql `
  mutation createUserFacebook($input: _UserCreationFacebook!) {
    createUserFacebook( input : $input ) ${TOKEN}
  }
`;

export const createUserFacebookGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_FACEBOOK,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createUserFacebook;
  } catch (err) {
    throw err;
  }
}

export const updateUserGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateUser;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const uploadPictureGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: UPLOAD_PICTURE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateProfilePictureUser;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const updatePasswordGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_PASSWORD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updatePasswordUser;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const forgotPasswordGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: FORGOT_PASSWORD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.forgotPassword;
  } catch (err) {
    throw err;
  }
}

export const updateForgottenPasswordGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE__FORGOTTEN_PASSWORD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateForgottenPasswordUser;
  } catch (err) {
    throw err;
  }
}

export const userCommentsGql = async(variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: USER_COMMENTS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.userComments;
  } catch (err) {
    //authError(err.message);
    throw err;
  }
}

export const reportUserGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: REPORT_USER,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.reportUser;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}


export const usersReportedGql = async(variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: USERS_REPORTED,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.usersReported;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const blockUserGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: BLOCK_USER,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.blockUser;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const usersBlockedGql = async(variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: USERS_BLOCKED,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.usersBlocked;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const unblockUserGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UNBLOCK_USER,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.unblockUser;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}


export const userFavoritesGql = async(variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: USER_FAVORITES,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.userFavorites;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const checkNewsletterGql = async(variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: CHECK_NEWSLETTER,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.checkNewsletter;
  } catch (err) {
    throw err;
  }
}

export const setNewsletterGql = async(variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: SET_NEWSLETTER,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.setNewsletter;
  } catch (err) {
    throw err;
  }
}

const IS_FACEBOOK = gql `
  query isFacebookAccountRegistered($userID: String!) {
    isFacebookAccountRegistered(userID: $userID)
  }
`;

export const isFacebookAccountRegisteredGql = async(variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: IS_FACEBOOK,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.isFacebookAccountRegistered;
  } catch (err) {
    throw err;
  }
}


const IS_REGISTERED = gql `
  query isAccountRegistered($mail: String!) {
    isAccountRegistered(mail: $mail) ${USER_TYPE_TYPE}
  }
`;

export const isAccountRegisteredGql = async(variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: IS_REGISTERED,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.isAccountRegistered;
  } catch (err) {
    throw err;
  }
}