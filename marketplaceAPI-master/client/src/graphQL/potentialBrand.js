import gql from "graphql-tag";
import { client } from '../graphQL'

import { authError } from '../shared/utils/error'

const POTENTIAL_BRAND_TYPE = `{
  id
  name
  categories
  keywords
  note
  mail
  site
  cities
  createdAt
  updatedAt
  state
}`


const POTENTIAL_BRANDS = gql `
  query potentialBrands {
    potentialBrands ${POTENTIAL_BRAND_TYPE}
  }
`;

const CREATE_POTENTIAL_BRAND = gql `
  mutation createPotentialBrand($input: _PotentialBrand) {
    createPotentialBrand(input: $input) ${POTENTIAL_BRAND_TYPE}
  }
`;

const UPDATE_POTENTIAL_BRAND = gql `
  mutation updatePotentialBrand($input: _PotentialBrandUpdate) {
    updatePotentialBrand(input: $input)
  }
`;

const DELETE_POTENTIAL_BRAND = gql `
  mutation deletePotentialBrand($potentialBrandId: ID!) {
    deletePotentialBrand(potentialBrandId: $potentialBrandId)
  }
`;

export const potentialBrandsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: POTENTIAL_BRANDS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.potentialBrands;
  } catch (err) {
    throw err;
  }
}

export const createPotentialBrandGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_POTENTIAL_BRAND,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createPotentialBrand;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const updatePotentialBrandGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_POTENTIAL_BRAND,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updatePotentialBrand;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const deletePotentialBrandGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: DELETE_POTENTIAL_BRAND,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.deletePotentialBrand;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}