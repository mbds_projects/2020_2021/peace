import gql from "graphql-tag";
import { client } from '../graphQL'
import _ from 'lodash'

const PARTNER = `{
  link
  imgUrl
}`

const PARTNERS = gql `
  query partners {
    partners ${PARTNER}
  }
`;

export const partnersGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: PARTNERS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.partners;
  } catch (err) {
    throw err;
  }
}