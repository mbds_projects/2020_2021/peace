import gql from "graphql-tag";
import { client } from '../graphQL'

const REWARD = `{
  id
  message
  vendor {
    id
    name
    profilePictureUrl
    categories
    genders
    keywords
    onlineShop
    clickAndCollect
    published
  }
  validity
  type
  value
  code
  activated
}`


export const REWARDS = gql `
  query rewards {
    rewards ${REWARD}
  }
`;




export const rewardsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: REWARDS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.rewards;
  } catch (err) {
    throw err;
  }
}


export const REWARDS_OF_VENDOR = gql `
  query rewardsOfVendor {
    rewardsOfVendor ${REWARD}
  }
`;

export const rewardsOfVendorGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: REWARDS_OF_VENDOR,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.rewardsOfVendor;
  } catch (err) {
    throw err;
  }
}


export const REWARDS_ADMIN_VENDOR = gql `
  query rewardsAdminVendor($vendorId: ID) {
    rewardsAdminVendor(vendorId: $vendorId) ${REWARD}
  }
`;

export const rewardsAdminVendorGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: REWARDS_ADMIN_VENDOR,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.rewardsAdminVendor;
  } catch (err) {
    throw err;
  }
}

export const CREATE_REWARD = gql `
  mutation createReward($input: _Reward){
    createReward(input:$input)
  }
`;

export const createRewardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_REWARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createReward;
  } catch (err) {
    throw err;
  }
}

export const CREATE_REWARD_ADMIN = gql `
  mutation createRewardAdmin($input: _Reward, $vendorId: ID){
    createRewardAdmin(input:$input, vendorId: $vendorId)
  }
`;

export const createRewardAdminGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_REWARD_ADMIN,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createRewardAdmin;
  } catch (err) {
    throw err;
  }
}

export const DISABLE_REWARD = gql `
  mutation disableReward($rewardId: ID){
    disableReward(rewardId:$rewardId)
  }
`;



export const disableRewardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: DISABLE_REWARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.disableReward;
  } catch (err) {
    throw err;
  }
}

export const ENABLE_REWARD = gql `
  mutation enableReward($rewardId: ID){
    enableReward(rewardId:$rewardId)
  }
`;

export const enableRewardGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: ENABLE_REWARD,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.enableReward;
  } catch (err) {
    throw err;
  }
}