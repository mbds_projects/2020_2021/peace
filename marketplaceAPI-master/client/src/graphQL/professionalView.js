import gql from "graphql-tag";
import { client } from '../graphQL'

const PROFESSIONAL_VIEW = `{
  id
  text
  vendor {
    id
    name
    comments {
      rate
    }
    profilePictureUrl
  }
  date
  rate
}`


export const PROFESSIONAL_VIEWS = gql `
  query professionalViews($minRate: Int!) {
    professionalViews(minRate: $minRate) ${PROFESSIONAL_VIEW}
  }
`;


export const professionalViewsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: PROFESSIONAL_VIEWS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.professionalViews;
  } catch (err) {
    throw err;
  }
}