import gql from "graphql-tag";
import { client, clientUpload } from '../graphQL'
import { authError } from '../shared/utils/error'

const VENDOR_LIMITED_TYPE= `{
  id
  name
  profilePictureUrl
  logoUrl
  comments
  rate
  categories
  genders
  keywords
  labels
  onlineShop
  clickAndCollect
  activated
  published
}`

const VENDOR_ACCOUNT = gql `
  query vendorAccount {
    vendorAccount ${VENDOR_LIMITED_TYPE}
  }
`;

export const vendorAccountGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: VENDOR_ACCOUNT,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.vendorAccount;
  } catch (err) {
    throw err;
  }
}

const RETRIEVE_ALL_INVOICES = gql`
  query retrieveAllInvoices {
    retrieveAllInvoices {
        id
        amount_paid
        currency
        period_start
        period_end
        invoice_pdf
    }
  }
`;

const UPLOAD_PROFILE_PICTURE = gql `
  mutation updateProfilePictureVendor($file: Upload!) {
    updateProfilePictureVendor(file: $file)
  }
`;


const UPLOAD_LOGO = gql `
  mutation updateLogoVendor($file: Upload!) {
    updateLogoVendor(file: $file)
  }
`;

const UPDATE_DELIVER_INFO = gql `
  mutation updateDeliverInfoVendor($input: _DeliverInfoVendor) {
    updateDeliverInfoVendor(input: $input)
  }
`;

export const retrieveAllInvoicesGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: RETRIEVE_ALL_INVOICES,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.retrieveAllInvoices;
  } catch (err) {
    throw err;
  }
}

export const uploadProfilePictureGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: UPLOAD_PROFILE_PICTURE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateProfilePictureVendor;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const uploadLogoGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: UPLOAD_LOGO,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateLogoVendor;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}



export const updateDeliverInfoVendorGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_DELIVER_INFO,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateDeliverInfoVendor;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}
