import gql from "graphql-tag";
import { client } from '../graphQL'

const YOUTUBE = `{
  link
  media
  title
}`

const YOUTUBE_RECENT = gql `
  query youtubeRecent {
    youtubeRecent ${YOUTUBE}
  }
`;

export const youtubeRecentGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: YOUTUBE_RECENT,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.youtubeRecent;
  } catch (err) {
    throw err;
  }
}