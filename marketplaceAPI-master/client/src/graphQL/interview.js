import gql from "graphql-tag";
import { client } from '../graphQL'

const INTERVIEW = `{
  author
  title
  subTitle
  link
  imgUrl
  date
  type
}`

const INTERVIEWS = gql `
  query interviews {
    interviews ${INTERVIEW}
  }
`;

export const interviewsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: INTERVIEWS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.interviews;
  } catch (err) {
    throw err;
  }
}