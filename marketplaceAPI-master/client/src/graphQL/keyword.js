import gql from "graphql-tag";
import { client, clientUpload } from '../graphQL'

import { authError } from '../shared/utils/error'

const KEYWORD = `{
  id
  name
  used
  lang {
    locale
    value
  }
  pictureUrl
  moral {
    name
    lang {
      locale
      value
    }
  }
  createdAt
  updatedAt
}`

const KEYWORDS = gql `
  query keywords {
    keywords ${KEYWORD}
  }
`;

const KEYWORDS_USED = gql `
  query keywordsUsed {
    keywordsUsed ${KEYWORD}
  }
`;

const KEYWORDS_POPULAR = gql `
  query keywordsPopular {
    keywordsPopular ${KEYWORD}
  }
`;


const UPDATE_KEYWORD_DESC = gql `
  mutation updateKeywordDesc($descFr: String, $descEn: String, $id:ID) {
    updateKeywordDesc(descFr: $descFr, descEn : $descEn, id: $id)
  }
`;

const UPDATE_KEYWORD_PICTURE = gql `
  mutation updatePictureKeyword($file: Upload, $id: ID) {
    updatePictureKeyword(file: $file, id: $id)
  }
`;


export const keywordsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: KEYWORDS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.keywords;
  } catch (err) {
    throw err;
  }
}

export const keywordsUsedGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: KEYWORDS_USED,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.keywordsUsed;
  } catch (err) {
    throw err;
  }
}

export const keywordsPopularGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: KEYWORDS_POPULAR,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.keywordsPopular;
  } catch (err) {
    throw err;
  }
}

export const uploadPictureGql = async (variables) => {
  try {
    let { data } = await clientUpload.mutate({
      variables: variables,
      mutation: UPDATE_KEYWORD_PICTURE,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updatePictureKeyword;
  } catch (err) {
    //authError(err.message);
    throw err;
  }
}

export const updateDescGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: UPDATE_KEYWORD_DESC,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.updateKeywordDesc;
  } catch (err) {
    //authError(err.message);
    throw err;
  }
}