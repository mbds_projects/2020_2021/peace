import gql from "graphql-tag";
import { client } from '../graphQL'
import { authError } from '../shared/utils/error'

const COMMENT_EXTENDED = `{
  id
  text
  postedBy {
    id
    name
    surname
    defaultColor
    profilePictureUrl
  }
  reportedBy
  vendor {
    name
    id
  }
  date
  rate
}`

const COMMENTS_BY_VENDOR = gql `
  query commentsByVendor($vendorId: ID!, $page: Int!) {
    commentsByVendor(vendorId: $vendorId, page: $page) ${COMMENT_EXTENDED}
  }
`;

const REPORT_COMMENT = gql `
query reportComment($commentId: ID!) {
  reportComment(commentId: $commentId) ${COMMENT_EXTENDED}
  }
`;

const COMMENTS_REPORTED = gql `
query commentsReported {
  commentsReported ${COMMENT_EXTENDED}
  }
`;

const CREATE_COMMENT = gql `
mutation createComment($input: _Comment!) {
    createComment(input: $input) ${COMMENT_EXTENDED}
  }
`;

const DELETE_COMMENT = gql `
mutation deleteComment($commentId: ID!) {
    deleteComment(commentId: $commentId)
  }
`;

const BEST_COMMENTS = gql `
query bestComments {
  bestComments ${COMMENT_EXTENDED}
  }
`;




export const commentsByVendorGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: COMMENTS_BY_VENDOR,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.commentsByVendor;
  } catch (err) {
    throw err;
  }
}

export const reportCommentGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: REPORT_COMMENT,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.reportComment;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const commentsReportedGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: COMMENTS_REPORTED,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.commentsReported;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}


export const bestCommentsGql = async (variables) => {
  try {
    let { data } = await client.query({
      variables: variables,
      query: BEST_COMMENTS,
      fetchPolicy: 'network-only', // skip the cache
    });
    return data.bestComments;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const createCommentGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: CREATE_COMMENT,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.createComment;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}

export const deleteCommentGql = async (variables) => {
  try {
    let { data } = await client.mutate({
      variables: variables,
      mutation: DELETE_COMMENT,
      fetchPolicy: 'no-cache', // skip the cache
    });
    return data.deleteComment;
  } catch (err) {
    authError(err.message);
    //throw err;
  }
}