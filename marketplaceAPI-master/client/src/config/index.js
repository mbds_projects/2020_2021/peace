import {prod} from './env/prod';
import {dev} from './env/dev';
import {local} from './env/local';

export const ENV = process.env.REACT_APP_ENV || 'local'

var config = local;

switch(ENV) {
    case "production": 
        config = prod;
        break;
    case "prod": 
        config = prod;
        break;
    case "dev":
        config = dev;
        break;
    case "development":
        config = dev;
        break;
    default:
        config = local;
        break;
}

export const CONF = config;