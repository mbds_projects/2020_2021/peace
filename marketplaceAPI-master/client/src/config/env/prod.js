let graphQLUrl = `${window.location.protocol}//${window.location.hostname}/api/public/graphql`;

export const prod = {
    graphQL: graphQLUrl,
    sitekey: process.env.REACT_APP_SITEKEY,
    GA: process.env.REACT_APP_GA,
    stripePK: process.env.REACT_APP_STRIPEPK,
}