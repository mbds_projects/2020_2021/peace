import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Loadable from "react-loadable";

import Loading from './pages/Loading/LoadingPage';
import GuestRoute from './components/GuestRoute/GuestRoute';
import UserRoute from './components/UserRoute/UserRoute';
import AdminRoute from './components/AdminRoute/AdminRoute';
import VendorRoute from './components/VendorRoute/VendorRoute';

const Home = Loadable({ loader: () => Promise.resolve(require('./pages/Home/HomePageContainer')), loading: Loading });
const VendorList = Loadable({ loader: () => Promise.resolve(require('./pages/VendorList/VendorListPageContainer')), loading: Loading });
const UserProfile = Loadable({ loader: () => Promise.resolve(require('./pages/UserProfile/UserProfilePageContainer')), loading: Loading });
const VendorProfile = Loadable({ loader: () => Promise.resolve(require('./pages/VendorProfile/VendorProfilePageContainer')), loading: Loading });
//const Error404 = Loadable({ loader: () => Promise.resolve(require('./pages/Error404/Error404Page')), loading: Loading });

const UserFavorite = Loadable({ loader: () => Promise.resolve(require('./pages/UserFavorite/UserFavoritePageContainer')), loading: Loading });
const UserReward = Loadable({ loader: () => Promise.resolve(require('./pages/UserReward/UserRewardPageContainer')), loading: Loading });
const UserSuggestedForYou = Loadable({ loader: () => Promise.resolve(require('./pages/UserSuggestedForYou/UserSuggestedForYouPageContainer')), loading: Loading });

const Login = Loadable({ loader: () => Promise.resolve(require('./pages/Login/LoginPageContainer')), loading: Loading });
const Signin = Loadable({ loader: () => Promise.resolve(require('./pages/Signin/SigninPageContainer')), loading: Loading });

const ForgotPassword = Loadable({ loader: () => Promise.resolve(require('./pages/ForgotPassword/ForgotPasswordPageContainer')), loading: Loading });
const PasswordRecreate = Loadable({ loader: () => Promise.resolve(require('./pages/PasswordRecreate/PasswordRecreatePageContainer')), loading: Loading });

const SuggestBrand = Loadable({ loader: () => Promise.resolve(require('./pages/SuggestBrand/SuggestBrandPageContainer')), loading: Loading });

const AdminDashboard = Loadable({ loader: () => Promise.resolve(require('./pages/AdminDashboard/AdminDashboardPageContainer')), loading: Loading });
const ContactUs = Loadable({ loader: () => Promise.resolve(require('./pages/ContactUs/ContactUsPageContainer')), loading: Loading });

const VendorCandidate = Loadable({ loader: () => Promise.resolve(require('./pages/VendorCandidate/VendorCandidatePageContainer')), loading: Loading });
const TalkAboutUs = Loadable({ loader: () => Promise.resolve(require('./pages/TalkAboutUs/TalkAboutUsPageContainer')), loading: Loading });
//const PartnerList = Loadable({ loader: () => Promise.resolve(require('./pages/PartnerList/PartnerListPageContainer')), loading: Loading });
const AboutUs = Loadable({ loader: () => Promise.resolve(require('./pages/AboutUs/AboutUsPageContainer')), loading: Loading });
const CGU = Loadable({ loader: () => Promise.resolve(require('./pages/CGU/CGUPageContainer')), loading: Loading });

const AdminDashboardVendor = Loadable({ loader: () => Promise.resolve(require('./pages/AdminVendorDashboard/AdminVendorDashboardPageContainer')), loading: Loading });
const AdminDashboardVendorCreateToken = Loadable({ loader: () => Promise.resolve(require('./pages/AdminDashboardVendorCreateToken/AdminDashboardVendorCreateTokenPageContainer')), loading: Loading });
const AdminDashboardVendorInfo = Loadable({ loader: () => Promise.resolve(require('./pages/AdminDashboardVendorInfo/AdminDashboardVendorInfoPageContainer')), loading: Loading });
const AdminVendorRewards = Loadable({ loader: () => Promise.resolve(require('./pages/AdminVendorRewards/AdminVendorRewardsPageContainer')), loading: Loading });
const AdminDashboardBrand = Loadable({ loader: () => Promise.resolve(require('./pages/AdminDashboardBrand/AdminDashboardBrandPageContainer')), loading: Loading });
const AdminDashboardUser = Loadable({ loader: () => Promise.resolve(require('./pages/AdminDashboardUser/AdminDashboardUserPageContainer')), loading: Loading });
const AdminDashboardLabel = Loadable({ loader: () => Promise.resolve(require('./pages/AdminDashboardLabel/AdminDashboardLabelPageContainer')), loading: Loading });
const AdminDashboardLocation = Loadable({ loader: () => Promise.resolve(require('./pages/AdminDashboardLocation/AdminDashboardLocationPageContainer')), loading: Loading });
const AdminDashboardCategory = Loadable({ loader: () => Promise.resolve(require('./pages/AdminDashboardCategory/AdminDashboardCategoryPageContainer')), loading: Loading });
const AdminDashboardKeyword = Loadable({ loader: () => Promise.resolve(require('./pages/AdminDashboardKeyword/AdminDashboardKeywordPageContainer')), loading: Loading });
const AdminDashboardGender = Loadable({ loader: () => Promise.resolve(require('./pages/AdminDashboardGender/AdminDashboardGenderPageContainer')), loading: Loading });
const AdminDashboardVendorCandidate = Loadable({ loader: () => Promise.resolve(require('./pages/AdminDashboardVendorCandidate/AdminDashboardVendorCandidatePageContainer')), loading: Loading });
const DashboardVendor = Loadable({ loader: () => Promise.resolve(require('./pages/VendorDashboard/VendorDashboardPageContainer')), loading: Loading });
const LoginVendor = Loadable({ loader: () => Promise.resolve(require('./pages/LoginVendor/LoginVendorPageContainer')), loading: Loading });
const ForgotPasswordVendor = Loadable({ loader: () => Promise.resolve(require('./pages/ForgotPasswordVendor/ForgotPasswordVendorPageContainer')), loading: Loading });
const VendorPasswordCreate = Loadable({ loader: () => Promise.resolve(require('./pages/VendorPasswordCreate/VendorPasswordCreatePageContainer')), loading: Loading });

const CampaignAchieved = Loadable({ loader: () => Promise.resolve(require('./pages/CampaignAchieved/CampaignAchievedPage')), loading: Loading });

const SubscribeDashboard = Loadable({ loader: () => Promise.resolve(require('./pages/SubscribeDashboard/SubscribeDashboardPageContainer')), loading: Loading });
const VendorAccount = Loadable({ loader: () => Promise.resolve(require('./pages/VendorAccount/VendorAccountPageContainer')), loading: Loading });
const VendorRewards = Loadable({ loader: () => Promise.resolve(require('./pages/VendorRewards/VendorRewardsPageContainer')), loading: Loading });
const VendorTutorials = Loadable({ loader: () => Promise.resolve(require('./pages/VendorTutorials/VendorTutorialsPageContainer')), loading: Loading });
const RGPD = Loadable({ loader: () => Promise.resolve(require('./pages/RGPD/RGPDPageContainer')), loading: Loading });
const Cookies = Loadable({ loader: () => Promise.resolve(require('./pages/Cookies/CookiesPageContainer')), loading: Loading });

const vendorsendinfo = Loadable({ loader: () => Promise.resolve(require('./pages/VendorSendInfo/VendorSendInfoPageContainer')), loading: Loading });

const result = 
    <Switch>
        <Route exact path="/" component={Home} />
        <GuestRoute redirectionPath={'/'} exact path="/login" component={Login} />
        <GuestRoute redirectionPath={'/'} exact path="/signin" component={Signin} />
        <GuestRoute redirectionPath={'/vendor/dashboard'} exact path="/loginVendor" component={LoginVendor} />
        <GuestRoute redirectionPath={'/'} exact path="/forgotPassword" component={ForgotPassword} />
        <GuestRoute redirectionPath={'/'} exact path="/vendorsendinfo/:id" component={vendorsendinfo} />
        <GuestRoute redirectionPath={'/'} exact path="/createForgottenPassword/:id" component={PasswordRecreate} />
        <GuestRoute redirectionPath={'/'} exact path="/forgotPasswordVendor" component={ForgotPasswordVendor} />
        <GuestRoute redirectionPath={'/'} exact path="/createForgottenPasswordVendor/:id" component={VendorPasswordCreate} />
        <GuestRoute redirectionPath={'/vendor/account'} exact path="/createVendorPassword/:id" component={VendorPasswordCreate} />
        <Route path="/contactUs" component={ContactUs} />                  
        <Route path="/aboutUs" component={AboutUs} />              
        <Route path="/cgu" component={CGU} />
        <Route path="/talkAboutUs" component={TalkAboutUs} />
        {/*<Route path="/partners" component={PartnerList} />*/}
        <Route exact path="/vendors" component={VendorList} />
        <Route exact path="/vendorCandidate" component={VendorCandidate} />
        <UserRoute exact path="/user/" component={UserProfile} />
        <Route exact path="/suggestBrand/" component={SuggestBrand} />
        <Route path="/vendors/:id" component={VendorProfile} />
        <Route exact path="/user/favorites" component={UserFavorite} />
        <Route exact path="/user/rewards" component={UserReward} />
        <Route exact path="/user/suggestedForYou" component={UserSuggestedForYou} />
        <AdminRoute redirectionPath={'/login'} exact path="/admin" component={AdminDashboard} />
        <AdminRoute redirectionPath={'/login'} exact path="/admin/vendor" component={AdminDashboardVendor} />
        <AdminRoute redirectionPath={'/login'} exact path="/admin/vendorToken" component={AdminDashboardVendorCreateToken} />
        <AdminRoute redirectionPath={'/login'} exact path="/admin/vendorInfo" component={AdminDashboardVendorInfo} />
        <AdminRoute redirectionPath={'/login'} exact path="/admin/vendorRewards" component={AdminVendorRewards} />
        <AdminRoute redirectionPath={'/login'} exact path="/admin/brand" component={AdminDashboardBrand} />
        <AdminRoute redirectionPath={'/login'} exact path="/admin/user" component={AdminDashboardUser} />
        <AdminRoute redirectionPath={'/login'} exact path="/admin/label" component={AdminDashboardLabel} />
        <AdminRoute redirectionPath={'/login'} exact path="/admin/location" component={AdminDashboardLocation} />
        <AdminRoute redirectionPath={'/login'} exact path="/admin/category" component={AdminDashboardCategory} />
        <AdminRoute redirectionPath={'/login'} exact path="/admin/keyword" component={AdminDashboardKeyword} />
        <AdminRoute redirectionPath={'/login'} exact path="/admin/gender" component={AdminDashboardGender} />
        <AdminRoute redirectionPath={'/login'} exact path="/admin/vendorCandidate" component={AdminDashboardVendorCandidate} />
        <VendorRoute redirectionPath={'/'} exact path="/vendor/dashboard" component={DashboardVendor} />
        <VendorRoute redirectionPath={'/'} exact path="/vendor/account" component={VendorAccount} />
        <VendorRoute redirectionPath={'/'} exact path="/vendor/rewards" component={VendorRewards} />
        <VendorRoute redirectionPath={'/'} exact path="/vendor/tutorials" component={VendorTutorials} />
        <Route path="/rgpd" component={RGPD} />
        <Route path="/cookies" component={Cookies} />
        <Route path="/landing-page" component={CampaignAchieved} />
        <Route path="/on-passe-a-l-action" component={CampaignAchieved} />
        <Route path="/subscribe" component={SubscribeDashboard} />
        <Route component={Home} />
    </Switch>

export default result;

export function Routes() { return result;}