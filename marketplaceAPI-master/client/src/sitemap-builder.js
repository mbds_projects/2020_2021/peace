require("babel-register")({
    presets: ["es2015", "react"]
  });
   
  const fs = require('fs');

  const router = require("./routes").default;
  const Sitemap = require("react-router-sitemap").default;

  
  let vendorIds = [];
  try {
    if (fs.existsSync('./public/vendors.json')) {
      let vendorIdsJson = fs.readFileSync('./public/vendors.json');
      vendorIds = JSON.parse(vendorIdsJson);
    }
  } catch(err) {
  }
  
  function generateSitemap() {
    const pathsConfig = {
        '/vendors/:id': [
            {
                id: vendorIds
            }
        ]
    };
      return (
        new Sitemap(router)
            .applyParams(pathsConfig)
            .build("https://www.super-responsable.org")
            .save("./public/sitemap.xml")
      );
  }
  
  if(vendorIds.length > 0) generateSitemap();