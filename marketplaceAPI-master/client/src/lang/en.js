export const en = {
    home: {
        latestVendorsAdded: 'Latest additions',
        aroundPositionVendors: 'Around you',
        aroundNice: 'Around Nice',
        genders: "Genders",
        categories: "Categories",
        bestRateVendors: 'More visited',
        moreFavoritesVendors: 'Favorites of the community',
        vendorsAssimilate: 'Recommended for you',
        articles: "Lastest articles",
        youtube: "Lastest videos",
        charter: {
            title: "Our values",
            text: "We highlight the brands engaged among 50 shades of healthy, ethical and sustainable values. Discover them in ",
            link: "our responsible charter"
        },
        fbGroup: {
            title: "Super Responsable Behind the Scenes",
            text: "This community site is also yours! Join the ",
            text2: " Facebook group to make your mark on the site, interact directly with the team and take part in our surveys. See you soon behind the scenes",
            link: "Super Responsable Behind the Scenes"
        },
        instaRecent: "Last publications",
        keywords: "The community trend",
        bestComments: "Best comments"
    },
    cgu: {
        title: "Terms of services",
        date: "Effective at 01/07/2020",
        def: `The present general conditions of use (known as “<0>CGU</0>”) have as their object the legal framework of the modalities of provision of the site and the services by En tant qu’utilisateur, comment puis-je être récompensé and to define the conditions of access and use of the services by “ <0>the user</0> ".».
        <1/><1/>These T & Cs are available on the site under the "<0>T & Cs</0>" section..
        <1/><1/>Any registration or use of the site implies acceptance without any reserve or restriction of these T & Cs by the user. When registering on the site via the Registration Form, each user expressly accepts these T & Cs by checking the box before the following text: "I acknowledge having read and understood the T & Cs and I accept them".
        <1/><1/>In the event of non-acceptance of the T & Cs stipulated in this contract, the User must renounce access to the services offered by the site.
        <1/><1/>https://super-responsable.org/ reserves the right to modify the content of these T & Cs unilaterally and at any time.`,
        legalNotice: {
            title: "Article 1: Legal notices",
            def: `The https://super-responsable.org/ website is edited by the SAS Super Responsable company in the process of being created, with its head office located at Business Pôle - Bt A - Bureau 209 1047 route des Dolines. Allée Pierre Ziller, 06560 Sophia Antipolis
            <0/><0/>E-mail address: bonjour@super-responsable.org.
            <0/><0/>The Director of publication is: Alexandra Tence
            <0/><0/>The host of the site https://super-responsable.org/ is the company Heroku Incorporated, whose head office is located at 650 7th Street, San Francisco, with the telephone number: +33 1 (877) 563-4311 .`
        },
        webSiteAccess: {
            title: "ARTICLE 2: Access to the site",
            def: `The https://super-responsable.org/ site allows the User free access to the following services:
            <0/><0/>The website offers the following services:
            <0/><0/>Discovering businesses corresponds to a certain number of criteria based on a selection based on the site's responsible charter, Discovering businesses around you based on geolocation, Create a user account, Put businesses in favorites, Complete your profile with your values and its information in order to obtain suggestions for businesses based on its values, Suggest businesses as a registered user for them to be studied, Suggest your own business as a brand, Discover the business listings containing visual information and links, Comment and give your opinion on a business as a registered user, Subscribe and unsubscribe to the news letter
            <0/><0/>The site is accessible free of charge to any User with access to the Internet. All costs incurred by the User to access the service (computer hardware, software, Internet connection, etc.) are his responsibility.
            <0/><0/>The non-member User does not have access to the reserved services. For this, he must register by filling out the form. By agreeing to subscribe to the reserved services, the User Member undertakes to provide sincere and exact information concerning his civil status and his contact details, in particular his email address.
            <0/><0/>To access the services, the User must then identify himself using his username and password used when registering.
            <0/><0/>Any regularly registered User Member may also request unsubscription by going to the contact page. This will be effective within a reasonable time.
            <0/><0/>Any event due to a case of force majeure resulting in a malfunction of the site or server and subject to any interruption or modification in the event of maintenance, does not engage the responsibility of https://super-responsable.org/. In these cases, the User agrees not to hold the publisher accountable for any interruption or suspension of service, even without notice.
            <0/><0/>The User has the possibility of contacting the site by electronic mail at the email address of the publisher communicated in ARTICLE 1.`
        },
        rgpd: {
            title: "ARTICLE 3: Data collection",
            def: `The site provides the User with the collection and processing of personal information with respect for privacy in accordance with Law No. 78-17 of January 6, 1978 relating to data processing, files and freedoms. In accordance with the GDPR directives in force since May 25, 2018, here is the processing register:
            <0/><0/>The company Super Responsable is the only one involved in the processing of data relating to personal information. These data are: surname, first name, email address, registration password and date of birth. This data is used to contact the user, as well as to compile statistics on the age groups of the users. No personal data is communicated and only the Super Responsable company has access to it. They are kept as long as the user account is active. The data is saved in the database, and the password is encrypted so that it is never visible in clear for anyone, including the company Super Responsable. In this sense, in the event of a forgotten password, a request must be made on the site by clicking on "forgotten password", and the user will receive an email in the following minutes in order to generate a new one. No password can therefore never be provided by email.
            <0/><0/>Under the Data Protection Act, dated January 6, 1978, the User has the right to access, rectify, delete and oppose his personal data. The User exercises this right:
            <0/>· By email to the email address bonjour@super-responsable.org
            <0/>· Via their personal space;`
        },
        credit: {
            title: "ARTICLE 4: Intellectual property",
            def: `The brands, logos, signs as well as all the contents of the site (texts, images, sound ...) are subject to protection by the Code of intellectual property and more particularly by copyright.
            <0/><0/>The Super Responsable brand is a trademark registered by Alexandra Tence. Any representation and / or reproduction and / or partial or total exploitation of this brand, of whatever nature, is totally prohibited.
            <0/><0/>The User must request the prior authorization of the site for any reproduction, publication, copy of the various content. He undertakes to use the contents of the site in a strictly private setting, any use for commercial and advertising purposes is strictly prohibited.
            <0/><0/>Any total or partial representation of this site by any process whatsoever, without the express authorization of the operator of the website would constitute an infringement punishable by article L 335-2 et seq. Of the Intellectual Property Code.
            <0/><0/>It is recalled in accordance with article L122-5 of the Intellectual Property Code that the User who reproduces, copies or publishes the protected content must cite the author and his source.
            <0/><0/>Some media on the site are subject to usage credits:
            <0/>· Some images come from the site https://pixabay.com/fr/ and are under license "Simplified Pixabay License"
            <0/>· Certain images come from the site https://unsplash.com/ and are under license "Unsplash License"
            <0/>· Certain icons come from the site https://www.flaticon.com/ and are under license "designed from Flaticon"`
        },
        responsability: {
            title: "ARTICLE 5: Liability",
            def: `The sources of the information disseminated on the site https://super-responsable.org/ are deemed reliable, but the site does not guarantee that it is free from defects, errors or omissions.
            <0/><0/>The information communicated is presented for information and general purposes without contractual value. Despite regular updates, the https://super-responsable.org/ site cannot be held responsible for any modification of the administrative and legal provisions occurring after publication. Likewise, the site cannot be held responsible for the use and interpretation of the information contained on this site.
            <0/><0/>The User makes sure to keep his password secret. Any disclosure of the password, whatever its form, is prohibited. He assumes the risks linked to the use of his username and password. The site declines all responsibility.
            <0/><0/>The site https://super-responsable.org/ cannot be held responsible for any viruses which could infect the computer or any computer equipment of the Internet user, following use, access or download. from this site.
            <0/><0/>The responsibility of the site cannot be engaged in the event of force majeure or the unforeseeable and insurmountable fact of a third party.`
        },
        link: {
            title: "ARTICLE 6: Hypertext links",
            def: `Hypertext links may be present on the site. The User is informed that by clicking on these links, he will leave the site https://super-responsable.org/. The latter has no control over the web pages to which these links lead and cannot, under any circumstances, be responsible for their content.`
        },
        cookies: {
            title: "ARTICLE 7 : Cookies",
            def: `The User is informed that during his visits to the site, a cookie can be automatically installed on his browser software.
            <0/><0/>Cookies are small files temporarily stored on the hard drive of the User's computer by your browser and which are necessary for the use of the site https://super-responsable.org/. Cookies do not contain personal information and cannot be used to identify someone. A cookie contains a unique identifier, generated randomly and therefore anonymous. Some cookies expire at the end of the User's visit, others remain.
            <0/><0/>The information contained in cookies is used to improve the site https://super-responsable.org/.
            <0/><0/>By browsing the site, the User accepts them.
            <0/><0/>The User can deactivate these cookies through the parameters appearing in their browser software.`
        },
        publishing: {
            title: "ARTICLE 8: Publication by the User",
            def: `The site allows members to publish the following content:
            <0/><0/>Comments and Opinions on a shop.
            <0/><0/>In his publications, the member undertakes to comply with the rules of Netiquette (rules of good Internet behavior) and the rules of law in force.
            <0/><0/>The site can exercise moderation on publications and reserves the right to refuse their posting, without having to justify it to the member.
            <0/><0/>The member remains the holder of all of their intellectual property rights. But by publishing a publication on the site, it gives the publishing company the non-exclusive and free right to represent, reproduce, adapt, modify, distribute and distribute its publication, directly or by an authorized third party, worldwide, on any medium (digital or physical), for the duration of the intellectual property. The Member cedes in particular the right to use its publication on the internet and on mobile telephone networks.
            <0/><0/>The publishing company undertakes to include the name of the member near each use of its publication.
            <0/><0/>Any content posted by the User is his sole responsibility. The User undertakes not to put online content that could harm the interests of third parties. Any legal action brought by an injured third party against the site will be borne by the User.
            <0/><0/>User content can be deleted or modified by the site at any time and for any reason, without notice.`
        },
        law: {
            title: "ARTICLE 9: Applicable law and competent jurisdiction",
            def: `French law applies to this contract. In the absence of an amicable resolution of a dispute between the parties, the French courts will have sole jurisdiction to hear it.
            <0/><0/>For any question relating to the application of these T & Cs, you can contact the publisher using the contact details listed in ARTICLE 1.`
        }
    },
    footer: {
        rgpd: 'GPDR',
        charter: 'The responsible charter',
        aboutUs: 'Who are we',
        candidate: 'Join us',
        partners: 'Our partners',
        talkOfUs: 'They talk about us!',
        blog: 'The Super Blog',
        contact: 'Contact',
        stayTuned: 'Stay tuned',
        suggestBrand: 'Share my address',
        vendorCandidate: "I'm a shop?",
        needHelp: "Need help?",
        faq: "FAQ",
        cgu: "Terms of Services",
        actualities: "Our super blog",
        contribute: "Contribute",
        favorites: "Favorites",
        rewards: "Good deals",
        directory: "Directory"
    },
    navbar: {
        account: "My account",
        search: "Search",
        searchPlaceholder: "Search a brand",
        shops: "Shared addresses",
        blog: "SUPER BLOG",
        aboutUs: 'Who are we ?',
        revealShop: "I'm a shop?",
        suggestBrand: 'Share my addresses',
        profile: "Profile",
        favorites: "Favorites",
        rewards: "Good deals",
        myRewards: "My good deals",
        adminDashboard: "Admin dashboard",
        logout: "Logout",
        searchDialog: "What do you search?",
        dashboard: "My shop page",
        tutorial: "My tutorials",
        suggestedForYou: "Suggested for you"
    },
    login: {
        toast: {
            mailAndPasswordRequired: 'Mail and password fields are required',
            invalidLogin: "Invalid login details",
            blockedUser: "This profiles has been blocked because it doesn't respect our conditions of use",
            somethingWrongAppends: 'Connection to this account is impossible, please contact support',
            facebookAccount: "This mail address is linked to a facebook account, login with it",
            messageFirefoxSupport: "Firefox block facebook using, to be able to login with facebook, please <0>{{linkTextFirefoxSupport}}</0>",
            linkTextFirefoxSupport : "turn content blocking off for super responsable",
            linkFirefoxSupport: "https://support.mozilla.org/en-US/kb/content-blocking#w_turn-content-blocking-off-on-individual-sites",
            notExist: "There is no account linked to this email",
            otherFacebookAccount: "Another facebook account is linked to this email",
            internalAccount: "This email address is not linked to a facebook account. Enter your password or click on forgot password"
        },
        enterMail: {
            title: "Login / Register",
            subTitle: "Enter your email to login or create an account",
        },
        title: "Log in",
        subTitle: "Enter your password",
        button: "Connect",
        forgotPasswordLink: "Forgot your password?",
        connectionFacebook: "Connect with facebook",
        connectionOrCreate: "Connection / Registration",
        fbButton: "Login or create your account with a single click",
        facebook: {
            title: "This email address is linked to a facebook account",
            subtitle: "Connect in one click",
        },
        candidate: "Become a Super Responsible business"
    },
    signin: {
        toast: {
            mailAndPasswordRequired: "name, surname, mail and passwords fields are required",
            wrongDetails: "Wrong information",
            mailAlreadyExist: "This mail address is already registered, login with it",
            facebookAlreadyExist: "This facebook account is already registered, login with it"
        },
        title: "Account creation",
        subTitle: "Complete the information below",
        button: "Register",
        connectionFacebook: "Register with facebook",
        loginLink: "Already registered?"
    },
    forgotPassword: {
        toast: {
            mailUnknown: "This is an unknown mail, create your account",
            mailSent: "Request sent, you will receive a mail",
            facebookAccount: "This mail address is linked to a facebook account, try to connect with this account"
        },
        title: "Forgot your password",
        button: "Send"
    },
    passwordRecreate: {
        title: "Create a new password",
        button: "Send"
    },
    contact: {
        title: "Contact us",
        subject: "Chose a subject",
        questions: {
            candidate: "I want to candidate for Super Responsable",
            cost: "I'm a brand and I want to obtain a cost estimate",
            support: "I have a technical issue",
            brand: "I'm a brand and I want to be identified",
            contact: "I want to contact the Super Responsable team"
        },
        message: "Your message here ...",
        button: "Send",
        toast: {
            messageSent: "Message sent",
            errorSent: "An error appears while sending the message"
        }
    },
    suggest: {
        searchPlaceholder: "Fill name of a brand",
        title: "Share with us your address favorites",
        comment: "Tell us your experience, what do you think this brand should be identify!",
        alreadySuggested: "You have already suggest this brand, thank you!",
        site: "Website",
        suggestOther: "Don't hesitate to suggest other brands",
        newSuggested: "Thank you to suggest this brand, you are the ",
        first: "first",
        th: 'th',
        opinion: "What do you think about this experience?",
        button: "Suggest",
        required: "required",
        toast: {
            opinionNotSent: "An error appears, your opinion could not be sent",
            couldNotBeAddedInFav: "An error appears, the brand cannot be added to favorites"
        }
    },
    profile: {
        accountSettings: "Account settings",
        newsletter: "Receive the newsletter",
        newsletterAdvisor: "Don't worry, we think about the planet, you won't get much ! ",
        values: "My values",
        contact: "Contact",
        keywords: "Values",
        personalDetails: "Personal details",
        updatePassword: "Update password",
        currentPassword: "Current password",
        newPassword: "New password",
        button: "Save",
        achievements: "Actions",
        completeProfile: "Complete your profile and get a gift",
        useDiamonds: "Use my points",
        winDiamonds: "Win points",
        nextStep: {
            text: "Next step: ",
            valuesRemaining: " value(s) to choose",
            profilePic: "Add a profile picture",
            birthday: "Fill in your date of birth"
        },
        toast: {
            wrongCurrentPassword: "The current password is invalid"
        }
    },
    achievements: {
        registration: "Registration on the site",
        complete_profile: "Profile completed",
        newsletter: "Newsletter Sign-Up",
        first_favorite: "Adding the first favorite",
        five_favorite: "Add 5 favorites",
        first_suggestion: "Sharing your first address",
        five_suggestion: "Sharing of 5 addresses",
        ten_suggestion: "Sharing of 10 addresses",
        twenty_suggestion: "Sharing of 20 addresses",
        fifty_suggestion: "Sharing of 50 addresses",
        first_rate: "First notice posted",
        five_rate: "5 posted reviews",
        ten_rate: "10 posted reviews",
        twenty_rate: "20 posted reviews",
        fifty_rate: "50 posted reviews",
        survey_satisfaction: "Satisfaction questionnaire",
        noNextActions: "New actions are coming soon!",
        noActionsAlready: "No action taken",
        site_opinion: "Review about our site",
        slowActions: "Some actions, like polls, can be assigned with a certain delay."
    },
    vendorList: {
        categories: "Categories",
        keywords: "Filter by values",
        values: "My values",
        healthy: "Healthy",
        ethical: "Ethical",
        sustainable: "Sustainable",
        cities: "Filter by cities",
        genders: "Genders",
        filterButton: "Open filters",
        onlineShop: "Home delivery",
        clickAndCollect: "Click & Collect",
        aroundMe: "Around me",
        reward: "Promo and gifts",
        userDeniedGeoloc: "Geolocation is blocked by this browser! Change your settings before activating geolocation.",
        shouldBeLogged: "Log in to filter by your values", 
        noValues: {
            message: "No value has been entered on your profile, <0>{{linkText}}</0>",
            linkText : "complete it"
        },
        emptyState: {
            title: "No results found",
            subTitle: "These combined search criteria yield no results"
        },
        notFound: {
            title: "No results found",
            subTitle: "No one may have proposed signs for this filter or which are being checked.",
            link: "Do you know any? Share your addresses!"
        }
    },
    favorites: {
        title: "My favorites addresses",
        subTitle: "<0/>This is my address book. There are many like it, but this one is mine. <1/>Without me, my address book is useless. Without my address book, I cannot do shopping.<0/>",
        notFound: {
            title: "No favorites added",
            subTitle: "You have not added any favorite sign",
            link: "Discover them now!"
        }
    },
    suggestedForYou: {
        title: "My personalized suggestions",
        subTitle: "Find here our personalized suggestions in relation to your profile.<0/>They are based on the values you have selected on your account to offer you brands that share the same values as you.",
        notFound: {
            title: "No suggestions available",
            subTitle: "We cannot offer you any suggestions based on your values. Check back later or update your values",
            link: "Update my values"
        }
    },
    articlesB2B: {
        title: "My tutorials",
        subTitle: "Find here our tutorials dedicated to companies.",
        notFound: {
            title: "No tutorial is available",
            subTitle: "New tutorials will be coming soon, come back to visit this page regularly."
        }
    },
    vendorCandidate: {
        slogan: "YOU HAVE VALUES AND THIS MUST BE KNOWN",
        catchPhrase: {
            title: "Maximize your digital presence in a responsible way",
            text: "If you want to increase your online reputation, be aware of market trends and benefit from a caring and supportive environment, all at an affordable price and without commitment, you couldn't have been better"
        },
        reveal: {
            title: "Reveal the best of your world in 'one shot'",
            text: `<0>Your all-in-one page mixes storytelling, values ​​and distinctions, points of sale, good deals, top sales, digital presences, 3 photos and 1 video</0>
            <0>Internet users can keep your brand in their favorites, share your page and express their opinions</0>
            <0>You are highlighted in our communication media: posts, articles, videos, newsletters, editorial reviews, advertisements</0>`
        },
        analyse: {
            title: "Analyze your performance and follow the evolution of your sector",
            text: `<0>Discover the brands, trends and values ​​most appreciated by our users to develop your business</0>
            <0>Receive your quarterly market study carried out with our Internet users, partners and referenced brands</0>
            <0>Access your page statistics: location data, social media popularity, top sales etc <0>(coming soon)</0></0>`
        },
        join: {
            title: "Join a network of trendy and responsible players",
            text: `<0>Create a lasting bond with Internet users by offering your good deals: promotion codes, invitations to events and small gifts are welcome</0>
            <0>Control your page or let us do it: creation, updates of your elements on request, management of your good deals, we take care of everything!</0>
            <0>Sell ​​your responsible products with high commitments and certainly modern by integrating our marketplace <0>(coming soon)</0></0>`
        },
        integrate: {
            title: "Join our community website today",
            text: "Our non-binding formulas were thought with referenced brands in order to meet your objectives according to your budget"
        },
        questions: `Questions ? <0>Contact us</0>`, 
        aboutUs: {
            title: "About Super Responsable",
            text: `We connect businesses and responsible consumers in France on our community site designed to boost your visibility, improve your image, increase your notoriety, integrate a network of responsible players and acquire new qualified leads. <0>Who are we</0>   `
        },
        numbers: {
            reboundRate: {
                title: "rebond rate",
                value: "1,62%"
            },
            recommendation: {
                title: "recommend it*",
                value: "100%"
            },
            county: {
                title: "departments",
                value: "85"
            },
            values: {
                title: "values",
                value: "50"
            },
            followers: {
                title: "followers",
                value: "+ 5000"
            },
            youtube: {
                title: "Youtube views",
                value: "+ 14 000"
            },
            note: "* Response to the survey of 07/10/2020 carried out with 14 referenced brands"
        },
        form: {
            title: "I CREATE MY SIGN PAGE",
            subtitle: "Please fill in the fields below"
        },
        categories: "Categories",
        keywords: "Values",
        toast: {
            creationError: "An error appears while saving your brand",
            creationSuccess: "Your brand has been registered, thank you for your interest, we will contact you very soon",
        }
    },
    reward: {
        copied: "Code copied to clipboard",
        unlockTitle: "Unlock this gift?",
        unlockReward: "Unlocking this gift consumes 1 point",
        cancel: "Later",
        unlock: "Use one point",
        howToObtainDiamond: "How to get points?",
        diamondExplanation: "Points are obtained during actions taken on the community site. <0 /> Each action earns one or more points.<0/><1>How to get points?</1>",
        nextActions: "Next actions",
        alreadyAchieved: "Completed actions",
        missingDiamondTitle: "Oops, you are missing points!",
        missingDiamond: "You did not get enough points to be able to unlock this gift. A host of actions await you",
        close: "Close",
        alreadyUnlocked: "Good deals already unlocked",
        availableToUnlock: "Good deals available",
        addButton: "Add a good plan",
        create: "Create a new good plan",
        message: "The details of the conditions",
        code: "The code to use on your site",
        value: "The value of the offer (10%, 20 €, free delivery ...)",
        type: "The type of offer",
        voucher: "A reduction",
        gift: "A gift",
        event: "An invitation",
        validity: "The validity date (optional)",
        other: "Other",
        toast: {
            creationFail: "An error occurred while creating the right plan",
            disableFail: "An error occurred while inactivating the good plan",
            enableFail: "An error occurred while activating the good plan"
        },
        activated: "My active good deal",
        disabled: "My inactive good deal",
        disable: "Make inactive",
        enable: "Make active",
        disabledTitle: "Make this good plan inactive?",
        disableInfo: "Are you sure? This good plan will no longer be visible to users",
        enabledTitle: "Make this good plan active?",
        enableInfo: "Are you sure? This good plan will be visible to users"
    },
    vendorProfile: {
        info: "This information was provided by the company and verified by our team. To do this, we rely on labels, associations and NGOs, sector experts, and what makes our strength, the community that reveals, investigates and tests the signs. Information seems inaccurate? <0>Please let us know</0>",
        searchPlaceholder: "Search a city, a county",
        more: "More",
        less: "Less",
        addComment: "Add a comment",
        comments: "Comments",
        vendorsAssimilate: "You will surely like",
        toast: {
            commentNotSend: "An error appears, the comment cannot be added",
            reportComment: "Thank you to report this comment, we will check it soon!",
            reportCommentError: "An error appears, this comment cannot be reported"
        },
        filters: "filters",
        cities: "cities",
        ourStory: "Our story",
        labels: {
            title: "Labels carried by ",
            desc: "Some articles of this brand are labeled"
        },
        map: "Shops & distributors",
        follow: "Follow",
        alsoAvailableOn: "Also available on:",
        rewards: {
            title: "<0>{{vendorName}} good deals</0><1/><1/>",
            explanation: "{{points}} good deal is available for this brand<0/><1>Enjoy it quickly!</1>",
            explanations: "{{points}} good deals are available for this brand<0/><1>Enjoy it quickly!</1>",
        },
        share: "Share"
    },
    onlineShop: {
        website: "Online shop",
        noPointOfSale: "No point of sale"
    },
    comments: {
        userDeleted: "User deleted",
        about: "About: "
    },
    shareComments: {
        title: "Share your opinion",
        rate: "Global rate:",
        text: "Comment: ",
        button: "Send",
        textPlaceholder: "Thanks to be polite and respectful."
    },
    adminDashboard: {
        activated: "activated",
        published: "published",
        keywords: "values",
        descFr: "storytelling Fr",
        descEn: "storytelling En",
        vendors: "vendors",
        vendorsCorresponding: "List of vendors corresponding",
        suggestedBy: "Suggested by",
        mode: "Mode",
        imagesCarousel: "Carousel pictures",
        uploadImageCarousel: "Update a picture",
        title: "Admin dashboard",
        userAndCommentManager: "Users and comments Management",
        suggestedBrandManager: "Brands Management",
        potentialBrandManager: "Supplier Management",
        instagramSuggestedBrandManager: "Instagram suggested brands by Instagram",
        reportedComments : "Reported comments",
        linkToVendor: "Link to the vendor",
        deleteComAndReportUser: "Delete comment and report user",
        noCommentsReported: "No comment reported",
        usersReportedMoreThan3: "Users reported 3 times or more",
        reported: "Reported",
        time: "times",
        blockUser: "Block user",
        noUsersReported: "No user reported",
        usersBlocked: "Users blocked",
        blockedSince: "Block since ",
        unblockUser: "Unblock user",
        noUsersBlocked: "No user blocked",
        deleteBrand: "Delete a brand: Are you sure ?",
        contactBrand: "Contact a brand: Are you sure ?",
        updateBrand: "Update the brand",
        createBrand: "Create a brand",
        cancel: "Cancel",
        delete: "Delete",
        update: "Update",
        create: "Create",
        contact: "Contact",
        search:"Type a name to search",
        name: "Name",
        occurrence: "Occurrence",
        checked: "Checked",
        createdAt: "Created at",
        updatedAt: "Updated at",
        mail: "Email address",
        site: "Web site",
        note: "Personal note",
        instagram: "Instagram Id",
        messages: "Messages",
        alreadyContacted: "Already contacted",
        send: "Send",
        transform: "Transform to vendor",
        brandToShop: "Transform a suggested brand to vendor: Are you sure ?",
        labels: "Labels",
        image: "Image",
        upload: "upload",
        updateLabel: "Update the label",
        createLabel: "Create a label",
        uploadLabel: "Upload logo label",
        locations: "Point of sale",
        createLocation: "Create a point of sale",
        updateLocation: "Update a point of sale",
        location: {
            city: "City",
            address: "Address",
            postalCode: "Postal Code",
            openingHours: "Opening hours",
            phoneNumber: "Phone number",
            webSite: "Web site",
            mail: "Mail",
            latitude: "Latitude",
            longitude: "Longitude",
            vendors: "Select vendors",
            state: "State",
            county: "County"
        },
        vendor: {
            label: "Labels",
            location: "Points of sale",
            select: "Select a vendor",
            profilePicture: "Profile picture",
            logo: "Logo",
            carouselPicture: "Carousel picture",
            featuredProduct: "Featured product",
            info: "Informations",
            publish: "Publish",
            categories: "Categories",
            keywords: "Values",
            genders: "Genders",
            desc: {
                fr: "French description",
                en: "English description"
            },
            videoYoutube: "Youtube video Id",
            videoDailymotion: "Dailymotion video Id",
            videoVimeo: "Vimeo video Id",
            site: "Web site",
            socialNetwork: {
                fb: "facebook",
                insta: "instagram",
                twitter: "twitter",
                pinterest: "pinterest",
                youtube: "youtube"
            },
            onlineShop: "Online shop",
            createToken: "Send a token",
            createTokenToAdmin: "Send token to admin",
        },
        profilePicture: "Define profile picture of a vendor",
        logo: "Define logo of a vendor",
        carouselPicture: "Define carousel picture of a vendor",
        carouselNumber: "Define the place of picture into carousel",
        featuredProductPicture: "Define featured product picture of a vendor",
        featuredProductNumber: "Define place of picture into featured products",
        featuredProductLink: "Define link of featured product",
        featuredProductCol: "Define size of column",
        state: {
            title: "State",
            notContacted: "Not contacted",
            firstContact: "First contact",
            certificationValidationOngoing: "Validation of certificates ongoing", 
            certificationValidated: "Certificates validated", 
            dataValidationOngoing: "Validation of data ongoing", 
            dataValidated: "Data validated", 
            dataCreatingOngoing: "Data creation ongoing", 
            dataCreated: "Data created",
            filter: "Filter by state"
        },
        toast: {
            commentDeleted: "Comment is deleted",
            userReported: "User is reported",
            userReportError: "User cannot be reported",
            commentDeletionError: "Comment cannot be deleted",
            userBlocked: "User is blocked",
            userBlockError: "User cannot be blocked",
            userUnblocked: "User is unblocked",
            userUnblockError: "User cannot be unblocked",
            suggestedBrandUpdated: "Brand suggested is updated",
            suggestedBrandUpdatedError: "Brand suggested cannot be updated",
            suggestedBrandDeleted: "Brand suggested is deleted",
            suggestedBrandDeletedError: "Brand suggested cannot be deleted",
            suggestedBrandContacted: "Brand suggested is contacted",
            suggestedBrandContactedError: "Brand suggested cannot be contacted",
            suggestedBrandCreated: "Brand suggested is created",
            suggestedBrandCreatedError: "Brand suggested cannot be created",
            potentialBrandUpdated: "Brand potential is updated",
            potentialBrandUpdatedError: "Brand potential cannot be updated",
            potentialBrandDeleted: "Brand potential is deleted",
            potentialBrandDeletedError: "Brand potential cannot be deleted",
            potentialBrandCreated: "Brand potential is created",
            potentialBrandCreatedError: "rand potential cannot be created",
            instagramSuggestedBrandUpdated: "Brand suggested on instagram is updated",
            instagramSuggestedBrandUpdatedError: "Brand suggested on instagram cannot be updated",
            instagramSuggestedBrandDeleted: "Brand suggested on instagram is deleted",
            instagramSuggestedBrandDeletedError: "Brand suggested on instagram cannot be deleted",
            imageUploaded: "Image updated correctly",
            imageUploadedError: "Image could not be updated correctly",
            labelUpdated: "Label successfully updated",
            labelUpdatedError: "Label cannot be successfully updated",
            vendorPublished: "Vendor published successfully",
            suggestedBrandTransformed: "Suggested brand has been transform successfully",
            suggestedBrandTransformedError: "Suggested brand cannot be transformed",
            createLabel: "The label is correctly created",
            locationUpdated: "The point of sale is updated",
            locationUpdatedError: "The point of sale cannot be updated",
            locationCreated: "The point of sale is created",
            locationCreatedError: "The point of sale cannot be created",
            vendorInfoUpdated: "Information updated",
            vendorTokenCreated: "Creation account link sent"
        }
    },
    vendor: {
        notAllowedToPublish: {
            numberOfPointOfSale: "Maximum number of point of sale reached for this subscription",
            expired: "The current subscription has expired",
            basic: "The Basic subscription does not allow you to publish your page",
            incomplete: "Payment for your current subscription is pending payment",
            notSubscribe: "You do not have a subscription to publish your page"
        }
    },
    subscription: {
        title: "Our subscriptions",
        subscribe: "Subscribe",
        currentSubscription: "Your current subscription",
        selectedSubscription: "The chosen subscription",
        chosen: "Select a new subscription:",
        alreadySubscribe: "You already have this subscription",
        issuePayment: "Payment for the current subscription failed, action is required",
        buttonReplace: "Replace current subscription",
        canceled: "Your current subscription is terminated, it is valid until {{expiresIn}} <0/>You can choose a new one now, which will replace the current subscription. <0 />The current subscription will then be lost. ",
        fromMonthlyToLowerMonthly: "Your current monthly subscription is valid until {{expiresIn}}} <0/>You can choose a lower plan now, which will replace the current subscription. <0 />The current subscription will then be lost. ",
        fromYearlyToLower: "Your annual subscription you commit until {{expiresIn}} <0/>You can choose a lower plan now, which will replace the current subscription. <0 />The current subscription will then be lost." ,
        fromYearlyToMonthly: "You commit your annual subscription until {{expiresIn}} <0/>You can choose a monthly plan now, which will replace the current subscription. <0 />The current subscription will then be lost." ,
        checkoutForm: {
            requireAction: "The validation of this subscription requires an action, please wait a while",
            processPayment: "Data being processed",
            cardDetails: "<0>Enter your card details here. <0 />Your subscription will begin after data verification by our team. </0>",
            totalDue: "Amount:",
            frequency: "Frequency",
            fullName: "Company name:",
            card: "Payment card:",
            country: {
                label: "Company domiciliation",
                france: "France",
                EU: "European Union",
                horsUE: "Outside the European Union"
            },
            tvaIntra: "Intra-community number",
            chooseCountry: "Select a domiciliation to obtain this calculation"
        },
        paymentComplete: "Payment completed",
        member: "Member",
        alreadySubscribeToOther: "You already benefit from a subscription, if you wish to modify it, you can cancel your current subscription at any time, and subscribe to another plan at the expiration date",
        basicSubscription: "You just need to be logged in to take advantage of this subscription! <0/><1>Create your page now</1>",
        proration: "Price invoiced immediately with deduction of unused time from the current subscription: {{prorationFinalPrice}} including VAT <0 /> Or {{prorationPrice}} ex-Tax reduction",
        trialPeriod: "Well done, you are eligible for a 90 day free trial",
        toast: {
            updateSuccess: "Your subscription has been successfully modified",
            updateError: "An error occurred while registering your new subscription, contact us"
        }
    },
    vendorDashboard: {
        searchPlaceholder: "Search a city, a county",
        cities: "City(es)",
        map: "Shops & distributors",
        toast: {
            youtubeMalformed: "The youtube link is incorrect, the expected format is https://www.youtube.com/watch?v=...",
            dailymotionMalformed: "The dailymotion link is incorrect, the expected format is https://www.dailymotion.com/video/...",
            vimeoMalformed: "The vimeo link is incorrect, the expected format is https://vimeo.com/...",
            youtubeChannelMalformed: "The youtube channel link is incorrect, the expected format is https://www.youtube.com/channel/...",
            pinterestMalformed: "The pinterest account link is incorrect, the expected format is https://www.pinterest.fr/...",
            instagramMalformed: "The instagram account link is incorrect, the expected format is https://www.instagram.com/...",
            twitterMalformed: "The twitter account link is incorrect, the expected format is https://twitter.com/...",
            facebookMalformed: "The facebook account link is incorrect, the expected format is https://www.facebook.com/...",
            websiteMalformed: "The website link is incorrect, the expected format is https://...",          
            linkMalformed: "The link is incorrect, the expected format is http: // ... or https: // ...",
            imageUploaded: "Image updated correctly",
            imageUploadedError: "Image could not be updated correctly",
            infoUpdated: "Information updated",
            infoUpdatedError: "The information could not be updated correctly",
            locationAdded: "Point of sale added",
            locationAddedError: "The point of sale could not be added correctly",
            locationUpdated: "Point of sale updated",
            locationUpdatedError: "The point of sale could not be updated correctly",
            locationRemoved: "Point of sale withdrawn",
            locationRemovedError: "The point of sale could not be successfully removed",
            published: "Published page",
            publishedError: "Error while publishing the page"
        },
        firstPublish: {
            title: 'Publish changes',
            info: `The first publication is included in the Basic subscription.
            After this publication, your brand page will be online and accessible to all users.
            The next changes will require a Super or Super + subscription`,
            do: 'Publish now once',
            passToUnlimited: 'Publish unlimited'
        },
        alreadyPublish: {
            title: 'Publish changes',
            info: `The first publication is included in the Basic subscription.
            After this publication, your brand page will be online and accessible to all users.
            The next changes will require a Super or Super + subscription`,
            do: 'Publish now once',
            passToUnlimited: 'Publish unlimited'
        },
        allowedToPublish: {
            info: `The changes are saved in a draft.
            Publishing these changes requires a <0>Basic, Super or Super +</0> subscription`,
            infoIncomplete: `Your subscription is pending payment. The changes are saved in a draft.
            To unblock the publication you can validate the pending procedure <0>here</0> `,
        },
        goEdit: {
            infoNotPublished: `Your brand page is not yet online, switch to edit mode to complete and publish it`,
            infoPublished: `Your brand page is <0> online! </0> All users can access it through the shared addresses tab or through the search field. Here you discover the preview of your page. Click on edit mode to make updates.`
        },
        isValidSubscription: {
            infoBasic: `You have a <0> {{status}} </0> subscription, well done! You can edit and publish your brand page once, administer up to 6 points of sale and manage your tips <`,
            infoSuper: `You have a <0> {{status}} </0> subscription, well done! You can modify and publish your brand page unlimited, administer up to 20 points of sale and manage your tips`,
            infoSuperPlus: `You have a <0> {{status}} </0> subscription, well done! You can modify and publish your banner page, manage your points of sale and manage your good deals, all in unlimited!`
        },
        subscription: {
            title: 'Publish changes',
            info: `An account without a subscription cannot publish its page to make it accessible.
            To modify your page and publish it, a Basic, Super or Super + subscription is required`,
            do: 'Become Super',
            doPlus: 'Become Super +'
        },
        publish: "Publish",
        empty: "Empty section",
        desc: {
            title: "Describe here the storytelling of your brand",
            edit: "Edit your storytelling",
            fr: "Description in French",
            in: "Description in English"
        },
        networks: {
            title: "Copy the links from your various social networks: instagram, facebook, twitter, pinterest, youtube, and finally your website",
            edit: "Edit your social networks"
        },
        video: {
            title: "Copy the link of a video from youtube, dailymotion or vimeo",
            edit: "Edit your video",
            select: "Choose the type of video"
        },
        carousel: {
            title: "Send the images that will take place in your carousel",
            edit: "Modify your carousel",
            info: "The recommended image format is 450x300, to obtain optimum resolution.",
            select: "Choose the photo to replace",
            first: "First picture",
            second: "Second picture",
            third: "Third picture"
        },
        featuredProduct: {
            title: "Create your featured products",
            edit: "Send this featured product",
            infoSquare: "The recommended image format is 300x300, to obtain optimum resolution.",
            infoRectangle: "The recommended image format is 450x300, to obtain optimum resolution.",
        },
        location: {
            allow: {
                title: "Manage points of sale",
                infoNotSubscribe: "To add, modify or delete a point of sale, a Basic, Super or Super + subscription is required",
                infoSuper: "You have reached the limit of points of sale for your subscription. To add unlimited points of sale, a Super + subscription is required.",
                infoBasic: "You have reached the limit of points of sale for your subscription. To add more points of sale, a Super or Super + subscription is required.",
                infoBasicPublished: "Your publication number has been reached. To publish unlimited and update your points of sale, a Super or Super + subscription is required."
            },
            remove: {
                title: "Delete this point of sale",
                info: "Are you sure? This point of sale will be removed from your page"
            },
            add: {
                title: "Add this point of sale",
                search: "Search for an address, for example by typing '52 rue des jonquilles BELLEVILLE'. Only French addresses allowed",
                results: "Select a result:",
                alreadyAdded: "Point of sale already added",
                noResult: "No results were found for this search, try another search",
                info: "Fill in the information for this point of sale"
            },
            edit: {
                title: "Modify this point of sale",
                info: "Update the information for this point of sale"
            },
            shopName: "Name of the shop or reseller",
            address: "Address",
            postalCode: "Postal code",
            city: "City",
            county: "Department",
            state: "Region",
            openingHours: "Hours",
            phoneNumber: "Phone",
            webSite: "Website",
            mail: "Email address"
        },
        editMode: "Edit mode",
        cancel: "Cancel",
        update: "Edit",
        remove: "Remove",
        search: "Search",
        select: "Select",
        add: "Add"
    },
    vendorAccount: {
        toast: {
            canceled: "The subscription has been suspended",
            canceledError: "An error occurred while suspending the subscription",
            deliverInfoUpdated: "Delivery information has been updated",
            deliverInfoUpdatedError: "An error occurred while updating delivery information"
        },
        deliver: {
            title: "Indicate your delivery methods",
            onlineShop: "Home delivery",
            clickAndCollect: "Click & Collect",
            button: "Save"
        },
        inactivate: "<0> Deactivated page </0> <1 /> <1 /> Your page has been deactivated by the Super Responsible team. For more information <2> contact us </2>",
        published: "<0> Online page </0> <1 /> <1 /> Your page is accessible to all users. To modify it, <2> go to the dashboard </2>",
        notPublished: "<0> Offline page </0> <1 /> <1 /> Your page is not published, it is inaccessible to all users. To publish it, <2> go to the dashboard </ 2 > ",
        logo: {
            title: "Update your logo",
            info: "The recommended image format is 200x200, to obtain an optimal resolution.",
        },
        subscription: {
            dialog: {
                terminate: {
                    title: "The subscription is activated",
                    info: `This subscription allows you to put your brand page online. <0 />
                    Once canceled, the services related to this subscription as well as your brand page will no longer be available from the expiration date, namely in {{expirationDate}} `
                }
            },
            cancel: "Cancel subscription",
             keep: "Keep the subscription",
             change: "Change subscription",
             terminate: "Cancel subscription",
             takeBack: "Take back a subscription",
             activate: "Activate subscription",
             do: "Subscribe immediately",
             price: "Price",
             startAt: "Since",
             nextPayment: "Next payment in",
             creditCard: "Card used",
             expiresIn: "Expires in",
             expiredSince: "Expired since",
             state: {
                title: "State",
                value: {
                    trialing: "Trial period",
                    active: 'Active',
                    incomplete: 'Pending payment',
                    incomplete_expired: 'Pending payment',
                    past_due: 'Pending payment',
                    canceled: 'Canceled',
                    unpaid: 'Unpaid'
                }
            },
             noSubscription: "No current subscription",
             year: "year",
             month: "month"
         },
         invoice: {
             title: "Invoices",
             paid: "paid",
             retrieve: "Get the invoice",
             produced: "Issued on"
         },
         profilePicture: {
             title: "Update your profile picture",
             info: "The recommended image format is 460x200, to obtain an optimal resolution.",
         }
     },
    partners: {
        supportSR: "They support Super Responsable",
        supportHashtagSR: "They contribute to the campaign"
    },
    aboutUs: {
        title: "Super Responsable, because you have to be a super hero today to live with respect for people, animals and the environment",
        introduction: "More than 60% of French people (42 million source: greenflex 2019) say they are concerned by healthy, ethical and sustainable consumption, but experience real difficulties in taking action and sticking to them over time. transition, Alexandra & Loïse designed the Super Responsable community site. Developed at the <0>Telecom Paristech Eurecom</0> Incubator in the 1st Technopôle in Europe, the start-up has created a win-win model for retailers and responsible consumers in France. ",
        questionOne: "Rewarding everyday heroes is kind of your watchword. <0 /> What does that mean?",
        answerOne: "We believe that anyone who participates directly or indirectly in growing this market rather than another deserves to be recognized and rewarded. This is why we allow <0>brands referenced</0> d '' to be boosted in communication and marketing and to users to obtain and choose their <1>gifts</1> for the actions they have carried out on the site Promo codes, events, goodies… We saw a lot! ",
        questionTwo: "How can users be rewarded?",
        answerTwo: "By <0>sharing your good addresses</0> on the site as you would for a friend, you help someone find what they are looking for and give your support to a brand that lives full force the health crisis is no small feat. Out of 5,000 members, 2,400 brands have been proposed and are being verified by our team. In addition, responding to our surveys helps to understand and anticipate trends, as in our case. 1st survey report <1>COVID-19: responsible consumption trends in France.</1> ",
        legend: "In our great office where everything comes to life",
        questionThree: "Do you think you meet everyone's requirements?",
        answerThree: "The aids are scattered. Depending on whether you prefer online shopping to land, organic or vintage etc. The user has to memorize a long list of new brands and products, which is not easy. This is why we offer an all-in-one solution: find brands and their products that are already responsible, and in all forms: producers, shops, resellers, e-shops and marketplaces in food, fashion, beauty and home for men, women, child and animal. ",
        questionFour: "Customizing the user journey, what does that mean exactly?",
        answerFour: "Users geolocate themselves, give their opinions, record their values ​​and favorite brands allowing tailor-made suggestions at the cutting edge of digital. These suggestions are possible through the use of mass and anonymized data. This allows you for example to discover the <0>favorites and trends</0> of other people who share your values. ",
        questionFive: "How do you see the super responsable future?",
        answerFive: "We are counting on the support of members who are the primary contributors to our site. We are developing a scoring algorithm in order to promote the most deserving brands. We also want to highlight the products as well as their brands, and give more weight for geolocation. ",
        seeYou: "See you soon in our super responsible adventures!"
    },
    campaignAchieved: {
        title: "Well done, the launch campaign is over!",
        subTitle: "It's your turn now:",
        signin: "Register and create your profile",
        link: "Add your favorite addresses and create your own notebook",
        discover: "Discover the businesses around you, in accordance with your values",
        follow: "Follow our super responsible adventures!",
    },
    priceList: {
        unlimited: "unlimited",
         limited: "limited",
         page: {
             name: "SIGN PAGE",
             pilot: {
                 name: "Manage your page",
                 basic: "1 upload"
             },
             pointOfSale: "Add your points of sale",
             featuredProducts: "Reveal your top sellers",
             carousel: "Integrate your best photos",
             storyTelling: "Write your storytelling FR & EN",
             links: "Integrate your digital presences",
             weTakeCare: "We take care of everything"
         },
         com: {
            name: "COMMUNICATION",
            posts: "Social media posts",
            newsletter: "Integrate the newsletter",
            articlesAndVideos: "Articles & videos",
            articleOrVideoDedicated: {
                name: "Dedicated article / video",
                super: "1 item"
            },
            redactionOpinion: "Editorial opinion"
        },
        rewards: {
            name: "REWARDS",
            manage: "Add / manage your tips",
            stats: "Popularity statistics (soon)",
            monthly: "Quarterly sending of your statistics (soon)",
            weTakeCare: "Managing your tips"
        },
        marketing: {
            name: "MARKETING",
            studies: "Quarterly market studies",
            stats: "Statistics of your page (soon)",
            weTakeCare: "Adapted annual market study"
        },
        marketplace: {
            name: "MARKETPLACE (soon)",
            integrate: "Integrate our marketplace",
            stats: "Popularity statistics",
            create: "Create your product sheets",
            statsWeTakeCare: "Quarterly sending of your statistics",
            weTakeCare: "Integration and management"
        },
        monthly: "MONTHLY",
         byMonth: "month",
         yearly: "ANNUAL 1 month free",
         byYear: "an",
         weTakeCare: "ANNUAL Personal Service",
         withWeTakeCare: "Personal service included",
         subscription: "Subscription",
         oneMonthOffer: "One month free",
         noObligation: "Without obligation",
         basic: {
            name: "BASIC",
            monthly: "1 € ex-Tax",
            yearly: "11 € ex-Tax",
            weTakeCare: "77 € ex-Tax"
        },
        super: {
            name: "SUPER",
            monthly: "14 € ex-Tax",
            yearly: "154 € ex-Tax",
            weTakeCare: "264 € ex-Tax"
        },
        superPlus: {
            name: "SUPER +",
            monthly: "44 € ex-Tax",
            yearly: "484 € ex-Tax",
            weTakeCare: "704 € ex-Tax"
        },
        subscribe: "Subscribe"
    },
    link: {
        facebookGroupLink: "Or follow the news on facebook group",
        vendorCandidateLink: "I'm a shop?",
        loginVendorLink: "Login to my shop",
        loginLink: "Login",
        loginUserLink: "User account"
    },
    form: {
        enterpriseName: {
            label: "Enterprise name",
            helper: "The enterprise name is required"
        },
        contactName: {
            label: "Contact name"
        },
        phone: {
            label: "Phone number"
        },
        site: {
            label: "Web site"
        },
        name: {
            label: 'First name',
            helper: 'Your first name is required'
        },
        brand: {
            label: 'Brand',
            helper: 'Brand name is required'
        },
        surname: {
            label: 'Last name',
            helper: 'Your last name is required'
        },
        birthday: {
            label: 'Birthday'
        },
        mail: {
            label: 'Email',
            helper: 'Your mail address is required and must be valid'
        },
        password: {
            label: 'Password',
            helper: 'Your password is required and must contains 8 char with: 1 number, 1 uppercase, 1 lowercase'
        },
        newsletter: {
            label: "Register to the newsletter"
        },
    },
    404: {
        title: "Error 404",
        subTitle: "This page doesn't exists or the link is corrupted"
    },
    error: {
        somethingWrongAppends: "Something wrong appends",
        shouldBeConnected: "Connection is need for this action",
        notAllowed: "You are not allowed to access to this page",
        connectionInvalid: "Connection is no longer valid, please identify yourself again"
    },
    rgpd: `<0>Definitions</0>
    <1><0>Client:</0> any professional or capable natural person within the meaning of articles 1123 and following of the Civil Code, or legal person, who visits the Site subject to these general conditions. <3 />
    <0>Benefits and Services:</0> <1 href = "https://www.super-responsable.org"> https://www.super-responsable.org</1> makes available to Customers:</1>

    <1><0>Content:</0> All of the elements that make up the information on the Site, including texts - images - videos.</0>

    <1><0>Customer information:</0> Hereinafter referred to as “Information (s)” which correspond to all the personal data likely to be held by <1>https: //www.super-responsable. org</1> for the management of your account, the management of the customer relationship and for the purposes of analysis and statistics.</1>


    <1><0>User:</0> Internet user connecting, using the aforementioned site.</1>
    <1><0>Personal information:</0> "Information which allows, in any form whatsoever, directly or indirectly, the identification of the natural persons to whom it applies" (article 4 of law no. 78-17 of January 6, 1978).</1>
    <1>The terms “personal data”, “data subject”, “subcontractor” and “sensitive data” have the meaning defined by the General Data Protection Regulation (RGPD: n ° 2016-679) </1>

    <0>1. Presentation of the website.</0>
    <1>Under article 6 of law n ° 2004-575 of June 21, 2004 on confidence in the digital economy, users of the website are specified <1>https://www.super-responsible.org</1> the identity of the various stakeholders in the context of its implementation and monitoring:
   </1><1><2> Owner </2>: SAS Super Responsable Share capital of € 2,000 VAT number: FR45887680379 - 90 av du Maréchal Juin 06400 Cannes <3 />
                  
    <2> Publication manager </2>: Alexandra Tence - bonjour@super-responsable.org <3 />
    The publication manager is a natural person or a legal person. <3 />
    <2> Webmaster </2>: Loïse Fenoll - support@super-responsable.org <3 />
    <2> Host </2>: Heroku - 415 Mission Street Suite 300 94105 San Francisco, CA (866) 278-1349 <3 />
    <2> Data protection officer </2>: Loïse Fenoll - support@super-responsable.org <3 />
   </1>

    <2> <0>This legal notice template is offered by the <1>free generator offered by Orson.io</1></0> </2>



    <0>2. General conditions of use of the site and the services offered.</0>

    <1>The Site constitutes an intellectual work protected by the provisions of the Code of Intellectual Property and applicable International Regulations.
    The Customer may not in any way reuse, transfer or exploit for his own account all or part of the elements or works of the Site.</1>

    <1>The use of the site <1>https://www.super-responsable.org</1> implies the full and complete acceptance of the general conditions of use described below. These terms of use may be modified or supplemented at any time, users of the <1>https://www.super-responsable.org</1> site are therefore invited to consult them on a regular basis. </1>

    <1>This website is normally accessible to users at all times. An interruption due to technical maintenance may however be decided by <1>https://www.super-responsable.org</1>, who will then endeavor to communicate to users beforehand the dates and times of the intervention.
    The website <1>https://www.super-responsable.org</1> is regularly updated by <1>https://www.super-responsable.org</1> responsible. Likewise, the legal notices can be modified at any time: they are nevertheless binding on the user, who is invited to refer to them as often as possible in order to become acquainted with them.</1>

    <0>3. Description of the services provided.</0>

    <1>The purpose of the <1>https://www.super-responsable.org</1> website is to provide information on all of the company's activities.
    <1>https://www.super-responsable.org</1> strives to provide on the site <1>https://www.super-responsable.org</1> as accurate information as possible . However, it cannot be held responsible for omissions, inaccuracies and deficiencies in the update, whether by itself or by the third party partners who provide it with this information.</1>

    <1>All the information indicated on the site <1>https://www.super-responsable.org</1> are given as an indication, and are likely to evolve. Furthermore, the information on the site <1>https://www.super-responsable.org</1> is not exhaustive. They are given subject to modifications having been made since they were put online.</1>

    <0>4. Contractual limitations on technical data.</0>
    <1>The site uses JavaScript technology.

    The website cannot be held responsible for material damage related to the use of the site. In addition, the user of the site agrees to access the site using recent equipment, which does not contain viruses and with an up-to-date latest generation browser
    The site <1>https://www.super-responsable.org</1> is hosted by a service provider on the territory of the European Union in accordance with the provisions of the General Data Protection Regulation (RGPD: n ° 2016 -679)</1>

    <1>The objective is to provide a service that ensures the best accessibility rate. The host ensures the continuity of its service 24 hours a day, every day of the year. It nevertheless reserves the right to interrupt the hosting service for the shortest possible periods of time, in particular for the purposes of maintenance, improvement of its infrastructures, failure of its infrastructures or if the Services and Services generate reputed traffic. abnormal.</1>

    <1><1>https://www.super-responsable.org</1> and the host cannot be held responsible in the event of a malfunction of the Internet network, telephone lines or computer and telephone equipment linked in particular network congestion preventing access to the server.</1>

    <0>5. Intellectual property and counterfeits.</0>

    <1><1>https://www.super-responsable.org</1> owns the intellectual property rights and holds the rights of use on all the elements accessible on the website, in particular the texts, images , graphics, logos, videos, icons and sounds.
    Any reproduction, representation, modification, publication, adaptation of all or part of the elements of the site, whatever the means or process used, is prohibited without the prior written consent of: <1>https: //www.super-responsable .org</1>.</1>

    <1>Any unauthorized use of the site or any of the elements it contains will be considered as constituting an infringement and prosecuted in accordance with the provisions of articles L.335-2 and following of the Intellectual Property Code. </1>

    <0>6. Limitations of Liability.</0>

    <1><1>https://www.super-responsable.org</1> acts as the publisher of the site. <1>https://www.super-responsable.org</1> is responsible for the quality and veracity of the Content it publishes.</1>

    <1><1>https://www.super-responsable.org</1> cannot be held responsible for direct or indirect damage caused to the user's equipment when accessing the website <1>https://www.super-responsable.org</1>, and resulting either from the use of equipment that does not meet the specifications indicated in point 4, or from the appearance of a bug or a incompatibility.</1>

    <1><1>https://www.super-responsable.org</1> cannot also be held responsible for indirect damages (such as for example a loss of market or loss of a chance) consecutive to the use of the site <1>https://www.super-responsable.org</1>.
    Interactive spaces (possibility to ask questions in the contact area) are available to users. <1>https://www.super-responsable.org</1> reserves the right to remove, without prior notice, any content posted in this space that would violate the law applicable in France, in particular the provisions relating to data protection. Where applicable, <1>https://www.super-responsable.org</1> also reserves the right to question the civil and / or criminal liability of the user, particularly in the event of a message of a nature racist, abusive, defamatory, or pornographic, whatever the medium used (text, photography ...).</1>

    <0>7. Personal data management.</0>

    <1>The Customer is informed of the regulations concerning marketing communication, the law of June 21, 2014 for confidence in the Digital Economy, the Data Protection Act of August 06, 2004 as well as the General Data Protection Regulation (RGPD : n ° 2016-679).</1>

    <3>7.1 Persons responsible for collecting personal data </3>

    <1>For Personal Data collected as part of creating the User's personal account and browsing the Site, the person responsible for processing Personal Data is: Super Manager. <1>https://www.super-responsable.org</1> is represented by Alexandra Tence, its legal representative</1>


5000/5000
<1>As responsible for processing the data it collects, <1>https://www.super-responsable.org</1> undertakes to comply with the framework of the legal provisions in force. It is up to the Client in particular to establish the purposes of its data processing, to provide its prospects and clients, from the collection of their consents, with complete information on the processing of their personal data and to maintain a register of treatments in accordance with reality.
    Whenever <1>https://www.super-responsable.org</1> processes Personal Data, <1>https://www.super-responsable.org</1> takes all reasonable steps to ensure the accuracy and relevance of Personal Data with regard to the purposes for which <1>https://www.super-responsable.org</1> processes them.</1>
     
    <3>7.2 Purpose of the data collected </3>
     
    <1><1>https://www.super-responsable.org</1> is likely to process all or part of the data:</1>

    <4>
      
    <0>to allow navigation on the Site and the management and traceability of the services and services ordered by the user: connection and use data of the Site, invoicing, order history, etc.</0>
     
    <0>to prevent and fight against computer fraud (spamming, hacking ...): computer equipment used for browsing, IP address, password (hash)</0>
     
    <0>to improve navigation on the Site: connection and usage data</0>
     
    <0>to conduct optional satisfaction surveys on <0>https://www.super-responsable.org</0>: email address</0>
    <0>to conduct communication campaigns (sms, mail): phone number, email address</0>


    </4>

    <1><1>https://www.super-responsable.org</1> does not market your personal data which is therefore only used out of necessity or for statistical and analytical purposes.</1>
     
    <3>7.3 Right of access, rectification and opposition </3>
     
    <1>
    In accordance with current European regulations, Users of <1>https://www.super-responsable.org</1> have the following rights:</1>
     

    <4>

    <0>right of access (article 15 RGPD) and rectification (article 16 RGPD), updating, completeness of User data right to block or erase User personal data (article 17 of GDPR), when they are inaccurate, incomplete, equivocal, out of date, or the collection, use, communication or storage of which is prohibited</0>
     
    <0>right to withdraw consent at any time (article 13-2c RGPD)</0>
     
    <0>right to limit the processing of User data (article 18 RGPD)</0>
     
    <0>right to oppose the processing of User data (article 21 RGPD)</0>
     
    <0>right to portability of the data that Users have provided, when this data is subject to automated processing based on their consent or on a contract (article 20 RGPD)</0>
     
    <0>right to define the fate of Users' data after their death and to choose to whom <0>https://www.super-responsable.org</0> must communicate (or not) their data to a third party who 'they will have previously designated</0>
     </4>

    <1>As soon as <1>https://www.super-responsable.org</1> becomes aware of the death of a User and in the absence of instructions from him, <1>https: // www. super-responsable.org</1> undertakes to destroy its data, unless their retention is necessary for evidentiary purposes or to meet a legal obligation.</1>
     
    <1>If the User wishes to know how <1>https://www.super-responsable.org</1> uses their Personal Data, ask to rectify them or oppose their processing, the User can contact <1>https://www.super-responsable.org</1> in writing to the following address:</1>
     
    Super Manager - DPO, Loïse Fenoll <5 />
    90 av du Maréchal June 06400 Cannes.
     
    <1>In this case, the User must indicate the Personal Data that he would like <1>https://www.super-responsable.org</1> to correct, update or delete, by identifying himself precisely with a copy of an identity document (identity card or passport).</1>

    <1>
    Requests for the deletion of Personal Data will be subject to the obligations imposed on <1>https://www.super-responsable.org</1> by law, in particular with regard to the conservation or archiving of documents. Finally, Users of <1>https://www.super-responsable.org</1> can file a complaint with the supervisory authorities, and in particular the CNIL (https://www.cnil.fr/fr / complaints).</1>
     
    <3>7.4 Non-communication of personal data </3>

    <1><1>https://www.super-responsable.org</1> refrains from processing, hosting or transferring the Information collected on its Customers to a country located outside the European Union or recognized as "Not adequate" by the European Commission without first informing the customer. However, <1>https://www.super-responsable.org</1> remains free to choose its technical and commercial subcontractors on the condition that they present sufficient guarantees with regard to the requirements of the General Regulations on Data Protection (RGPD: n ° 2016-679).</1>

    <1><1>https://www.super-responsable.org</1> undertakes to take all the necessary precautions in order to preserve the security of the Information and in particular that it is not communicated to persons not authorized. However, if an incident impacting the integrity or confidentiality of Customer Information is brought to the attention of <1>https://www.super-responsable.org</1>, it must as soon as possible inform the Customer and inform him of the corrective measures taken. Furthermore <1>https://www.super-responsable.org</1> does not collect any "sensitive data".</1>

    <1>
    The User's Personal Data may be processed by subsidiaries of <1>https://www.super-responsable.org</1> and subcontractors (service providers), exclusively in order to achieve the purposes of this policy.</1>
    <1>
    Within the limits of their respective attributions and for the purposes mentioned above, the main persons likely to have access to the data of the Users of <1>https://www.super-responsable.org</1> are mainly the our customer service agents.</1>
    
    <2> <2> 7.5 Types of data collected </2> <0>Concerning the users of a Site <1>https://www.super-responsable.org</1>, we collect the following data which are essential for the operation of the service, and which will be kept for a maximum period of 9 months after the end of the contractual relationship: last name, first name, email</0> <0><0>https://www.super-responsable.org</0> also collects information that makes it possible to improve the user experience and to offer contextualized advice: Audience measurement data</0> <0>These data are kept without time limit</0> </2>


    <0>8. Incident notification</0>
    <1>
    No matter how hard you try, no method of transmission over the Internet or method of electronic storage is completely secure. We cannot therefore guarantee absolute security.
    If we become aware of a security breach, we will notify affected users so that they can take appropriate action. Our incident notification procedures take into account our legal obligations, whether at national or European level. We are committed to fully informing our clients of all matters relating to the security of their account and to providing them with all the information necessary to help them meet their own regulatory reporting obligations.</1>
    <1>
    No personal information of the user of the site <1>https://www.super-responsable.org</1> is published without the knowledge of the user, exchanged, transferred, assigned or sold on a medium any to third parties. Only the assumption of the repurchase of <1>https://www.super-responsable.org</1> and its rights would allow the transmission of said information to the possible purchaser who would in turn be bound by the same obligation of conservation and modification of data with regard to the user of the site <1>https://www.super-responsable.org</1>.</1>

    <3>Safety </3>

    <1>
    To ensure the security and confidentiality of Personal Data and Personal Health Data, <1>https://www.super-responsable.org</1> uses networks protected by standard devices such as a firewall, pseudonymization, encryption and password.</1>
     
    <1>
    When processing Personal Data, <1>https://www.super-responsable.org</1> takes all reasonable measures to protect them against any loss, misuse, unauthorized access, disclosure, alteration or destruction .</1>
     
    <0>9. Internet "cookies" and "tags" hypertext links</0>
     <1>
     The site <1>https://www.super-responsable.org</1> contains a number of hypertext links to other sites, set up with the permission of <1>https: // www. super- responsible.org</1>. However, <1>https://www.super-responsable.org</1> does not have the possibility to check the content of the sites thus visited, and will therefore not assume any responsibility for this fact.</1>
     Unless you decide to disable cookies, you agree that the site can use them. You can deactivate these cookies at any time free of charge using the deactivation possibilities offered to you and recalled below, knowing that this may reduce or prevent accessibility to all or part of the Services offered by the site.
     <1></1>

     <3>9.1. "COOKIES" </3>
     <1>
    A "cookie" is a small information file sent to the User's browser and saved in the User's terminal (eg: computer, smartphone), (hereinafter "Cookies"). This file includes information such as the User's domain name, the User's Internet service provider, the User's operating system, as well as the date and time of access. Cookies do not risk damaging the User's terminal.</1>
     <1><1>https://www.super-responsable.org</1> may process the User's information concerning his visit to the Site, such as the pages viewed, the searches carried out. This information allows <1>https://www.super-responsable.org</1> to improve the content of the Site, the navigation of the User.</1>
     <1>
    Cookies facilitating navigation and / or the provision of the services offered by the Site, the User can configure his browser so that it allows him to decide whether or not he wishes to accept them so that Cookies are saved in the terminal or, on the contrary, that they be rejected, either systematically or according to their issuer. The User can also configure his browser software so that the acceptance or rejection of Cookies is offered to him from time to time, before a Cookie is likely to be saved in his terminal. <1>https://www.super-responsable.org</1> informs the User that, in this case, it is possible that the functionalities of his browser software are not all available.</1>
     <1>
    If the User refuses the registration of Cookies in his terminal or his browser, or if the User deletes those registered there, the User is informed that his browsing and experience on the Site may be limited. This could also be the case when <1>https://www.super-responsable.org</1> or one of its service providers cannot recognize, for technical compatibility purposes, the type of browser used by the terminal, the language and display settings or the country from which the terminal appears to be connected to the Internet.</1>
     <1>
    Where applicable, <1>https://www.super-responsable.org</1> declines any responsibility for the consequences related to the degraded functioning of the Site and the services possibly offered by <1>https: //www.super -responsable.org</1>, resulting (i) from the refusal of Cookies by the User (ii) from the impossibility for <1>https://www.super-responsable.org</1> to register or to consult the Cookies necessary for their operation due to the choice of the User. For the management of Cookies and the choices of the User, the configuration of each browser is different. It is described in the browser's help menu, which will allow you to know how the User can modify their preferences in terms of Cookies.</1>
     <1>
    At any time, the User can choose to express and modify their wishes in terms of Cookies. <1>https://www.super-responsable.org</1> may also use the services of external providers to help collect and process the information described in this section.</1>
     <1>
    Finally, by clicking on the icons dedicated to the social networks Twitter, Facebook, Linkedin and Google Plus appearing on the Site of <1>https://www.super-responsable.org</1> or in its mobile application and if the '' User has accepted the deposit of cookies by continuing to browse the Website or the mobile application of <1>https://www.super-responsable.org</1>, Twitter, Facebook, Linkedin and Google Plus may also place cookies on your terminals (computer, tablet, mobile phone).</1>
     <1>
    These types of cookies are only placed on your terminals if you consent to them, by continuing to browse the Website or the mobile application of <1>https://www.super-responsable.org</1>. At any time, the User may nevertheless revoke their consent to <1>https://www.super-responsable.org</1> depositing this type of cookies.</1>

    <3>Article 9.2. INTERNET TAGS </3>
     

    <1>

    <1>https://www.super-responsable.org</1> may occasionally use Internet tags (also known as "tags", or action tags, single-pixel GIFs, transparent GIFs, invisible GIFs, and unmarked GIFs. à un) and deploy them through a specialist web analytics partner likely to be located (and therefore store the corresponding information, including the User's IP address) in a foreign country. </1>
     
    <1>
    These tags are placed both in online advertisements allowing Internet users to access the Site, and on its various pages.
    </1>
    <1>
    This technology allows <1>https://www.super-responsable.org</1> to evaluate visitors' responses to the Site and the effectiveness of its actions (for example, the number of times a page is open and the information has been consulted), as well as the use of this Site by the User.</1>
     <1>
    The external service provider may collect information on visitors to the Site and other websites using these tags, compile reports on the activity of the Site for the attention of <1>https: //www.super-responsable .org</1>, and provide other services relating to the use of this one and the Internet.</1>
     <1>
   </1> <0>10. Applicable law and attribution of jurisdiction.</0>
     <1>
    Any dispute in connection with the use of the <1>https://www.super-responsable.org</1> site is subject to French law.
    Except in cases where the law does not allow it, exclusive jurisdiction is attributed to the competent courts of Cannes</1>`,
    dialog: {
        shouldBeConnected: "Connect to perform this action",
        cookies: {
            title: "This website uses cookies (yum)",
            info: `We use cookies to give you the best possible experience. <0 />
            By clicking on "Accept all", you accept the use of cookies for technical purposes necessary for its proper functioning, as well as cookies, including third-party cookies, for statistical or personalization purposes to provide you with services and offers adapted to your interests on our site. To learn more and configure, click <1> HERE </1> `,
            button: "Accept all",
            areYouSure: {
                title: "Are you sure you want to deactivate us?",
                info: `We are nice cookies, and we help improve your user experience, but also those of all users! The statistics collected allow us to help the brands present on the site by giving them valuable advice, while others provide you with the best possible site.`,
                yes: "I deactivate",
                no: "I changed my mind"
            }
        },
        customerOpinion: {
            title: `Your opinion to win a <0>good deal</0>`,
            info: `Welcome to you young responsible ! What do you think of our website ? Something to improve ? What do you prefer ? Thank you for remaining courteous and respectful`,
            already: "You have already posted a review with this email address, thank you!"
        },
        newAchievement: {
            info: "Your actions have unlocked <0/>a good deal",
            button: "Discover"
        },
        createAccount: {
            info: "Log in to unlock <0/> your good deal now",
            button: "Let's go"
        }
    },
    newsletter: {
        title: "Our great news in your mailbox",
        info: "Subscribe to the newsletter to receive a good plan and discover the news in your mailbox once a month",
        button: "Send",
        toast: {
            subscribeSuccess: "Thank you, you have subscribed to our great newsletter",
            subscribeError: "An error occurred while subscribing to the newsletter, contact us"
        }
    },
    cookies: {
        info: `<0> Find out more and configure cookies </0>
        The purpose of this page is to inform you about the use of cookies on our sites and to show you how to set them.
        
        <1/><1/> <0> What is a cookie? </0>
        Cookies are small text files placed on your computer / tablet / smartphone / connected television / video game console connected to the Internet when visiting a site. Their purpose is to collect information relating to your browsing and to send you personalized offers and services.
        <1 /> Cookies are managed by your internet browser.
        
        <1/><1/> <0> Why do we use cookies? </0>
        They allow us to check the proper functioning of the site and allow us, for example, to record your preferences, the validation of your subscription, your identification with your customer area.
        <1 /> We also use cookies to measure the audience of our sites. In this way we can analyze the traffic and use of our sites and constantly improve our services and content.
        <1 /> There are also cookies issued by third parties to monitor targeted advertising operations or share content via social networks for example.
        
        <1/><1/> <0> How to accept or refuse cookies? </0>
        To manage your preferences, activate or deactivate the cookies below, then click on VALIDATE`,
        statistics: {
            info: `These are cookies that allow us to know the use and performance of the site and to improve its functioning (eg: the most frequently visited pages, searches, ...) `,
            title: "Statistics data"
        },
        necessary: {
            info: `Cookies essential for the proper functioning of the site, they cannot be deactivated from our systems. If these cookies are blocked by your browser, certain elements of the site will not be able to function.`,
            title: "Essential data"
        },
    },
    cnil: `For more information on cookies, visit the <0> CNIL website. </0>`,
    or: "or"
}