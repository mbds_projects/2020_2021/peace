export const fr = {
    home: {
        latestVendorsAdded: 'Derniers ajouts',
        aroundPositionVendors: 'Autour de toi',
        aroundNice: 'Autour de Nice',
        genders: "Genres",
        categories: "Catégories",
        bestRateVendors: 'Les mieux notés',
        moreFavoritesVendors: 'Les favoris de la communauté',
        vendorsAssimilate: 'Recommandés pour toi',
        articles: "Derniers articles",
        youtube: "Dernières vidéos",
        charter: {
            title: "Nos valeurs",
            text: "Nous mettons en avant les enseignes engagées parmi 50 nuances de valeurs saines, éthiques et durables. Découvre-les dans ",
            link: "notre charte responsable"
        },
        fbGroup: {
            title: "Super Responsable Coulisses",
            text: "Ce site communautaire est aussi le tien ! Rejoins le groupe Facebook ",
            text2: " pour poser ton empreinte sur le site, échanger directement avec l’équipe et prendre part à nos sondages. A très vite dans les coulisses",
            link: "Super Responsable Coulisses"
        },
        instaRecent: "Publication récentes",
        keywords: "Tendances de la communauté",
        bestComments: "Meilleurs avis"
    },
    cgu: {
        title: "Conditions générales d'utilisation",
        date: "En vigueur au 01/07/2020",
        def: `Les présentes conditions générales d'utilisation (dites « <0>CGU</0> ») ont pour objet l'encadrement juridique des modalités de mise à disposition du site et des services par Super Responsable et de définir les conditions d’accès et d’utilisation des services par « <0>l'Utilisateur</0> ».
        <1/><1/>Les présentes CGU sont accessibles sur le site à la rubrique «<0>CGU</0>».
        <1/><1/>Toute inscription ou utilisation du site implique l'acceptation sans aucune réserve ni restriction des présentes CGU par l’utilisateur. Lors de l'inscription sur le site via le Formulaire d’inscription, chaque utilisateur accepte expressément les présentes CGU en cochant la case précédant le texte suivant : « Je reconnais avoir lu et compris les CGU et je les accepte ».
        <1/><1/>En cas de non-acceptation des CGU stipulées dans le présent contrat, l'Utilisateur se doit de renoncer à l'accès des services proposés par le site.
        <1/><1/>https://super-responsable.org/ se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes CGU.`,
        legalNotice: {
            title: "Article 1 : Les mentions légales",
            def: `L'édition du site https://super-responsable.org/ est assurée par la Société SAS Super Responsable en cours de création, dont le siège social est situé au Business Pôle - Bt A - Bureau 209 1047 route des Dolines. Allée Pierre Ziller, 06560 Sophia Antipolis
            <0/><0/>Adresse e-mail : bonjour@super-responsable.org.
            <0/><0/>Le Directeur de la publication est : Alexandra Tence
            <0/><0/>L'hébergeur du site https://super-responsable.org/ est la société Heroku Incorporated, dont le siège social est situé au 650 7th Street, San Francisco, avec le numéro de téléphone : +33 1 (877) 563-4311.`
        },
        webSiteAccess: {
            title: "ARTICLE 2 : Accès au site",
            def: `Le site https://super-responsable.org/ permet à l'Utilisateur un accès gratuit aux services suivants :
            <0/><0/>Le site internet propose les services suivants :
            <0/><0/>Découvrir les commerces corresponds à un certains nombres de critères sur une sélection basé sur la charte responsable du site, Découvrir les commerces autour de soi basé sur la géolocalisation, Créer un compte utilisateur, Mettre des commerces en favoris,Compléter sa fiche profil avec ses valeurs et ses informations afin d'obtenir des suggestions de commerces basées sur ses valeurs, Suggérer des commerces en tant qu'utilisateur inscrit pour qu'ils soient à l'étude, Suggérer son propre commerce en tant qu'enseigne, Découvrir les fiches de commerces contenant des informations visuelles et des liens, Commenter et donner son avis sur un commerce en tant qu’utilisateur inscrit, S’inscrire et se désinscrire à la chaine d’information
            <0/><0/>Le site est accessible gratuitement en tout lieu à tout Utilisateur ayant un accès à Internet. Tous les frais supportés par l'Utilisateur pour accéder au service (matériel informatique, logiciels, connexion Internet, etc.) sont à sa charge.
            <0/><0/>L’Utilisateur non membre n'a pas accès aux services réservés. Pour cela, il doit s’inscrire en remplissant le formulaire. En acceptant de s’inscrire aux services réservés, l’Utilisateur membre s’engage à fournir des informations sincères et exactes concernant son état civil et ses coordonnées, notamment son adresse email.
            <0/><0/>Pour accéder aux services, l’Utilisateur doit ensuite s'identifier à l'aide de son identifiant et de son mot de passe utilisés lors de son inscription.
            <0/><0/>Tout Utilisateur membre régulièrement inscrit pourra également solliciter sa désinscription en se rendant à la page de contact. Celle-ci sera effective dans un délai raisonnable.
            <0/><0/>Tout événement dû à un cas de force majeure ayant pour conséquence un dysfonctionnement du site ou serveur et sous réserve de toute interruption ou modification en cas de maintenance, n'engage pas la responsabilité de https://super-responsable.org/. Dans ces cas, l’Utilisateur accepte ainsi ne pas tenir rigueur à l’éditeur de toute interruption ou suspension de service, même sans préavis.
            <0/><0/>L'Utilisateur a la possibilité de contacter le site par messagerie électronique à l’adresse email de l’éditeur communiqué à l’ARTICLE 1.`
        },
        rgpd: {
            title: "ARTICLE 3 : Collecte des données",
            def: `Le site assure à l'Utilisateur une collecte et un traitement d'informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. Conformément aux directives RGPD en vigueur depuis le 25 mai 2018 voici le registre des traitements:
            <0/><0/>L’entreprise Super Responsable est la seule à intervenir dans le traitement des données relatives aux informations personnelles. Ces données sont: le nom, prénom, l’adresse mail, le mot de passe d’inscription et la date de naissance. Ces données servent à contacter l’utilisateur, ainsi qu’établir des statistiques sur les tranches d’âges des utilisateurs. Aucune donnée personnelle n’est communiqué et seule l’entreprise Super Responsable y à accès. Elles sont conservées tant que le compte utilisateur est actif. Les données sont sauvegardées en base de donnée, et le mot de passe est crypté de sorte de n’être jamais visible en clair pour qui que ce soit, y compris l’entreprise Super Responsable. En ce sens, en cas de mot de passe oublié, une demande devra être faite sur le site en cliquant sur « mot de passe oublié », et l’utilisateur recevra un mail dans les minutes suivantes afin d’en généré un nouveau. Aucun mot de passe ne pour donc jamais être fournis par mail.
            <0/><0/>En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978, l'Utilisateur dispose d'un droit d'accès, de rectification, de suppression et d'opposition de ses données personnelles. L'Utilisateur exerce ce droit :
            <0/>· par mail à l'adresse mail bonjour@super-responsable.org
            <0/>· via son espace personnel ;`
        },
        credit: {
            title: "ARTICLE 4 : Propriété intellectuelle",
            def: `Les marques, logos, signes ainsi que tous les contenus du site (textes, images, son…) font l'objet d'une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d'auteur.
            <0/><0/>La marque Super Responsable est une marque déposée par Alexandra Tence.Toute représentation et/ou reproduction et/ou exploitation partielle ou totale de cette marque, de quelque nature que ce soit, est totalement prohibée.
            <0/><0/>L'Utilisateur doit solliciter l'autorisation préalable du site pour toute reproduction, publication, copie des différents contenus. Il s'engage à une utilisation des contenus du site dans un cadre strictement privé, toute utilisation à des fins commerciales et publicitaires est strictement interdite.
            <0/><0/>Toute représentation totale ou partielle de ce site par quelque procédé que ce soit, sans l’autorisation expresse de l’exploitant du site Internet constituerait une contrefaçon sanctionnée par l’article L 335-2 et suivants du Code de la propriété intellectuelle.
            <0/><0/>Il est rappelé conformément à l’article L122-5 du Code de propriété intellectuelle que l’Utilisateur qui reproduit, copie ou publie le contenu protégé doit citer l’auteur et sa source.
            <0/><0/>Certains médias présents sur le site font l’objet de crédits d’utilisation: 
            <0/>· certaines images proviennent du site https://pixabay.com/fr/ et sont sous licence « Simplified Pixabay License » 
            <0/>· certaines images proviennent du site https://unsplash.com/ et sont sous licence « Unsplash License » 
            <0/>· certaines icones proviennent du site https://www.flaticon.com/ et sont sous licence « designed from Flaticon » `
        },
        responsability: {
            title: "ARTICLE 5 : Responsabilité",
            def: `Les sources des informations diffusées sur le site https://super-responsable.org/ sont réputées fiables mais le site ne garantit pas qu’il soit exempt de défauts, d’erreurs ou d’omissions.
            <0/><0/>Les informations communiquées sont présentées à titre indicatif et général sans valeur contractuelle. Malgré des mises à jour régulières, le site https://super-responsable.org/ ne peut être tenu responsable de la modification des dispositions administratives et juridiques survenant après la publication. De même, le site ne peut être tenue responsable de l’utilisation et de l’interprétation de l’information contenue dans ce site.
            <0/><0/>L'Utilisateur s'assure de garder son mot de passe secret. Toute divulgation du mot de passe, quelle que soit sa forme, est interdite. Il assume les risques liés à l'utilisation de son identifiant et mot de passe. Le site décline toute responsabilité.
            <0/><0/>Le site https://super-responsable.org/ ne peut être tenu pour responsable d’éventuels virus qui pourraient infecter l’ordinateur ou tout matériel informatique de l’Internaute, suite à une utilisation, à l’accès, ou au téléchargement provenant de ce site.
            <0/><0/>La responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d'un tiers.`
        },
        link: {
            title: "ARTICLE 6 : Liens hypertextes",
            def: `Des liens hypertextes peuvent être présents sur le site. L’Utilisateur est informé qu’en cliquant sur ces liens, il sortira du site https://super-responsable.org/. Ce dernier n’a pas de contrôle sur les pages web sur lesquelles aboutissent ces liens et ne saurait, en aucun cas, être responsable de leur contenu.`
        },
        cookies: {
            title: "ARTICLE 7 : Cookies",
            def: `L’Utilisateur est informé que lors de ses visites sur le site, un cookie peut s’installer automatiquement sur son logiciel de navigation.
            <0/><0/>Les cookies sont de petits fichiers stockés temporairement sur le disque dur de l’ordinateur de l’Utilisateur par votre navigateur et qui sont nécessaires à l’utilisation du site https://super-responsable.org/. Les cookies ne contiennent pas d’information personnelle et ne peuvent pas être utilisés pour identifier quelqu’un. Un cookie contient un identifiant unique, généré aléatoirement et donc anonyme. Certains cookies expirent à la fin de la visite de l’Utilisateur, d’autres restent.
            <0/><0/>L’information contenue dans les cookies est utilisée pour améliorer le site https://super-responsable.org/.
            <0/><0/>En naviguant sur le site, L’Utilisateur les accepte.
            <0/><0/>L’Utilisateur pourra désactiver ces cookies par l’intermédiaire des paramètres figurant au sein de son logiciel de navigation.`
        },
        publishing: {
            title: "ARTICLE 8 : Publication par l’Utilisateur",
            def: `Le site permet aux membres de publier les contenus suivants :
            <0/><0/>Commentaires et Avis sur un commerce.
            <0/><0/>Dans ses publications, le membre s’engage à respecter les règles de la Netiquette (règles de bonne conduite de l’internet) et les règles de droit en vigueur.
            <0/><0/>Le site peut exercer une modération sur les publications et se réserve le droit de refuser leur mise en ligne, sans avoir à s’en justifier auprès du membre.
            <0/><0/>Le membre reste titulaire de l’intégralité de ses droits de propriété intellectuelle. Mais en publiant une publication sur le site, il cède à la société éditrice le droit non exclusif et gratuit de représenter, reproduire, adapter, modifier, diffuser et distribuer sa publication, directement ou par un tiers autorisé, dans le monde entier, sur tout support (numérique ou physique), pour la durée de la propriété intellectuelle. Le Membre cède notamment le droit d'utiliser sa publication sur internet et sur les réseaux de téléphonie mobile.
            <0/><0/>La société éditrice s'engage à faire figurer le nom du membre à proximité de chaque utilisation de sa publication.
            <0/><0/>Tout contenu mis en ligne par l'Utilisateur est de sa seule responsabilité. L'Utilisateur s'engage à ne pas mettre en ligne de contenus pouvant porter atteinte aux intérêts de tierces personnes. Tout recours en justice engagé par un tiers lésé contre le site sera pris en charge par l'Utilisateur.
            <0/><0/>Le contenu de l'Utilisateur peut être à tout moment et pour n'importe quelle raison supprimé ou modifié par le site, sans préavis.`
        },
        law: {
            title: "ARTICLE 9 : Droit applicable et juridiction compétente",
            def: `La législation française s'applique au présent contrat. En cas d'absence de résolution amiable d'un litige né entre les parties, les tribunaux français seront seuls compétents pour en connaître.
            <0/><0/>Pour toute question relative à l’application des présentes CGU, vous pouvez joindre l’éditeur aux coordonnées inscrites à l’ARTICLE 1.`
        }
    },
    footer: {
        rgpd: 'RGPD',
        charter: 'La charte responsable',
        aboutUs: 'Qui sommes-nous',
        candidate: 'Nous rejoindre',
        partners: 'Nos partenaires',
        talkOfUs: 'On parle de nous !',
        blog: 'Le Super Blog',
        contact: 'Contact',
        stayTuned: 'Suis nos aventures super responsables',
        suggestBrand: 'Partager mes adresses',
        vendorCandidate: 'Je suis une enseigne',
        needHelp: "Besoin d'aide ?",
        faq: "FAQ",
        cgu: "CGU",
        actualities: "Notre super blog",
        contribute: "Contribuer",
        favorites: "Favoris",
        rewards: "Bons Plans",
        directory: "Annuaire"
    },
    navbar: {
        account: "Mon compte",
        search: "Chercher",
        searchPlaceholder: "Chercher un commerce",
        shops: "Les adresses partagées",
        blog: "SUPER BLOG",
        aboutUs: "Qui sommes nous ?",
        revealShop: 'Je suis une enseigne ?',
        suggestBrand: 'Partager mes adresses',
        profile: "Profil",
        favorites: "Favoris",
        rewards: "Les bons plans",
        myRewards: "Mes bons plans",
        adminDashboard: "Dashboard admin",
        logout: "Déconnexion",
        searchDialog: "Que cherches-tu ?",
        dashboard: "Ma page",
        tutorial: "Mes tutos",
        suggestedForYou: "Recommandé pour toi"
    },
    login: {
        toast: {
            mailAndPasswordRequired: 'Les champs email et mot de passe sont requis',
            invalidLogin: "Identifiants invalides",
            blockedUser: 'Ce profil a été bloqué car il ne respectait pas nos CGU',
            somethingWrongAppends: 'Connexion à ce compte impossible, merci de contacter le support',
            facebookAccount: "Cette adresse mail est lié à un compte facebook, connecte-toi avec",
            messageFirefoxSupport: "Firefox bloque l'utilisation de facebook, pour pouvoir vous connecter avec facebook, merci de <0>{{linkTextFirefoxSupport}}</0>",
            linkTextFirefoxSupport: "désactiver le blocage de contenu pour super responsable",
            linkFirefoxSupport: "https://support.mozilla.org/fr/kb/blocage-de-contenu#w_daesactiver-le-blocage-de-contenu-pour-des-sites-praecis",
            notExist: `Il n'y a pas de compte lié à cet e-mail, clique sur "S'inscrire" pour créer ton compte`,
            otherFacebookAccount: "Un autre compte Facebook est lié à cet e-mail",
            internalAccount: "Cette adresse e-mail n'est pas liée à un compte Facebook. Entrez votre mot de passe ou cliquez sur mot de passe oublié"
        },
        enterMail: {
            title: "Connexion / Inscription",
            subTitle: "Entre ton e-mail pour te connecter ou créer un compte",
        },
        title: "Se connecter",
        subTitle: "Entre ton mot de passe",
        button: "Se connecter",
        forgotPasswordLink: "Mot de passe oublié ?",
        connectionFacebook: "Se connecter avec facebook",
        connectionOrCreate: "Connexion / Inscription",
        fbButton: "Connecte-toi ou crée ton compte en un seul clic",
        facebook: {
            title: "Cette adresse mail est lié à un compte facebook",
            subtitle: "Connecte-toi en un clic",
        } ,
        candidate: "Devenir un commerce Super Responsable"
    },
    signin: {
        toast: {
            mailAndPasswordRequired: "Les champs prénom, nom, email, mot de passe sont requis",
            wrongDetails: "Informations incorrectes",
            mailAlreadyExist: "Cet email est déjà enregistré, connecte-toi avec",
            facebookAlreadyExist: "Ce compte facebook est déjà enregistré, connecte-toi avec"
        },
        title: "Création de compte",
        subTitle: "Complete les informations ci-dessous",
        button: "S'inscrire",
        connectionFacebook: "S'inscrire avec facebook",
        loginLink: "Déjà inscrit ?"
    },
    forgotPassword: {
        toast: {
            mailUnknown: "Cette adresse mail est inconnue, creez votre compte dès maintenant !",
            mailSent: "Demande envoyé, vous allez recevoir un mail",
            facebookAccount: "Cette adresse mail correspond à un compte facebook, essaie de te connecter avec ce compte"
        },
        title: "Mot de passe oublié",
        button: "Envoyer"
    },
    passwordRecreate: {
        title: "Créer un nouveau mot de passe",
        button: "Envoyer"
    },
    contact: {
        title: "Contacte-nous",
        subject: "Choisir un sujet",
        questions: {
            candidate: "Je souhaite candidater pour Super Responsable",
            cost: "Je suis une enseigne et souhaite obtenir un devis",
            support: "J'ai un problème technique",
            brand: "Je suis une enseigne et souhaite être recensée",
            contact: "Je voudrais contacter l'équipe Super Responsable"
        },
        message: "Ton message ici ...",
        button: "Envoyer",
        toast: {
            messageSent: "Message envoyé",
            errorSent: "Une erreur est survenue lors de l'envoie du message"
        }
    },
    suggest: {
        searchPlaceholder: "Renseigne le nom d'une enseigne",
        title: "Partage avec nous tes adresses <0>coup de coeur</0>",
        comment: "Raconte-nous ton expérience, pourquoi cette enseigne devrait être recensée",
        alreadySuggested: "Tu as déjà suggéré cette enseigne, merci !",
        site: "Site web",
        suggestOther: "N'hésite pas à en suggerer d'autres.",
        newSuggested: "Merci d'avoir ajouté cette enseigne, tu es le ",
        first: "premier",
        th: 'ieme',
        opinion: "Qu'as tu pensé de cette expérience ?",
        button: "Suggérer",
        required: "requis",
        toast: {
            opinionNotSent: "Une erreur est survenue, ton avis n'a pu être pris en compte",
            couldNotBeAddedInFav: "Une erreur est survenue, l'enseigne n'a pu être ajouté en favoris"
        }
    },
    profile: {
        accountSettings: "Paramètre de compte",
        newsletter: "Recevoir la newsletter",
        newsletterAdvisor: "Ne t'inquiète pas, on pense à la planète, tu n'en recevras pas beaucoup ! ",
        values: "Mes valeurs",
        contact: "Contact",
        keywords: "Valeurs",
        personalDetails: "Informations personnelles",
        updatePassword: "Changer son mot de passe",
        currentPassword: "Mot de passe actuel",
        newPassword: "Nouveau mot de passe",
        button: "Sauvegarder",
        achievements: "Actions réalisées",
        completeProfile: "Complète ton profil et obtient un bon plan",
        useDiamonds: "Utiliser mes points",
        winDiamonds: "Gagner des points",
        nextStep: {
            text: "Prochaine étape : ",
            valuesRemaining: " valeur(s) à choisir",
            profilePic: "Ajoutes une photo de profil",
            birthday: "Remplis ta date de naissance"
        },
        toast: {
            wrongCurrentPassword: "Le mot de passe actuel est invalide"
        }
    },
    achievements: {
        registration: "Inscription sur le site",
        complete_profile: "Profil complété",
        newsletter: "Inscription à la newsletter",
        first_favorite: "Ajout du premier favoris",
        five_favorite: "Ajout de 5 favoris",
        first_suggestion: "Partage de sa première adresse",
        five_suggestion: "Partage de 5 adresses",
        ten_suggestion: "Partage de 10 adresses",
        twenty_suggestion: "Partage de 20 adresses",
        fifty_suggestion: "Partage de 50 adresses",
        first_rate: "Premier avis posté",
        five_rate: "5 avis postés",
        ten_rate: "10 avis postés",
        twenty_rate: "20 avis postés",
        fifty_rate: "50 avis postés",
        survey_satisfaction: "Questionnaire de satisfaction",
        noNextActions: "De nouvelles actions arrivent bientôt !",
        noActionsAlready: "Aucune action réalisée",
        site_opinion: "Avis à propos de notre site",
        slowActions: "Certaines actions, comme les sondages, peuvent être attribuées avec un certain délais."
    },
    vendorList: {
        categories: "Catégories",
        keywords: "Filtrer par valeurs",
        values: "Mes valeurs",
        healthy: "Sain",
        ethical: "Ethique",
        sustainable: "Durable",
        cities: "Filtrer par villes",
        genders: "Genres",
        filterButton: "Ouvrir les filtres",
        onlineShop: "Livraison à domicile",
        clickAndCollect: "Retrait en magasin",
        aroundMe: "Autour de moi",
        reward: "Bons plans",
        userDeniedGeoloc: "La géolocalisation est bloquée par ce navigateur ! Change tes paramètres avant d'activer la géolocalisation.",
        shouldBeLogged: "Connecte-toi pour filtrer par tes valeurs",
        noValues: {
            message: "Aucune valeur n'a été renseigné sur ton profil, <0>{{linkText}}</0>",
            linkText: "complète-le"
        },
        emptyState: {
            title: "Aucun résultat trouvé",
            subTitle: "Ces critères de recherche combinés ne donnent aucun résultat"
        },
        notFound: {
            title: "Aucun résultat trouvé",
            subTitle: "Il se peut que personne n'ait proposé d'enseignes pour ce filtre ou quelles soient en cours de vérification.",
            link: "Tu en connais ? Partage tes adresses !"
        },
    },
    favorites: {
        title: "Mes adresses favorites",
        subTitle: "<0/>Ceci est mon carnet d’adresses. Il y en a beaucoup d’autres comme lui, mais celui-là c’est le mien.<1/> Sans mon carnet d’adresses, je ne peux pas shopper. Et sans moi, mon carnet d’adresses n’est rien.<0/>",
        notFound: {
            title: "Aucun favoris ajouté",
            subTitle: "Tu n'as ajouté aucune enseigne en favoris",
            link: "Découvre les dès maintenant !"
        }
    },
    suggestedForYou: {
        title: "Recommandé pour moi",
        subTitle: "Retrouve ici les marques susceptibles de te correspondre.</0> Les recommandations sont basées sur les valeurs que tu as sélectionnées sur ton profil.",
        notFound: {
            title: "Aucune recommandation disponible",
            subTitle: "Nous ne pouvons te proposer aucune recommandation basé sur tes valeurs. Reviens plus tard ou mets à jour tes valeurs",
            link: "Gérer mes valeurs"
        }
    },
    articlesB2B: {
        title: "Mes tutos",
        subTitle: "Retrouvez ici nos tutoriaux dédiés aux entreprises.",
        notFound: {
            title: "Aucun tutorial n'est disponible",
            subTitle: "De nouveaux tutoriaux arriveront bientôt, revenez visiter cette page régulierement."
        }
    },
    vendorCandidate: {
        slogan: "VOUS AVEZ DES VALEURS ET CELA DOIT SE SAVOIR",
        catchPhrase: {
            title: "Maximisez votre présence digitale de façon responsable",
            text: "Vous souhaitez augmenter votre e-réputation, être au fait des tendances du marché et bénéficier d'un entourage bienveillant et solidaire, le tout à un prix abordable et sans engagement ? Alors vous ne pouviez pas mieux tomber"
        },
        reveal: {
            title: "Révélez le meilleur de votre univers en 'one shot' ",
            text: `<0>Votre page tout-en-un mixe storytelling, valeurs et distinctions, points de vente, bons plans, top ventes, présences digitales, 3 photos et 1 vidéo</0>
            <0>Les internautes peuvent garder votre enseigne dans leurs favoris, partager votre page et exprimer leurs avis</0>
            <0>Vous êtes mis en avant dans nos supports de communication: posts, articles, vidéos, newsletters, avis de la rédac', publicités</0>`
        },
        analyse: {
            title: "Analysez vos performances et suivez l'évolution de votre secteur ",
            text: `<0>Nos utilisateurs vous aiguillent par leurs enseignes, tendances et valeurs les plus appréciées</0>
            <0>Recevez votre étude de marché trimestrielle réalisée auprès de nos internautes, partenaires et enseignes référencées</0>
            <0>Les données de localisation, popularité des réseaux sociaux, top ventes et autres statistiques sont à retrouver sur votre page <0>(bientôt disponible)</0></0>`
        },
        join: {
            title: "Rejoignez un réseau d'acteurs tendances et responsables",
            text: `<0>Vos bons plans partagés auprès de nos internautes créent un lien durable: codes promo, invitations à des événements et petits cadeaux sont les bienvenus</0>
            <0>Deux options, contrôler votre page ou nous laisser faire: création, mises à jour de vos éléments sur demande, gestion de vos bons plans, on s'occupe de tout !</0>
            <0>Vos produits responsables aux engagements élevés et assurément modernes intègrent notre marketplace<0>(bientôt disponible)</0></0>`
        },
        integrate: {
            title: "Intégrez notre site communautaire dès aujourd'hui",
            text: "Nos formules sans engagement ont été réfléchies auprès d'enseignes référencées afin de répondre au mieux à vos objectifs selon votre budget"
        },
        questions: `Des questions ? <0>Contactez-nous</0>`, 
        aboutUs: {
            title: "A propos de Super Responsable",
            text: `Nous mettons en relation les commerces et les consommateurs responsables de France sur notre site communautaire conçu pour booster votre visibilité, soigner votre image, augmenter votre notoriété, intégrer un réseaux d’acteurs responsables et acquérir de nouveaux leads qualifiés. <0>Qui sommes-nous</0>   `
        },
        numbers: {
            reboundRate: {
                title: "taux de rebond",
                value: "1,62%"
            },
            recommendation: {
                title: "nous recommandent*",
                value: "100%"
            },
            county: {
                title: "departements",
                value: "85"
            },
            values: {
                title: "valeurs",
                value: "50"
            },
            followers: {
                title: "followers",
                value: "+ 5000"
            },
            youtube: {
                title: "vues Youtube",
                value: "+ 14 000"
            },
            note: "*Réponse au sondage du 10/07/2020 réalisé auprès de 14 enseignes référencées"
        },
        form: {
            title: "JE CREE MA PAGE D'ENSEIGNE",
            subtitle: "Merci de remplir les champs ci-dessous"
        },
        categories: "Catégories",
        keywords: "Valeurs",
        toast: {
            creationError: "Une erreur est survenue lors de l'enregistrement de votre enseigne",
            creationSuccess: "L'enregistrement de votre enseigne à été effectué, merci de votre intérêt, nous allons vous contacter très prochainement",
            vendorCandidateSaveInfo: "Enregistrement réussi"
        }
    },
    reward: {
        copied: "Code copié dans le presse-papier",
        unlockTitle: "Débloquer ce bon plan ?",
        unlockReward: "Débloquer ce bon plan consomme 1 point",
        cancel: "Plus tard",
        unlock: "Utiliser un point",
        howToObtainDiamond: "Comment obtenir des points ?",
        diamondExplanation: "Les points sont obtenus lors d'actions faites sur le site communautaire.<0/>Chaque action rapporte un ou plusieurs points.<0/><1>Comment obtenir des points ?</1>",
        nextActions: "Prochaines actions",
        alreadyAchieved: "Actions réalisées",
        missingDiamondTitle: "Oups, il te manque des points !",
        missingDiamond: "Tu n'as pas obtenu assez de points pour pouvoir débloquer ce bon plan. Une foule d'actions t'attendent",
        close: "Fermer",
        alreadyUnlocked: "Bons plans déjà débloqués",
        availableToUnlock: "Bons plans disponibles",
        addButton: "Ajouter un bon plan",
        create: "Créer un nouveau bon plan",
        message: "Les détails des conditions",
        code: "Le code à utiliser sur votre site",
        value: "La valeur de l'offre (10%, 20€, livraison offerte ...)",
        type: "Le type d'offre",
        voucher: "Une réduction",
        gift: "Un cadeau",
        event: "Une invitation",
        validity: "La date de validité (facultatif)",
        other: "Autre",
        toast: {
            creationFail: "Une erreur est survenue lors de la création du bon plan",
            disableFail: "Une erreur est survenue lors de l'inactivation du bon plan",
            enableFail: "Une erreur est survenue lors de l'activation du bon plan"
        },
        activated: "Mes bons plans actifs",
        disabled: "Mes bons plans inactifs",
        disable: "Rendre inactif",
        enable: "Rendre actif",
        disabledTitle: "Rendre inactif ce bon plan ?",
        disableInfo: "Êtes-vous sûr ? Ce bon plan ne sera plus visible par les utilisateurs",
        enabledTitle: "Rendre actif ce bon plan ?",
        enableInfo: "Êtes-vous sûr ? Ce bon plan sera visible par les utilisateurs"
    },
    vendorProfile: {
        info: "Ces informations ont été fournies par l'entreprise et vérifiées par notre équipe. Pour ce faire, nous nous appuyons sur les labels, associations et ONG, experts du secteur, et ce qui fait notre force, la communauté qui révèle, enquête et teste les enseignes. Une information vous semble inexacte ? <0>Merci de nous en informer</0>",
        searchPlaceholder: "Chercher une ville, un département",
        more: "Lire la suite",
        less: "Réduire",
        addComment: "Ajouter un commentaire",
        comments: "Commentaires",
        vendorsAssimilate: "Tu aimeras surement",
        toast: {
            commentNotSend: "Une erreur est survenue, le commentaire n'a pu être ajouté",
            reportComment: "Merci d'avoir signaler ce commentaire, nous allons l'étudier prochainement !",
            reportCommentError: "Une erreur s'est produite, ce commentaire n'a pu être signalé"
        },
        filters: "filtres",
        cities: "ville(s)",
        ourStory: "Notre histoire",
        labels: {
            title: "Labels portés par ",
            desc: "Certains articles de cette enseigne sont labellisés"
        },
        map: "Boutiques & revendeurs",
        follow: "Suivre",
        alsoAvailableOn: "Egalement disponible sur :",
        rewards: {
            title: "<0>Les bons plans de {{vendorName}}</0><1/><1/>",
            explanation: "{{points}} bon plan est disponible pour cette enseigne<0/><1>Profites-en vite !</1>",
            explanations: "{{points}} bons plans sont disponibles pour cette enseigne<0/><1>Profites-en vite !</1>",
        },
        share: 'Partager'
    },
    onlineShop: {
        website: "Boutique en ligne",
        noPointOfSale: "Pas de point de vente"
    },
    comments: {
        userDeleted: "Utilisateur supprimé",
        about: "A propos de : "
    },
    shareComments: {
        title: "Partage ton avis",
        rate: "Note générale :",
        text: "Commentaire: ",
        button: "Envoyer",
        textPlaceholder: "Merci d'être courtois et respectueux."
    },
    adminDashboard: {
        activated: "activé",
        published: "publié",
        keywords: "valeurs",
        descFr: "storytelling Fr",
        descEn: "storytelling En",
        vendors: "vendeurs",
        vendorsCorresponding: "Liste des vendeurs correspondant",
        suggestedBy: "Suggeré par",
        mode: "Mode",
        imagesCarousel: "Images du carousel",
        uploadImageCarousel: "Mettre à jour une image",
        title: "Dashboard administrateur",
        userAndCommentManager: "Gestion utilisateurs et commentaires",
        suggestedBrandManager: "Gestion des marques",
        potentialBrandManager: "Gestion des fournisseurs",
        instagramSuggestedBrandManager: "Gestion des marques suggéré sur instagram",
        reportedComments: "Commentaires signalés",
        linkToVendor: "Lien vers la boutique",
        deleteComAndReportUser: "Supprimer le commentaire et signaler l'utilisateur",
        noCommentsReported: "Aucun commentaire signalé",
        usersReportedMoreThan3: "Utilisateurs signalés 3 fois ou plus",
        reported: "Signalé",
        time: "fois",
        blockUser: "Bloquer l'utilisateur",
        noUsersReported: "Aucun utilisateur signalé",
        usersBlocked: "Utilisateurs bloqués",
        blockedSince: "Bloqué depuis le",
        unblockUser: "Débloqer l'utilisateur",
        noUsersBlocked: "Aucun utilisateur bloqué",
        deleteBrand: "Supprimer une enseigne; Es-tu sûr ?",
        contactBrand: "Contacter une enseigne; Es-tu sûr ?",
        updateBrand: "Editer l'enseigne",
        createBrand: "Créer une enseigne",
        cancel: "Annuler",
        delete: "Supprimer",
        update: "Editer",
        create: "Créer",
        contact: "Contacter",
        search: "Entre un nom à chercher",
        name: "Nom",
        occurrence: "Occurrence",
        checked: "Validé",
        createdAt: "Crée le",
        updatedAt: "Edité le",
        mail: "Addresse mail",
        site: "Site web",
        note: "Note personnelle",
        instagram: "Id instagram",
        messages: "Messages",
        alreadyContacted: "Déjà contacté",
        send: "Envoyer",
        transform: "Transformer en vendeur",
        brandToShop: "Transformer une enseigne suggéré en vendeur; Es-tu sûr ?",
        labels: "Labels",
        image: "Image",
        upload: "upload",
        updateLabel: "Editer le label",
        createLabel: "Créer un label",
        uploadLabel: "Mettre en ligne le logo du label",
        locations: "Points de vente",
        createLocation: "Créer un point de vente",
        updateLocation: "Mettre à jour un point de vente",
        location: {
            city: "Ville",
            address: "Addresse",
            postalCode: "Code postal",
            openingHours: "Horaires",
            phoneNumber: "Téléphone",
            webSite: "Site web",
            mail: "Mail",
            latitude: "Latitude",
            longitude: "Longitude",
            vendors: "Selectionner les vendeurs",
            state: "Region",
            county: "Departement"
        },
        vendor: {
            label: "Labels",
            location: "Points de vente",
            select: "Choisir un vendeur",
            profilePicture: "Image de profil",
            logo: "Logo",
            carouselPicture: "Image de carousel",
            featuredProduct: "Top vente",
            info: "Information",
            publish: "Publier",
            categories: "Catégories",
            keywords: "Valeurs",
            genders: "Genders",
            desc: {
                fr: "Description en français",
                en: "Description en anglais"
            },
            videoYoutube: "Id de vidéo youtube",
            videoDailymotion: "Id de vidéo dailymotion",
            videoVimeo: "Id de vidéo vimeo",
            site: "Site web",
            socialNetwork: {
                fb: "facebook",
                insta: "instagram",
                twitter: "twitter",
                pinterest: "pinterest",
                youtube: "youtube"
            },
            onlineShop: "Boutique en ligne",
            createToken: "Envoyer un lien au vendeur",
            createTokenToAdmin: "Envoyer un lien à l'administrateur",
        },
        profilePicture: "Définir l'image de profile d'un vendeur",
        logo: "Définir le logo d'un vendeur",
        carouselPicture: "Définir l'image de carousel d'un vendeur",
        carouselNumber: "Definir l'emplacement de l'image dans le carousel",
        featuredProductPicture: "Définir l'image du top vente d'un vendeur",
        featuredProductNumber: "Definir l'emplacement de l'image dans le top vente",
        featuredProductLink: "Definir le lien du top vente",
        featuredProductCol: "Definir la taille de la colone",
        state: {
            title: "État",
            notContacted: "Pas contacté",
            firstContact: "Premier contact",
            certificationValidationOngoing: "Certificats en cours de validation",
            certificationValidated: "Certificats validés",
            dataValidationOngoing: "Données en cours de validation",
            dataValidated: "Données validées",
            dataCreatingOngoing: "Données en cours de création",
            dataCreated: "Données créées",
            filter: "Filtrer par état",
           
        },
        toast: {
            commentDeleted: "Le commentaire a bien été supprimé",
            userReported: "L'utilisateur a bien été signalé",
            userReportError: "L'utilisateur n'a pu être signalé correctement",
            commentDeletionError: "Le commentaire n'a pu être supprimé correctement",
            userBlocked: "L'utilisateur a bien été bloqué",
            userBlockError: "L'utilisateur n'a pu être bloqué correctement",
            userUnblocked: "L'utilisateur a bien été débloqué",
            userUnblockError: "L'utilisateur n'a pu être débloqué correctement",
            suggestedBrandUpdated: "L'enseigne suggérée à été modifiée correctement",
            suggestedBrandUpdatedError: "L'enseigne suggérée n'a pu être modifiée correctement",
            suggestedBrandDeleted: "L'enseigne suggérée à été supprimé correctement",
            suggestedBrandDeletedError: "L'enseigne suggérée n'a pu être supprimé correctement",
            suggestedBrandContacted: "L'enseigne suggérée à été contacté",
            suggestedBrandContactedError: "L'enseigne suggérée n'a pu être contacté correctement",
            suggestedBrandCreated: "L'enseigne suggérée à été crée",
            suggestedBrandCreatedError: "L'enseigne suggérée n'a pu être crée correctement",
            potentialBrandUpdated: "L'enseigne potentielle à été modifiée correctement",
            potentialBrandUpdatedError: "L'enseigne potentielle n'a pu être modifiée correctement",
            potentialBrandDeleted: "L'enseigne potentielle à été supprimé correctement",
            potentialBrandDeletedError: "L'enseigne potentielle n'a pu être supprimé correctement",
            potentialBrandCreated: "L'enseigne potentielle à été créé correctement",
            potentialBrandCreatedError: "L'enseigne potentielle n'a pu être créé correctement",
            instagramSuggestedBrandUpdated: "L'enseigne suggérée sur instagram à été modifiée correctement",
            instagramSuggestedBrandUpdatedError: "L'enseigne suggérée sur instagram n'a pu être modifiée correctement",
            instagramSuggestedBrandDeleted: "L'enseigne suggérée sur instagram à été supprimé correctement",
            instagramSuggestedBrandDeletedError: "L'enseigne suggérée sur instagram n'a pu être supprimé correctement",
            imageUploaded: "Image correctement mise à jour",
            imageUploadedError: "Image n'a pu être correctement mis à jour",
            labelUpdated: "Le label correctement mis à jour",
            labelUpdatedError: "Le label n'a pu être correctement mis à jour",
            vendorPublished: "Vendeur correctement publié",
            suggestedBrandTransformed: "L'enseigne suggérée à été transformé correctement",
            suggestedBrandTransformedError: "L'enseigne suggérée n'a pu être transformé correctement",
            createLabel: "Le label à été correctement créé",
            locationUpdated: "Le point de vente à été mis à jour",
            locationUpdatedError: "Le point de vente n'a pas été mis à jour",
            locationCreated: "Le point de vente à été créé",
            locationCreatedError: "Le point de vente n'a pas été créé",
            vendorInfoUpdated: "Informations mise à jour",
            vendorTokenCreated: "Lien de création de compte envoyé",
        }
    },
    vendor: {
        notAllowedToPublish: {
            numberOfPointOfSale: "Nombre maximum de point de vente atteint pour cet abonnement",
            expired: "L'abonnement actuel a expiré",
            basic: "L'abonnement Basic limite le nombre de publication de votre page à une fois",
            incomplete: "Le paiement de votre abonnement actuel est attente de paiement",
            notSubscribe: "Vous ne possedez pas d'abonnement pour publier votre page"
        }
    },
    subscription: {
        title: "Nos abonnements",
        subscribe: "S'abonner",
        currentSubscription: "Votre abonnement actuel",
        selectedSubscription: "L'abonnement choisi",
        chose: "Selectionnez un nouvel abonnement :",
        alreadySubscribe: "Vous possedez déjà cet abonnement",
        issuePayment: "Le paiement de l'abonnement actuel à échoué, une action est requise",
        buttonReplace: "Remplacer l'abonnement actuel",
        canceled: "Votre abonnement actuel est résilié, il est valable jusqu'au {{expiresIn}} <0/> Vous pouvez en choisir un nouveau dès maintenant, qui remplacera l'actuel abonnement.<0/> L'abonnement actuel sera alors perdu.",
        fromMonthlyToLowerMonthly: "Votre abonnement mensuel actuel est valable jusqu'au {{expiresIn}} <0/> Vous pouvez en choisir une formule inférieure dès maintenant, qui remplacera l'actuel abonnement.<0/> L'abonnement actuel sera alors perdu.",
        fromYearlyToLower: "Votre abonnement annuel vous engage jusqu'au {{expiresIn}} <0/> Vous pouvez en choisir une formule inférieure dès maintenant, qui remplacera l'actuel abonnement.<0/> L'abonnement actuel sera alors perdu.",
        fromYearlyToMonthly: "Votre abonnement annuel vous engage jusqu'au {{expiresIn}} <0/> Vous pouvez en choisir une formule mensuelle dès maintenant, qui remplacera l'actuel abonnement.<0/> L'abonnement actuel sera alors perdu.",
        checkoutForm: {
            requireAction: "La validation de cet abonnement requiers une action, veuillez patientez quelque instant",
            processPayment: "Données en cours de traitement",
            cardDetails: "<0>Entrez les détails de votre carte ici.<0/>Votre abonnement va commencencer après vérification des données par notre équipe.</0>",
            totalDue: "Montant: ",
            frequency: "Fréquence",
            fullName: "Nom de l'entreprise: ",
            card: "Carte de paiement: ",
            country: {
                label: "Domiciliation société",
                france: "France",
                UE: "Union Européenne",
                horsUE: "Hors Union Européenne"
            },
            tvaIntra: "Numéro intracommunautaire",
            chooseCountry: "Selectionner une domiciliation pour obtenir ce calcul"
        },
        paymentComplete: "Paiement complété",
        member: "Membre",
        alreadySubscribeToOther: "Vous profitez déjà d'un abonnement, si vous souhaitez le modifier, vous pouvez résilier votre abonnement actuel à tout moment, et souscrire à une autre formule à date d'échéance",
        basicSubscription: "Il suffit d'être connecté pour profiter de cet abonnement ! <0/><1>Créez votre page dès à présent</1>",
        proration: "Prix facturé immédiatement avec déduction de temps inutilisé de l'abonnement actuel : {{prorationFinalPrice}} TTC <0/> Soit {{prorationPrice}} HT de réduction",
        trialPeriod: "Bravo, vous êtes élligible à 90 jours d'essai gratuit",
        toast: {
            updateSuccess: "Votre abonnement à été modifié avec succès",
            updateError: "Une erreur est survenue lors de l'enregistrement de votre nouvel abonnement, contactez-nous"
        }
    },
    vendorDashboard: {
        searchPlaceholder: "Chercher une ville, un département",
        cities: "Ville(s)",
        map: "Boutiques & revendeurs",
        toast: {
            youtubeMalformed: "Le lien youtube est incorrect, le format attendu est https://www.youtube.com/watch?v=...",
            dailymotionMalformed: "Le lien dailymotion est inccorect, le format attendu est https://www.dailymotion.com/video/...",
            vimeoMalformed: "Le lien vimeo est inccorect, le format attendu est https://vimeo.com/...",
            youtubeChannelMalformed: "Le lien de chaine youtube est incorrect, le format attendu est https://www.youtube.com/channel/...",
            pinterestMalformed: "Le lien de compte pinterest est incorrect, le format attendu est https://www.pinterest.fr/...",
            instagramMalformed: "Le lien de compte instagram est incorrect, le format attendu est https://www.instagram.com/...",
            twitterMalformed: "Le lien de compte twitter est incorrect, le format attendu est https://twitter.com/...",
            facebookMalformed: "Le lien de compte facebook est incorrect, le format attendu est https://www.facebook.com/...",
            websiteMalformed: "Le lien du site web est incorrect, le format attendu est https://...",
            linkMalformed: "Le lien est incorrect, le format attendu est http://... ou https://...",
            imageUploaded: "Image correctement mise à jour",
            imageUploadedError: "L'image n'a pu être correctement mis à jour",
            infoUpdated: "Information(s) mise(s) à jour",
            infoUpdatedError: "Les informations n'ont pu être correctement mises à jour",
            locationAdded: "Point de vente ajouté",
            locationAddedError: "Le point de vente n'a pu être ajouté correctement",
            locationUpdated: "Point de vente mis à jour",
            locationUpdatedError: "Le point de vente n'a pu être correctement mis à jour",
            locationRemoved: "Point de vente retiré",
            locationRemovedError: "Le point de vente n'a pu être correctement retiré",
            published: "Page publiée",
            publishedError: "Erreur lors de la publication de la page"
        },
        firstPublish: {
            title: 'Publier les modifications',
            info: `La première publication est comprise dans l'abonnement Basic.
            A l'issue de cette publication, votre page d'enseigne sera en ligne et accessible à tous les utilisateurs.
            Les prochaines modifications nécessiteront un abonnement Super ou Super+`,
            do: 'Publier maintenant une fois',
            passToUnlimited: 'Publier en illimité'
        },
        alreadyPublish: {
            title: 'Publier les modifications',
            info: `La première publication est comprise dans l'abonnement Basic.
            A l'issue de cette publication, votre page d'enseigne sera en ligne et accessible à tous les utilisateurs.
            Les prochaines modifications nécessiteront un abonnement Super ou Super+`,
            do: 'Publier maintenant une fois',
            passToUnlimited: 'Publier en illimité'
        },
        allowedToPublish: {
            info: `Les modifications sont enregistrées dans un brouillon. 
            Publier ces modifications nécessite un <0>abonnement Basic, Super ou Super+</0>`,
            infoIncomplete: `Votre abonnement est en attente de paiement. Les modifications sont enregistrées dans un brouillon. 
            Pour débloquer la publication vous pouvez valider la procédure en attente <0>ici</0>`,
        },
        goEdit: {
            infoNotPublished: `Votre page d'enseigne n'est pas encore en ligne, passez en mode édition pour la compléter et la publier`,
            infoPublished: `Votre page d'enseigne est <0>en ligne !</0> Tous les utilisateurs peuvent y accéder par l'onglet des adresses partagées ou par le champ de recherche. Ici vous découvrez l'apperçu de votre page. Cliquez sur le mode édition pour effectuez des mises à jours.`,
        },
        isValidSubscription: {
            infoBasic: `Vous possedez un abonnement <0>{{status}}</0>, bravo ! Vous pouvez modifier et publier votre page d'enseigne une fois, administrer jusqu'à 6 points de ventes et gérer vos bons plans<`,
            infoSuper: `Vous possedez un abonnement <0>{{status}}</0>, bravo ! Vous pouvez modifier et publier votre page d'enseigne en illimité, administrer jusqu'à 20 points de ventes et gérer vos bons plans`,
            infoSuperPlus: `Vous possedez un abonnement <0>{{status}}</0>, bravo ! Vous pouvez modifier et publier votre page d'enseigne, administrer vos points de ventes et gérer vos bons plans, le tout en illimité !`
        },
        subscription: {
            title: 'Publier les modifications',
            info: `Un compte sans abonnement ne peut pas publier sa page pour la rendre accessible.
            Pour modifier sa page et la publier, un abonnement Basic, Super ou Super+ est necessaire`,
            do: 'Devenir Super',
            doPlus: 'Devenir Super +'
        },
        publish: "Publier",
        empty: "Section vide",
        desc: {
            title: "Decrivez ici le storytelling de votre enseigne",
            edit: "Modifier votre storytelling",
            fr: "Description en français",
            en: "Description en anglais"
        },
        networks: {
            title: "Copiez les liens de vos différents réseaux sociaux: instagram, facebook, twitter, pinterest, youtube, et enfin votre site web",
            edit: "Modifier vos reseaux sociaux"
        },
        video: {
            title: "Copiez le lien d'une video provenant de youtube, dailymotion ou vimeo",
            edit: "Modifier votre video",
            select: "Choisir le type de video"
        },
        carousel: {
            title: "Envoyer les images qui se dérouleront dans votre carousel",
            edit: "Modifier votre carousel",
            info: "Le format d'image conseillé est 450x300, pour obtenir une résolution optimale.",
            select: "Choisir la photo à remplacer",
            first: "Première photo",
            second: "Deuxième photo",
            third: "Troisième photo"
        },
        featuredProduct: {
            title: "Créer votre top vente",
            edit: "Envoyer ce top vente",
            infoSquare: "Le format d'image conseillé est 450x300, pour obtenir une résolution optimale.",
            infoRectangle: "Le format d'image conseillé est 450x300, pour obtenir une résolution optimale.",
        },
        location: {
            allow: {
                title: "Gérer les points de vente",
                infoNotSubscribe: "Pour ajouter, modifier ou supprimer un point de vente, un abonnement Basic, Super ou Super+ est requis",
                infoSuper: "Vous avez atteint le nombre limite de points de vente pour votre abonnement. Pour ajouter des points de vente en illimité, un abonnement Super+ est requis.",
                infoBasic: "Vous avez atteint le nombre limite de points de vente pour votre abonnement. Pour ajouter plus de points de vente, un abonnement Super ou Super+ est requis.",
                infoBasicPublished: "Votre nombre de publication à été atteint. Pour publier en illimité et mettre à jour vos points de vente,  un abonnement Super ou Super+ est requis."
            },
            remove: {
                title: "Supprimer ce point de vente",
                info: "Êtes-vous sûr ? Ce point de vente sera retiré de votre page"
            },
            add: {
                title: "Ajouter ce point de vente",
                search: "Chercher une adresse, par exemple en tapant '52 rue des jonquilles BELLEVILLE'. Seules les addresses en France sont autorisées.",
                results: "Sélectionnez un résultat :",
                alreadyAdded: "Point de vente déjà ajouté",
                noResult: "Aucun résultat n'a été trouvé pour cette recherche, essayez avec une autre recherche",
                info: "Remplissez les informations de ce point de vente"
            },
            edit: {
                title: "Modifier ce point de vente",
                info: "Mettre à jour les informations de ce point de vente"
            },
            shopName: "Nom de la boutique ou du revendeur",
            address: "Addresse",
            postalCode: "Code postal",
            city: "Ville",
            county: "Département",
            state: "Région",
            openingHours: "Horaires",
            phoneNumber: "Téléphone",
            webSite: "Site web",
            mail: "Adresse mail"
        },
        editMode: "Mode edition",
        cancel: "Annuler",
        update: "Modifier",
        remove: "Supprimer",
        search: "Chercher",
        select: "Choisir",
        add: "Ajouter"
    },
    vendorAccount: {
        toast: {
            canceled: "L'abonnement a bien été suspendu",
            canceledError: "Une erreur est survenue lors de la suspension de l'abonnement",
            deliverInfoUpdated: "Les modes de livraison ont bien été sauvegardés",
            deliverInfoUpdatedError: "Une erreur est survenue lors de la sauvegarde des modes de livraison"
        },
        deliver: {
            title: "Indiquez vos modes de livraison",
            onlineShop: "Livraison à domicile",
            clickAndCollect: "Retrait en magasin",
            button: "Sauvegarder"
        },
        inactivate: "<0>Page désactivée</0><1/><1 />Votre page à été désactivée par l'équipe Super Responsable. Pour en savoir plus <2>contactez-nous</2>",
        published: "<0>Page en ligne</0><1/><1 />Votre page est accessible à tous les utilisateurs. Pour la modifier, <2>direction le dashboard</2>",
        notPublished: "<0>Page hors ligne</0><1/><1 />Votre page n'est pas publiée, elle est inaccessible à tous les utilisateurs. Pour la publier, <2>direction le dashboard</2>",
        logo: {
            title: "Mettre à jour son logo",
            info: "Le format d'image conseillé est 200x200, pour obtenir une résolution optimale.",
        },
        subscription: {
            dialog: {
                terminate: {
                    title: "L'abonnement est activé",
                    info: `Cet abonnement vous permet la mise en ligne de votre page d'enseigne. <0/>
                    Une fois annulé, les services liés à cette abonnement ainsi que votre page d'enseigne ne seront plus disponible à compter de la date d'expiration, à savoir dans {{expirationDate}}`
                }
            },
            cancel: "Annuler l'abonnement",
            keep: "Conserver l'abonnement",
            change: "Changer d'abonnement",
            terminate: "Résilier l'abonnement",
            takeBack: "Reprendre un abonnement",
            activate: "Activer l'abonnement",
            do: "S'abonner immédiatement",
            price: "Prix",
            startAt: "Depuis le",
            nextPayment: "Prochain paiement dans ",
            creditCard: "Carte utilisé",
            expiresIn: "Expire dans",
            expiredSince: "Expiré depuis",
            state: {
                title: "État",
                value: {
                    trialing: "Période d'essai",
                    active: 'Actif',
                    incomplete: 'En attente de paiement',
                    incomplete_expired: 'En attente de paiement',
                    past_due: 'En attente de paiement',
                    canceled: 'Annulé',
                    unpaid: 'Non payé'
                }
            },
            noSubscription: "Pas d'abonnement en cours",
            year: "an",
            month: "mois"
        },
        invoice: {
            title: "Factures",
            paid: "payé",
            retrieve: "Obtenir la facture",
            produced: "Émise le"
        },
        profilePicture: {
            title: "Mettre à jour sa photo de profil",
            info: "Le format d'image conseillé est 460x200, pour obtenir une résolution optimale.",
        }
    },
    partners: {
        supportSR: "Ils soutiennent Super Responsable",
        supportHashtagSR: "Ils contribuent à la campagne"
    },
    aboutUs: {
        title: "Super Responsable, parce qu’il faut être un super héros aujourd’hui pour vivre dans le respect de l’homme, de l’animal et de l’environnement",
        introduction: "Plus de 60% des français (42 millions source: greenflex 2019) se disent concernés par la consommation saine, éthique et durable mais éprouvent de vraies difficultés à passer à l’action et tenir dans la durée. Pour les accompagner dans leur transition, Alexandra & Loïse ont conçu le site communautaire Super Responsable. Développé à l’<0>Incubateur Telecom Paristech Eurecom</0> dans la 1ère Technopôle d’Europe, la start-up a créé un modèle gagnant-gagnant pour les enseignes et consommateurs responsables de France.",
        questionOne: "Récompenser les héros du quotidien, c'est un peu votre mot d'ordre. <0/>Qu'est-ce que ça signifie ?",
        answerOne: "On pense que toute personne qui participe de près ou de loin à faire grandir ce marché plutôt qu'un autre mérite d'être reconnue et récompensée. C'est pourquoi on permet aux <0>enseignes référencées</0> d'être boostées en communication et en marketing et aux utilisateurs d'obtenir et de choisir leurs <1>bons plans</1> pour les actions qu'ils ont effectuées sur le site. Codes promo, événements, goodies… On a vu large !",
        questionTwo: "En tant qu’utilisateur, comment puis-je être récompensé ?",
        answerTwo: "En <0>partageant tes bonnes adresses</0> sur le site comme tu le ferais pour un ami, tu aides quelqu'un à trouver ce qu'il cherche et apportes ton soutien à une enseigne qui vit de plein fouet la crise sanitaire, ce n'est pas rien. Sur 5 000 membres, 2 400 enseignes ont été proposées et sont en cours de vérification par notre équipe. En outre, répondre à nos sondages aide à comprendre et anticiper les tendances, comme pour notre 1er rapport de sondage <1>COVID-19: les tendances de la consommation responsable en France.</1>",
        legend: "Dans notre super bureau où tout prend vie",
        questionThree: "Pensez-vous répondre aux exigences de chacun ?",
        answerThree: "Les aides sont éparpillées. Selon que vous préférez l'achat en ligne au terrain, bio ou vintage etc. L'utilisateur doit mémoriser une longue liste de nouvelles enseignes et de produits, ce qui n'est pas simple. C'est pourquoi on propose une solution tout en un : trouver les enseignes et leurs produits déjà responsables, et de toutes formes : producteurs, boutiques, revendeurs, e-shops et marketplaces dans l’alimentation, mode, beauté et maison pour homme, femme, enfant et animal.",
        questionFour: "Personnaliser le parcours utilisateur, ça veut dire quoi exactement ?",
        answerFour: "Les utilisateurs se géolocalisent, donnent leurs avis, enregistrent leurs valeurs et enseignes favorites permettant des suggestions sur-mesure à la pointe du digital. Ces suggestions sont possibles par l'exploitation de données de masse et anonymisées. Ça te permet par exemple de découvrir les <0>favoris et tendances</0> d'autres gens qui partagent tes valeurs.",
        questionFive: "Comment voyez-vous l'avenir super responsable ?",
        answerFive: "On compte sur le soutien des membres qui sont les premiers contributeurs de notre site. Nous développons un algorithme de scoring afin de valoriser les enseignes les plus méritantes. On souhaite aussi mettre en avant les produits tout autant que leurs enseignes, et donner plus de poids à la géolocalisation.",
        seeYou: "A très vite dans nos aventures super responsables !"
    },
    campaignAchieved: {
        title: "Bravo, la campagne de lancement est achevée !",
        subTitle: "C'est à toi de jouer maintenant :",
        signin: "Inscris-toi et crée ton profil",
        link: "Ajoutes tes adresses coups de coeur et crée ton propre carnet",
        discover: "Découvre les commerces autour de toi, en accord avec tes valeurs",
        follow: "Suis nos super aventures responsables !",
    },
    priceList: {
        unlimited: "illimité",
        limited: "limité",
        page: {
            name: "PAGE D'ENSEIGNE",
            pilot: {
                name: "Pilotez votre page",
                basic: "1 mise en ligne"
            },
            pointOfSale: "Ajoutez vos points de vente",
            featuredProducts: "Révélez vos top ventes",
            carousel: "Intégrez vos meilleures photos",
            storyTelling: "Ecrivez votre storytelling FR & EN",
            links: "Intégrez vos présences digitales",
            weTakeCare: "On s'occupe de tout "
        },
        com: {
            name: "COMMUNICATION",
            posts: "Publications réseaux sociaux",
            newsletter: "Intégrez la newsletter",
            articlesAndVideos: "Articles & vidéos",
            articleOrVideoDedicated: {
                name: "Article/vidéo dédiée",
                super: "1 article"
            },
            redactionOpinion: "Avis de la rédaction"
        },
        rewards: {
            name: "RÉCOMPENSES",
            manage: "Ajoutez/gérez vos bons plans",
            stats: "Statistiques de popularité (bientôt)",
            monthly: "Envoi trimestriel de vos statistiques (bientôt)",
            weTakeCare: "Gestion de vos bons plans"
        },
        marketing: {
            name: "MARKETING",
            studies: "Etudes de marché trimestrielles",
            stats: "Statistiques de votre page (bientôt)",            
            weTakeCare: "Etude de marché annuelle adaptée"
        },
        marketplace: {
            name: "MARKETPLACE (bientôt)",
            integrate: "Intégrez notre marketplace",
            stats: "Statistiques de popularité",
            create: "Créez vos fiches produits",
            statsWeTakeCare: "Envoi trimestriel de vos statistiques",
            weTakeCare: "Intégration et gestion"
        },
        monthly: "MENSUEL",
        byMonth: "mois",
        yearly: "ANNUEL  1 mois offert",
        byYear: "an",
        weTakeCare: "ANNUEL Service personnel",
        withWeTakeCare: "Service personnel inclus",
        subscription: "Abonnement",
        oneMonthOffer: "Un mois offert",
        noObligation: "Sans engagement",
        basic: {
            name: "BASIC",
            monthly: "1€ HT",
            yearly: "11€ HT",
            weTakeCare: "77€ HT"
        },
        super: {
            name: "SUPER",
            monthly: "14€ HT",
            yearly: "154€ HT",
            weTakeCare: "264€ HT"
        },
        superPlus: {
            name: "SUPER +",
            monthly: "44€ HT",
            yearly: "484€ HT",
            weTakeCare: "704€ HT"
        },
        subscribe: "S'abonner"
    },
    link: {
        facebookGroupLink: "Tu peux aussi suivre les actualités sur le groupe facebook",
        vendorCandidateLink: "Je suis une enseigne",
        loginLink: "Se connecter",
        loginVendorLink: "Se connecter à mon commerce",
        loginUserLink: "Compte utilisateur"
    },
    form: {
        enterpriseName: {
            label: "Nom de l'entreprise",
            helper: "Le nom de l'entreprise est requis"
        },
        contactName: {
            label: "Nom du contact"
        },
        phone: {
            label: "Téléphone"
        },
        site: {
            label: "Site web"
        },
        name: {
            label: 'Prénom',
            helper: 'Le prénom est requis'
        },
        brand: {
            label: 'Marque',
            helper: 'Le nom de la marque est requis'
        },
        surname: {
            label: 'Nom',
            helper: 'Le nom est requis'
        },
        birthday: {
            label: 'Date de naissance'
        },
        mail: {
            label: 'Email',
            helper: "L'adresse mail est requise et doit être valide"
        },
        password: {
            label: 'Mot de passe',
            helper: 'Le mot de passe est requis et dois contenir 8 charactères dont: 1 nombre, 1 majuscule, 1 minuscule'
        },
        newsletter: {
            label: "S'inscrire à la newsletter"
        }
    },
    404: {
        title: "Erreur 404",
        subTitle: "Cette page n'existe pas ou le lien est corrompu"
    },
    error: {
        somethingWrongAppends: "Une erreur est survenue",
        shouldBeConnected: "La connexion est nécessaire pour cette action",
        notAllowed: "Vous n'êtes pas authorisé à acceder à cette page.",
        connectionInvalid: "La connexion n'est plus valide, merci de vous identifier à nouveau"
    },
    genders: "Cibles",
    rgpd: `   
    <0>Définitions</0>
    <1><0>Client :</0> tout professionnel ou personne physique capable au sens des articles 1123 et suivants du Code civil, ou personne morale, qui visite le Site objet des présentes conditions générales.<3/>
    <0>Prestations et Services :</0> <1 href="https://www.super-responsable.org">https://www.super-responsable.org</1> met à disposition des Clients :</1>

    <1><0>Contenu :</0> Ensemble des éléments constituants l’information présente sur le Site, notamment textes – images – vidéos.</1>

    <1><0>Informations clients :</0> Ci après dénommé « Information (s) » qui correspondent à l’ensemble des données personnelles susceptibles d’être détenues par <1>https://www.super-responsable.org</1> pour la gestion de votre compte, de la gestion de la relation client et à des fins d’analyses et de statistiques.</1>


    <1><0>Utilisateur :</0> Internaute se connectant, utilisant le site susnommé.</1>
    <1><0>Informations personnelles :</0> « Les informations qui permettent, sous quelque forme que ce soit, directement ou non, l'identification des personnes physiques auxquelles elles s'appliquent » (article 4 de la loi n° 78-17 du 6 janvier 1978).</1>
    <1>Les termes « données à caractère personnel », « personne concernée », « sous traitant » et « données sensibles » ont le sens défini par le Règlement Général sur la Protection des Données (RGPD : n° 2016-679)</1>

    <0>1. Présentation du site internet.</0>
    <1>En vertu de l'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique, il est précisé aux utilisateurs du site internet <1>https://www.super-responsable.org</1> l'identité des différents intervenants dans le cadre de sa réalisation et de son suivi:
    </1><1><2>Propriétaire</2> :  SAS Super Responsable Capital social de 2000€ Numéro de TVA: FR45887680379 – 90 av du Maréchal Juin 06400 Cannes<3/>
                  
    <2>Responsable publication</2> : Alexandra Tence – bonjour@super-responsable.org<3/>
    Le responsable publication est une personne physique ou une personne morale.<3/>
    <2>Webmaster</2> : Loïse Fenoll – support@super-responsable.org<3/>
    <2>Hébergeur</2> : Heroku – 415 Mission Street Suite 300 94105 San Francisco, CA (866) 278-1349<3/>
    <2>Délégué à la protection des données</2> : Loïse Fenoll – support@super-responsable.org<3/>
    </1>

    <2><0>Ce modèle de mentions légales est proposé par le <1>générateur gratuit offert par Orson.io</1></0></2>



    <0>2. Conditions générales d’utilisation du site et des services proposés.</0>

    <1>Le Site constitue une œuvre de l’esprit protégée par les dispositions du Code de la Propriété Intellectuelle et des Réglementations Internationales applicables. 
    Le Client ne peut en aucune manière réutiliser, céder ou exploiter pour son propre compte tout ou partie des éléments ou travaux du Site.</1>

    <1>L’utilisation du site <1>https://www.super-responsable.org</1> implique l’acceptation pleine et entière des conditions générales d’utilisation ci-après décrites. Ces conditions d’utilisation sont susceptibles d’être modifiées ou complétées à tout moment, les utilisateurs du site <1>https://www.super-responsable.org</1> sont donc invités à les consulter de manière régulière.</1>

    <1>Ce site internet est normalement accessible à tout moment aux utilisateurs. Une interruption pour raison de maintenance technique peut être toutefois décidée par <1>https://www.super-responsable.org</1>, qui s’efforcera alors de communiquer préalablement aux utilisateurs les dates et heures de l’intervention.
    Le site web <1>https://www.super-responsable.org</1> est mis à jour régulièrement par <1>https://www.super-responsable.org</1> responsable. De la même façon, les mentions légales peuvent être modifiées à tout moment : elles s’imposent néanmoins à l’utilisateur qui est invité à s’y référer le plus souvent possible afin d’en prendre connaissance.</1>

    <0>3. Description des services fournis.</0>

    <1>Le site internet <1>https://www.super-responsable.org</1> a pour objet de fournir une information concernant l’ensemble des activités de la société.
    <1>https://www.super-responsable.org</1> s’efforce de fournir sur le site <1>https://www.super-responsable.org</1> des informations aussi précises que possible. Toutefois, il ne pourra être tenu responsable des oublis, des inexactitudes et des carences dans la mise à jour, qu’elles soient de son fait ou du fait des tiers partenaires qui lui fournissent ces informations.</1>

    <1>Toutes les informations indiquées sur le site <1>https://www.super-responsable.org</1> sont données à titre indicatif, et sont susceptibles d’évoluer. Par ailleurs, les renseignements figurant sur le site <1>https://www.super-responsable.org</1> ne sont pas exhaustifs. Ils sont donnés sous réserve de modifications ayant été apportées depuis leur mise en ligne.</1>

    <0>4. Limitations contractuelles sur les données techniques.</0>

    <1>Le site utilise la technologie JavaScript.

    Le site Internet ne pourra être tenu responsable de dommages matériels liés à l’utilisation du site. De plus, l’utilisateur du site s’engage à accéder au site en utilisant un matériel récent, ne contenant pas de virus et avec un navigateur de dernière génération mis-à-jour
    Le site <1>https://www.super-responsable.org</1> est hébergé chez un prestataire sur le territoire de l’Union Européenne conformément aux dispositions du Règlement Général sur la Protection des Données (RGPD : n° 2016-679)</1>

    <1>L’objectif est d’apporter une prestation qui assure le meilleur taux d’accessibilité. L’hébergeur assure la continuité de son service 24 Heures sur 24, tous les jours de l’année. Il se réserve néanmoins la possibilité d’interrompre le service d’hébergement pour les durées les plus courtes possibles notamment à des fins de maintenance, d’amélioration de ses infrastructures, de défaillance de ses infrastructures ou si les Prestations et Services génèrent un trafic réputé anormal.</1>

    <1><1>https://www.super-responsable.org</1> et l’hébergeur ne pourront être tenus responsables en cas de dysfonctionnement du réseau Internet, des lignes téléphoniques ou du matériel informatique et de téléphonie lié notamment à l’encombrement du réseau empêchant l’accès au serveur.</1>

    <0>5. Propriété intellectuelle et contrefaçons.</0>

    <1><1>https://www.super-responsable.org</1> est propriétaire des droits de propriété intellectuelle et détient les droits d’usage sur tous les éléments accessibles sur le site internet, notamment les textes, images, graphismes, logos, vidéos, icônes et sons.
    Toute reproduction, représentation, modification, publication, adaptation de tout ou partie des éléments du site, quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable de : <1>https://www.super-responsable.org</1>.</1>

    <1>Toute exploitation non autorisée du site ou de l’un quelconque des éléments qu’il contient sera considérée comme constitutive d’une contrefaçon et poursuivie conformément aux dispositions des articles L.335-2 et suivants du Code de Propriété Intellectuelle.</1>

    <0>6. Limitations de responsabilité.</0>

    <1><1>https://www.super-responsable.org</1> agit en tant qu’éditeur du site. <1>https://www.super-responsable.org</1>  est responsable de la qualité et de la véracité du Contenu qu’il publie. </1>

    <1><1>https://www.super-responsable.org</1> ne pourra être tenu responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site internet <1>https://www.super-responsable.org</1>, et résultant soit de l’utilisation d’un matériel ne répondant pas aux spécifications indiquées au point 4, soit de l’apparition d’un bug ou d’une incompatibilité.</1>

    <1><1>https://www.super-responsable.org</1> ne pourra également être tenu responsable des dommages indirects (tels par exemple qu’une perte de marché ou perte d’une chance) consécutifs à l’utilisation du site <1>https://www.super-responsable.org</1>.
    Des espaces interactifs (possibilité de poser des questions dans l’espace contact) sont à la disposition des utilisateurs. <1>https://www.super-responsable.org</1> se réserve le droit de supprimer, sans mise en demeure préalable, tout contenu déposé dans cet espace qui contreviendrait à la législation applicable en France, en particulier aux dispositions relatives à la protection des données. Le cas échéant, <1>https://www.super-responsable.org</1> se réserve également la possibilité de mettre en cause la responsabilité civile et/ou pénale de l’utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamant, ou pornographique, quel que soit le support utilisé (texte, photographie …).</1>

    <0>7. Gestion des données personnelles.</0>

    <1>Le Client est informé des réglementations concernant la communication marketing, la loi du 21 Juin 2014 pour la confiance dans l’Economie Numérique, la Loi Informatique et Liberté du 06 Août 2004 ainsi que du Règlement Général sur la Protection des Données (RGPD : n° 2016-679). </1>

    <3>7.1 Responsables de la collecte des données personnelles</3>

    <1>Pour les Données Personnelles collectées dans le cadre de la création du compte personnel de l’Utilisateur et de sa navigation sur le Site, le responsable du traitement des Données Personnelles est : Super Responsable. <1>https://www.super-responsable.org</1>est représenté par Alexandra Tence, son représentant légal</1>

    <1>En tant que responsable du traitement des données qu’il collecte, <1>https://www.super-responsable.org</1> s’engage à respecter le cadre des dispositions légales en vigueur. Il lui appartient notamment au Client d’établir les finalités de ses traitements de données, de fournir à ses prospects et clients, à partir de la collecte de leurs consentements, une information complète sur le traitement de leurs données personnelles et de maintenir un registre des traitements conforme à la réalité.
    Chaque fois que <1>https://www.super-responsable.org</1> traite des Données Personnelles, <1>https://www.super-responsable.org</1> prend toutes les mesures raisonnables pour s’assurer de l’exactitude et de la pertinence des Données Personnelles au regard des finalités pour lesquelles <1>https://www.super-responsable.org</1> les traite.</1>
     
    <3>7.2 Finalité des données collectées</3>
     
    <1><1>https://www.super-responsable.org</1> est susceptible de traiter tout ou partie des données : </1>

    <4>
      
    <0>pour permettre la navigation sur le Site et la gestion et la traçabilité des prestations et services commandés par l’utilisateur : données de connexion et d’utilisation du Site, facturation, historique des commandes, etc. </0>
     
    <0>pour prévenir et lutter contre la fraude informatique (spamming, hacking…) : matériel informatique utilisé pour la navigation, l’adresse IP, le mot de passe (hashé) </0>
     
    <0>pour améliorer la navigation sur le Site : données de connexion et d’utilisation </0>
     
    <0>pour mener des enquêtes de satisfaction facultatives sur <0>https://www.super-responsable.org</0> : adresse email </0>
    <0>pour mener des campagnes de communication (sms, mail) : numéro de téléphone, adresse email</0>


    </4>

    <1><1>https://www.super-responsable.org</1> ne commercialise pas vos données personnelles qui sont donc uniquement utilisées par nécessité ou à des fins statistiques et d’analyses.</1>
     
    <3>7.3 Droit d’accès, de rectification et d’opposition</3>
     
    <1>
    Conformément à la réglementation européenne en vigueur, les Utilisateurs de <1>https://www.super-responsable.org</1> disposent des droits suivants : </1>
     

    <4>

    <0>droit d'accès (article 15 RGPD) et de rectification (article 16 RGPD), de mise à jour, de complétude des données des Utilisateurs droit de verrouillage ou d’effacement des données des Utilisateurs à caractère personnel (article 17 du RGPD), lorsqu’elles sont inexactes, incomplètes, équivoques, périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite </0>
     
    <0>droit de retirer à tout moment un consentement (article 13-2c RGPD) </0>
     
    <0>droit à la limitation du traitement des données des Utilisateurs (article 18 RGPD) </0>
     
    <0>droit d’opposition au traitement des données des Utilisateurs (article 21 RGPD) </0>
     
    <0>droit à la portabilité des données que les Utilisateurs auront fournies, lorsque ces données font l’objet de traitements automatisés fondés sur leur consentement ou sur un contrat (article 20 RGPD) </0>
     
    <0>droit de définir le sort des données des Utilisateurs après leur mort et de choisir à qui <0>https://www.super-responsable.org</0> devra communiquer (ou non) ses données à un tiers qu’ils aura préalablement désigné</0>
     </4>

    <1>Dès que <1>https://www.super-responsable.org</1> a connaissance du décès d’un Utilisateur et à défaut d’instructions de sa part, <1>https://www.super-responsable.org</1> s’engage à détruire ses données, sauf si leur conservation s’avère nécessaire à des fins probatoires ou pour répondre à une obligation légale.</1>
     
    <1>Si l’Utilisateur souhaite savoir comment <1>https://www.super-responsable.org</1> utilise ses Données Personnelles, demander à les rectifier ou s’oppose à leur traitement, l’Utilisateur peut contacter <1>https://www.super-responsable.org</1> par écrit à l’adresse suivante : </1>
     
    Super Responsable – DPO, Loïse Fenoll <5/>
    90 av du Maréchal Juin 06400 Cannes.
     
    <1>Dans ce cas, l’Utilisateur doit indiquer les Données Personnelles qu’il souhaiterait que <1>https://www.super-responsable.org</1> corrige, mette à jour ou supprime, en s’identifiant précisément avec une copie d’une pièce d’identité (carte d’identité ou passeport). </1>

    <1>
    Les demandes de suppression de Données Personnelles seront soumises aux obligations qui sont imposées à <1>https://www.super-responsable.org</1> par la loi, notamment en matière de conservation ou d’archivage des documents. Enfin, les Utilisateurs de <1>https://www.super-responsable.org</1> peuvent déposer une réclamation auprès des autorités de contrôle, et notamment de la CNIL (https://www.cnil.fr/fr/plaintes).</1>
     
    <3>7.4 Non-communication des données personnelles</3>

    <1><1>https://www.super-responsable.org</1> s’interdit de traiter, héberger ou transférer les Informations collectées sur ses Clients vers un pays situé en dehors de l’Union européenne ou reconnu comme « non adéquat » par la Commission européenne sans en informer préalablement le client. Pour autant, <1>https://www.super-responsable.org</1> reste libre du choix de ses sous-traitants techniques et commerciaux à la condition qu’il présentent les garanties suffisantes au regard des exigences du Règlement Général sur la Protection des Données (RGPD : n° 2016-679).</1>

    <1><1>https://www.super-responsable.org</1> s’engage à prendre toutes les précautions nécessaires afin de préserver la sécurité des Informations et notamment qu’elles ne soient pas communiquées à des personnes non autorisées. Cependant, si un incident impactant l’intégrité ou la confidentialité des Informations du Client est portée à la connaissance de <1>https://www.super-responsable.org</1>, celle-ci devra dans les meilleurs délais informer le Client et lui communiquer les mesures de corrections prises. Par ailleurs <1>https://www.super-responsable.org</1> ne collecte aucune « données sensibles ».</1>

    <1>
    Les Données Personnelles de l’Utilisateur peuvent être traitées par des filiales de <1>https://www.super-responsable.org</1> et des sous-traitants (prestataires de services), exclusivement afin de réaliser les finalités de la présente politique.</1>
    <1>
    Dans la limite de leurs attributions respectives et pour les finalités rappelées ci-dessus, les principales personnes susceptibles d’avoir accès aux données des Utilisateurs de <1>https://www.super-responsable.org</1> sont principalement les agents de notre service client.</1>
    
    <2><2>7.5 Types de données collectées</2><0>Concernant les utilisateurs d’un Site <1>https://www.super-responsable.org</1>, nous collectons les données suivantes qui sont indispensables au fonctionnement du service , et qui seront conservées pendant une période maximale de 9 mois après la fin de la relation contractuelle: nom, prénom, email</0><0><0>https://www.super-responsable.org</0> collecte en outre des informations qui permettent d’améliorer l’expérience utilisateur et de proposer des conseils contextualisés :<3/>Données de mesure d'audience</0><0> Ces  données sont conservées sans limite de temps</0></2>


    <0>8. Notification d’incident</0>
    <1>
    Quels que soient les efforts fournis, aucune méthode de transmission sur Internet et aucune méthode de stockage électronique n'est complètement sûre. Nous ne pouvons en conséquence pas garantir une sécurité absolue. 
    Si nous prenions connaissance d'une brèche de la sécurité, nous avertirions les utilisateurs concernés afin qu'ils puissent prendre les mesures appropriées. Nos procédures de notification d’incident tiennent compte de nos obligations légales, qu'elles se situent au niveau national ou européen. Nous nous engageons à informer pleinement nos clients de toutes les questions relevant de la sécurité de leur compte et à leur fournir toutes les informations nécessaires pour les aider à respecter leurs propres obligations réglementaires en matière de reporting.</1>
    <1>
    Aucune information personnelle de l'utilisateur du site <1>https://www.super-responsable.org</1> n'est publiée à l'insu de l'utilisateur, échangée, transférée, cédée ou vendue sur un support quelconque à des tiers. Seule l'hypothèse du rachat de <1>https://www.super-responsable.org</1> et de ses droits permettrait la transmission des dites informations à l'éventuel acquéreur qui serait à son tour tenu de la même obligation de conservation et de modification des données vis à vis de l'utilisateur du site <1>https://www.super-responsable.org</1>.</1>

    <3>Sécurité</3>

    <1>
    Pour assurer la sécurité et la confidentialité des Données Personnelles et des Données Personnelles de Santé, <1>https://www.super-responsable.org</1> utilise des réseaux protégés par des dispositifs standards tels que par pare-feu, la pseudonymisation, l’encryption et mot de passe. </1>
     
    <1>
    Lors du traitement des Données Personnelles, <1>https://www.super-responsable.org</1>prend toutes les mesures raisonnables visant à les protéger contre toute perte, utilisation détournée, accès non autorisé, divulgation, altération ou destruction.</1>
     
    <0>9. Liens hypertextes « cookies » et balises (“tags”) internet</0>
    <1>
    Le site <1>https://www.super-responsable.org</1> contient un certain nombre de liens hypertextes vers d’autres sites, mis en place avec l’autorisation de <1>https://www.super-responsable.org</1>. Cependant, <1>https://www.super-responsable.org</1> n’a pas la possibilité de vérifier le contenu des sites ainsi visités, et n’assumera en conséquence aucune responsabilité de ce fait.</1>
    Sauf si vous décidez de désactiver les cookies, vous acceptez que le site puisse les utiliser. Vous pouvez à tout moment désactiver ces cookies et ce gratuitement à partir des possibilités de désactivation qui vous sont offertes et rappelées ci-après, sachant que cela peut réduire ou empêcher l’accessibilité à tout ou partie des Services proposés par le site.
    <1></1>

    <3>9.1. « COOKIES »</3>
     <1>
    Un « cookie » est un petit fichier d’information envoyé sur le navigateur de l’Utilisateur et enregistré au sein du terminal de l’Utilisateur (ex : ordinateur, smartphone), (ci-après « Cookies »). Ce fichier comprend des informations telles que le nom de domaine de l’Utilisateur, le fournisseur d’accès Internet de l’Utilisateur, le système d’exploitation de l’Utilisateur, ainsi que la date et l’heure d’accès. Les Cookies ne risquent en aucun cas d’endommager le terminal de l’Utilisateur.</1>
     <1><1>https://www.super-responsable.org</1> est susceptible de traiter les informations de l’Utilisateur concernant sa visite du Site, telles que les pages consultées, les recherches effectuées. Ces informations permettent à <1>https://www.super-responsable.org</1> d’améliorer le contenu du Site, de la navigation de l’Utilisateur.</1>
     <1>
    Les Cookies facilitant la navigation et/ou la fourniture des services proposés par le Site, l’Utilisateur peut configurer son navigateur pour qu’il lui permette de décider s’il souhaite ou non les accepter de manière à ce que des Cookies soient enregistrés dans le terminal ou, au contraire, qu’ils soient rejetés, soit systématiquement, soit selon leur émetteur. L’Utilisateur peut également configurer son logiciel de navigation de manière à ce que l’acceptation ou le refus des Cookies lui soient proposés ponctuellement, avant qu’un Cookie soit susceptible d’être enregistré dans son terminal. <1>https://www.super-responsable.org</1> informe l’Utilisateur que, dans ce cas, il se peut que les fonctionnalités de son logiciel de navigation ne soient pas toutes disponibles.</1>
     <1>
    Si l’Utilisateur refuse l’enregistrement de Cookies dans son terminal ou son navigateur, ou si l’Utilisateur supprime ceux qui y sont enregistrés, l’Utilisateur est informé que sa navigation et son expérience sur le Site peuvent être limitées. Cela pourrait également être le cas lorsque <1>https://www.super-responsable.org</1> ou l’un de ses prestataires ne peut pas reconnaître, à des fins de compatibilité technique, le type de navigateur utilisé par le terminal, les paramètres de langue et d’affichage ou le pays depuis lequel le terminal semble connecté à Internet.</1>
     <1>
    Le cas échéant, <1>https://www.super-responsable.org</1> décline toute responsabilité pour les conséquences liées au fonctionnement dégradé du Site et des services éventuellement proposés par <1>https://www.super-responsable.org</1>, résultant (i) du refus de Cookies par l’Utilisateur (ii) de l’impossibilité pour <1>https://www.super-responsable.org</1> d’enregistrer ou de consulter les Cookies nécessaires à leur fonctionnement du fait du choix de l’Utilisateur. Pour la gestion des Cookies et des choix de l’Utilisateur, la configuration de chaque navigateur est différente. Elle est décrite dans le menu d’aide du navigateur, qui permettra de savoir de quelle manière l’Utilisateur peut modifier ses souhaits en matière de Cookies.</1>
     <1>
    À tout moment, l’Utilisateur peut faire le choix d’exprimer et de modifier ses souhaits en matière de Cookies. <1>https://www.super-responsable.org</1> pourra en outre faire appel aux services de prestataires externes pour l’aider à recueillir et traiter les informations décrites dans cette section.</1>
     <1>
    Enfin, en cliquant sur les icônes dédiées aux réseaux sociaux Twitter, Facebook, Linkedin et Google Plus figurant sur le Site de <1>https://www.super-responsable.org</1> ou dans son application mobile et si l’Utilisateur a accepté le dépôt de cookies en poursuivant sa navigation sur le Site Internet ou l’application mobile de <1>https://www.super-responsable.org</1>, Twitter, Facebook, Linkedin et Google Plus peuvent également déposer des cookies sur vos terminaux (ordinateur, tablette, téléphone portable).</1>
     <1>
    Ces types de cookies ne sont déposés sur vos terminaux qu’à condition que vous y consentiez, en continuant votre navigation sur le Site Internet ou l’application mobile de <1>https://www.super-responsable.org</1>. À tout moment, l’Utilisateur peut néanmoins revenir sur son consentement à ce que <1>https://www.super-responsable.org</1> dépose ce type de cookies.</1>
     
    <3>Article 9.2. BALISES (“TAGS”) INTERNET</3>
     

    <1>

    <1>https://www.super-responsable.org</1> peut employer occasionnellement des balises Internet (également appelées « tags », ou balises d’action, GIF à un pixel, GIF transparents, GIF invisibles et GIF un à un) et les déployer par l’intermédiaire d’un partenaire spécialiste d’analyses Web susceptible de se trouver (et donc de stocker les informations correspondantes, y compris l’adresse IP de l’Utilisateur) dans un pays étranger.</1>
     
    <1>
    Ces balises sont placées à la fois dans les publicités en ligne permettant aux internautes d’accéder au Site, et sur les différentes pages de celui-ci. 
     </1>
    <1>
    Cette technologie permet à <1>https://www.super-responsable.org</1> d’évaluer les réponses des visiteurs face au Site et l’efficacité de ses actions (par exemple, le nombre de fois où une page est ouverte et les informations consultées), ainsi que l’utilisation de ce Site par l’Utilisateur. </1>
     <1>
    Le prestataire externe pourra éventuellement recueillir des informations sur les visiteurs du Site et d’autres sites Internet grâce à ces balises, constituer des rapports sur l’activité du Site à l’attention de <1>https://www.super-responsable.org</1>, et fournir d’autres services relatifs à l’utilisation de celui-ci et d’Internet.</1>
     <1>
    </1><0>10. Droit applicable et attribution de juridiction.</0>  
     <1>
    Tout litige en relation avec l’utilisation du site <1>https://www.super-responsable.org</1> est soumis au droit français. 
    En dehors des cas où la loi ne le permet pas, il est fait attribution exclusive de juridiction aux tribunaux compétents de Cannes</1>`,
    dialog: {
        shouldBeConnected: "Connecte-toi pour effectuer cette action",
        cookies: {
            title: "Ce site web utilise des cookies (miam)",
            info: `Nous utilisons les cookies pour t'offrir la meilleure expérience possible.<0 />
            En cliquant sur « Tout accepter », tu acceptes l’utilisation de cookies à usages techniques nécessaires à son bon fonctionnement, ainsi que des cookies, y compris des cookies tiers, à des fins statistiques ou de personnalisation pour te proposer des services et des offres adaptés à tes centres d’intérêts sur notre site. Pour en savoir plus et paramétrer, clique <1>ICI</1>`,
            button: "Tout accepter",
            areYouSure: {
                title: "Tu es sûr(e) de vouloir nous désactiver ?",
                info: `Nous sommes de gentils cookies, et nous participons à améliorer ton expérience utilisateur, mais également celles de tous les utilisateurs ! Les statistiques récoltées nous permettent d'aider les marques présentes sur le site en leur donnant de précieux conseils, quand aux autres ils te fournissent le meilleur site possible.`,
                yes: "Je désactive",
                no: "J'ai changé d'avis"
            }
        },
        customerOpinion: {
            title: `Ton avis contre un <0>bon plan</0>`,
            info: `Bienvenue à toi jeune responsable ! Comment trouves-tu notre site ? Quelque chose à améliorer ? Que préfères-tu ? Merci de rester courtois et respectueux`,
            already: "Tu as déjà posté un avis avec cette adresse mail, merci !"
        },
        newAchievement: {
            info: "Tes actions ont débloqué<0/> un bon plan",
            button: "Découvrir"
        },
        createAccount: {
            info: "Connecte-toi pour débloquer<0/> ton bon plan dès maintenant",
            button: "C'est parti"
        }
    },
    newsletter: {
        title: "Nos super news dans ta boîte mail ",
        info: "Inscris-toi à la newsletter pour recevoir un bon plan et découvrir les nouveautés dans ta boîte mail 1 fois par mois",
        button: "Envoyer",
        toast: {
            subscribeSuccess: "Merci, tu est inscrit à notre super newsletter",
            subscribeError: "Une erreur est survenue lors de l'inscription à la newsletter, contacte-nous"
        }
    },
    cookies: {
        info: `<0>En savoir plus et paramétrer les cookies</0>
        Cette page a pour but de vous informer sur l’utilisation des cookies de nos sites et de vous indiquer comment les paramétrer.
        
        <1/><1/><0>Qu’est-ce qu’un cookie ?</0>
        Les cookies sont de petits fichiers texte déposés sur votre ordinateur/tablette/smartphone/télévision connectée/console de jeux vidéo connectée à Internet lors de la visite d'un site. Ils ont pour but de collecter des informations relatives à votre navigation et de vous adresser des offres et services personnalisés.
        <1/>Les cookies sont gérés par votre navigateur internet.
        
        <1/><1/><0>Pourquoi utilisons-nous des cookies ?</0>
        Ils nous permettent de vérifier le bon fonctionnement du site et permettent par exemple d’enregistrer vos préférences, la validation de votre souscription, votre identification à votre espace client.
        <1/>Nous utilisons également des cookies pour mesurer l’audience de nos sites. Ainsi nous pouvons analyser la fréquentation et l’utilisation de nos sites et améliorer constamment nos services et contenus.
        <1/>Il existe aussi des cookies émis par des tiers pour suivre des opérations de publicité ciblée ou partager des contenus via des réseaux sociaux par exemple.
        
        <1/><1/><0>Comment accepter ou refuser les cookies ?</0>
        Pour gérer vos préférences, activer ou désactiver les cookies ci-dessous, puis cliquez sur VALIDER`,
        statistics:  {
            info: `Il s'agit des cookies qui nous permettent de connaître l'utilisation et les performances du site et d'en améliorer le fonctionnement (par ex : les pages le plus souvent consultées, recherches,...)`,
            title: "Données de statistiques"
        },
        necessary: {
            info: `Cookies indispensables au bon fonctionnement du site, ils ne peuvent être désactivés de nos systèmes. Si ces cookies sont bloqués par votre navigateur, des éléments du site ne pourront pas fonctionner.`,
            title: "Données indispensables"
        },
    },
    cnil: `Pour plus d'informations sur les cookies, visitez le site de la <0>CNIL.</0>`,
    or: "ou"
}